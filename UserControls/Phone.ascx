<%@ Control AutoEventWireup="true" CodeFile="Phone.ascx.cs" Inherits="Phone" Language="C#" %>
<asp:TextBox ID="txtPhone1" runat="server" MaxLength="3" Width="27px"></asp:TextBox>-<asp:TextBox
ID="txtPhone2" runat="server" MaxLength="3" Width="26px"></asp:TextBox>-<asp:TextBox
ID="txtPhone3" runat="server" MaxLength="4" Width="35px"></asp:TextBox><asp:RequiredFieldValidator
ID="Req1" runat="server" ControlToValidate="txtPhone1" Display="Dynamic" Enabled="False"
ErrorMessage="Please enter first 3 digits of the number.">*</asp:RequiredFieldValidator><asp:RequiredFieldValidator
ID="Req2" runat="server" ControlToValidate="txtPhone2" Display="Dynamic" Enabled="False"
ErrorMessage="Please enter second 3 digits of the number.">*</asp:RequiredFieldValidator><asp:RequiredFieldValidator
ID="Req3" runat="server" ControlToValidate="txtPhone3" Display="Dynamic" Enabled="False"
ErrorMessage="Please enter last 4 digits of the number.">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
ID="Reg1" runat="server" ControlToValidate="txtPhone1" Display="Dynamic" ErrorMessage="Please enter valid first 3 digits in the number."
ValidationExpression="\d\d\d">*</asp:RegularExpressionValidator><asp:RegularExpressionValidator
ID="Reg2" runat="server" ControlToValidate="txtPhone2" Display="Dynamic" ErrorMessage="Please enter valid second 3 digits in the number."
ValidationExpression="\d\d\d">*</asp:RegularExpressionValidator><asp:RegularExpressionValidator
ID="Reg3" runat="server" ControlToValidate="txtPhone3" Display="Dynamic" ErrorMessage="Please enter valid last 4 digits in the number."
ValidationExpression="\d\d\d\d">*</asp:RegularExpressionValidator>
