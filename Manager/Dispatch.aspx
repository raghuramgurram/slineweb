<%@ Page AutoEventWireup="true" CodeFile="Dispatch.aspx.cs" Inherits="Dispatch" Language="C#"
    MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="../UserControls/TimePicker.ascx" TagName="TimePicker" TagPrefix="uc4" %>
<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/Grid.ascx" TagName="Grid" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc1" %>

<%--SLine 2016 Enhancements--%>
<%@ Register Src="~/UserControls/wucDatePickerForDispatcher.ascx" TagName="DatePickerForDispatcher" TagPrefix="ucDispatcher" %>
<%--SLine 2016 Enhancements--%>

<%--
<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="dtpWorkPermit" TagPrefix="uc12" %>
<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="dtpLicenseExpiry" TagPrefix="uc13" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">
        function WinOpen(idParam) {
            //alert('LoadDetails.aspx?'+idParam);
            window.open('~/LoadDetails.aspx?' + idParam, 'Load Details', 'scrollbars=yes,resizable=yes,width=600,height=600');
        }
    </script>
    <style>
        .ml-10 {
            margin-left: 10px;
        }
    </style>

    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
        <tr valign="top">
            <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td><%--<uc1:GridBar ID="GridBar1" runat="server" />--%>
                            <table id="GridBar" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblLoadId" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:LinkButton ID="lnkLoadRequest" runat="server" Text="Load Request" CausesValidation="false"></asp:LinkButton>&nbsp;|&nbsp;
                        <asp:LinkButton ID="lnkLoadDetails" runat="server" Text="Load Deatils" CausesValidation="false"></asp:LinkButton>&nbsp;
                          <asp:LinkButton ID="lnkUploadStatus" runat="server" Text="|&nbsp; Update Load Status" CausesValidation="false"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img src="../images/pix.gif" alt="" width="1" height="3" /></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="height: auto;">
                    <tr>
                        <th align="center">
                            <blink><asp:Label Visible="false" ID="lblWarning" ForeColor="red" Font-Bold="true" Font-Size="Large" runat="server" Text="!!!There are notes attached to this load. Please look into the notes prior to proceeding further.!!!"></asp:Label></blink>
                        </th>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="height: auto;">
                    <tr>
                        <td width="50%" valign="top">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblform">
                                <tr>
                                    <th colspan="2">Load Assignment (&nbsp;<strong><asp:LinkButton ID="lnkUpload" runat="server" Text="Upload Documents" OnClick="lnkUpload_Click" CausesValidation="false" /></strong> &nbsp;|&nbsp; <strong>
                                        <asp:LinkButton ID="lnkLoadCharge" runat="server" Text="Load Charges" OnClick="lnkLoadCharge_Click" CausesValidation="false" /></strong>&nbsp;) </th>


                                </tr>
                                <tr id="altrow" style="height: 12px;">
                                    <td align="right" style="height: 14px;">Customer : </td>
                                    <td style="height: 14px">
                                        <asp:Label ID="lblCustomer" runat="server" Text="" />
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td align="right" style="height: 14px;">Assign To : </td>
                                    <td style="height: 14px">&nbsp;<asp:RadioButton ID="radCarrier" CssClass="radio" runat="server" Text="Carrier" TextAlign="Right" Checked="True" AutoPostBack="True" OnCheckedChanged="radCarrier_CheckedChanged" GroupName="RadGroup" />
                                        <asp:RadioButton ID="rabDriver" CssClass="radio" runat="server" Text="Driver" TextAlign="Right" AutoPostBack="True" OnCheckedChanged="rabDriver_CheckedChanged" ValidationGroup="RadGroup" GroupName="RadGroup" /></td>
                                </tr>
                                <tr id="row">
                                    <td align="right" valign="top">
                                        <asp:Label ID="lblCarrier" runat="server" Text="Carrier :"></asp:Label></td>
                                    <td>
                                        <asp:DropDownList ID="ddlCarrier" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCarrier_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:Label ID="lblErrCarrier" runat="server" ForeColor="Red" Text="Select carrier."
                                            Visible="False"></asp:Label>
                                        &nbsp;&nbsp;<asp:Label ID="lblInvoice" runat="server" Text="Invoice# :"></asp:Label>
                                        <asp:TextBox ID="txtInvoice" runat="server" MaxLength="50"></asp:TextBox>
                                        <br />
                                        <asp:Label ID="lblCustAssigned" runat="server" Text="Date / Time : "></asp:Label>&nbsp;<uc3:DatePicker ID="dtpCustAssigned" runat="server" IsDefault="false" IsRequired="true" SetInitialDate="true" />

                                        &nbsp;<uc4:TimePicker ID="tpCustAssigned" runat="server" ReqFieldValidation="true" SetCurrentTime="true" />
                                        <asp:Button ID="btnUnassignCarrier" runat="server" CssClass="btnstyle" Text="Unassign Carrier" OnClick="btnUnassignCarrier_Click" CausesValidation="False" UseSubmitBehavior="False" Visible="False" /></td>
                                </tr>
                            </table>

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblform1">
                                <tr valign="top">
                                    <td width="100%">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr valign="top">
                                                <td>
                                                    <asp:Panel ID="pnlDrives" runat="Server" Width="100%" Visible="False">
                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblform" style="height: auto;">
                                                            <tr>
                                                                <th colspan="2">Driver(s) ( &nbsp;<strong><asp:LinkButton ID="lnkDriverCharges" runat="server" Text="Driver Charges" OnClick="lnkDriverCharges_Click" CausesValidation="false" /></strong>&nbsp;)</th>
                                                            </tr>
                                                            <tr id="row">
                                                                <td align="right" width="15%">Team : </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlteam" runat="server" Width="152" AutoPostBack="true" 
                                                                        OnSelectedIndexChanged="ddlTeam_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                    Driver : 
                              <asp:DropDownList ID="ddlDrivers" runat="server" AutoPostBack="True"
                                  OnSelectedIndexChanged="ddlDrivers_SelectedIndexChanged" EnableViewState="true" />
                                                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlDrivers"
                                                                        Display="Dynamic" ErrorMessage="CompareValidator" Operator="NotEqual" ToolTip="Please select driver"
                                                                        ValueToCompare="--Select Driver--">Select Driver</asp:CompareValidator>
                                                                    <asp:Label ID="lblErrDriver" runat="server" ForeColor="Red" Text="Select Driver.."
                                                                        Visible="False"></asp:Label>
                                                                    <asp:LinkButton ID="lnkNew" runat="server" Text="New" OnClick="lnkNew_Click" CausesValidation="false" />
                                                                </td>
                                                            </tr>
                                                            <%--<tr id="row">                                
                              <td width="15%" align="right">Driver : </td>
                              <td><asp:DropDownList ID="ddlDrivers" runat="server" AutoPostBack="True" 
                                      onselectedindexchanged="ddlDrivers_SelectedIndexChanged" EnableViewState="true"/>
                                  <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlDrivers"
                                      Display="Dynamic" ErrorMessage="CompareValidator" Operator="NotEqual" ToolTip="Please select driver"
                                      ValueToCompare="--Select Driver--">Select Driver</asp:CompareValidator>--%>
                                                            <%--<asp:DropDownList ID="ddlDriverAssign" runat="Server" OnSelectedIndexChanged="ddlDrivers_SelectedIndexChanged" AutoPostBack="True">                                   
                                    <asp:ListItem Text="Assign" Value="Assign"></asp:ListItem>
                                    <asp:ListItem Text="Edit" Value="Edit"></asp:ListItem>
                                 </asp:DropDownList>
                                  <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlDriverAssign"
                                      Display="Dynamic" ErrorMessage="CompareValidator" Operator="NotEqual" ToolTip="Select Edit or Assign"
                                      ValueToCompare="Select">Select Edit or Assign</asp:CompareValidator>--%>
                                                            <%--<asp:Label ID="lblErrDriver" runat="server" ForeColor="Red" Text="Select Driver.."
                                      Visible="False"></asp:Label>
                                <asp:LinkButton ID="lnkNew" runat="server" Text="New" OnClick="lnkNew_Click" CausesValidation="false"/></td>
                              </tr>--%>

                                                            <%--SLine 2016 Enhancements for Providing Dispatcher provision to edit Driver details--%>



                                                            <tr id="trForUpdateDriverDetails" runat="server" style="display: none">
                                                                <td colspan="2" style="border: 2">
                                                                    <table style="background: #eee; font-family: Arial; color: #555; font-size: 12px; padding-left: 5px; border-top: 1px solid #ccc; border-bottom: 1px solid #ccc; margin: 5px 0;"
                                                                        width="100%">
                                                                        <tr id="trLicenceExp" runat="server" class="row" style="display: none">
                                                                            <td colspan="4">
                                                                                <table>
                                                                                    <tr id="row">
                                                                                        <td align="right" style="padding-left: 80px;">Driver License # :
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="txtLicense" runat="server" MaxLength="100" ReadOnly="true"></asp:TextBox>
                                                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtLicense"
                                                    Display="Dynamic" ErrorMessage="Please enter Driving License #.">*</asp:RequiredFieldValidator>--%>
                                                                                        </td>
                                                                                        <td align="right" style="padding-left: 30px;">&nbsp;License # Expiry Date :
                                                                                        </td>
                                                                                        <td>
                                                                                            <ucDispatcher:DatePickerForDispatcher ID="dtpLicenseExpiry" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="row">
                                                                            <td colspan="4">
                                                                                <table width="100%">
                                                                                    <tr id="trMedExp" runat="server" style="display: none">
                                                                                        <td align="left" style="padding-left: 80px; width: 143px;">Medical Card Expiry Date :</td>
                                                                                        <td>
                                                                                            <ucDispatcher:DatePickerForDispatcher ID="dtpMedicalCard" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="right" colspan="4">
                                                                                            <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btnstyle"
                                                                                                OnClick="btnUpdate_Click" />

                                                                                            <asp:Button runat="Server" Text="Cancel" ID="btnCancel" CssClass="btnstyle"
                                                                                                CausesValidation="False" UseSubmitBehavior="False"
                                                                                                OnClick="btnCancel_Click" /></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                            <tr id="altrow">
                                                                <td align="right">From : </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtFrom" runat="server" MaxLength="50" />
                                                                    City : 
                              <asp:TextBox ID="txtFromCity" runat="server" MaxLength="50" CssClass="ml-10" />
                                                                    &nbsp;State :
                                <asp:TextBox ID="txtFromstate" runat="server" MaxLength="50" /></td>
                                                            </tr>
                                                            <tr id="row">
                                                                <td align="right">To : </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtTO" runat="server" MaxLength="50" />
                                                                    City :                       
                              <asp:TextBox ID="txtToCity" runat="server" MaxLength="50" CssClass="ml-10" />
                                                                    &nbsp;State :
                            <asp:TextBox ID="txtState" runat="server" MaxLength="50" /></td>
                                                            </tr>
                                                            <tr id="altrow">
                                                                <td align="right">Tag# :</td>
                                                                <td>
                                                                    <asp:TextBox ID="txtTag" runat="server" MaxLength="50" />
                                                                    &nbsp;&nbsp;&nbsp;<asp:Label ID="lblAssignDriver" runat="server"
                                                                        Text="Date / Time : ">
                                                                        <uc3:DatePicker ID="dtpAssignDriver" runat="server" IsDefault="false" IsRequired="true" SetInitialDate="true" />
                                                                    </asp:Label><%--<uc3:DatePicker ID="dtpAssignDriver" runat="server" IsDefault="false" IsRequired="true" SetInitialDate="true" />--%>
                                                                    <uc4:TimePicker ID="tpAssignDriver" runat="server" ReqFieldValidation="true" />
                                                                    &nbsp;&nbsp;<asp:CheckBox ID="chkSendSms" runat="server" Height="15px" Text="Send SMS" Checked="true" Style="text-align: center" />&nbsp;&nbsp;<asp:CheckBox ID="chkSendMail" runat="server" Height="15px" Text="Send Email" Checked="true" Style="text-align: center" />
                                                                    &nbsp;&nbsp;<asp:CheckBox ID="Chkwhatsapp" runat="server" Height="15px" Text="Send WhatsApp" Checked="true" Style="text-align: center" /></td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                                <td>
                                                    <asp:Panel ID="pnlSpace" runat="server">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td style="width: 1%;">
                                                                    <img height="1" src="../images/pix.gif" width="5" /></td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                                <%--<td style="width: 1%;"><img src="../images/pix.gif" width="5" height="1" /></td>--%>
                                                <td>
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblform" style="left: 200px;">
                                                        <tr>
                                                            <th colspan="2">Container Size/Location Details </th>
                                                        </tr>
                                                        <tr id="row">
                                                            <td width="40%" align="right">Container Size/Location :</td>
                                                            <td>
                                                                <asp:TextBox ID="txtRailBill" runat="server" MaxLength="50" /></td>
                                                        </tr>
                                                        <tr id="altrow">
                                                            <td align="right">Order Bill Date :</td>
                                                            <td>
                                                                <uc3:DatePicker ID="txtOrderBillDate" runat="server" Visible="true" IsDefault="true" IsRequired="false" CountNextYears="2" CountPreviousYears="0" />
                                                            </td>
                                                        </tr>
                                                        <tr id="row">
                                                            <td align="right">Person Bill : </td>
                                                            <td>
                                                                <asp:TextBox ID="txtPersonBill" runat="server" MaxLength="50" /></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <img alt="" height="5" src="../images/pix.gif" width="1" /></td>
                                            </tr>
                                        </table>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
                                            <tr>
                                                <td align="right">
                                                    <blink><asp:Label Visible="false" ID="lblWarningPOH" ForeColor="red" Font-Bold="true" runat="server" Font-Size="Smaller" Text="This load is currently put on hold, change the status if you want to assign.  "></asp:Label></blink>
                                                    <asp:Button ID="btnAssignToLoadPlaner" runat="server" CssClass="btnstyle" OnClick="btnSave_Click"
                                                        Text="Add To Load Planner" Width="140px" Visible="false" />
                                                    <asp:Button ID="btnConformAssign" runat="server" CssClass="btnstyle" OnClick="btnConformAssign_Click"
                                                        Text="Confirm Assign" Width="105px" CausesValidation="False" Visible="false" />
                                                    <asp:Button ID="btnSaveCarrier" runat="server" CssClass="btnstyle" OnClick="btnSave_Click"
                                                        Text="Assign To Load" Width="101px" Visible="false" />
                                                    <asp:Button ID="btnSave" runat="server" CssClass="btnstyle" Text="Save" OnClick="btnCancelCarrier_Click" />
                                                    <asp:Button ID="btnCancelCarrier" runat="server" CssClass="btnstyle" Text="Cancel" OnClick="btnCancelCarrier_Click" CausesValidation="False" /></td>
                                            </tr>
                                        </table>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <img alt="" height="5" src="../images/pix.gif" width="1" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <!--  <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
            </tr>
          </table>
          <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
            <tr>
              <td align="right"><input name="Button" type="button" class="btnstyle" value="Assign to Load" />
              </td>
            </tr>
          </table>     -->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <uc2:Grid ID="Grid2" runat="server" Visible="true" />
                        </td>
                    </tr>
                </table>

                <%-- <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
            <tr>
              <td align="right"><input name="Button2" type="button" class="btnstyle" value="Save" />
                  <input name="Submit2" type="button" class="btnstyle" value="Cancel" /></td>
            </tr>
          </table>--%>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td width="49%">
                            <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <th colspan="4">Order Identification  <span id="spanishazmat" runat="server" style="color: Red; text-align: right; width: 80%">HAZMAT</span></th>
                                </tr>
                                <tr id="row">
                                    <td align="right" valign="middle" width="20%">Customer : </td>
                                    <td colspan="3" valign="middle">
                                        <asp:Label ID="Label1" runat="server" />&nbsp;</td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" valign="middle">Pickup# : </td>
                                    <td valign="middle" width="25%">
                                        <asp:Label ID="lblPickUp" runat="server" />&nbsp;</td>
                                    <td align="right" valign="middle">Last Free Date : </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblLastfreedate" runat="server" Text="" />&nbsp;</td>
                                </tr>
                                <tr id="row">
                                    <td align="right" valign="middle">Container# : </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblContainer" runat="server" />&nbsp;</td>
                                    <td align="right" valign="middle">Commodity :</td>
                                    <td valign="middle">
                                        <asp:Label ID="lblCommodity" runat="server" Text="" />&nbsp;</td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" valign="middle">Chasis# : </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblChasis" runat="server" Text=""></asp:Label>&nbsp;</td>
                                    <td align="right" valign="middle">Seal# :  </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblSeal" runat="server" Text="" />&nbsp;</td>
                                </tr>
                                <tr id="row">
                                    <td align="right" valign="middle">Pieces : </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblPieces" runat="server"></asp:Label>&nbsp;
                                    </td>
                                    <td align="right" valign="middle">Booking# : </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblBooking" runat="server" />&nbsp;</td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" valign="middle">Weight : </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblWeigth" runat="server"></asp:Label>&nbsp;</td>
                                    <td align="right" valign="middle">&nbsp; </td>
                                    <td valign="middle">&nbsp;</td>
                                </tr>
                                <tr id="row">
                                    <td align="right" valign="middle">Created By : </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblPersong" runat="server" Text="" />&nbsp;</td>
                                    <td align="right" valign="middle">Created On : </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblCreated" runat="server" />&nbsp;</td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" valign="middle">Updated By : </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblUpdatedBy" runat="server" Text="" />&nbsp;</td>
                                    <td align="right" valign="middle">Updated On : </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblUpdatedOn" runat="server" />&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 2px; height: auto;"></td>
                        <td width="49%" style="height: auto;">
                            <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <th colspan="2">Routing</th>
                                </tr>
                                <tr id="row">
                                    <td colspan="2">
                                        <asp:Label ID="lblRoute" runat="server" Text=""></asp:Label>&nbsp;
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" width="40%">Pier Termination / Empty Return:</td>
                                    <td>
                                        <asp:Label ID="lblPier" runat="server" Text=""></asp:Label>&nbsp;
                                    </td>
                                </tr>
                                <tr id="row" valign="top">
                                    <td align="right">Contact Person(s) :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblShippingContact" runat="server" Text=""></asp:Label>&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img src="../Images/pix.gif" alt="" width="1" height="10" /></td>
                    </tr>
                </table>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <uc2:Grid ID="Grid3" runat="server" Visible="true" />
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img src="../Images/pix.gif" alt="" width="1" height="10" /></td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <uc2:Grid ID="Grid4" runat="server" Visible="true" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    </table>
     
</asp:Content>

