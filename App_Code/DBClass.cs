using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// This is Class of All Common DB Functions
/// </summary>
public class DBClass
{
	public DBClass()
	{       
	}
    
	/// <summary>
	/// Builds a DataList Taking SQL Query
	/// </summary>
	/// <param name="sqlString"></param>
	/// <param name="tableName"></param>
	/// <param name="listname"></param>
    /// 
    public static void bulidDataList(string sqlString, string tableName, ref System.Web.UI.WebControls.DataList listname)
	{
        DataSet ds = null;
        SqlDataAdapter da = null;
        SqlTransaction myTrans = null;
        SqlConnection myConn = null;
        try
        {            
            myConn = new SqlConnection(ConfigurationSettings.AppSettings["conString"].ToString());           
            myConn.Open();
            myTrans = myConn.BeginTransaction(IsolationLevel.ReadUncommitted);
            ds = new DataSet();
            da = new SqlDataAdapter(sqlString, myConn);
            da.SelectCommand.Transaction = myTrans;
            da.Fill(ds, tableName);
            listname.DataSource = ds.Tables[tableName];
            listname.DataBind();
            myTrans.Commit();
        }
        catch
        {
            if (myTrans != null) { myTrans.Rollback(); }
        }
        finally
        {
            if (myTrans != null) { myTrans.Dispose(); myTrans = null; }
            if (myConn != null) { myConn.Close(); myConn.Dispose(); myConn = null; }            
            if (da != null) { da.Dispose(); da = null; }           
            if (ds != null) { ds.Dispose(); ds = null; }            
        }
	}
	/// <summary>
	/// Executes A Scalar Query :: Returns Integer as Output
	/// </summary>
	/// <param name="sqlString"></param>
	/// <param name="returnType"></param>
	/// <returns></returns>
    public static string executeScalar(string sqlString)
	{
        SqlCommand cmd = null;
        string returnStr = "";
        SqlTransaction myTrans = null;
        SqlConnection myConn = null;
        try
        {
            myConn = new SqlConnection(ConfigurationSettings.AppSettings["conString"].ToString());
            myConn.Open();
            myTrans = myConn.BeginTransaction(IsolationLevel.ReadUncommitted);
            cmd = new SqlCommand(sqlString, myConn, myTrans);
            returnStr = Convert.ToString(cmd.ExecuteScalar());            
            myTrans.Commit();
        }
        catch
        {
            if (myTrans != null) { myTrans.Rollback(); }
        }
        finally
        {            
            if (myTrans != null) { myTrans.Dispose(); myTrans = null; }
            if (myConn != null) { myConn.Close(); myConn.Dispose(); myConn = null; }
            if (cmd != null) { cmd.Dispose(); cmd = null; }            
        }
        return returnStr;
	}
	/// <summary>
	/// Executes A Non Query
	/// </summary>
	/// <param name="sqlString"></param>
	/// <returns></returns>
    public static int executeNonQuery(string sqlString)
	{
        SqlCommand cmd = null;
        int returnCount = 0;
        SqlTransaction myTrans = null;
        SqlConnection myConn = null;
        try
        {
            myConn = new SqlConnection(ConfigurationSettings.AppSettings["conString"].ToString());
            myConn.Open();
            myTrans = myConn.BeginTransaction(IsolationLevel.ReadCommitted);
            cmd = new SqlCommand(sqlString, myConn, myTrans);
		    returnCount = cmd.ExecuteNonQuery();
            myTrans.Commit();
        }
        catch
        {
            if (myTrans != null) { myTrans.Rollback(); }
        }
        finally
        {            
            if (myTrans != null) { myTrans.Dispose(); myTrans = null; }
            if (myConn != null) { myConn.Close(); myConn.Dispose(); myConn = null; }
            if (cmd != null) { cmd.Dispose(); cmd = null; }
        }
        return returnCount;
	}
	/// <summary>
	/// Returns a SQL Data Reader
	/// </summary>
	/// <param name="sqlString"></param>
	/// <returns></returns>
    public static SqlDataReader returnReader(string sqlString)
	{
        SqlCommand cmd = null;
        SqlDataReader objReader = null;
        SqlTransaction myTrans = null;
        SqlConnection myConn = null;
        try
        {
            myConn = new SqlConnection(ConfigurationSettings.AppSettings["conString"].ToString());
            myConn.Open();
            myTrans = myConn.BeginTransaction(IsolationLevel.ReadUncommitted);
            cmd = new SqlCommand(sqlString, myConn, myTrans);
            objReader = cmd.ExecuteReader();
            myTrans.Commit();	            
        }
        catch
        {
            if (myTrans != null) { myTrans.Rollback(); }
        }
        finally
        {
            if (myTrans != null) { myTrans.Dispose(); myTrans = null; }
            if (myConn != null) { myConn.Close(); myConn.Dispose(); myConn = null; }
            if (cmd != null) { cmd.Dispose(); cmd = null; }
        }
        return objReader;
	}
	/// <summary>
	/// Returns a DataSet with table given name
	/// </summary>
	/// <param name="sqlString"></param>
	/// <param name="tableName"></param>
	/// <returns></returns>
    public static DataSet returnDataSet(string sqlString, string tableName)
	{
        DataSet ds = new DataSet();
        SqlDataAdapter objAdapter = null;
        SqlTransaction myTrans = null;
        SqlConnection myConn = null;
        try
        {
            myConn = new SqlConnection(ConfigurationSettings.AppSettings["conString"].ToString());
            myConn.Open();
            myTrans = myConn.BeginTransaction(IsolationLevel.ReadUncommitted);
            objAdapter = new SqlDataAdapter(sqlString, myConn);
            objAdapter.SelectCommand.Transaction = myTrans;
            objAdapter.Fill(ds, tableName);
            myTrans.Commit();
        }
        catch
        {
            if (myTrans != null) { myTrans.Rollback(); }
        }
        finally
        {            
            if (myTrans != null) { myTrans.Dispose(); myTrans = null; }
            if (myConn != null) { myConn.Close(); myConn.Dispose(); myConn = null; }
            if (objAdapter != null) { objAdapter.Dispose(); objAdapter = null; }
        }
        return ds;
	}
    /// <summary>
    /// Returns a DataTable with table given name
    /// </summary>
    /// <param name="sqlString"></param>
    /// <param name="tableName"></param>
    /// <returns></returns>
    public static DataTable returnDataTable(string sqlString)
    {
        DataTable dt = new DataTable();
        SqlDataAdapter objAdapter = null;
        SqlTransaction myTrans = null;
        SqlConnection myConn = null;
        try
        {
            myConn = new SqlConnection(ConfigurationSettings.AppSettings["conString"].ToString());
            myConn.Open();
            myTrans = myConn.BeginTransaction(IsolationLevel.ReadUncommitted);
            objAdapter = new SqlDataAdapter(sqlString, myConn);
            objAdapter.SelectCommand.Transaction = myTrans;
            objAdapter.Fill(dt);
            myTrans.Commit();
        }
        catch(Exception ex)
        {
            if (myTrans != null) { myTrans.Rollback(); }
        }
        finally
        {           
            if (myTrans != null) { myTrans.Dispose(); myTrans = null; }
            if (myConn != null) { myConn.Close(); myConn.Dispose(); myConn = null; }
            if (objAdapter != null) { objAdapter.Dispose(); objAdapter = null; }
        }
        return dt;
    }
    /// <summary>
    /// Execute Multiple Queries with same Transaction
    /// </summary>
    /// <param name="sqlStrings"></param>
    /// <returns></returns>
    public static int executeQueries(string[] sqlStrings)
    {
        SqlCommand cmd = null;
        int returnCount = 0;
        SqlTransaction myTrans = null;
        SqlConnection myConn = null;
        try
        {
            myConn = new SqlConnection(ConfigurationSettings.AppSettings["conString"].ToString());
            myConn.Open();
            myTrans = myConn.BeginTransaction(IsolationLevel.ReadCommitted);
            cmd = new SqlCommand();
            cmd.Connection = myConn;
            cmd.Transaction = myTrans;
            for (int i = 0; i < sqlStrings.Length; i++)
            {
                cmd.CommandText = sqlStrings[i];
                returnCount = cmd.ExecuteNonQuery();
            }            
            myTrans.Commit();
        }
        catch(Exception ex)
        {
            if (myTrans != null) { myTrans.Rollback(); }
        }
        finally
        {           
            if (myTrans != null) { myTrans.Dispose(); myTrans = null; }
            if (myConn != null) { myConn.Close(); myConn.Dispose(); myConn = null; }
            if (cmd != null) { cmd.Dispose(); cmd = null; }
        }
        return returnCount;
    }
    /// <summary>
    /// Execute Multiple Queries and sub queries with generated id  with same Transaction
    /// </summary>
    /// <param name="sqlStrings"></param>
    /// <returns></returns>
    public static int executeQueries(string[] sqlStrings,string[] subTableStrings)
    {
        SqlCommand cmd = null;
        int returnCount = 0;
        SqlTransaction myTrans = null;
        SqlConnection myConn = null;
        try
        {
            myConn = new SqlConnection(ConfigurationSettings.AppSettings["conString"].ToString());
            myConn.Open();
            myTrans = myConn.BeginTransaction(IsolationLevel.ReadCommitted);
            cmd = new SqlCommand();
            cmd.Connection = myConn;
            cmd.Transaction = myTrans;
            cmd.CommandText = sqlStrings[0];
            returnCount = cmd.ExecuteNonQuery();
            if (returnCount > 0)
            {
                cmd.CommandText = sqlStrings[1];
                int id = (int)cmd.ExecuteScalar();
                for (int i = 0; i < subTableStrings.Length; i++)
                {
                    cmd.CommandText = subTableStrings[i].Replace("^^^^ID^^^^",id.ToString());
                    returnCount = cmd.ExecuteNonQuery();
                }
            }
            myTrans.Commit();
        }
        catch
        {
            if (myTrans != null) { myTrans.Rollback(); }
        }
        finally
        {           
            if (myTrans != null) { myTrans.Dispose(); myTrans = null; }
            if (myConn != null) { myConn.Close(); myConn.Dispose(); myConn = null; }
            if (cmd != null) { cmd.Dispose(); cmd = null; }
        }
        return returnCount;
    }

}
