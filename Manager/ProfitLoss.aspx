<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProfitLoss.aspx.cs" Inherits="Manager_ProfitLoss" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<style>
.check input[type=checkbox]{height:auto; vertical-align:top; display:inline-block;}
</style>
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>
                <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="middle">
                        <asp:Label ID="lblMonth" runat="server" Text="Month : "></asp:Label>
                            <asp:DropDownList ID="ddlMonth" runat="server">
                                 <asp:ListItem Text="Jan" Value="1"/>
                                 <asp:ListItem Text="Feb" Value="2"/>
                                 <asp:ListItem Text="Mar" Value="3"/>
                                 <asp:ListItem Text="Apr" Value="4"/>
                                 <asp:ListItem Text="May" Value="5"/>
                                 <asp:ListItem Text="Jun" Value="6"/>
                                 <asp:ListItem Text="July" Value="7"/>
                                 <asp:ListItem Text="Aug" Value="8"/>
                                 <asp:ListItem Text="Sep" Value="9"/>
                                 <asp:ListItem Text="Oct" Value="10"/>
                                 <asp:ListItem Text="Nov" Value="11"/>
                                 <asp:ListItem Text="Dec" Value="12"/>
                            </asp:DropDownList>&nbsp; &nbsp;
                        <asp:Label ID="lblYear" runat="server" Text="Year : "></asp:Label>
                            <asp:DropDownList ID="ddlYear" runat="server">
                            </asp:DropDownList>
                        <span id="spnAllOfficeLocation" runat="server"><label style="display:inline-block; vertical-align:text-bottom;">All Office Locations:</label> <asp:CheckBox ID="chkAllOfficeLocation" runat="server" CssClass="check"></asp:CheckBox></span>
                        <asp:Button ID="btnSearch" runat="server" CssClass="btnStyle" Text="Search" onClick="btnSearch_Click"/>
                        <asp:Button ID="btnMonthWise" runat="server" CssClass="btnStyle" Text="Month Wise P&L" onClick="btnMonthWisePLReport_Click"/>
                        <%--<asp:Button ID="btnChartMonthly" runat="server" CssClass="btnStyle" Text="Monthly Charts" onClick="btnChartMonthly_Click"/>
                        <asp:Button ID="btnChartDaily" runat="server" CssClass="btnStyle" Text="Daily Charts" onClick="btnChartDaily_Click"/>--%>
                        </td>
                    </tr>
                </table>                
            </td>
        </tr>
    </table>
</asp:Content>
