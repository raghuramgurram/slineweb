using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class UserControls_USState : System.Web.UI.UserControl
{
    public string Text
    {
        get 
        {
            if (dropStates.Items.Count > 0 && dropStates.SelectedIndex!=0)
                return dropStates.SelectedItem.Text;
            else
                return "";
        }
        set 
        {
            if (dropStates.Items.Contains(new ListItem(Convert.ToString(value).Trim())))
                dropStates.SelectedIndex = dropStates.Items.IndexOf(new ListItem(Convert.ToString(value).Trim()));
            else
                dropStates.SelectedIndex = 0;
        }
    }
    public bool IsRequired
    {
        set { cmpState.Enabled = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
}
