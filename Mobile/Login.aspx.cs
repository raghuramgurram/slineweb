using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Mobile_Login : System.Web.UI.Page
{
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        ltlYearloginfooter.Text = DateTime.Now.ToString("yyyy");
        ltlcompanynameTitle.Text = ltlcompanyname.Text = ltlcompanynameSub.Text = ConfigurationManager.AppSettings["CompanyName"].ToString();
        if (!IsPostBack)
        {
            if (Request.Cookies[Keys.COOKES_USERNAME] != null)
                email.Text = Request.Cookies[Keys.COOKES_USERNAME].Value;
            if (Request.Cookies[Keys.COOKES_PWD] != null)
                password.Attributes.Add("value", Request.Cookies[Keys.COOKES_PWD].Value);
            if (Request.Cookies[Keys.COOKES_USERNAME] != null && Request.Cookies[Keys.COOKES_PWD] != null)
                checkbox1.Checked = true;
        }
    }
    #endregion

    #region Login Link
    protected void lnklogin_click(object sender, EventArgs e)
    {
        LoginBase loginCheck = new LoginBase();
        if (string.IsNullOrEmpty(email.Text) || string.IsNullOrEmpty(password.Text))
        {
            errorDiv.InnerHtml = "<p>" + ErrorMessages.LOGIN_IS_EMPTY + "</p>";
            errorDiv.Visible = true;
        }
        else
        {
            try
            {
                DataTable dt = loginCheck.Login(email.Text, password.Text);
                if (dt.Rows.Count > 0)
                {
                    try
                    {
                        int loginid = Convert.ToInt32(dt.Rows[0]["bint_UserLoginId"]);
                        int emploiid = Convert.ToInt32(dt.Rows[0][3]);
                        string type = dt.Rows[0]["nvar_RoleName"].ToString();

                        if (UserType.Driver.ToString() == type)
                        {
                            Session[Keys.LOGIN_ID_KEY] = loginid;

                            Session[Keys.EMPLOY_ID] = emploiid;
                            if (checkbox1.Checked)
                            {
                                Response.Cookies[Keys.COOKES_USERNAME].Value = email.Text;
                                Response.Cookies[Keys.COOKES_PWD].Value = password.Text;

                                Response.Cookies[Keys.COOKES_USERNAME].Expires = DateTime.Now.AddDays(15);
                                Response.Cookies[Keys.COOKES_PWD].Expires = DateTime.Now.AddDays(15);
                            }
                            else
                            {
                                Response.Cookies[Keys.COOKES_USERNAME].Expires = DateTime.Now.AddDays(-1);
                                Response.Cookies[Keys.COOKES_PWD].Expires = DateTime.Now.AddDays(-1);
                            }
                            Response.Redirect(Keys.PAGE_LOAD_HOME);
                        }
                        else
                        {
                            errorDiv.InnerHtml = "<p>" + ErrorMessages.LOGIN_IS_NOT_DRIVER + "</p>";
                            errorDiv.Visible = true;
                        }
                    }
                    catch (Exception es)
                    {
                        errorDiv.InnerHtml = "<p>" + ErrorMessages.LOGIN_IS_NOT_VALID + "</p>";
                        errorDiv.Visible = true;
                    }

                }
            }
            catch (Exception ex)
            {
                errorDiv.InnerHtml = "<p>" + ErrorMessages.LOGIN_IS_NOT_VALID + "</p>";
                errorDiv.Visible = true;
            }
        }
    }
    #endregion
}
