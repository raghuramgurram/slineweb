<%@ Page AutoEventWireup="true" CodeFile="CustomerAccount.aspx.cs" Inherits="CustomerAccount"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/Grid.ascx" TagName="Grid" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblbox1">
        <tr>
          <td align="left">
              &nbsp;<asp:Label ID="lblText" runat="server" Text="Select Customer :"></asp:Label>&nbsp;
              <asp:DropDownList ID="ddlList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlList_SelectedIndexChanged">
              </asp:DropDownList>&nbsp;
          </td>
        </tr>
        </table>      
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
            </tr>
        </table>    
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
              <uc2:GridBar ID="barCurrent" runat="server" HeaderText="Customers List" LinksList="Add New Customer Account"/>
          </td>
        </tr>
      </table>   
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tbldisplay">
          <tr>
            <td>
                <uc3:Grid ID="gridCurrent" runat="server" />                
            </td>
          </tr>
      </table>
      
    </td>
   </tr>
 </table>      
</asp:Content>

