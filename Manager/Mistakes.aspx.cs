using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Manager_Mistakes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Staff Mistakes";
        if (!IsPostBack)
            FillLists();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Session["ReportName"] = "GetStaffMistakes";
        Session["ReportParameters"] = dtpFromDate.Date + "~~^^^^~~" + dtpToDate.Date + "~~^^^^~~" + ddlStaffNames.SelectedValue;
        Session["SerchCriteria"] = " From Date : " + dtpFromDate.Date + "&nbsp;&nbsp;&nbsp; To Date : " + dtpToDate.Date + "&nbsp;&nbsp;&nbsp; Staff Name : " + ddlStaffNames.SelectedItem.Text;
        Response.Redirect("~/Manager/DisplayReport.aspx");
    }

    private void FillLists()
    {
        ddlStaffNames.Items.Clear();

        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetStaffNames");
        if (ds.Tables.Count > 0)
        {
            ddlStaffNames.DataSource = ds.Tables[0];
            ddlStaffNames.DataTextField = "Name";
            ddlStaffNames.DataValueField = "Id";
            ddlStaffNames.DataBind();
            ddlStaffNames.Items.Insert(0, new ListItem("All", "0"));
        }
        if (ds != null) { ds.Dispose(); ds = null; }
    }
}
