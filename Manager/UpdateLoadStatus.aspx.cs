using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class UpdateLoadStatus : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Update Load Status";
        if (!Page.IsPostBack)
        {
            ViewState["PreviousPage"] =Request.UrlReferrer;//Saves the Previous page url in ViewState
            if (Request.QueryString.Count > 0)
            {
                string strRes = DBClass.executeScalar("Select Count(bint_LoadId) from eTn_Load where bint_LoadId=" + Request.QueryString[0].Trim());
               // Session["BackPage4"] = "~/Manager/UpdateLoadStatus.aspx?" + Request.QueryString[0].Trim();
                if (strRes != null)
                {
                    if (strRes.Trim().Length > 0 && Convert.ToInt64(strRes) > 0)
                    {
                        GridBar1.HeaderText = "Update Load Status ( " + Request.QueryString[0].Trim() + " )";
                        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadTypeAndContainerNumber", new object[] { Convert.ToInt64(Request.QueryString[0].Trim()) });
                        if (ds.Tables.Count != null && ds.Tables[0].Rows.Count>0)
                        {
                            GridBar1.HeaderRightText = (ds.Tables[0].Rows[0].ItemArray[0].ToString().Trim() + " : " + ds.Tables[0].Rows[0].ItemArray[1] + ((ds.Tables[0].Rows[0].ItemArray[2] != null && !string.IsNullOrEmpty(ds.Tables[0].Rows[0].ItemArray[2].ToString().Trim())) ? " - " : string.Empty) + ds.Tables[0].Rows[0].ItemArray[2].ToString().Trim());
                            GridBar1.HeaderRightTextVisible = true;
                            ViewState["SendMails"] = bool.Parse(ds.Tables[0].Rows[0][4].ToString());
                        }
                        //GridBar1.LinksList = "Upload Documents";
                        //GridBar1.PagesList = "UpLoadDocuments.aspx?" + Request.QueryString[0].Trim();
                        ViewState["ID"] = Request.QueryString[0].Trim();
                        FillDetails(Convert.ToInt64(Request.QueryString[0].Trim()));
                    }
                }
            }

        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (dgrLoad.Items.Count > 0)
        {
            for (int i = 0; i < dgrLoad.Items.Count; i++)
            {
                if ((i % 2) == 0)
                {
                    dgrLoad.Items[i].BackColor = System.Drawing.Color.Red;
                }
                else
                {
                    dgrLoad.Items[i].BackColor = System.Drawing.Color.Blue;
                }
            }
        }
        this.Master.FindControl("Top1").FindControl("pnlDispatcher").Visible = false;
        this.Master.FindControl("Top1").FindControl("pnlManager").Visible = false;
        this.Master.FindControl("Top1").FindControl("pnlStaff").Visible = false;
    }

    private void FillDetails(long loadId)
    {
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "sp_GetAllStatusInfoByLoadId", new object[] { loadId });
        if (ds.Tables.Count > 0)
        {
            #region Dummy Row Addition
            DataRow dr = ds.Tables[0].NewRow();
            for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
            {
                if (ds.Tables[0].Columns[i].DataType == typeof(string))
                {
                    dr[i] = "test";
                }
                else if (ds.Tables[0].Columns[i].DataType == typeof(System.Int64))
                {
                    dr[i] = 0;
                }
                else if (ds.Tables[0].Columns[i].DataType == typeof(DateTime))
                {
                    dr[i] = DateTime.Now;
                }
            }
            ds.Tables[0].Rows.InsertAt(dr, 0);
            ds.Tables[0].AcceptChanges();
            dr = null;
            #endregion
            if (ds.Tables[0].Rows.Count > 0)
            {
                dgrLoad.DataSource = ds.Tables[0].Copy();
                dgrLoad.DataBind();
            }
        }
        if (ds != null) { ds.Dispose(); ds = null; }
        #region Formatting Grid
        if (dgrLoad.Items.Count > 0)
        {
            for (int i = 0; i < dgrLoad.Items.Count; i++)
            {
                foreach (Control ctl in dgrLoad.Items[i].Cells[0].Controls)
                {
                
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.Label")
                    {
                        ctl.Visible = (i == 0) ? false : true;
                    }
                   
                }

                foreach (Control ctl in dgrLoad.Items[i].Cells[1].Controls)
                {
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.DropDownList")
                    {
                        ctl.Visible = (i == 0) ? true : false;
                    }
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.Label")
                    {
                        ctl.Visible = (i == 0) ? false : true;
                    }
                }
                foreach (Control ctl in dgrLoad.Items[i].Cells[2].Controls)
                {
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.TextBox")
                    {
                        ctl.Visible = (i == 0) ? true : false;
                    }
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.Label")
                    {
                        ctl.Visible = (i == 0) ? false : true;
                    }
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.RequiredFieldValidator")
                    {
                        ctl.Visible = (i == 0) ? true : false;
                    }
                }
                foreach (Control ctl in dgrLoad.Items[i].Cells[3].Controls)
                {
                    if (ctl.GetType().ToString() == "ASP.usercontrols_datepicker_ascx")
                    {
                        ctl.Visible = (i == 0) ? true : false;
                    }
                    if (ctl.GetType().ToString() == "ASP.usercontrols_timepicker_ascx")
                    {
                        ctl.Visible = (i == 0) ? true : false;
                    }
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.Label")
                    {
                        ctl.Visible = (i == 0) ? false : true;
                    }
                }

                foreach (Control ctl in dgrLoad.Items[i].Cells[4].Controls)
                {
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.TextBox")
                    {
                        ctl.Visible = (i == 0) ? true : false;
                    }
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.Label")
                    {
                        ctl.Visible = (i == 0) ? false : true;
                    }
                }
                foreach (Control ctl in dgrLoad.Items[i].Cells[5].Controls)
                {
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.Button")
                    {
                        ctl.Visible = (i == 0) ? true : false;
                        //Button btn = (Button)ctl;
                        //btn.CausesValidation = (i == 0) ? true : false;
                        //btn = null;
                    }
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.ImageButton")
                    {
                        ctl.Visible = (i == 0) ? false : true;
                        //ImageButton imgbtn = (ImageButton)ctl;
                        //imgbtn.CausesValidation = (i == 0) ? false : false;
                        //imgbtn = null;
                    }
                }
            }
        }
        #endregion
        #region Format Items of LoadStatus.
        string strQuery = "SELECT [bint_LoadStatusId] FROM [eTn_TrackLoad] where [bint_LoadId]=" + loadId;
        DataTable dt = DBClass.returnDataTable(strQuery);
        if (dt.Rows.Count > 0)
        {
            DropDownList dname = (DropDownList)dgrLoad.Items[0].Cells[0].FindControl("dropstatus");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dname.Items.Contains(dname.Items.FindByValue(Convert.ToString(dt.Rows[i][0]))))
                {
                    if (Convert.ToInt64(dt.Rows[i][0]) == 5)
                        continue;
                    dname.Items.Remove(dname.Items.FindByValue(Convert.ToString(dt.Rows[i][0])));
                }
            }
        }
        #endregion
    }

    protected void dropStatus_SelectedIndexChanged(object source, EventArgs e)
    {
        DropDownList ddl = (DropDownList)source;
        if ((ddl.SelectedValue == "8" || ddl.SelectedValue == "7" || ddl.SelectedValue == "12" || ddl.SelectedValue == "14") && ViewState["SendMails"] != null && bool.Parse(ViewState["SendMails"].ToString()))
        {
            ChkSendMail.Visible = true;
            ChkSendMail.Checked = true;
        }
        else
        {
            ChkSendMail.Visible = false ;
            ChkSendMail.Checked = false;
        }
    }

    protected void dgrLoad_ItemCommand(object source, DataGridCommandEventArgs e)
    {        
        if (string.Compare(e.CommandName, "ADD", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            if (!Page.IsValid)
                return;
            if (Page.IsPostBack)
            {
                DatePicker Dpicker = (DatePicker)e.Item.FindControl("txtDate");
                TimePicker Tpicker = (TimePicker)e.Item.FindControl("txtTime");
                if (!Dpicker.IsValidDate)
                    return;

                Label lbl1 = (Label)dgrLoad.Items[1].FindControl("lblID");
                if (dgrLoad.Items.Count > 1)
                {
                    if (Convert.ToDateTime(DBClass.executeScalar("Select top 1 date_CreateDate from eTn_TrackLoad where bint_LoadId = " +
                        "(select bint_LoadId from eTn_TrackLoad where bint_TrackLoadId = " + lbl1.Text.Trim() + ") order by date_CreateDate desc ")) >= Convert.ToDateTime(Dpicker.Date + " " + Tpicker.Time.Trim()))
                    {
                        Response.Write("<script>alert('Date entered must be greater than the date of previous status dates.')</script>");
                        return;
                    }
                }

                DropDownList dname = (DropDownList)e.Item.FindControl("dropstatus");
                TextBox Cloc = (TextBox)e.Item.FindControl("txtLoc");
                TextBox Cnotes = (TextBox)e.Item.FindControl("txtComments");

                if (dname.SelectedItem.Text == "Driver On Waiting")
                {
                    if (Convert.ToInt32(DBClass.executeScalar("Select count(bint_LoadId) from eTn_TrackLoad where bint_LoadId = " + Convert.ToInt64(ViewState["ID"]) + " and bint_LoadStatusId=(select bint_LoadStatusId from eTn_LoadStatus where Lower(nvar_LoadStatusDesc)='reached destination')")) == 0)
                    {
                        dname.SelectedIndex = dname.Items.IndexOf(dname.Items.FindByText("Reached Destination"));
                        Response.Write("<script>alert('Driver On Waiting status can only be updated when Reached @ Destination status is entered.')</script>");
                        return;
                    }
                }

                if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_Insert_Update_TrackLoad", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(dname.SelectedItem.Value),
                 Cloc.Text.Trim(),CommonFunctions.CheckDateTimeNull((Dpicker.Date !=null)?Dpicker.Date+" "+Tpicker.Time.Trim():null),Cnotes.Text.Trim(),Convert.ToInt64(Session["UserLoginId"]),"I"}) > 0)
                {
                    ChangeLoadStatus cls = new ChangeLoadStatus();
                    cls.UpdateLoadStatusToPostman(Convert.ToInt64(ViewState["ID"]), dname.SelectedItem.Text, (DateTime)CommonFunctions.CheckDateTimeNull((Dpicker.Date != null) ? Dpicker.Date + " " + Tpicker.Time.Trim() : null));
                    DataSet ds = null;
                    if (string.Compare(dname.SelectedItem.Text, "Pickedup", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                    {
                        if (Convert.ToInt64(ViewState["ID"]) <= (Convert.ToInt64(ConfigurationSettings.AppSettings["LastLoadId"])))
                        {
                            string[] Values = Convert.ToString(DBClass.executeScalar("select Case when Len([eTn_Load].[nvar_Container])>0 and Len([eTn_Load].[nvar_Container1])>0 then [eTn_Load].[nvar_Container] when Len([eTn_Load].[nvar_Container])>0 and Len([eTn_Load].[nvar_Container1])=0 then [eTn_Load].[nvar_Container] when Len([eTn_Load].[nvar_Container1])>0 then [eTn_Load].[nvar_Container1] else '' end+'^'+Case when Len([eTn_Load].[nvar_Chasis])>0 and Len([eTn_Load].[nvar_Chasis1])>0 then [eTn_Load].[nvar_Chasis] when Len([eTn_Load].[nvar_Chasis])>0 and Len([eTn_Load].[nvar_Chasis1])=0 then [eTn_Load].[nvar_Chasis] when Len([eTn_Load].[nvar_Chasis1])>0 then [eTn_Load].[nvar_Chasis1] else '' end+'^'+(isnull((select top 1 case when Len(nvar_Email)>0 then nvar_Email else '' end from eTn_ShippingContacts where bint_ShippingId=[eTn_Load].bint_ShippingId),(isnull((select top 1 case when Len(nvar_Email)>0 then nvar_Email else '' end from eTn_Shipping where bint_ShippingId=[eTn_Load].bint_ShippingId),'')))) from [eTn_Load] where [eTn_Load].[bint_LoadId]=" + Convert.ToInt64(ViewState["ID"]))).Trim().Split('^');
                            if (Values != null)
                            {
                                //CommonFunctions.SendEmail(GetFromXML.AdminEmail, (Values.Length == 3 ? Values[2] : ""), GetFromXML.AdminEmail, "", "Pre Authorization for empty UNIT # " + Values[0]+" ( OffHire ) ", GetPickedUpBodyDetails(Values[0], Values[1]), new string[] { "<font color=red>This email was not sent to the shipping line,because Contact email address is not available for this shipping line.</font><br/> " },null);
                                CommonFunctions.SendEmail(GetFromXML.AdminEmail, (Values.Length == 3 ? Values[2] : ""), GetFromXML.CCPickup, "", "Pre Authorization for empty UNIT # " + Values[0] + " ( OffHire ) ", GetPickedUpBodyDetails(Values[0], Values[1]), new string[] { "<font color=red>This email was not sent to the shipping line,because Contact email address is not available for this shipping line.</font><br/> " }, null);
                            }
                            Values = null;
                        }
                    }
                    else if (string.Compare(dname.SelectedItem.Text, "Driver On Waiting", true, System.Globalization.CultureInfo.CurrentCulture) == 0 && ChkSendMail.Checked)
                    {
                        ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriveronWaitingDetails", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(dname.SelectedItem.Value) });
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            //CommonFunctions.SendEmail(GetFromXML.FromDriveronWaiting, Convert.ToString(ds.Tables[1].Rows[0][0]),GetFromXML.AdminEmail + ";" + GetFromXML.CcEmail, "", GetSubject(ds.Tables[0].Rows[0][0].ToString(), ds.Tables[0].Rows[0][1].ToString()), GetBodyDetails(ds.Tables[0].Rows[0][0].ToString(), ds.Tables[0].Rows[0][1].ToString(), ds.Tables[0].Rows[0][2].ToString()), null,null);
                            CommonFunctions.SendEmail(GetFromXML.FromDriveronWaiting, Convert.ToString(ds.Tables[1].Rows[0][0]), GetFromXML.CCDriveronWaiting, "", GetSubject(ds.Tables[0].Rows[0][0].ToString(), ds.Tables[0].Rows[0][1].ToString()), GetBodyDetails(ds.Tables[0].Rows[0][0].ToString(), ds.Tables[0].Rows[0][1].ToString(), ds.Tables[0].Rows[0][2].ToString()), null, null);
                        }
                    }
                    else if (string.Compare(dname.SelectedItem.Text, "Delivered", true, System.Globalization.CultureInfo.CurrentCulture) == 0 && ChkSendMail.Checked)
                    {
                        string[] filePaths;
                        DataTable dt = DBClass.returnDataTable("select [nvar_DocumentName],isnull([bint_FileNo],0),[date_ReceivedDate] from [eTn_UploadDocuments] where [bint_LoadId]=" + Convert.ToInt64(ViewState["ID"]) + " and [bint_DocumentId]=" + 4 + " and [nvar_DocumentName] is not null");
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            filePaths = new string[dt.Rows.Count];
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                filePaths[i] = Server.MapPath("../Documents" + "\\" + Convert.ToString(ViewState["ID"]).Trim() + "\\" + Convert.ToString(dt.Rows[i][0]));
                            }
                        }
                        else
                        {
                            filePaths = new string[0];
                        }
                        ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriveronWaitingDetails", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(dname.SelectedItem.Value) });
                        DataSet ds1 = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadEmailsByLoadId", new object[] { Convert.ToInt64(ViewState["ID"]) });
                        if (ds.Tables.Count == 5 && ds.Tables[4].Rows.Count > 0 && Convert.ToString(ds.Tables[4].Rows[0][0]).Trim() != "0.00" && Convert.ToString(ds.Tables[4].Rows[0][0]).Trim() != "0")
                        {
                            string toAddress = string.Empty;
                            if (ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
                            {
                                toAddress = !string.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0][0])) ? Convert.ToString(ds1.Tables[0].Rows[0][0]) : !string.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0][1])) ? Convert.ToString(ds1.Tables[0].Rows[0][1]) : Convert.ToString(ds.Tables[1].Rows[0][0]);
                            }
                            else
                            {
                                toAddress = Convert.ToString(ds.Tables[1].Rows[0][0]);
                            }
                            try
                            {
                                //CommonFunctions.SendEmail(GetFromXML.FromAccessrial, toAddress, GetFromXML.AdminEmail, "", GetAccerialSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetAccerialChargesBody(ds),null ,filePaths);
                                CommonFunctions.SendEmail(GetFromXML.FromAccessrial, toAddress, "", "", GetAccerialSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetAccerialChargesBody(ds), null, filePaths);
                            }
                            catch (Exception ex)
                            { }
                           
                        }

                         string toDeliverAddress = string.Empty;
                            if (ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
                            {
                                toDeliverAddress = !string.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0][4])) ? Convert.ToString(ds1.Tables[0].Rows[0][4]) : Convert.ToString(ds1.Tables[0].Rows[0][5]);
                            }
                            if (!string.IsNullOrEmpty(toDeliverAddress) && ChkSendMail.Checked)
                            {
                                try
                                {
                                    //CommonFunctions.SendEmail(GetFromXML.FromAccessrial, toDeliverAddress, GetFromXML.AdminEmail, "", GetDeliveredSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetBodyDeliveredDetails(ds.Tables[0].Rows[0][0].ToString(), ds.Tables[0].Rows[0][1].ToString(), ds.Tables[0].Rows[0][2].ToString(), ds.Tables[0].Rows[0][3].ToString()), null, null);
                                    CommonFunctions.SendEmail(GetFromXML.FromAccessrial, toDeliverAddress, "", "", GetDeliveredSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetBodyDeliveredDetails(ds.Tables[0].Rows[0][0].ToString(), ds.Tables[0].Rows[0][1].ToString(), ds.Tables[0].Rows[0][2].ToString(), ds.Tables[0].Rows[0][3].ToString()), null, null);
                                }
                                catch (Exception ex)
                                { }
                            }
                        //ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriveronWaitingDetails", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(dname.SelectedItem.Value) });
                        //if (ds.Tables.Count == 5 && ds.Tables[4].Rows.Count > 0 && Convert.ToString(ds.Tables[4].Rows[0][0]).Trim() != "0.00" && Convert.ToString(ds.Tables[4].Rows[0][0]).Trim() != "0")
                        //{
                        //    CommonFunctions.SendEmail(GetFromXML.FromAccessrial, Convert.ToString(ds.Tables[1].Rows[0][0]), GetFromXML.AdminEmail, "", GetAccerialSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetAccerialChargesBody(ds), null);
                        //}
                    }

                    //SLine 2016 Enhancements
                    // Adding "Send Invoice Via Email" feature when the Load is Closed
                    #region "Closed"

                    else if (string.Compare(dname.SelectedItem.Text, "Closed", true, System.Globalization.CultureInfo.CurrentCulture) == 0 && ChkSendMail.Checked)
                    {
                        string[] filePaths;
                        DataTable dt = DBClass.returnDataTable("select [nvar_DocumentName],isnull([bint_FileNo],0),[date_ReceivedDate] from [eTn_UploadDocuments] where [bint_LoadId]=" + Convert.ToInt64(ViewState["ID"]) + " and [bint_DocumentId]=" + 7 + " and [nvar_DocumentName] is not null");
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            filePaths = new string[dt.Rows.Count];
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                filePaths[i] = Server.MapPath("../Documents" + "\\" + Convert.ToString(ViewState["ID"]).Trim() + "\\" + Convert.ToString(dt.Rows[i][0]));
                            }
                        }
                        else
                        {
                            filePaths = new string[0];
                        }
                        ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriveronWaitingDetails", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(dname.SelectedItem.Value) });
                        DataSet ds1 = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadEmailsByLoadId", new object[] { Convert.ToInt64(ViewState["ID"]) });
                        if (filePaths.Length > 0)
                        {
                            string podAddress = string.Empty;
                            if (ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
                            {
                                podAddress = !string.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0][2])) ? Convert.ToString(ds1.Tables[0].Rows[0][2]) : Convert.ToString(ds1.Tables[0].Rows[0][3]);
                            }
                            if (!string.IsNullOrEmpty(podAddress))
                            {
                                try
                                {
                                    //CommonFunctions.SendEmail(GetFromXML.FromAccessrial, podAddress, GetFromXML.AdminEmail, "", GetPODSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetBodyPODDetails(), null, filePaths);
                                                                        
                                    CommonFunctions.SendEmail(GetFromXML.FromAccessrial, podAddress, "", "", GetPODSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetBodyPODDetails(), null, filePaths);                                   
                                }
                                catch (Exception ex)
                                { }
                            }
                        }

                        //SLine 2016 Enhancements for Automatic Invoice via Email ---- Implementation block
                        //Checking if the Customer has opted for Send Invoice via Email, if yes all the four docs will be made into a single pdf file
                        #region Revoikng the Changes before Phase 1 ---- Hosting And COMMENTED BELOW CODE BLOCK 
                        /* if (!Convert.IsDBNull(ds1.Tables[0].Rows[0]["nvar_BillingEmail"]))
                        {
                            //Getting the Document names for that LoadId
                            DataSet ds_GetDocumentNames = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDocumentNames", new object[] { Convert.ToInt64(ViewState["ID"]) });

                            //Merging or Making a Single Pdf for all the 3-4 Documents and getting the name of the Invoice for that Customer/LoadId
                            string[] str_InvoicePath = new string[1];
                            str_InvoicePath[0] = Server.MapPath("../Documents" + "\\" + Convert.ToInt64(ViewState["ID"])) + "\\" + CommonFunctions.MergeAllPdf(ds_GetDocumentNames, Server.MapPath("../Documents" + "\\" + Convert.ToInt64(ViewState["ID"])), Convert.ToInt64(ViewState["ID"]));
                            //Sending the Mail
                            CommonFunctions.SendEmail(GetFromXML.FromAccessrial, Convert.ToString(ds1.Tables[0].Rows[0]["nvar_BillingEmail"]), "", "", GetInvoiceSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetInvoiceBodyDetails(), null, str_InvoicePath);
                        } */
                        #endregion
                        //SLine 2016 Enhancements for Automatic Invoice via Email ---- Implementation block
                    }
                    #endregion
                    //SLine 2016 Enhancements

                    else if (string.Compare(dname.SelectedItem.Text, "Reached Destination", true, System.Globalization.CultureInfo.CurrentCulture) == 0 && ChkSendMail.Checked)
                    {                      
                        ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriveronWaitingDetails", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(dname.SelectedItem.Value) });
                        
                                try
                                {
                                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                    {
                                        //CommonFunctions.SendEmail(GetFromXML.FromDriveronWaiting, Convert.ToString(ds.Tables[1].Rows[0][0]), GetFromXML.AdminEmail, "", GetReachedDestinationSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetBodyReachedDestinationDetails(ds.Tables[0].Rows[0][0].ToString(), ds.Tables[0].Rows[0][1].ToString(), ds.Tables[0].Rows[0][2].ToString()), null, null);
                                        CommonFunctions.SendEmail(GetFromXML.FromDriveronWaiting, Convert.ToString(ds.Tables[1].Rows[0][0]), "", "", GetReachedDestinationSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetBodyReachedDestinationDetails(ds.Tables[0].Rows[0][0].ToString(), ds.Tables[0].Rows[0][1].ToString(), ds.Tables[0].Rows[0][2].ToString()), null, null);
                                    }
                                }
                                catch (Exception ex)
                                { }
                    }
                    if (ds != null) { ds.Dispose(); ds = null; }
                }
                //CheckBackPage();
                //Response.Redirect("~/Manager/DashBoard.aspx");
               //Response.Write("<Script>this.close();</script>");
                RedirectToBackPage();
            }
        }
        if (string.Compare(e.CommandName, "Delete", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            if (Page.IsPostBack)
            {
                Label lbl = (Label)e.Item.FindControl("lblID");
                if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_Delete_TrackLoad", new object[] { lbl.Text.Trim(), Convert.ToInt64(Session["UserLoginId"]) }) > 0)
                {
                    FillDetails(Convert.ToInt64(ViewState["ID"]));
                }
            }
        }
    }

    #region Old Code
    //private string GetAccerialChargesBody(DataSet ds)
    //{
    //    System.Text.StringBuilder strBody = new System.Text.StringBuilder();
    //    strBody.Append("<%@ Page AutoEventWireup=true CodeFile='AccessorialCharges.aspx.cs' Inherits='AccessorialCharges' Language='C#' MasterPageFile='~/Manager/MasterPage.master' %>");
    //    strBody.Append("<asp:Content ID='Content1' ContentPlaceHolderID='ContentPlaceHolder1' Runat='Server'>");
    //    strBody.Append("<table width='100%' border='0' cellpadding='3' cellspacing='0' id='ContentTbl'><tr valign='top'><td><table width='100%' border='0' cellspacing='0' cellpadding='0'>");
    //    strBody.Append("<tr><td align='center' class='head1'>ACCEPTANCE FORM </td></tr></table>");
    //    strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img alt='' height='10' src='../Images/pix.gif' width='1'/></td></tr></table>");
    //    strBody.Append("<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td style='background-color: #CCCCCC; left: 10px;'><img src='../Images/pix.gif' width='1' height='1' /></td></tr></table>");
    //    strBody.Append("<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td><img src='../Images/pix.gif' alt=' width='1' height='10' /></td></tr></table>");
    //    strBody.Append("<table border='0' cellpadding='0' cellspacing='0' style='padding-right: 5px; padding-left: 15px;height: 20px; padding-top: 1px; padding-bottom: 1px;' width='100%'><tr><td width='50%' valign='top'>");
    //    strBody.Append("<table border='0' cellpadding='0' cellspacing='0' style='font-family: Arial, sans-serif;color: #4D4B4C; font-size: 12px;' width='100%'><tr><td width='30%' valign='top'><p><strong>Bill To:</strong><br />");
    //    strBody.Append("<asp:Label ID='lblBillTo' runat='server' Text=" + Convert.ToString(ds.Tables[2].Rows[0][0]) + "<br/>" + Convert.ToString(ds.Tables[2].Rows[0][1]) + "  " + Convert.ToString(ds.Tables[2].Rows[0][2]) + "<br/>" + Convert.ToString(ds.Tables[2].Rows[0][3]) + ", " + Convert.ToString(ds.Tables[2].Rows[0][4]) + " " + Convert.ToString(ds.Tables[2].Rows[0][5]) + "");
    //    strBody.Append("<br />Billing Ref.# :" + Convert.ToString(ds.Tables[2].Rows[0][6]) + "</p></td>");
    //    strBody.Append("<td valign='top'><strong>Load Number&nbsp;:&nbsp;" + Convert.ToString(ViewState["ID"]) + "</strong><br />");
    //    strBody.Append("Type&nbsp;: " + Convert.ToString(ds.Tables[2].Rows[0][7]) + "<br />");
    //    strBody.Append("Commodity&nbsp;: " + Convert.ToString(ds.Tables[2].Rows[0][8]) + "<br />");
    //    strBody.Append("Booking No&nbsp;: " + Convert.ToString(ds.Tables[2].Rows[0][9]) + "<br />");
    //    strBody.Append("Container ID&nbsp;:&nbsp;" + Convert.ToString(ds.Tables[2].Rows[0][10]) + "<br />");
    //    strBody.Append("Empty/ Chassie#&nbsp;:&nbsp;" + Convert.ToString(ds.Tables[2].Rows[0][12]) + "<br />");
    //    strBody.Append("Remarks&nbsp;:&nbsp;" + Convert.ToString(ds.Tables[2].Rows[0][14]) + "<br />");
    //    strBody.Append("Description&nbsp;:&nbsp;" + Convert.ToString(ds.Tables[2].Rows[0][15]) + "</td></tr></table>");
    //    strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0' style='font-family: Arial, sans-serif;color: #4D4B4C; font-size: 12px;'><tr><td><img src='../Images/pix.gif' alt='' width='1' height='10' /></td></tr><tr id='row'><td align=left><strong>" + ConfigurationSettings.AppSettings["CompanyName"] + "</strong></td>");
    //    strBody.Append("<td align='right'><strong>" + ConfigurationSettings.AppSettings["CompanyPhone"] + "&nbsp;&nbsp;&nbsp;" + ConfigurationSettings.AppSettings["CompanyFax"] + "</strong></td></tr></table>");
    //    strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0' id='TABLE1'><tr><td bgcolor='#CCCCCC'><img src='../Images/pix.gif' alt='' width='1' height='1' /></td></tr></table>");
    //    strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img src='../Images/pix.gif' alt='' width='1' height='10'/></td></tr></table>");
    //    strBody.Append("<table border='0' cellpadding='0' cellspacing='0' style='font-family: Arial, sans-serif;color: #4D4B4C; font-size: 12px;' width='100%'>");
    //    strBody.Append("<tr><td valign='top'>Dear Customer,<br />Our company request authorization for the additional charges. please sign below and fax (510 625 0635/510 444 2899) it back to us asap.</td></tr></table>");
    //    strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img src='../Images/pix.gif' alt=' width='1' height='10' /></td></tr></table>");
    //    strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td bgcolor='#CCCCCC'><img src='../Images/pix.gif' alt='' width='1' height='1' /></td></tr></table>");
    //    strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img src='../Images/pix.gif' alt='' width='1' height='10'/></td></tr></table>");
    //    strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td>" + DesginPaybleDetails(ds.Tables[3]) + "</td></tr></table>");
    //    // need to write code for charges
    //    strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img src='../Images/pix.gif' alt=' width='1' height=10'/></td></tr></table>");
    //    strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td bgcolor='#CCCCCC'><img src='../Images/pix.gif' alt='' width='1' height='1'/></td></tr></table>");
    //    strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img src='../Images/pix.gif' alt='' width='1' height='25' /></td></tr></table>");
    //    strBody.Append("<table border='0' cellpadding='0' cellspacing='0' style='font-family: Arial, sans-serif;font-size: 12px;' width='100%'><tr><td width='50%' valign='top'><p>Authorized by (Signature) : ________________________ </p></td><td valign='top'>Date : ________________________ </td></tr><tr><td valign='top'></td><td valign='top'></td></tr><tr><td><img alt='' height='25' src='../Images/pix.gif' width='1' /></td></tr>");
    //    strBody.Append("<tr><td valign='top'>Name : ________________________ </td><td valign='top'></td></tr></table></td></tr></table></td></tr></table></asp:Content>");
    //    return strBody.ToString();
    //}

    //private string GetBodyDetails(string Container,string RefNo,string DriverWaitingtime)
    //{
    //    System.Text.StringBuilder strBody = new System.Text.StringBuilder();
    //    strBody.Append("Hello Sir/Madam <br/><br/><br/><br/><br/><p></p>");
    //    strBody.Append("Regarding ");
    //    if (Container.Length > 0)
    //        strBody.Append("&nbsp;Container# " + Container);
    //    if(RefNo.Length>0 && Container.Length >0)
    //        strBody.Append("&nbsp;and&nbsp; Ref #&nbsp; " + RefNo);
    //    strBody.Append("<br/>Driver is on waiting time, <br/><br/>");
    //    strBody.Append("Driver Time in: " + DriverWaitingtime);
    //    strBody.Append("<br/>Driver   time out: Driver is still at warehouse.<br/> We will advise you when driver finishes unloading");
    //    strBody.Append("<p></p>Regards<br/>" + ConfigurationSettings.AppSettings["CompanyName"] + "<br/>Dispatch Dept.");

    //    return strBody.ToString();
    //}
    //public string DesginPaybleDetails(DataTable dtPayables)
    //{
    //    System.Text.StringBuilder strCode = new System.Text.StringBuilder();
    //    strCode.Append("");
    //    if (dtPayables.Rows.Count > 0)
    //    {
    //        bool blVal = false;
    //        for (int i = 0, j = 0; i < dtPayables.Columns.Count; i++)
    //        {
    //            #region Validations
    //            blVal = false;
    //            if (Convert.IsDBNull(dtPayables.Rows[0][i]))
    //                dtPayables.Rows[0][i] = 0;
    //            if (Convert.ToString(dtPayables.Rows[0][i]).Length == 0)
    //                dtPayables.Rows[0][i] = 0;
    //            try
    //            {
    //                if (Convert.ToDouble(dtPayables.Rows[0][i]) > 0)
    //                {
    //                    blVal = true;
    //                }
    //            }
    //            catch
    //            {
    //                if (Convert.ToString(dtPayables.Rows[0][i]).Trim().Length > 0)
    //                {
    //                    blVal = true;
    //                }
    //            }
    //            #endregion
    //            if (blVal)
    //            {
    //                strCode.Append("<tr id=" + (((j % 2) == 0) ? "row" : "altrow") + " >");
    //                if (i < (dtPayables.Columns.Count - 1))
    //                    strCode.Append("<td align=right width=30%>" + dtPayables.Columns[i].ColumnName + "</td>");
    //                else
    //                    strCode.Append("<td align=right width=30%><b>" + dtPayables.Columns[i].ColumnName + "</b></td>");
    //                strCode.Append("<td align=left>" + Convert.ToString(dtPayables.Rows[0][i]).Trim() + "</td>");
    //                strCode.Append("</tr>");
    //                j = j + 1;
    //            }
    //        }
    //        if (strCode.ToString().Length > 0)
    //        {
    //            strCode.Insert(0, "<table width=100% border=0 cellspacing=0 cellpadding=0 id=tblform> <tr><th colspan=2>Load Charges($)</th></tr>");
    //            strCode.Append("</table>");
    //        }
    //        else
    //        {
    //            strCode.Append("<table width=100% border=0 cellspacing=0 cellpadding=0 id=tblform> <tr><th colspan=2>Load Charges($)</th></tr>");
    //            strCode.Append("<tr id=row>");
    //            strCode.Append("<td align=center width=30%><b>No Charges Available for Load : " + Convert.ToString(ViewState["ID"]) + " </b></td>");
    //            strCode.Append("</tr>");
    //            strCode.Append("</table>");
    //        }
    //    }
    //    else
    //    {
    //        strCode.Append("<table width=100% border=0 cellspacing=0 cellpadding=0 id=tblform> <tr><th colspan=2>Load Charges($)</th></tr>");
    //        strCode.Append("<tr id=row>");
    //        strCode.Append("<td align=center width=30%><b>No Charges Available for Load : " + Convert.ToString(ViewState["ID"]) + " </b></td>");
    //        strCode.Append("</tr>");
    //        strCode.Append("</table>");
    //    }
    //    return strCode.ToString();
    //}
    
    #endregion
    private void CheckBackPage()
    {
        if (Session["BackPage4"] != null)
        {
            if (Convert.ToString(Session["BackPage4"]).Trim().Length > 0)
            {
                string strTemp = Convert.ToString(Session["BackPage4"]).Trim();
                Session["BackPage4"] = null;
                Response.Redirect(strTemp);
            }
        }
    }

    private void RedirectToBackPage()
    {
        if (ViewState["PreviousPage"] != null)	//Check if the ViewState contains Previous page URL
        {
            Response.Redirect(ViewState["PreviousPage"].ToString());//Redirect to Previous page by retrieving the PreviousPage Url from ViewState.
        }
        else
        {
            Response.Redirect("DashBoard.aspx");
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        // CheckBackPage();
        //Response.Redirect("~/Manager/DashBoard.aspx");
        RedirectToBackPage();
        
    }

    protected void dgrLoad_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DropDownList dllStatus = (DropDownList)e.Item.FindControl("dropStatus");
            TextBox txtBox = (TextBox)e.Item.FindControl("txtLoc");
            Button btnAdd = (Button)e.Item.FindControl("btnAdd");
            //if (dllStatus != null && txtBox != null && btnAdd != null)
            //{
            //    btnAdd.Attributes.Add("onClick", "Javascript:return ValidateControls('" + dllStatus.ClientID + "','" + txtBox.ClientID + "');");
            //}
            dllStatus = null; btnAdd = null; txtBox = null;
        }
    }
    private string GetAccerialChargesBody(DataSet ds)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();
        strBody.Append("<%@ Page AutoEventWireup=true CodeFile='AccessorialCharges.aspx.cs' Inherits='AccessorialCharges' Language='C#' MasterPageFile='~/Manager/MasterPage.master' %>");
        strBody.Append("<asp:Content ID='Content1' ContentPlaceHolderID='ContentPlaceHolder1' Runat='Server'>");
        strBody.Append("<table width='100%' border='0' cellpadding='3' cellspacing='0' id='ContentTbl'><tr valign='top'><td><table width='100%' border='0' cellspacing='0' cellpadding='0'>");
        strBody.Append("<tr><td align='center' class='head1'>ACCEPTANCE FORM <br/> Accessorial Charges </td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img alt='' height='10' src='../Images/pix.gif' width='1'/></td></tr></table>");
        strBody.Append("<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td style='background-color: #CCCCCC; left: 10px;'><img src='../Images/pix.gif' width='1' height='1' /></td></tr></table>");
        strBody.Append("<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td><img src='../Images/pix.gif' alt=' width='1' height='10' /></td></tr></table>");
        strBody.Append("<table border='0' cellpadding='0' cellspacing='0' style='padding-right: 5px; padding-left: 15px;height: 20px; padding-top: 1px; padding-bottom: 1px;' width='100%'><tr><td width='50%' valign='top'>");
        strBody.Append("<table border='0' cellpadding='0' cellspacing='0' style='font-family: Arial, sans-serif;color: #4D4B4C; font-size: 12px;' width='100%'><tr><td width='30%' valign='top'><p><strong>Bill To:</strong><br />");
        strBody.Append("<strong>" + Convert.ToString(ds.Tables[2].Rows[0][0]) + "</strong><br/>" + Convert.ToString(ds.Tables[2].Rows[0][1]) + "  " + Convert.ToString(ds.Tables[2].Rows[0][2]) + "<br/>" + Convert.ToString(ds.Tables[2].Rows[0][3]) + ", " + Convert.ToString(ds.Tables[2].Rows[0][4]) + " " + Convert.ToString(ds.Tables[2].Rows[0][5]) + Convert.ToString(ds.Tables[2].Rows[0][16]) + Convert.ToString(ds.Tables[2].Rows[0][17]));
        strBody.Append("<br /><br/><strong>Billing Ref.# </strong>:" + Convert.ToString(ds.Tables[2].Rows[0][6]) + "</p></td>");
        strBody.Append("<td valign='top'><strong>Load Number&nbsp;:&nbsp;" + Convert.ToString(ViewState["ID"]) + "</strong><br />");
        strBody.Append("Type&nbsp;: " + Convert.ToString(ds.Tables[2].Rows[0][7]) + "<br />");
        strBody.Append("Commodity&nbsp;: " + Convert.ToString(ds.Tables[2].Rows[0][8]) + "<br />");
        strBody.Append("Booking#&nbsp;: " + Convert.ToString(ds.Tables[2].Rows[0][9]) + "<br />");
        strBody.Append("Container#&nbsp;:&nbsp;" + Convert.ToString(ds.Tables[2].Rows[0][10]) + "<br />");
        strBody.Append("Chassis#&nbsp;:&nbsp;" + Convert.ToString(ds.Tables[2].Rows[0][12]) + "<br />");
        strBody.Append("Remarks&nbsp;:&nbsp;" + Convert.ToString(ds.Tables[2].Rows[0][14]) + "<br />");
        strBody.Append("Description&nbsp;:&nbsp;" + Convert.ToString(ds.Tables[2].Rows[0][15]) + "</td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0' style='font-family: Arial, sans-serif;color: #4D4B4C; font-size: 12px;'><tr><td><img src='../Images/pix.gif' alt='' width='1' height='10' /></td></tr><tr id='row'><td align=left><strong>" + GetFromXML.CompanyName + "<br/>" + GetFromXML.CompanyPhone+ "</strong></td>");
        //strBody.Append("<td align='right'><strong>" + ConfigurationSettings.AppSettings["CompanyPhone"] + "&nbsp;&nbsp;&nbsp;" + ConfigurationSettings.AppSettings["CompanyFax"] + "</strong></td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0' id='TABLE1'><tr><td bgcolor='#CCCCCC'><img src='../Images/pix.gif' alt='' width='1' height='1' /></td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img src='../Images/pix.gif' alt='' width='1' height='10'/></td></tr></table>");
        strBody.Append("<table border='0' cellpadding='0' cellspacing='0' style='font-family: Arial, sans-serif;color: #4D4B4C; font-size: 12px;' width='100%'>");
        strBody.Append("<tr><td valign='top'>Dear Customer,<br />Our company request authorization for the additional charges. please sign below and " + GetFromXML.CompanyFax + " with in 24 hrs after receiving this form.</td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img src='../Images/pix.gif' alt=' width='1' height='10' /></td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td bgcolor='#CCCCCC'><img src='../Images/pix.gif' alt='' width='1' height='1' /></td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img src='../Images/pix.gif' alt='' width='1' height='10'/></td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td>" + DesginPaybleDetails(ds.Tables[3]) + "</td></tr></table>");
        //need to write code for charges
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img src='../Images/pix.gif' alt=' width='1' height=10'/></td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td bgcolor='#CCCCCC'><img src='../Images/pix.gif' alt='' width='1' height='1'/></td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img src='../Images/pix.gif' alt='' width='1' height='25' /></td></tr></table>");
        strBody.Append("<table border='0' cellpadding='0' cellspacing='0' style='font-family: Arial, sans-serif;font-size: 12px;' width='100%'><tr><td width='50%' valign='top'><p>Authorized by (Signature) : ________________________ </p></td><td valign='top'>Date : ________________________ </td></tr><tr><td valign='top'></td><td valign='top'></td></tr><tr><td><img alt='' height='25' src='../Images/pix.gif' width='1' /></td></tr>");
        strBody.Append("<tr><td valign='top'>Name : ________________________ </td><td valign='top'></td></tr></table></td></tr></table></td></tr></table></asp:Content>");
        return strBody.ToString();
    }
    private string GetSubject(string Container, string RefNo)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();

        strBody.Append("Regarding ");
        if (Container.Length > 0)
            strBody.Append(" Container# " + Container);
        if (RefNo.Length > 0 && Container.Length > 0)
            strBody.Append(" and  Ref #  " + RefNo);
        strBody.Append(" Driver is on detention");

        return strBody.ToString();
    }

    private string GetDeliveredSubject(string LoadID, string Container, string RefNo)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();

        strBody.Append("Re ");
        if (Container.Length > 0)
            strBody.Append("unit # " + Container);
        if (RefNo.Length > 0)
            strBody.Append(" ,  Ref #  " + RefNo);
        strBody.Append(" Delivered");

        return strBody.ToString();
    }

    private string GetPODSubject(string LoadID, string Container, string RefNo)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();

        strBody.Append("Re ");
        if (Container.Length > 0)
            strBody.Append("unit # " + Container);
        if (RefNo.Length > 0)
            strBody.Append(" ,  Ref #  " + RefNo);
        strBody.Append(". Proof of delivery is attached.");

        return strBody.ToString();
    }

    private string GetReachedDestinationSubject(string LoadID, string Container, string RefNo)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();

        strBody.Append("Re ");
        if (Container.Length > 0)
            strBody.Append("unit # " + Container);
        if (RefNo.Length > 0)
            strBody.Append(" ,  Ref #  " + RefNo);
        strBody.Append(" Driver reached destination ");

        return strBody.ToString();
    }

    private string GetAccerialSubject(string LoadID, string Container, string RefNo)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();

        strBody.Append("Accessorial Charges for Load# " + LoadID);
        if (RefNo.Length > 0)
            strBody.Append(" ,  Ref #  " + RefNo);
        if (Container.Length > 0)
            strBody.Append(" , Container# " + Container);

        return strBody.ToString();
    }

    #region Sline 2016 Enhancements
    //private string GetInvoiceSubject(string LoadID, string Container, string RefNo)
    //{
    //    System.Text.StringBuilder strBody = new System.Text.StringBuilder();

    //    strBody.Append("Re ");
    //    if (Container.Length > 0)
    //        strBody.Append("unit # " + Container);
    //    if (RefNo.Length > 0)
    //        strBody.Append(" ,  Ref #  " + RefNo);
    //    strBody.Append(". Invoice_" + LoadID + " copy attached");

    //    return strBody.ToString();
    //}

    //private string GetInvoiceBodyDetails()
    //{
    //    System.Text.StringBuilder strBody = new System.Text.StringBuilder();
    //    strBody.Append("Dear Valued Customer, <br/>");
    //    strBody.Append("<p>Please find the attached copy of Invoice, including POD, Bill of Lading and Load Order.</p>");
    //    strBody.Append("<p>We know you have a choice of carriers. Thanks for choosing " + ConfigurationSettings.AppSettings["CompanyName"].ToString() + ". Please choose " + ConfigurationSettings.AppSettings["CompanyName"].ToString() + " for your future transportation needs.</p>");
    //    strBody.Append("<p></p>Regards<br/>" + GetFromXML.CompanyName + "<br/>Dispatch Dept.<br/>" + GetFromXML.CompanyPhone + "<br/>" + GetFromXML.CompanyFax);
    //    return strBody.ToString();
    //} 
    #endregion

    private string GetBodyDeliveredDetails(string Container, string RefNo, string DriverWaitingtime, string DelevaryTime)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();
        strBody.Append("Hello Valued Customer , <br/><p>");
        if (Container.Length > 0)
            strBody.Append("Container# " + Container);
        if (RefNo.Length > 0 && Container.Length > 0)
            strBody.Append("&nbsp;and&nbsp; Ref #&nbsp; " + RefNo + "</p>");
        strBody.Append("Container is delivered. We will provide you p.o.d/bill of lading after we receive from the driver. Please note below driver Times, <br/><br/>");
        strBody.Append("Time in: " +FormatDate(DriverWaitingtime));
        strBody.Append("<br/>Time out: " + FormatDate(DelevaryTime));
        strBody.Append("<p>We know you have a choice of carriers. Thanks for choosing " + ConfigurationSettings.AppSettings["CompanyName"].ToString() + ". Please choose " + ConfigurationSettings.AppSettings["CompanyName"].ToString() + " for your future transportation needs.</p>");
        strBody.Append("<p></p>Regards<br/>" + GetFromXML.CompanyName + "<br/>Dispatch Dept.<br/>" + GetFromXML.CompanyPhone + "<br/>" + GetFromXML.CompanyFax);
        return strBody.ToString();
    }

    private string GetBodyReachedDestinationDetails(string Container, string RefNo, string DriverWaitingtime)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();
        strBody.Append("Valued Customer , <br/><p>");
        if (Container.Length > 0)
            strBody.Append("Please note re unit # " + Container);
        if (RefNo.Length > 0 && Container.Length > 0)
            strBody.Append("&nbsp;and&nbsp; Ref #&nbsp; " + RefNo + ". Driver reached warehouse. </p>");
        
        strBody.Append("Time in: " + FormatDate(DriverWaitingtime));
        strBody.Append("<p>We will advise you when driver finishes unloading. Please note that the free unloading time is 2 hrs. We will advise you if any detention on this load. Also we will send you another e-mail when container is unloaded.</p>");
        strBody.Append("<p></p>Regards<br/>" + GetFromXML.CompanyName + "<br/>Dispatch Dept.<br/>" + GetFromXML.CompanyPhone + "<br/>" + GetFromXML.CompanyFax);
        return strBody.ToString();
    }

    private string GetBodyPODDetails()
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();
        strBody.Append("Dear Valued Customer, <br/>");
        strBody.Append("<p>Please see attached proof of delivery. Your load is delivered.</p>");
        strBody.Append("<p>We know you have a choice of carriers. Thanks for choosing " + ConfigurationSettings.AppSettings["CompanyName"].ToString() + ". Please choose " + ConfigurationSettings.AppSettings["CompanyName"].ToString() + " for your future transportation needs.</p>");
        strBody.Append("<p></p>Regards<br/>" + GetFromXML.CompanyName + "<br/>Dispatch Dept.<br/>" + GetFromXML.CompanyPhone + "<br/>" + GetFromXML.CompanyFax);
        return strBody.ToString();
    }

    private string FormatDate(string dateTime)
    {
        if(!string.IsNullOrEmpty(dateTime.Trim()))
        {
            string[] dateTimeArr= dateTime.Split(' ');
            return dateTimeArr[0] + " <b>" + dateTimeArr[1] + " " + dateTimeArr[2] + "</b>";
        }
        else 
            return string.Empty;
        
    }

    private string GetBodyDetails(string Container, string RefNo, string DriverWaitingtime)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();
        strBody.Append("Hello Sir/Madam <br/><p></p>");
        strBody.Append("Regarding ");
        if (Container.Length > 0)
            strBody.Append("&nbsp;Container# " + Container);
        if (RefNo.Length > 0 && Container.Length > 0)
            strBody.Append("&nbsp;and&nbsp; Ref #&nbsp; " + RefNo);
        strBody.Append("<br/>Driver is on waiting time, <br/><br/>");
        strBody.Append("Driver Time in: " + DriverWaitingtime);
        strBody.Append("<br/>Driver   time out: Driver is still at warehouse.<br/> We will advise you when driver finishes unloading");
        strBody.Append("<p></p>Regards<br/>" + GetFromXML.CompanyName + "<br/>Dispatch Dept.<br/>" + GetFromXML.CompanyPhone + "<br/>" + GetFromXML.CompanyFax);
        return strBody.ToString();
    }
    public string DesginPaybleDetails(DataTable dtPayables)
    {
        System.Text.StringBuilder strCode = new System.Text.StringBuilder();
        strCode.Append("");
        if (dtPayables.Rows.Count > 0)
        {
            bool blVal = false;
            for (int i = 0, j = 0; i < dtPayables.Columns.Count; i++)
            {
                #region Validations
                blVal = false;
                if (Convert.IsDBNull(dtPayables.Rows[0][i]))
                    dtPayables.Rows[0][i] = 0;
                if (Convert.ToString(dtPayables.Rows[0][i]).Length == 0)
                    dtPayables.Rows[0][i] = 0;
                try
                {
                    if (Convert.ToDouble(dtPayables.Rows[0][i]) > 0)
                    {
                        blVal = true;
                    }
                }
                catch
                {
                    if (Convert.ToString(dtPayables.Rows[0][i]).Trim().Length > 0)
                    {
                        blVal = true;
                    }
                }
                #endregion
                if (blVal)
                {
                    strCode.Append("<tr id=" + (((j % 2) == 0) ? "row" : "altrow") + " >");
                    if (i < (dtPayables.Columns.Count - 1))
                        strCode.Append("<td align=right width=30%>" + dtPayables.Columns[i].ColumnName + "</td>");
                    else
                        strCode.Append("<td align=right width=30%><b>" + dtPayables.Columns[i].ColumnName + "</b></td>");
                    strCode.Append("<td align=left>" + Convert.ToString(dtPayables.Rows[0][i]).Trim() + "</td>");
                    strCode.Append("</tr>");
                    j = j + 1;
                }
            }
            if (strCode.ToString().Length > 0)
            {
                strCode.Insert(0, "<table width=100% border=0 cellspacing=0 cellpadding=0 id=tblform> <tr><th colspan=2>Accessorial Charges($)</th></tr>");
                strCode.Append("</table>");
            }
            else
            {
                strCode.Append("<table width=100% border=0 cellspacing=0 cellpadding=0 id=tblform> <tr><th colspan=2>Accessorial Charges($)</th></tr>");
                strCode.Append("<tr id=row>");
                strCode.Append("<td align=center width=30%><b>No Charges Available for Load : " + Convert.ToString(ViewState["ID"]) + " </b></td>");
                strCode.Append("</tr>");
                strCode.Append("</table>");
            }
        }
        else
        {
            strCode.Append("<table width=100% border=0 cellspacing=0 cellpadding=0 id=tblform> <tr><th colspan=2>Accessorial Charges($)</th></tr>");
            strCode.Append("<tr id=row>");
            strCode.Append("<td align=center width=30%><b>No Charges Available for Load : " + Convert.ToString(ViewState["ID"]) + " </b></td>");
            strCode.Append("</tr>");
            strCode.Append("</table>");
        }
        return strCode.ToString();
    }
    private string GetPickedUpBodyDetails(string Container, string Chasis)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();
        strBody.Append("Date :"+DateTime.Today.ToShortDateString()+"<br/><p></p>");
        strBody.Append("Dear Sir/Madam, <br/><p></p>");
        strBody.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Regarding ");
        if (Container.Length > 0)
            strBody.Append("&nbsp;Container# " + Container+",");
        if (Chasis.Length > 0 && Chasis.Length > 0)
            strBody.Append("&nbsp;&nbsp;  CHASSIS #&nbsp; " + Chasis+",");
        strBody.Append("&nbsp;Please  put this container in Terminal system for offhire. Container is arriving Oakland California soon. Also please advise us at which port we have to terminate this container when empty.");
        strBody.Append("<p>Please reply   this  e mail  asap. </p>");
        strBody.Append("</br>Container # " + Container + " </br>Chassis # " + Chasis + "</br></br><p>Your help is greatly appreciated.</p>");
        strBody.Append("<p></p>Regards<br/>" + GetFromXML.CompanyName + "<br/>Dispatch Dept.<br/>" + GetFromXML.CompanyPhone + "<br/>" + GetFromXML.CompanyFax + "<br/>E Mail:<a href='mailto:" + GetFromXML.AdminEmail + "'> " + GetFromXML.AdminEmail + "</a>");
        return strBody.ToString();
    }    
}

