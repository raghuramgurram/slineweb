using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CustomerInvoice : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Invoice";
        //lblCompanyAddress.Text = Convert.ToString(Application["CompanyAddress"]);
        if (!IsPostBack)
        {
            //lblCompanyAddress.Text = "<strong>" + Convert.ToString(ConfigurationSettings.AppSettings["CompanyName"]) + "</strong><br/>" + Convert.ToString(ConfigurationSettings.AppSettings["ComapanyAddress"]) + "<br/>" + Convert.ToString(ConfigurationSettings.AppSettings["ComapanyState"]) + "<br/>" + Convert.ToString(ConfigurationSettings.AppSettings["CompanyPhone"]) + "<br/>" + Convert.ToString(ConfigurationSettings.AppSettings["CompanyFax"]);
            lblCompanyAddress.Text = "<strong>" + GetFromXML.CompanyName + "</strong><br/>" + GetFromXML.ComapanyAddress + "<br/>" + GetFromXML.ComapanyState + "<br/>" + GetFromXML.CompanyPhone + "<br/>" + GetFromXML.CompanyFax;
            if (Request.QueryString.Count > 0)
            {
                string[] strParams = Convert.ToString(Request.QueryString[0]).Trim().Split('^');
                ViewState["ID"] = Convert.ToInt64(strParams[0]);
                ViewState["CustomerId"] = Convert.ToInt64(strParams[1]);
                FillDetails(Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(ViewState["CustomerId"]));
                strParams = null;
            }
        }
    }

    private void FillDetails(long ID, long CustomerId)
    {
        lblInvoiceLoad.Text = lblLoad.Text = ID.ToString();

        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetInvoiceDetails", new object[] { ID });

        if (ds.Tables.Count > 2)
        {
            lblOrigin.Text = CommonFunctions.FormatMultipleOrigins(Convert.ToString(ds.Tables[0].Rows[0][0]).Trim());
            lblDestination.Text = CommonFunctions.FormatMultipleOrigins(Convert.ToString(ds.Tables[0].Rows[0][1]).Trim());
            if (ds.Tables[1].Rows.Count > 0)
            {
                lblBillTo.Text = Convert.ToString(ds.Tables[1].Rows[0][0]) + "<br/>" + Convert.ToString(ds.Tables[1].Rows[0][1]) + "  " + Convert.ToString(ds.Tables[1].Rows[0][2]) + "<br/>";
                lblBillTo.Text += Convert.ToString(ds.Tables[1].Rows[0][3]) + ", " + Convert.ToString(ds.Tables[1].Rows[0][4]) + " " + Convert.ToString(ds.Tables[1].Rows[0][5]);
                lblBillingRef.Text = Convert.ToString(ds.Tables[1].Rows[0][6]);
                lblDate.Text = Convert.ToString(ds.Tables[1].Rows[0][7]);
                lblLoad.Text = Convert.ToString(ID);
                lblContainer.Text = Convert.ToString(ds.Tables[1].Rows[0][11]);
                lblChasis.Text = Convert.ToString(ds.Tables[1].Rows[0][12]);
                lblDescription.Text = Convert.ToString(ds.Tables[1].Rows[0][14]);
                lblTerms.Text = "Due on Receipt";
                //lblremarks.Text = Convert.ToString(ds.Tables[1].Rows[0][14]);
                //lblBooking.Text = Convert.ToString(ds.Tables[1].Rows[0][9]);
            }
            if (ds.Tables[2].Rows.Count > 0)
            {
                lblSacript.Text = Convert.ToString(ds.Tables[2].Rows[0][0]);
            }
            #region Old Code
            //{
            //    lblAmount.Text = "$" + Convert.ToString(ds.Tables[2].Rows[0][0]).Trim();
            //    lblFuelCharges.Text = "$" + Convert.ToString(ds.Tables[2].Rows[0][2]).Trim();

            //    if (Convert.ToString(ds.Tables[2].Rows[0][3]).Trim().Length > 0 && Convert.ToDouble(ds.Tables[2].Rows[0][3]) > 0)
            //    {
            //        lblChargesSplit.Text = "$" + Convert.ToString(ds.Tables[2].Rows[0][3]).Trim();
            //    }
            //    else
            //    {
            //        lblChargesSplit.Visible = false;
            //        lblChasisSplit1.Visible = false;
            //    }
            //    if (Convert.ToString(ds.Tables[2].Rows[0][4]).Trim().Length > 0 && Convert.ToDouble(ds.Tables[2].Rows[0][4]) > 0)
            //    {
            //        lblChasisRent.Text = "$" + Convert.ToString(ds.Tables[2].Rows[0][4]).Trim();
            //    }
            //    else
            //    {
            //        lblChasisRent.Visible = false;
            //        lblChasisRent1.Visible = false;
            //    }
            //    if (Convert.ToString(ds.Tables[2].Rows[0][7]).Trim().Length > 0)
            //    {
            //        lblRemarks.Text = Convert.ToString(ds.Tables[2].Rows[0][7]).Trim() + "&nbsp;&nbsp;";
            //    }
            //    else
            //    {
            //        lblRemarks.Visible = false;
            //        lblOtherChargesReason1.Visible = false;
            //    }
            //    if (Convert.ToString(ds.Tables[2].Rows[0][5]).Trim().Length > 0 && Convert.ToDouble(ds.Tables[2].Rows[0][5]) > 0)
            //    {
            //        lblYardStorage.Text = "$" + Convert.ToString(ds.Tables[2].Rows[0][5]).Trim();
            //    }
            //    else
            //    {
            //        lblYardStorage.Visible = false;
            //        lblYardStorage1.Visible = false;
            //    }
            //    if (Convert.ToString(ds.Tables[2].Rows[0][6]).Trim().Length > 0 && Convert.ToDouble(ds.Tables[2].Rows[0][6]) > 0)
            //    {
            //        lblYardPull.Text = "$" + Convert.ToString(ds.Tables[2].Rows[0][6]).Trim() + "&nbsp;";
            //    }
            //    else
            //    {
            //        lblYardPull.Visible = false;
            //        lblYardPull1.Visible = false;
            //    }


            //    lblBalance.Text = "$" + Convert.ToString(Convert.ToInt64(ds.Tables[2].Rows[0][0]) + Convert.ToInt64(ds.Tables[2].Rows[0][1]) +
            //        Convert.ToInt64(ds.Tables[2].Rows[0][2]) + Convert.ToInt64(ds.Tables[2].Rows[0][3]) + Convert.ToInt64(ds.Tables[2].Rows[0][4]) +
            //        Convert.ToInt64(ds.Tables[2].Rows[0][5]) + Convert.ToInt64(ds.Tables[2].Rows[0][6]));
            //}
            //else
            //{
            //    lblAmount.Text = "$0";
            //    lblFuelCharges.Text = "$0";
            //    lblBalance.Text = "$0";
            //    lblChargesSplit.Visible = false;
            //    lblChasisSplit1.Visible = false;
            //    lblChasisRent.Visible = false;
            //    lblChasisRent1.Visible = false;
            //    lblRemarks.Visible = false;
            //    lblOtherChargesReason1.Visible = false;
            //    lblYardStorage.Visible = false;
            //    lblYardStorage1.Visible = false;
            //    lblYardPull.Visible = false;
            //    lblYardPull1.Visible = false;
            //}
            #endregion
        }
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (ViewState["ID"] != null)
        {
            Response.Redirect("~/Customer/CustomerPayments.aspx?" + Convert.ToInt64(ViewState["ID"]));
        }
    }    
}
