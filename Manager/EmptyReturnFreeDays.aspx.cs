using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

public partial class EmptyReturnFreeDays : System.Web.UI.Page
{
    //added for checking Grid data to show(Madhav)
    protected int GridDataToShow
    {
        get
        {
            if (ViewState["GridDataToShow"] == null)
            {
                if (Request.QueryString["type"] != null)
                {
                    ViewState["GridDataToShow"] = Convert.ToInt32(Request.QueryString["type"]);
                }
                else
                {
                    ViewState["GridDataToShow"] = 0;
                }
            }
            return Convert.ToInt32(ViewState["GridDataToShow"].ToString());
        }
        set
        {
            ViewState["GridDataToShow"] = value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //New Header Test changes (madhav)
            if (GridDataToShow == 0)
            {
                Session["TopHeaderText"] = "Containers Going Per Diem";
                lblAccLoads.Visible = false;
            }
            else if (GridDataToShow == 1)
            {
                Session["TopHeaderText"] = "Same Day Loads";
                lblAccLoads.Visible = false;
            }
            else if (GridDataToShow == 2)
            {
                Session["TopHeaderText"] = "Next Day Loads";
                lblAccLoads.Visible = false;
            }
            else if (GridDataToShow == 3)
            {
                Session["TopHeaderText"] = "Accessorial Charges Loads";
                if (!IsPostBack)
                {
                   FillLists();
                   Session["LoadGridPageIndex"] = null;
                }
            }
            else if (GridDataToShow == 4)
            {
                Session["TopHeaderText"] = "Today�s LFD Loads";
                lblAccLoads.Visible = false;
            }
            
            Session["BackPage3"] = null;
            Session["BackPage4"] = null;
            Session["BackPage2"] = "~/Manager/EmptyReturnFreeDaysAlerts.aspx";
            Response.AppendHeader("Refresh", "300");
            GridFill();   
        }
        catch (Exception ex)
        {
            //CommonFunctions.SendEmail(ConfigurationSettings.AppSettings["SmtpUserName"], "tbnsridhar@savitr.com", "shiva994u@gmail.com", "",
            //"Error Details",
            //"error in Dash board for the role :" + Convert.ToString(Session["Role"]) + "While" + ex.Message, null);
        }
    }

    private void FillLists()
    {
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCustomerNames");
        if (ds.Tables.Count > 0)
        {
            ddlCustomers.DataSource = ds.Tables[0];
            ddlCustomers.DataValueField = "bint_CustomerId";
            ddlCustomers.DataTextField = "nvar_CustomerName";
            ddlCustomers.DataBind();
        }
        ddlCustomers.Items.Insert(0, new ListItem("All", "0"));
        ds.Tables.Clear();
        if (ds != null) { ds.Dispose(); ds = null; }
    }

    private void GridFill()
    {/*changes done by madhav for today , tomorrow and Accessorial list */
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        else
        {
            if (Session["LocationDS"] != null)
            {
                DataSet locDs = Session["LocationDS"] as DataSet;
                officeLocationId = Convert.ToInt64(locDs.Tables[1].Rows[0][0]);
            }
        }
        //SLine 2017 Enhancement for Office Location Ends
        if (GridDataToShow == 0)
        {
            // old code for getting Containers Going Per Diem
            grdManager.MainTableName = "eTn_EmptyReturnFreeDays_View";
            grdManager.MainTablePK = "eTn_EmptyReturnFreeDays_View.bint_LoadId";
            grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_EmptyReturnFreeDays_View.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_EmptyReturnFreeDays_View.bint_LoadId as 'Load'," +
                "eTn_EmptyReturnFreeDays_View.nvar_Container as 'Container1',eTn_EmptyReturnFreeDays_View.nvar_Container1 as 'Container2', Case when bit_RailContainer=1 then 'Rail-' else '' end as 'Type', nvar_TripType as TripType," +
                "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_EmptyReturnFreeDays_View.date_LastFreeDate", "Last Free Date") + ";eTn_EmptyReturnFreeDays_View.nvar_Booking as 'Booking#';eTn_EmptyReturnFreeDays_View.nvar_Pickup as 'Pickup#', eTn_EmptyReturnFreeDays_View.EmptyReturnFreeDate as 'Empty Return Last Free Date',eTn_EmptyReturnFreeDays_View.nvar_LoadStatusDesc as 'Status'";
            grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_EmptyReturnFreeDays_View.bint_CustomerId ";
            grdManager.SingleRowColumnsWhereClause = "bint_OfficeLocationId = " + officeLocationId;            
            grdManager.SingleRowColumnsOrderByClause = " order by eTn_EmptyReturnFreeDays_View.EmptyReturnFreeDate desc,eTn_EmptyReturnFreeDays_View.bint_LoadId";

            grdManager.VisibleColumnsList = "Load;Customer;Driver/Carrier;Origin;Destination;Delivery Appt Date;Empty Return Last Free Date;Status";
            grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
            grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";
            grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");
            grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver') ";

            grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
            grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
            grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To<br/> '+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.';eTn_TrackLoad.nvar_Location as 'Last Location'";
            grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                "inner join eTn_EmptyReturnFreeDays_View on eTn_EmptyReturnFreeDays_View.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
            grdManager.MultipleRowTablesAdditionalWhereClause = " ;; 1=1";          
        }
        else if(GridDataToShow ==1) // code for today list(Madhav)
        {
            grdManager.MainTableName = "eTn_Load";
            grdManager.MainTablePK = "eTn_Load.bint_LoadId";
            grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2', Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end as 'Type', eTn_Load.nvar_TripType as TripType, DATEADD(dd, eTn_Shipping.sint_EmptyReturnFreeDays, (select top 1 eTn_TrackLoad.date_CreateDate from eTn_TrackLoad where eTn_TrackLoad.bint_LoadId = eTn_Load.bint_LoadId order by eTn_TrackLoad.date_CreateDate )) AS 'Empty Return Last Free Date'," +
                "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";eTn_Load.nvar_Booking as 'Booking#';eTn_Load.nvar_Pickup as 'Pickup#', eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Date',eTn_LoadStatus.nvar_LoadStatusDesc as 'Status'";
            grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId inner join eTn_LoadStatus on  eTn_LoadStatus.bint_LoadStatusId = eTn_Load.bint_LoadStatusId inner join eTn_Destination on eTn_Destination.bint_LoadId = eTn_Load.bint_LoadId INNER JOIN eTn_Shipping ON eTn_Load.bint_ShippingId = eTn_Shipping.bint_ShippingId";
            //SLine 2017 Enhancement for Office Location
            grdManager.SingleRowColumnsWhereClause = "eTn_Destination.date_DeliveryAppointmentDate  >= CONVERT(datetime, CONVERT(varchar, DATEADD(day, 0, GETDATE()), 102)) and eTn_Destination.date_DeliveryAppointmentDate < CONVERT(datetime, CONVERT(varchar, DATEADD(day, 1, GETDATE()), 102))  and eTn_Load.bint_LoadStatusId = 1 and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
            grdManager.SingleRowColumnsOrderByClause = " order by eTn_Destination.date_DeliveryAppointmentDate,eTn_Load.bint_LoadId";

            grdManager.VisibleColumnsList = "Load;Customer;Driver/Carrier;Origin;Destination;Delivery Appt Date;Empty Return Last Free Date;Status";
            grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
            grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";
            grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");
            grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver') ";

            grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
            grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
            grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To<br/> '+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.';eTn_TrackLoad.nvar_Location as 'Last Location'";
            grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                "inner join eTn_EmptyReturnFreeDays_View on eTn_EmptyReturnFreeDays_View.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
            grdManager.MultipleRowTablesAdditionalWhereClause = " ;; 1=1";
        }
        else if (GridDataToShow == 2)// code for tomarrow list(Madhav)
        {
            grdManager.MainTableName = "eTn_Load";
            grdManager.MainTablePK = "eTn_Load.bint_LoadId";
            grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',DATEADD(dd, eTn_Shipping.sint_EmptyReturnFreeDays, (select top 1 eTn_TrackLoad.date_CreateDate from eTn_TrackLoad where eTn_TrackLoad.bint_LoadId = eTn_Load.bint_LoadId order by eTn_TrackLoad.date_CreateDate )) AS 'Empty Return Last Free Date', " +
                "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";eTn_Load.nvar_Booking as 'Booking#';eTn_Load.nvar_Pickup as 'Pickup#', eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Date',eTn_LoadStatus.nvar_LoadStatusDesc as 'Status'";
            grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId inner join eTn_LoadStatus on  eTn_LoadStatus.bint_LoadStatusId = eTn_Load.bint_LoadStatusId inner join eTn_Destination on eTn_Destination.bint_LoadId = eTn_Load.bint_LoadId INNER JOIN eTn_Shipping ON eTn_Load.bint_ShippingId = eTn_Shipping.bint_ShippingId";
            //SLine 2017 Enhancement for Office Location
            grdManager.SingleRowColumnsWhereClause = " eTn_Destination.date_DeliveryAppointmentDate  >= CONVERT(datetime, CONVERT(varchar, DATEADD(day, 1, GETDATE()), 102)) and eTn_Destination.date_DeliveryAppointmentDate < CONVERT(datetime, CONVERT(varchar, DATEADD(day, 2, GETDATE()), 102))  and eTn_Load.bint_LoadStatusId = 1  and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
            grdManager.SingleRowColumnsOrderByClause = " order by eTn_Destination.date_DeliveryAppointmentDate,eTn_Load.bint_LoadId";

            grdManager.VisibleColumnsList = "Load;Customer;Driver/Carrier;Origin;Destination;Delivery Appt Date;Empty Return Last Free Date;Status";
            grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
            grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";
            grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");
            grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver') ";

            grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
            grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
            grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To<br/> '+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.';eTn_TrackLoad.nvar_Location as 'Last Location'";
            grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                "inner join eTn_EmptyReturnFreeDays_View on eTn_EmptyReturnFreeDays_View.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
            grdManager.MultipleRowTablesAdditionalWhereClause = " ;; 1=1";
        }

            //SLine 2016 Enhancements
        //The Accessorial Charges List is being generated now with a new condition, 
        //If the Acc charges are set as to be paid, then only they have to be shown and counted
        //The old code is commented and grouped as Old Code -- Akhtar
        else if (GridDataToShow == 3)// code for Accessorial list(Madhav)
        {
            #region Old Code
            /*

            string customer = Convert.ToString(ddlCustomers.SelectedValue);
            string whereClause = "";
            grdManager.MainTableName = "eTn_Load";
            grdManager.MainTablePK = "eTn_Load.bint_LoadId";
            grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                 "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',isnull(DATEADD(dd, eTn_Shipping.sint_EmptyReturnFreeDays, (select top 1 eTn_TrackLoad.date_CreateDate from eTn_TrackLoad where eTn_TrackLoad.bint_LoadId = eTn_Load.bint_LoadId order by eTn_TrackLoad.date_CreateDate )),'') AS 'Empty Return Last Free Date', " +
                 "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                 "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";eTn_Load.nvar_Booking as 'Booking#';eTn_Load.nvar_Pickup as 'Pickup#',eTn_LoadStatus.nvar_LoadStatusDesc as 'Status'";
            grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId inner join eTn_LoadStatus on  eTn_LoadStatus.bint_LoadStatusId = eTn_Load.bint_LoadStatusId  INNER JOIN eTn_Shipping ON eTn_Load.bint_ShippingId = eTn_Shipping.bint_ShippingId ";

            if (customer != "0")
                whereClause = " and eTn_Customer.bint_CustomerId=" + customer;

            grdManager.SingleRowColumnsWhereClause = " eTn_Load.bint_LoadId in (select eTn_Receivables.bint_LoadId from [eTn_Receivables] where (isnull([num_Dryrun],0)+isnull([num_PierTermination],0)+isnull([bint_ExtraStops],0)+isnull([num_LumperCharges],0)+" +
    "isnull([num_DetentionCharges],0)+isnull([num_ChassisSplit],0)+isnull([num_ChassisRent],0)+isnull([num_Pallets],0)+isnull([num_ContainerWashout],0)+isnull([num_YardStorage],0)+isnull([num_RampPullCharges],0)+isnull([num_TriaxleCharge],0)+isnull([num_TransLoadCharges],0)+" +
    "isnull([num_HeavyLightScaleCharges],0)+isnull([num_ScaleTicketCharges],0)+isnull([num_OtherCharges1],0)+isnull([num_OtherCharges2],0)+isnull([num_OtherCharges3],0)+isnull([num_OtherCharges4],0)) > 0 and (nvar_ReceivableStatus is null or nvar_ReceivableStatus = ''))" + whereClause;


            grdManager.SingleRowColumnsOrderByClause = " order by eTn_Load.bint_LoadId";

            grdManager.VisibleColumnsList = "Load;Customer;Driver/Carrier;Origin;Destination;Delivery Appt Date;Empty Return Last Free Date;Status";
            grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
            grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";
            grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");
            grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver') ";

            grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
            grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
            grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To<br/> '+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.';eTn_TrackLoad.nvar_Location as 'Last Location'";
            grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                "inner join eTn_EmptyReturnFreeDays_View on eTn_EmptyReturnFreeDays_View.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
            grdManager.MultipleRowTablesAdditionalWhereClause = " ;; 1=1";

            */

            #endregion


            string customer = Convert.ToString(ddlCustomers.SelectedValue);
            string whereClause = "";
            grdManager.MainTableName = "eTn_Load";
            grdManager.MainTablePK = "eTn_Load.bint_LoadId";
            grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                 "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end as 'Type', eTn_Load.nvar_TripType as TripType,isnull(DATEADD(dd, eTn_Shipping.sint_EmptyReturnFreeDays, (select top 1 eTn_TrackLoad.date_CreateDate from eTn_TrackLoad where eTn_TrackLoad.bint_LoadId = eTn_Load.bint_LoadId order by eTn_TrackLoad.date_CreateDate )),'') AS 'Empty Return Last Free Date', " +
                 "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                 "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";eTn_Load.nvar_Booking as 'Booking#';eTn_Load.nvar_Pickup as 'Pickup#',eTn_LoadStatus.nvar_LoadStatusDesc as 'Status'";
            grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId inner join eTn_LoadStatus on  eTn_LoadStatus.bint_LoadStatusId = eTn_Load.bint_LoadStatusId  INNER JOIN eTn_Shipping ON eTn_Load.bint_ShippingId = eTn_Shipping.bint_ShippingId ";

            if (customer != "0")
                whereClause = "and eTn_Customer.bint_CustomerId=" + customer;
            //SLine 2016 Enhancements
            //SLine 2017 Enhancement for Office Location
            grdManager.SingleRowColumnsWhereClause = " eTn_Load.bint_LoadId in (select eTn_Load.bint_LoadId from eTn_Load where eTn_Load.bit_IsAccessorialCharges=1)  and eTn_Load.bint_OfficeLocationId = " + officeLocationId + " " + whereClause;
            //SLine 2016 Enhancements

            grdManager.SingleRowColumnsOrderByClause = " order by eTn_Load.bint_LoadId";

            grdManager.VisibleColumnsList = "Load;Customer;Driver/Carrier;Origin;Destination;Delivery Appt Date;Empty Return Last Free Date;Status";
            grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
            grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";
            grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");
            grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver') ";

            grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
            grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
            grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To<br/> '+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.';eTn_TrackLoad.nvar_Location as 'Last Location'";
            grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                "inner join eTn_EmptyReturnFreeDays_View on eTn_EmptyReturnFreeDays_View.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
            //grdManager.MultipleRowTablesAdditionalWhereClause = " ;; 1=1";


        }
        else if (GridDataToShow == 4)// code for Today's LFD list(Madhav)
        {
            grdManager.MainTableName = "eTn_Load";
            grdManager.MainTablePK = "eTn_Load.bint_LoadId";
            grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                 "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',isnull(DATEADD(dd, eTn_Shipping.sint_EmptyReturnFreeDays, (select top 1 eTn_TrackLoad.date_CreateDate from eTn_TrackLoad where eTn_TrackLoad.bint_LoadId = eTn_Load.bint_LoadId order by eTn_TrackLoad.date_CreateDate )),'') AS 'Empty Return Last Free Date', " +
                 "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                 "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";eTn_Load.nvar_Booking as 'Booking#';eTn_Load.nvar_Pickup as 'Pickup#',eTn_LoadStatus.nvar_LoadStatusDesc as 'Status'";
            grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId inner join eTn_LoadStatus on  eTn_LoadStatus.bint_LoadStatusId = eTn_Load.bint_LoadStatusId  INNER JOIN eTn_Shipping ON eTn_Load.bint_ShippingId = eTn_Shipping.bint_ShippingId ";            
            grdManager.SingleRowColumnsWhereClause = " eTn_Load.date_LastFreeDate >= CONVERT(datetime, CONVERT(varchar, DATEADD(day, 0, GETDATE()), 102))" +
                "AND eTn_Load.date_LastFreeDate < CONVERT(datetime, CONVERT(varchar, DATEADD(day, 1, GETDATE()), 102)) " +
                "and eTn_Load.bint_LoadStatusId in (1,2,15)" +
                //SLine 2017 Enhancement for Office Location
                "  and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
            grdManager.SingleRowColumnsOrderByClause = " order by eTn_Load.bint_LoadId";

            grdManager.VisibleColumnsList = "Load;Customer;Driver/Carrier;Origin;Destination;Delivery Appt Date;Empty Return Last Free Date;Status";
            grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
            grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";
            grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");
            grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver') ";

            grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
            grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
            grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To<br/> '+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.';eTn_TrackLoad.nvar_Location as 'Last Location'";
            grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                "inner join eTn_EmptyReturnFreeDays_View on eTn_EmptyReturnFreeDays_View.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
            grdManager.MultipleRowTablesAdditionalWhereClause = " ;; 1=1";
        }
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/UpdateLoadStatus.aspx";
                
                grdManager.DeleteVisible = false;
                grdManager.LastLocationVisible = true;
                grdManager.LoadStatus = "ContainersGoingPerDiem";
                
            //New Header Test changes(Madhav)
                if (GridDataToShow == 0)
                {
                    barManager.HeaderText = "Containers Going Per Diem";
                }
                else if (GridDataToShow == 1)
                {
                    barManager.HeaderText = "Same Day Loads";
                }
                else if (GridDataToShow == 2)
                {
                    barManager.HeaderText = "Next Day Loads";
                }
                else if (GridDataToShow == 3)
                {
                    barManager.HeaderText = "Accessorial Charges Loads";
                }
                else if (GridDataToShow == 4)
                {
                    barManager.HeaderText = "Today�s LFD Loads";
                }
                if (Session["LoadGridPageIndex"] == null)
                    Session["LoadGridPageIndex"] = 0;
                grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
           
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Session["LoadGridPageIndex"] = null;
        GridFill();
    }
   
}
