<%@ Page Language="C#" MasterPageFile="~/Customer/CustomerMaster.master" AutoEventWireup="true" CodeFile="CustomerLoadDetails.aspx.cs" Inherits="CustomerLoadDetails" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/Grid.ascx" TagName="Grid" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" style="width: 30%; height: 777px;" valign="top">
                            <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0"
                                width="100%">
                                <tr>
                                    <th colspan="2">
                                        Identification -&nbsp;<asp:Label ID="lblLoadId" runat="server" Text="LoadId"></asp:Label></th>
                                </tr>
                                <tr id="row">
                                    <td align="right" width="40%" style="height: 20px">
                                        Ref.# : &nbsp;
                                    </td>
                                    <td width="60%" style="height: 20px" align="left" valign="middle">
                                        <asp:Label ID="lblRef" runat="server"></asp:Label>&nbsp;
                                        </td>                                        
                                        </tr>                               
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>
                            <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <th colspan="2">
                                        Order Status
                                    </th>
                                </tr>
                                <tr id="row">
                                    <td align="right" width="40%">
                                        Load Status :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblStatus" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        Container Size/Location :</td>
                                    <td>
                                        <asp:Label ID="lblRailBill" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="row">
                                    <td align="right">
                                        Order Bill Date :</td>
                                    <td>
                                        <asp:Label ID="lblBillDate" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        Person Bill :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPersonBill" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>
                            <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <th colspan="2">
                                        Routing</th>
                                </tr>
                                <tr id="row">
                                    <td colspan="2" style="height: 75px">
                                        <%--<strong>RAIL LINE-HAUL </strong><br />
                      EXEL TRANSPORTATION SERVICES, INC.<br />
                      P O BOX 844711 <br />
                      DALLAS , TX 75284-4650 <br />
                      <a href="#">www.tidework.com</a>--%>
                                        <asp:Label ID="lblRoute" runat="server" Text=""></asp:Label>&nbsp;
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" width="40%">
                                        Pier Termination / Empty Return:</td>
                                    <td>
                                        <asp:Label ID="lblPier" runat="server" Text=""></asp:Label>&nbsp;
                                    </td>
                                </tr>
                                <tr id="row" valign="top">
                                    <td align="right">
                                        Contact Person(s) :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblShippingContact" runat="server" Text=""></asp:Label>&nbsp;</td>
                                </tr>
                            </table>                        
                        </td>
                        <td align="left" style="width: 1%; height: 777px;" valign="top">
                            <img alt="" height="1" src="../images/pix.gif" width="5" /></td>
                        <td align="left" valign="top" style="height: 777px">
                            <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <th colspan="4" style="height: 22px">
                                        Order Identification
                                    </th>
                                </tr>
                                <tr id="row">
                                    <td align="right" valign="top" width="15%">
                                        Customer :
                                    </td>
                                    <td colspan="3" valign="top">
                                        <asp:Label ID="lblCustomer" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        Pickup# :
                                    </td>
                                    <td width="25%">
                                        <asp:Label ID="lblPickUp" runat="server"></asp:Label>&nbsp;</td>
                                    <td align="right">
                                        Commodity :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCommodity" runat="server" Text=""></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="row">
                                    <td align="right">
                                        Container# :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblContainer" runat="server"></asp:Label>&nbsp;</td>
                                    <td align="right">
                                        Seal# :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblSeal" runat="server" Text=""></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" valign="top">
                                        Chasis# :
                                    </td>
                                    <td valign="top">
                                        <asp:Label ID="lblChasis" runat="server" Text=""></asp:Label>&nbsp;</td>
                                    <td align="right" valign="top">
                                        Created :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCreated" runat="server" Text=""></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="row">
                                    <td align="right" valign="top">
                                        Pieces :
                                    </td>
                                    <td valign="top">
                                        <asp:Label ID="lblPieces" runat="server"></asp:Label>&nbsp;
                                    </td>
                                    <td align="right" valign="top">
                                        Create Person :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPersong" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" valign="top">
                                        Weight :
                                    </td>
                                    <td valign="top">
                                        <asp:Label ID="lblWeigth" runat="server"></asp:Label>&nbsp;</td>
                                    <td align="right" valign="top">
                                     &nbsp; &nbsp; &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;</td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <uc2:Grid ID="Grid1" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <uc2:Grid ID="Grid2" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>                        
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="right">
                                        <uc1:GridBar ID="GridBar2" runat="server" HeaderText="Load Status Information" />
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <uc2:Grid ID="Grid4" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="right">
                                        <uc1:GridBar ID="GridBar3" runat="server" HeaderText="Notes" />
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <uc2:Grid ID="Grid5" runat="server"/>
                                    </td>
                                </tr>
                            </table>                                             
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

