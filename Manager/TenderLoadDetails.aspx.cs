﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Manager_TenderLoadDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Tendered Loads";
        if (!IsPostBack)
        {
            if (Request.QueryString != null)
            {
                string cid = Request.QueryString["CID"].ToString();
                btnacpt.CommandArgument = btnrjct.CommandArgument = btnback.CommandArgument = cid;
                List<Customer> ld = (List<Customer>)Session["LoadData"];
                Customer localObject = ld.Select(x => x).Where(x => x.orderNumber == cid).FirstOrDefault();
                ViewState["Load"] = localObject;
                btnMove.Text = localObject.SiteName;
                LoadDroupDowns(localObject);
            }
        }
    }
    private void LoadDroupDowns(Customer localObject)
    {
        DataSet ds;
        ds = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["conString"], "Get_ALLOffice_Locations", new object[] { });

        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            List<string> locationCodes = new List<string>();
            string locationCode=null;
            foreach (DataRow row in ds.Tables[0].Rows)
                locationCodes.Add(row[2].ToString());

            if (localObject.OriginDetails != null && localObject.OriginDetails.Count > 0 && localObject.DestinationDetails != null && localObject.DestinationDetails.Count > 0)
            {
                if (localObject.TripType != null && localObject.TripType == "PU")
                {
                    foreach (var item in localObject.OriginDetails)
                    {
                        if (locationCodes.Contains(item.StateorProvinceCode))
                        {
                            locationCode = item.StateorProvinceCode;
                            break;
                        }
                    }
                }
                else
                {
                    foreach (var item in localObject.DestinationDetails)
                    {
                        if (locationCodes.Contains(item.StateorProvinceCode))
                        {
                            locationCode = item.StateorProvinceCode;
                            break;
                        }
                    }
                }
            }
            else if ((localObject.OriginDetails != null && localObject.OriginDetails.Count > 0))
            {
                foreach (var item in localObject.OriginDetails)
                {
                    if (locationCodes.Contains(item.StateorProvinceCode))
                    {
                        locationCode = item.StateorProvinceCode;
                        break;
                    }
                }
            }
            else if ((localObject.DestinationDetails != null && localObject.DestinationDetails.Count > 0))
            {
                foreach (var item in localObject.DestinationDetails)
                {
                    if (locationCodes.Contains(item.StateorProvinceCode))
                    {
                        locationCode = item.StateorProvinceCode;
                        break;
                    }
                }
            }
            ddlTopLocation.DataSource = ds.Tables[0];
            ddlTopLocation.DataTextField = "nvar_OfficeLocationName";
            ddlTopLocation.DataValueField = "bint_OfficeLocationId";
            ddlTopLocation.DataBind();
            ddlTopLocation.Items.Insert(0, "Select");
            if (!string.IsNullOrWhiteSpace(locationCode))
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    if (row[2].ToString() == locationCode)
                        ddlTopLocation.SelectedValue = row[0].ToString();
                }
            }
        }
        DataSet ds1 = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCommonLoadDetails", new object[] { "Add", ddlTopLocation.SelectedValue });
        if (ds1.Tables.Count > 0)
        {
            ddlshipping.DataSource = ds1.Tables[4];
            ddlshipping.DataTextField = "nvar_ShippingLineName";
            ddlshipping.DataValueField = "bint_ShippingId";
            ddlshipping.DataBind();
            ddlshipping.Items.Insert(0, "Select");
        }

    }
    public DataTable ToDataTable<T>(List<T> items)

    {

        DataTable dataTable = new DataTable(typeof(T).Name);

        //Get all the properties

        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

        foreach (PropertyInfo prop in Props)

        {

            //Setting column names as Property names

            dataTable.Columns.Add(prop.Name);

        }

        foreach (T item in items)

        {

            var values = new object[Props.Length];

            for (int i = 0; i < Props.Length; i++)

            {

                //inserting property values to datatable rows

                values[i] = Props[i].GetValue(item, null);

            }

            dataTable.Rows.Add(values);

        }

        //put a breakpoint here and check datatable

        return dataTable;

    }

    private string ConverttoMoney(string value)
    {
        if (string.IsNullOrWhiteSpace(value))
            return "0.00";
        else if (value.Length > 1)
        {
            return value.Substring(0, value.Length - 2) + "." + value.Substring(value.Length - 2);
        }
        else return value;

    }

    private void SendInvoiceNumber(string aggregator, string strshipmentno, string strInvoiceNumber)
    {
        string url = ConfigurationManager.AppSettings["PostmanURL"] + "api/ReciveInvoiceNumber/";
        var request = (HttpWebRequest)WebRequest.Create(url);
        request.Method = "POST";
        request.Headers.Add("token", ConfigurationManager.AppSettings["EDIToken"]);
        request.Headers.Add("aggregator", aggregator);
        request.Headers.Add("strshipmentno", strshipmentno);
        request.Headers.Add("strInvoiceNumber", strInvoiceNumber);
        request.ContentLength = 0;
        var response = (HttpWebResponse)request.GetResponse();
        string content = string.Empty;
        if (response.StatusCode == HttpStatusCode.OK)
        {
        }
    }

    private object NullCheck(object obj)
    {
        return obj != null ? obj : DBNull.Value;
    }

    protected void ddlTopLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet ds1 = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCommonLoadDetails", new object[] { "Add", ddlTopLocation.SelectedValue });
        if (ds1.Tables.Count > 0)
        {
            ddlshipping.DataSource = ds1.Tables[4];
            ddlshipping.DataTextField = "nvar_ShippingLineName";
            ddlshipping.DataValueField = "bint_ShippingId";
            ddlshipping.DataBind();
            ddlshipping.Items.Insert(0, "Select");
        }
    }

    protected void btnAction_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        if (btn.CommandName == "MOVE")
        {
            string cid = ((Customer)ViewState["Load"]).orderNumber;
            string url = ConfigurationManager.AppSettings["PostmanURL"] + "api/UpdateLoadLocation/";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.Headers.Add("token", ConfigurationManager.AppSettings["EDIToken"]);
            request.Headers.Add("partner", ConfigurationManager.AppSettings["PartnerCode"].ToString());
            request.Headers.Add("loadnumber", cid);
            request.Headers.Add("partnerlocation", ConfigurationManager.AppSettings["PartnerLocationCode"].ToString());
            request.ContentLength = 0;
            var response = (HttpWebResponse)request.GetResponse();
            string content = string.Empty;
            using (var stream = response.GetResponseStream())
            {
                using (var sr = new StreamReader(stream))
                {
                    content = sr.ReadToEnd();
                }
            }
        }
        if (btn.CommandName == "ACCEPT" || btn.CommandName == "REJECT")
        {
            try
            {
                string cid = btn.CommandArgument.ToString();
                List<Customer> ld = (List<Customer>)Session["LoadData"];
                Customer loadDeatils = ld.Select(x => x).Where(x => x.orderNumber == cid).FirstOrDefault();
                string url = ConfigurationManager.AppSettings["PostmanURL"] + "api/AcknowldgeTenderedLoad/";
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.Headers.Add("token", ConfigurationManager.AppSettings["EDIToken"]);
                request.Headers.Add("aggregator", loadDeatils.name);
                request.Headers.Add("partner", loadDeatils.PartnerCode);
                request.Headers.Add("strshipmentno", loadDeatils.orderNumber);
                request.Headers.Add("acceptstatus", btn.CommandName == "ACCEPT" ? "True" : "False");
                request.ContentLength = 0;
                var response = (HttpWebResponse)request.GetResponse();
                string content = string.Empty;
                using (var stream = response.GetResponseStream())
                {
                    using (var sr = new StreamReader(stream))
                    {
                        content = sr.ReadToEnd();
                    }
                }
                if (content == "0" && btn.CommandName == "ACCEPT")
                {

                        ViewState["LoadID"] = 0;
                    string loadType = string.Empty;
                    if (loadDeatils.name.ToUpper().Contains("HUB"))
                    {
                        string AT5 = string.Empty;
                        if (loadDeatils.AT5 != null)
                            loadDeatils.AT5.ForEach(x => AT5 = AT5 + (!string.IsNullOrWhiteSpace(AT5) ? "~" : string.Empty) + x);
                        loadType = AT5.ToString();
                    }
                    else
                    {
                        string containerNumber = string.Empty;
                        if (!string.IsNullOrWhiteSpace(loadDeatils.ContainerNumber))
                            loadType = "Destination Drayage";
                        else
                            loadType = "Origin Drayage"; 

                    }
                    //if(Load)
                    object[] objParams = new object[]{"I",loadDeatils.name,loadDeatils.orderNumber.Trim(),loadDeatils.remarks,string.IsNullOrWhiteSpace(loadType)?string.Empty:loadType,ddlshipping.SelectedValue,(loadDeatils.TripType=="ROUND"?2:1),loadDeatils.RailPickupNum,string.IsNullOrWhiteSpace(loadDeatils.SealNumber)?string.Empty:loadDeatils.SealNumber,
                                                    string.Empty,0,loadDeatils.TripType,"40HC",(string.IsNullOrWhiteSpace(loadDeatils.TrailerNumber)?string.Empty:loadDeatils.TrailerNumber),string.Empty,ddlTopLocation.SelectedValue,Convert.ToInt64(ViewState["LoadID"]),Convert.ToString(Session["UserLoginId"])};
                        System.Data.SqlClient.SqlParameter[] objsqlparams = null;
                        ViewState["LoadID"] = "0";
                        objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationManager.AppSettings["conString"], "Add_Load_Details_New", objParams);

                        if (SqlHelper.ExecuteNonQuery(ConfigurationManager.AppSettings["conString"], CommandType.StoredProcedure, "Add_Load_Details_New", objsqlparams) > 0)
                        {
                            ViewState["LoadID"] = Convert.ToInt64(objsqlparams[objsqlparams.Length - 2].Value);

                            // string[] Values = Convert.ToString(DBClass.executeScalar("select Case when Len([eTn_Load].[nvar_Container])>0 and Len([eTn_Load].[nvar_Container1])>0 then [eTn_Load].[nvar_Container] when Len([eTn_Load].[nvar_Container])>0 and Len([eTn_Load].[nvar_Container1])=0 then [eTn_Load].[nvar_Container] when Len([eTn_Load].[nvar_Container1])>0 then [eTn_Load].[nvar_Container1] else '' end+'^'+Case when Len([eTn_Load].[nvar_Chasis])>0 and Len([eTn_Load].[nvar_Chasis1])>0 then [eTn_Load].[nvar_Chasis] when Len([eTn_Load].[nvar_Chasis])>0 and Len([eTn_Load].[nvar_Chasis1])=0 then [eTn_Load].[nvar_Chasis] when Len([eTn_Load].[nvar_Chasis1])>0 then [eTn_Load].[nvar_Chasis1] else '' end+'^'+(isnull((select top 1 case when Len(nvar_Email)>0 then nvar_Email else '' end from eTn_ShippingContacts where bint_ShippingId=[eTn_Load].bint_ShippingId),(isnull((select top 1 case when Len(nvar_Email)>0 then nvar_Email else '' end from eTn_Shipping where bint_ShippingId=[eTn_Load].bint_ShippingId),'')))) from [eTn_Load] where [eTn_Load].[bint_LoadId]=" + Convert.ToInt64(ViewState["LoadID"]))).Trim().Split('^');
                            //if (Values != null)
                            //{
                            //    // //CommonFunctions.SendEmail(GetFromXML.AdminEmail, (Values.Length == 3 ? Values[2] : ""), GetFromXML.AdminEmail, "", "Pre Authorization for empty UNIT # " + Values[0] + " ( OffHire ) ", GetPickedUpBodyDetails(Values[0], Values[1]), new string[] { "<font color=red>This email was not sent to the shipping line,because Contact email address is not available for this shipping line.</font><br/> " }, null);
                            //    //CommonFunctions.SendEmail(GetFromXML.AdminEmail, (Values.Length == 3 ? Values[2] : ""), "", "", "Pre Authorization for empty UNIT # " + Values[0] + " ( OffHire ) ", GetPickedUpBodyDetails(Values[0], Values[1]), new string[] { "<font color=red>This email was not sent to the shipping line,because Contact email address is not available for this shipping line.</font><br/> " }, null);
                            //}
                            //Values = null;
                        }
                        if (Convert.ToString(ViewState["LoadID"]).Trim().Length > 0 || Convert.ToString(ViewState["LoadID"]) != "0")
                        {
                            if (loadDeatils.OriginDetails != null && loadDeatils.OriginDetails.Count > 0)
                            {
                                foreach (OriginDestination org in loadDeatils.OriginDetails)
                                {                            
                                    object[] objParamsorg = new object[]{org.CompanyName,"O", ddlTopLocation.SelectedValue,NullCheck(org.Address),NullCheck(org.Address1),NullCheck(org.City),NullCheck(org.StateorProvinceCode),
                                NullCheck(org.PostalCode), CommonFunctions.TelNumber(org.Tel),NullCheck(CommonFunctions.FaxNumber(org.Fax)),NullCheck(org.Email),ViewState["LoadID"].ToString(),NullCheck(org.FromDate),NullCheck(org.ContactPerson), NullCheck(org.ToDate), org.sequenceNumber,Convert.ToString(Session["UserLoginId"]),NullCheck(org.Pieces),NullCheck(org.Weight),NullCheck(org.AppNum)};
                                    System.Data.SqlClient.SqlParameter[] objsqlparamsorg = null;
                                    objsqlparamsorg = SqlHelper.PrepareSqlParamsFromSP(ConfigurationManager.AppSettings["conString"], "SP_Insert_Org_Des", objParamsorg);
                                    SqlHelper.ExecuteNonQuery(ConfigurationManager.AppSettings["conString"], CommandType.StoredProcedure, "SP_Insert_Org_Des", objsqlparamsorg).ToString();
                                }

                            }
                            if (loadDeatils.DestinationDetails != null && loadDeatils.DestinationDetails.Count > 0)
                            {
                                foreach (OriginDestination org in loadDeatils.DestinationDetails)
                                {
                                object[] objParamsorg = new object[]{org.CompanyName,"D", ddlTopLocation.SelectedValue,NullCheck(org.Address),NullCheck(org.Address1),NullCheck(org.City),NullCheck(org.StateorProvinceCode),
                                NullCheck(org.PostalCode), CommonFunctions.TelNumber(org.Tel),NullCheck(CommonFunctions.FaxNumber(org.Fax)),NullCheck(org.Email),ViewState["LoadID"].ToString(),NullCheck(org.FromDate),NullCheck(org.ContactPerson), NullCheck(org.ToDate), org.sequenceNumber,Convert.ToString(Session["UserLoginId"]),NullCheck(org.Pieces),NullCheck(org.Weight),NullCheck(org.AppNum)};
                                    System.Data.SqlClient.SqlParameter[] objsqlparamsorg = null;
                                    objsqlparamsorg = SqlHelper.PrepareSqlParamsFromSP(ConfigurationManager.AppSettings["conString"], "SP_Insert_Org_Des", objParamsorg);
                                    SqlHelper.ExecuteNonQuery(ConfigurationManager.AppSettings["conString"], CommandType.StoredProcedure, "SP_Insert_Org_Des", objsqlparamsorg).ToString();
                                }

                            }
                        if (loadDeatils.Charges != null && !string.IsNullOrWhiteSpace(loadDeatils.Charges.Charge))
                        {
                             object[] objParamsorg = new object[]{ViewState["LoadID"].ToString(),NullCheck(CommonFunctions.RemoveCurencySymbol(loadDeatils.Charges.Charge)),NullCheck(CommonFunctions.RemoveCurencySymbol(loadDeatils.Charges.Advances)), NullCheck(CommonFunctions.RemoveCurencySymbol(loadDeatils.Charges.FreightRate)) };
                                    System.Data.SqlClient.SqlParameter[] objsqlparamsorg = null;
                                    objsqlparamsorg = SqlHelper.PrepareSqlParamsFromSP(ConfigurationManager.AppSettings["conString"], "SP_InsertUpdate_Receivables", objParamsorg);
                                    SqlHelper.ExecuteNonQuery(ConfigurationManager.AppSettings["conString"], CommandType.StoredProcedure, "SP_InsertUpdate_Receivables", objsqlparamsorg).ToString();
                        }
                            //SendInvoiceNumber(loadDeatils.customerName, loadDeatils.orderNumber, ConfigurationManager.AppSettings["InvoiceNumberPrefix"].ToString() + ViewState["LoadID"].ToString());
                            Response.Write("<script>alert('Load saved successully with load number " + ViewState["LoadID"].ToString() + ".')</script>");
                        }
                        objsqlparams = null;
                        objParams = null;

                                        
                }
                else if (content == "0" && btn.CommandName == "REJECT")
                {
                    Response.Redirect("~/Manager/TenderLoads.aspx");
                }
            }
            catch (Exception ex)
            { }
            finally
            {
                Response.Redirect("~/Manager/TenderLoads.aspx");
            }
        }
        else
        {
            Response.Redirect("~/Manager/TenderLoads.aspx");
        }
    }
    
}