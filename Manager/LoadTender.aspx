﻿<%@ Page AutoEventWireup="true" CodeFile="LoadTender.aspx.cs" Inherits="Manager_LoadTender"
    Language="C#" Title="S Line Transport Inc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head  id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Carrier Load Tender</title>
</head>

<body style="background:#FFFFFF; padding:0 0 0 0; margin:0 0 0 0">
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333; font-weight:normal;">
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" valign="top" style="padding:10px 10px 5px 10px;">
              <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:0px; font-size:12px;">
              <tr>
                <td width="25%" align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td align="left""><img src="../Images/logo.jpg"/></td>
                    </tr>
                  </table></td>
                <td align="center" valign="middle" style="padding-bottom:15px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td align="center" style="font-size:24px; font-style:normal; font-weight:bold;">Carrier Load Tender </td>
                    </tr>
                    <tr>
                      <td style="font-size:12px;" align="center"><strong>Carrier</strong> : <asp:Label ID="lblCarrier" runat="server" Text=""></asp:Label></td>
                    </tr>
                  </table></td>
                <td width="25%" align="center" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td align="center" style="font-size:20px; font-style:normal; font-weight:bold; font-style:italic"><asp:Label ID="lblCompanyHedder" runat="server" Text=""></asp:Label></td>
                    </tr>
                    <tr>
                      <td align="center" style="font-size:12px; font-style:normal;"></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td align="left" valign="bottom"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td align="left"><p style="padding:0px; margin:0px; font-size:12px; font-style: normal; text-transform:none; font-weight:bold;">Load #:</p>
                        <p style="padding:0px; margin:0px; font-size:12px; font-style:normal; text-transform:none; font-weight:bold;"><asp:Label ID="lblLoadNumber" runat="server" Text=""></asp:Label></p></td>
                    </tr>
                  </table></td>
                <td align="center" valign="bottom"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td align="center"><p style="padding:0px; margin:0px; font-size:12px; font-style:normal; text-transform:none;"><strong>Vendor</strong> :<asp:Label ID="lblDotlic" runat="server" Text=""></asp:Label><br />
                        </p>
                        <p style="padding:0px; margin:0px; font-size:12px; font-style:normal; text-transform:none;"><strong>Name</strong> :  <asp:Label ID="lblName" runat="server" Text=""></asp:Label> <strong>Phone</strong>: # <asp:Label ID="lblPhone" runat="server" Text=""></asp:Label><br />
                        </p>
                        <p style="padding:0px; margin:0px; font-size:12px; font-style:normal; text-transform:none;"><strong>Email</strong> : <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label> <strong>Fax</strong>: <asp:Label ID="lblFax" runat="server" Text=""></asp:Label> </p></td>
                    </tr>
                  </table></td>
                <td align="center" valign="bottom"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td align="right"><p style="padding:0px; margin:0px; font-size:12px; font-style:normal; text-transform:none; font-weight:bold;"></p>
                        <p style="padding:0px; margin:0px; font-size:12px; font-style:normal; text-transform:none;"><asp:Label ID="lblAssignDate" runat="server" Text=""></asp:Label></p></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td style="border-top:2px solid #555555; border-bottom:2px solid #555555; padding:5px 0px 5px 0px; padding:10px;">The Broker-Carrier Agreement between <asp:Label ID="lblCompany1" runat="server" Text=""></asp:Label>, a licensed Property Broker - USDOT #3194831, and <asp:Label ID="lblCarrier1" runat="server" Text=""></asp:Label>. is amended by the verbal agreement between <asp:Label ID="lblName1" runat="server" Text=""></asp:Label> of <asp:Label ID="lblCompany2" runat="server" Text=""></asp:Label> hereafter referred to as BROKER, and Carrier Tenders - PRENOTE of <asp:Label ID="lblCarrier2" runat="server" Text=""></asp:Label> hereafter referred to as CARRIER, dated <asp:Label ID="lblDate" runat="server" Text=""></asp:Label>. All Van/Container loads MUST be sealed at origin either by shipper or driver with a seal number noted on bill of lading or proof of delivery/Carrier's delivery receipt. The driver is responsible for re-sealing the trailer after each pickup/drop on a multi-stop shipment. In the event a shipment that was sealed at origin or after each additional pickup/drop arrives at the destination with a tampered seal or without the seal intact then (i) the Carrier shall be liable for any shortage or damage claims with respect to such shipment and (ii) the shipper shall have the right, in its sole discretion, to deem the entire shipment damaged, adulterated/contaminated and unsalvageable, without the need for any inspection and the Carrier shall be liable for the full value of the shipment.</td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" valign="top" style="padding:10px; border-bottom:2px solid #555555;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="50%" align="left" valign="top">ORIGIN</th>
                <th align="left" valign="top">DESTINATION</th>
              </tr>
              <tr>
                  
                <td align="left" valign="top"><asp:Label ID="lblOrigin" runat="server" Text=""></asp:Label><%--BNSF Los Angeles Ramp - Los Angeles<br />
                  3770 E, Washington Blvd<br />
                  Los Angeles, CA 90023<br />
                  <strong>Phone</strong> +1(323)267 3246--%><br />
                  <strong>PU #</strong>: <asp:Label ID="lblPickupNum" runat="server" Text=""></asp:Label><br />
                  <strong>Notes</strong> <asp:Label ID="lblDiscription" runat="server" Text=""></asp:Label></td>
                  
                <td align="left" valign="top"><asp:Label ID="lblDestination" runat="server" Text=""></asp:Label><%--USPS LOS ANGELES - Los Angeles<br />
                  7001 S CENTRAL AVE<br />
                  Los Angeles, CA 90054<br />
                  <strong>Phone</strong> +1(323) 586 1868--%><br />
                  <%--<strong>Confirmation#</strong>: <asp:Label ID="lblDesAPPNum" runat="server" Text=""></asp:Label><br />--%>
                  <strong>Notes</strong> 
                  &quot;POD MUST BE TURNED IN WITHIN 48 HOURS OR<br />
                  DETENTION WILL NOT BE HONORED&quot;<br />
                  DETENTION TIMES MUST BE SIGNED ON<br />
                  PAPERWORK. CARRIER PAPERWORK/CARTAGE<br />
                  TICKET NOT ACCEPTABLE.</td>
              </tr>
            </table></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" valign="top" style="padding:10px; border-bottom:1px solid #555555;">
             <%-- <table width="100%" border="0" cellspacing="0" cellpadding="5">--%>
              
                  <asp:Repeater ID="rptPayables" runat="server">
                       <HeaderTemplate>
                <table id="tableCustomer" width="100%" border="0" cellspacing="0" cellpadding="5">
                    <thead>
                        <tr>
                            <th align="left" bgcolor="#CCCCCC" style="font-size:13px;">
                                DESCRIPTION
                            </th>
                            <th align="right" bgcolor="#CCCCCC" style="font-size:13px;">
                                AMOUNT
                            </th>      
                            </tr>
                        </thead>
                   </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td align="left">
                      <asp:Label ID="lblTitle" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Key")%>'></asp:Label>
                       </td>
                <td align="right">
                       $<asp:Label ID="lblValue" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Value")%>'></asp:Label>
                    </td></tr>
                </ItemTemplate>
                      <FooterTemplate>
                </table>
            </FooterTemplate>
                  </asp:Repeater>
              <asp:Label ID="lblNoCharges" runat="server" Text="No charges added"></asp:Label>
              <%--<tr>
                <th width="0" align="left" valign="middle" bgcolor="#CCCCCC">DESCRIPTION</th>
                <th width="20%" align="right" valign="middle" bgcolor="#CCCCCC">AMOUNT</th>
              </tr>
              <tr>
                <td align="left" valign="middle">Flat Rate</td>
                <td align="right" valign="middle">$162.00</td>
              </tr>
              <tr>
                <td align="left" valign="middle">Fuel Surcharge</td>
                <td align="right" valign="middle">$41.31</td>
              </tr>
              <tr>
                <td align="left" valign="middle">Chassis Equipment Lease Charge</td>
                <td align="right" valign="middle">$25.00</td>
              </tr>
              <tr>
                <td align="left" valign="middle">Pier Termination</td>
                <td align="right" valign="middle">$110.00</td>
              </tr>--%>
           <%-- </table>--%></td>
        </tr>
        <tr>
          <td align="left" valign="top" style="padding:10px; border-bottom:1px solid #555555;"><table width="100%" border="0" cellspacing="0" cellpadding="5">
              <tr>
                <td>These amounts to be paid to contact carrier less lawful deductions.</td>
                <td style="font-size:13px;"><strong>TOTAL:</strong></td>
                <td align="right" style="font-size:13px;"><strong>USD</strong> $<asp:Label ID="lblTotal" runat="server" Text=""></asp:Label></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td style="border-top:1px solid #555555; border-bottom:2px solid #555555; padding:5px 0px 5px 0px; padding:10px;"><table width="100%" border="0" cellspacing="0" cellpadding="5">
              <tr>
                <td align="left" valign="top" width="50%"><strong>Pickup #</strong> : <asp:Label ID="lblPuckupNum" runat="server" Text=""></asp:Label></td>
                <td align="left" valign="top"><strong>EQUIPMENT</strong> : <asp:Label ID="lblEquipmentNum" runat="server" Text=""></asp:Label></td>
              </tr>
            </table>
            <table width="100%" border="0" cellpadding="5" cellspacing="0">              
              <tr>
                <td align="left" valign="top"><strong>SEAL</strong> : <asp:Label ID="lblSealNumber" runat="server" Text=""></asp:Label></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td style="border-bottom:2px solid #555555; padding:5px 0px 5px 0px; padding:10px;"><table width="100%" border="0" cellspacing="0" cellpadding="5">
              <tr>
                <td width="50%" align="left" valign="top"><strong>CARRIER </strong>: <asp:Label ID="lblCarrier3" runat="server" Text=""></asp:Label>.</td>
                <td align="left" valign="top"><strong>BROKER</strong> : <asp:Label ID="lblCompany3" runat="server" Text=""></asp:Label></td>
              </tr>
              <tr>
                <td align="left" valign="top"><strong>Fax</strong> : <asp:Label ID="lblCarrierFax" runat="server" Text=""></asp:Label></td>
                <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="40%" height="25" align="left" valign="middle"><strong>Phone</strong> : <asp:Label ID="lblCompanyPhone1" runat="server" Text=""></asp:Label></td>
                      <td height="25" align="left" valign="middle">Rep <asp:Label ID="lblAssignedUser" runat="server" Text=""></asp:Label></td>
                    </tr>
                    <tr>
                      <td height="25" align="left" valign="middle"><strong>Fax</strong> : <asp:Label ID="lblCompanyFax" runat="server" Text=""></asp:Label></td>
                      <td height="25" align="left" valign="middle"></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td style="border-bottom:2px solid #555555; padding:10px; font-size:10px;"><p style="padding:0px 0px 10px 0px; margin:0px">This confirmation is subject to the terms of the master Broker-Carrier agreement and this document constitutes and amendment to the master agreement. If the carrier has not signed a master agreement, then the rate shown above is the agreed individually negotiated rate and no other rate shall apply including any carrier tarrif rate or terms. THIS LOAD SHALL NOT BE DOUBLE BROKERED. No additional charges not listed above may be added by the carrier. Any additional charges must appear on a revised confirmation sheet signed by the broker. Carrier must include signed copy of the shipper's bill of lading and any other proof of delivery with invoice to Broker. Rates, except as specifically designated above, are inclusive of any fuel surcharge. Carrier certifies that it is in compliance with all requirements of the California Air Resources Board (CARB) that are applicable to the scope of Carrier's operations, including, but not limited to: Statewide Truck and Bus Regulations, Transport Refrigeration Unit (TRU) Regulations, Tractor - Trailer Greenhouse(GHG) Gas Regulation, and Drayage Truck Regulations. Carrier also warrants that it is in compliance with any comparable requirements of the Environmental protection Agency(EPA) and other states, where applicable. Carrier shall be responsible for any fines imposed on Broker and / or shipper resulting from noncompliance. CARRIER hereby confirms that it maintains and valid insurance without exclusions that would prevent coverage for the items listed above.</p>
            <p style="padding:0px 0px 5px 0px; margin:0px">CARRIER has at least $100,000.00 in cargo insurance and $1,000,000.00 in automotive liablity coverage. Carrier further confirms that in transporting the shipment described here in above, it will comply with all U.S. DOT and FDA regulations applicable to its operations while transporting said shipment, including, but not limited to driver's hours of service, and the Food Safety Modernization Act (FSMA), if applicable. Carrier agrees to the attached requirements from the shipper, if any. <strong>ALL LOADS ARE SUBJECT TO ELECTRONIC MONITORING</strong></p></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="5" style="font-size:18px;">
        <tr>
          <td width="50%" align="left" valign="top" style="padding-right:60px;"><table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td width="120" height="50" align="left" valign="bottom"><strong>PRINT NAME</strong></td>
                <td height="50" align="left" valign="middle" style="border-bottom:1px solid #333333">&nbsp;</td>
              </tr>
            </table></td>
          <td align="left" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td width="110" height="50" align="left" valign="bottom"><strong>SIGNATURE</strong></td>
                <td height="50" align="left" valign="middle" style="border-bottom:1px solid #333333">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td align="left" valign="top" style="padding-right:60px;"><table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td width="60" height="50" align="left" valign="bottom"><strong>TITLE</strong></td>
                <td height="50" align="left" valign="middle" style="border-bottom:1px solid #333333">&nbsp;</td>
              </tr>
            </table></td>
          <td align="left" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td width="60" height="50" align="left" valign="bottom"><strong>DATE</strong></td>
                <td height="50" align="left" valign="middle" style="border-bottom:1px solid #333333">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center" valign="middle" style="border-bottom:2px solid #555555; padding:5px 0px 5px 0px; padding:10px;"><p style="font-size:16px; font-weight:bold; padding:25px 0px 5px 0px; margin:0px;">Please sign this agreement and fax to <asp:Label ID="lblCompanyFax1" runat="server" Text=""></asp:Label></p>
            <p style="padding:0px; margin:0px;"><strong>Send Invoices to:</strong> <asp:Label ID="lblInvoiceEmail" runat="server" Text=""></asp:Label> <br /> <asp:Label ID="lblCompany4" runat="server" Text=""></asp:Label> is an equal Opportunity Employer</p></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
