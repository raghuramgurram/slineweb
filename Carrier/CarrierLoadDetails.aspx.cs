using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CarrierLoadDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
//        this.Page.Title = Convert.ToString(ConfigurationSettings.AppSettings["CompanyName"]);
        this.Page.Title = GetFromXML.CompanyName;
        if (!IsPostBack)
        {
            if (Request.QueryString.Count > 0 && Session["CarrierId"]!=null)
            {
                try
                {
                    ViewState["LoadId"] = Convert.ToInt64(Request.QueryString[0]);
                    FillDetails(Convert.ToInt64(ViewState["LoadId"]));
                }
                catch
                {
                }
            }
        }
    }
    private void FillDetails(long ID)
    {
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCarrierPaymentsDetails", new object[] { ID, Convert.ToString(Session["CarrierId"]) });
        DataTable dt = ds.Tables[0];
        string BillingTo = "";
        if (dt.Rows.Count > 0)
        {
//            lblAddress.Text = "<strong>" + Convert.ToString(ConfigurationSettings.AppSettings["CompanyName"]) + "</strong><br/>" + Convert.ToString(ConfigurationSettings.AppSettings["ComapanyAddress"]) + "<br/>" + Convert.ToString(ConfigurationSettings.AppSettings["ComapanyState"]) + "<br/>" + Convert.ToString(ConfigurationSettings.AppSettings["CompanyPhone"]) + "<br/>" + Convert.ToString(ConfigurationSettings.AppSettings["CompanyFax"]);

            lblAddress.Text = "<strong>" + GetFromXML.CompanyName + "</strong><br/>" + GetFromXML.ComapanyAddress + "<br/>" + GetFromXML.ComapanyState + "<br/>" + GetFromXML.CompanyPhone + "<br/>" + GetFromXML.CompanyFax;
            lblOrigin.Text = CommonFunctions.FormatMultipleOrigins(Convert.ToString(dt.Rows[0][0]).Trim());
            lblDestination.Text = CommonFunctions.FormatMultipleOrigins(Convert.ToString(dt.Rows[0][1]).Trim());
            if (ds.Tables.Count > 1)
            {
                if (ds.Tables[1].Rows.Count > 0)
                {
                    lblAssigned.Text = Convert.ToString(ds.Tables[1].Rows[0][0]).Trim();
                    lblAssigned.Font.Underline = true;
                    lblDriverOrCarrier.Text = Convert.ToString(ds.Tables[1].Rows[0][1]).Trim();
                    lblCarrier.Text = Convert.ToString(ds.Tables[1].Rows[0][2]);
                    lblTotal.Text = "Total Charges:  $ <b>" + Convert.ToString(ds.Tables[2].Rows[0][0]).Trim() + "</b>";
                }
                if (ds.Tables[3].Rows.Count > 0)
                {
                    lblLoad.Text = lblLoadId.Text = Convert.ToString(ID);
                    lblType.Text = Convert.ToString(ds.Tables[3].Rows[0][0]);
                    lblCommodity.Text = Convert.ToString(ds.Tables[3].Rows[0][1]);
                    lblBooking.Text = Convert.ToString(ds.Tables[3].Rows[0][2]);
                    if (Convert.ToString(ds.Tables[3].Rows[0][3]).Trim().Length == 0)
                        lblContainerId.Text = Convert.ToString(ds.Tables[3].Rows[0][4]);
                    else
                        lblContainerId.Text = Convert.ToString(ds.Tables[3].Rows[0][3]);

                    if (Convert.ToString(ds.Tables[3].Rows[0][5]).Trim().Length == 0)
                        lblChasis.Text = Convert.ToString(ds.Tables[3].Rows[0][6]);
                    else
                        lblChasis.Text = Convert.ToString(ds.Tables[3].Rows[0][5]);

                    lblRemarks.Text = Convert.ToString(ds.Tables[3].Rows[0][7]);
                    lblDescription.Text = Convert.ToString(ds.Tables[3].Rows[0][8]);
                    BillingTo = "<b>" + Convert.ToString(ds.Tables[3].Rows[0][9]) + " </b> <br/>&nbsp;&nbsp;" + Convert.ToString(ds.Tables[3].Rows[0][10]) + "  " + Convert.ToString(ds.Tables[3].Rows[0][11]) + "<br/>&nbsp;&nbsp;";
                    BillingTo += Convert.ToString(ds.Tables[3].Rows[0][12]) + ", " + Convert.ToString(ds.Tables[3].Rows[0][13]) + " " + Convert.ToString(ds.Tables[3].Rows[0][14]);
                    lblPickup.Text = Convert.ToString(ds.Tables[3].Rows[0][16]);
                    string[] str=Convert.ToString(ds.Tables[3].Rows[0][17]).Trim().Split(';');
                    for(int i=0;i<str.Length;i++)
                    {
                        if (i != str.Length - 1)
                        {
                            if (str[i].Length > 0)
                                lblPO.Text = str[i];
                            if (str[i + 1].Length > 0)
                                lblPO.Text += "<br/>";
                        }
                        else
                        {
                            lblPO.Text += str[i];
                        }                        
                    }                    
                }
            }
        }
        if (ds != null) { ds.Dispose(); ds = null; }

        //string strQuery = "SELECT T.[nvar_LoadTypeDesc],CT.[nvar_CommodityTypeDesc],L.[nvar_Booking]," +
        //                  "L.[nvar_Container],L.[nvar_Container1],L.[nvar_Chasis],L.[nvar_Chasis1],L.[nvar_Remarks],L.[nvar_Description],C.[nvar_CustomerName],C.[nvar_Street],C.[nvar_Suite],C.[nvar_City],C.[nvar_State],C.[nvar_Zip]" +
        //                  " FROM [eTn_Load] L inner join [eTn_LoadType] T on L.[bint_LoadTypeId]=T.[bint_LoadTypeId] inner join [eTn_Customer] C on L.[bint_CustomerId]=C.[bint_CustomerId]" +
        //                  "inner join [eTn_CommodityType] CT on L.[bint_CommodityTypeId]=CT.[bint_CommodityTypeId]"+
        //                  "inner join [eTn_AssignCarrier] on L.[bint_LoadId]=[eTn_AssignCarrier].[bint_LoadId]" +
        //            " WHERE L.[bint_LoadId]=" + ID + " and [eTn_AssignCarrier].[bint_CarrierId]=" + Convert.ToInt64(Session["CarrierId"]);
        //dt = DBClass.returnDataTable(strQuery);
        //if (dt.Rows.Count > 0)
        //{
        //    lblLoad.Text=lblLoadId.Text = Convert.ToString(ID);
        //    lblType.Text = Convert.ToString(dt.Rows[0][0]);
        //    lblCommodity.Text = Convert.ToString(dt.Rows[0][1]);
        //    lblBooking.Text = Convert.ToString(dt.Rows[0][2]);
        //    if(Convert.ToString(dt.Rows[0][3]).Trim().Length==0)
        //        lblContainerId.Text = Convert.ToString(dt.Rows[0][4]);
        //    else
        //        lblContainerId.Text = Convert.ToString(dt.Rows[0][3]);

        //    if (Convert.ToString(dt.Rows[0][5]).Trim().Length == 0)
        //        lblChasis.Text = Convert.ToString(dt.Rows[0][6]);
        //    else
        //        lblChasis.Text = Convert.ToString(dt.Rows[0][5]);

        //    lblRemarks.Text = Convert.ToString(dt.Rows[0][7]);
        //    lblDescription.Text = Convert.ToString(dt.Rows[0][8]);
        //    lblBillingTo.Text = Convert.ToString(dt.Rows[0][9]) + "<br/>" + Convert.ToString(dt.Rows[0][10]) + "  " + Convert.ToString(dt.Rows[0][11]) + "<br/>";
        //    lblBillingTo.Text += Convert.ToString(dt.Rows[0][12]) + ", " + Convert.ToString(dt.Rows[0][13]) + " " + Convert.ToString(dt.Rows[0][14]);
        //}

        //lblTotal.Text = "Total Charges:  $" + DBClass.executeScalar("select Isnull([num_Charges],0)+Isnull([num_AdvancedPaid],0)+Isnull([num_DryRun],0)+Isnull([num_FuelSurcharge],0)+Isnull([num_FuelSurchargeAmounts],0)+Isnull([num_PierTermination],0)+Isnull([bint_ExtraStops],0)+Isnull([num_LumperCharges],0)+Isnull([num_DetentionCharges],0)+Isnull([num_ChassisSplit],0)+" +
        //                      "Isnull([num_ChassisRent],0)+Isnull([num_Pallets],0)+Isnull([num_ContainerWashout],0)+Isnull([num_YardStorage],0)+Isnull([num_RampPullCharges],0)+Isnull([num_TriaxleCharge],0)+Isnull([num_TransLoadCharges],0)+Isnull([num_HLScaleCharges],0)+Isnull([num_ScaleTicketCharges],0)+Isnull([num_OtherCharges1],0)+" +
        //                      "Isnull([num_OtherCharges2],0)+Isnull([num_OtherCharges3],0)+Isnull([num_OtherCharges4],0) from [eTn_Payables] WHERE [bint_LoadId]=" + Convert.ToInt64(ViewState["LoadId"]) + " and [bint_PayablePlayerId]=" + Convert.ToInt64(Session["CarrierId"]));

        //lblCarrier.Text = DBClass.executeScalar("Select [nvar_CarrierName] from [eTn_Carrier] where [bint_CarrierId]=" + Convert.ToInt64(Session["CarrierId"]));       
        
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (ViewState["LoadId"] != null)
        {
            Response.Redirect("~/Carrier/CarrierTrackLoad.aspx?" + Convert.ToInt64(ViewState["LoadId"]));
        }
    }    
}
