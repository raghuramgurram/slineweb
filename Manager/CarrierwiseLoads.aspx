<%@ Page AutoEventWireup="true" CodeFile="CarrierwiseLoads.aspx.cs" Inherits="CarrierwiseLoads"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>
                <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td valign="middle">
                            <asp:Label ID="lblText" runat="server" Text="Carrier Name : "></asp:Label>                            
                            <asp:DropDownList ID="ddlCarriers" runat="server">
                            </asp:DropDownList>
                            &nbsp;&nbsp;
                            &nbsp; &nbsp;<asp:Label ID="lblStatus" runat="server" Text="Status : "></asp:Label>&nbsp;<asp:DropDownList
                                ID="dropStatus" runat="server">
                            </asp:DropDownList>
                            &nbsp;
                            &nbsp; &nbsp;<asp:Button ID="btnSearch" runat="server" CssClass="btnStyle" Text="Search" OnClick="btnSearch_Click" />
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>               
            </td>
        </tr>
    </table>
</asp:Content>

