using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class LoadReceivables : System.Web.UI.UserControl
{
    public string LoadId
    {
        set { ViewState["LoadId"] = value; }
    }
    public string CustomerId
    {
        set { ViewState["CustomerId"] = value; }
    }
    public string RedirectBackURL
    {
        set { ViewState["RedirectBackURL"] = value; }
    }
    public bool BackButtonVisible
    {
        get { return pnlButton.Visible; }
        set
        {
            if (value != null)
                pnlButton.Visible = value;
            else
                pnlButton.Visible = true;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ShowLoadChargeDetails();
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            ShowLoadChargeDetails();
        }
    }
    private void ShowLoadChargeDetails()
    {
        if (ViewState["LoadId"] != null)
        {
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCustomerReceivables", new object[] { Convert.ToInt64(ViewState["LoadId"]) });
            if (ds.Tables.Count > 0)
            {
                DesginPaybleDetails(ds.Tables[0]);
            }
            if (ds != null) { ds.Dispose(); ds = null; }
        }
    }
    protected void btnBackPage_Click(object sender, EventArgs e)
    {
        if (ViewState["RedirectBackURL"] != null)
            Response.Redirect(Convert.ToString(ViewState["RedirectBackURL"]));
    }
    public void DesginPaybleDetails(DataTable dtPayables)
    {
        System.Text.StringBuilder strCode = new System.Text.StringBuilder();
        strCode.Append("");
        if (dtPayables.Rows.Count > 0)
        {
            bool blVal = false;            
            for (int i = 0,j = 0; i < dtPayables.Columns.Count; i++)
            {
                if (i == 0 || i == 1 || i == 3 || i == 4)
                    continue;
                #region Validations
                blVal = false;
                if (Convert.IsDBNull(dtPayables.Rows[0][i]))
                    dtPayables.Rows[0][i] = 0;
                if(Convert.ToString(dtPayables.Rows[0][i]).Length==0)
                    dtPayables.Rows[0][i] = 0;
                try
                {
                    if (Convert.ToDouble(dtPayables.Rows[0][i]) > 0)
                    {
                        blVal = true;
                    }
                }
                catch
                {
                    if (Convert.ToString(dtPayables.Rows[0][i]).Trim().Length > 0)
                    {
                        blVal = true;
                    }
                }
                #endregion
                if (blVal)
                {                    
                    strCode.Append("<tr id=" + (((j%2)==0)?"row":"altrow") + " >");
                    if(i < (dtPayables.Columns.Count-1))
                        strCode.Append("<td align=right width=30%>" + dtPayables.Columns[i].ColumnName + "</td>");
                    else
                        strCode.Append("<td align=right width=30%><b>" + dtPayables.Columns[i].ColumnName + "</b></td>");
                    strCode.Append("<td align=left>" + Convert.ToString(dtPayables.Rows[0][i]).Trim() + "</td>");
                    strCode.Append("</tr>");
                    j = j + 1;
                }                
                #region Old
                //if (i == 0 || i == 3)
                //    continue;
                //strCSS = "row";
                //if (i == 0 || i == 3)
                //{
                //    continue;
                //    strCode.Append("<tr id=" + strCSS.Trim() + " >");
                //    strCode.Append("<td align=right width=30%>" + dtPayables.Columns[i].ColumnName + "</td>");
                //    strCode.Append("<td align=left>" + Convert.ToString(dtPayables.Rows[0][i]).Trim() + "</td>");
                //    strCode.Append("</tr>");
                //    if (strCSS.Trim() == "row")
                //        strCSS = "altrow";
                //    else if (strCSS.Trim() == "altrow")
                //    {
                //        strCSS = "row";
                //    }
                //}
                //else if (i == dtPayables.Columns.Count - 2)
                //{
                //    if (Convert.ToString(dtPayables.Rows[0][i]).Length > 0)
                //    {
                //        strCode.Append("<tr id=" + strCSS.Trim() + " >");
                //        strCode.Append("<td align=right>" + dtPayables.Columns[i].ColumnName + "</td>");
                //        strCode.Append("<td align=left>" + Convert.ToString(dtPayables.Rows[0][i]).Trim() + "</td>");
                //        strCode.Append("</tr>");
                //        if (strCSS.Trim() == "row")
                //            strCSS = "altrow";
                //        else if (strCSS.Trim() == "altrow")
                //        {
                //            strCSS = "row";
                //        }
                //    }
                //}
                //else if (Convert.ToString(dtPayables.Rows[0][i]).Length > 0 && Convert.ToDouble(dtPayables.Rows[0][i]) != 0)
                //{
                //    strCode.Append("<tr id=" + strCSS.Trim() + " >");
                //    if (i < dtPayables.Columns.Count - 1)
                //    {
                //        strCode.Append("<td align=right width=30%>" + dtPayables.Columns[i].ColumnName + "</td>");
                //        strCode.Append("<td align=left>" + Convert.ToString(dtPayables.Rows[0][i]).Trim() + "</td>");
                //    }
                //    else if (i == dtPayables.Columns.Count - 1)
                //    {
                //        strCode.Append("<td align=right width=30%><strong>" + dtPayables.Columns[i].ColumnName + "</strong ></td>");
                //        strCode.Append("<td align=left><strong>" + Convert.ToString(dtPayables.Rows[0][i]).Trim() + "</strong ></td>");
                //    }
                //    strCode.Append("</tr>");
                //    if (strCSS.Trim() == "row")
                //        strCSS = "altrow";
                //    else if (strCSS.Trim() == "altrow")
                //    {
                //        strCSS = "row";
                //    }
                //}
                #endregion                
            }
            if (strCode.ToString().Length > 0)
            {
                strCode.Insert(0, "<table width=100% border=0 cellspacing=0 cellpadding=0 id=tblform> <tr><th colspan=2>Load Charges($)</th></tr>");
                strCode.Append("</table>");
            }
            else
            {
                strCode.Append("<table width=100% border=0 cellspacing=0 cellpadding=0 id=tblform> <tr><th colspan=2>Load Charges($)</th></tr>");
                strCode.Append("<tr id=row>");
                strCode.Append("<td align=center width=30%><b>No Charges Available for Load : " + Convert.ToString(ViewState["LoadId"]) + " </b></td>");
                strCode.Append("</tr>");
                strCode.Append("</table>");
            }
            lblDisplay.Text = strCode.ToString();
            strCode = null;
        }
        else
        {
            strCode.Append("<table width=100% border=0 cellspacing=0 cellpadding=0 id=tblform> <tr><th colspan=2>Load Charges($)</th></tr>");
            strCode.Append("<tr id=row>");
            strCode.Append("<td align=center width=30%><b>No Charges Available for Load : " + Convert.ToString(ViewState["LoadId"]) + " </b></td>");
            strCode.Append("</tr>");
            strCode.Append("</table>");
            lblDisplay.Text = strCode.ToString();
            strCode = null;
        }
    }
}
