<%@ Page Language="C#" MasterPageFile="~/Customer/CustomerMaster.master" AutoEventWireup="true" CodeFile="CustomerInvoice.aspx.cs" Inherits="CustomerInvoice" Title="S Line Transport Inc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td style="width: 952px"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center" class="head1">Invoice#&nbsp;<asp:Label ID="lblInvoiceLoad" runat="server" Text=""/></td>
      </tr>
    </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="../images/pix.gif" alt="" width="1" height="10" /></td>
          </tr>
        </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td bgcolor="#CCCCCC"><img src="../images/pix.gif" alt="" width="1" height="1" /></td>
          </tr>
        </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="../images/pix.gif" alt="" width="1" height="10" /></td>
          </tr>
        </table>
          <table border="0" cellpadding="0" cellspacing="0" style="padding-right: 5px; padding-left: 15px;
            height: 20px; padding-top: 1px; padding-bottom: 1px;" width="100%">
            <tr>
              <td>
             <table border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, sans-serif;
                color: #4D4B4C; font-size: 12px;" width="100%" id="tblform1">
                  <tr>
                    <td width="30%" valign="top" style="height: 80px"><p><strong>Bill To:</strong><br />
                    <asp:Label ID="lblBillTo" runat="Server" Text="Bill To" /><br />
                      Billing Ref.#: <asp:Label ID="lblBillingRef" runat="Server" Text="Billing Ref.#" /></p></td>
                    <td valign="top" style="height: 80px">Date :  <asp:Label ID="lblDate" runat="Server" /><br />
                      Load# :  <asp:Label ID="lblLoad" runat="Server" /><br />
                      Container# :  <asp:Label ID="lblContainer" runat="Server" /><br />
                        Chasis#:<asp:Label ID="lblChasis" runat="server"></asp:Label><br />
                      Description :  <asp:Label ID="lblDescription" runat="Server" /><br />
                    Terms :  <asp:Label ID="lblTerms" runat="Server" Text="Due on Receipt" /></td>
                  </tr>
                </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td><img src="../images/pix.gif" alt="" width="1" height="10" /></td>
                    </tr>
                  </table>
                <%--<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td bgcolor="#CCCCCC"><img src="../images/pix.gif" alt="" width="1" height="1" /></td>
                    </tr>
                  </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td><img src="../images/pix.gif" alt="" width="1" height="10" /></td>
                    </tr>
                  </table>--%>
               <%-- <table border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, sans-serif;
                  color: #4D4B4C; font-size: 12px;" width="100%">
                <tr>
                  <td valign="top">Dear Customer,<br />
                  Our company request authorization for the additional charges. please sign below and fax it back to us asap.</td>
                </tr>
              </table>--%>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td><img src="../images/pix.gif" alt="" width="1" height="10" /></td>
                    </tr>
                  </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td bgcolor="#CCCCCC"><img src="../images/pix.gif" alt="" width="1" height="1" /></td>
                    </tr>
                  </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td><img src="../images/pix.gif" alt="" width="1" height="10" /></td>
                    </tr>
                </table>
             <table cellpadding="0" cellspacing="0" border="0" style="width: 909px" >
                <tr>
                <td style="width:55%;">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblform" style="width:100%;height:100%">
                    <tr>
                      <th colspan="2" style="height: 23px">Description</th>                      
                    </tr>
                    <tr style="padding-left:10px;padding-top:20px;">
                        <td width="25%" valign="top" align="left">
                                <strong>Origin</strong>:<br />
                                 <asp:Label ID="lblOrigin" runat="server" Text="Origin Address" />
                        </td>
                        <td width="25%" valign="top" align="left">
                                <strong>Destination:</strong>:<br />
                                 <asp:Label ID="lblDestination" runat="server" Text="Destination Address" />                                 
                        </td>
                       </tr>                                       
                  </table>
                 </td>
                 <td style="width:1%;height:100%">
                 <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td><img src="../images/pix.gif" alt="" width="1" height="10" /></td>
                    </tr>
                </table>
                </td>
                 <td style="width: 44%">
                  <table border="0" cellpadding="0" cellspacing="0" id="tblform" style="width:100%;height:100%">
                    <tr>
                        <th colspan="2" style="height: 23px">Amount</th>
                    </tr>
                    <tr>
                    <td valign="top" align="left" style="width: 51%">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lblSacript" runat="server" Width="100%"></asp:Label>
                                </td>
                            </tr>
                          <%--<tr id="row1">
                             <td width="50%" valign="top" align="right">
                                 <strong>Rate :</strong> 
                             </td>
                             <td valign="top"><strong><asp:Label ID="lblAmount" runat="server" /></strong></td>
                          </tr>
                          <tr id="altrow1">                      
                             <td align="right">Fuel Surcharge:
                             </td>
                             <td><asp:Label ID="lblFuelCharges" runat="server" Text="0" /></td>
                          </tr>
                          <tr id="row1">
                            <td width="25%" align="right">
                            <asp:Label ID="lblChasisSplit1" runat="server" Text="Chasis Split :" />
                            </td>
                            <td><asp:Label ID="lblChargesSplit" runat="server" Text="" /></td>
                          </tr>
                           <tr id="altrow1">
                            <td align="right" ><asp:Label ID="lblChasisRent1" runat="server" Text="Chasis Rent:" /></td>
                            <td ><asp:Label ID="lblChasisRent" runat="server" Text="" />
                            </td>
                           </tr>
                            <tr id="row1">
                             <td align="center" colspan="2" >
                             <asp:Label ID="lblOtherChargesReason1" runat="server" Text="Other Charges Reason:" />
                                 <asp:Label ID="lblRemarks" runat="server" Text="" />                                 
                                 <asp:Label ID="lblYardPull1" runat="server" Text="Yard Pull:" />
                                 <asp:Label ID="lblYardPull" runat="server" Text="" />
                                 <asp:Label ID="lblYardStorage1" runat="server" Text="Yard Storage:" />
                                 <asp:Label ID="lblYardStorage" runat="server" Text="" /> </td>
                            </tr>
                             <tr id="altrow1">
                                <td align="right" ><strong>Balance Due  : </strong></td>
                                <td ><strong><asp:Label ID="lblBalance" runat="server" Width="67px" >0</asp:Label></strong></td>
                            </tr>--%>
                        </table>
                    </td>
                    </tr>
                  </table>
                  </td>                                           
               </tr>           
            </table>            
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td><img src="../images/pix.gif" alt="" width="1" height="10" /></td>
                    </tr>
                  </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td bgcolor="#CCCCCC"><img src="../images/pix.gif" alt="" width="1" height="1" /></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><img src="../images/pix.gif" alt="" width="1" height="10" /></td>
                  </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, sans-serif;
                  color: #4D4B4C; font-size: 12px;" width="100%">
                  <tr>
                    <td width="25%"><strong>Please make check payable to : </strong><br /><asp:Label ID="lblCompanyAddress" runat="server" /></td>
                    <td width="25%" valign="top"><strong>Thank you for your business.<br />
Please note that late charges may apply after 30 days. </strong></td>
                    <td valign="top">&nbsp;</td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><img src="../images/pix.gif" alt="" width="1" height="10" /></td>
                  </tr>
                </table>                
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                      <tr>
                          <td align="right" valign="middle">
                              <asp:Button ID="btnBack" runat="server" CausesValidation="false" CssClass="btnstyle"
                                  Text="Back" Visible="true" OnClick="btnBack_Click" />
                          </td>
                      </tr>
                  </table>
                </td>
            </tr>
          </table>
     </td>
  </tr>
</table>
</asp:Content>

