﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class InvoiceProcessing : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
      Session["TopHeaderText"] = "Invoice Processing";

        if (!IsPostBack)
        {
            FillLists();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Gets all the records
        if (ddlIsVerified.SelectedValue == "0")
        {
            Session["ReportParameters"] = ddlMonth.SelectedValue + "~~^^^^~~" + ddlYear.SelectedValue + "~~^^^^~~" + "All";
            Session["SerchCriteria"] = "All Loads";
        }

        //Gets Only Verified records
        else if (ddlIsVerified.SelectedValue == "1")
        {
            Session["ReportParameters"] = ddlMonth.SelectedValue + "~~^^^^~~" + ddlYear.SelectedValue + "~~^^^^~~" + "Verified";
            Session["SerchCriteria"] = "Verified Loads";
        }
        //Gets Only Not Verified records
        else if (ddlIsVerified.SelectedValue == "2")
        {
            Session["ReportParameters"] = ddlMonth.SelectedValue + "~~^^^^~~" + ddlYear.SelectedValue + "~~^^^^~~" + "NotVerified";
            Session["SerchCriteria"] = "Not Verified Loads";
        }

        Session["ReportName"] = "SP_GetReportInvoiceProcessing";
        Response.Redirect("~/Manager/DisplayInvoiceProcessingReport.aspx");
    }

    private string getLongName(int key)
    {
        string[] Months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
        return Months[key - 1];
    }

    private void FillLists()
    {
        ddlMonth.SelectedValue = DateTime.Today.Month.ToString();
        int currentYear = DateTime.Today.Year;
        for (int i = 8; i >= 0; i--)
        {
            ddlYear.Items.Add((currentYear - i).ToString());
        }
        ddlYear.Items.Add((currentYear + 1).ToString());
        ddlYear.SelectedValue = currentYear.ToString();
    }
}