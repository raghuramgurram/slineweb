using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class UserDayLogs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "User Logs";
        if (!Page.IsPostBack)
        {
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "Get_All_Users", new object[] { });
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                ddlUsers.DataSource = ds.Tables[0];
                ddlUsers.DataTextField = "Name";
                ddlUsers.DataValueField = "Id";
                ddlUsers.DataBind();
            }
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            lblDisplay.Text=string.Empty;
            lblSearchCriteria.Text = string.Empty;
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetUserLogsByUserDay", new object[] { ddlUsers.SelectedValue, dtpDate.Date});
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                dgUserLogDetails.DataSource = ds.Tables[0];
                dgUserLogDetails.DataBind();                
                
            }
            else
            {

                //Session["SerchCriteria"] = null;
                //Session["ReportParameters"] = null;
                //Session["ReportName"] = null;
                //Session["ReportParameters"] = dtpFromDate.Date + "~~^^^^~~" + dtpToDate.Date;
                //Session["SerchCriteria"] = " From Date : " + dtpFromDate.Date + "&nbsp;&nbsp;&nbsp; To Date : " + dtpToDate.Date;
                //Session["ReportName"] = "GetUserLogs";
                //Response.Redirect("~/Manager/DisplayReport.aspx");
                dgUserLogDetails.DataSource = ds.Tables[0];
                dgUserLogDetails.DataBind();
                lblDisplay.Text = "<table width=80% border=0 cellpadding=0 cellspacing=0 id=ContentTbl><tr valign=top><td>" +
                    "<table border=0 width=100% height=200px border=0 cellpadding=0 cellspacing=0 align=center valign=middle><tr><td width=100% align=center valign=middle>" +
                    "<b><font color=blue>No Records To Display.<font></b></td></tr></table><br/><br/><br/></td></tr></table>";
            }
            lblSearchCriteria.Text = " User Name : " + ds.Tables[1].Rows[0][1].ToString() + "&nbsp;&nbsp;&nbsp; Role : " + ds.Tables[1].Rows[0][2].ToString() + "&nbsp;&nbsp;&nbsp; Selected Date : " + dtpDate.Date;
        }
    }
}
