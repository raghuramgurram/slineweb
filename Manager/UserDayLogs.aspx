<%@ Page AutoEventWireup="true" CodeFile="UserDayLogs.aspx.cs" Inherits="UserDayLogs" Language="C#"
    MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="tblform1">
                    <tr>
                        <td valign="middle">
                            <asp:Label ID="lbldate" runat="server" Text="Select Date : "></asp:Label>
                            <uc1:DatePicker ID="dtpDate" runat="server" IsDefault="false" IsRequired="false" CountNextYears="4" CountPreviousYears="0" SetInitialDate="true" />
                            &nbsp; &nbsp;&nbsp; &nbsp;<asp:Label ID="lblUser" runat="server" Text="Select User : "></asp:Label>
                            <asp:DropDownList ID="ddlUsers" runat="server"></asp:DropDownList>
                            &nbsp; &nbsp;
                            <asp:Button ID="btnSearch" runat="server" CssClass="btnstyle" Text="Search" OnClick="btnSearch_Click" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                    <td>
                     <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                    <tr>
                    <td>&nbsp;</td>
                    </tr>
                    <tr>
            <td style="height: 4px" valign="top">
                &nbsp;<asp:Label ID="lblSearchCriteria" runat="server" Font-Bold="True" ForeColor="Blue"
        Style="font-family: Arial, sans-serif; font-size: 12px;">
    </asp:Label>
                    </td></tr>
                    <tr>
                    <td>&nbsp;</td>
                    </tr>
                    <tr  valign="top"><td>   
                     
        <asp:DataGrid ID="dgUserLogDetails" Width="80%" runat="server" CssClass="Grid" AutoGenerateColumns="false">          
        <ItemStyle CssClass="GridItem" BorderColor="White" /><AlternatingItemStyle CssClass="GridAltItem" />         
        <HeaderStyle CssClass="GridHeader" BorderColor="White" />
         <Columns>
        <asp:BoundColumn DataField="ActionDone" HeaderText="Action Done"></asp:BoundColumn>
        <asp:BoundColumn DataField="Time" HeaderText="Time" ></asp:BoundColumn>
        <asp:HyperLinkColumn datatextfield="LoadId" HeaderText="Load"
            DataNavigateUrlField="LoadId" DataNavigateUrlFormatString="~\Manager\LoadLogs.aspx?{0}"
            target="_blank" />
        <asp:BoundColumn DataField="ToNumber" HeaderText="To Number" ></asp:BoundColumn>        
        </Columns>            
    </asp:DataGrid>
    <asp:Label ID="lblDisplay" runat="server"></asp:Label>
                    </td></tr>
                </table>
                    </td>
                    <td></td></tr>
                </table>
            </td>
        </tr>
    </table>   
</asp:Content>

