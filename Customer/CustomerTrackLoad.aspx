<%@ Page AutoEventWireup="true" CodeFile="CustomerTrackLoad.aspx.cs" Inherits="CustomerTrackLoad"
    Language="C#" MasterPageFile="~/Customer/CustomerMaster.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/Grid.ascx" TagName="Grid" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td style="width: 953px">
                <table id="tblbox1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                        </td>
                        <td align="right">
                            Track Load :
                            <asp:TextBox ID="txtContainer" runat="server">
                            </asp:TextBox>
                            <asp:DropDownList ID="ddlContainer" runat="server">
                                <asp:ListItem Selected="True" Value="1">Container#</asp:ListItem>
                                <asp:ListItem Value="2">Ref#</asp:ListItem>
                                <asp:ListItem Value="3">Booking#</asp:ListItem>
                                <asp:ListItem Value="4">Load#</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Button ID="btnSearch" runat="server" CssClass="btnstyle"
                                Text="Search" OnClick="btnSearch_Click" />
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" style="width: 30%; height: 777px;" valign="top">
                            <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0"
                                width="100%">
                                <tr>
                                    <th colspan="2">
                                        Identification -&nbsp;<asp:Label ID="lblLoadId" runat="server" Text="LoadId" style="width:25%;"></asp:Label>
                                          <span id="spanishazmat" runat="server" style="color:Red;text-align:right;width:35%">HAZMAT</span>
                                        </th>
                                </tr>
                                <tr id="row">
                                    <td align="right" width="40%" style="height: 20px">
                                        Ref.# : &nbsp;
                                    </td>
                                    <td width="60%" style="height: 20px">
                                        <asp:Label ID="lblRef" runat="server"></asp:Label>&nbsp;<br />
                                        <asp:LinkButton ID="lnkAccessorialCharges" runat="server" Text="Accessorial Charges"></asp:LinkButton></td>                                        
                                        </tr>                               
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>
                            <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <th colspan="2">
                                        Order Status
                                    </th>
                                </tr>
                                <tr id="row">
                                    <td align="right" width="40%">
                                        Load Status :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblStatus" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        Container Size/Location :</td>
                                    <td>
                                        <asp:Label ID="lblRailBill" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="row">
                                    <td align="right">
                                        Order Bill Date :</td>
                                    <td>
                                        <asp:Label ID="lblBillDate" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        Person Bill :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPersonBill" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="row">
                                    <td align="right">
                                        Hot Shipment :
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkHotShipment" runat="server" OnCheckedChanged="chkHotShipment_CheckedChanged"
                                            AutoPostBack="true"></asp:CheckBox>&nbsp;
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>
                            <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <th colspan="2">
                                        Routing</th>
                                </tr>
                                <tr id="row">
                                    <td colspan="2" style="height: 75px">
                                        <%--<strong>RAIL LINE-HAUL </strong><br />
                      EXEL TRANSPORTATION SERVICES, INC.<br />
                      P O BOX 844711 <br />
                      DALLAS , TX 75284-4650 <br />
                      <a href="#">www.tidework.com</a>--%>
                                        <asp:Label ID="lblRoute" runat="server" Text=""></asp:Label>&nbsp;
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" width="45%">
                                        Pier Termination / Empty Return:</td>
                                    <td>
                                        <asp:Label ID="lblPier" runat="server" Text=""></asp:Label>&nbsp;
                                    </td>
                                </tr>
                                <tr id="row" valign="top">
                                    <td align="right">
                                        Contact Person(s) :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblShippingContact" runat="server" Text=""></asp:Label>&nbsp;</td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                       <table width="100%" border="0" cellpadding="0" cellspacing="0" id="maroonBar">
                                        <tr>
                                            <td>
                                                Documents
                                        <%--<uc1:GridBar ID="GridBar1" runat="server" HeaderText="Documents"/>--%>
                                            </td>
                                        </tr>
                            </table>
                                    </td>
                                </tr>
                            </table>
                            <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <th style="width: 119px">
                                        Document Type
                                    </th>
                                    <th>
                                        Date Received
                                    </th>
                                </tr>                          
                                <tr id="row">
                                    <td align="right" style="width: 119px">
                                        <asp:LinkButton ID="lnlPoOrder" runat="server" OnClick="lnlPoOrder_Click">Purchase Order</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPOrder" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" style="width: 119px">
                                        <asp:LinkButton ID="lnkLading" runat="server" OnClick="lnkLading_Click">Bill Of Lading</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblBillofLading" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="row">
                                    <td align="right" style="width: 119px">
                                        <asp:LinkButton ID="lnkAccessorial" runat="server" OnClick="lnkAccessorial_Click">Accessorial Charges</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblAccessorial" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" style="width: 119px">
                                        <asp:LinkButton ID="lnkOutgate" runat="server" OnClick="lnkOutgate_Click">Out-Gate Interchange</asp:LinkButton></td>
                                    <td>
                                        <asp:Label ID="lblOutGate" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="row">
                                    <td align="right" style="width: 119px">
                                        <asp:LinkButton ID="lnkInGate" runat="server" OnClick="lnkInGate_Click">In-Gate Interchange</asp:LinkButton></td>
                                    <td>
                                        <asp:Label ID="lblInGate" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" style="width: 119px">
                                        <asp:LinkButton ID="lnkProof" runat="server" OnClick="lnkProof_Click">Proof of Delivery</asp:LinkButton></td>
                                    <td>
                                        <asp:Label ID="lblDelivery" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="row">
                                    <td align="right" style="width: 119px">
                                        <asp:LinkButton ID="lnkMisc" runat="server" OnClick="lnkMisc_Click">Misc. / Scale Ticket</asp:LinkButton></td>
                                    <td>
                                        <asp:Label ID="lblTicket" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>
                            <asp:PlaceHolder ID="plhDocs" runat="server"></asp:PlaceHolder>
                        </td>
                        <td align="left" style="width: 1%; height: 777px;" valign="top">
                            <img alt="" height="1" src="../images/pix.gif" width="5" /></td>
                        <td align="left" valign="top" style="height: 777px">
                            <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <%--<th colspan="4">
                                        Order Identification
                                    </th>--%>
                                    <td colspan="4">
                                         <table width="100%" border="0" cellpadding="0" cellspacing="0" id="goldBar">
                                          <tr>
                                            <td align="left">
                                                <strong>Order Identification</strong>
                                            </td>
                                          </tr>
                                        </table> 
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td align="right" valign="middle" width="20%">
                                        Customer :
                                    </td>
                                    <td colspan="3" valign="middle">
                                        <asp:Label ID="lblCustomer" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" valign="middle">
                                        Pickup# :
                                    </td>
                                    <td valign="middle" width="25%">
                                        <asp:Label ID="lblPickUp" runat="server"></asp:Label>&nbsp;</td>
                                    <td align="right">
                                        Commodity :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblCommodity" runat="server" Text=""></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="row">
                                    <td align="right" valign="middle">
                                        Container# :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblContainer" runat="server"></asp:Label>&nbsp;</td>
                                    <td align="right" valign="middle">
                                        Seal# :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblSeal" runat="server" Text=""></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" valign="middle">
                                        Chasis# :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblChasis" runat="server" Text=""></asp:Label>&nbsp;</td>
                                    <td align="right" valign="middle">
                                        Last free date :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblLastfreedate" runat="server" Text=""></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="row">
                                    <td align="right" valign="middle">
                                        Pieces :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblPieces" runat="server"></asp:Label>&nbsp;
                                    </td>
                                    <td align="right" valign="middle">
                                       &nbsp; &nbsp;Booking# :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblBooking" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" valign="middle">
                                        Weight :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblWeigth" runat="server"></asp:Label>&nbsp;</td>
                                    <td align="right" valign="middle">
                                     &nbsp;
                                    </td>
                                    <td valign="middle">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td align="right" valign="middle">
                                        Create Person :
                                    </td>
                                    <td valign="top">
                                        <asp:Label ID="lblPersong" runat="server"></asp:Label>
                                    <td align="right" valign="middle">
                                     Created :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblCreated" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="orangeBar">
                                          <tr>
                                            <td align="left">
                                                <strong>Origin(s)</strong>
                                            </td>
                                          </tr>
                                        </table>  
                                        <uc2:Grid ID="Grid1" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tealBar">
                                          <tr>
                                            <td align="left">
                                                <strong>Destination(s)</strong>
                                            </td>
                                          </tr>
                                        </table> 
                                        <uc2:Grid ID="Grid2" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>                        
                            <%--<table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="right">
                                        <uc1:GridBar ID="GridBar2" runat="server" HeaderText="Load Status Information" />
                                    </td>
                                </tr>
                            </table>--%>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tealBar">
                                <tr>
                                    <td align="left">
                                        Notes
                                        <%--                    <uc1:GridBar ID="GridBar3" runat="server" HeaderText="Notes"/>
--%>
                                    </td>
                                    <td align="right">
                                        <asp:LinkButton ID="lnkNotes" runat="server">Add Note</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>                                        
                                        <uc2:Grid ID="Grid5" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>
                            <%--<table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="right">
                                        <uc1:GridBar ID="GridBar3" runat="server" HeaderText="Notes" />
                                    </td>
                                </tr>
                            </table>--%>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="yellowBar">
                                <tr>
                                    <td align="left">
                                        Load Status Information
                                        <%--<uc1:GridBar ID="GridBar2" runat="server" HeaderText="Load Status Information"/>--%>
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>                                        
                                        <uc2:Grid ID="Grid4" runat="server" PageSize="20"/>
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="violetBar">
                                <tr>
                                    <td align="left">
                                        Rail/Equipment Tracing
                                    </td>
                                    <td align="right">
                                        <asp:LinkButton ID="lnkRailNote" runat="server">Add Rail/Equipment Note</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                            
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        
                                        <uc2:Grid ID="GridRailNotes" runat="server"/>
                                    </td>
                                </tr>
                            </table>    
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>
                            
                           <%-- <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                    <td align="right">
                                        <uc1:GridBar ID="BarRailNotes" runat="server" HeaderText="Rail / Equipment Notes" />
                                    </td>
                                </tr>
                                         </table>
                                        <uc2:Grid ID="GridRailNotes" runat="server"/>
                                    </td>
                                </tr>
                            </table>   --%>                                            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

