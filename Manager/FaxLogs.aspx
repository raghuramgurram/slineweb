<%@ Page AutoEventWireup="true" CodeFile="FaxLogs.aspx.cs" Inherits="FaxLogs" Language="C#"
    MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>
             <table border="0" cellpadding="0" cellspacing="0" width="100%" id="tblform1">
                    <tr>
                        <td valign="middle">
                            <asp:Label ID="lblFromdate" runat="server" Text="From : "></asp:Label>
                            <uc1:DatePicker ID="dtpFromDate" runat="server" IsDefault="false" IsRequired="false" CountNextYears="2" CountPreviousYears="0" SetInitialDate="true" />
                            &nbsp; &nbsp;&nbsp; &nbsp;<asp:Label ID="lblToDate" runat="server" Text="To : "></asp:Label>
                            <uc1:DatePicker ID="dtpToDate" runat="server" IsDefault="false" IsRequired="false" CountNextYears="2" CountPreviousYears="0" SetInitialDate="true" />
                            &nbsp; &nbsp;
                            <asp:Button ID="btnSearch" runat="server" CssClass="btnstyle" Text="Search" OnClick="btnSearch_Click" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle"></td>
                        <td>   
                        &nbsp;        
                        </td>
                    </tr>
                   
                    <tr><td colspan="2">
                      <asp:DataGrid AutoGenerateColumns="false" ID="dggrid" Width="100%" runat="server" AllowPaging="true" OnPageIndexChanged="dggrid_PageIndexChanged"  CssClass="Grid" PageSize="10" AllowCustomPaging="true">          
        <PagerStyle Mode="NumericPages" HorizontalAlign="Center"/>
        <ItemStyle CssClass="GridItem" BorderColor="White" />
        <HeaderStyle CssClass="GridHeader" BorderColor="White" />
        <AlternatingItemStyle CssClass="GridAltItem" />
        <Columns>
        <%--<asp:TemplateColumn HeaderText="SMS Id">
            <ItemTemplate>
            <asp:LinkButton id="lbnSMSId" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "bint_SMSId")%>' OnClick="lbnSMSId_Click"></asp:LinkButton>               
            </ItemTemplate>
        </asp:TemplateColumn>--%>
        <asp:BoundColumn HeaderText="Transaction Id" DataField="int_TransactionId"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Load Number" DataField="bint_LoadId"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="To Number" DataField="nvar_ToFaxNumber"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="User" DataField="bint_UserLoginId"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Send Date Time" DataField="date_SendDateTime"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Description" DataField="nvar_Description"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Completion Time" DataField="date_CompletionTime"></asp:BoundColumn>         
        <asp:BoundColumn HeaderText="Destination Fax" DataField="nvar_DestinationFax"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Duration" DataField="int_Duration"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Pages Sent" DataField="sint_PagesSent"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Pages Submitted" DataField="sint_PagesSubmitted"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Remote CSID" DataField="nvar_RemoteCSID"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Status" DataField="int_Status"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Submit Time" DataField="date_SubmitTime"></asp:BoundColumn>        
        </Columns></asp:DataGrid>
                    </td></tr>
                    </table>
            </td>
        </tr>
    </table>  
</asp:Content>