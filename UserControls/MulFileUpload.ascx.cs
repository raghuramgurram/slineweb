using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.IO;

public partial class MulFileUpload : System.Web.UI.UserControl
{
    public string LoadID
    {
        set { ViewState["LoadID"] = value.Trim(); }
    }
    public string DocumnetID
    {
        set { ViewState["DocumnetID"] = value.Trim(); }
    }
    public string Name
    {
        set { ViewState["SessionName"] = value.Trim(); }
    }
    public string BrowseLabelName
    {
        set { lblBrowseFile.Text = value.Trim(); }
    }
    public string[] PostedFileNames
    {
        get
        {
            if (Session[Convert.ToString(ViewState["SessionName"])] != null)
            {
                List<HttpPostedFile> lstTemp = (List<HttpPostedFile>)Session[Convert.ToString(ViewState["SessionName"])];
                string[] strPostedFileNames = new string[lstTemp.Count];
                string[] strTemp = null;
                for (int i = 0; i < lstTemp.Count; i++)
                {
                    strTemp = lstTemp[i].FileName.Trim().Split('\\');
                    strPostedFileNames[i] = strTemp[strTemp.Length - 1];
                    strTemp = null;
                }
                lstTemp = null;
                return strPostedFileNames;
            }
            else
                return null;
        }
    }
    //public string Name
    //{
    //    set { ViewState["SessionName"] = value.Trim(); }
    //}
    //public string[] FullFileNames
    //{
    //    get
    //    {
    //        string[] strFileNames = new string[lstFileNames.Items.Count];
    //        for (int i = 0; i < lstFileNames.Items.Count; i++)
    //            strFileNames[i] = lstFileNames.Items[i].Value.Trim();
    //        return strFileNames;
    //    }
    //}
    //public string[] FileNames
    //{
    //    get
    //    {
    //        string[] strFileNames = new string[lstFileNames.Items.Count];
    //        for (int i = 0; i < lstFileNames.Items.Count; i++)
    //            strFileNames[i] = lstFileNames.Items[i].Text.Trim();
    //        return strFileNames;
    //    }
    //}
    //public int FileListCount
    //{
    //    get { return lstFileNames.Items.Count; }
    //}
    //public List<HttpPostedFile> PostedFiles
    //{
    //    get
    //    {
    //        if (Session[Convert.ToString(ViewState["SessionName"])] != null)
    //            return (List<HttpPostedFile>)Session[Convert.ToString(ViewState["SessionName"])];

    //        else
    //            return null;
    //    }
    //    set { Session[Convert.ToString(ViewState["SessionName"])] = null; }
    //}
    //public string Path
    //{

    //    set { ViewState["CurrentPath"] = value.Trim(); }
    //    get { return Convert.ToString(ViewState["CurrentPath"]).Trim(); }

    //}

    protected void Page_Load(object sender, EventArgs e)
    {        
    }
    protected void btnAddToList_Click(object sender, EventArgs e)
    {
        if (FileUpload.PostedFile.FileName.Trim().Length > 0)
        {
            if (!lstFileNames.Items.Contains(new ListItem(FileUpload.FileName.Trim(), FileUpload.PostedFile.FileName.Trim())))
            {               
                if (Session[Convert.ToString(ViewState["SessionName"])] == null)
                {
                    List<HttpPostedFile> PostedFile = new List<HttpPostedFile>();
                    PostedFile.Add(FileUpload.PostedFile);
                    Session[Convert.ToString(ViewState["SessionName"])] = PostedFile;
                    //PostedFiles = null; 
                }
                else
                {
                    List<HttpPostedFile> PostedFiles = (List<HttpPostedFile>)Session[Convert.ToString(ViewState["SessionName"])];
                    PostedFiles.Add(FileUpload.PostedFile);
                    Session[Convert.ToString(ViewState["SessionName"])] = PostedFiles;
                    //PostedFiles = null; 
                }                                                   
                ListItem lstTemp = new ListItem(FileUpload.FileName.Trim(), FileUpload.PostedFile.FileName.Trim());
                lstFileNames.Items.Add(lstTemp);
                lstTemp = null;
            }
        }
    }
    protected void btnDel_Click(object sender, EventArgs e)
    {
        if (lstFileNames.Items.Count > 0)
        {
            if (lstFileNames.SelectedIndex >= 0)
            {
                if (Session[Convert.ToString(ViewState["SessionName"])] != null)
                {
                    List<HttpPostedFile> PostedFiles = (List<HttpPostedFile>)Session[Convert.ToString(ViewState["SessionName"])];
                    for (int i = 0; i < PostedFiles.Count; i++)
                    {
                        if (PostedFiles[i].FileName.Trim().Length > 0)
                        {
                            string[] strSplit = PostedFiles[i].FileName.Trim().Split('\\');
                            if (string.Compare(strSplit[strSplit.Length - 1].Trim(), lstFileNames.SelectedItem.Text.Trim(), true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                            {
                                PostedFiles.RemoveAt(i);
                            }
                            strSplit = null;
                        }
                    }
                    Session[Convert.ToString(ViewState["SessionName"])] = PostedFiles;
                    PostedFiles = null;
                }                
                lstFileNames.Items.Remove(lstFileNames.SelectedItem);
            }
        }
    }
    public void Fill()
    {
        string strPath = Server.MapPath(ConfigurationSettings.AppSettings["DocumentsFolder"]) + Convert.ToString(ViewState["LoadID"]);
        if (Directory.Exists(strPath))
        {
            strPath = strPath + "\\" + Convert.ToString(ViewState["DocumnetID"]);
            if (Directory.Exists(strPath))
            {
                try
                {
                    string[] strExistedFilesNames = Directory.GetFiles(strPath);
                    if (strExistedFilesNames.Length > 0)
                    {
                        for (int i = 0; i < strExistedFilesNames.Length; i++)
                        {
                            string[] strSplitFileName = strExistedFilesNames[i].Split('\\');
                            if (strSplitFileName.Length > 0)
                            {
                                lstFileNames.Items.Add(new ListItem(strSplitFileName[strSplitFileName.Length - 1], strExistedFilesNames[i]));
                            }
                            strSplitFileName = null;
                        }
                    }
                    strExistedFilesNames = null;
                }
                catch
                {
                }
            }
        }
    }
    public void Clear()
    {        
        lstFileNames.Items.Clear();             
    }
    public void Save()
    {
        if (ViewState["LoadID"] != null && ViewState["DocumnetID"] != null )
        {
            try
            {
                if (Convert.ToInt32(DBClass.executeScalar("select count(bint_LoadId) from eTn_Load where bint_LoadId = " + Convert.ToString(ViewState["LoadID"]))) <= 0)
                    return;
                if (Convert.ToInt32(DBClass.executeScalar("select count(bint_DocumentId) from eTn_DocumentTable where bint_DocumentId = " + Convert.ToString(ViewState["DocumnetID"]))) <= 0)
                    return;
            }
            catch
            {
                return;
            }
            if (lstFileNames.Items.Count > 0)
            {
                string strPath = Server.MapPath("~/Documents")+"\\" + Convert.ToString(ViewState["LoadID"]);
                if (!Directory.Exists(strPath))
                    Directory.CreateDirectory(strPath);
                strPath = strPath + "\\" + Convert.ToString(ViewState["DocumnetID"]);
                if (!Directory.Exists(strPath))
                    Directory.CreateDirectory(strPath);
                DeleteExistedFilesNotInList(strPath);
                if (Session[Convert.ToString(ViewState["SessionName"])] != null)
                {
                    List<HttpPostedFile> PostedFiles = (List<HttpPostedFile>)Session[Convert.ToString(ViewState["SessionName"])];
                    for (int i = 0; i < PostedFiles.Count; i++)
                    {
                        PostedFiles[i].SaveAs(strPath + "\\" + PostedFileNames[i]);
                        //System.Threading.Thread.Sleep(1000);
                        for (int j = 0; j < 100000000; j++)
                        {
                        }
                    }
                    PostedFiles = null;
                    Session[Convert.ToString(ViewState["SessionName"])] = null;
                }
                lstFileNames.Items.Clear();
                SaveToDB("ioru");
            }
            else
            {
                string strPath1 = Server.MapPath(ConfigurationSettings.AppSettings["DocumentsFolder"]) + Convert.ToString(ViewState["LoadID"]);
                if (Directory.Exists(strPath1))
                {
                    string strPath2 = strPath1 + "\\" + Convert.ToString(ViewState["DocumnetID"]);
                    if (Directory.Exists(strPath2))
                    {
                        DeleteExistedFilesNotInList(strPath2);
                        if (Directory.Exists(strPath2))
                            Directory.Delete(strPath2);
                        if (Directory.Exists(strPath1))
                        {
                            string[] strDirs = Directory.GetDirectories(strPath1);
                            if (strDirs.Length == 0)
                            {
                                Directory.Delete(strPath1);
                            }
                            strDirs = null;
                        }
                    }                   
                }
                SaveToDB("d");
            }
        }       
    }
    public void SaveToDB(string strAction)
    {
        if (ViewState["LoadID"] != null && ViewState["DocumnetID"] != null)
        {
            object[] parms = new object[] { Convert.ToString(ViewState["LoadID"]), Convert.ToString(ViewState["DocumnetID"]), strAction };
            if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_Add_Update_UploadDocuments", parms) > 0)
            {
                Fill();
            }
            parms = null;
        }       
    }
    private void DeleteExistedFilesNotInList(string strPath)
    {
        string[] strExistedFilesNames = Directory.GetFiles(strPath);
        if (strExistedFilesNames.Length > 0)
        {
            for (int i = 0; i < strExistedFilesNames.Length; i++)
            {
                string[] strSplitFileName = strExistedFilesNames[i].Split('\\');
                if (strSplitFileName.Length > 0)
                {
                    if (!lstFileNames.Items.Contains(new ListItem(strSplitFileName[strSplitFileName.Length - 1], strExistedFilesNames[i])))
                    {
                        if (File.Exists(strExistedFilesNames[i]))
                            File.Delete(strExistedFilesNames[i]);
                    }
                }
                strSplitFileName = null;
            }            
        }
        strExistedFilesNames = null;
    }
}
