using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class printreport : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
       // this.Page.Title = Convert.ToString(ConfigurationSettings.AppSettings["CompanyName"]);
        this.Page.Title = GetFromXML.CompanyName;
        if(Session["Total"]!=null)
            lblDisplay.Text = "<div Style='font-family: Arial, sans-serif; font-size: 14px;color:Blue;' align='center'> <b>" + Convert.ToString(Session["TopHeaderText"]) + "</b><br/></div><div align='right'>"+DateTime.Now+"</div><div Style='font-family: Arial, sans-serif; font-size: 12px;color:Blue;'><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + Convert.ToString(Session["SerchCriteria"]) + "&nbsp;&nbsp;&nbsp;" + Convert.ToString(Session["Total"]) + "<br/><br/></div>" + Convert.ToString(Session["PrintDetails"]);
        else if (Session["DriverResult"] != null)
        {
            lblDisplay.Text = "<div Style='font-family: Arial, sans-serif; font-size: 14px;color:Blue;' align='center'> <b> Payment  settlement Sheet </b><br/></div><div align='right'>" + DateTime.Now + "</div><div Style='font-family: Arial, sans-serif; font-size: 12px;color:Blue;'><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + Convert.ToString(Session["SerchCriteria"]) + "&nbsp;&nbsp;&nbsp;" + Convert.ToString(Session["Total"]) + "<br/><br/></div>" + Convert.ToString(Session["PrintDetails"]);

            string[] strPayments = Convert.ToString(Session["DriverResult"]).Replace("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "^").Split('^');
            if (strPayments.Length == 4)
            {
                lblPaymentTotal.Text = strPayments[0];
                lblPaid.Text = strPayments[1];
                lblPending.Text = strPayments[2];
                tblResult.Visible = true;
                lblcount.Text = strPayments[3];
            }
        }
        else
        {
            lblDisplay.Text = "<div Style='font-family: Arial, sans-serif; font-size: 14px;color:Blue;' align='center'> <b>" + Convert.ToString(Session["TopHeaderText"]) + "</b><br/></div><div align='right'>" + DateTime.Now + "</div><div Style='font-family: Arial, sans-serif; font-size: 12px;color:Blue;'><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + Convert.ToString(Session["SerchCriteria"]) + "<br/><br/></div>" + Convert.ToString(Session["PrintDetails"]);
        }
    }
}
