using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;

/// <summary>
/// Summary description for SmallCard
/// </summary>
public class SmallCard
{
    // Public properties (all read-only).
    public string Text
    {
        get { return this.text; }
    }
    public Bitmap Image
    {
        get { return this.image; }
    }
    public int Width
    {
        get { return this.width; }
    }
    public int Height
    {
        get { return this.height; }
    }

    // Internal properties.
    private string text;
    private int width;
    private int height;
    private string familyName;
    private Bitmap image;

    // For generating random numbers.
    private Random random = new Random();

    // ====================================================================
    // Initializes a new instance of the CaptchaImage class using the
    // specified text, width and height.
    // ====================================================================
    //public SmallCard(string s, int width, int height)
    //{
    //    this.text = s;
    //    this.SetDimensions(width, height);
    //    this.GenerateImage();
    //}

    // ====================================================================
    // Initializes a new instance of the CaptchaImage class using the
    // specified text, width, height and font family.
    // ====================================================================
    public SmallCard(string s, int width, int height, string familyName, Int64 LoadIdvalue)
    {
        this.text = s;
        this.SetDimensions(width, height);
        this.SetFamilyName(familyName);
        this.GenerateImage(LoadIdvalue);
    }

    // ====================================================================
    // This member overrides Object.Finalize.
    // ====================================================================
    ~SmallCard()
    {
        Dispose(false);
    }

    // ====================================================================
    // Releases all resources used by this object.
    // ====================================================================
    public void Dispose()
    {
        GC.SuppressFinalize(this);
        this.Dispose(true);
    }

    // ====================================================================
    // Custom Dispose method to clean up unmanaged resources.
    // ====================================================================
    protected virtual void Dispose(bool disposing)
    {
        if (disposing)
            // Dispose of the bitmap.
            this.image.Dispose();
    }

    // ====================================================================
    // Sets the image width and height.
    // ====================================================================
    private void SetDimensions(int width, int height)
    {
        // Check the width and height.
        if (width <= 0)
            throw new ArgumentOutOfRangeException("width", width, "Argument out of range, must be greater than zero.");
        if (height <= 0)
            throw new ArgumentOutOfRangeException("height", height, "Argument out of range, must be greater than zero.");
        this.width = width;
        this.height = height;
    }

    // ====================================================================
    // Sets the font used for the image text.
    // ====================================================================
    private void SetFamilyName(string familyName)
    {
        // If the named font is not installed, default to a system font.
        try
        {
            Font font = new Font(this.familyName, 12F);
            this.familyName = familyName;
            font.Dispose();
        }
        catch (Exception ex)
        {
            this.familyName = System.Drawing.FontFamily.GenericSerif.Name;
        }
    }

    // ====================================================================
    // Creates the bitmap image.
    // ====================================================================
    private void GenerateImage(Int64 LoadIdvalue)
    {
        DataTable dt = SqlHelper.ExecuteDataset(System.Configuration.ConfigurationManager.ConnectionStrings["eTransportString"].ConnectionString, "SP_GetTCardLoadDetails", new object[] { LoadIdvalue }).Tables[0];
        if (dt != null && dt.Rows.Count > 0)
        {
            try
            {
                //string dtime; string ddate;
                //if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[0][2])))
                //{
                //    dtime = "";
                //    ddate = "";
                //}
                //else
                //{
                //    dtime = Convert.ToDateTime(Convert.ToString(dt.Rows[0][2])).ToShortTimeString();
                //    ddate = Convert.ToDateTime(Convert.ToString(dt.Rows[0][2])).ToShortDateString();
                //}
                // Create a new 32-bit bitmap image.
                Bitmap bitmap = new Bitmap(this.width, this.height, PixelFormat.Format32bppArgb);

                // Create a graphics object for drawing.
                Graphics g = Graphics.FromImage(bitmap);
                g.SmoothingMode = SmoothingMode.AntiAlias;
                Rectangle rect = new Rectangle(0, 0, this.width, this.height);

                // Fill in the background.
                HatchBrush hatchBrush = new HatchBrush(HatchStyle.SmallConfetti, Color.White, Color.White);
                g.FillRectangle(hatchBrush, rect);

                // Set up the text font.
                SizeF size;
                float fontSize = rect.Height + 1;
                Font font;
                // Adjust the font size until the text fits within the image.
                do
                {
                    fontSize--;
                    font = new Font(this.familyName, fontSize, FontStyle.Bold);
                    size = g.MeasureString(this.text, font);
                } while (size.Width > rect.Width);
                int heightoffset = Convert.ToInt32(ConfigurationSettings.AppSettings["SmallTCardPrintOffset"]);
                // Set up the text format.
                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;

                //Create a path using the text and warp it randomly.
                //GraphicsPath path = new GraphicsPath();
                //path.AddString("DMU471959", font.FontFamily, (int)font.Style, 13, new Point(158, 26 + heightoffset), format);
                //float v = 4F;
                //PointF[] points =
                //{
                //    new PointF(this.random.Next(rect.Width) / v, this.random.Next(rect.Height) / v),
                //    new PointF(rect.Width - this.random.Next(rect.Width) / v, this.random.Next(rect.Height) / v),
                //    new PointF(this.random.Next(rect.Width) / v, rect.Height - this.random.Next(rect.Height) / v),
                //    new PointF(rect.Width - this.random.Next(rect.Width) / v, rect.Height - this.random.Next(rect.Height) / v)
                //};
                //PointF[] points =
                //{
                //    new PointF(20, 20),
                //    new PointF(40, 40),
                //    new PointF(100, 100),
                //    new PointF(200, 200)
                //};

                //Matrix matrix = new Matrix();
                //matrix.Translate(0F, 0F);
                //path.Warp(points, rect, matrix, WarpMode.Perspective, 0F);

                // Draw the text.
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                // g.FillPath(hatchBrush, path);

                SolidBrush Container = new SolidBrush(Color.Black);
                g.DrawString(Convert.ToString(dt.Rows[0][1]), new Font(font.FontFamily, 10, FontStyle.Bold), Container, new PointF(10, 33 + heightoffset));

                //GraphicsPath path2 = new GraphicsPath();
                //path2.AddString("This is text two", font.FontFamily, (int)font.Style, 12, new Point(160, 120), format);
                //path2.AddString("10:00", font.FontFamily, (int)font.Style, 11, new Point(346, 26 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path2);

                SolidBrush Time = new SolidBrush(Color.Black);
                g.DrawString(string.IsNullOrEmpty(Convert.ToString(dt.Rows[0][2])) ? "" : Convert.ToDateTime(Convert.ToString(dt.Rows[0][2])).ToShortTimeString(), new Font(font.FontFamily, 8, FontStyle.Regular), Time, new PointF(210, 33 + heightoffset));

                //GraphicsPath path212 = new GraphicsPath();
                //path212.AddString("20/07/2007", font.FontFamily, (int)font.Style, 11, new Point(425, 26 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path212);

                SolidBrush AppDate = new SolidBrush(Color.Black);
                g.DrawString(string.IsNullOrEmpty(Convert.ToString(dt.Rows[0][2])) ? "" : Convert.ToDateTime(Convert.ToString(dt.Rows[0][2])).ToShortDateString(), new Font(font.FontFamily, 8, FontStyle.Regular), AppDate, new PointF(265, 33 + heightoffset));

                //GraphicsPath path3 = new GraphicsPath();
                //path3.AddString("FORZ400345", font.FontFamily, (int)font.Style, 11, new Point(133, 64 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path3);

                SolidBrush Chassis = new SolidBrush(Color.Black);
                g.DrawString(Convert.ToString(dt.Rows[0][3]), new Font(font.FontFamily, 8, FontStyle.Regular), Chassis, new PointF(26, 68 + heightoffset));

                //GraphicsPath path4 = new GraphicsPath();
                //path4.AddString("559-488-4703", font.FontFamily, (int)font.Style, 11, new Point(367, 64 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path4);

                SolidBrush Phone = new SolidBrush(Color.Black);
                g.DrawString(Convert.ToString(dt.Rows[0][4]), new Font(font.FontFamily, 8, FontStyle.Regular), Phone, new PointF(220, 68 + heightoffset));

                //GraphicsPath path5 = new GraphicsPath();
                //path5.AddString("UNIVER USA INC", font.FontFamily, (int)font.Style, 11, new Point(133, 101 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path5);

                SolidBrush Shipper = new SolidBrush(Color.Black);
                g.DrawString(Convert.ToString(dt.Rows[0][6]).Length > 20 ? Convert.ToString(dt.Rows[0][6]).Substring(0, 20) : Convert.ToString(dt.Rows[0][6]), new Font(font.FontFamily, 8, FontStyle.Regular), Shipper, new PointF(26, 95 + heightoffset));

                //GraphicsPath path6 = new GraphicsPath();
                //path6.AddString("SAVITIR", font.FontFamily, (int)font.Style, 11, new Point(367, 101 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path6);

                SolidBrush Name = new SolidBrush(Color.Black);
                g.DrawString(Convert.ToString(dt.Rows[0][5]), new Font(font.FontFamily, 8, FontStyle.Regular), Name, new PointF(220, 95 + heightoffset));

                //GraphicsPath path7 = new GraphicsPath();
                //path7.AddString("4485 EAST FLORENCE STREET", font.FontFamily, (int)font.Style, 11, new Point(133, 137 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path7);

                SolidBrush Address = new SolidBrush(Color.Black);
                g.DrawString(Convert.ToString(dt.Rows[0][7]).Length > 20 ? Convert.ToString(dt.Rows[0][7]).Substring(0, 20) : Convert.ToString(dt.Rows[0][7]), new Font(font.FontFamily, 8, FontStyle.Regular), Address, new PointF(26, 120 + heightoffset));

                //GraphicsPath path8 = new GraphicsPath();
                //path8.AddString("", font.FontFamily, (int)font.Style, 11, new Point(329, 137 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path8);

                SolidBrush StayWidth1 = new SolidBrush(Color.Black);
                g.DrawString("", new Font(font.FontFamily, 8, FontStyle.Regular), StayWidth1, new PointF(220, 120 + heightoffset));

                //GraphicsPath path9 = new GraphicsPath();
                //path9.AddString("", font.FontFamily, (int)font.Style, 11, new Point(396, 137 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path9);

                SolidBrush Drop = new SolidBrush(Color.Black);
                g.DrawString("", new Font(font.FontFamily, 8, FontStyle.Regular), Drop, new PointF(275, 120 + heightoffset));

                //GraphicsPath path10 = new GraphicsPath();
                //path10.AddString("FRESNO CA 93725", font.FontFamily, (int)font.Style, 11, new Point(133, 174 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path10);

                SolidBrush City = new SolidBrush(Color.Black);
                g.DrawString(Convert.ToString(dt.Rows[0][8]), new Font(font.FontFamily, 8, FontStyle.Regular), City, new PointF(26, 143 + heightoffset));

                //GraphicsPath path11 = new GraphicsPath();
                //path11.AddString("SHIP", font.FontFamily, (int)font.Style, 11, new Point(367, 174 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path11);

                SolidBrush Ship = new SolidBrush(Color.Black);
                g.DrawString("", new Font(font.FontFamily, 8, FontStyle.Regular), Ship, new PointF(220, 143 + heightoffset));

                //GraphicsPath path12 = new GraphicsPath();
                //path12.AddString("RAVI", font.FontFamily, (int)font.Style, 11, new Point(66, 209 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path12);

                SolidBrush Agent = new SolidBrush(Color.Black);
                g.DrawString("", new Font(font.FontFamily, 8, FontStyle.Regular), Agent, new PointF(26, 168 + heightoffset));

                //GraphicsPath path13 = new GraphicsPath();
                //path13.AddString("ATL424980", font.FontFamily, (int)font.Style, 11, new Point(208, 209 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path13);

                SolidBrush Booking = new SolidBrush(Color.Black);
                g.DrawString(Convert.ToString(dt.Rows[0][10]), new Font(font.FontFamily, 8, FontStyle.Regular), Booking, new PointF(80, 168 + heightoffset));

                //GraphicsPath path14 = new GraphicsPath();
                //path14.AddString("Multiple(2)", font.FontFamily, (int)font.Style, 11, new Point(367, 209 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path14);

                SolidBrush PU = new SolidBrush(Color.Black);
                g.DrawString(Convert.ToString(dt.Rows[0][11]), new Font(font.FontFamily, 8, FontStyle.Regular), PU, new PointF(143, 168 + heightoffset));

                SolidBrush Dest = new SolidBrush(Color.Black);
                g.DrawString(Convert.ToString(dt.Rows[0][12]), new Font(font.FontFamily, 8, FontStyle.Regular), Dest, new PointF(220, 168 + heightoffset));

                //GraphicsPath path15 = new GraphicsPath();
                //path15.AddString("BNSF / OAKLAND 333 MARITIME ST", font.FontFamily, (int)font.Style, 11, new Point(150, 245 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path15);

                SolidBrush Consignee = new SolidBrush(Color.Black);
                g.DrawString(Convert.ToString(dt.Rows[0][13]), new Font(font.FontFamily, 8, FontStyle.Regular), Consignee, new PointF(26, 191 + heightoffset));


                //GraphicsPath path16 = new GraphicsPath();
                //path16.AddString("OAKLAND CA 94803", font.FontFamily, (int)font.Style, 11, new Point(224, 279 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path16);

                SolidBrush Location = new SolidBrush(Color.Black);
                g.DrawString(Convert.ToString(dt.Rows[0][14]), new Font(font.FontFamily, 8, FontStyle.Regular), Location, new PointF(26, 217 + heightoffset));

                //GraphicsPath path17 = new GraphicsPath();
                //path17.AddString("Multiple Origins(2)", font.FontFamily, (int)font.Style, 11, new Point(224, 316 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path17);

                SolidBrush Ramp = new SolidBrush(Color.Black);
                g.DrawString(Convert.ToString(dt.Rows[0][15]), new Font(font.FontFamily, 8, FontStyle.Regular), Ramp, new PointF(26, 241 + heightoffset));

                //GraphicsPath path18 = new GraphicsPath();
                //path18.AddString("20", font.FontFamily, (int)font.Style, 11, new Point(66, 353 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path18);

                SolidBrush PCS = new SolidBrush(Color.Black);
                g.DrawString(Convert.ToString(dt.Rows[0][16]), new Font(font.FontFamily, 8, FontStyle.Regular), PCS, new PointF(26, 266 + heightoffset));


                //GraphicsPath path19 = new GraphicsPath();
                //path19.AddString("42504", font.FontFamily, (int)font.Style, 11, new Point(224, 353 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path19);

                SolidBrush WT = new SolidBrush(Color.Black);
                g.DrawString(Convert.ToString(dt.Rows[0][17]), new Font(font.FontFamily, 8, FontStyle.Regular), WT, new PointF(102, 266 + heightoffset));

                //GraphicsPath path20 = new GraphicsPath();
                //path20.AddString("123456", font.FontFamily, (int)font.Style, 11, new Point(367, 353 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path20);

                SolidBrush Seal = new SolidBrush(Color.Black);
                g.DrawString(Convert.ToString(dt.Rows[0][18]), new Font(font.FontFamily, 8, FontStyle.Regular), Seal, new PointF(187, 266 + heightoffset));

                //GraphicsPath path21 = new GraphicsPath();
                //path21.AddString("ST/PM", font.FontFamily, (int)font.Style, 11, new Point(207, 381 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path21);

                SolidBrush ST = new SolidBrush(Color.Black);
                g.DrawString("", new Font(font.FontFamily, 8, FontStyle.Regular), ST, new PointF(86, 287 + heightoffset));

                //GraphicsPath path22 = new GraphicsPath();
                //path22.AddString("LOAD # : 50", font.FontFamily, (int)font.Style, 13, new Point(224, 451 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path22);

                SolidBrush RFR = new SolidBrush(Color.Black);
                g.DrawString("", new Font(font.FontFamily, 8, FontStyle.Bold), RFR, new PointF(26, 307 + heightoffset));

                SolidBrush Temp = new SolidBrush(Color.Black);
                g.DrawString("", new Font(font.FontFamily, 8, FontStyle.Bold), Temp, new PointF(86, 307 + heightoffset));
                //GraphicsPath path23 = new GraphicsPath();
                //path23.AddString("", font.FontFamily, (int)font.Style, 11, new Point(396, 495 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path23);
                SolidBrush LoadId = new SolidBrush(Color.Black);
                g.DrawString("Load# : " + Convert.ToString(dt.Rows[0][0]), new Font(font.FontFamily, 10, FontStyle.Bold), LoadId, new PointF(102, 333 + heightoffset));


                SolidBrush StayWidth2 = new SolidBrush(Color.Black);
                g.DrawString("", new Font(font.FontFamily, 8, FontStyle.Regular), StayWidth2, new PointF(237, 363 + heightoffset));

                //GraphicsPath path24 = new GraphicsPath();
                //path24.AddString("10:30AM", font.FontFamily, (int)font.Style, 11, new Point(50, 531 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path24);


                SolidBrush TimeIn = new SolidBrush(Color.Black);
                g.DrawString("", new Font(font.FontFamily, 8, FontStyle.Regular), TimeIn, new PointF(26, 390 + heightoffset));

                //GraphicsPath path25 = new GraphicsPath();
                //path25.AddString("12:30PM", font.FontFamily, (int)font.Style, 11, new Point(147, 531 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path25);

                SolidBrush TimeOut = new SolidBrush(Color.Black);
                g.DrawString("", new Font(font.FontFamily, 8, FontStyle.Regular), TimeOut, new PointF(102, 390 + heightoffset));

                //GraphicsPath path26 = new GraphicsPath();
                //path26.AddString("", font.FontFamily, (int)font.Style, 11, new Point(396, 531 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path26);

                SolidBrush StayWidth3 = new SolidBrush(Color.Black);
                g.DrawString("", new Font(font.FontFamily, 8, FontStyle.Regular), StayWidth3, new PointF(237, 390 + heightoffset));

                //GraphicsPath path27 = new GraphicsPath();
                //path27.AddString("BNSF / OAKLAND 333 MARITIME ST", font.FontFamily, (int)font.Style, 11, new Point(208, 568 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path27);

                SolidBrush Treminate = new SolidBrush(Color.Black);
                g.DrawString(Convert.ToString(dt.Rows[0][19]), new Font(font.FontFamily, 8, FontStyle.Regular), Treminate, new PointF(26, 415 + heightoffset));

                //GraphicsPath path28 = new GraphicsPath();
                //path28.AddString("ROAR LOGISTICS/GA", font.FontFamily, (int)font.Style, 11, new Point(99, 625 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path28);

                SolidBrush BW = new SolidBrush(Color.Black);
                g.DrawString("", new Font(font.FontFamily, 8, FontStyle.Regular), BW, new PointF(46, 434 + heightoffset));

                //GraphicsPath path29 = new GraphicsPath();
                //path29.AddString("40.00", font.FontFamily, (int)font.Style, 11, new Point(262, 608 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path29);

                //SolidBrush brush30 = new SolidBrush(Color.Black);
                //g.DrawString("40.00", new Font(font.FontFamily, 8, FontStyle.Regular), brush30, new PointF(251, 608 + heightoffset));

                //GraphicsPath path30 = new GraphicsPath();
                //path30.AddString("30.00", font.FontFamily, (int)font.Style, 11, new Point(334, 608 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path30);

                //SolidBrush brush31 = new SolidBrush(Color.Black);
                //g.DrawString("30.00", new Font(font.FontFamily, 8, FontStyle.Regular), brush31, new PointF(319, 608 + heightoffset));

                //GraphicsPath path31 = new GraphicsPath();
                //path31.AddString("70.00", font.FontFamily, (int)font.Style, 11, new Point(401, 608 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path31);

                //SolidBrush brush32 = new SolidBrush(Color.Black);
                //g.DrawString("70.00", new Font(font.FontFamily, 8, FontStyle.Regular), brush32, new PointF(392, 608 + heightoffset));

                //GraphicsPath path32 = new GraphicsPath();
                //path32.AddString("123456", font.FontFamily, (int)font.Style, 11, new Point(99, 659 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path32);

                SolidBrush DriverRate = new SolidBrush(Color.Black);
                g.DrawString("", new Font(font.FontFamily, 8, FontStyle.Regular), DriverRate, new PointF(174, 441 + heightoffset));

                //GraphicsPath path33 = new GraphicsPath();
                //path33.AddString("123456", font.FontFamily, (int)font.Style, 11, new Point(329, 659 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path33);

                //SolidBrush brush34 = new SolidBrush(Color.Black);
                //g.DrawString("123456", new Font(font.FontFamily, 8, FontStyle.Regular), brush34, new PointF(251, 659 + heightoffset));

                ///////////////
                // Add some random noise.
                //int m = Math.Max(rect.Width, rect.Height);
                //for (int i = 0; i < (int)(rect.Width * rect.Height / 30F); i++)
                //{
                //    int x = this.random.Next(rect.Width);
                //    int y = this.random.Next(rect.Height);
                //    int w = this.random.Next(m / 50);
                //    int h = this.random.Next(m / 50);
                //    g.FillEllipse(hatchBrush, x, y, w, h);
                //}

                // Clean up.
                font.Dispose();
                hatchBrush.Dispose();
                g.Dispose();

                // Set the image.
                this.image = bitmap;
            }
            catch(Exception ex)
            {
                Bitmap bitmap = new Bitmap(this.width, this.height, PixelFormat.Format32bppArgb);
                // Create a graphics object for drawing.
                Graphics g = Graphics.FromImage(bitmap);
                g.SmoothingMode = SmoothingMode.AntiAlias;
                Rectangle rect = new Rectangle(0, 0, this.width, this.height);

                // Fill in the background.
                HatchBrush hatchBrush = new HatchBrush(HatchStyle.SmallConfetti, Color.White, Color.White);
                g.FillRectangle(hatchBrush, rect);
                SizeF size;
                float fontSize = rect.Height + 1;
                Font font;
                // Adjust the font size until the text fits within the image.
                do
                {
                    fontSize--;
                    font = new Font(this.familyName, fontSize, FontStyle.Bold);
                    size = g.MeasureString(this.text, font);
                } 
                while (size.Width > rect.Width);
                int heightoffset = Convert.ToInt32(ConfigurationSettings.AppSettings["SmallTCardPrintOffset"]);
                // Set up the text format.
                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;
                SolidBrush Time = new SolidBrush(Color.Black);
                g.DrawString("There is Error in data. Please check and try again.", new Font(font.FontFamily, 10, FontStyle.Regular), Time, new PointF(0, 0));

                font.Dispose();
                hatchBrush.Dispose();
                g.Dispose();
                // Set the image.
                this.image = bitmap;
            }
        }
    }
}
