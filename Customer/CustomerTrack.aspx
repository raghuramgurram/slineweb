<%@ Page Language="C#" MasterPageFile="~/Customer/CustomerMaster.master" AutoEventWireup="true" CodeFile="CustomerTrack.aspx.cs" Inherits="CustomerTrack" Title="S Line Transport Inc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td>
       <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblbox1">
          <tr>
            <td align="right">Track Load:
                <asp:TextBox ID="txtContainer" runat="server"/>
               <asp:DropDownList ID="ddlContainer" runat="server" >
                   <asp:ListItem Selected="True" Value="1">Container#</asp:ListItem>
                   <asp:ListItem Value="2">Ref#</asp:ListItem>
                   <asp:ListItem Value="3">Booking#</asp:ListItem>
                   <asp:ListItem Value="4">Load#</asp:ListItem>
               </asp:DropDownList>
               <asp:Button ID="btnSearch" runat="server" CssClass="btnstyle" Text="Search" OnClick="btnSearch_Click"/>       
             </td>   
          </tr>
       </table>
     </td>
   </tr>
 </table>
</asp:Content>

