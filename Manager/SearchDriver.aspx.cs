using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class SearchDriver : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Drivers";
        if (!IsPostBack)
        {
            FillDetails();
        }
    }
    private void FillDetails()
    {
        GridView1.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"].ToString());          
        GridBar1.LinksList = "Check for Drivers;Show UnAssigned Drivers";
        GridBar1.PagesList = "CheckDrivers.aspx;UnAssignedDrivers.aspx";        
    }     
}
