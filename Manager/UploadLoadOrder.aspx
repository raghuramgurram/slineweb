<%@ Page AutoEventWireup="true" CodeFile="UploadLoadOrder.aspx.cs" Inherits="UploadLoadOrder"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%--<%@ Register Src="~/UserControls/MulFileUpload.ascx" TagName="MulFileUpload" TagPrefix="uc2" %>--%>

<%@ Register Src="../UserControls/MulFileUpload1.ascx" TagName="MulFileUpload1" TagPrefix="uc2" %>

<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td>
              <uc1:GridBar ID="GridBar1" runat="server" HeaderText="Load Number :"/>
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../Images/pix.gif" alt="" width="1" height="3" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblform">
        <tr>
          <td height="50" align="center">             
                 <%--<uc2:MulFileUpload ID="upLoadOrder" runat="server" BrowseLabelName="Select Load Order Document :"
                     DocumnetID="1" Name="upLoadOrder" Visible="true" />  --%>    
                     <uc2:MulFileUpload1 ID="MulFileUpload1_1" runat="server" />                
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../images/pix.gif" alt="" width="1" height="2" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
        <tr>
          <td align="right">
              <asp:Button ID="btnupload" runat="server" CssClass="btnstyle" Text="Save" OnClick="btnupload_Click" />&nbsp;<asp:Button
                  ID="btncancel" runat="server" CssClass="btnstyle" Text="Cancel" OnClick="btncancel_Click" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../images/pix.gif" alt="" width="1" height="10" /></td>
        </tr>
      </table></td>
  </tr>
</table>

</asp:Content>

