<%@ Page AutoEventWireup="true" CodeFile="CustomerTrackLoadResults.aspx.cs" Inherits="CustomerTrackLoadResults"
    Language="C#" MasterPageFile="~/Customer/CustomerMaster.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/Grid.ascx" TagName="Grid" TagPrefix="uc2" %>

<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblbox1">
          <tr>
            <td align="right">Track Load:
                <asp:TextBox ID="txtContainer" runat="server"/>
               <asp:DropDownList ID="ddlContainer" runat="server" >
                   <asp:ListItem Selected="True" Value="1">Container#</asp:ListItem>
                   <asp:ListItem Value="2">Ref#</asp:ListItem>
                   <asp:ListItem Value="3">Booking#</asp:ListItem>
                   <asp:ListItem Value="4">Chasis#</asp:ListItem>
                   <asp:ListItem Value="5">Load#</asp:ListItem>
               </asp:DropDownList>
               <asp:Button ID="btnSearch" runat="server" CssClass="btnstyle" Text="Search" OnClick="btnSearch_Click"/>       
             </td>   
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr>
              <td><img src="../Images/pix.gif" alt="" width="1" height="3" /></td>
           </tr>
        </table> 
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr>
              <td>
                  <uc1:GridBar ID="GridBar1" runat="server" HeaderText="Search Results"/>
              </td>
           </tr>
        </table> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr>
              <td><img src="../Images/pix.gif" alt="" width="1" height="3" /></td>
           </tr>
        </table> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tbldisplay">
           <tr>
              <td>
                  <uc2:Grid ID="GridSearch" runat="server" />
              
              </td>
           </tr>
        </table> 
    </td>
   </tr>
  </table>
        
</asp:Content>

