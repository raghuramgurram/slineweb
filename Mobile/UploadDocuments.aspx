<%@ Page Language="C#" MasterPageFile="~/Mobile/eTransportMobile.master" AutoEventWireup="true" CodeFile="UploadDocuments.aspx.cs" Inherits="Mobile_UploadDocuments" Title="Untitled Page" %>
<%@ Register Src="MobileMenu.ascx" TagName="Menu"
    TagPrefix="mu" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<mu:Menu ID="MuPageMenu" runat="server" />
      <div class="lists">
      <asp:LinkButton ID="lnkInGate" runat="server" OnClick="lnkInGate_click" data-role="button" data-theme="b" data-icon="arrow-r" data-iconpos="right"  data-transition="flip" rel="external">
       In-Gate 
      </asp:LinkButton>
       <asp:LinkButton ID="lnkOutGate" runat="server" OnClick="lnkOutGate_click" data-role="button" data-theme="b" data-icon="arrow-r" data-iconpos="right"  data-transition="flip" rel="external">
       Out-Gate 
      </asp:LinkButton>
      <asp:LinkButton ID="lnkBillOfLanding" runat="server" OnClick="lnkBillOfLanding_click" data-role="button" data-theme="b" data-icon="arrow-r" data-iconpos="right"  data-transition="flip" rel="external">
       Bill of Lading
      </asp:LinkButton>
       <asp:LinkButton ID="lnkPOD" runat="server" OnClick="lnkPOD_click" data-role="button" data-theme="b" data-icon="arrow-r" data-iconpos="right"  data-transition="flip" rel="external">
       POD 
      </asp:LinkButton>
      <asp:LinkButton ID="lnkScaleTicket" runat="server" OnClick="lnkScaleTicket_click" data-role="button" data-theme="b" data-icon="arrow-r" data-iconpos="right"  data-transition="flip" rel="external">
       Scale Ticket 
      </asp:LinkButton>
         </div>
      <asp:LinkButton ID="lnkgoback" runat="server" data-role="button" data-inline="true" data-theme="b" 
        data-icon="arrow-l" data-iconpos="left" class="backbtn fl-right" rel="external" OnClick="lnkgoback_click"> Back </asp:LinkButton>
</asp:Content>


