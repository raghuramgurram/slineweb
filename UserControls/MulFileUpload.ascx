<%@ Control AutoEventWireup="true" CodeFile="MulFileUpload.ascx.cs" Inherits="MulFileUpload"
    Language="C#" %>
<table border="0" cellpadding="0" cellspacing="0" width="100%" id="tbldisplay">
    <tr>
        <td align="right" valign="top" width="30%">
            <asp:Label ID="lblBrowseFile" runat="server" Text="" Font-Bold="true"></asp:Label>&nbsp;
        </td>
        <td align="left" valign="top" width="70%">           
            <asp:FileUpload ID="FileUpload" runat="server" Width="300px" />
            <%--<input id="FileUpload1" runat="server" type="file" />--%>
        </td>
    </tr>
    <tr>
        <td align="right" valign="top" width="30%">
        </td>
        <td align="left" valign="top" width="70%">
            <asp:Button ID="btnAddToList" runat="server" CssClass="btnstyle" 
                OnClick="btnAddToList_Click" Text="Add" UseSubmitBehavior="False" />
            <asp:Button ID="btnDelToList" runat="server" CssClass="btnstyle" OnClick="btnDel_Click"
                Text="Delete" UseSubmitBehavior="False" />
        </td>
    </tr>
    <tr>
        <td colspan="2" style="height:2px"  ></td> 
    </tr>
    <tr>
        <td align="right" valign="top" width="30%">
        </td>
        <td align="left" valign="top" width="70%">
            <asp:ListBox ID="lstFileNames" runat="server" Width="300px" Height="75px"></asp:ListBox>
        </td>
    </tr>
</table>
