using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class DriverPayments : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Payments";
        if (!IsPostBack)
        {
            if (Session["EmployeeId"] != null)
            {
                GridFill("Pending","");
                //FindTotal();
            }
        }
    }
    protected void FindTotal(string strWhereClause)
    {
        object[] objParams = new object[] { strWhereClause, "" };
        System.Data.SqlClient.SqlParameter[] objsqlparams = null;
        objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_GetPendingTotalOfDriverByCriteria", objParams);
        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_GetPendingTotalOfDriverByCriteria", objsqlparams);
        lblTotalDisplay.Text = "Total : " + objsqlparams[objsqlparams.Length - 1].Value;
        objsqlparams = null;
        objParams = null;
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (ViewState["FromDate"] != null)
        {
            datFromDate.SetInitialDate = false;
            datToDate.SetInitialDate = false;
            datFromDate.SetDate(Convert.ToString(ViewState["FromDate"]));
            datToDate.SetDate(Convert.ToString(ViewState["ToDate"]));
            //ddlPaymentStatus.SelectedIndex = ddlPaymentStatus.Items.IndexOf(ddlPaymentStatus.Items.FindByText(Convert.ToString(ViewState["PendingStatus"])));
        }
    }
    private void GridFill(string strPending,String Type)
    {
        grdCurrent.MainTableName = "eTn_Load";
        grdCurrent.MainTablePK = "eTn_Load.bint_LoadId";
        grdCurrent.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) as 'ID',eTn_Load.bint_LoadId as 'Load',eTn_AssignDriver.[nvar_Tag] as 'TagNo','' as 'Tag No#', " +
            "eTn_LoadStatus.nvar_LoadStatusDesc as 'LoadStatus','$' + Convert(varchar(20),isnull(eTn_Payables.num_Charges,0)) as 'Charges'," +
            "'$' + Convert(varchar(20),isnull(eTn_Payables.num_LumperCharges,0)) as 'Lumper','$' + Convert(varchar(20),isnull(eTn_Payables.num_DetentionCharges,0)) as 'Dtn.'," +
            "'$' + Convert(varchar(22),(isnull(eTn_Payables.num_OtherCharges1,0) + isnull(eTn_Payables.num_OtherCharges2,0) + " +
            " isnull(eTn_Payables.num_OtherCharges3,0) + isnull(eTn_Payables.num_OtherCharges4,0))) as 'Other'," +
            "'$' + Convert(varchar(20),isnull(eTn_Payables.[num_AdvancedPaid],0)) as 'Adv.'," +
            "'<a target=New href=DriverLoadCharges.aspx?'+Convert(varchar(20),eTn_Load.bint_LoadId)+'^'+Convert(varchar(20),eTn_AssignDriver.bint_AssignedId)+'>'+'$' + Convert(varchar(25),(isnull([eTn_Payables].[num_Charges],0)-isnull([eTn_Payables].[num_AdvancedPaid],0)+isnull([eTn_Payables].[num_Dryrun],0)+isnull([eTn_Payables].[num_FuelSurcharge],0)+isnull([eTn_Payables].[num_FuelSurchargeAmounts],0)+isnull([eTn_Payables].[num_PierTermination],0)+isnull([eTn_Payables].[bint_ExtraStops],0)+isnull([eTn_Payables].[num_LumperCharges],0)+" +
            "isnull([eTn_Payables].[num_DetentionCharges],0)+isnull([eTn_Payables].[num_ChassisSplit],0)+isnull([eTn_Payables].[num_ChassisRent],0)+isnull([eTn_Payables].[num_Pallets],0)+isnull([eTn_Payables].[num_ContainerWashout],0)+isnull([eTn_Payables].[num_YardStorage],0)+isnull([eTn_Payables].[num_RampPullCharges],0)+isnull([eTn_Payables].[num_TriaxleCharge],0)+isnull([eTn_Payables].[num_TransLoadCharges],0)+" +
            "isnull([eTn_Payables].[num_HLScaleCharges],0)+isnull([eTn_Payables].[num_ScaleTicketCharges],0)+isnull([eTn_Payables].[num_OtherCharges1],0)+isnull([eTn_Payables].[num_OtherCharges2],0)+isnull([eTn_Payables].[num_OtherCharges3],0)+isnull([eTn_Payables].[num_OtherCharges4],0)-isnull(eTn_Payables.[num_PaidToAnotherDriver],0)+isnull(eTn_Payables.[num_TollCharges],0)+isnull(eTn_Payables.[num_SlipCharges],0)+isnull(eTn_Payables.[num_RepairCharges],0)))+'</a>' AS 'Total'";
        grdCurrent.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_LoadStatus on eTn_LoadStatus.bint_LoadStatusId = eTn_Load.bint_LoadStatusId " +
            " inner join [eTn_AssignDriver] on eTn_Load.[bint_LoadId]=[eTn_AssignDriver].[bint_LoadId] " +
            " left outer join eTn_Payables on eTn_Payables.bint_LoadId = eTn_Load.bint_LoadId and [eTn_AssignDriver].[bint_AssignedId]=[eTn_Payables].[bint_AssignedId] ";
        if (string.Compare(strPending, "Pending", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            strPending = " and (lower(isnull(eTn_Payables.nvar_PayableStatus,'pending'))='pending' or len(eTn_Payables.nvar_PayableStatus)=0)" +
            " and (eTn_Load.date_DateOfCreation >= '" + datFromDate.Date + "' and eTn_Load.date_DateOfCreation <= DateAdd(dd,1,'" + datToDate.Date + "'))";
        }
        else if (string.Compare(strPending, "Paid", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            strPending = " and (lower(eTn_Payables.nvar_PayableStatus)='paid')" +
            " and (eTn_Load.date_DateOfCreation >= '" + datFromDate.Date + "' and eTn_Load.date_DateOfCreation <= DateAdd(dd,1,'" + datToDate.Date + "'))";
        }
        grdCurrent.SingleRowColumnsWhereClause = " lower(eTn_Load.[nvar_AssignTo])='driver' and  [eTn_AssignDriver].[bint_EmployeeId]=" + Convert.ToString(Session["EmployeeId"]) +
            strPending;
        if(Type.Length==0)
            FindTotal(" lower(eTn_Load.[nvar_AssignTo])='driver' and  [eTn_AssignDriver].[bint_EmployeeId]=" + Convert.ToString(Session["EmployeeId"]) + strPending);
        else
            FindTotal(" lower(eTn_Load.[nvar_AssignTo])='driver' and  [eTn_AssignDriver].[bint_EmployeeId]=" + Convert.ToString(Session["EmployeeId"]) + strPending);

        grdCurrent.SingleRowColumnsOrderByClause = " order by eTn_Load.bint_LoadId desc";
        grdCurrent.VisibleColumnsList = "Load;Tag No#;Origin;Destination;Delivered;Charges;Lumper;Dtn.;Other;Adv.;Total";
        //grdCurrent.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
        //grdCurrent.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";
        //grdCurrent.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
        //    "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_WorkPhone") + " as 'Driver/Carrier'," +
        //    CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");
        //grdCurrent.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
        //    "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver') ";
        grdCurrent.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
        grdCurrent.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
        grdCurrent.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + ";" + 
            CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + ";" +
            "eTn_TrackLoad.date_CreateDate as 'Delivered'";
        grdCurrent.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
            "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
            "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId and lower(eTn_Load.[nvar_AssignTo])='driver'";
        grdCurrent.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=(select eTn_LoadStatus.bint_LoadStatusId from eTn_LoadStatus where lower(eTn_LoadStatus.nvar_LoadStatusDesc) = 'delivered')";        
        grdCurrent.LastColumnLinksList = "Invoice";
        //grdCurrent.LastColumnPagesList = "~/Customer/CustomerInvoice.aspx";
        grdCurrent.DeleteVisible = false;
        grdCurrent.LastLocationVisible = false;
        grdCurrent.LoadStatus = "All";
        Session["RedirectURL"] = "~/Driver/DriverPayments.aspx?";
        grdCurrent.BindGrid(0);                     
    }
    protected void btnShow_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            GridFill("Pending","");
            ViewState["FromDate"] = datFromDate.Date;
            ViewState["ToDate"] = datToDate.Date;
            ViewState["PendingStatus"] = "Pending";
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {        
        string whereClause = "";
        if (txtSearchText.Text.Trim().Length > 0)
        {
            switch (ddlTypes.SelectedIndex)
            {
                case 0:
                    whereClause = " and [eTn_AssignDriver].[nvar_Tag] like '"+ txtSearchText.Text.Trim()+"%'";
                    break;
                case 1:
                    try
                    {
                        whereClause = " and eTn_Load.[bint_LoadId]=" + Convert.ToInt64(txtSearchText.Text.Trim());
                    }
                    catch
                    {
                        whereClause = " and eTn_Load.[bint_LoadId]=0";
                    }
                    break;
            }
        }
        else
        {
            whereClause = "";
        }
        //if (Convert.ToInt32(DBClass.executeScalar("select count([eTn_AssignDriver].eTn_Load.bint_LoadId) from eTn_Load inner join [eTn_AssignDriver] on eTn_Load.[bint_LoadId]=[eTn_AssignDriver].[bint_LoadId] where " + Convert.ToString(whereClause))) == 1)
        //{
        //    Response.Redirect("~/Driver/DriverTrackLoad.aspx?" + DBClass.executeScalar("select [eTn_AssignDriver].eTn_Load.bint_LoadId from eTn_Load inner join [eTn_AssignDriver] on eTn_Load.[bint_LoadId]=[eTn_AssignDriver].[bint_LoadId] where " + Convert.ToString(whereClause)));
        //}
        GridFill(whereClause,"D");
    }
}
