<%@ Page AutoEventWireup="true" CodeFile="DashBoard.aspx.cs" Inherits="DashBoard"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/LoadGridManager.ascx" TagName="LoadGridManager" TagPrefix="uc1" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">


    <table id="ContentTbl1" border="0" cellpadding="2" cellspacing="0" width="100%">
        <tr>
            <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tbldisplay">
                    <tr id="row">
                        <th colspan="2">
                            <asp:Label ID="lblText" runat="server">Loads Summary</asp:Label>
                        </th>
                        <th>&nbsp;</th>
                        <th style="text-align: right" colspan="3">
                            <asp:LinkButton ID="lnkDriverswithDispatch" ForeColor="Red" runat="server" OnClick="lnkDriversDispatch_click">Drivers Dispatch Details</asp:LinkButton>&nbsp;&nbsp;
                       <span runat="server" id="spntunnel" visible="true" style="color: Red">|</span>&nbsp;&nbsp;
                       <asp:LinkButton ID="LinkButton1" ForeColor="Red" runat="server" PostBackUrl="~/Manager/EmptyReturnFreeDays.aspx">Containers Going Per Diem</asp:LinkButton>&nbsp;&nbsp;
                        </th>
                    </tr>
                    <tr id="row">
                        <td style="background-color: White" width="20%">
                            <asp:Label ID="lblNew" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="background-color: White" width="20%">
                            <asp:Label ID="lblLoadPlanner" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="background-color: White" width="20%">
                            <asp:Label ID="lblDelivered" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="background-color: White" width="20%">
                            <%-- link for Same day Loads (Madhav)--%>
                            <asp:LinkButton ID="lnkSameDayLoads" ForeColor="green" runat="Server" OnClick="lnkSameDayLoads_click" Font-Bold="true" CausesValidation="false"></asp:LinkButton>
                        </td>
                        <td style="background-color: White" width="20%">
                            <asp:LinkButton ID="lnkIsExportLoads" runat="Server" OnClick="lnkIsExportLoads_click"></asp:LinkButton>
                        </td>
                    </tr>
                    <tr id="row">
                        <td style="background-color: White">
                            <asp:Label ID="lblAssigned" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="background-color: White">
                            <asp:Label ID="lblDriveronWaiting" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="background-color: White">
                            <asp:Label ID="lblTerminated" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="background-color: White">
                            <%-- link for Next day Loads (Madhav)--%>
                            <asp:LinkButton ID="lnkNextDayLoads" ForeColor="deeppink" runat="Server" Font-Bold="true" OnClick="lnkNextDayLoads_click" CausesValidation="false"></asp:LinkButton>
                        </td>
                        <td style="background-color: White">&nbsp;</td>

                    </tr>
                    <tr id="row">
                        <td style="background-color: White">
                            <asp:Label ID="lblPickedup" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="background-color: White">
                            <asp:Label ID="lblDropinWarehouse" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="background-color: White">
                            <asp:Label ID="lblPaperworkpending" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="background-color: White">
                            <asp:LinkButton ID="lnkAccessorialLoads" runat="server" OnClick="lnkAccessorialLoads_click"></asp:LinkButton>
                        </td>
                        <td style="background-color: White">&nbsp;</td>
                    </tr>
                    <tr id="row">
                        <td style="background-color: White">
                            <asp:Label ID="lblEnRoute" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="background-color: White">
                            <asp:Label ID="lblLoadedinYard" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="background-color: White">
                            <%--<asp:Label ID="lblClosed" runat="server" Font-Bold="true"></asp:Label>--%>
                            <asp:Label ID="lblStreetTurn" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="background-color: White">
                            <asp:LinkButton ID="lnkltodayLFDLoads" ForeColor="Coral" runat="Server" Font-Bold="true" OnClick="lnkltodayLFDLoads_click" CausesValidation="false"></asp:LinkButton>
                        </td>
                        <td style="background-color: White">&nbsp;</td>
                    </tr>
                    <tr id="row">
                        <td style="background-color: White">
                            <asp:Label ID="lblReachedDestination" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="background-color: White">
                            <asp:Label ID="lblEmptyinYard" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="background-color: White">
                            <asp:LinkButton ID="lblInvoiceable" ForeColor="Coral" runat="Server" Font-Bold="true" OnClick="lnkInvoiceableLoads_click" CausesValidation="false"></asp:LinkButton>
                            <%--<asp:Label ID="lblCancel" runat="server" Font-Bold="true"></asp:Label>--%>
                        </td>
                        <td style="background-color: White">
                            <%--<asp:Label ID="lblInvoiceable" runat="server" Font-Bold="true"></asp:Label>--%>
                            <asp:LinkButton ID="lnkLFDExpired" ForeColor="Coral" runat="Server" Font-Bold="true" OnClick="lnkLFDExpired_click" CausesValidation="false"></asp:LinkButton>
                        </td>
                        <td style="background-color: White">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tbldisplay">
                    <tr id="row">
                        <td style="background-color: White">
                            <asp:LinkButton ID="lnkDay0" Style="text-decoration: none;" ForeColor="Blue" runat="Server" Font-Bold="true" OnClick="lnkDay_click" CausesValidation="false"></asp:LinkButton></td>
                        <td style="background-color: White">
                            <asp:LinkButton ID="lnkDay1" Style="text-decoration: none;" ForeColor="Blue" runat="Server" Font-Bold="true" OnClick="lnkDay_click" CausesValidation="false"></asp:LinkButton></td>
                        <td style="background-color: White">
                            <asp:LinkButton ID="lnkDay2" Style="text-decoration: none;" ForeColor="Blue" runat="Server" Font-Bold="true" OnClick="lnkDay_click" CausesValidation="false"></asp:LinkButton></td>
                        <td style="background-color: White">
                            <asp:LinkButton ID="lnkDay3" Style="text-decoration: none;" ForeColor="Blue" runat="Server" Font-Bold="true" OnClick="lnkDay_click" CausesValidation="false"></asp:LinkButton></td>
                        <td style="background-color: White">
                            <asp:LinkButton ID="lnkDay4" Style="text-decoration: none;" ForeColor="Blue" runat="Server" Font-Bold="true" OnClick="lnkDay_click" CausesValidation="false"></asp:LinkButton></td>
                        <td style="background-color: White">
                            <asp:LinkButton ID="lnkDay5" Style="text-decoration: none;" ForeColor="Blue" runat="Server" Font-Bold="true" OnClick="lnkDay_click" CausesValidation="false"></asp:LinkButton></td>
                        <td style="background-color: White">
                            <asp:LinkButton ID="lnkDay6" Style="text-decoration: none;" ForeColor="Blue" runat="Server" Font-Bold="true" OnClick="lnkDay_click" CausesValidation="false"></asp:LinkButton></td>
                        <%--<td style="background-color:White"><asp:Label ID="lblDay0" runat="server" Font-Bold="true" ForeColor="black"></asp:Label></td>
    <td style="background-color:White"><asp:Label ID="lblDay1" runat="server" Font-Bold="true" ForeColor="black"></asp:Label></td>
    <td style="background-color:White"><asp:Label ID="lblDay2" runat="server" Font-Bold="true" ForeColor="black"></asp:Label></td>
    <td style="background-color:White"><asp:Label ID="lblDay3" runat="server" Font-Bold="true" ForeColor="black"></asp:Label></td>
    <td style="background-color:White"><asp:Label ID="lblDay4" runat="server" Font-Bold="true" ForeColor="black"></asp:Label></td>
    <td style="background-color:White"><asp:Label ID="lblDay5" runat="server" Font-Bold="true" ForeColor="black"></asp:Label></td>
    <td style="background-color:White"><asp:Label ID="lblDay6" runat="server" Font-Bold="true" ForeColor="black"></asp:Label></td>--%>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 100%">
                <table id="tblbox1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">Get Loads:
                            <asp:DropDownList ID="dropStatus" runat="server" OnSelectedIndexChanged="dropStatus_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:Label ID="lblNewStatus" runat="server" Text="with"></asp:Label>&nbsp;
                            <asp:DropDownList ID="ddlNewStatus" runat="server">
                                <asp:ListItem Text="LFD & Appt.Date" Value="LFD & Appt.Date" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="LFD & No Appt.Date" Value="LFD & No Appt.Date"></asp:ListItem>
                                <asp:ListItem Text="No LFD & Appt.Date" Value="No LFD & Appt.Date"></asp:ListItem>
                                <asp:ListItem Text="No LFD & No Appt.Date" Value="No LFD & No Appt.Date"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:Button ID="btnShow" runat="server" CssClass="btnstyle" OnClick="btnShow_Click"
                                Text="Show" /></td>
                        <td align="right">Track Load:
                            <asp:TextBox ID="txtContainer" runat="server">
                            </asp:TextBox>
                            <asp:DropDownList ID="ddlContainer" runat="server">
                                <asp:ListItem Selected="True" Value="1">Container#</asp:ListItem>
                                <asp:ListItem Value="2">Ref#</asp:ListItem>
                                <asp:ListItem Value="3">Booking#</asp:ListItem>
                                <asp:ListItem Value="4">Chasis#</asp:ListItem>
                                <asp:ListItem Value="5">Load#</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Button ID="btnSearch" runat="server" CssClass="btnstyle" OnClick="btnSearch_Click"
                                Text="Search" />
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="5" src="../images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>

                        <td>
                            <uc2:GridBar ID="barManager" runat="server" HeaderText="Loads" />
                        </td>

                    </tr>
                </table>

                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="5" src="../images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table id="tbldisplay1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <uc1:LoadGridManager ID="grdManager" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</asp:Content>
