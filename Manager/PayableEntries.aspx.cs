using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using PdfSharp;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using System.Drawing.Imaging;
using System.IO;

public partial class PayableEntries : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Payable  Entries";
        lblErrorMsg.Visible = false;
        if (!IsPostBack)
        {
            ddlDriverorCarrier.Visible = false;
            RadAll.Checked = true;
            dtpPrintCheckDate.SetDate(Convert.ToString(DateTime.Now.Date));
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        SetOldDates();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        lblErrorMsg.Visible = false;
        Session["SerchCriteria"] = null;
        Session["ReportParameters"] = null;
        Session["ReportName"] = null;
        Session["DriverWiseReport"] = null;
        string strTemp1="";
        string strTemp2 = "";
        string strTemp = "";
        switch (ddlContainer.SelectedIndex)
        {
            case 0:
                try
                {
                    if (txtContainer.Text.Trim().Length > 0)
                    {
                        strTemp1 = "[eTn_Load].[bint_LoadId]=" + Convert.ToInt64(txtContainer.Text.Trim());
                        strTemp = " Load Id = " + Convert.ToInt64(txtContainer.Text.Trim());
                    }
                    else
                    {
                        strTemp1 = "[eTn_Load].[bint_LoadId]>0";
                        strTemp = " All Loads";
                    }
                }
                catch
                {
                    lblErrorMsg.Visible = true;
                    return;
                }
                break;
            case 1:
                strTemp1 = "[eTn_Load].nvar_Container : '" + txtContainer.Text.Trim() + "%' or [eTn_Load].nvar_Container1 : '" + txtContainer.Text.Trim() + "%'";
                strTemp = (txtContainer.Text.Trim().Length > 0) ? " Container# : " + txtContainer.Text.Trim() : " All Containers";
                break;
            case 2:
                strTemp1 = "[eTn_Load].nvar_Booking : '" + txtContainer.Text.Trim() + "%'";
                strTemp = (txtContainer.Text.Trim().Length > 0) ? " Booking# : " + txtContainer.Text.Trim() : " All Bookings";
                break;
            case 3:
                strTemp1 = "[eTn_Load].nvar_Chasis : '" + txtContainer.Text.Trim() + "%' or [eTn_Load].nvar_Chasis1 : '" + txtContainer.Text.Trim() + "%'";
                strTemp = (txtContainer.Text.Trim().Length > 0) ? " Chasis# : " + txtContainer.Text.Trim() : " All Chasisses";
                break;
            case 4:
                strTemp2 = txtContainer.Text.Trim();
                strTemp = (txtContainer.Text.Trim().Length > 0) ? " Tag# : " + txtContainer.Text.Trim() : " All Tags";
                break;
        }
        Session["PayableSPName"] = "SP_GetPayableEntriesWithOutDates";
        Session["ReportParameters"] = strTemp1 + "~~^^^^~~" + strTemp2;
        Session["SerchCriteria"] = strTemp;
        Session["ReportName"] = "GetPayableEntries";
        Response.Redirect("~/Manager/DisplayReport.aspx");
    }
    protected void btnDateSearch_Click(object sender, EventArgs e)
    {
        string TempInnerJoin =null, TempwhereClause=null,strTempId=null;
        string driverOrcarrier = string.Empty;
        if (Page.IsValid)
        {
            Session["SerchCriteria"] = null;
            Session["ReportParameters"] = null;
            Session["ReportName"] = null;
            Session["DriverWiseReport"] = null;
            Session["PayableSPName"] = "SP_GetPayableEntriesWithDates";
            string strTemp = " From Date : " + dtpFromDate.Date + "&nbsp;&nbsp;&nbsp; To Date : " + dtpToDate.Date+"&nbsp;&nbsp;&nbsp; ";
            if (RadAll.Checked)
            {
                TempInnerJoin = "";
                TempwhereClause = "";
                strTemp += " All Drivers and Carriers";
                strTempId = "*";    
            }
            else if (RadDriver.Checked)
            {
                if (ddlDriverorCarrier.SelectedIndex > 0)
                {
                    TempInnerJoin = "inner join [eTn_AssignDriver] on [eTn_AssignDriver].[bint_LoadId]=";
                    TempwhereClause = " and [eTn_AssignDriver].[bint_EmployeeId]="+ddlDriverorCarrier.SelectedValue+"";
                    strTemp += " Driver name : " + ddlDriverorCarrier.SelectedItem.Text;
                    strTempId = ddlDriverorCarrier.SelectedValue;                    
                }
                else
                {
                    TempInnerJoin = "inner join [eTn_AssignDriver] on [eTn_AssignDriver].[bint_LoadId]=";
                    TempwhereClause = " and [eTn_AssignDriver].[bint_EmployeeId]>0";
                    strTemp += " All Drivers";
                    strTempId = "*";         
                }
            }
            else if (RadCarrier.Checked)
            {
                if (ddlDriverorCarrier.SelectedIndex > 0)
                {
                    TempInnerJoin = "inner join [eTn_AssignCarrier] on [eTn_AssignCarrier].[bint_LoadId]=";
                    TempwhereClause = " and [eTn_AssignCarrier].[bint_CarrierId]=" + ddlDriverorCarrier.SelectedValue + "";
                    strTemp += " Carrier name : " + ddlDriverorCarrier.SelectedItem.Text;
                    strTempId = "*";   
                }
                else
                {
                    TempInnerJoin = "inner join [eTn_AssignCarrier] on [eTn_AssignCarrier].[bint_LoadId]=";
                    TempwhereClause = " and [eTn_AssignCarrier].[bint_CarrierId]>0";
                    strTemp += " All Carriers";
                    strTempId = "*";   
                }
            }
            Session["ReportParameters"] = dtpFromDate.Date + "~~^^^^~~" + dtpToDate.Date + "~~^^^^~~" + TempInnerJoin + "~~^^^^~~" + TempwhereClause + "~~^^^^~~" + strTempId;
            Session["SerchCriteria"] = strTemp;
            Session["ReportName"] = "GetPayableEntries";
            Response.Redirect("~/Manager/DisplayReport.aspx");
        }
    }

    //Added by Raghu for driver wise print
    protected void btnExportDrives_Click(object sender, EventArgs e)
    {
        string TempInnerJoin = null;
        if (Page.IsValid)
        {
            Session["SerchCriteria"] = null;
            Session["ReportParameters"] = null;
            Session["ReportName"] = null;
            Session["DriverWiseReport"] = null;
            string driverIdandNames = null;
            foreach (ListItem item in ltbDrivers.Items)
            {
                if (item.Selected)
                {
                    if (string.IsNullOrEmpty(driverIdandNames))
                        driverIdandNames = item.Value + "`" + item.Text;
                    else
                        driverIdandNames = driverIdandNames + "~" + item.Value + "`" + item.Text;
                } 
            }
            if (!string.IsNullOrEmpty(driverIdandNames))
            {
                Session["DriverWiseReport"] = driverIdandNames;
                Session["PayableSPName"] = "SP_GetPayableEntriesWithDates";
                string strTemp = " From Date : " + dtpFromDate.Date + "&nbsp;&nbsp;&nbsp; To Date : " + dtpToDate.Date + "&nbsp;&nbsp;&nbsp; ";
                TempInnerJoin = "inner join [eTn_AssignDriver] on [eTn_AssignDriver].[bint_LoadId]=";

                Session["ReportParameters"] = dtpFromDate.Date + "~~^^^^~~" + dtpToDate.Date + "~~^^^^~~" + TempInnerJoin;
                Session["SerchCriteria"] = strTemp;
                Session["ReportName"] = "GetPayableEntries";
                ClientScript.RegisterStartupScript(typeof(Page), "OpenWindow", "window.open('DriverWisePrint.aspx');", true);
                //Response.Redirect("~/Manager/DriverWisePrint.aspx");
            }
        }
    }

    protected void btnPrintDrivesChecks_Click(object sender, EventArgs e)
    {
        DateTime PrintCheckDate = Convert.ToDateTime(dtpPrintCheckDate.Date);
        string TempInnerJoin = null;
        if (Page.IsValid)
        {
            string driverIdandNames = null;
            foreach (ListItem item in ltbDrivers.Items)
            {
                if (item.Selected)
                {
                    if (string.IsNullOrEmpty(driverIdandNames))
                        driverIdandNames = item.Value + "`" + item.Text;
                    else
                        driverIdandNames = driverIdandNames + "~" + item.Value + "`" + item.Text;
                }
            }
            if (!string.IsNullOrEmpty(driverIdandNames))
            {
                //SLine 2017 Enhancement for Office Location Starts
                long officeLocationId = 0;
                if (Session["OfficeLocationID"] != null)
                {
                    officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
                }
                //SLine 2017 Enhancement for Office Location Ends
                string strTemp = " From Date : " + dtpFromDate.Date + "&nbsp;&nbsp;&nbsp; To Date : " + dtpToDate.Date + "&nbsp;&nbsp;&nbsp; ";
                TempInnerJoin = "inner join [eTn_AssignDriver] on [eTn_AssignDriver].[bint_LoadId]=";
                DataSet ds = new DataSet();
                object[] objParams = null;
                PdfDocument doc = new PdfDocument();
                int i = 0;

                foreach (string idAndName in driverIdandNames.Split('~'))
                {

                    string id = idAndName.Split('`')[0].Trim();
                    string Name = idAndName.Split('`')[1].Trim();
                    objParams = ((dtpFromDate.Date + "~~^^^^~~" + dtpToDate.Date + "~~^^^^~~" + TempInnerJoin) + "~~^^^^~~" + (" and [eTn_AssignDriver].[bint_EmployeeId]=" + id + "") + "~~^^^^~~" + id + "~~^^^^~~" + officeLocationId).Trim().Replace(":", " like ").Replace("~~^^^^~~", "^").Split('^');
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetPayableEntriesWithDates", objParams);
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        string[] strPayments = Convert.ToString(ds.Tables[1].Rows[0][0]).Replace("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "^").Split('^');
                        if (strPayments.Length == 3)
                        {
                            string Pending = strPayments[2].Replace("<font color=red> Pending : ", string.Empty).Replace("</font>", string.Empty).Trim();
                            if (double.Parse(Pending) > 0)
                            {
                                doc.Pages.Add(new PdfPage());
                                XGraphics xgr = XGraphics.FromPdfPage(doc.Pages[i]);

                                objParams = null;
                                objParams = id.Split(' ');
                                DataSet ds1 = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriverDetails", objParams);
                                string empName = ((ds1.Tables[0].Rows[0][6].ToString() + " " + ds1.Tables[0].Rows[0][7].ToString()).Trim() + " " + ds1.Tables[0].Rows[0][8].ToString()).Trim().ToUpper();
                                string empAdd = (ds1.Tables[0].Rows[0][1].ToString() + " " + ds1.Tables[0].Rows[0][2].ToString()).Trim().ToUpper();
                                string empAdd1 = ((ds1.Tables[0].Rows[0][3].ToString() + ", " + ds1.Tables[0].Rows[0][4].ToString()).Trim()+" "+ ds1.Tables[0].Rows[0][5].ToString()).Trim().ToUpper();
                                PrintCheck pc = new PrintCheck("mADHAVA sWAMY", PrintCheckDate.Date.ToString("MM/dd/yyyy"), empName, empAdd, empAdd1, double.Parse(Pending), 800, 800, "Century Schoolbook");
                                // Change the response headers to output a JPEG image.

                                // Write the image to the response stream in JPEG format.
                                XImage img = XImage.FromGdiPlusImage(pc.Image);
                                xgr.PdfPage.Size = PageSize.Letter;
                                xgr.DrawImage(img, 0, 0);
                                i++;

                            }
                        }
                        objParams = null;



                        DataTable dt = ds.Tables[0];

                        if (dt != null) { dt.Dispose(); dt = null; }
                    }
                    if (ds != null) { ds.Dispose(); ds = null; }
                }
                //Response.Clear();
                //Response.BufferOutput = false;
                //String pdfName = "PrintCheck_" + DateTime.Now.ToString("yyyy_MMM_dd_HHmmss") + ".pdf";
                //Response.ContentType = "application/pdf";
                //Response.AddHeader("content-disposition", "attachment; filename=" + pdfName);
                //doc.Save(Server.MapPath("~/" + pdfName));
                if (doc.Pages.Count > 0)
                {
                    String pdfName = "PrintCheck_" + DateTime.Now.ToString("yyyy_MMM_dd_HHmmss") + ".pdf";
                    MemoryStream ms = new MemoryStream();
                    doc.Save(ms, false);
                    byte[] buffer = ms.GetBuffer();
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-disposition", "attachment; filename=" + pdfName);
                    Response.BinaryWrite(buffer);
                    Response.Flush();
                }


            }
        }
    }
        //Added by Raghu for driver wise print
    protected void FillCarriers()
    {
        ddlDriverorCarrier.Items.Clear();
        //DataTable dt = DBClass.returnDataTable("select bint_CarrierId,nvar_CarrierName from eTn_Carrier order by [nvar_CarrierName]");
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCarrierNames", new object[] { officeLocationId });
        //SLine 2017 Enhancement for Office Location Ends
        if (ds.Tables.Count > 0)
        {
            ddlDriverorCarrier.DataSource = ds.Tables[0];
            ddlDriverorCarrier.DataTextField = "nvar_CarrierName";
            ddlDriverorCarrier.DataValueField = "bint_CarrierId";
            ddlDriverorCarrier.DataBind();            
        }
        ddlDriverorCarrier.Items.Insert(0, "All");
        ddlDriverorCarrier.SelectedIndex = 0;
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    private void FillDrivers()
    {
        ddlDriverorCarrier.Items.Clear();
        //dt = DBClass.returnDataTable("select bint_EmployeeId as 'Id',nvar_NickName as 'Name' from eTn_Employee where bint_RoleId=(select R.bint_RoleId from eTn_Role R where lower(nvar_RoleName)='driver') order by [Name]");
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetActiveDriverNamesForaLocation", new object[] { officeLocationId });
        //SLine 2017 Enhancement for Office Location Ends
         if (ds.Tables.Count > 0)
         {
             ddlDriverorCarrier.DataSource = ds.Tables[0];
             ddlDriverorCarrier.DataTextField = "Name";
             ddlDriverorCarrier.DataValueField = "Id";
             ddlDriverorCarrier.DataBind();
             //Added by Raghu for driver wise print
             ltbDrivers.Items.Clear();
             ltbDrivers.DataSource = ds.Tables[0];
             ltbDrivers.DataTextField = "Name";
             ltbDrivers.DataValueField = "Id";
             ltbDrivers.DataBind();
             //Added by Raghu for driver wise print
         }
        ddlDriverorCarrier.Items.Insert(0, "All");
        ddlDriverorCarrier.SelectedIndex = 0;
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    protected void RadDriver_CheckedChanged(object sender, EventArgs e)
    {
        GetOldDates();
        ddlDriverorCarrier.Visible = true;
        //Added by Raghu for driver wise print
        ltbDrivers.Visible = true;
        btnExportDrives.Visible = true;
        btnPrintDrivesChecks.Visible = true;
        lblMsg.Visible = true;
        tblPrintCheckDate.Visible = true;
        //Added by Raghu for driver wise print
        FillDrivers();
    }
    protected void RadCarrier_CheckedChanged(object sender, EventArgs e)
    {
        GetOldDates();
        ddlDriverorCarrier.Visible = true;
        //Added by Raghu for driver wise print
        ltbDrivers.Visible = false;
        btnExportDrives.Visible = false;
        btnPrintDrivesChecks.Visible = false;
        lblMsg.Visible = false;
        tblPrintCheckDate.Visible = false;
        //Added by Raghu for driver wise print
        FillCarriers();
    }
    protected void RadAll_CheckedChanged(object sender, EventArgs e)
    {
        GetOldDates();
        ddlDriverorCarrier.Visible = false;
        //Added by Raghu for driver wise print
        ltbDrivers.Visible = false;
        btnExportDrives.Visible = false;
        btnPrintDrivesChecks.Visible = false;
        lblMsg.Visible = false;
        //Added by Raghu for driver wise print
    }

    private void GetOldDates()
    {
        ViewState["FromDate"] = dtpFromDate.Date;
        ViewState["ToDate"] = dtpToDate.Date;
    }
    private void SetOldDates()
    {
        if (ViewState["FromDate"] != null)
        {
            dtpFromDate.SetInitialDate = false;
            dtpToDate.SetInitialDate = false;
            dtpFromDate.Date = Convert.ToString(ViewState["FromDate"]);
            dtpToDate.Date = Convert.ToString(ViewState["ToDate"]);
            //dtpFromDate.SetDate(Convert.ToString(ViewState["FromDate"]));
            //dtpToDate.SetDate(Convert.ToString(ViewState["ToDate"]));
        }           
    }
}

//public class DriverWiseReportPableEntries
//{
//    private string innerHtml;

//    public string InnerHtml
//    {
//        get { return innerHtml; }
//        set { innerHtml = value; }
//    }

//    private string header;

//    public string Header
//    {
//        get { return header; }
//        set { header = value; }
//    }

//    private string searchCriteria;

//    public string SearchCriteria
//    {
//        get { return searchCriteria; }
//        set { searchCriteria = value; }
//    }

//    private string total;

//    public string Total
//    {
//        get { return total; }
//        set { total = value; }
//    }

//    private string paid;

//    public string Paid
//    {
//        get { return paid; }
//        set { paid = value; }
//    }

//    private string pending;

//    public string Pending
//    {
//        get { return pending; }
//        set { pending = value; }
//    }

//    private string count;

//    public string Count
//    {
//        get { return count; }
//        set { count = value; }
//    }
//}
