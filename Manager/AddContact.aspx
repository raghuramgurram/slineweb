<%@ Page AutoEventWireup="true" CodeFile="AddContact.aspx.cs" Inherits="AddContact"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/Url.ascx" TagName="Url" TagPrefix="uc2" %>

<%@ Register Src="~/UserControls/Phone.ascx" TagName="Phone" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td>
        <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <asp:ValidationSummary ID="ReqContacts" runat="server" Width="526px" />
                </td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblform">
            <tr style="background-color:#E1E1E1;">
                <th colspan="2">Contact Person Details </th>
            </tr>
            <tr id="row">
                <td width="30%" align="right"><asp:Label ID="lblName" runat="server" Text="Name : "/></td>
                <td><asp:TextBox ID="txtName" runat="Server" MaxLength="150"/>
                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="Please enter Name." ControlToValidate="txtName" Display="Dynamic">*</asp:RequiredFieldValidator></td> 
            </tr>
                
            <tr id="altrow">              
                <td align="right"><asp:Label ID="lblTitle" runat="server" Text="Title : "/></td>
                <td><asp:TextBox ID="txtTitle" runat="Server" MaxLength="100"/></td>
            </tr>
        
            <tr id="row">             
                <td align="right"><asp:Label ID="lblEmail" runat="server" Text="Email : "/></td>
                <td><asp:TextBox ID="txtEmail" runat="Server" MaxLength="100"/>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="Please enter Email." ControlToValidate="txtEmail" Display="Dynamic">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Please enter a valid Email." ControlToValidate="txtEmail" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td> 
            </tr>
        
            <tr id="altrow">
                <td align="right" valign="top" style="height: 20px"><asp:Label ID="lblWorkPhone" runat="server" Text="Work Phone : "/></td>
                <td style="height: 20px"><%--<asp:TextBox ID="txtWorkPhone" runat="Server" MaxLength="18"/>
                    <asp:RegularExpressionValidator ID="revPhone" runat="server" ErrorMessage="Please enter a valid Phone number in the format (123) 123-1234 or 123-123-1234." ControlToValidate="txtWorkPhone" Display="Dynamic" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">*</asp:RegularExpressionValidator>--%>
                    <uc1:Phone ID="txtWorkPhone" runat="server" Visible="true" IsRequiredField="true" />
                &nbsp;&nbsp;Extn.<asp:TextBox ID="txtPhoneExtn" runat="Server" MaxLength="6" Width="56px"/>
                    <asp:RegularExpressionValidator ID="RegExten" runat="server" ControlToValidate="txtPhoneExtn"
                        Display="Dynamic" ErrorMessage="Please enter valid extension." ValidationExpression="\d*">*</asp:RegularExpressionValidator></td>
            </tr>
        
            <tr id="row">                
                <td align="right" valign="top"><asp:Label ID="lblDirect" runat="server" Text="Direct : "/></td>
                <td>
                    <uc1:Phone ID="txtDirect" runat="server" IsRequiredField="false" />
                </td> 
            </tr>

            <tr id="altrow">               
                <td align="right"><asp:Label ID="lblMobile" runat="server" Text="Mobile : "/></td>
                <td><%--<asp:TextBox ID="txtMobile" runat="Server" MaxLength="18"/>--%>
                    <uc1:Phone ID="txtMobile" runat="server" Visible="true" IsRequiredField="false" />
                </td>
            </tr>
        
            <tr id="row">                
                <td align="right" style="height: 19px"><asp:Label ID="lblFax" runat="server" Text="Fax : "/></td>
                <td style="height: 19px"><%--<asp:TextBox ID="txtFax" runat="Server" MaxLength="18"/>
                    <asp:RegularExpressionValidator ID="revFax" runat="server" ErrorMessage="Please enter a valid Fax number in the format (123) 123-1234 or 123-123-1234." ControlToValidate="txtFax" Display="Dynamic" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">*</asp:RegularExpressionValidator>--%>
                    <uc1:Phone ID="txtFax" runat="server" Visible="true" IsRequiredField="false" />
                </td>
            </tr>

            <tr id="altrow">                
                <td align="right" style="height: 20px"><asp:Label ID="lblWebsiteURL" runat="server" Text="Website URL : "/></td>
                <td>
                    <uc2:Url ID="txtWebsiteURL" runat="server" IsRequired="false" />
                </td>
            </tr>
        
            <tr id="row" valign="top">                
                <td align="right"><asp:Label ID="lblNotes" runat="server" Text="Notes : "/></td>
                <td><asp:TextBox ID="txtNotes" runat="Server" Height="91px" TextMode="MultiLine" Width="326px" MaxLength="1000"/></td>
            </tr>           
          </table>
          
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
            </tr>
          </table>
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
            </tr>
          </table>           
        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
        <tr>
          <td align="right">
              &nbsp;<asp:Button ID="btnSave" runat="server" Height="22px" Text="Save" OnClick="btnSave_Click" CssClass="btnstyle"/>&nbsp;<asp:Button
                  ID="btnCancel" runat="server" CausesValidation="False" Height="22px" Text="Cancel"
                  UseSubmitBehavior="False" OnClick="btnCancel_Click" CssClass="btnstyle"/>
          </td>        
        </tr>
        </table>        
     </td>
   </tr>
 </table>      
 
</asp:Content>

