using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //lblErrMsg.Visible = false;
        Session["TopHeaderText"] = null;
        this.Page.SetFocus(txtusername.ClientID);
        if (!IsPostBack)
        {
            HttpCookie _userInfoCookie = Request.Cookies["UserInfo"];

            if (_userInfoCookie != null)
            {
                txtusername.Text = _userInfoCookie["UserName"];
                txtpassword.Attributes.Add("Value", _userInfoCookie["Password"]);
                chkRememberMe.Checked = true;
            }
        }
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        string strRedirect = "";
        try
        {
            lblErrMsg.Visible = false;
            FormsAuthentication.Initialize();
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["conString"], "SP_Check_Users", new object[] { txtusername.Text.Trim(), txtpassword.Text.Trim() });
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables.Count == 3)
                {
                    if (Convert.ToBoolean(ds.Tables[2].Rows[0][0]))
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, txtusername.Text.Trim(), DateTime.Now, DateTime.Now.AddHours(2), false, Convert.ToString(ds.Tables[0].Rows[0][2]), FormsAuthentication.FormsCookiePath);
                            string hash = FormsAuthentication.Encrypt(ticket);
                            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, hash);
                            //cookie.
                            Response.Cookies.Add(cookie);
                            Session["UserLoginId"] = Convert.ToInt64(ds.Tables[0].Rows[0][0]);
                            Session["UserId"] = Convert.ToString(ds.Tables[0].Rows[0][1]);
                            Session["UserEmailId"] = Convert.ToString(ds.Tables[0].Rows[0][4]);

                            CommonFunctions.InsertLogEntry(Convert.ToString(Session["UserId"]));
                            string allowedIPAddresses = GetFromXML.AllowedIPAddresses;
                             bool isValidIP = true;
                            switch (Convert.ToString(ds.Tables[0].Rows[0][2]).ToLower())
                            {
                                case "manager":
                                    Session["Role"] = "Manager";
                                    Session["Folder"] = "Manager";
                                    //Session["Role"] = "Staff";
                                    Session["ManagerId"] = Convert.ToString(ds.Tables[0].Rows[0][3]);
                                    Session["LogsId"] = Convert.ToString(ds.Tables[1].Rows[0][0]);
                                    Session["IsAdmin"] = Convert.ToBoolean(ds.Tables[0].Rows[0][5]);
                                    strRedirect = "Manager/DashBoard.aspx";
                                    break;
                                case "customer":
                                    Session["Role"] = "Customer";
                                    Session["Folder"] = "Customer";
                                    Session["CustomerId"] = Convert.ToString(ds.Tables[0].Rows[0][3]);
                                    Session["LogsId"] = Convert.ToString(ds.Tables[1].Rows[0][0]);
                                    strRedirect = "Customer/CustomerDashBoard.aspx";
                                    break;
                                case "driver":
                                    Session["Role"] = "Driver";
                                    Session["Folder"] = "Driver";
                                    Session["EmployeeId"] = Convert.ToString(ds.Tables[0].Rows[0][3]);
                                    Session["LogsId"] = Convert.ToString(ds.Tables[1].Rows[0][0]);
                                    strRedirect = "Driver/DriverDashBoard.aspx";
                                    break;
                                case "carrier":
                                    Session["Role"] = "Carrier";
                                    Session["Folder"] = "Carrier";
                                    Session["CarrierId"] = Convert.ToString(ds.Tables[0].Rows[0][3]);
                                    Session["LogsId"] = Convert.ToString(ds.Tables[1].Rows[0][0]);
                                    strRedirect = "Carrier/CarrierDashBoard.aspx";
                                    break;
                                    //SLine 2016 ---- Added a Special Permission for "Load Planner" view to the Dispatcher
                                case "dispatcher":
                                    if (!string.IsNullOrEmpty(allowedIPAddresses.Trim()))
                                    {
                                        string strIpAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                                        if (string.IsNullOrEmpty(strIpAddress))
                                        {
                                            strIpAddress = Request.ServerVariables["REMOTE_ADDR"];
                                        }
                                        if (!string.IsNullOrWhiteSpace(strIpAddress) && strIpAddress.Contains(":"))
                                            strIpAddress=strIpAddress.Split(':')[0];
                                        string[] str = allowedIPAddresses.Split(',');
                                        isValidIP = false;
                                        foreach (string tempStr in str)
                                        {
                                            if (tempStr.Trim() == strIpAddress.Trim())
                                            {
                                                isValidIP = true;
                                                break;
                                            }
                                        }
                                        if (!isValidIP)
                                        {
                                            SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_UpdateLogOutDate", new object[] { Convert.ToString(Session["LogsId"]) });
                                            FormsAuthentication.SignOut();
                                            Response.Cookies.Clear();
                                            Request.Cookies.Clear();
                                            Session.Clear();
                                            Session.Abandon();
                                            HttpContext.Current.Session.Abandon();
                                            HttpContext.Current.Response.Cookies.Clear();
                                            HttpContext.Current.Response.ContentType = "text/html";
                                            HttpContext.Current.Response.AddHeader("pragma", "no-cache");
                                            HttpContext.Current.Response.AddHeader("cache-control", "private, no-cache, must-revalidate");
                                            Response.Write("<script>alert('You are not allowed to access from this system("+ strIpAddress + ").')</script>");
                                            break;
                                        }

                                    }
                                    Session["Role"] = "Dispatcher";
                                    Session["LoadPlannerViewPermission"] = ds.Tables[0].Rows[0]["bit_PermissionFlag"].ToString();
                                    Session["Folder"] = "Manager";
                                    Session["DispactherId"] = Convert.ToString(ds.Tables[0].Rows[0][3]);
                                    Session["LogsId"] = Convert.ToString(ds.Tables[1].Rows[0][0]);
                                    strRedirect = "Manager/DashBoard.aspx";
                                    break;
                                case "staff":
                                    if (!string.IsNullOrEmpty(allowedIPAddresses.Trim()))
                                    {
                                        string strIpAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                                        if (string.IsNullOrEmpty(strIpAddress))
                                        {
                                            strIpAddress = Request.ServerVariables["REMOTE_ADDR"];
                                        }
                                        if (!string.IsNullOrWhiteSpace(strIpAddress) && strIpAddress.Contains(":"))
                                            strIpAddress = strIpAddress.Split(':')[0];
                                        string[] str= allowedIPAddresses.Split(',');
                                        isValidIP = false;
                                        foreach (string tempStr in str)
                                        {
                                            if (tempStr.Trim() == strIpAddress.Trim())
                                            {
                                                isValidIP = true;
                                                break;
                                            }
                                        }
                                        if (!isValidIP)
                                        {
                                            SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_UpdateLogOutDate", new object[] { Convert.ToString(Session["LogsId"]) });
                                            FormsAuthentication.SignOut();
                                            Response.Cookies.Clear();
                                            Request.Cookies.Clear();
                                            Session.Clear();
                                            Session.Abandon();
                                            HttpContext.Current.Session.Abandon();
                                            HttpContext.Current.Response.Cookies.Clear();
                                            HttpContext.Current.Response.ContentType = "text/html";
                                            HttpContext.Current.Response.AddHeader("pragma", "no-cache");
                                            HttpContext.Current.Response.AddHeader("cache-control", "private, no-cache, must-revalidate");
                                            Response.Write("<script>alert('You are not allowed to access from this system(" + strIpAddress + ").')</script>");
                                            break;
                                        }

                                    }
                                    
                                    Session["Role"] = "Staff";
                                    Session["Folder"] = "Manager";
                                    Session["StaffId"] = Convert.ToString(ds.Tables[0].Rows[0][3]);
                                    Session["LogsId"] = Convert.ToString(ds.Tables[1].Rows[0][0]);
                                    strRedirect = "Manager/DashBoard.aspx";
                                    break;
                            }
                            Session["LogsId"] = DBClass.executeScalar(" select ident_current('eTn_UserLogs')");
                            if (chkRememberMe.Checked == true && isValidIP)
                            {
                                HttpCookie _userInfoCookies = new HttpCookie("UserInfo");

                                //Setting values inside it
                                _userInfoCookies["UserName"] = txtusername.Text;
                                _userInfoCookies["Password"] = txtpassword.Text;
                                _userInfoCookies.Expires = DateTime.Now.AddMonths(1);

                                //Adding cookies to current web response
                                Response.Cookies.Add(_userInfoCookies);

                                //Response.Cookies["UName"].Value = txtusername.Text;
                                //Response.Cookies["PWD"].Value =txtpassword.Text;
                                //Response.Cookies["UName"].Expires = DateTime.Now.AddMonths(2);
                                //Response.Cookies["PWD"].Expires = DateTime.Now.AddMonths(2);
                            }
                            else
                            {
                                //Response.Cookies["UName"].Expires = DateTime.Now.AddMonths(-1);
                                //Response.Cookies["PWD"].Expires = DateTime.Now.AddMonths(-1);

                                if (Request.Cookies["UserInfo"] != null)
                                {
                                    Response.Cookies["UserInfo"].Expires = DateTime.Now;
                                }
                            }
                        }
                        else
                        {
                            lblErrMsg.Visible = true;
                            this.Page.SetFocus(txtpassword.ClientID);
                        }
                    }
                    else
                    {
                        lblErrMsg.Visible = true;
                        this.Page.SetFocus(txtpassword.ClientID);

                    }
                }
                else
                {
                    lblErrMsg.Visible = true;
                    this.Page.SetFocus(txtpassword.ClientID);
                }
            }
            else
            {
                lblErrMsg.Visible = true;
                this.Page.SetFocus(txtpassword.ClientID);
            }
            if (ds != null) { ds.Dispose(); ds = null; }
            //SLine 2017 Enhancement for Office Location Starts
            long UserLoginId = (long)Session["UserLoginId"];
            DataSet locationDs = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["conString"], "Get_OfficeLocation_ForTopMenu", new object[] { UserLoginId });
            Session["LocationDS"] = locationDs;
            if (locationDs != null) { locationDs.Dispose(); locationDs = null; }
            //SLine 2017 Enhancement for Office Location Ends
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (strRedirect.Trim().Length > 0)
            {
                Response.Redirect(strRedirect, false);
            }
        }
    }


}
