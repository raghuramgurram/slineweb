<%@ Page AutoEventWireup="true" CodeFile="SearchDriver.aspx.cs" Inherits="SearchDriver"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="../UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc2" %>

<%@ Register Src="../UserControls/Grid.ascx" TagName="Grid" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>
                <%--<table id="tblbox1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr style="background-color: #FFFFFF;">
                        <td align="left" valign="middle">
                           Show Assigned Drivers by :&nbsp;
                            <asp:Button ID="btnSearch" runat="server" CssClass="btnStyle" Text="Search" OnClick="btnSearch_Click" />                            
                   </tr>
                </table>--%>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="height: 5px">
                            <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
               
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="height: 28px">
                            <uc2:GridBar ID="GridBar1" runat="server" HeaderText="Assigned Drivers" />                            
                       </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="3" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding-right:0px;padding-right:0px;">
                    <tr>
                        <td align="left" width="100%">
                            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
                                CssClass="Grid" PageSize="15" Width="100%" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
                                <PagerStyle Font-Size="Smaller" Height="20px" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:BoundField DataField="DriverName" HeaderText="Driver Name" SortExpression="DriverName">
                                        <ItemStyle Width="40%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Location" HeaderText="Location"
                                        SortExpression="Location" >
                                        <ItemStyle Width="40%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" >
                                        <ItemStyle Width="20%" />
                                    </asp:BoundField>
                                 </Columns>
                                <RowStyle BorderColor="White" CssClass="GridItem"/>
                                <HeaderStyle BorderColor="White" CssClass="GridHeader" ForeColor="Black"/>
                                <AlternatingRowStyle CssClass="GridAltItem" />
                                <FooterStyle Font-Strikeout="False" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:eTransportString %>"
                                SelectCommand="Get_dispatcher_AssignedDriverDetails" SelectCommandType="StoredProcedure">
                            </asp:SqlDataSource>
                            &nbsp;
                        </td>
                    </tr>
                </table>                           
            </td>
        </tr>
    </table>
</asp:Content>

