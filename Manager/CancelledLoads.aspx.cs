using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Manager_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {            
            if (Request.QueryString.Count > 0)
            {
                Session["SerchCriteria"] = null;
                Session["ReportParameters"] = null;
                Session["ReportName"] = null;
                if (Convert.ToString(Request.QueryString["Type"]) == "Cancel")
                {
                    Session["ReportName"] = "CancelledLoads";
                    Session["TopHeaderText"] = "Cancelled Loads";
                    lblText.Visible = false;
                    ddlShipingLines.Visible = false;
                }
                else
                {
                    lblText.Visible = true;
                    ddlShipingLines.Visible = true;
                    FillLists();
                    Session["TopHeaderText"] = "Closed Loads";
                    Session["ReportName"] = "ClosedLoads";
                }
            }
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //SLine 2017 Enhancement for Office Location Starts
        string fromLoad = "0";
        string toLoad = "9223372036854775807";
        if (!string.IsNullOrEmpty(txtFromLoad.Text.Trim()))
        {
            fromLoad = txtFromLoad.Text.Trim();
        }
        if (!string.IsNullOrEmpty(txtToLoad.Text.Trim()))
        {
            toLoad = txtToLoad.Text.Trim();
        }
        //SLine 2017 Enhancement for Office Location Ends
        if (Convert.ToString(Request.QueryString["Type"]) == "Cancel")
        {
            Session["ReportParameters"] = DatePicker1.Date + "~~^^^^~~" + DatePicker2.Date + "~~^^^^~~" + fromLoad + "~~^^^^~~" + toLoad;
            Session["SerchCriteria"] = " From Date : " + DatePicker1.Date + "&nbsp;&nbsp;&nbsp; To Date : " + DatePicker2.Date + "&nbsp;&nbsp;&nbsp; Load # between&nbsp;" + fromLoad + " To " + (toLoad == "9223372036854775807" ? "All" : toLoad) + " Loads";
        }
        else
        {
            Session["ReportParameters"] = DatePicker1.Date + "~~^^^^~~" + DatePicker2.Date + "~~^^^^~~" + fromLoad + "~~^^^^~~" + toLoad + "~~^^^^~~" + ddlShipingLines.SelectedValue;
            Session["SerchCriteria"] = " From Date : " + DatePicker1.Date + "&nbsp;&nbsp;&nbsp; To Date : " + DatePicker2.Date + "&nbsp;&nbsp;&nbsp; Load # between&nbsp;" + fromLoad + " To " + (toLoad == "9223372036854775807" ? "All" : toLoad) + " Loads" + "&nbsp;&nbsp;&nbsp; Shipping Line : " + ddlShipingLines.SelectedItem.Text;
        }
        Response.Redirect("~/Manager/DisplayReport.aspx");
    }
    private void FillLists()
    {
        ddlShipingLines.Items.Clear();
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        //SLine 2017 Enhancement for Office Location Ends
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetActiveShipingLines", new object[] { officeLocationId });
        if (ds.Tables.Count > 0)
        {
            ddlShipingLines.DataSource = ds.Tables[0];
            ddlShipingLines.DataTextField = "ShippingLineName";
            ddlShipingLines.DataValueField = "Id";
            ddlShipingLines.DataBind();
            ddlShipingLines.Items.Insert(0, new ListItem("All","0"));
        }
        if (ds != null) { ds.Dispose(); ds = null; }
    }
}
