using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

public partial class Manager_ProfitLoss : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Profit and Loss";
        if (Convert.ToBoolean(Session["IsAdmin"]))
        {
            spnAllOfficeLocation.Style.Add(HtmlTextWriterStyle.Display, "");
        }
        else
        {
            spnAllOfficeLocation.Style.Add(HtmlTextWriterStyle.Display, "none");
        }
        
        if (!IsPostBack)
            FillLists();
    }

    private void FillLists()
    {
        ddlMonth.SelectedValue = DateTime.Today.Month.ToString();
        int currentYear = DateTime.Today.Year;
        for (int i = 8; i >= 0; i--)
        {
            ddlYear.Items.Add((currentYear - i).ToString());
        }
        ddlYear.Items.Add((currentYear + 1).ToString());
        ddlYear.SelectedValue = currentYear.ToString();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Session["AllOfficeLocationChecked"] = chkAllOfficeLocation.Checked;
        Session["ReportParameters"] = ddlMonth.SelectedValue + "~~^^^^~~" + ddlYear.SelectedValue + "~~^^^^~~" + getLongName(Convert.ToInt16(ddlMonth.SelectedValue)) + "~~^^^^~~" + (chkAllOfficeLocation.Checked == true ? "1" : "0");
        Session["SerchCriteria"] = null;
        Session["ReportName"] = "GetProfitandLoss";
        //Response.Redirect("~/Manager/DisplayReport.aspx");
        Response.Redirect("~/Manager/DisplayPLReport.aspx");
    }

    protected void btnMonthWisePLReport_Click(object sender, EventArgs e)
    {
        Session["AllOfficeLocationChecked"] = chkAllOfficeLocation.Checked;
        Session["ReportParameters"] = ddlMonth.SelectedValue + "~~^^^^~~" + ddlYear.SelectedValue + "~~^^^^~~" + "Monthly" + "~~^^^^~~" + (chkAllOfficeLocation.Checked == true ? "1" : "0");
        Session["SerchCriteria"] = null;
        Session["ReportName"] = "GetProfitandLossMonthWise";
        Response.Redirect("~/Manager/DisplayReport.aspx");
    }

    /*protected void btnChartMonthly_Click(object sender, EventArgs e)
    {
        Session["ReportParameters"] = ddlMonth.SelectedValue + "~~^^^^~~" + ddlYear.SelectedValue + "~~^^^^~~" + getLongName(Convert.ToInt16(ddlMonth.SelectedValue));
        Response.Redirect("~/Manager/ProfitLossCharts.aspx?type=1");
    }

    protected void btnChartDaily_Click(object sender, EventArgs e)
    {
        Session["ReportParameters"] = ddlMonth.SelectedValue + "~~^^^^~~" + ddlYear.SelectedValue + "~~^^^^~~" + getLongName(Convert.ToInt16(ddlMonth.SelectedValue));
        Response.Redirect("~/Manager/ProfitLossCharts.aspx?type=2");
    }*/

    private string getLongName(int key)
    {
        string[] Months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
        return Months[key-1];
    }
}
