using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Customer_AddAppointmentDate_ : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString.Count > 0 && Session["UserId"] != null)
            {
                string[] strParams = Convert.ToString(Request.QueryString[0]).Trim().Split('^');
                ViewState["LoadId"] = strParams[0];
                ViewState["DestinationId"] = strParams[1];
                FillDeatails(strParams[0], strParams[1]);
                strParams = null;
            }
        }
    }
    private void FillDeatails(string LoadId, string DestinationId)
    {
        if (Session["UserId"] != null)
        {
            string strQuery = "select " + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName;eTn_Location.nvar_Street;eTn_Location.nvar_Suite;eTn_Location.nvar_City;eTn_Location.nvar_State;eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") +
                            ",eTn_Destination.date_DeliveryAppointmentDate,eTn_Destination.nvar_App,eTn_Destination.date_DeliveryAppointmentToDate from eTn_Destination inner join eTn_Location on eTn_Destination.bint_LocationId = eTn_Location.bint_LocationId  inner join eTn_Company on eTn_Location.bint_CompanyId = eTn_Company.bint_CompanyId " +
                            " where eTn_Destination.[bint_LoadId]=" + LoadId + " and eTn_Destination.bint_DestinationId=" + DestinationId;
            DataTable dt = DBClass.returnDataTable(strQuery);
            if (dt.Rows.Count > 0)
            {
                lblDestination.Text = dt.Rows[0][0].ToString();
                lblLoadId.Text = LoadId;
                lblUserId.Text = Convert.ToString(Session["UserId"]);
                try
                {

                    if (dt.Rows[0][1] != null && Convert.ToString(dt.Rows[0][1]).Length > 0)
                    {
                        DateTime Dtime = Convert.ToDateTime(dt.Rows[0][1]);
                        dtpAppDate.Date = Dtime.ToString();
                        TimePicker1.Time = Dtime.ToShortTimeString();
                    }
                    if (dt.Rows[0][3] != null && Convert.ToString(dt.Rows[0][3]).Length > 0)
                    {
                        txtTOtime.Time = Convert.ToDateTime(dt.Rows[0][3]).ToShortTimeString();
                    }
                }
                catch
                { }
                txtAppt.Text = Convert.ToString(dt.Rows[0][2]);
            }
        }
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        if (ViewState["LoadId"] != null && ViewState["DestinationId"] != null)
        {
            Response.Redirect("~/Customer/CustomerTrackLoad.aspx?" + Convert.ToInt64(ViewState["LoadId"]));
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ViewState["LoadId"] != null && ViewState["DestinationId"] != null)
        {
            if (Page.IsValid)
            {
                if (!dtpAppDate.IsValidDate)
                    return;
                //Convert.ToDateTime(Dpicker.Date + " " + Tpicker.Time)
                if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Update_DestinationAppDate", new object[] { Convert.ToString(Session["UserLoginId"]), Convert.ToInt64(ViewState["DestinationId"]), Convert.ToInt64(ViewState["LoadId"]), CommonFunctions.CheckDateTimeNull(dtpAppDate.Date + " " + TimePicker1.Time), txtAppt.Text.Trim(), Convert.ToString(Session["UserId"]), CommonFunctions.CheckDateTimeNull((txtTOtime.Time != null && txtTOtime.Time.Length > 0) ? dtpAppDate.Date + " " + txtTOtime.Time.Trim() : null) }) > 0)
                {
                    CommonFunctions.SendEmail(Convert.ToString(Session["UserEmailId"]), GetFromXML.NotificationToEmail, "", "",
                    "Addition of new Delivery appointment date for Load Id = " + lblLoadId.Text.Trim() + " and Destination id = " + Convert.ToString(ViewState["DestinationId"]),
                    "<br/>" + Convert.ToString(Session["UserId"]) + " added a Delivery appointment date " + Convert.ToDateTime(dtpAppDate.Date+" "+TimePicker1.Time) + " for Load Id = " + lblLoadId.Text.Trim() + " and for destination <br/>" +
                         lblDestination.Text.Trim(), null,null);

                    Response.Redirect("~/Customer/CustomerTrackLoad.aspx?" + Convert.ToInt64(ViewState["LoadId"]));
                }
            }
        }
    }
}
