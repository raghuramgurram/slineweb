using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.IO;

public partial class UserControls_MulFileUpload1 : System.Web.UI.UserControl
{
    public long LoadID
    {
        set { ViewState["LoadID"] = value; }
    }
    public long DocumentID
    {
        set { ViewState["DocumentID"] = value; }
    }
    public int FileNo
    {
        get { return (int)ViewState["FileNo"]; }
        set 
        {
           ViewState["FileNo"] = value; 
        }
    }
    public bool bDelete
    {
        get 
        {
            if (ViewState["bDelete"] != null)
                return (bool)ViewState["bDelete"];
            else
                return false;
        }
        set { ViewState["bDelete"] = value; }
    }
    public bool bAdd
    {
        get 
        {
            if (ViewState["bAdd"] != null)
                return (bool)ViewState["bAdd"];
            else
                return false;
        }
        set { ViewState["bAdd"] = value; }
    }
    public string Files
    {
        get{return (string)ViewState["Files"];}
         set{ViewState["Files"]=value;}
    }
    public int FileCount
    {
        get { return lstFileNames.Items.Count; }
    }
    public List<HttpPostedFile> PostedFiles
    {
        get 
        {
            if (Session["PostedFiles"] != null)
                return (List<HttpPostedFile>)Session["PostedFiles"];
            else
                return null;
        }
    }
    public void BindData(DataTable dt)
    {
        lstFileNames.Items.Clear();
        foreach (DataRow drow in dt.Rows)
        {            
            lstFileNames.Items.Add(drow[0].ToString());
        }
    }
    public void GetFileNo(long LoadId, long DocumentId)
    {
        object obj = DBClass.executeScalar("select isnull(Max(isnull([bint_FileNo],0)),0) from [eTn_UploadDocuments] where [bint_LoadId]=" + LoadId + " and [bint_DocumentId]=" + DocumentId);
        ViewState["FileNo"] = Convert.ToInt32(DBClass.executeScalar("select isnull(Max(isnull([bint_FileNo],0)),0) from [eTn_UploadDocuments] where [bint_LoadId]=" + LoadId + " and [bint_DocumentId]=" + DocumentId));
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        /* Code Written By Sudha */
        if (Session["FilesSaved"] != null && Convert.ToBoolean(Session["FilesSaved"].ToString()) == true)
        {
            ClearItems();
        }
        /* Code Written By Sudha */
        if (!IsPostBack)
            
            GetFileNo(Convert.ToInt64(ViewState["LoadID"]), Convert.ToInt64(ViewState["DocumentID"]));
    }
    protected void btnAddToList_Click(object sender, EventArgs e)
    {
        if (FileUpload.PostedFile.ContentLength > 0)
        {
            FileInfo fileinfo=new FileInfo(FileUpload.PostedFile.FileName);
            //if (fileinfo.Exists)
            //{
                List<HttpPostedFile> postedfiles = null;
                if (Session["PostedFiles"] != null)
                    postedfiles = (List<HttpPostedFile>)Session["PostedFiles"];
                else
                    postedfiles = new List<HttpPostedFile>();
                postedfiles.Add(FileUpload.PostedFile);
                Session["PostedFiles"] = postedfiles;
                postedfiles = null;
                lstFileNames.Items.Add(fileinfo.Name.Trim());
                ViewState["bAdd"] = true;
                if (ViewState["Files"] != null)
                    ViewState["Files"] = (string)ViewState["Files"] + ";" + fileinfo.Name + "^" + "A";
                else
                    ViewState["Files"] = fileinfo.Name + "^" + "A";
            //}
            //else
            //{
            //    Response.Write("<script>alert('" + fileinfo.FullName + " does not exist.')</script>");
            //}
        }
        //if (lstFileNames.Items.Count > 0)
        //{
        //    Session["NoFiles"] = false;
        //}
    }

    /* Code Written By Sudha */

    private void ClearItems()
    {
        lstFileNames.Items.Clear();
        Session["FilesSaved"] = null;
    }

    /* Code Written By Sudha  */

    private string GenarateFileName(string fname,string FileExt)
    {
        string FileName = string.Empty;
        FileName = Convert.ToString(ViewState["LoadID"]) + "_" + Convert.ToString(ViewState["DocumentID"]) + "_" + Convert.ToString(ViewState["FileNo"]) + "." + FileExt;
        return FileName;
    }
    protected void btnDel_Click(object sender, EventArgs e)
    {

        if (lstFileNames.Items.Count > 0 && lstFileNames.SelectedItem != null)
        {
            if (ViewState["Files"] != null && Convert.ToString(ViewState["Files"]).Length > 0)
                ViewState["Files"] = (string)ViewState["Files"] + ";" + lstFileNames.SelectedItem.Text + "^" + "D";
            else
                ViewState["Files"] = lstFileNames.SelectedItem.Text + "^" + "D";

            lstFileNames.Items.Remove(lstFileNames.SelectedItem);
            ViewState["bDelete"] = true;
        }
        if(lstFileNames.Items.Count==0)
        {
            Session["NoFiles"] = true;
        }
    }
}
