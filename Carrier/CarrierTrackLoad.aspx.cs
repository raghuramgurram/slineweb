using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class CarrierTrackLoad : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Track Load";
        if (!IsPostBack)
        {
            if (Request.QueryString.Count > 0 && Session["CarrierId"]!=null)
            {
                try
                {
                    ViewState["ID"] = Convert.ToInt64(Request.QueryString[0]);
                    FillDetails(Convert.ToInt64(ViewState["ID"]));
                }
                catch 
                { 
                }
            }
        }
    }
    private void FillDetails(long ID)
    {
        string strAssigned = null, strEnterBy = null;
        long ShippingId = 0;
        string strQuery = "";
        DataTable dt;

            
        lblLoadId.Text = Convert.ToString(ID);
        lnkAdds.PostBackUrl = "CarrierInvoiceAdds.aspx?"+ID;
       
        //Origin Grid
        Grid1.Visible = true;
        Grid1.DeleteVisible = false;
        Grid1.EditVisible = false;
        Grid1.TableName = "eTn_Origin";
        Grid1.Primarykey = "eTn_Origin.bint_OriginId";
        Grid1.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        Grid1.ColumnsList = "eTn_Origin.bint_OriginId 'Id';" + CommonFunctions.AddressQueryStringWithLinks("eTn_Company.nvar_CompanyName;eTn_Location.nvar_Street;eTn_Location.nvar_Suite;eTn_Location.nvar_City;eTn_Location.nvar_State;eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "eTn_Origin.date_PickupDateTime", "Origin", "AddPickUpdate.aspx", "convert(varchar(20),eTn_Origin.[bint_LoadId])+'^'+convert(varchar(20),eTn_Origin.bint_OriginId)", "Add PickUp Date") + ";eTn_Origin.nvar_PO as 'PO';eTn_Origin.bint_Pieces as 'Pieces';eTn_Origin.num_Weight as 'Weight';case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' To '+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end  as 'Date';eTn_Origin.nvar_App as 'Appt';eTn_Origin.nvar_ApptGivenby as 'ApptGivenBy'";
        Grid1.VisibleColumnsList = "Origin;Date;Appt;ApptGivenBy";
        Grid1.VisibleHeadersList = "Origin(s);Pickup Date /Time;Appt.#;Appt. Given by";
        Grid1.InnerJoinClause = " inner join eTn_Location on eTn_Origin.bint_LocationId = eTn_Location.bint_LocationId " +
               " inner join eTn_Company on eTn_Location.bint_CompanyId = eTn_Company.bint_CompanyId ";
        Grid1.WhereClause = "eTn_Origin.[bint_LoadId]=" + ID;
        Grid1.IsPagerVisible = false;
        Grid1.BindGrid(0);

        //Destination Grid
        Grid2.Visible = true;
        Grid2.DeleteVisible = false;
        Grid2.EditVisible = false;
        Grid2.TableName = "eTn_Destination";
        Grid2.Primarykey = "eTn_Destination.bint_DestinationId";
        Grid2.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        Grid2.ColumnsList = "eTn_Destination.bint_DestinationId as 'Id';" + CommonFunctions.AddressQueryStringWithLinks("eTn_Company.nvar_CompanyName;eTn_Location.nvar_Street;eTn_Location.nvar_Suite;eTn_Location.nvar_City;eTn_Location.nvar_State;eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "eTn_Destination.date_DeliveryAppointmentDate", "Destination", "AddAppointmentDate.aspx", "convert(varchar(20),eTn_Destination.[bint_LoadId])+'^'+convert(varchar(20),eTn_Destination.bint_DestinationId)", "Add Appointment Date ") + ";eTn_Destination.nvar_PO as 'PO';eTn_Destination.bint_Pieces as 'Pieces';eTn_Destination.num_Weight as 'Weight';case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' To '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Date';eTn_Destination.nvar_App as 'Appt';eTn_Destination.nvar_ApptGivenby as 'ApptGivenBy'";
        Grid2.VisibleColumnsList = "Destination;Date;Appt;ApptGivenBy";
        Grid2.VisibleHeadersList = "Destination(s);Appt. Date /Time;Appt.#;Appt. Given by";
        Grid2.InnerJoinClause = " inner join eTn_Location on eTn_Destination.bint_LocationId = eTn_Location.bint_LocationId " +
               " inner join eTn_Company on eTn_Location.bint_CompanyId = eTn_Company.bint_CompanyId ";
        Grid2.WhereClause = "eTn_Destination.[bint_LoadId]=" + ID;
        Grid2.IsPagerVisible = false;
        Grid2.BindGrid(0);

        //grid LoadStatus Information
        Grid4.Visible = true;
        Grid4.DeleteVisible = false;
        Grid4.EditVisible = false;
        Grid4.TableName = "eTn_TrackLoad";
        Grid4.Primarykey = "eTn_TrackLoad.bint_TrackLoadId";
        //Grid4.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        Grid4.ColumnsList = "eTn_LoadStatus.nvar_LoadStatusDesc as 'Status';eTn_TrackLoad.nvar_Location as 'Loc';eTn_TrackLoad.date_CreateDate as 'Date';eTn_TrackLoad.nvar_Notes as 'Notes'";
        Grid4.VisibleColumnsList = "Status;Loc;Date;Notes";
        Grid4.VisibleHeadersList = "Status;Location;Date / Time;Comments";
        Grid4.InnerJoinClause = " inner join eTn_LoadStatus on eTn_LoadStatus.bint_LoadStatusId = eTn_TrackLoad.bint_LoadStatusId ";
        Grid4.WhereClause = "eTn_TrackLoad.[bint_LoadId]=" + ID;
        Grid4.IsPagerVisible = false;
        Grid4.BindGrid(0);

        //Grid Notes
        if (Session["UserLoginId"] != null)
        {
            Grid5.Visible = true;
            Grid5.DeleteVisible = false;
            Grid5.EditVisible = false;
            Grid5.TableName = "eTn_Notes";
            Grid5.Primarykey = "eTn_Notes.bint_NoteId";
            Grid5.PageSize = 10;
            Grid5.ColumnsList = "eTn_UserLogin.nvar_FirstName+' '+eTn_UserLogin.nvar_LastName as 'EnterBy';eTn_Notes.date_CreateDate as 'Date';eTn_Notes.nvar_NotesDesc as 'Notes'";
            Grid5.VisibleColumnsList = "EnterBy;Date;Notes";
            Grid5.VisibleHeadersList = "Entered By ;Date / Time;Comments";
            Grid5.InnerJoinClause = " inner join [eTn_Load] on [eTn_Load].bint_LoadId =[eTn_Notes].[bint_LoadId] inner join eTn_UserLogin on [eTn_UserLogin].bint_UserLoginId = [eTn_Notes].bint_UserLoginId";
            Grid5.WhereClause = "eTn_Notes.[bint_LoadId]=" + ID;
            Grid5.IsPagerVisible = true;
            Grid5.BindGrid(0);
        }
       // GridBar3.HeaderText = "Notes";
       // GridBar3.LinksList = "Add Note";
       // GridBar3.PagesList = "AddNotes.aspx?" + Convert.ToInt64(ID);
        lnkNotes.PostBackUrl = "AddNotes.aspx?" + Convert.ToInt64(ID);
        //added a collumn for hazmat(madhav)
        strQuery = "SELECT C.[nvar_CustomerName],L.[nvar_Pickup],L.[nvar_Container],L.[nvar_Container1],L.[nvar_Chasis],L.[nvar_Chasis1],CT.[nvar_CommodityTypeDesc],L.[nvar_Seal],L.[date_DateOfCreation],L.[nvar_AssignTo],S.[nvar_LoadStatusDesc],L.[bint_ShippingId],eTn_AssignCarrier.[nvar_Invoice]," +
                    "L.[nvar_RailBill],L.[date_OrderBillDate],L.[nvar_PersonalBill],S.[nvar_LoadStatusDesc],L.[date_LastFreeDate] , L.[bit_IsHazmatLoad] FROM [eTn_Load] L " +
                    "inner join [eTn_Customer] C on L.[bint_CustomerId]=C.[bint_CustomerId] inner join [eTn_CommodityType] CT on L.[bint_CommodityTypeId]=CT.[bint_CommodityTypeId]  inner join [eTn_LoadStatus] S on L.[bint_LoadStatusId]=S.[bint_LoadStatusId] " +
                    "inner join [eTn_AssignCarrier] on L.[bint_LoadId]=[eTn_AssignCarrier].[bint_LoadId]" +
                    " WHERE L.[bint_LoadId]=" + ID + " and Lower(L.[nvar_AssignTo])='carrier' and [eTn_AssignCarrier].[bint_CarrierId]=" + Convert.ToInt64(Session["CarrierId"]);
        dt = DBClass.returnDataTable(strQuery);
        if (Session["ViewLog"] != null)
        {
            Session["ViewLog"] = null;
            SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_LoadView_Log", new object[] { ID, Convert.ToString(Session["UserLoginId"]) });
        }
        if (dt.Rows.Count > 0)
        {
            //lblCustomer.Text = Convert.ToString(dt.Rows[0][0]);
            lblPickUp.Text = Convert.ToString(dt.Rows[0][1]);
            if (Convert.ToString(dt.Rows[0][3]).Trim().Length > 0)
                lblContainer.Text = Convert.ToString(dt.Rows[0][2]) + "<br/>" + Convert.ToString(dt.Rows[0][3]);
            else
                lblContainer.Text = Convert.ToString(dt.Rows[0][2]);

            if (Convert.ToString(dt.Rows[0][4]).Trim().Length > 0)
                    lblChasis.Text = Convert.ToString(dt.Rows[0][4]);
            if (Convert.ToString(dt.Rows[0][5]).Trim().Length > 0)
                lblChasis.Text = (Convert.ToString(dt.Rows[0][4]).Trim().Length>0)?Convert.ToString(dt.Rows[0][4]).Trim()+"<br/>":""+  Convert.ToString(dt.Rows[0][5]);
            
            lblCommodity.Text = Convert.ToString(dt.Rows[0][6]);
            lblSeal.Text = Convert.ToString(dt.Rows[0][7]);
            lblCreated.Text = Convert.ToString(dt.Rows[0][8]);           
            strAssigned = Convert.ToString(dt.Rows[0][9]);
            strEnterBy = Convert.ToString(dt.Rows[0][10]);
            ShippingId = Convert.ToInt64(dt.Rows[0][11]);
            lblInvoice.Text = Convert.ToString(dt.Rows[0][12]);
            lblRailBill.Text = Convert.ToString(dt.Rows[0][13]);
            lblBillDate.Text = Convert.ToString(dt.Rows[0][14]);
            lblPersonBill.Text = Convert.ToString(dt.Rows[0][15]);
            lblStatus.Text = Convert.ToString(dt.Rows[0][16]);
            lblLastfreedate.Text = Convert.ToString(dt.Rows[0][17]);
            //added a collumn for hazmat(madhav)
            if (Convert.ToBoolean(dt.Rows[0][18]))
            {
                spanishazmat.Visible = true;
            }
            else
            {
                spanishazmat.Visible = false;
            }

        }

        lblPerson.Text = DBClass.executeScalar("select [eTn_UserLogin].[nvar_UserId] from [eTn_UserLogin] inner join [eTn_Load] on [eTn_Load].bint_UserLoginId=[eTn_UserLogin].bint_UserLoginId where [eTn_Load].[bint_LoadId]=" + ID);

        //strQuery = "Select isnull(nvar_OtherChargesReason,&nbsp;) from eTn_Payables where bint_LoadId = " + ID;
        //lblComments.Text = DBClass.executeScalar(strQuery);
        //if (lblComments.Text.Trim().Length == 0)
        //    lblComments.Text = "&nbsp;";

        strQuery = "select '<b>'+[nvar_ShippingLineName]+'</b> <br/>'+[nvar_Street]+'&nbsp;'+ [nvar_Suite]+'<br/>'+[nvar_City]+','+[nvar_State] +'&nbsp;'+ [nvar_Zip]+'<br/><a href='+ [nvar_WebsiteURL]+' target=_blank>'+[nvar_WebsiteURL]+'</a>',[nvar_PierTermination] from [eTn_Shipping] where [bint_ShippingId]=" + ShippingId;
        dt = DBClass.returnDataTable(strQuery);
        if (dt.Rows.Count > 0)
        {
            lblRoute.Text = Convert.ToString(dt.Rows[0][0]);
            lblPier.Text = Convert.ToString(dt.Rows[0][1]);
            strQuery = "select [nvar_Name]+'<br/>'+case when len(cast([num_WorkPhone]  as varchar(10)))=10 then substring(cast([num_WorkPhone]  as varchar(10)),1,3)+'-'+" +
                      "substring(cast([num_WorkPhone]  as varchar(10)),4,3)+'-'+substring(cast([num_WorkPhone]  as varchar(10)),7,4) else '' end +'<br/>'+case when len(cast([num_Mobile]  as varchar(10)))=10 then substring(cast([num_Mobile]  as varchar(10)),1,3)+'-'+" +
                      "substring(cast([num_Mobile]  as varchar(10)),4,3)+'-'+substring(cast([num_Mobile]  as varchar(10)),7,4) else '' end" +
                      " from [eTn_ShippingContacts] where bint_ShippingId=" + ShippingId;
            DataTable tempDt;
            tempDt = DBClass.returnDataTable(strQuery);
            if (tempDt.Rows.Count > 0)
            {
                StringBuilder tempStr = new StringBuilder();
                tempStr.Append(string.Empty);
                foreach (DataRow dr in tempDt.Rows)
                {
                    if (tempStr.ToString().Trim().Length != 0)
                        tempStr.Append("<br/>");
                    tempStr.Append(Convert.ToString(dr[0]).Trim() + "<br/>");
                }
                lblShippingContact.Text = tempStr.ToString().Trim();
            }
        }

        strQuery = "SELECT sum([bint_Pieces]),sum([num_Weight]) FROM [eTn_Origin] where [bint_LoadId]=" + ID;
        dt = DBClass.returnDataTable(strQuery);
        if (dt.Rows.Count > 0)
        {
            lblPieces.Text = Convert.ToString(dt.Rows[0][0]);
            lblWeigth.Text = Convert.ToString(dt.Rows[0][1]);
        }

        //barTrackLoad.Visible = true;
        //barTrackLoad.LinksList = "Load Charges;Update Load Status ";
        //barTrackLoad.PagesList = "CarrierLoadCharges.aspx?" + ID + ";CarrierUpdateLoadStatus.aspx?" + ID;
        lblBarText.Text = "Load Details ( " + ID + " )";
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetUserPermissionsByUserId", new object[] { Convert.ToInt64(Session["UserLoginId"]) });
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToBoolean(ds.Tables[0].Rows[0][0]))
                {
                    lnkLoadCharges.Visible = true;
                    lnkLoadCharges.PostBackUrl = "CarrierLoadCharges.aspx?" + ID;
                }
                else
                {
                    lnkLoadCharges.Visible = false;
                }
                if (Convert.ToBoolean(ds.Tables[0].Rows[0][1]))
                {
                    lnkUpdateLoadStatus.Visible = true;
                    lnkUpdateLoadStatus.PostBackUrl = "CarrierUpdateLoadStatus.aspx?" + ID;
                }
                else
                {
                    lnkUpdateLoadStatus.Visible = false;
                }
            }
        }
        if (ds != null) { ds.Dispose(); ds = null; }

        if (dt != null) { dt.Dispose(); dt = null; }
    }    
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (ViewState["ID"]!=null)
        {
            Session["ViewLog"] = true;
            if (txtContainer.Text.Trim().Length > 0)
            {
                switch (ddlContainer.SelectedIndex)
                {
                    case 0:
                        Session["TrackWhereClause"] = "(eTn_Load.[nvar_Container] like '" + txtContainer.Text.Trim() + "' or eTn_Load.[nvar_Container1] like '" + txtContainer.Text.Trim() + "')";
                        break;
                    case 1:
                        Session["TrackWhereClause"] = "[eTn_AssignCarrier].[nvar_Invoice] like '" + txtContainer.Text.Trim() + "%'";
                        break;
                    case 2:
                        Session["TrackWhereClause"] = "eTn_Load.[nvar_Booking] like '" + txtContainer.Text.Trim() + "%'";
                        break;
                    case 3:
                        Session["TrackWhereClause"] = "(eTn_Load.[nvar_Chasis] like '" + txtContainer.Text.Trim() + "%' or eTn_Load.[nvar_Chasis1] like '" + txtContainer.Text.Trim() + "%')";
                        break;
                    case 4:
                        try
                        {
                            Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=" + Convert.ToInt64(txtContainer.Text.Trim());
                        }
                        catch
                        {
                            Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=0";
                        }
                        break;
                }
                Session["TrackWhereClause"] += " and lower(eTn_Load.[nvar_AssignTo])='carrier' and [eTn_AssignCarrier].[bint_CarrierId]=" + Convert.ToString(Session["CarrierId"]);
            }
            else
            {
                Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=0";
            }
            bool blVal = false;
            try
            {
                if (Convert.ToInt32(DBClass.executeScalar("select count([eTn_AssignCarrier].bint_LoadId) from eTn_Load inner join [eTn_AssignCarrier] on eTn_Load.[bint_LoadId]=[eTn_AssignCarrier].[bint_LoadId] where " + Convert.ToString(Session["TrackWhereClause"]))) == 1)
                {
                    blVal = true;
                }
            }
            catch
            {
            }
            if (blVal)
            {
                Response.Redirect("~/Carrier/CarrierTrackLoad.aspx?" + DBClass.executeScalar("select [eTn_AssignCarrier].bint_LoadId from eTn_Load inner join [eTn_AssignCarrier] on eTn_Load.[bint_LoadId]=[eTn_AssignCarrier].[bint_LoadId] where " + Convert.ToString(Session["TrackWhereClause"])));
            }
           
            Response.Redirect("~/Carrier/CarrierTrackLoadResults.aspx");
        }
    }
    protected void lnkLoadRequest_Click(object sender, EventArgs e)
    {
        if (ViewState["ID"] != null)
        {
            Response.Redirect(Convert.ToString("~/Carrier/CarrierLoadDetails.aspx?" + Convert.ToString(ViewState["ID"])));
        }
    }
}
