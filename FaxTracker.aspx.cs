using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FAXService;
using System.IO;

public partial class FaxTracker : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //SLine 2016
        //First the Old Fax sending method will do its work
        UpadateFaxStatus();

        //Here for the first time it will check for the 00:00 hrs value set default at start up in the XML
        //Then onwards, it will save the last run time and when ever invoked, will add-50 minutes to the saved time
        //Which logically gets triggered every 1 Hour
        if ((Convert.ToDateTime(GetFromXML.StreetTurnFlagTimer)).AddMinutes(50) < Convert.ToDateTime(System.DateTime.Now.ToShortTimeString()) || GetFromXML.StreetTurnFlagTimer.ToString() == "00:00")
        {
            SendMailForStreetTurn();
            
            //Here the Last run time will be saved in the XML doc
            GetFromXML.StreetTurnFlagTimer = System.DateTime.Now.ToString();
        }        
    }

    private void UpadateFaxStatus()
    {
        InterFax inf = new InterFax();
        int totalsize = 0;
        int listsize = 0;
        int refcode = 0;
        try
        {
            object obj = SqlHelper.ExecuteScalar(ConfigurationSettings.AppSettings["conString"], "sp_GetLatestFAXTransactionId", new object[] { });
            if (obj != null)
            {
                Int64 lastTransactionId = Int64.Parse(obj.ToString());
                System.Net.ServicePointManager.Expect100Continue = false;
                FaxItem[] faxItems = inf.FaxStatus(ConfigurationSettings.AppSettings["InterFaxUserName"].ToString(), ConfigurationSettings.AppSettings["InterFaxPassword"].ToString(), Convert.ToInt32(lastTransactionId+1), int.Parse(ConfigurationSettings.AppSettings["FaxMaxTransactions"].ToString()), ref totalsize, ref listsize, ref refcode);
                foreach (FaxItem faxItem in faxItems)
                {
                    if (faxItem.Subject == ConfigurationSettings.AppSettings["SMSClientRef"].ToString())
                    {
                        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_Update_eTn_FAXLog", new object[] { faxItem.TransactionID, faxItem.CompletionTime, faxItem.DestinationFax, faxItem.Duration, faxItem.PagesSent, faxItem.PagesSubmitted, faxItem.RemoteCSID, faxItem.Status, faxItem.Subject, faxItem.SubmitTime });
                    }

                }
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message);
        }
    }

    private void LogError(string msg)
    {
        FileStream fs = null;
        if (!File.Exists(Server.MapPath("~/LogDeleteDetails.txt")))
            using (fs = File.Create(Server.MapPath("~/LogDeleteDetails.txt"))) { }
        TextWriter tw = new StreamWriter(Server.MapPath("~/LogDeleteDetails.txt"), true);
        // write a line of text to the file
        tw.WriteLine(msg);
        // close the stream
        tw.Close();
    }

    private void SendMailForStreetTurn()
    {       
        Uri strURL = new Uri(System.Web.HttpContext.Current.Request.Url.AbsoluteUri);
        string pathQuery = strURL.PathAndQuery;
        string strDomainName = strURL.ToString().Replace(pathQuery, "");
        string strSubject = "STREET TURN CONTAINERS AVAILABLE IN OAKLAND CA"  + System.DateTime.Now.ToShortDateString() + " @ " + System.DateTime.Now.ToShortTimeString();
        CommonFunctions.SendEmail(
            GetFromXML.StreetFromAndToEMail,
            GetFromXML.StreetFromAndToEMail,
            "", "",
            strSubject, StreetTurnEMailBody(strDomainName), null, null);
    }

    private string StreetTurnEMailBody(string strLink)
    {
        string strBody =
        "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /><title>Untitled Document</title></head>" +
        "<body style=\"margin:0; padding:0;\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family:Arial, Helvetica, sans-serif;\">" +
        "<tr><td><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"color:#fff; background:#f4f2f4;\" width=\"100%\"><tr>" +
        "<th width=\"231\" valign=\"middle\"><img src=\"" + strLink + "/images/logo.gif\" /></th><th valign=\"middle\" align=\"left\" style=\" padding:28px 0 15px; font-size:18px; color:#800511; position:relative;\">" +
        "<img src=\"" + strLink + "/images/border-top.jpg\" style=\"position:absolute; top:0; width:100%; height:9px;\" /><span style=\"padding-left:13px;\">Street containers available at S LINE TRANSPORTATION INC.</span>  </th>" +
        "</tr></table></td></tr><tr><td><img src=\"" + strLink + "/images/border.jpg\" width=\"100%\" height=\"27\" /></td></tr><tr><td>" +
        "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background:#5b97c8; padding:30px 20px; color:#fff;\" width=\"100%\"><tr>" +
        "<td style=\"font-weight:600; font-size:16px; padding-bottom:10px;\">Email us the list of containers you want to street turn from our company to <a href=\"mailto:streetturn@slinetransport.com\" style=\"color:#fff;\">streetturn@slinetransport.com</a></td>" +
        "</tr><tr><td style=\" font-size:14px;\">Below link will be updated every 1 hour from our system. Please keep checking below link. This will help our company as well as your company to save our drivers' time </td>" +
        "</tr></table></td></tr><tr><td><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:14px; padding:30px 20px 20px; font-weight:bold\">" +
        "<tr><td style=\"padding-bottom:20px;\">" + strLink + "/etransport/StreetTurn.aspx" + "</td></tr><tr><td><br /><br /><br /><br /></td></tr>" +
        "<tr><td style=\"padding-bottom:20px;\">All containers are in our yard located at:</td></tr><tr><td style=\"padding-bottom:3px;\" >1 market street </td></tr><tr>"+
        "<td style=\"padding-bottom:3px;\" >Oakland Ca  94607</td></tr><tr><td style=\"padding-bottom:3px;\" >Tel 510-3425066</td></tr></table></td></tr><tr><td style=\"background:#f2f2f2; font-size:11px; padding:10px 20px;\">"+
        "� Copyright 2016 S Line Transportation Inc. All rights reserved.</td></tr></table></body></html>";

        return strBody;
    }

    
}
