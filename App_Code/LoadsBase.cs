﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;


    public class LoadsBase
    {
        public LoadsBase()
        {
        }

        public DataTable LoadLoads(int employid)
        {
            try
            {
                parametereforload parameter = new parametereforload();
                parameter.bint_Employid = employid;
                string[] str = { "bint_Employid" };
                BaseClass bis = new BaseClass();
                DataSet ds = bis.Loaddata(parameter, "SP_GetLoadbyEmployid", str);
                DataTable dt = ds.Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable Loadpaperworkpending(int employid)
        {
            try
            {
                parametereforload parameter = new parametereforload();
                parameter.bint_Employid = employid;
                string[] str = { "bint_Employid" };
                BaseClass bis = new BaseClass();
                DataSet ds = bis.Loaddata(parameter, "SP_GetpaperworkPendingbyEmployid", str);
                DataTable dt = ds.Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }

public class parametereforload : NewBaseClass
    {
        public parametereforload()
        {
        }
        
    }