<%@ Page AutoEventWireup="true" CodeFile="AddRailNotes.aspx.cs" Inherits="AddRailNotes" Language="C#"
    MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript" language="javascript">
function CheckValidation()
{
    var text=document.getElementById("txtNotes").value;
    if(text=null)
    {
        text.focus();
        return false;
    }
}

</script>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
  <td align="center" valign="top">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblform">
      <tr>
        <th colspan="2">Rail Notes</th>
      </tr>
      <tr id="row">
        <td width="30%" align="right" valign="middle">Load Number  : </td>
        <td><asp:Label ID="lblLoadNo" runat="server" Text="" /></td>
      </tr>
      <tr id="altrow">
        <td align="right" style="height: 20px" valign="middle">Load Entered by  : </td>
        <td style="height: 20px"><asp:Label ID="lblEnterBy" runat="server" Text="" /></td>
      </tr>
      <tr id="row">
        <td align="right" valign="top" style="height: 20px">Rail Note :</td>
        <td style="height: 15px">
            &nbsp;<asp:TextBox ID="txtNotes" runat="server" Height="84px" TextMode="MultiLine" Width="276px" MaxLength="1000" />
    </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
      </tr>
    </table>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
      <tr>
        <td align="right" style="height: 23px">
            <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="btnstyle" OnClick="btnsave_Click"/>&nbsp;<asp:Button ID="btncancel"
        runat="server" Text="Cancel" CssClass="btnstyle" OnClick="btncancel_Click"/></td>
      </tr>
    </table>
  </td>
 </tr>
 </table>
</asp:Content>

