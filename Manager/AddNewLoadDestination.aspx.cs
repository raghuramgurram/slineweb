using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class addnewloaddestination : System.Web.UI.Page
{
    //private string strQuery = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        txtpickup.DisplayError = false;
        if (!IsPostBack)
        {
            FillCompanyNames();
            if (Request.QueryString.Count > 0)
            {
                string[] strQueryValues=null;
               if (Convert.ToString(Request.QueryString[0]).EndsWith("New"))
               {
                   strQueryValues = Convert.ToString(Request.QueryString[0]).Split('^');
                   ViewState["Mode"] = "New";
                   ViewState["LoadId"] = Convert.ToInt64(strQueryValues[0]);
                   GridBar1.HeaderText = "New Destination ( Load Number :  " + Convert.ToString(ViewState["LoadId"]) + " ) ";
                   Session["TopHeaderText"] = "Add Destination";
               }
               else 
               {
                   strQueryValues=Convert.ToString(Request.QueryString[0]).Split('^');
                   if (strQueryValues.Length == 2)
                   {
                       ViewState["LoadId"] = Convert.ToInt64(strQueryValues[1]);
                       //ViewState["ID"] = Convert.ToInt64(strQueryValues[1].Replace("?", ""));
                       ViewState["ID"] = Convert.ToInt64(strQueryValues[0]);
                   }
                   ViewState["Mode"] = "Edit";
                   FillDetails(Convert.ToInt64(ViewState["ID"]));
                   GridBar1.HeaderText = "Edit Destination ( Load Number :  " + Convert.ToString(ViewState["LoadId"]) + " ) ";
                   Session["TopHeaderText"] = "Edit Destination";
               }
            }            
       }
    }

    private void FillCompanyNames()
    {
        if (!IsPostBack)
        {
            //DataTable dt = DBClass.returnDataTable("select [bint_CompanyId],[nvar_CompanyName] FROM [eTn_Company] order by [nvar_CompanyName]");
            long officeLocationId = 0;
            //SLine 2017 Enhancement for Office Location Starts
            if (Session["OfficeLocationID"] != null)
            {
                officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
            }
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCompanyNames", new object[] { "D", officeLocationId });
            //SLine 2017 Enhancement for Office Location Ends
            ddlcompany.DataSource = ds.Tables[0];
            ddlcompany.DataTextField = "nvar_CompanyName";
            ddlcompany.DataValueField = "bint_CompanyId";
            ddlcompany.DataBind();
            ddlcompany.Items.Insert(0, "--Select Company--");
            lblAddress.Text = "";
            if (ds != null) { ds.Dispose(); ds = null; }
        }
    }
    private void FillDetails(long ID)
    {
        //strQuery = "SELECT O.[nvar_PO],O.[bint_Pieces],O.[num_Weight],O.[date_DeliveryAppointmentDate],O.[nvar_ApptGivenby],O.[nvar_App],L.[bint_LocationId],C.[bint_CompanyId] FROM [eTn_Destination] O " +
        //            "Inner Join [eTn_Location] L on O.[bint_LocationId]=L.[bint_LocationId] Inner Join eTn_Company C on L.[bint_CompanyId]=C.[bint_CompanyId] WHERE O.[bint_DestinationId]=" + ID;
        //DataTable dt = DBClass.returnDataTable(strQuery);
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDestinationDetails", new object[] { ID});
        if (ds.Tables[0].Rows.Count > 0)
        {
            txtpo.Text = Convert.ToString(ds.Tables[0].Rows[0][0]).Trim();
            txtpieces.Text = Convert.ToString(ds.Tables[0].Rows[0][1]);
            txtweight.Text = Convert.ToString(ds.Tables[0].Rows[0][2]);
            if (Convert.ToString(ds.Tables[0].Rows[0][3]).Trim().Length > 0)
            {
                try
                {
                    DateTime dat = Convert.ToDateTime(ds.Tables[0].Rows[0][3]);
                    txtpickup.Date = dat.ToShortDateString();
                    txttime.Time = dat.ToShortTimeString();
                }
                catch
                { }
            } 
            txtapptgivenby.Text = Convert.ToString(ds.Tables[0].Rows[0][4]).Trim();
            txtappt.Text = Convert.ToString(ds.Tables[0].Rows[0][5]).Trim();
            if (Convert.ToString(ds.Tables[0].Rows[0][7]).Trim().Length > 0)
            {
                ddlcompany.SelectedIndex = ddlcompany.Items.IndexOf(ddlcompany.Items.FindByValue(ds.Tables[0].Rows[0][7].ToString().Trim()));
            }
            if (ddlcompany.SelectedIndex != 0)
            {
                FillLocations(ddlcompany.SelectedValue);
            }
            if (Convert.ToString(ds.Tables[0].Rows[0][6]).Trim().Length > 0)
            {
                ddladdress.SelectedIndex = ddladdress.Items.IndexOf(ddladdress.Items.FindByValue(ds.Tables[0].Rows[0][6].ToString().Trim()));
                lblAddress.Text = ddladdress.SelectedItem.Text;
            }
            if (Convert.ToString(ds.Tables[0].Rows[0][8]).Trim().Length > 0)
            {
                try
                {
                    DateTime date = Convert.ToDateTime(ds.Tables[0].Rows[0][8]);
                    txtTOtime.Time = date.ToShortTimeString();
                }
                catch
                { }
            }
        }
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    protected void ddlcompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlcompany.SelectedIndex > 0)
        {
            FillLocations(ddlcompany.SelectedValue);
        }
    }
    private void FillLocations(string SelectedValue)
    {
        //strQuery = "select [bint_LocationId],[nvar_Street]+','+[nvar_Suite]+','+[nvar_City]+','+[nvar_State]+','+[nvar_Zip] as 'Address' from [eTn_Location] where [bint_CompanyId]=" + SelectedValue + " order by [nvar_City]";
        //DataTable dtLocations = DBClass.returnDataTable(strQuery);
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLocationDetailsWithCompanyId", new object[] { SelectedValue});
        ddladdress.DataSource = ds.Tables[0];
        ddladdress.DataTextField = "Address";
        ddladdress.DataValueField = "bint_LocationId";
        ddladdress.DataBind();
        ddladdress.Items.Insert(0, "--Select Address--");
        lblAddress.Text = ddladdress.SelectedItem.Text;
        if (ds != null) { ds.Dispose(); ds = null; }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        int iRes = 0;
        try
        {
            if (ViewState["Mode"] != null)
            {
                if (!txtpickup.IsValidDate)
                    return;
                if (txtpieces.Text.Trim().Length == 0)
                    txtpieces.Text = "0";
                if (txtweight.Text.Trim().Length == 0)
                    txtweight.Text = "0";
                if (ViewState["LoadId"] != null)
                {
                    object[] objParams = null;

                    if (string.Compare(Convert.ToString(ViewState["Mode"]), "Edit", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                    {
                        objParams = new object[] { Convert.ToInt64(ViewState["ID"]), txtpo.Text.Trim(), Convert.ToInt64(txtpieces.Text), Convert.ToDecimal(txtweight.Text), CommonFunctions.CheckDateTimeNull((txtpickup.Date != null) ? txtpickup.Date + " " + txttime.Time.Trim() : null), txtapptgivenby.Text.Trim(), txtappt.Text.Trim(), Convert.ToInt64(ddladdress.SelectedValue), CommonFunctions.CheckDateTimeNull((txtTOtime.Time != null && txtTOtime.Time.Length > 0) ? txtpickup.Date + " " + txtTOtime.Time.Trim() : null), Convert.ToString(Session["UserLoginId"]) };
                        if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Update_DestinationDetails", SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Update_DestinationDetails", objParams)) > 0)
                        {
                            iRes = 1;
                        }
                    }
                    else if (string.Compare(Convert.ToString(ViewState["Mode"]), "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                    {
                        objParams = new object[] { Convert.ToInt64(ViewState["LoadId"]), txtpo.Text.Trim(), Convert.ToInt64(txtpieces.Text), Convert.ToDecimal(txtweight.Text), CommonFunctions.CheckDateTimeNull((txtpickup.Date != null) ? txtpickup.Date + " " + txttime.Time.Trim() : null), txtapptgivenby.Text.Trim(), txtappt.Text.Trim(), Convert.ToInt64(ddladdress.SelectedValue), CommonFunctions.CheckDateTimeNull((txtTOtime.Time != null && txtTOtime.Time.Length > 0) ? txtpickup.Date + " " + txtTOtime.Time.Trim() : null), Convert.ToString(Session["UserLoginId"]) };
                        if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Add_DestinationDetails", SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Add_DestinationDetails", objParams)) > 0)
                        {
                            iRes = 1;                            
                        }
                    }
                    objParams = null;
                }
            }
        }
        catch (Exception ex)
        {
            //CommonFunctions.SendEmail(ConfigurationSettings.AppSettings["SmtpUserName"], "tbnsridhar@savitr.com", "shiva994u@gmail.com", "",
            //"Error Details",
            //"error in NewLoadOrigin While " + ex.Message, null);
        }
        finally
        {
            if (iRes == 1)
            {
                Response.Redirect("~/Manager/NewLoad.aspx?" + Convert.ToString(ViewState["LoadId"]));
            }
        }
    }  

    protected void btncancel_Click(object sender, EventArgs e)
    {
        if (string.Compare(Convert.ToString(ViewState["Mode"]), "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            Response.Redirect("~/Manager/NewLoad.aspx?" + Convert.ToString(ViewState["LoadId"]));
        else
            Response.Redirect("~/Manager/NewLoad.aspx?" + Convert.ToString(ViewState["LoadId"]));
    }

    protected void ddladdress_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddladdress.SelectedIndex > 0)
        {
            lblAddress.Text = ddladdress.SelectedItem.Text;
        }
        else
            lblAddress.Text = "";
    }
    protected void lblCompany_Click(object sender, EventArgs e)
    {
        if (string.Compare(Convert.ToString(ViewState["Mode"]), "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            Session["BackPage"] = "~/Manager/AddNewLoadDestination.aspx?" + Convert.ToString(ViewState["LoadId"]) + "^New";
        }
        else if (string.Compare(Convert.ToString(ViewState["Mode"]), "Edit", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            Session["BackPage"] = "~/Manager/AddNewLoadDestination.aspx?" + Convert.ToString(ViewState["LoadId"]) + "^" + Convert.ToString(ViewState["ID"]);
        }
        Response.Redirect("~/Manager/NewLocation.aspx");
    }
    protected void lnkAddress_Click(object sender, EventArgs e)
    {
        if (ddlcompany.Items.Count > 0 && ddlcompany.SelectedIndex > 0)
        {
            if (string.Compare(Convert.ToString(ViewState["Mode"]), "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                Session["BackPage"] = "~/Manager/AddNewLoadDestination.aspx?" + Convert.ToString(ViewState["LoadId"]) + "^New";
            }
            else if (string.Compare(Convert.ToString(ViewState["Mode"]), "Edit", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                Session["BackPage"] = "~/Manager/AddNewLoadDestination.aspx?" + Convert.ToString(ViewState["LoadId"]) + "^" + Convert.ToString(ViewState["ID"]);
            }
            Response.Redirect("~/Manager/NewLocation.aspx?" + ddlcompany.SelectedItem.Value + "^" + ddlcompany.SelectedItem.Text);
        }
    }
}
