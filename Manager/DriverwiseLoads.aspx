<%@ Page AutoEventWireup="true" CodeFile="DriverwiseLoads.aspx.cs" Inherits="DriverwiseLoads"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>
                <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="middle">
                            <asp:Label ID="lblText" runat="server" Text="Driver Nick Name : "></asp:Label>
                            <asp:DropDownList ID="ddlDrivers" runat="server">
                            </asp:DropDownList>
                            &nbsp;&nbsp;
                            &nbsp; &nbsp;
                            <asp:Label ID="lblStatus" runat="server" Text="Status : "></asp:Label>&nbsp;<asp:DropDownList
                                ID="dropStatus" runat="server">
                            </asp:DropDownList>
                            &nbsp;
                            &nbsp; &nbsp;
                            <asp:Button ID="btnSearch" runat="server" CssClass="btnStyle" Text="Search" OnClick="btnSearch_Click" />
                        </td>
                    </tr>
                </table>                
            </td>
        </tr>
    </table>
</asp:Content>

