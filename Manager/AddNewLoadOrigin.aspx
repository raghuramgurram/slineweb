<%@ Page AutoEventWireup="true" CodeFile="AddNewLoadOrigin.aspx.cs" Inherits="NewLoadOrigin"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/TimePicker.ascx" TagName="TimePicker" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td>
        <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="869px" />
                </td>
            </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <uc3:gridbar id="GridBar1" runat="server" headertext="New Origin">
                    </uc3:gridbar>
                </td>
            </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <img alt="" height="3" src="../Images/pix.gif" width="1" /></td>
            </tr>
        </table>
      <table width="100%" border="0" cellpadding="3" cellspacing="0" id="tblform">
        <tr>
          <td colspan="3"><img src="../Images/pix.gif" width="1" height="5" /><img src="../Images/pix.gif" width="1" height="5" /></td>
          </tr>
        <tr>
          <td width="15%" align="right"><strong>P.O.# : </strong></td>
          <td colspan="2">
              <asp:TextBox ID="txtpo" runat="server" MaxLength="50"></asp:TextBox>
              </td>
          </tr>
        <tr>
          <td align="right">Pieces :</td>
          <td colspan="2">
              <asp:TextBox ID="txtpieces" runat="server" MaxLength="18"></asp:TextBox>
              <asp:RegularExpressionValidator ID="regPieces" runat="server" ControlToValidate="txtpieces"
                  Display="Dynamic" ErrorMessage="Please enter valid number of pieces" ValidationExpression="\d*">*</asp:RegularExpressionValidator></td>
          </tr>
        <tr>
          <td align="right">Weight : </td>
          <td colspan="2">
              <asp:TextBox ID="txtweight" runat="server" MaxLength="18"></asp:TextBox>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtweight"
                  Display="Dynamic" ErrorMessage="Please enter valid weight" ValidationExpression="\d*">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr>
          <td align="right">Pickup Date  : </td>
          <td colspan="2">
              <uc1:DatePicker ID="txtpickup" runat="server" EnableViewState="true" Visible="true" IsDefault="true" IsRequired="false" CountNextYears="2" CountPreviousYears="0" />
              &nbsp;
               From Time :
              <uc2:TimePicker ID="txttime" runat="server" ReqFieldValidation="false" />
              &nbsp;&nbsp;&nbsp;
               To Time :&nbsp;
              <uc2:TimePicker ID="txtTOtime" runat="server" ReqFieldValidation="false" Visible="true" />
          </td>
          </tr>
        <tr>
          <td align="right">Appt. Given by : </td>
          <td colspan="2">
              <asp:TextBox ID="txtapptgivenby" runat="server" MaxLength="200"></asp:TextBox>
              &nbsp;&nbsp; Appt.#:
              <asp:TextBox ID="txtappt" runat="server" MaxLength="50"></asp:TextBox>
          </td>
        </tr>
        <tr>
          <td align="right">Company : </td>
          <td colspan="2">
             <asp:DropDownList ID="ddlcompany" runat="server" Width="368px" AutoPostBack="True" OnSelectedIndexChanged="ddlcompany_SelectedIndexChanged">
                 <asp:ListItem>Select Company</asp:ListItem>
              </asp:DropDownList>
              <asp:LinkButton ID="lblCompany" runat="server" Text="Add New Company" CausesValidation="False" OnClick="lblCompany_Click" />
              <asp:CompareValidator ID="cmpCompany" runat="server" ControlToValidate="ddlcompany"
                  Display="Dynamic" ErrorMessage="Please select a company." Operator="NotEqual" ValueToCompare="Select Company" Width="16px">*</asp:CompareValidator></td>
          </tr>
        <tr valign="top">
          <td align="right">Select Address : </td>
          <td width="25%">
              <asp:DropDownList ID="ddladdress" runat="server" Width="368px" OnSelectedIndexChanged="ddladdress_SelectedIndexChanged" AutoPostBack="True">
                  <asp:ListItem>--Select Address--</asp:ListItem>
              </asp:DropDownList></td>
          <td>
                <asp:LinkButton ID="lnkAddress" runat="server" Text="Add New Address" CausesValidation="False" OnClick="lnkAddress_Click" />
              <asp:CompareValidator ID="cmpAddress" runat="server" ErrorMessage="Please select address." ControlToValidate="ddladdress" Display="Dynamic" Operator="NotEqual" ValueToCompare="--Select Address--" Width="16px">*</asp:CompareValidator><br />
                <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
          </td>
        </tr>        
        <tr>
          <td colspan="3"><img src="../Images/pix.gif" width="1" height="5" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../Images/pix.gif" alt="" width="1" height="3" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
          <tr>
            <td align="right">
               <asp:Button ID="btnsave" runat="server" Height="22px" Text="Save" CssClass="btnstyle" OnClick="btnsave_Click"/>&nbsp;
                <asp:Button ID="btncancel" runat="server" Height="22px" Text="Cancel" CssClass="btnstyle" OnClick="btncancel_Click" CausesValidation="False" UseSubmitBehavior="False"/></td>
          </tr>
        </table>
    
    </td>
  </tr>
</table>

</asp:Content>

