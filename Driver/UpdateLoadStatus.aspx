<%@ Page AutoEventWireup="true" CodeFile="UpdateLoadStatus.aspx.cs" Inherits="UpdateLoadStatus"
    Language="C#" MasterPageFile="~/Driver/DriverMaster.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/TimePicker.ascx" TagName="TimePicker" TagPrefix="uc3" %>

<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>
             <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <img alt="" height="3" src="../Images/pix.gif" width="1" /></td>
                </tr>
            </table>
            
             <table border="0" cellpadding="0" cellspacing="0" width="100%" id="tblform1">
                <tr>
                    <td align="left" valign="middle">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                    </td>
                    <td align="right" valign="middle">
                       <asp:Button ID="btnBack" runat="server" CausesValidation="false" CssClass="btnstyle"
                            OnClick="btnBack_Click" Text="Back" Visible="true" />
                         <%--<input type="button" causesvalidation="false" class="btnstyle" value="Close" onclick="window.close();"/>--%>
                    </td>
                </tr>
            </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="3" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <uc1:GridBar ID="GridBar1" runat="server" />
                        </td>
                        <td align="right">
                        </td>
                    </tr>
                </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <img alt="" height="3" src="../Images/pix.gif" width="1" /></td>
                </tr>
            </table>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">               
            <tr>
            <td align="right">
                 <asp:CheckBox ID="ChkSendMail" runat="server" Text="Send Mail" TextAlign="Left" Visible="false" style="text-align:right" ForeColor="Red"/>
        <asp:DataGrid ID="dgrLoad" runat="server" AlternatingItemStyle-CssClass="GridAltItem"
            AutoGenerateColumns="False" CssClass="Grid" GridLines="both" OnItemCommand="dgrLoad_ItemCommand"
            PageSize="3" Width="100%" OnItemDataBound="dgrLoad_ItemDataBound">
            <ItemStyle CssClass="GridItem"/>
              <HeaderStyle CssClass="GridHeader" />
            <AlternatingItemStyle CssClass="GridAltItem" /> 
           <%-- <ItemStyle BorderColor="BLACK" BorderStyle="Solid" BorderWidth="2px" />
            <HeaderStyle BorderColor="BLACK" BorderStyle="Solid" BorderWidth="2px" />
            <AlternatingItemStyle BorderColor="BLACK" BorderStyle="Solid" BorderWidth="2px" />--%>
            <Columns>               
                <asp:TemplateColumn HeaderStyle-CssClass="GridHeader" HeaderText="Status" ItemStyle-CssClass="GridItem">
                    <ItemTemplate>
                        <asp:DropDownList ID="dropStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dropStatus_SelectedIndexChanged">
                            <asp:ListItem Value="3">Pickedup</asp:ListItem>
                            <asp:ListItem Value="5">En-route</asp:ListItem>                            
                            <asp:ListItem Value="14">Reached Destination</asp:ListItem>
                            <asp:ListItem Value="7">Driver On Waiting</asp:ListItem>
                            <asp:ListItem Value="8">Delivered</asp:ListItem>
                            <asp:ListItem Value="6">Drop in Warehouse</asp:ListItem>                             
                            <asp:ListItem Value="9">Empty in Yard</asp:ListItem>
                            <asp:ListItem Value="4">Loaded in Yard</asp:ListItem>
                            <asp:ListItem Value="10">Terminated</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Label ID="lblStatus" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"LoadStatus")%>'
                            Visible="False"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-CssClass="GridHeader" HeaderText="Location" ItemStyle-CssClass="GridItem"> 
                    <ItemTemplate>                        
                        <asp:TextBox ID="txtLoc" runat="server" Visible="False"></asp:TextBox>                    
<%--                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter location." ControlToValidate="txtLoc" Text="*"></asp:RequiredFieldValidator>
--%>                        <asp:Label ID="lblLoc" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"nvar_Location")%>'
                            Visible="False"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn  HeaderStyle-CssClass="GridHeader" HeaderText="Date/Time" ItemStyle-CssClass="GridItem" ItemStyle-Width="300px">
                    <ItemTemplate>
                        <%--<asp:TextBox ID="txtDate" runat="server" Visible="False"></asp:TextBox>--%>
                        <uc2:DatePicker ID="txtDate" runat="server" Visible="False" IsRequired="true" IsDefault="true" CountNextYears="2" CountPreviousYears="0" SetInitialDate="true"/>
                        <uc3:TimePicker ID="txtTime" runat="server" Visible="False" EnableDropDowns="true" ReqFieldValidation="false" SetCurrentTime="true"/>
                        <asp:Label ID="lblDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"date_CreateDate")%>'
                            Visible="False">
                            </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-CssClass="GridHeader" HeaderText="Comments/Notes"
                    ItemStyle-CssClass="GridItem">
                    <ItemTemplate>
                        <asp:TextBox ID="txtComments" runat="server" Visible="False"></asp:TextBox>
                        <asp:Label ID="lblComments" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"nvar_Notes")%>'
                            Visible="False"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-CssClass="GridHeader" ItemStyle-CssClass="GridItem">
                    <ItemTemplate>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>                    
                    <td align="center" style="width:100%;" valign="middle">
                    <asp:Button ID="btnAdd" runat="server" CommandName="ADD" Text="Add" Visible="false" CssClass="btnstyle" CausesValidation="true"/>
                        <%--<asp:ImageButton ID="btnDel" runat="server" CommandName="Delete" ImageUrl="~/images/delete_icon.gif" 
                            ToolTip="Delete" Visible="false" height="13px" Width="12"  CausesValidation="false"
                            OnClientClick="return confirm('Are you sure you want to delete?');"/>--%>
                    </td>
                    </tr>
                    </table>                        
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-CssClass="GridHeader" HeaderText="ID" ItemStyle-CssClass="GridItem"
                    Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="lblID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"bint_TrackLoadId")%>'
                            Visible="False"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
        
       </td>
      </tr>
      </table>
                
</td>
</tr>
</table>
</asp:Content>

