<%@ Page AutoEventWireup="true" CodeFile="ReceivableEntries.aspx.cs" Inherits="ReceivableEntries"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>
<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<style>
.check input[type=checkbox]{height:auto; vertical-align:top; display:inline-block;}
</style>
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top"> 
    <td>
       <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td valign="middle">
                &nbsp; &nbsp; From:
                <uc1:DatePicker ID="dtpFromDate" runat="server" IsDefault="false" IsRequired="false" SetInitialDate="true" CountNextYears="2" CountPreviousYears="0" />
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;To:
                <uc1:DatePicker ID="dtpToDate" runat="server" IsDefault="false" IsRequired="false" SetInitialDate="true" CountNextYears="2" CountPreviousYears="0" />
                &nbsp; &nbsp;Customer Name: <asp:DropDownList ID="ddlCustomer" runat="server">
                </asp:DropDownList>
                <span id="spnAllOfficeLocation" runat="server"><label style="display:inline-block; vertical-align:text-bottom;">All Office Locations:</label> <asp:CheckBox ID="chkAllOfficeLocation" runat="server" CssClass="check" ></asp:CheckBox></span>
                &nbsp; &nbsp;
                <asp:Button ID="btnSearch" runat="server" CssClass="btnstyle" OnClick="btnSearch_Click"
                    Text="Search" />
            </td>
            <td>
            </td>
        </tr>
       </table>
      </td>
    </tr> 
 </table>
</asp:Content>

