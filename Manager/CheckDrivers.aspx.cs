using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CheckDriver : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Drivers";
        if (!IsPostBack)
        {
            gridDrivers.Visible = false;
            pnlSave.Visible = false;
            ViewState["FromDate"] = DateTime.Now.ToShortDateString();
            gridDrivers.PageSize =  Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"].ToString());          
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (ViewState["FromDate"] != null)
        {
            dtpdate.Date = Convert.ToString(ViewState["FromDate"]);
        }
    }

    private DataSet ds;
    private void BindDetails()
    {
        ViewState["FromDate"] = dtpdate.Date;
        ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "Get_dispatcher_AllDriverDetails", new object[] { dtpdate.Date});
        if (ds.Tables.Count > 0)
        {
            pnlSave.Visible = true;
            gridDrivers.Visible = true;
            gridDrivers.DataSource = ds.Tables[0];
            gridDrivers.RowDataBound += new GridViewRowEventHandler(gridDrivers_RowDataBound);
            gridDrivers.DataBind();
        }
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindDetails();
    }
    protected void gridDrivers_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox chkAssigned = (CheckBox)e.Row.FindControl("chkAssigned");
            if (chkAssigned != null)
            {
                if (Convert.ToBoolean(ds.Tables[0].Rows[(gridDrivers.PageIndex*gridDrivers.PageSize)+e.Row.RowIndex][2]))
                {
                    chkAssigned.Checked = true;
                }
                else
                    chkAssigned.Checked = false;
            }
            chkAssigned = null;            
            e.Row.Cells[3].Visible = false;
        }        
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("SearchDriver.aspx");
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < gridDrivers.Rows.Count; i++)
        {
            CheckBox chkAssigned = (CheckBox)gridDrivers.Rows[i].FindControl("chkAssigned");
            if (chkAssigned != null && chkAssigned.Checked)
            {
                SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Add_Dispacther_Drivers", new object[] { Convert.ToString(gridDrivers.Rows[i].Cells[gridDrivers.Columns.Count-1].Text), Convert.ToString(gridDrivers.Rows[i].Cells[0].Text), Convert.ToString(gridDrivers.Rows[i].Cells[1].Text), dtpdate.Date });
            }
            else
            {
                SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Delete_Dispacther_Drivers", new object[] { Convert.ToString(gridDrivers.Rows[i].Cells[gridDrivers.Columns.Count - 1].Text), Convert.ToString(gridDrivers.Rows[i].Cells[0].Text), dtpdate.Date });
            }
            chkAssigned = null;            
        }
        Response.Redirect("SearchDriver.aspx");
    }
    protected void gridDrivers_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gridDrivers.PageIndex = e.NewPageIndex;
        BindDetails();
    }
}
