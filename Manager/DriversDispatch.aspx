<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DriversDispatch.aspx.cs" Inherits="Manager_DriversDispatch" MasterPageFile="~/Manager/MasterPage.master" MaintainScrollPositionOnPostback="true"%>

<%@ Register Src="~/UserControls/Grid.ascx" TagName="Grid" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="ContentTbl">
<tr valign="top">
    <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
          </tr>
        </table> 
       <table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="left" width="49%">
                <uc2:GridBar ID="barManager" runat="server" Visible="true" />
            </td>
            <td width="2%">&nbsp;&nbsp;</td>
            <td align="right" width="49%">
                <uc2:GridBar ID="barManager1" runat="server" Visible="true" />
            </td>
         </tr>   
         </table>  
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
          </tr>
        </table>
        <table cellpadding="0" cellspacing="0" width="100%">
          <tr valign="top">
            <td align="left" width="49%">
                <%--<uc3:Grid ID="DriverwithDispatchGrid" runat="server" PageSize="5"  />--%>
                <asp:GridView ID="DriverwithDispatchGrid" runat="server" width="100%" AllowPaging="false" onrowdatabound="GVDriversDispatch_RowDataBound">
                    <HeaderStyle BackColor="LightBlue" Font-Names="arial,helvetica,sans-serif;" Font-Size="14px"
                        HorizontalAlign="Left" />
                    <RowStyle BackColor="#F3F8FC" Font-Names="arial,helvetica,sans-serif;" Font-Size="12px"
                        HorizontalAlign="Left" />
                </asp:GridView>
             </td>
             <td width="2%">&nbsp;&nbsp;</td>
            <td align="right" width="49%">
               <asp:GridView ID="DriverwithoutDispatchGrid" runat="server" width="100%" AllowPaging="false" HorizontalAlign="Left">
                   <HeaderStyle BackColor="LightBlue" Font-Names="arial,helvetica,sans-serif;" Font-Size="14px"
                       HorizontalAlign="Left"/>
                   <RowStyle BackColor="#F3F8FC" Font-Names="arial,helvetica,sans-serif;" Font-Size="12px"
                       HorizontalAlign="Left" />
               </asp:GridView>
            </td>
          </tr>
       </table>
    </td>
  </tr>
</table>

</asp:Content>
