<%@ Page AutoEventWireup="true" CodeFile="ShowReceivables.aspx.cs" Inherits="showreceivables"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td style="width: 954px">
     <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblform1">
        <tr>
          <td>
              <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="544px" />
          
          </td>
        </tr>
      </table>    
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td>
              <uc1:GridBar ID="BarReceivables" runat="server" />
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../images/pix.gif" alt="" width="1" height="3" /></td>
        </tr>
      </table>    
        <table width="100%" border="0" cellpadding="3" cellspacing="0" id="tblform">
              <tr>
                <th colspan="6">Receivables ($) </th>
               </tr>
              <tr>
                <td align="right" style="width: 139px; height: 20px">Gross Amount  : </td>
                <td width="20%" style="height: 20px">
                    &nbsp;<asp:TextBox ID="txtGrossAmount" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtGrossAmount"
                        Display="Dynamic" ErrorMessage="Please enter a valid amount." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                <td width="15%" align="right" style="height: 20px">Chassis Split : </td>
                <td style="height: 20px; width: 137px;">
                   &nbsp;<asp:TextBox ID="txtChassisSplit" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtChassisSplit"
                        Display="Dynamic" ErrorMessage="Please enter a valid charge." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                <td width="20%" align="right" style="height: 20px">Heavy & Light Scale  Charges : </td>
                <td style="height: 20px; width: 141px;">
                  &nbsp;<asp:TextBox ID="txtHeavyLight" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server"
                        ControlToValidate="txtHeavyLight" Display="Dynamic" ErrorMessage="Please enter a valid charge."
                        ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
              </tr>
              <tr>
                <td align="right" style="width: 139px">Advance Received (-) :</td>
                <td>
                    &nbsp;<asp:TextBox ID="txtAdvanceReceived" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtAdvanceReceived"
                        Display="Dynamic" ErrorMessage="Please enter a valid charge." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                <td align="right">Chassis Rent : </td>
                <td style="width: 137px">
                   &nbsp;<asp:TextBox ID="txtChassisRent" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                        ControlToValidate="txtChassisRent" Display="Dynamic" ErrorMessage="Please enter a valid charge."
                        ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                <td align="right">Scale Ticket Charges : </td>
                <td style="width: 141px">
                   &nbsp;<asp:TextBox ID="txtScaleTicketCharges" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server"
                        ControlToValidate="txtScaleTicketCharges" Display="Dynamic" ErrorMessage="Please enter a valid charge."
                        ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
              </tr>
              <tr>
                <td align="right" style="width: 139px">Dry run : </td>
                <td>
                   &nbsp;<asp:TextBox ID="txtDryRun" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtDryRun"
                        Display="Dynamic" ErrorMessage="Please enter a valid charge." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                <td align="right">Pallets : </td>
                <td style="width: 137px">
                   
                    &nbsp;<asp:TextBox ID="txtPallets" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                        ControlToValidate="txtPallets" Display="Dynamic" ErrorMessage="Please enter a valid charge."
                        ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                        
                            <td align="right">Hazmat Charges: </td>
                <td style="width: 141px">
                  &nbsp;<asp:TextBox ID="txtCharges4" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator22" runat="server"
                        ControlToValidate="txtCharges4" Display="Dynamic" ErrorMessage="Please enter a valid charge."
                        ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                        
              
              </tr>
              <tr>
                <td align="right" style="width: 139px">Fuel Surcharge (auto calc%) : </td>
                <td>
                   &nbsp;<asp:TextBox ID="txtFuelSurcharge" runat="server" Width="29px"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtFuelSurcharge"
                        Display="Dynamic" ErrorMessage="Please enter a valid Fuel surcharge  percentage." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator>(<strong>OR)</strong> Amount ($) : 
                    &nbsp;<asp:TextBox ID="txtAmount" runat="server" Width="32px"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator23" runat="server"
                        ControlToValidate="txtAmount" Display="Dynamic" ErrorMessage="Please enter a valid  Fuel surcharge."
                        ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                <td align="right">Container wash out : <br />                  </td>
                <td style="width: 137px">
                    &nbsp;<asp:TextBox ID="txtContainerWashOut" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server"
                        ControlToValidate="txtContainerWashOut" Display="Dynamic" ErrorMessage="Please enter a valid charge."
                        ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                 <td align="right">Other Charges 1 : </td>
                <td style="width: 141px">
                  &nbsp;<asp:TextBox ID="txtCharges1" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator19" runat="server"
                        ControlToValidate="txtCharges1" Display="Dynamic" ErrorMessage="Please enter a valid charge."
                        ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
              
                        
              </tr>
              <tr>
                <td align="right" style="width: 139px">Pier Termination :</td>
                <td>
                   &nbsp;<asp:TextBox ID="txtPierTermination" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtPierTermination"
                        Display="Dynamic" ErrorMessage="Please enter a valid charge." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                <td align="right">Yard storage : <br /></td>
                <td style="width: 137px">
                    &nbsp;<asp:TextBox ID="txtYardStorage" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server"
                        ControlToValidate="txtYardStorage" Display="Dynamic" ErrorMessage="Please enter a valid charge."
                        ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
               
               
             
                        
                        
                <td align="right">Other Charges 2 : </td>
                <td style="width: 141px">
                   &nbsp;<asp:TextBox ID="txtcharges2" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator20" runat="server"
                        ControlToValidate="txtcharges2" Display="Dynamic" ErrorMessage="Please enter a valid charge."
                        ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                        
              </tr>
              <tr>
                <td align="right" style="width: 139px">Extra Stops : </td>
                <td>
                  &nbsp;<asp:TextBox ID="txtextraStops" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtextraStops"
                        Display="Dynamic" ErrorMessage="Please enter a valid charge." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                <td align="right">Ramp pull charges : <br /></td>
                <td style="width: 137px">
                    &nbsp;<asp:TextBox ID="txtRampPullCharges" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server"
                        ControlToValidate="txtRampPullCharges" Display="Dynamic" ErrorMessage="Please enter a valid charge."
                        ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
            
            
             <td align="right">Other Charges 3: </td>
                <td style="width: 141px">
                    &nbsp;<asp:TextBox ID="txtChrges3" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator21" runat="server"
                        ControlToValidate="txtChrges3" Display="Dynamic" ErrorMessage="Please enter a valid charge."
                        ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                        
                        
               
              </tr>
              <tr id="Tr5">
                <td align="right" style="width: 139px">Lumper Charges : </td>
                <td>
                 &nbsp;<asp:TextBox ID="txtLumperCharges" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtLumperCharges"
                        Display="Dynamic" ErrorMessage="Please enter a valid charge." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                <td align="right">Triaxle charge : <br /></td>
                <td style="width: 137px">
                 &nbsp;<asp:TextBox ID="txtTriaxle" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                        ControlToValidate="txtTriaxle" Display="Dynamic" ErrorMessage="Please enter a valid charge."
                        ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                <td align="right">Other Charges Reason: </td>
                <td style="width: 141px">
                    &nbsp;<asp:TextBox ID="txtOtherChargesReason" runat="server"></asp:TextBox></td>
              </tr>
              <tr>
                <td align="right" style="width: 139px; height: 20px;">Detention Charges : </td>
                <td style="height: 20px">
                   &nbsp;<asp:TextBox ID="txtDetentionCharges" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtDetentionCharges"
                        Display="Dynamic" ErrorMessage="Please enter a valid charge." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                <td align="right" style="height: 20px">Trans load charges : </td>
                <td style="height: 20px; width: 137px;">
                   &nbsp;<asp:TextBox ID="txtTransLoad" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator16" runat="server"
                        ControlToValidate="txtTransLoad" Display="Dynamic" ErrorMessage="Please enter a valid charge."
                        ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                <td align="right" style="height: 20px"><strong>
                    <asp:Label ID="lblTotaltext" Text="Total :" Width="119px" runat="server"></asp:Label></strong></td>
                <td style="height: 20px; width: 141px;">
                   <strong>&nbsp; <asp:Label ID="lblTotal" runat="server" Text="" Width="119px"></asp:Label></strong></td>
              </tr>
            </table>       
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><img src="../images/pix.gif" alt="" width="1" height="3" /></td>
                  </tr>
              </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
                  <tr>
                    <td align="right">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btnstyle" OnClick="btnSave_Click"/>
                        &nbsp;<asp:Button ID="btnCancle" runat="server" Text="Cancel" CssClass="btnstyle" OnClick="btnCancle_Click"/></td>
                  </tr>
                </table></td>
          </tr>
        </table>    
</asp:Content>

