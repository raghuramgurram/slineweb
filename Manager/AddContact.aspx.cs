using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class AddContact : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            if (Request.QueryString.Count > 0)
            {
                if (Request.QueryString[0].Trim().EndsWith("New", true, System.Globalization.CultureInfo.CurrentCulture))
                {
                    ViewState["Mode"] = "New";
                    ViewState["InsertID"] = Request.QueryString[0].Trim().Replace("New","");
                    Session["TopHeaderText"] = "New Contact";
                }
                else
                {
                    ViewState["Mode"] = "Edit";
                    ViewState["ContactID"] = Request.QueryString[0].Trim();
                    try
                    {
                        FillDetails(Convert.ToInt64(ViewState["ContactID"]));
                        Session["TopHeaderText"] = "Edit Contact";
                    }
                    catch
                    {
                    }
                }                               
            }
            //Phone1.da
        }
    }
    private void FillDetails(long ID)
    {
        if (Session["ContactType"] != null)
        {
            //string strQuery = "SELECT [nvar_Name],[nvar_Title],[nvar_Email],[num_WorkPhone],[num_Extn],[num_Direct]," +
            //    "[num_Mobile],[num_Fax],[nvar_WebsiteURL],[nvar_Notes] " +
            //    "FROM [eTn_" + Convert.ToString(Session["ContactType"]).Trim() + "Contacts] where bint_ContactId=" + ID;
            //DataTable dt = DBClass.returnDataTable(strQuery);
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetContactDetails", new object[] { ID, Convert.ToString(Session["ContactType"]).Trim() });
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtName.Text = Convert.ToString(ds.Tables[0].Rows[0][0]).Trim();
                txtTitle.Text = Convert.ToString(ds.Tables[0].Rows[0][1]).Trim();
                txtEmail.Text = Convert.ToString(ds.Tables[0].Rows[0][2]).Trim();
                //txtWorkPhone.Text = CommonFunctions.SetPhoneString(Convert.ToString(ds.Tables[0].Rows[0][3]).Trim());
                txtWorkPhone.Text = Convert.ToString(ds.Tables[0].Rows[0][3]).Trim();
                txtPhoneExtn.Text = CommonFunctions.SetPhoneString(Convert.ToString(ds.Tables[0].Rows[0][4]).Trim());
                txtDirect.Text = CommonFunctions.SetPhoneString(Convert.ToString(ds.Tables[0].Rows[0][5]).Trim());
                //txtMobile.Text = CommonFunctions.SetPhoneString(Convert.ToString(ds.Tables[0].Rows[0][6]).Trim());
                txtMobile.Text = Convert.ToString(ds.Tables[0].Rows[0][6]).Trim();
                txtFax.Text = Convert.ToString(ds.Tables[0].Rows[0][7]).Trim();
                txtWebsiteURL.Text = Convert.ToString(ds.Tables[0].Rows[0][8]).Trim();
                txtNotes.Text = Convert.ToString(ds.Tables[0].Rows[0][9]).Trim();
            }
            if (ds != null) { ds.Dispose(); ds = null; }
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        RedirectPage();
    }
    protected void RedirectPage()
    {
        if (Session["ContactType"] != null)
        {
            if (ViewState["InsertID"] != null)
                Response.Redirect("~/Manager/New" + Convert.ToString(Session["ContactType"]).Trim() + ".aspx?" + Convert.ToString(ViewState["InsertID"]));
            else if (ViewState["ContactID"] != null)
            {
                string strQuery = "select bint_" + Convert.ToString(Session["ContactType"]).Trim() + "Id from eTn_" + Convert.ToString(Session["ContactType"]).Trim() + "Contacts " +
                    " where bint_ContactId=" + Convert.ToString(ViewState["ContactID"]).Trim();
                string strRes = DBClass.executeScalar(strQuery);
                if (!Convert.IsDBNull(strRes) && strRes.Trim().Length > 0)
                    Response.Redirect("~/Manager/New" + Convert.ToString(Session["ContactType"]).Trim() + ".aspx?" + strRes.Trim());
            }
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ViewState["Mode"] != null && Session["ContactType"] != null)
        {
            try { Convert.ToInt64(txtPhoneExtn.Text); }
            catch {txtPhoneExtn.Text="0"; }
            string strQuery = "";
            if (string.Compare(Convert.ToString(ViewState["Mode"]), "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                strQuery = "Select Count(bint_" + Convert.ToString(Session["ContactType"]).Trim() + "Id) from eTn_" + Convert.ToString(Session["ContactType"]) +
                    " where bint_" + Convert.ToString(Session["ContactType"]).Trim() + "Id = " + Convert.ToInt64(ViewState["InsertID"]);
                if (DBClass.executeScalar(strQuery) == "1")
                {
                    object[] parms = new object[] {  Convert.ToInt64(ViewState["InsertID"]), txtName.Text.Trim(), txtTitle.Text.Trim(),txtEmail.Text.Trim(), txtWorkPhone.Text,txtPhoneExtn.Text,
                                                    txtDirect.Text,txtMobile.Text,txtFax.Text,txtWebsiteURL.Text.Trim(), Convert.ToString(txtNotes.Text.Trim())};
                    if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_Add_" + Convert.ToString(Session["ContactType"]) + "Contacts", parms) > 0)
                    {
                        RedirectPage();
                    }
                    parms = null;
                }
            }
            else if (string.Compare(Convert.ToString(ViewState["Mode"]), "Edit", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                object[] parms = new object[] {  Convert.ToInt64(ViewState["ContactID"]), txtName.Text.Trim(), txtTitle.Text.Trim(),txtEmail.Text.Trim(),txtWorkPhone.Text,txtPhoneExtn.Text,
                                                   txtDirect.Text,txtMobile.Text,txtFax.Text,txtWebsiteURL.Text.Trim(), Convert.ToString(txtNotes.Text.Trim())};
                if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_Update_" + Convert.ToString(Session["ContactType"]) + "Contacts", parms) > 0)
                {
                    RedirectPage();                    
                }
                parms = null;
            }
        }
    }
}
