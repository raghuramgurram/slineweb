<%@ Page AutoEventWireup="true" CodeFile="SMSLogs.aspx.cs" Inherits="SMSLogs" Language="C#"
    MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="tblform1">
                    <tr>
                        <td valign="middle">
                            <asp:Label ID="lblFromdate" runat="server" Text="From : "></asp:Label>
                            <uc1:DatePicker ID="dtpFromDate" runat="server" IsDefault="false" IsRequired="false" CountNextYears="2" CountPreviousYears="0" SetInitialDate="true" />
                            &nbsp; &nbsp;&nbsp; &nbsp;<asp:Label ID="lblToDate" runat="server" Text="To : "></asp:Label>
                            <uc1:DatePicker ID="dtpToDate" runat="server" IsDefault="false" IsRequired="false" CountNextYears="2" CountPreviousYears="0" SetInitialDate="true" />
                            &nbsp; &nbsp;
                            <asp:Button ID="btnSearch" runat="server" CssClass="btnstyle" Text="Search" OnClick="btnSearch_Click" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle"></td>
                        <td>   
                        &nbsp;        
                        </td>
                    </tr>
                   
                    <tr><td colspan="2">
                      <asp:DataGrid AutoGenerateColumns="false" ID="dggrid" Width="100%" runat="server" AllowPaging="true" OnPageIndexChanged="dggrid_PageIndexChanged"  CssClass="Grid" PageSize="10" AllowCustomPaging="true">          
        <PagerStyle Mode="NumericPages" HorizontalAlign="Center"/>
        <ItemStyle CssClass="GridItem" BorderColor="White" />
        <HeaderStyle CssClass="GridHeader" BorderColor="White" />
        <AlternatingItemStyle CssClass="GridAltItem" />
        <Columns>
        <asp:TemplateColumn HeaderText="SMS Id">
            <ItemTemplate>
            <asp:LinkButton id="lbnSMSId" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "bint_SMSId")%>' OnClick="lbnSMSId_Click"></asp:LinkButton>               
            </ItemTemplate>
        </asp:TemplateColumn>
        <asp:BoundColumn HeaderText="Load Number" DataField="bint_LoadId"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="To Name" DataField="nvar_To"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="To Number" DataField="nvar_ToNumber"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Type" DataField="nvar_Type"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Message Text" DataField="nvar_MessageText" ItemStyle-Width="40%"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Message Count" DataField="tint_MessageCount"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Created Date" DataField="date_CreatedDate"></asp:BoundColumn>        
        </Columns>
    </asp:DataGrid>
                    </td></tr>
                    </table>
                    
                    
                    <asp:Panel ID="SmsPanel" runat="server" Visible="false">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                    <td>&nbsp;</td>
                    </tr>
                    <tr>
                    <td>
                        <asp:Label ID="LblSMSDetailsHeader" runat="server" Text="" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                    <td>&nbsp;</td>
                    </tr>
                    <tr><td>                    
                      <asp:DataGrid ID="dgSmsDetails" Width="100%" runat="server" AutoGenerateColumns="false" CssClass="Grid" >          
        <ItemStyle CssClass="GridItem" BorderColor="White" />
        <HeaderStyle CssClass="GridHeader" BorderColor="White" />
        <AlternatingItemStyle CssClass="GridAltItem" />
        <Columns>
        <asp:BoundColumn HeaderText="Message Id" DataField="nvar_MessageId"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="To Number" DataField="nvar_To"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Price" DataField="num_MessagePrice"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Response" DataField="Response"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="MSISDN" DataField="nvar_DrMSISDN"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Network Code" DataField="nvar_DrNetworkCode"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="SCTS" DataField="nvar_scts"></asp:BoundColumn>     
        <asp:BoundColumn HeaderText="Delivery Status" DataField="DeliveryStatus"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Time (UTC)" DataField="nvar_MessageTimeStamp"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Error" DataField="DeliveryError"></asp:BoundColumn>        
        </Columns>
    </asp:DataGrid>
    
                    </td></tr>
                </table>
                 </asp:Panel>
            </td>
        </tr>
    </table>   
   
</asp:Content>

