using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Manager_StaffMistakes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Mistakes";
        String UserLgnId;
        if (!IsPostBack)
        {
            if (Request.QueryString.Count > 0)
            {
                UserLgnId = Convert.ToString(Request.QueryString[0].Trim());
                Session["UserLgnId"] = UserLgnId;
                barCurrent.HeaderText = "Mistakes of " + getEmployeeName(UserLgnId);
                btnBack.Visible = true;
            }
            else
            {
                UserLgnId = Convert.ToString(Session["UserLoginId"]);
                Session["UserLgnId"] = "";
                barCurrent.HeaderText = "My Mistakes";
            }

            gridCurrent.Visible = true;
            gridCurrent.DeleteVisible = true;
            gridCurrent.EditVisible = true;
            gridCurrent.TableName = "eTn_Mistakes";
            gridCurrent.Primarykey = "bint_MistakeId";
            gridCurrent.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
            //SLine 2017 Enhancement for Office Location Starts
            gridCurrent.ColumnsList = "eTn_Mistakes.bint_MistakeId;eTn_Mistakes.nvar_description as Description;" + CommonFunctions.DateQueryString("eTn_Mistakes.date_MistakeDoneDate", "MistakeDoneDate") + ";eTn_OfficeLocation.nvar_OfficeLocationName as OffLoc";
            gridCurrent.VisibleColumnsList = "Description;MistakeDoneDate;OffLoc";
            gridCurrent.VisibleHeadersList = "Mistake Done;Mistake Done Date;Office Location";
            gridCurrent.InnerJoinClause = " inner join eTn_OfficeLocation on eTn_OfficeLocation.bint_OfficeLocationId = eTn_Mistakes.bint_OfficeLocationId";
            //SLine 2017 Enhancement for Office Location Ends
            gridCurrent.DeleteTablesList = "eTn_Mistakes";
            gridCurrent.DeleteTablePKList = "bint_MistakeId";
            gridCurrent.EditPage = "~/Manager/NewMistake.aspx";
            gridCurrent.OrderByClause = "date_MistakeDoneDate desc";
            gridCurrent.WhereClause = "bint_UserLoginId=" + UserLgnId;
            gridCurrent.BindGrid(0);

            barCurrent.PagesList = "NewMistake.aspx";
        }  
    }

    protected string getEmployeeName(String UserLoginId)
    {
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["conString"], "Get_User_Details", new object[] { UserLoginId });
        if (ds.Tables.Count > 0)
            return Convert.ToString(ds.Tables[0].Rows[0]["nvar_NickName"]);
        else
            return "";
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Manager/DisplayReport.aspx");
    }
}
