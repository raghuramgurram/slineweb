using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CarrierwiseLoads : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Carrier Wise Loads";
        if (!IsPostBack)
        {
            FillLists();
        }
    }

    private void FillLists()
    {
        //ddlCarriers.DataSource = DBClass.returnDataTable("Select bint_CarrierId,nvar_CarrierName from eTn_Carrier order by [nvar_CarrierName]");
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        System.Data.DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCarrierNames", new object[] { officeLocationId });
        //SLine 2017 Enhancement for Office Location Ends
        if (ds.Tables.Count > 0)
        {
            ddlCarriers.DataSource = ds.Tables[0];
            ddlCarriers.DataValueField = "bint_CarrierId";
            ddlCarriers.DataTextField = "nvar_CarrierName";
            ddlCarriers.DataBind();
            ddlCarriers.Items.Insert(0, new ListItem("All", "0"));
        }
        //[SP_GetLoadStatusDetails]
        ds.Tables.Clear();
        //dropStatus.DataSource = DBClass.returnDataTable("Select bint_LoadStatusId,nvar_LoadStatusDesc from eTn_LoadStatus");
        ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadStatusDetails");
        if (ds.Tables.Count > 0)
        {
            dropStatus.DataSource = ds.Tables[0];
            dropStatus.DataValueField = "bint_LoadStatusId";
            dropStatus.DataTextField = "nvar_LoadStatusDesc";
            dropStatus.DataBind();
        }
        if (dropStatus.Items.Count > 0)
        {
            dropStatus.Items.Remove(dropStatus.Items.FindByText("Delivered"));
            dropStatus.Items.Remove(dropStatus.Items.FindByText("Closed"));
            dropStatus.Items.Remove(dropStatus.Items.FindByText("New"));
            dropStatus.Items.Insert(1, new ListItem("All Transit Loads", dropStatus.Items.Count + 1.ToString()));
        }        
        if (ds != null) { ds.Dispose(); ds = null; }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Session["SerchCriteria"] = null;
            Session["ReportParameters"] = null;
            Session["ReportName"] = null;
            Session["ReportParameters"] = ddlCarriers.SelectedValue.Trim() + "~~^^^^~~" + dropStatus.SelectedItem.Text;
            Session["SerchCriteria"] = ((Convert.ToInt64(ddlCarriers.SelectedValue) > 0) ? "Carrier name : " + ddlCarriers.SelectedItem.Text.Trim() : "All Carriers ") + "&nbsp;&nbsp;&nbsp; Status : " + dropStatus.SelectedItem.Text;
            Session["ReportName"] = "GetCarrierwiseLoads";
            Response.Redirect("~/Manager/DisplayReport.aspx");
        }
    }
    //private void DisplayResport()
    //{
    //    DataSet ds;
    //    object[] objParams = new object[] { txtName.Text.Trim(), ddlStatus.SelectedItem.Value };
    //    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCarrierwiseLoads", objParams);
    //    string[] strColumnsList = new string[] { "Name", "Phone", "Fax", "Created" };
    //    string[] colWidths = new string[] { "50%", "20%", "20%", "10%" };
    //    objParams = null;
    //    if (ds.Tables.Count > 0)
    //    {
    //        DataTable dt = ds.Tables[0];
    //        CommonFunctions.FormatDataTable(ref dt);
    //        lblDisplay.Text = CommonFunctions.ReportDisplay(dt, strColumnsList, colWidths);
    //        if (dt != null) { dt.Dispose(); dt = null; }
    //    }
    //    else
    //    {
    //        lblDisplay.Text = "<table width=100% border=0 cellpadding=0 cellspacing=0 id=ContentTbl><tr valign=top><td>" +
    //            "<table border=0 width=100% height=200px border=0 cellpadding=0 cellspacing=0 align=center valign=middle><tr><td width=100% align=center valign=middle>" +
    //            "<b><font color=blue>No Records To Display.<font></b></td></tr></table><br/><br/><br/></td></tr></table>";
    //    }
    //    if (ds != null) { ds.Dispose(); ds = null; }
    //    strColumnsList = null; colWidths = null;
    //}    
}
