using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Manager_LoadRequest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["TopHeaderText"] = "Load Request";  
           //this.Page.Title = Convert.ToString(ConfigurationSettings.AppSettings["CompanyName"]);
            this.Page.Title = GetFromXML.CompanyName;
            if (Request.QueryString.Count > 0)
            {
                try
                {
                    if (Convert.ToString(Request.QueryString[0]).IndexOf("^") > 0)
                    {
                        string[] strValues = Convert.ToString(Request.QueryString[0]).Trim().Split('^');
                        ViewState["LoadId"] = Convert.ToString(strValues[0]);
                        ViewState["DriverId"] = Convert.ToString(strValues[1]);
                        ViewState["AssignedId"] = Convert.ToString(strValues[2]);        
                    }
                    else
                        ViewState["LoadId"] = Convert.ToString(Request.QueryString[0]);
                    FillDetails();
                }
                catch(Exception ex)
                {
                }
            }
        }

    }
    private void FillDetails()
    {
        if (ViewState["LoadId"] != null)
        {
            //lblAddress.Text = Application["CompanyAddress"].ToString();
           // lblAddress.Text = "<strong>" + Convert.ToString(ConfigurationSettings.AppSettings["CompanyName"]) + "</strong><br/>" + Convert.ToString(ConfigurationSettings.AppSettings["ComapanyAddress"]) + "<br/>" + Convert.ToString(ConfigurationSettings.AppSettings["ComapanyState"]) + "<br/>" + Convert.ToString(ConfigurationSettings.AppSettings["CompanyPhone"]) + "<br/>" + Convert.ToString(ConfigurationSettings.AppSettings["CompanyFax"]);
            lblAddress.Text = "<strong>" + GetFromXML.CompanyName + "</strong><br/>" + GetFromXML.ComapanyAddress + "<br/>" + GetFromXML.ComapanyState + "<br/>" + GetFromXML.CompanyPhone + "<br/>" + GetFromXML.CompanyFax;
            DataSet ds=new DataSet();
            lblLoad.Text = Convert.ToString(ViewState["LoadId"]);
            if(ViewState["DriverId"]!=null)
                ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriverLoadDetails", new object[] { Convert.ToInt64(ViewState["LoadId"]), Convert.ToInt64(ViewState["DriverId"]), Convert.ToInt64(ViewState["AssignedId"]) });
            else
                ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadRequestDetails", new object[] { Convert.ToInt64(ViewState["LoadId"]) });
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblLoadId.Text = Convert.ToString(ViewState["LoadId"]);
                    lblType.Text = Convert.ToString(ds.Tables[0].Rows[0][0]);
                    lblCommodity.Text = Convert.ToString(ds.Tables[0].Rows[0][1]);
                    lblBooking.Text = Convert.ToString(ds.Tables[0].Rows[0][2]);
                    if (Convert.ToString(ds.Tables[0].Rows[0][3]).Trim().Length == 0)
                        lblContainerId.Text = Convert.ToString(ds.Tables[0].Rows[0][4]);
                    else
                        lblContainerId.Text = Convert.ToString(ds.Tables[0].Rows[0][3]);

                    if (Convert.ToString(ds.Tables[0].Rows[0][5]).Trim().Length == 0)
                        lblChasis.Text = Convert.ToString(ds.Tables[0].Rows[0][6]);
                    else
                        lblChasis.Text = Convert.ToString(ds.Tables[0].Rows[0][5]);

                    lblRemarks.Text = Convert.ToString(ds.Tables[0].Rows[0][7]);
                    lblDescription.Text = Convert.ToString(ds.Tables[0].Rows[0][8]);
                    lblPickup.Text = Convert.ToString(ds.Tables[0].Rows[0][10]);
                    string[] str = Convert.ToString(ds.Tables[0].Rows[0][11]).Trim().Split(';');
                    for (int i = 0; i < str.Length; i++)
                    {
                        if (i != str.Length - 1)
                        {
                            if (str[i].Length > 0)
                                lblPO.Text = str[i];
                            if (str[i + 1].Length > 0)
                                lblPO.Text += "<br/>";
                        }
                        else
                        {
                            lblPO.Text += str[i];
                        }
                    } 
                    //BillingTo = "<b>" + Convert.ToString(ds.Tables[0].Rows[0][9]) + " </b> <br/>&nbsp;&nbsp;" + Convert.ToString(ds.Tables[0].Rows[0][10]) + "  " + Convert.ToString(ds.Tables[0].Rows[0][11]) + "<br/>&nbsp;&nbsp;";
                    //BillingTo += Convert.ToString(ds.Tables[0].Rows[0][12]) + ", " + Convert.ToString(ds.Tables[0].Rows[0][13]) + " " + Convert.ToString(ds.Tables[0].Rows[0][14]);
                    //BillingTo += "<br/>" + Convert.ToString(ds.Tables[0].Rows[0][16]);
                    //lblBillingTo.Text = Convert.ToString(ds.Tables[0].Rows[0][9]);
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    lblOrigin.Text = CommonFunctions.FormatMultipleOrigins(Convert.ToString(ds.Tables[1].Rows[0][0]).Trim());
                    lblDestination.Text = CommonFunctions.FormatMultipleOrigins(Convert.ToString(ds.Tables[1].Rows[0][1]).Trim());
                }
                if (ds.Tables.Count > 2)
                {
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        lblAssigned.Text = Convert.ToString(ds.Tables[2].Rows[0][0]).Trim();
                        lblAssigned.Font.Underline = true;
                        lblDriverOrCarrier.Text = Convert.ToString(ds.Tables[2].Rows[0][1]).Trim();
                        lblTotal.Text = "Total Charges:  $ <b>" + Convert.ToString(ds.Tables[2].Rows[0][2]).Trim()+"</b>";
                        lblCarrier.Text = Convert.ToString(ds.Tables[2].Rows[0][3]);
                    }
                }
            }
            if (ds != null) { ds.Dispose(); ds = null; }
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (ViewState["LoadId"] != null)
        {
            CheckBackPage();
            Response.Redirect("~/Manager/TrackLoad.aspx?" + Convert.ToInt64(ViewState["LoadId"]));
        }
    }
    private void CheckBackPage()
    {
        if (Session["BackPage4"] != null)
        {
            if (Convert.ToString(Session["BackPage4"]).Trim().Length > 0)
            {
                string strTemp = Convert.ToString(Session["BackPage4"]).Trim();
                Session["BackPage4"] = null;
                Response.Redirect(strTemp);
            }
        }  
    }
}
