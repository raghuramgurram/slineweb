<%@ Page Language="C#" MasterPageFile="~/Manager/MasterPage.master" AutoEventWireup="true" CodeFile="NewEmployee.aspx.cs" Inherits="NewEmployee" Title="Untitled Page" %>

<%@ Register Src="~/UserControls/USState.ascx" TagName="USState" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/Phone.ascx" TagName="Phone" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">
        function VisibleDateControl(panelId, roleId) {
            var ddlStatus = document.getElementById(roleId);
            alert(document.getElementById(panelId));
            alert(ddlStatus.options[ddlStatus.options.selectedIndex].text);

            if (ddlStatus.options[ddlStatus.options.selectedIndex].text == 'Work Permit')
                document.getElementById(panelId).visible = true;
            else
                document.getElementById(panelId).visible = false;
        }
    </script>

    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
        <tr valign="top">
            <td>
                <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                            <asp:Label ID="lblErMess" runat="server" ForeColor="Red" Text="User Id already exists."
                                Visible="False"></asp:Label></td>
                        <asp:Label ID="lblErLoc" runat="server" ForeColor="Red" Text="Please select a location."
                            Visible="False"></asp:Label>
            </td>
        </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="50%" valign="top" style="height: 518px">
                <table width="100%" border="0" cellpadding="3" cellspacing="0" id="tblform">
                    <tr>
                        <th colspan="5">Address Details </th>
                    </tr>
                    <tr>
                        <td width="15%" align="right">First Name   : </td>
                        <td width="30%">
                            <asp:TextBox ID="txtfirstname" runat="server" MaxLength="150"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ReqFirstName" runat="server" ControlToValidate="txtfirstname"
                                Display="Dynamic" ErrorMessage="Please enter first name.">*</asp:RequiredFieldValidator></td>
                        <td width="15%" align="right">Status : </td>
                        <td>
                            <asp:DropDownList ID="ddlstatus" runat="server">
                                <asp:ListItem Value="1">Active</asp:ListItem>
                                <asp:ListItem Value="0">In Active</asp:ListItem>
                            </asp:DropDownList></td>
                        <td rowspan="4" align="center" valign="top">
                            <table border="0" cellspacing="0" cellpadding="5">
                                <tr>
                                    <td bgcolor="#eeeeee" style="height: 10px">
                                        <asp:Image ID="imgphoto" runat="server" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Middle Name   :</td>
                        <td>
                            <asp:TextBox ID="txtmiddlename" runat="server" MaxLength="100"></asp:TextBox></td>
                        <td align="right">Type  : </td>
                        <td>
                            <asp:DropDownList ID="ddltype" runat="server" Width="71px">
                                <asp:ListItem Selected="True">Select</asp:ListItem>
                                <asp:ListItem>W2</asp:ListItem>
                                <asp:ListItem>Contractor</asp:ListItem>
                                <asp:ListItem>Part Time</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="right">Last Name   :</td>
                        <td>
                            <asp:TextBox ID="txtlastname" runat="server" MaxLength="100"></asp:TextBox></td>
                        <td align="right">Date of Birth  : </td>
                        <td>
                            <uc1:DatePicker ID="txtdateofbirth" runat="server" IsDefault="true" IsRequired="false" IsDOB="true" />
                            <%--                  <asp:TextBox ID="txtdateofbirth" runat="server"></asp:TextBox></td>
                            --%>
                    </tr>
                    <tr>
                        <td align="right" valign="top">Nick Name  :</td>

                        <td valign="top">
                            <asp:Label Style="font-weight: bold" ID="lblEmpId" runat="server"></asp:Label>
                            <asp:TextBox ID="txtnickname" runat="server"></asp:TextBox></td>
                        <td align="right" valign="top">Date Hired : </td>
                        <td>
                            <uc1:DatePicker ID="txtdatehired" runat="server" IsDefault="true" IsRequired="false" CountNextYears="2" CountPreviousYears="0" IsPrvYears="true" />
                            <%--                  <asp:TextBox ID="txtdatehired" runat="server"></asp:TextBox></td>
                            --%>
                    </tr>
                    <tr>
                        <td align="right" valign="top">Role : </td>
                        <td valign="top">
                            <asp:DropDownList ID="ddlrole" runat="server" OnSelectedIndexChanged="ddlrole_SelectedIndexChanged" Width="91px" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlrole"
                                Display="Dynamic" ErrorMessage="Please select Role." Operator="NotEqual" ValueToCompare="Select">*</asp:CompareValidator>

                            <%--<asp:LinkButton ID="lnkAattchEque" runat="server" Text="Get Equipment" Visible="False" OnClick="lnkAattchEque_Click" CausesValidation="False" />
                            <asp:DropDownList ID="ddlEquipments" runat="server" OnSelectedIndexChanged="ddlrole_SelectedIndexChanged" Visible="False">
                            </asp:DropDownList><asp:Label ID="lblErrEquip" runat="server" ForeColor="Red" Text="Invalid Equipment."
                                Visible="False"></asp:Label></td>--%>

                           
                        <td align="right" valign="top" style="height: 48px">Tax ID : </td>
                        <td style="height: 48px" valign="top">
                            <asp:TextBox ID="txttaxid" runat="server" MaxLength="100" Width="150px"></asp:TextBox></td>
                        <td align="center" style="height: 48px" valign="top">
                            <asp:LinkButton CausesValidation="false" ID="lnkUploadPhoto" runat="server" ForeColor="red" Font-Overline="false" OnClick="lnkUploadPhoto_Click">Upload Photo</asp:LinkButton>
                            <br />
                            <asp:Panel ID="pnlPhoto" runat="server" Height="30px" Width="210px" Visible="False">
                                <asp:FileUpload ID="UpPhoto" runat="server" Width="170px" />
                                <asp:Button ID="btnAddPhoto" runat="server" CssClass="btnstyle" Text="Add" OnClick="btnAddPhoto_Click" CausesValidation="false" UseSubmitBehavior="false" />
                            </asp:Panel>
                            <br />
                            &nbsp;</td>
                    </tr>

                    <tr>
                        <td align="right" valign="top">Team :</td>
                        <td valign="top">
                            <asp:DropDownList ID="ddlteam" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
                    </tr>
                </table>
                <asp:Panel ID="pnlDriveDetails" runat="server" Width="100%">
                    <table width="100%" border="0" cellpadding="3" cellspacing="0" id="tblform">
                        <tr>
                            <th colspan="4">Driver Details </th>
                        </tr>
                        <tr>
                            <td width="15%" align="right">Driver Status : </td>
                            <td width="20%">
                                <asp:DropDownList ID="ddlDriverStatus" runat="Server" OnSelectedIndexChanged="ddlDriverStatus_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Text="Select"></asp:ListItem>
                                    <asp:ListItem Text="Citizen" Value="Citizen"></asp:ListItem>
                                    <asp:ListItem Text="Alien Resident" Value="Alien Resident"></asp:ListItem>
                                    <asp:ListItem Text="Work Permit" Value="Work Permit"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlDriverStatus"
                                    Display="Dynamic" ErrorMessage="Please select  work status." ValueToCompare="Select Work Status" Operator="NotEqual">*</asp:CompareValidator></td>
                            <td width="20%" align="right">
                                <asp:Label ID="lblWorkPermit" runat="server">Work Permit Expiry Date :</asp:Label></td>
                            <td>&nbsp;
                                <uc1:DatePicker ID="dtpWorkPermit" runat="server" Visible="False" IsDefault="true" IsRequired="true" CountNextYears="15" CountPreviousYears="0" IsExpireDate="true" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Driver License # : </td>
                            <td>
                                <asp:TextBox ID="txtLicense" runat="server" MaxLength="100"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtLicense"
                                    Display="Dynamic" ErrorMessage="Please enter Driving License #.">*</asp:RequiredFieldValidator></td>
                            <td align="right">&nbsp;License # Expiry Date : </td>
                            <td>&nbsp;<uc1:DatePicker ID="dtpLicenseExpiry" runat="server" IsDefault="true" IsRequired="true" CountNextYears="15" CountPreviousYears="0" IsExpireDate="true" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">&nbsp; </td>
                            <td>&nbsp;</td>
                            <td align="right">&nbsp;Medical Card Expiry Date : </td>
                            <td>&nbsp;<uc1:DatePicker ID="dtpMedicalCard" runat="server" IsDefault="true" IsRequired="true" CountNextYears="15" CountPreviousYears="0" IsExpireDate="true" />
                            </td>
                        </tr>
                        <%--new coloumns add by madhav for Hazmat --%>
                        <tr>

                            <td align="right">&nbsp;Is Hazmat Certified : </td>
                            <td>&nbsp;<asp:CheckBox ID="ChkHaxmatCertified" runat="server" AutoPostBack="true" OnCheckedChanged="ChkHaxmatCertified_Changed" /></td>
                            <td align="right">
                                <span runat="server" id="spanHazmatexpirydate" visible="false">&nbsp;Hazmat Certificate Expiry Date : </span></td>
                            <td>&nbsp;<uc1:DatePicker ID="dtpHazmatCertigiedExpaires" Visible="false" runat="server" IsDefault="true" CountNextYears="15" CountPreviousYears="0" IsExpireDate="true" />
                            </td>
                        </tr>
                        <%--end--%>
                    </table>
                </asp:Panel>
                <%-- This code added by padmavathi --%>

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
                    </tr>
                </table>
                <asp:Panel ID="pnlLicenseDetails" runat="server" Width="100%">
                    <table width="100%" border="0" cellpadding="3" cellspacing="0" id="tblform">
                        <tr>
                            <th colspan="2">Equipment License Details </th>
                        </tr>
                        <tr>
                            <td width="15%" align="right">License #    :</td>
                            <td>
                                <asp:TextBox ID="txtlicensedriver" runat="server" MaxLength="100"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="right" width="15%">VIN #     :</td>
                            <td>
                                <asp:TextBox ID="txtvin" runat="server" MaxLength="100"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="right">Plate# : </td>
                            <td>
                                <asp:TextBox ID="txtplate" runat="server" MaxLength="100"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="right"></td>
                            <td>
                                <asp:TextBox ID="txteqpname" runat="server" Visible="false"></asp:TextBox></td>
                        </tr>
                    </table>
                </asp:Panel>

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
                    </tr>
                </table>
                <asp:Panel ID="pnlInsuranceDetails" runat="server" Width="100%">
                    <table width="100%" border="0" cellpadding="3" cellspacing="0" id="tblform">
                        <tr>
                            <th colspan="4">Equipment Insurance Details </th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>Liability Insurance </th>
                            <th>Cargo Insurance </th>
                            <th>Physical Insurance </th>
                        </tr>
                        <tr>
                            <td style="height: 26px"><strong>Company</strong></td>
                            <td style="height: 26px">
                                <asp:TextBox ID="txtcliability" runat="server" MaxLength="150"></asp:TextBox></td>
                            <td style="height: 26px">
                                <asp:TextBox ID="txtccargo" runat="server" MaxLength="150"></asp:TextBox></td>
                            <td style="height: 26px">
                                <asp:TextBox ID="txtcphysical" runat="server" MaxLength="150"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td><strong>Policy#</strong></td>
                            <td>
                                <asp:TextBox ID="txtpliability" runat="server" MaxLength="100"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtpcargo" runat="server" MaxLength="100"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtpphysical" runat="server" MaxLength="100"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td><strong>Limit($)</strong></td>
                            <td>
                                <asp:TextBox ID="txtlliability" runat="server" MaxLength="18"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtlliability"
                                    Display="Dynamic" ErrorMessage="Please enter a valid amount." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                            <td>
                                <asp:TextBox ID="txtlcargo" runat="server" MaxLength="18"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtlcargo"
                                    Display="Dynamic" ErrorMessage="Please enter a valid amount." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                            <td>
                                <asp:TextBox ID="txtlphysical" runat="server" MaxLength="18"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtlphysical"
                                    Display="Dynamic" ErrorMessage="Please enter a valid amount." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                        </tr>
                        <tr>
                            <td><strong>Broker</strong></td>
                            <td>
                                <asp:TextBox ID="txtbliability" runat="server" MaxLength="100"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtbcargo" runat="server" MaxLength="100"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtbphysical" runat="server" MaxLength="100"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td><strong>Phone</strong></td>
                            <td>
                                <%--<asp:TextBox ID="txtphliabity" runat="server" MaxLength="18"></asp:TextBox>--%>
                                <uc2:Phone ID="txtphliabity" runat="server" />
                            </td>
                            <td>
                                <%--<asp:TextBox ID="txtphcargo" runat="server" MaxLength="18"></asp:TextBox>--%>
                                <uc2:Phone ID="txtphcargo" runat="server" />
                            </td>
                            <td>
                                <%--<asp:TextBox ID="txtphphysical" runat="server" MaxLength="18"></asp:TextBox>--%>
                                <uc2:Phone ID="txtphphysical" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Fax</strong></td>
                            <td>
                                <%--<asp:TextBox ID="txtfliability" runat="server" MaxLength="18"></asp:TextBox>--%>
                                <uc2:Phone ID="txtfliability" runat="server" />
                            </td>
                            <td>
                                <%--<asp:TextBox ID="txtfcargo" runat="server" MaxLength="18"></asp:TextBox>--%>
                                <uc2:Phone ID="txtfcargo" runat="server" />
                            </td>
                            <td>
                                <%--<asp:TextBox ID="txtfphysical" runat="server" MaxLength="18"></asp:TextBox>--%>
                                <uc2:Phone ID="txtfphysical" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Email</strong></td>
                            <td>
                                <asp:TextBox ID="txtemailliabi" runat="server" MaxLength="100"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="reqemailliabi" runat="server" ControlToValidate="txtemailliabi"
                                    ErrorMessage="Please enter valid emial id" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
                            <td>
                                <asp:TextBox ID="txtemailcargo" runat="server" MaxLength="100"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="reqemailcargo" runat="server" ControlToValidate="txtemailcargo"
                                    ErrorMessage="Please enter valid emial id" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
                            <td>
                                <asp:TextBox ID="txtemailphy" runat="server" MaxLength="100"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="reqemailphy" runat="server" ControlToValidate="txtemailphy"
                                    ErrorMessage="Please enter valid emial id" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
                        </tr>
                        <tr>
                            <td style="height: 26px"><strong>Expiry Date </strong></td>
                            <td style="height: 26px">
                                <uc1:DatePicker ID="txteliaility" runat="server" IsDefault="true" IsRequired="false" CountNextYears="2" CountPreviousYears="0" />
                            </td>
                            <td style="height: 26px">
                                <uc1:DatePicker ID="txtecargo" runat="server" IsDefault="true" IsRequired="false" CountNextYears="2" CountPreviousYears="0" />
                            </td>
                            <td style="height: 26px">
                                <uc1:DatePicker ID="txtephysical" runat="server" IsDefault="true" IsRequired="false" CountNextYears="2" CountPreviousYears="0" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="0" id="tblform">
                    <tr>
                        <th colspan="4">Login Details / User Permissions </th>
                    </tr>
                    <tr>
                        <td width="15%" align="right">User ID    : </td>
                        <td width="25%">
                            <asp:TextBox ID="txtuserid" runat="server" MaxLength="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtuserid"
                                Display="Dynamic" ErrorMessage="Please enter valid user id.">*</asp:RequiredFieldValidator></td>
                        <td width="15%" align="right">Access to Payments :</td>
                        <td>
                            <asp:CheckBox ID="chkpayments" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="15%" align="right">Password : </td>
                        <td width="25%">
                            <asp:TextBox ID="txtpwd" runat="server" MaxLength="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtpwd"
                                Display="Dynamic" ErrorMessage="Please enter valid password.">*</asp:RequiredFieldValidator></td>
                        <td width="15%" align="right">Access to Documents : </td>
                        <td>
                            <asp:CheckBox ID="chkdocuments" runat="server" />
                        </td>
                    </tr>


                    <tr id="trDispatcherPermission" runat="server" style="display: none">
                        <td width="15%" align="right"></td>
                        <td width="25%"></td>
                        <td width="15%" align="right">Access to Load Planner : </td>
                        <td>
                            <asp:CheckBox ID="chkDispatcherPermission" runat="server" /></td>
                    </tr>
                </table>
                <%-- SLine 2017 Enhancement for Office Location Starts --%>
                <table width="100%" border="0" cellpadding="3" cellspacing="0" id="tblform" style="margin-top: 15px">
                    <tr>
                        <th colspan="4">Office Location </th>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Repeater ID="rptLocation" runat="server">
                                <HeaderTemplate>
                                    <table width="100%" border="0" cellpadding="2" cellspacing="0" id="tblform">
                                        <tr>
                                            <th width="15%" style="text-align: right; font-weight: normal">Office Location</th>
                                            <th width="2%">&nbsp;</th>
                                            <th style="text-align: left; font-weight: normal">Default Location</th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td width="15%" align="right">
                                            <asp:HiddenField ID="hdnLocation" runat="server" Value='<%# Eval("bint_OfficeLocationId") %>' />
                                            <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("nvar_OfficeLocationName") %>' />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="cbLocation" runat="server" />
                                        </td>
                                        <td>
                                            <input type="radio" name="rbDefaultLocation" value='<%# Eval("bint_OfficeLocationId") %>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
                <%-- SLine 2017 Enhancement for Office Location Ends --%>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="0" id="tblform">
                    <tr>
                        <th colspan="4">Contact Details </th>
                    </tr>
                    <tr>
                        <td width="15%" align="right">Email : </td>
                        <td width="25%">
                            <asp:TextBox ID="txtemail" runat="server" MaxLength="100"></asp:TextBox>
                        </td>
                        <td width="15%" align="right">Home Phone  : </td>
                        <td>
                            <%--                    <asp:TextBox ID="txthomeph" runat="server" MaxLength="18"></asp:TextBox>
                            --%>
                            <uc2:Phone ID="txthomeph" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">Work Phone  : </td>
                        <td>
                            <%--                    <asp:TextBox ID="txtworkph" runat="server" MaxLength="18"></asp:TextBox>
                            --%>
                            <uc2:Phone ID="txtworkph" runat="server" />
                            &nbsp;
                    Extn.:
                    <asp:TextBox ID="txtextn" runat="server" Width="50px" MaxLength="10"></asp:TextBox></td>
                        <td align="right">Fax : </td>
                        <td>
                            <%--                    <asp:TextBox ID="txtfax" runat="server" MaxLength="18"></asp:TextBox>
                            --%>
                            <uc2:Phone ID="txtfax" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">Mobile : </td>
                        <td valign="top">
                            <%--                    <asp:TextBox ID="txtmobile" runat="server" MaxLength="18"></asp:TextBox>
                            --%>
                            <uc2:Phone ID="txtmobile" runat="server" />
                        </td>
                        <td align="right" valign="top">Notes : </td>
                        <td valign="top">
                            <asp:TextBox ID="txtnotes" runat="server" Height="45px" MaxLength="1000" TextMode="MultiLine" Width="204px"></asp:TextBox></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="0" id="tblform">
                    <tr>
                        <th colspan="4">Home Address </th>
                    </tr>
                    <tr>
                        <td width="15%" align="right" style="height: 27px">Street    : </td>
                        <td width="25%" style="height: 27px">
                            <asp:TextBox ID="txtstreet" runat="server" MaxLength="200"></asp:TextBox>
                        </td>
                        <td width="15%" align="right" style="height: 27px">State   :</td>
                        <td style="height: 27px">
                            <uc3:USState ID="txtstate" runat="server"></uc3:USState>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Appt.#    :</td>
                        <td>
                            <asp:TextBox ID="txtappt" runat="server" MaxLength="100"></asp:TextBox></td>
                        <td align="right">Zip Code : </td>
                        <td>
                            <asp:TextBox ID="txtzipcode" runat="server" MaxLength="20"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">City    : </td>
                        <td valign="top">
                            <asp:TextBox ID="txtcity" runat="server" MaxLength="100"></asp:TextBox>
                        </td>
                        <td align="right" valign="top"></td>
                        <td valign="top"></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
        </tr>
    </table>


    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
        <tr>
            <td align="right">
                <asp:Button ID="btnAgreements" runat="server" Text="Agreements" CssClass="btnstyle" Height="22px" Visible="false" />
                <asp:Button ID="bynsave" runat="server" Text="Save" CssClass="btnstyle" Height="22px" OnClick="btnSave_Click" />
                <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="btnstyle" Height="22px" OnClick="btnCancel_Click" CausesValidation="False" UseSubmitBehavior="False" /></td>
        </tr>
    </table>

    </td>
  </tr>
</table>
    <%-- SLine 2017 Enhancement for Office Location Starts --%>
    <script type="text/javascript">
        function setCheckedValue(radioObj, newValue) {
            if (!radioObj)
                return;
            var radioLength = radioObj.length;
            if (radioLength == undefined) {
                radioObj.checked = (radioObj.value == newValue.toString());
                return;
            }
            for (var i = 0; i < radioLength; i++) {
                radioObj[i].checked = false;
                if (radioObj[i].value == newValue.toString()) {
                    radioObj[i].checked = true;
                }
            }
        }
    </script>
    <%-- SLine 2017 Enhancement for Office Location Ends --%>
</asp:Content>

