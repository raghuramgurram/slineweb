using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class NewCarrierAccount : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblNameError.Visible = false;
        if (!Page.IsPostBack)
        {
            if (Request.QueryString.Count > 0)
            {
                if (Request.QueryString[0].Trim().EndsWith("New", true, System.Globalization.CultureInfo.CurrentCulture))
                {
                    ViewState["Mode"] = "New";
                    ViewState["ID"] = Request.QueryString[0].Trim().Replace("New", "");
                    Session["TopHeaderText"] = "New " + Convert.ToString(Session["Type"]) + " Account ";
                }
                else
                {
                    ViewState["Mode"] = "Edit";
                    ViewState["ID"] = Request.QueryString[0].Trim();
                    Session["TopHeaderText"] = "Edit " + Convert.ToString(Session["Type"]) + " Account ";
                    try
                    {
                        FillDetails(Convert.ToInt64(ViewState["ID"]));
                    }
                    catch
                    {
                    }
                }
           }
        }
    }

    private void FillDetails(long ID)
    {
        string strQuery = "";
        //strQuery = "SELECT [nvar_FirstName],[nvar_LastName],[nvar_UserId],[nvar_Password],[nvar_UserEmail],[bit_Payments],[bit_Documents] " +
        //" FROM [eTn_UserLogin] where [bint_UserLoginId] = " + ID;
        //DataTable dt = DBClass.returnDataTable(strQuery);
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCarrierAccountDetails", new object[] { ID });
        if (ds.Tables.Count > 0)
        {
            ViewState["UserId"] = Convert.ToString(ds.Tables[0].Rows[0][2]).Trim();
            txtFirstName.Text = Convert.ToString(ds.Tables[0].Rows[0][0]).Trim();
            txtLastName.Text = Convert.ToString(ds.Tables[0].Rows[0][1]).Trim();
            txtUserID.Text = Convert.ToString(ds.Tables[0].Rows[0][2]).Trim();
            txtPassword.Text = Convert.ToString(ds.Tables[0].Rows[0][3]).Trim();
            txtEmail.Text = Convert.ToString(ds.Tables[0].Rows[0][4]).Trim();
            chkPayments.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0][5]);
            chkDocuments.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0][6]);
        }
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Manager/" + Convert.ToString(Session["Type"]) + "Account.aspx?"+Convert.ToString(ViewState["ID"]));
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            lblNameError.Visible = false;
            if (ViewState["Mode"] != null)
            {
                if (string.Compare(Convert.ToString(ViewState["UserId"]), txtUserID.Text.Trim(), true, System.Globalization.CultureInfo.CurrentCulture) != 0)
                {
                    if (Convert.ToInt32(DBClass.executeScalar("Select Count(bint_UserLoginId) from eTn_UserLogin Where lower(nvar_UserId) ='" + txtUserID.Text.Trim().ToLower() + "'")) > 0)
                    {
                        lblNameError.Visible = true;
                        return;
                    }
                }
                long RoleID = 0;
                if (Session["Type"] != null)
                {
                    string strQuery = "select [bint_RoleId] from [eTn_Role] where lower([nvar_RoleName])='" + Convert.ToString(Session["Type"]).ToLower() + "'";
                    RoleID = Convert.ToInt64(DBClass.executeScalar(strQuery));
                }
                object[] objParams = new object[] { txtFirstName.Text.Trim(), txtLastName.Text.Trim(), txtUserID.Text.Trim(), txtPassword.Text.Trim(), txtEmail.Text.Trim(), chkPayments.Checked ? 1 : 0, chkDocuments.Checked ? 1 : 0, RoleID, Convert.ToInt64(ViewState["ID"]) };
                if (ViewState["ID"] != null)
                {
                    if (Convert.ToString(ViewState["Mode"]).StartsWith("Edit", true, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Update_Users", objParams) > 0)
                        {
                            Response.Redirect("~/Manager/" + Convert.ToString(Session["Type"]) + "Account.aspx");
                        }
                    }
                    else if (Convert.ToString(ViewState["Mode"]).StartsWith("New", true, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Add_Users", objParams) > 0)
                        {
                            Response.Redirect("~/Manager/" + Convert.ToString(Session["Type"]) + "Account.aspx");
                        }
                    }
                }
            }
        }
    }
}

