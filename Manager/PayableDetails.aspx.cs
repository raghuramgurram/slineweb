using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class PayableDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Load Charges";
        if (!IsPostBack)
        {
            string BackPage = "";
            if (Request.QueryString.Count > 0)
            {
                try
                {
                    string[] strParams = Convert.ToString(Request.QueryString[0]).Trim().Split('^');
                    ViewState["LoadId"] = Convert.ToInt64(strParams[0]);
                    LoadPayments1.LoadId = Convert.ToString(ViewState["LoadId"]);
                    LoadPayments1.PlayerId = Convert.ToString(strParams[1]);
                    lblBarText.Text = "Load Id : " + Convert.ToString(ViewState["LoadId"]);
                    LoadPayments1.RedirectBackURL = Convert.ToString("~/Manager/DisplayReport.aspx");
                }
                catch
                {
                }
            }
        }
    }
}
