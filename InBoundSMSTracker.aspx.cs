using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections.Specialized;

public partial class InBoundSMSTracker : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FileStream fs = null;
        if (!File.Exists(Server.MapPath("~/InBoundUrls.txt")))
            using (fs = File.Create(Server.MapPath("~/InBoundUrls.txt"))) { }
        TextWriter tw = new StreamWriter(Server.MapPath("~/InBoundUrls.txt"), true);
        try
        {
            // write a line of text to the file
            tw.WriteLine(HttpContext.Current.Request.Url.ToString());
            // close the stream
            //SLine 2017 Enhancement for Office Location Starts
            long officeLocationId = 0;
            if (Session["OfficeLocationID"] != null)
            {
                officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
            }
            //SLine 2017 Enhancement for Office Location Ends
            string queryString = HttpContext.Current.Request.Url.Query;
            if (!string.IsNullOrEmpty(queryString))
            {
                NameValueCollection queryStringCollection = HttpUtility.ParseQueryString(queryString);
                if (!string.IsNullOrEmpty(Convert.ToString(queryStringCollection.Get("to"))) && (GetFromXML.NexmoSMSNumber.ToString() == queryStringCollection.Get("to")))
                {
                    //SLine 2017 Enhancement for Office Location
                    SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_Insert_eTn_InboundSMSLogDetails", new object[] { queryStringCollection.Get("msisdn"), queryStringCollection.Get("to"), queryStringCollection.Get("messageId"), queryStringCollection.Get("text"), queryStringCollection.Get("type"), queryStringCollection.Get("message-timestamp"), queryString, officeLocationId });
                }
                else
                {
                    tw.WriteLine("Not saved.");
                }
            }
            else
            {
                tw.WriteLine("Not saved.");
            }
        }
        finally { tw.Close(); }
       
    }
}
