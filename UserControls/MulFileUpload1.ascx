<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MulFileUpload1.ascx.cs" Inherits="UserControls_MulFileUpload1" %>
<STYLE>.noborder{border:0px solid #FFF !important;}</STYLE>
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border:none !important">
    <tr style="padding-top: 5px; padding-bottom: 5px; border:none !important">
        <td align="left" valign="top" style="border:none;">           
            <asp:FileUpload ID="FileUpload" runat="server" Width="337px" />
            <%--<input id="FileUpload1" runat="server" type="file" />--%>
        </td>
    </tr>    
    <tr style="padding-top: 5px; padding-bottom: 5px; border:none !important">
       
        <td align="right" valign="top" style="border:none;">
            <asp:Button ID="btnAddToList" runat="server" CssClass="btnstyle" 
                OnClick="btnAddToList_Click" Text="Add" UseSubmitBehavior="False" 
                Width="55px" />
                
            <asp:Button ID="btnDelToList" runat="server" CssClass="btnstyle" OnClick="btnDel_Click"
                Text="Delete" UseSubmitBehavior="False" Width="66px" />
        </td>
    </tr>
    <tr style="border:none;">
        <td align="left" valign="top"  style="height:100px; border:none !important">
            <asp:ListBox ID="lstFileNames" runat="server" Width="337px" Height="100%" CssClass="noborder"></asp:ListBox>
        </td>
    </tr>
</table>