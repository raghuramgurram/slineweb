<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProfitLossCharts.aspx.cs"
    Inherits="Manager_ProfitLossGraphs" MasterPageFile="~/Manager/MasterPage.master"
    Title="S Line Transport Inc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<style>
.check input[type=checkbox]{height:auto; vertical-align:top; display:inline-block;}
</style>
<script>
    function setsearchType(id) {
         document.getElementById('<%= searchType.ClientID %>').value = id;
    }
</script>
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>
                <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="middle">
                        <asp:Label ID="lblMonth" runat="server" Text="Month : "></asp:Label>
                            <asp:DropDownList ID="ddlMonth" runat="server">
                                 <asp:ListItem Text="Jan" Value="1"/>
                                 <asp:ListItem Text="Feb" Value="2"/>
                                 <asp:ListItem Text="Mar" Value="3"/>
                                 <asp:ListItem Text="Apr" Value="4"/>
                                 <asp:ListItem Text="May" Value="5"/>
                                 <asp:ListItem Text="Jun" Value="6"/>
                                 <asp:ListItem Text="July" Value="7"/>
                                 <asp:ListItem Text="Aug" Value="8"/>
                                 <asp:ListItem Text="Sep" Value="9"/>
                                 <asp:ListItem Text="Oct" Value="10"/>
                                 <asp:ListItem Text="Nov" Value="11"/>
                                 <asp:ListItem Text="Dec" Value="12"/>
                            </asp:DropDownList>&nbsp; &nbsp;
                        <asp:Label ID="lblYear" runat="server" Text="Year : "></asp:Label>
                            <asp:DropDownList ID="ddlYear" runat="server">
                            </asp:DropDownList>
                        <asp:HiddenField runat="server" ID="searchType"/>
                        <%--<asp:Button ID="btnSearch" runat="server" CssClass="btnStyle" Text="Search" onClick="btnSearch_Click"/>--%>
                        <span id="spnAllOfficeLocation" runat="server"><label style="display:inline-block; vertical-align:text-bottom;">All Office Locations:</label> <asp:CheckBox ID="chkAllOfficeLocation" runat="server" CssClass="check"></asp:CheckBox></span>
                        <asp:Button ID="btnChartMonthly" runat="server" CssClass="btnStyle" Text="Monthly Charts" OnClientClick="return setsearchType(1)"/>
                        <asp:Button ID="btnChartDaily" runat="server" CssClass="btnStyle" Text="Daily Charts" OnClientClick="return setsearchType(2)"/>
                        <asp:Button ID="btnChartYearly" runat="server" CssClass="btnStyle" Text="Year Wise Charts" OnClientClick="return setsearchType(3)"/>
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="3" src="../Images/pix.gif" width="1" />
                        </td>
                    </tr>
                </table>
                <asp:Label ID="lblDisplay" runat="server" Text="" Width="100%"></asp:Label>
                <asp:Chart ID="Chart1" runat="server" BorderlineColor="Black" 
                    BorderlineDashStyle="Solid" BackSecondaryColor="White"
                    Height="300px" Width="1200px" Palette="None" 
                    PaletteCustomColors="255, 128, 128">
                    <Titles>
                        <asp:Title Name="Title1" Text="Loads Line Chart" Alignment="TopCenter" Font="Verdana, 12pt, style=Bold">
                        </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Series1" CustomProperties="MaxPixelPointWidth=50" ShadowOffset="2" IsValueShownAsLabel="false" ChartType="Line" BorderWidth="2">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                            BorderDashStyle="Solid" BorderWidth="2" Area3DStyle-Enable3D="false">
                            <AxisX>
                                <MajorGrid Enabled="False" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="3" src="../Images/pix.gif" width="1" />
                        </td>
                    </tr>
                </table>
                <asp:Chart ID="Chart2" runat="server" BorderlineColor="Black" 
                    BorderlineDashStyle="Solid" BackSecondaryColor="White"
                    Height="300px" Width="400px" Palette="None" PaletteCustomColors="Green">
                    <Titles>
                        <asp:Title Name="Title1" Text="Receivable Line Chart" Alignment="TopCenter" Font="Verdana, 12pt, style=Bold">
                        </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Series4" CustomProperties="DrawingStyle=Pie,
         PieDrawingStyle=Concave, MaxPixelPointWidth=50" ShadowOffset="2" ChartType="Line" 
                            IsValueShownAsLabel="false" BorderWidth="2">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea2" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                            BorderDashStyle="Solid" BorderWidth="2">
                            <AxisX>
                                <MajorGrid Enabled="False" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
                <asp:Chart ID="Chart3" runat="server" BorderlineColor="Black" 
                    BorderlineDashStyle="Solid" BackSecondaryColor="White"
                    Height="300px" Width="400px" Palette="None" PaletteCustomColors="Red">
                    <Titles>
                        <asp:Title Name="Title1" Text="Payable Line Chart" Alignment="TopCenter" Font="Verdana, 12pt, style=Bold">
                        </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Series5" CustomProperties="DrawingStyle=Pie,
         PieDrawingStyle=Concave, MaxPixelPointWidth=50" ShadowOffset="2" ChartType="Line" IsValueShownAsLabel="false" BorderWidth="2">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea2" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                            BorderDashStyle="Solid" BorderWidth="2">
                            <AxisX>
                                <MajorGrid Enabled="False" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
                <asp:Chart ID="Chart4" runat="server" BorderlineColor="Black" 
                    BorderlineDashStyle="Solid" BackSecondaryColor="White"
                    Height="300px" Width="400px" Palette="None" PaletteCustomColors="Blue">
                    <Titles>
                        <asp:Title Name="Title1" Text="Margin Line Chart" Alignment="TopCenter" Font="Verdana, 12pt, style=Bold">
                        </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Series6" CustomProperties="DrawingStyle=Pie,
         PieDrawingStyle=Concave, MaxPixelPointWidth=50" ShadowOffset="2" ChartType="Line" IsValueShownAsLabel="false" BorderWidth="2">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea2" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                            BorderDashStyle="Solid" BorderWidth="2">
                            <AxisX>
                                <MajorGrid Enabled="False" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </td>
        </tr>
    </table>
</asp:Content>
