<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DriverWisePrint.aspx.cs" Inherits="Manager_DriverWisePrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <style>
        .btnstyle {
	    font-family: Arial, Helvetica, sans-serif;
	    font-size: 13px;
	    color: #FFFFFF;
	    background-image: url(../Images/btnbgr.gif);
	    background-repeat: repeat-x;
	    border: 1px solid #0A4B73;
	    height: 22px;
        cursor:pointer;
        }
    </style>
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="updpage" runat="server"  UpdateMode="Conditional" ChildrenAsTriggers="false">
            <ContentTemplate>
    <div>
    <asp:Label ID="lblNorecords" runat="server" Visible="false" Text="No Results found" Font-Bold="true" Font-Size="XX-Large" ForeColor="red"></asp:Label>
        <asp:Repeater ID="rptDrivers" runat="server">
        <ItemTemplate>
        <table id="tbldispaly"  cellpadding="0" cellspacing="0" border="0" width="100%" style="page-break-after:always">
    <tr id="row">
        <td>
    <table  border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, sans-serif;
        color: #4D4B4C; font-size: 12px; height: 1px;" width="100%">
        <tr>
            <td valign="top">
                <asp:Label ID="lblHeader" runat="server" Font-Bold="True" ForeColor="Blue"
        Style="font-family: Arial, sans-serif; font-size: 12px;" Text='<%# Eval("Header")%>' ></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="height: 4px" valign="top">
                &nbsp;<asp:Label ID="lblSearchCriteria" runat="server" Font-Bold="True" ForeColor="Blue"
        Style="font-family: Arial, sans-serif; font-size: 12px;" Text='<%# Eval("SearchCriteria")%>'>
    </asp:Label>    
    </td>            
        </tr>
        
    </table>
    </td>
    </tr>
    <tr>
    <td>
    <table  border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, sans-serif;
        color: #4D4B4C; font-size: 12px; height: 1px;" width="100%" id="Table1">
        <tr><td>
       
    <asp:Label ID="lblDisplay" runat="server" Text='<%# Eval("InnerHtml")%>' Width="100%"></asp:Label>&nbsp;
    
    <table id="tblResult" runat="server" width="100%">
        <tr>
            <td colspan="2" align="right" Style="font-family: Arial, sans-serif; font-size: 12px;">
                <asp:Label ID="lblPaymentTotal" runat="server" Text='<%# Eval("Total")%>'></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2" Style="font-family: Arial, sans-serif; font-size: 12px;">
                <asp:Label ID="lblPaid" runat="server" Text='<%# Eval("Paid")%>'></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2" Style="font-family: Arial, sans-serif; font-size: 12px;">
                <asp:Label ID="lblPending" runat="server" Text='<%# Eval("Pending")%>'></asp:Label>
            </td>
        </tr>
         <tr>
            <td align="right" colspan="2" Style="font-family: Arial, sans-serif; font-size: 12px; padding-top:10px">
                <asp:Label ID="lblEmailNotAvailable" runat="server" Style="color:red;font-weight:bold" Text='Email ID Not Available'  Visible ='<%# string.IsNullOrEmpty(Eval("Email").ToString())%>'></asp:Label>
                <asp:Button ID="btnSendEmail" runat="server" CssClass="btnstyle" CommandArgument ='<%# Eval("Email") %>' OnClick="btnSendEmail_Click"  Text="Send Email" Visible='<%# !string.IsNullOrEmpty(Eval("Email").ToString())%>'/>
            </td>
        </tr>
        <tr>
            <td colspan="2" Style="font-family: Arial, sans-serif; font-size: 14px;color:Red;font-weight:bold;">
                Total  deduction $ ______________________________
            </td>
        </tr>
        <tr>
            <td colspan="2" Style="font-family: Arial, sans-serif; font-size: 14px;color:Red;font-weight:bold;">
                Paid  check # __________________________________
            </td>
        </tr>
        <tr>
            <td colspan="2" Style="font-family: Arial, sans-serif; font-size: 14px;color:Red;font-weight:bold;">
               Total  Loads  on this sheet : <asp:Label ID="lblcount" runat="server" Text='<%# Eval("Count")%>'></asp:Label>
            </td>
        </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
        </ItemTemplate>
        </asp:Repeater>
    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
    </form>
</body>
</html>
