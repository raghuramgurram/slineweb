<%@ Page AutoEventWireup="true" CodeFile="AddNewLoadDestination.aspx.cs" Inherits="addnewloaddestination"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc3" %>

<%@ Register Src="~/UserControls/TimePicker.ascx" TagName="TimePicker" TagPrefix="uc2" %>

<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td>
        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="tblform1">
            <tr>
                <td>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="869px" />
                </td>
            </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <uc3:GridBar ID="GridBar1" runat="server" HeaderText="New Destination"/>
                </td>
            </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <img alt="" height="3" src="../Images/pix.gif" width="1" /></td>
            </tr>
        </table>
      <table width="100%" border="0" cellpadding="3" cellspacing="0" id="tblform">
        <tr>
          <td width="15%" align="right" style="height: 25px"><strong>P.O.# : </strong></td>
          <td colspan="2" style="height: 25px">
              <asp:TextBox ID="txtpo" runat="server" MaxLength="50"></asp:TextBox>
              </td>
          </tr>
        <tr>
          <td align="right" style="height: 25px">Pieces :</td>
          <td colspan="2" style="height: 25px">
              <asp:TextBox ID="txtpieces" runat="server" MaxLength="15"></asp:TextBox>
              <asp:RegularExpressionValidator ID="regPieces" runat="server" Display="Dynamic" ErrorMessage="Please enter valid number of pieces"
                  ValidationExpression="\d*" ControlToValidate="txtpieces">*</asp:RegularExpressionValidator></td>
          </tr>
        <tr>
          <td align="right" style="height: 26px">Weight : </td>
          <td colspan="2" style="height: 26px">
              <asp:TextBox ID="txtweight" runat="server" MaxLength="18"></asp:TextBox>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                  ErrorMessage="Please enter valid weight" ValidationExpression="\d*" ControlToValidate="txtweight">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr>
          <td align="right" style="height: 26px">
              Delivery Appt. Date  : </td>
          <td colspan="2" style="height: 26px">
          <uc1:DatePicker ID="txtpickup" runat="server" Visible="true" IsDefault="true" IsRequired="false" CountNextYears="2" CountPreviousYears="0" />
             &nbsp;
               From Time :&nbsp;
              <uc2:TimePicker ID="txttime" runat="server" ReqFieldValidation="false" Visible="true" />
              &nbsp;&nbsp;&nbsp;
               To Time :&nbsp;
              <uc2:TimePicker ID="txtTOtime" runat="server" ReqFieldValidation="false" Visible="true" />
          </td>
          </tr>
        <tr>
          <td align="right">Appt. Given by : </td>
          <td colspan="2">
              <asp:TextBox ID="txtapptgivenby" runat="server" MaxLength="200"></asp:TextBox>&nbsp; Appt.#:
              <asp:TextBox ID="txtappt" runat="server" MaxLength="50"></asp:TextBox>
          </td>
        </tr>
        <tr>
          <td align="right">Company : </td>
          <td colspan="2">
             <asp:DropDownList ID="ddlcompany" runat="server" Width="374px" AutoPostBack="True" OnSelectedIndexChanged="ddlcompany_SelectedIndexChanged">
                 <asp:ListItem>--Select Company--</asp:ListItem>
              </asp:DropDownList>
              <asp:LinkButton ID="lblCompany" runat="server" Text="Add New Company" CausesValidation="False" OnClick="lblCompany_Click" />
              <asp:CompareValidator ID="cmpCompany" runat="server" ControlToValidate="ddlcompany"
                  Display="Dynamic" ErrorMessage="Please select a company." Operator="NotEqual"
                  ValueToCompare="--Select Company--">*</asp:CompareValidator></td>
          </tr>
        <tr valign="top">
          <td align="right">Select Address : </td>
          <td width="25%">
              <asp:DropDownList ID="ddladdress" runat="server" Width="374px" OnSelectedIndexChanged="ddladdress_SelectedIndexChanged" AutoPostBack="True">
                  <asp:ListItem>--Select Address--</asp:ListItem>
              </asp:DropDownList></td>
          <td>
                <asp:LinkButton ID="lnkAddress" runat="server" Text="Add New Address" CausesValidation="False" OnClick="lnkAddress_Click" />
              <asp:CompareValidator ID="cmpAddress" runat="server" ControlToValidate="ddladdress"
                  Display="Dynamic" ErrorMessage="Please select address." Operator="NotEqual" ValueToCompare="--Select Address--"
                  Width="16px">*</asp:CompareValidator><br />
                <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
          </td>
        </tr>        
        <tr>
          <td colspan="3"><img src="../Images/pix.gif" width="1" height="5" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../Images/pix.gif" alt="" width="1" height="3" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
          <tr>
            <td align="right">
               <asp:Button ID="btnsave" runat="server" Height="22px" Text="Save" CssClass="btnstyle" OnClick="btnsave_Click"/>&nbsp;
                <asp:Button ID="btncancel" runat="server" Height="22px" Text="Cancel" CssClass="btnstyle" OnClick="btncancel_Click" CausesValidation="False" UseSubmitBehavior="False"/></td>
          </tr>
        </table>
    
    </td>
  </tr>
</table>
</asp:Content>

