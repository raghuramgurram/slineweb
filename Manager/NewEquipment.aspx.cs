using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class NewEquipment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        txtlastbit.DisplayError = false;
        txttag.DisplayError = false;
        txtdate.DisplayError = false;
        txtbought.DisplayError = false;
        lblNameError.Visible = false;
        Session["TopHeaderText"] = "New / Edit Equipment";
        if (!Page.IsPostBack)
        {
            fillTypes();
            lnlInsurenceEqup.Enabled = false;
            if (Request.QueryString.Count > 0)
            {
                ViewState["ID"] = Request.QueryString[0].Trim();
                try
                {
                    ViewState["Mode"] = "Edit";
                    ViewState["ID"] = Convert.ToInt64(Request.QueryString[0]);
                    FillDetails(Convert.ToInt64(ViewState["ID"]));
                }
                catch
                {
                }
                Session["TopHeaderText"] = "Edit Equipment";
            }
            else
            {
                if (ViewState["ID"] == null)
                {
                    ViewState["Mode"] = "New";
                    lnlInsurenceEqup.Enabled=false;
                    Session["TopHeaderText"] = "New Equipment";
                }
                else
                {
                    try
                    {
                        ViewState["Mode"] = "Edit";
                        FillDetails(Convert.ToInt64(ViewState["ID"]));
                        Session["TopHeaderText"] = "Edit Equipment";
                    }
                    catch
                    {
                    }
                }
            }               
        }
    }
    private void fillTypes()
    {
        //ddltype.DataSource = DBClass.returnDataTable("select bint_LoadTypeId,nvar_LoadTypeDesc from eTn_LoadType order by [nvar_LoadTypeDesc]");
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadTypeDetails");
        if (ds.Tables.Count > 0)
        {
            ddltype.DataSource = ds.Tables[0];
            ddltype.DataTextField = "nvar_LoadTypeDesc";
            ddltype.DataValueField = "bint_LoadTypeId";
            ddltype.DataBind();
            ddltype.Items.Insert(0,new ListItem( "Select","0"));
        }
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    private void FillDetails(long ID)
    {
        //string strQuery = "SELECT [nvar_EquipmentName],[nvar_Type],[bit_Reefer],[bit_Status],[nvar_EquipState],[bit_Lessor],[num_Owner],[date_LastBitInspection],[date_TagExpiration],[nvar_City],[nvar_State],"+
        //                   "[date_LastLocDateAndTime],[num_InitialOdometer],[num_CurrentOdometer],[num_TotalMiles],[nvar_License],[nvar_VIN],[nvar_Plate],[nvar_Colour],[nvar_Title],[date_DateBought],[nvar_Notes]," +
        //                   "[nvar_Model],[int_Year],[int_Weight],[int_Height],[nvar_TireSize],[nvar_Make],[num_Cost],[num_Down],[num_CurrentLoan],[nvar_LoanNumber],[nvar_CreditorName],[num_CreditorPhone]"+
        //                   " FROM [eTn_Equipment] where [bint_EquipmentId]='"+ID+"'";
        //DataTable dt = DBClass.returnDataTable(strQuery);
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetEquipmentDetails", new object[] { ID });
        if (ds.Tables.Count > 0)
        {
            txteqpname.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_EquipmentName"]).Trim();
            ViewState["TempName"] = Convert.ToString(ds.Tables[0].Rows[0]["nvar_EquipmentName"]).Trim();
            ddltype.SelectedIndex = ddltype.Items.IndexOf(ddltype.Items.FindByText(Convert.ToString(ds.Tables[0].Rows[0]["nvar_Type"]).Trim()));
            cbreef.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["bit_Reefer"]);
            if (Convert.ToBoolean(ds.Tables[0].Rows[0]["bit_Status"]))
                ddlstatus.SelectedIndex = 0;
            else
                ddlstatus.SelectedIndex = 1;
            ddlstate.SelectedIndex = ddlstate.Items.IndexOf(ddlstate.Items.FindByText(Convert.ToString(ds.Tables[0].Rows[0]["nvar_EquipState"]).Trim()));
            //ddlstate.SelectedIndex = ddlstate.Items.IndexOf(ddlstate.Items.FindByText(Convert.ToString(ds.Tables[0].Rows[0]["nvar_EquipState"]).Trim()));
            cblessor.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["bit_Lessor"]);
            txtowner.Text = Convert.ToString(ds.Tables[0].Rows[0]["num_Owner"]).Trim();
            txtlastbit.Date=Convert.ToString(ds.Tables[0].Rows[0]["date_LastBitInspection"]);
            txttag.Date = Convert.ToString(ds.Tables[0].Rows[0]["date_TagExpiration"]);
            if (Convert.ToString(ds.Tables[0].Rows[0]["date_LastLocDateAndTime"]).Trim().Length>0)
            {
                DateTime dat = Convert.ToDateTime(ds.Tables[0].Rows[0]["date_LastLocDateAndTime"]);
                txtdate.Date = dat.ToShortDateString();
                ddldate.Time = dat.ToShortTimeString();
            }
            txtcity.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_City"]).Trim();
            //txtdate.Date=Convert.ToString(ds.Tables[0].Rows[0]["date_LastLocDateAndTime"]);
            ddlcity.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_State"]); ;
            //ddldate.Text = ddldate.Items.IndexOf(ddldate.Items.FindByText(Convert.ToString(ds.Tables[0].Rows[0][6]).Trim())); ;
            txtinitial.Text = Convert.ToString(ds.Tables[0].Rows[0]["num_InitialOdometer"]).Trim();
            txtcurrent.Text = Convert.ToString(ds.Tables[0].Rows[0]["num_CurrentOdometer"]).Trim();
            txtmiles.Text = Convert.ToString(ds.Tables[0].Rows[0]["num_TotalMiles"]).Trim();
            txtlicense.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_License"]).Trim();
            txtvin.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_VIN"]).Trim();
            txtplate.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_Plate"]).Trim();
            txtcolour.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_Colour"]).Trim();
            txttitle.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_Title"]).Trim();
            txtbought.Date=Convert.ToString(ds.Tables[0].Rows[0]["date_DateBought"]);
            txtnotes.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_Notes"]).Trim();
            txtmodel.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_Model"]).Trim();
            txtweight.Text = Convert.ToString(ds.Tables[0].Rows[0]["int_Weight"]).Trim();
            txttiresize.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_TireSize"]).Trim();
            txtmake.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_Make"]).Trim();
            txtyear.Text = Convert.ToString(ds.Tables[0].Rows[0]["int_Year"]).Trim();
            txtheight.Text = Convert.ToString(ds.Tables[0].Rows[0]["int_Height"]).Trim();
            txtcost.Text = Convert.ToString(ds.Tables[0].Rows[0]["num_Cost"]).Trim();
            txtloan.Text = Convert.ToString(ds.Tables[0].Rows[0]["num_CurrentLoan"]).Trim();
            txtcreditor.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_CreditorName"]).Trim();
            txtcreditorph.Text = Convert.ToString(ds.Tables[0].Rows[0]["num_CreditorPhone"]);
            txtdown.Text = Convert.ToString(ds.Tables[0].Rows[0]["num_Down"]).Trim();
            txtlon.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_LoanNumber"]).Trim();
        }
        if (ds != null) { ds.Dispose(); ds = null; }
        lnlInsurenceEqup.Enabled = true;

    }    
    protected void lnkMsg_Click(object sender, EventArgs e)
    {
        Response.Write("<script>alert(Please save Equipment details.)</script>");
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Manager/SearchEquipment.aspx");
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (string.Compare(Convert.ToString(ViewState["TempName"]), txteqpname.Text.Trim(), true, System.Globalization.CultureInfo.CurrentCulture) != 0)
            {
                if (Convert.ToInt32(DBClass.executeScalar("Select Count(bint_EquipmentId) from eTn_Equipment Where nvar_EquipmentName ='" + txteqpname.Text.Trim() + "'")) > 0)
                {
                    lblNameError.Visible = true;
                    return;
                }
            }

            if (!txtlastbit.IsValidDate)
                return;
            if (!txttag.IsValidDate)
                return;
            if (!txtdate.IsValidDate)
                return;
            if (!txtbought.IsValidDate)
                return;

            if (ViewState["Mode"] != null)
            {
                CheckIsNulls();
                object[] objParams = new object[] { txteqpname.Text.Trim(),ddltype.SelectedItem.Text,(cbreef.Checked ? 1 : 0),Convert.ToInt32(ddlstatus.SelectedValue),ddlstate.SelectedItem.Text,(cblessor.Checked ? 1 :0),Convert.ToDouble(txtowner.Text),CommonFunctions.CheckDateTimeNull(txtlastbit.Date),
                                                       CommonFunctions.CheckDateTimeNull(txttag.Date),txtcity.Text.Trim(),ddlcity.Text,CommonFunctions.CheckDateTimeNull((txtdate.Date!=null)?txtdate.Date+" "+ddldate.Time.Trim():null),Convert.ToDouble(txtinitial.Text),Convert.ToDouble(txtcurrent.Text),Convert.ToDouble(txtmiles.Text),txtlicense.Text.Trim(),txtvin.Text.Trim(),txtplate.Text.Trim(), 
                                                        txtcolour.Text.Trim(),txttitle.Text.Trim(),CommonFunctions.CheckDateTimeNull(txtbought.Date),txtnotes.Text.Trim(),txtmodel.Text.Trim(),Convert.ToInt32(txtyear.Text),Convert.ToInt32(txtweight.Text),Convert.ToInt32(txtheight.Text),txttiresize.Text.Trim(),txtmake.Text.Trim(),
                                                       Convert.ToDouble(txtcost.Text),Convert.ToDouble(txtdown.Text),Convert.ToDouble(txtcurrent.Text),txtloan.Text.Trim(),txtcreditor.Text.Trim(),Convert.ToDouble(txtcreditorph.Text),Convert.ToInt64(ViewState["ID"])};
                System.Data.SqlClient.SqlParameter[] objsqlparams = null;
                if (Convert.ToString(ViewState["Mode"]).StartsWith("Edit", true, System.Globalization.CultureInfo.CurrentCulture))
                {
                    objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Update_Equipment", objParams);
                    if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Update_Equipment", objsqlparams) > 0)
                    {
                        Response.Redirect("~/Manager/SearchEquipment.aspx");
                    }
                }
                else if (Convert.ToString(ViewState["Mode"]).StartsWith("New", true, System.Globalization.CultureInfo.CurrentCulture))
                {
                    objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Add_Equipment", objParams);
                    if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Add_Equipment", objsqlparams) > 0)
                    {
                        ViewState["ID"] = Convert.ToInt64(objsqlparams[objsqlparams.Length - 1].Value);
                        ViewState["TempName"] = txteqpname.Text.Trim();
                        //Response.Write("<script>alert('You can add insurence information now.')</script>");
                        lnlInsurenceEqup.Enabled = true;
                        ViewState["Mode"] = "Edit";
                        //Response.Redirect("NewEquipment.aspx?" + Convert.ToString(ViewState["ID"]));
                    }
                }
                objParams = null;
                objsqlparams = null;
            }
        }
    }
    protected void lnlInsurenceEqup_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Manager/InsurancEequipment.aspx?" + Convert.ToString(ViewState["ID"]));
    }

    private void CheckIsNulls()
    {
        if (txtcost.Text.Trim().Length == 0)
            txtcost.Text = "0";
        if (txtdown.Text.Trim().Length == 0)
            txtdown.Text = "0";
        if (txtinitial.Text.Trim().Length == 0)
            txtinitial.Text = "0";
        if (txtcurrent.Text.Trim().Length == 0)
            txtcurrent.Text = "0";
        if (txtmiles.Text.Trim().Length == 0)
            txtmiles.Text = "0";
        if (txtyear.Text.Trim().Length == 0)
            txtyear.Text = DateTime.Now.Year.ToString();
        if (txtweight.Text.Trim().Length == 0)
            txtweight.Text = "0";
        if (txtheight.Text.Trim().Length == 0)
            txtheight.Text = "0";
        if (txtowner.Text.Trim().Length == 0)
            txtowner.Text = "0";
    }
}
