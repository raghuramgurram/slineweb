<%@ Control AutoEventWireup="true" CodeFile="customertop.ascx.cs" Inherits="customertop"
    Language="C#" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>S Line Transport Inc</title>
<link href="../Styles/style.css" rel="stylesheet" type="text/css"/>
<script language="JavaScript" type="text/javascript" src="../mm_menu.js"></script>

<%--<script language="javascript" type="text/javascript">
var Temp='<%= Session["UserId"]%>'
alert(Temp);
if(Temp==null)
{
    history.forward();
}
</script>--%>
</head>
 
<BODY>
<script language="JavaScript1.2">mmLoadMenus();</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tbody>
  <tr>
    <td width="231"><img src="../Images/logo.gif" alt="Logo" width="231" height="78" /></td>
    <td valign="bottom"><table width="100%" border="0" cellpadding="0" cellspacing="0" style="background:#4C4C4C;">
      <tr>
        <td width="100%">&nbsp;</td>
        <td width="80"><a href="CustomerDashBoard.aspx"><img id="Img1" alt="Loads" border="0" height="32" name="image1" src="../Images/loads.gif"
                width="70" /></a></td>
          <td width="70">
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td width="130"><a href="CustomerPayments.aspx"><img id="Img2" alt="Payments" border="0" height="32" name="image2" src="../Images/payments.gif"
                width="124" /></a></td>
        <td width="130"><a href="CustomerTrack.aspx"><img id="Img3" alt="Track Load" border="0" height="32" name="image3" src="../Images/trackload.gif"
                width="129" /></a></td>
        <td width="130"><a href="mailto:customerservice@slinetransport.com"><img id="Img4" alt="Send Mail" border="0" height="32" name="image5" src="../Images/sendmail.gif"
                width="124" /></a></td>          
        </tr>
    </table></td>
  </tr>
  <tr>
    <td class="welcome1" colspan="2">
        Welcome ! &nbsp;<asp:Label ID="lblUserId" runat="server" Text=""></asp:Label>, &nbsp;
        <asp:LinkButton ID="lnlLogOut" runat="server" CausesValidation="false" OnClick="lnlLogOut_Click">Logout</asp:LinkButton> </td>
  </tr>  
  <tr>
    <td colspan="2" class="pagehead">
        <asp:Label ID="lblHeadText" runat="server" Text="Customer Dashboard"></asp:Label></td>
  </tr>
 </tbody>
</table>
</BODY>
</html>