using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Mobile_MobileMenu : System.Web.UI.UserControl
{
    #region propertes
    private string _SubTitle;
    public string SubTitle
    {
        get { return _SubTitle; }
        set { _SubTitle=value; }
    }
    public PageType Menuselected
    {
        get
        {
            PageType type = PageType.Loads;
            if (Session[Keys.TAB_SELECTED] != null)
            {
                try
                {
                    type = (PageType)Session[Keys.TAB_SELECTED];
                }
                catch (Exception ex)
                {
                    type = PageType.Loads;
                }
            }
            return type;
        }
    }
    #endregion

    #region Pageload
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public void LoadMenu()
    {
        if (!string.IsNullOrEmpty(SubTitle))
        {
            h3Subtitle.InnerText = SubTitle;
        }
        if (Menuselected == PageType.Loads)
        {
            linkmyLoads.CssClass = "act ftsz";
            lnkpageworks.CssClass = "ftsz";
        }
        else
        {
            linkmyLoads.CssClass = "ftsz";
            lnkpageworks.CssClass = "act ftsz";
        }
    }
    #endregion

    #region button Click
    protected void linkmyLoads_click(object sender, EventArgs e)
    {
        Response.Redirect(Keys.PAGE_LOAD_HOME);
    }
    protected void lnkpageworks_click(object sender, EventArgs e)
    {
        Response.Redirect(Keys.PAGE_PAPER_WORK_PENDING);
    }
    #endregion
    }
