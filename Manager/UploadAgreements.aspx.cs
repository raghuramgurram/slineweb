using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Manager_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString.Count > 0)
            {
                try
                {
                    string[] strValues=Convert.ToString(Request.QueryString[0]).Trim().Split('^');
                    ViewState["EmpId"] = strValues[0];
                    ViewState["UserId"] = strValues[1];
                    BindDocuments();
                }
                catch
                { 
                }
            }
        }
    }
    private void BindDocuments()
    {
        if (ViewState["EmpId"] != null)
        {
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "Get_Employee_Documents", new string[] { ViewState["EmpId"].ToString() });
            if (ds.Tables.Count > 0)
            {
                grdAggrements.DataSource = ds.Tables[0];
                grdAggrements.DataBind();

                ViewState["NoofDocuments"] = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
            }
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        for (int index = 0; index < grdAggrements.Rows.Count; index++)
        {
            if (index % 2 == 0)
            {
                for (int k = 0; k < grdAggrements.Columns.Count; k++)
                {
                    grdAggrements.Rows[index].Cells[k].CssClass = "GridItem";
                }
            }
            else
            {
                for (int k = 0; k < grdAggrements.Columns.Count; k++)
                {
                    grdAggrements.Rows[index].Cells[k].CssClass = "GridAltItem";
                }
            }
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Manager/NewEmployee.aspx?" + Convert.ToString(ViewState["EmpId"]) + "^" + Convert.ToString(ViewState["UserId"]));
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (ViewState["NoofDocuments"] != null)
        {
            if (Convert.ToInt32(ViewState["NoofDocuments"]) > 5)
            {
                Response.Write("<script>alert('you can not upload more than 6 documents.')</script>");
                return;
            }
        }
        if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Add_EmployeeDocuments", new string[] { Convert.ToString(ViewState["EmpId"]),fileUploadDocuments.FileName }) > 0)
        {
            if (!System.IO.File.Exists(Server.MapPath("~/Agreement") + "\\" + ViewState["EmpId"].ToString() + "_" + fileUploadDocuments.FileName))
                    fileUploadDocuments.SaveAs(Server.MapPath("~/Agreement") + "\\" + ViewState["EmpId"].ToString() + "_" + fileUploadDocuments.FileName);

            BindDocuments();
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        ImageButton imgBtn = (ImageButton)sender;
        if (imgBtn != null)
        {
            GridViewRow gridrow = (GridViewRow)imgBtn.Parent.Parent;
            long Id = Convert.ToInt64(grdAggrements.DataKeys[gridrow.RowIndex].Value);
            if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Delete_EmployeeDocuments", new string[] { Id.ToString() }) > 0)
            {
                if (System.IO.File.Exists(Server.MapPath("~/Agreement") + "\\" + gridrow.Cells[0].Text))
                    System.IO.File.Delete(Server.MapPath("~/Agreement") + "\\" + gridrow.Cells[0].Text);

                BindDocuments();
            }
            gridrow = null;       
        }
        imgBtn = null;
    }
}
