using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class LoadDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       // this.Page.Title = Convert.ToString(ConfigurationSettings.AppSettings["CompanyName"]);
        this.Page.Title = GetFromXML.CompanyName;
        Session["TopHeaderText"] = "Track Load";
        if (!IsPostBack)
        {
            if (Request.QueryString.Count > 0)
            {
                ViewState["ID"] = Convert.ToInt64(Request.QueryString[0]);
                lblLoadId.Text = Convert.ToString(ViewState["ID"]);
                FillDetails(Convert.ToInt64(ViewState["ID"]));
            }
        }

    }

    private void FillDetails(long ID)
    {
        string strAssigned = null;
        long ShippingId = 0;
        //string strQuery = "select [bint_LoadStatusId] as 'Id',[nvar_LoadStatusDesc] as 'Name' from [eTn_LoadStatus]";
        //DataTable dt;// = DBClass.returnDataTable(strQuery);
        ////if (dt.Rows.Count > 0)
        //{
        //    ddlLoads.DataSource = dt;
        //    ddlLoads.DataTextField = "Name";
        //    ddlLoads.DataValueField = "Id";
        //    ddlLoads.DataBind();
        //}
        Grid1.Visible = true;
        Grid1.DeleteVisible = false;
        Grid1.EditVisible = false;
        Grid1.TableName = "eTn_Origin";
        Grid1.Primarykey = "eTn_Origin.bint_OriginId";
        Grid1.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        Grid1.ColumnsList = "eTn_Origin.bint_OriginId as 'Id';eTn_Origin.nvar_PO as 'PO';eTn_Origin.bint_Pieces as 'Pieces';eTn_Origin.num_Weight as 'Weight';" + CommonFunctions.AddressQueryString("eTn_Company.nvar_CompanyName;eTn_Location.nvar_Street;eTn_Location.nvar_Suite;eTn_Location.nvar_City;eTn_Location.nvar_State;eTn_Location.nvar_Zip", "Origin") +
            ";eTn_Origin.date_PickupDateTime as 'Date';eTn_Origin.nvar_App as 'Appt';eTn_Origin.nvar_ApptGivenby as 'ApptGivenBy'";
        Grid1.VisibleColumnsList = "Origin;Date;Appt;ApptGivenBy";
        Grid1.VisibleHeadersList = "Origin(s);Pickup Date /Time;Appt.#;Appt. Given by";
        Grid1.InnerJoinClause = " inner join eTn_Location on eTn_Origin.bint_LocationId = eTn_Location.bint_LocationId " +
               " inner join eTn_Company on eTn_Location.bint_CompanyId = eTn_Company.bint_CompanyId ";
        Grid1.WhereClause = "eTn_Origin.[bint_LoadId]=" + ID;
        Grid1.IsPagerVisible = false;
        Grid1.BindGrid(0);

        //Destination Grid
        Grid2.Visible = true;
        Grid2.DeleteVisible = false;
        Grid2.EditVisible = false;
        Grid2.TableName = "eTn_Destination";
        Grid2.Primarykey = "eTn_Destination.bint_DestinationId";
        Grid2.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        Grid2.ColumnsList = "eTn_Destination.bint_DestinationId as 'Id';eTn_Destination.nvar_PO as 'PO';eTn_Destination.bint_Pieces as 'Pieces';eTn_Destination.num_Weight as 'Weight';" + CommonFunctions.AddressQueryString("eTn_Company.nvar_CompanyName;eTn_Location.nvar_Street;eTn_Location.nvar_Suite;eTn_Location.nvar_City;eTn_Location.nvar_State;eTn_Location.nvar_Zip", "Destination") +
            ";eTn_Destination.date_DeliveryAppointmentDate as 'Date';eTn_Destination.nvar_App as 'Appt';eTn_Destination.nvar_ApptGivenby as 'ApptGivenBy'";
        Grid2.VisibleColumnsList = "Destination;Date;Appt;ApptGivenBy";
        Grid2.VisibleHeadersList = "Destination(s);Appt. Date /Time;Appt.#;Appt. Given by";
        Grid2.InnerJoinClause = " inner join eTn_Location on eTn_Destination.bint_LocationId = eTn_Location.bint_LocationId " +
               " inner join eTn_Company on eTn_Location.bint_CompanyId = eTn_Company.bint_CompanyId ";
        Grid2.WhereClause = "eTn_Destination.[bint_LoadId]=" + ID;
        Grid2.IsPagerVisible = false;
        Grid2.BindGrid(0);

        //Driver Grid
        Grid3.Visible = true;
        Grid3.DeleteVisible = false;
        Grid3.EditVisible = false;
        Grid3.TableName = "eTn_AssignDriver";
        Grid3.Primarykey = "eTn_AssignDriver.bint_AssignedId";
        Grid3.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        Grid3.ColumnsList = "eTn_AssignDriver.bint_AssignedId as 'Id';eTn_Employee.nvar_NickName as 'Name';eTn_AssignDriver.nvar_Tag as 'Tag';" + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_FromState", "From") + ";" + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_Tocity;eTn_AssignDriver.nvar_ToState", "To") + ";eTn_Payables.[num_Charges] as 'Charges'";
        Grid3.VisibleColumnsList = "Id;Name;Tag;From;To;Charges";
        Grid3.VisibleHeadersList = "Id;Name;Tag;From City State ;To City  State;Charges";
        Grid3.InnerJoinClause = " inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId " +
               " inner join eTn_Payables on eTn_Payables.bint_LoadId = eTn_AssignDriver.bint_LoadId ";
        Grid3.WhereClause = "eTn_AssignDriver.[bint_LoadId]=" + ID;
        Grid3.IsPagerVisible = false;
        Grid3.BindGrid(0);


        //grid LoadStatu Information
        Grid4.Visible = true;
        Grid4.DeleteVisible = false;
        Grid4.EditVisible = false;
        Grid4.TableName = "eTn_TrackLoad";
        Grid4.Primarykey = "eTn_TrackLoad.bint_TrackLoadId";
        Grid4.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        Grid4.ColumnsList = "eTn_LoadStatus.nvar_LoadStatusDesc as 'Status';eTn_TrackLoad.nvar_Location as 'Loc';eTn_TrackLoad.date_CreateDate as 'Date';eTn_TrackLoad.nvar_Notes as 'Notes'";
        Grid4.VisibleColumnsList = "Status;Loc;Date;Notes";
        Grid4.VisibleHeadersList = "Status;Location;Date / Time;Comments";
        Grid4.InnerJoinClause = " inner join eTn_LoadStatus on eTn_LoadStatus.bint_LoadStatusId = eTn_TrackLoad.bint_LoadStatusId ";
        Grid4.WhereClause = "eTn_TrackLoad.[bint_LoadId]=" + ID;
        Grid4.IsPagerVisible = false;
        Grid4.BindGrid(0);

        //Grid Notes
        Grid5.Visible = true;
        Grid5.DeleteVisible = false;
        Grid5.EditVisible = false;
        Grid5.TableName = "eTn_Notes";
        Grid5.Primarykey = "eTn_Notes.bint_NoteId";
        Grid5.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        Grid5.ColumnsList = "eTn_UserLogin.nvar_FirstName as 'EnterBy';eTn_Notes.date_CreateDate as 'Date';eTn_Notes.nvar_NotesDesc as 'Notes'";
        Grid5.VisibleColumnsList = "EnterBy;Date;Notes";
        Grid5.VisibleHeadersList = "Entered By ;Date / Time;Comments";
        Grid5.InnerJoinClause = " inner join eTn_UserLogin on eTn_UserLogin.bint_UserLoginId = eTn_Notes.bint_UserLoginId ";
        Grid5.WhereClause = "eTn_Notes.[bint_LoadId]=" + ID;
        Grid5.IsPagerVisible = true;        
        Grid5.PageSize=Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        Grid5.BindGrid(0);

        //Grid Contact Persons
        Grid6.Visible = true;
        Grid6.DeleteVisible = false;
        Grid6.EditVisible = false;
        Grid6.TableName = "eTn_Load";
        Grid6.Primarykey = "eTn_Load.bint_LoadId";
        Grid6.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        Grid6.ColumnsList = "eTn_CustomerContacts.nvar_Name as 'Name';" + CommonFunctions.PhoneQueryString("eTn_CustomerContacts.num_Mobile", "Mobile") + ";" + CommonFunctions.PhoneQueryString("eTn_CustomerContacts.num_WorkPhone", "Phone") + ";" + CommonFunctions.PhoneQueryString("eTn_CustomerContacts.num_Fax", "Fax") + ";eTn_CustomerContacts.nvar_Email as 'Email'";
        Grid6.VisibleColumnsList = "Name;Mobile;Phone;Fax;Email";
        Grid6.VisibleHeadersList = "Name ;Mobile;Work Phone;Fax;Email";
        Grid6.InnerJoinClause = " inner join eTn_CustomerContacts on eTn_CustomerContacts.bint_CustomerId = eTn_Load.bint_CustomerId ";
        Grid6.WhereClause = "eTn_Load.bint_LoadId=" + ID;
        Grid6.IsPagerVisible = false;
        Grid6.BindGrid(0);

        GridBar3.HeaderText = "Notes";
        //GridBar3.LinksList = "Add Note";
        //GridBar3.PagesList = "AddNotes.aspx?" + Convert.ToInt64(ID);

        //strQuery = "SELECT C.[nvar_CustomerName],L.[nvar_Pickup],L.[nvar_Container],L.[nvar_Container1],L.[nvar_Booking],CT.[nvar_CommodityTypeDesc],L.[nvar_Seal],L.[date_DateOfCreation], L.[nvar_Ref],L.[nvar_AssignTo],S.[nvar_LoadStatusDesc],L.[nvar_RailBill],convert(varchar(20),L.[date_OrderBillDate],101),L.[nvar_PersonalBill],L.[bint_ShippingId] FROM [eTn_Load] L " +
        //                "inner join [eTn_Customer] C on L.[bint_CustomerId]=C.[bint_CustomerId] inner join [eTn_CommodityType] CT on L.[bint_CommodityTypeId]=CT.[bint_CommodityTypeId]  inner join [eTn_LoadStatus] S on L.[bint_LoadStatusId]=S.[bint_LoadStatusId] " +
        //                " WHERE L.[bint_LoadId]=" + ID;
        //dt = DBClass.returnDataTable(strQuery);
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadDetailsWithLoadId", new object[] { ID });
        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"],"SP_LoadView_Log",new object[] { ID,Convert.ToString(Session["UserLoginId"]) });
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblCustomer.Text = Convert.ToString(ds.Tables[0].Rows[0][0]);
                lblPickUp.Text = Convert.ToString(ds.Tables[0].Rows[0][1]);
                if (Convert.ToString(ds.Tables[0].Rows[0][3]).Trim().Length > 0)
                    lblContainer.Text = Convert.ToString(ds.Tables[0].Rows[0][2]) + "<br/>" + Convert.ToString(ds.Tables[0].Rows[0][3]);
                else
                    lblContainer.Text = Convert.ToString(ds.Tables[0].Rows[0][2]);
                lblBooking.Text = Convert.ToString(ds.Tables[0].Rows[0][4]);
                lblCommodity.Text = Convert.ToString(ds.Tables[0].Rows[0][5]);
                lblSeal.Text = Convert.ToString(ds.Tables[0].Rows[0][6]);
                lblCreated.Text = Convert.ToString(ds.Tables[0].Rows[0][7]);
                //lblPersong.Text = Convert.ToString(ds.Tables[0].Rows[0][8]);
                lblRef.Text = Convert.ToString(ds.Tables[0].Rows[0][8]);
                strAssigned = Convert.ToString(ds.Tables[0].Rows[0][9]);
                lblStatus.Text = Convert.ToString(ds.Tables[0].Rows[0][10]);
                lblRailBill.Text = Convert.ToString(ds.Tables[0].Rows[0][11]);
                lblBillDate.Text = Convert.ToString(ds.Tables[0].Rows[0][12]);
                lblPersonBill.Text = Convert.ToString(ds.Tables[0].Rows[0][13]);
                //strEnterBy = Convert.ToString(ds.Tables[0].Rows[0][13]);
                ShippingId = Convert.ToInt64(ds.Tables[0].Rows[0][14]);
            }
            if (ds.Tables[1].Rows.Count > 0)
            {
                lblRoute.Text = Convert.ToString(ds.Tables[1].Rows[0][0]);
                lblPier.Text = Convert.ToString(ds.Tables[1].Rows[0][1]);
            }            
            if (ds.Tables[2].Rows.Count > 0)
            {
                StringBuilder tempStr = new StringBuilder();
                tempStr.Append(string.Empty);
                foreach (DataRow dr in ds.Tables[2].Rows)
                {
                    if (tempStr.ToString().Trim().Length != 0)
                        tempStr.Append("<br/>");
                    tempStr.Append(Convert.ToString(dr[0]).Trim() + "<br/>");
                }
                lblShippingContact.Text = tempStr.ToString().Trim();
            }
            if (ds.Tables[3].Rows.Count > 0)
            {
                lblPieces.Text = Convert.ToString(ds.Tables[3].Rows[0][0]);
                lblWeigth.Text = Convert.ToString(ds.Tables[3].Rows[0][1]);
            }            
            if (ds.Tables[4].Rows.Count > 0)
            {
                if (Convert.ToString(ds.Tables[4].Rows[0][0]).Trim().Length>0)
                    lblAssigned.Text = Convert.ToString(ds.Tables[4].Rows[0][0]).Trim().Substring(1,Convert.ToString(ds.Tables[4].Rows[0][0]).Trim().Length-1);
            }
            lblPersong.Text = DBClass.executeScalar("select [eTn_UserLogin].[nvar_UserId] from [eTn_UserLogin] inner join [eTn_Load] on [eTn_Load].bint_UserLoginId=[eTn_UserLogin].bint_UserLoginId where [eTn_Load].[bint_LoadId]=" + ID);
        }        
        //strQuery = "";
        //if (string.Compare(strAssigned, "Driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        //{
        //    strQuery = "select E.nvar_NickName as 'Name' from [eTn_AssignDriver] D inner join [eTn_Employee] E on D.[bint_EmployeeId]=E.[bint_EmployeeId] where D.[bint_LoadId]=" + ID;
        //    dt = DBClass.returnDataTable(strQuery);
        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        lblAssigned.Text += Convert.ToString(dt.Rows[i][0]);
        //        if (i != dt.Rows.Count - 1)
        //            lblAssigned.Text += ", ";
        //    }
        //}
        //if (string.Compare(strAssigned, "Carrier", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        //{
        //    strQuery = "select C.[nvar_CarrierName] as 'Name' from [eTn_AssignCarrier] AC inner join [eTn_Carrier] C on AC.[bint_CarrierId]=C.[bint_CarrierId] where AC.[bint_LoadId]=" + ID;
        //    lblAssigned.Text = DBClass.executeScalar(strQuery);
        //}

        //strQuery = "SELECT sum([bint_Pieces]),sum([num_Weight]) FROM [eTn_Origin] where [bint_LoadId]=" + ID;
        //dt = DBClass.returnDataTable(strQuery);
        //if (dt.Rows.Count > 0)
        //{
        //    lblPieces.Text = Convert.ToString(dt.Rows[0][0]);
        //    lblWeigth.Text = Convert.ToString(dt.Rows[0][1]);
        //}
        //dt = DBClass.returnDataTable("select L.[nvar_RailBill],L.[date_OrderBillDate],L.[nvar_PersonalBill],S.[nvar_LoadStatusDesc] from [eTn_Load] L " +
        //                             "inner join [eTn_LoadStatus] S on L.bint_LoadStatusId=S.bint_LoadStatusId WHERE L.[bint_LoadId] =" + ID);
        //if (dt.Rows.Count > 0)
        //{
        //    lblStatus.Text = Convert.ToString(dt.Rows[0][3]);
        //    lblRailBill.Text = Convert.ToString(dt.Rows[0][0]);
        //    lblBillDate.Text = Convert.ToString(dt.Rows[0][1]);
        //    lblPersonBill.Text = Convert.ToString(dt.Rows[0][2]);
        //}
        #region Document Data
        //strQuery = "select isnull(bint_DocumentId,0) as 'Id',isnull(date_ReceivedDate,getdate()) as 'Date' from eTn_UploadDocuments where bint_LoadId=" + ID + " order by bint_DocumentId asc";
        if(ds.Tables[5].Rows.Count>0)
        {
            for (int i = 0; i < ds.Tables[5].Rows.Count; i++)
            {
                switch (Convert.ToInt64(ds.Tables[5].Rows[i][0]))
                {
                    case 1:
                        lblLoadOrder.Text = Convert.ToDateTime(ds.Tables[5].Rows[i][1]).ToShortDateString();
                        break;
                    case 2:
                        lblPOrder.Text = Convert.ToDateTime(ds.Tables[5].Rows[i][1]).ToShortDateString();
                        break;
                    case 3:
                        lblBillofLading.Text = Convert.ToDateTime(ds.Tables[5].Rows[i][1]).ToShortDateString();
                        break;
                    case 4:
                        lblAccessorial.Text = Convert.ToDateTime(ds.Tables[5].Rows[i][1]).ToShortDateString();
                        break;
                    case 5:
                        lblOutGate.Text = Convert.ToDateTime(ds.Tables[5].Rows[i][1]).ToShortDateString();
                        break;
                    case 6:
                        lblInGate.Text = Convert.ToDateTime(ds.Tables[5].Rows[i][1]).ToShortDateString();
                        break;
                    case 7:
                        lblDelivery.Text = Convert.ToDateTime(ds.Tables[5].Rows[i][1]).ToShortDateString();
                        break;
                    case 8:
                        lblTicket.Text = Convert.ToDateTime(ds.Tables[5].Rows[i][1]).ToShortDateString();
                        break;
                }
            }
        }
        #endregion

        if (ds != null) { ds.Dispose(); ds = null; }

    }

    //protected void btnSearch_Click(object sender, EventArgs e)
    //{
    //    if (txtContainer.Text.Trim().Length > 0)
    //    {

    //        switch (ddlContainer.SelectedIndex)
    //        {
    //            case 0:
    //                Session["TrackWhereClause"] = "eTn_Load.[nvar_Container] like '" + txtContainer.Text.Trim() + "%' or eTn_Load.[nvar_Container1] like '" + txtContainer.Text.Trim() + "%'";
    //                break;
    //            case 1:
    //                Session["TrackWhereClause"] = "eTn_Load.[nvar_Ref] like '" + txtContainer.Text.Trim() + "%'";
    //                break;
    //            case 2:
    //                Session["TrackWhereClause"] = "eTn_Load.[nvar_Booking] like '" + txtContainer.Text.Trim() + "%'";
    //                break;
    //            case 3:
    //                Session["TrackWhereClause"] = "eTn_Load.[nvar_Chasis] like '" + txtContainer.Text.Trim() + "%' or eTn_Load.[nvar_Chasis1] like '" + txtContainer.Text.Trim() + "%'";
    //                break;
    //            case 4:
    //                try
    //                {
    //                    Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=" + Convert.ToInt64(txtContainer.Text.Trim());
    //                }
    //                catch
    //                {
    //                    Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=0";
    //                }
    //                break;
    //        }
    //    }
    //    else
    //    {
    //        Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=0";
    //    }
    //    if (Convert.ToInt32(DBClass.executeScalar("select count(bint_LoadId) from eTn_Load where " + Convert.ToString(Session["TrackWhereClause"]))) == 1)
    //    {
    //        Response.Redirect("TrackLoad.aspx?" + DBClass.executeScalar("select bint_LoadId from eTn_Load where " + Convert.ToString(Session["TrackWhereClause"])));
    //    }
    //    Response.Redirect("SearchResults.aspx");
    //}
    private void ViewDocumnets(string strFol)
    {
        if (ViewState["ID"] == null)
            return;
        strFol = strFol.Trim();
        string strPath = Server.MapPath(ConfigurationSettings.AppSettings["DocumentsFolder"]) + Convert.ToString(ViewState["ID"]).Trim() + "\\" + strFol;
        if (System.IO.Directory.Exists(strPath))
        {
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(strPath);
            System.IO.FileSystemInfo[] fi = di.GetFileSystemInfos();
            if (fi.Length > 0)
            {
                DateTime[] strTemp = new DateTime[fi.Length];
                string[] strSortFile = new string[fi.Length];
                for (int i = 0; i < fi.Length; i++)
                {
                    strTemp[i] = fi[i].CreationTime;
                }
                Array.Sort(strTemp);
                int index = 0; int flag = 0;
                for (int j = 0; j < strTemp.Length; j++)
                {
                    for (int i = 0; i < fi.Length; i++)
                    {
                        flag = 0;
                        if (fi[i].CreationTime == strTemp[j])
                        {
                            for (int k = 0; k < index; k++)
                            {
                                if (strSortFile[k] == fi[i].FullName)
                                {
                                    flag = 1; break;
                                }
                            }
                            if (flag == 0)
                            {
                                strSortFile[index] = fi[i].FullName;
                                index = index + 1;
                                break;
                            }
                            flag = 0;
                        }
                    }
                }
                if (strSortFile.Length > 0)
                {
                    string strFormat = "<table border=1 cellpadding=0 cellspacing=0 id=tbldisplay><tr><th>View Documents</th></tr>";
                    string[] strFileNames = null; string strOpenPath = "";
                    for (int i = 0; i < strSortFile.Length; i++)
                    {
                        strFileNames = strSortFile[i].Trim().Split('\\');
                        strFormat += "<tr id=" + ((i % 2 == 0) ? "row" : "altrow") + "><td align=left>";
                        strOpenPath = ConfigurationSettings.AppSettings["DocumentsFolder"] + Convert.ToString(ViewState["ID"]).Trim() + "\\" + strFol + "\\" + strFileNames[strFileNames.Length - 1];
                        strOpenPath = strOpenPath.Replace("\\\\", "\\");
                        strFormat += "<a style=font-size:12;color:blue;height:15; href='" + strOpenPath + "' target='_blank'>" + strFileNames[strFileNames.Length - 1] + "</a>";
                        strFormat += "</td></tr>";
                    }
                    strFormat += "</table>";
                    plhDocs.Controls.Add(new LiteralControl(strFormat));
                    strFileNames = null;
                }
            }
            fi = null; di = null;
        }
    }

    protected void lnkLoadOrder_Click(object sender, EventArgs e)
    {
        ViewDocumnets("1");
    }
    protected void lnlPoOrder_Click(object sender, EventArgs e)
    {
        ViewDocumnets("2");
    }
    protected void lnkLading_Click(object sender, EventArgs e)
    {
        ViewDocumnets("3");
    }
    protected void lnkAccessorial_Click(object sender, EventArgs e)
    {
        ViewDocumnets("4");
    }
    protected void lnkOutgate_Click(object sender, EventArgs e)
    {
        ViewDocumnets("5");
    }
    protected void lnkInGate_Click(object sender, EventArgs e)
    {
        ViewDocumnets("6");
    }
    protected void lnkProof_Click(object sender, EventArgs e)
    {
        ViewDocumnets("7");
    }
    protected void lnkMisc_Click(object sender, EventArgs e)
    {
        ViewDocumnets("8");
    }
}
