using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class SearchCustomer : System.Web.UI.Page
{   
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Search Customer";
        Session["BackPage4"] = null;
        Session["BackPage2"] = null;
        if (!IsPostBack)
        {
            gridCustomer.Visible = true;
            gridCustomer.DeleteVisible = true;
            gridCustomer.EditVisible = true;
            gridCustomer.TableName = "eTn_Customer";
            gridCustomer.Primarykey = "bint_CustomerId";
            gridCustomer.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
            gridCustomer.ColumnsList = "bint_CustomerId;" + CommonFunctions.AddressQueryString("nvar_CustomerName;nvar_Street;nvar_City;nvar_State", "Name") + ";" +
                CommonFunctions.PhoneQueryString("num_Phone", "Phone") + ";" + CommonFunctions.PhoneQueryString("num_Fax", "Fax") + ";" + CommonFunctions.DateQueryString("date_CreatedDate","Created");
            gridCustomer.VisibleColumnsList = "Name;Phone;Fax;Created";
            gridCustomer.VisibleHeadersList = "Name;Phone;Fax;Created";
            gridCustomer.DependentTablesList = "eTn_Load";
            gridCustomer.DependentTableKeysList = "bint_CustomerId";
            gridCustomer.DependentTableAddtionialWhereClauseList = "bint_RoleId=" + DBClass.executeScalar("select bint_RoleId from eTn_Role where Lower(nvar_RoleName)='customer'") + ";";
            gridCustomer.DeleteTablesList = "eTn_CustomerContacts;eTn_UserLogin;eTn_Customer";
            gridCustomer.DeleteTablePKList = "bint_CustomerId;bint_RolePlayerId;bint_CustomerId";
            gridCustomer.EditPage = "~/Manager/NewCustomer.aspx";
            gridCustomer.WhereClause = "bit_Status=" +ddlList.SelectedValue;
            gridCustomer.OrderByClause = "eTn_Customer.nvar_CustomerName";
            gridCustomer.FunctionName = "DeleteCustomer";
            lblShowError.Visible = false;
            gridCustomer.IsPagerVisible = true;
            gridCustomer.BindGrid(0);
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Session["GridPageIndex"] = 0;
        lblShowError.Visible = false;
        string strWhereClause = "bit_Status=" + ddlList.SelectedValue;
        if (txtName.Text.Trim().Length > 0)
        {
            strWhereClause += " and nvar_CustomerName like '" + txtName.Text.Trim() + "%'";
            int iCount = Convert.ToInt32(DBClass.executeScalar("select count(bint_CustomerId) from eTn_Customer where " + strWhereClause));
            if (iCount > 1)
            {
                gridCustomer.Visible = true;
                gridCustomer.WhereClause = strWhereClause;
                gridCustomer.BindGrid(0);
            }
            else if (iCount == 1)
            {
                Response.Redirect("~/Manager/NewCustomer.aspx?" + DBClass.executeScalar("select [bint_CustomerId] from eTn_Customer where " + strWhereClause));
            }
            else if (iCount == 0)
            {
                lblShowError.Visible = true;
                gridCustomer.Visible = false;
            }                      
        }
        else
        {
            gridCustomer.Visible = true;
            gridCustomer.WhereClause = strWhereClause;
            gridCustomer.BindGrid(0);
        }
    }   
}
