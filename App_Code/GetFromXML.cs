using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// This class is used to get application setting values
/// </summary>
public class GetFromXML
{
	public GetFromXML()
	{		
	}

    public static string SmtpServer
    {
        get {return GetValueByKey("SmtpServer"); }
        set { EditValueByKey("SmtpServer",value); }
    }

    public static string SmtpUserName
    {
        get { return GetValueByKey("SmtpUserName"); }
        set { EditValueByKey("SmtpUserName", value); }
    }

    public static string NotificationToEmail
    {
        get { return GetValueByKey("NotificationToEmail"); }
        set { EditValueByKey("NotificationToEmail", value); }
    }

    public static string AdminEmail
    {
        get { return GetValueByKey("AdminEmail"); }
        set { EditValueByKey("AdminEmail", value); }
    }

    public static string CcEmail
    {
        get { return GetValueByKey("CcEmail"); }
        set { EditValueByKey("CcEmail", value); }
    }

    public static string FromDriveronWaiting
    {
        get { return GetValueByKey("FromDriveronWaiting"); }
        set { EditValueByKey("FromDriveronWaiting", value); }
    }

    public static string FromAccessrial
    {
        get { return GetValueByKey("FromAccessrial"); }
        set { EditValueByKey("FromAccessrial", value); }
    }

    public static string CompanyName
    {
        get { return GetValueByKey("CompanyName"); }
        set { EditValueByKey("CompanyName", value); }
    }

    public static string ComapanyAddress
    {
        get { return GetValueByKey("ComapanyAddress"); }
        set { EditValueByKey("ComapanyAddress", value); }
    }

    public static string ComapanyState
    {
        get { return GetValueByKey("ComapanyState"); }
        set { EditValueByKey("ComapanyState", value); }
    }

    public static string CompanyPhone
    {
        get { return GetValueByKey("CompanyPhone"); }
        set { EditValueByKey("CompanyPhone","Phone : "+ value); }
    }

    public static string CompanyFax
    {
        get { return GetValueByKey("CompanyFax"); }
        set { EditValueByKey("CompanyFax", "Fax : " + value); }
    }

    public static string AllowedIPAddresses
    {
        get { return GetValueByKey("AllowedIPAddresses"); }
        set { EditValueByKey("AllowedIPAddresses", value); }
    }

    public static string NexmoSMSNumber
    {
        get { return GetValueByKey("NexmoSMSNumber"); }
        set { EditValueByKey("NexmoSMSNumber", value); }
    }

    public static string CCPickup
    {
        get { return GetValueByKey("CCPickup"); }
        set { EditValueByKey("CCPickup", value); }
    }

    public static string CCDriveronWaiting
    {
        get { return GetValueByKey("CCDriveronWaiting"); }
        set { EditValueByKey("CCDriveronWaiting", value); }
    }

    public static string CCAccessorial
    {
        get { return GetValueByKey("CCAccessorial"); }
        set { EditValueByKey("CCAccessorial", value); }
    }

    public static string FromLoadPlanner
    {
        get { return GetValueByKey("FromLoadPlanner"); }
        set { EditValueByKey("FromLoadPlanner", value); }
    }

    public static string PreviousYearsSelection
    {
        get { return GetValueByKey("PreviousYearsSelection"); }
        set { EditValueByKey("PreviousYearsSelection", value); }
    }

    //SLine 2016 Updates ---- Added new Properties for Street Turn and Others

    public static string StreetTurnFlagTimer
    {
        get { return GetValueByKey("StreetTurnFlagTimer"); }
        set { EditValueByKey("StreetTurnFlagTimer", value); }
    }

    public static string StreetFromAndToEMail
    {
        get { return GetValueByKey("StreetFromAndToEMail"); }
        set { EditValueByKey("StreetFromAndToEMail", value); }
    }

    public static string FromInvoiceEmailAddress
    {
        get { return GetValueByKey("FromInvoiceEmailAddress"); }
        set { EditValueByKey("FromInvoiceEmailAddress", value); }
    }

    public static string CCInvoiceEmailAddress
    {
        get { return GetValueByKey("CCInvoiceEmailAddress"); }
        set { EditValueByKey("CCInvoiceEmailAddress", value); }
    }

    //SLine 2016 Updates ---- Added new Properties for Street Turn

    public static string GetValueByKey(string strKey)
    {       
        string strValue = "";
        try
        {
            System.Xml.XmlDocument xmldoc = new System.Xml.XmlDocument();
            //System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(System.Web.HttpContext.Current.Server.MapPath("_messages.xml"));
            System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(AppDomain.CurrentDomain.BaseDirectory + "/XML/SiteSettings.xml");
            reader.WhitespaceHandling = System.Xml.WhitespaceHandling.None;
            //Load the file into the XmlDocument            
            xmldoc.Load(reader);
            reader.Close(); reader = null;
            //xmldoc.InnerXml = xmldoc.InnerXml.Trim().ToLower();
            System.Xml.XmlNode xnode = xmldoc.SelectSingleNode("//eTransport//setting[settingid='" + strKey + "']");
            if (xnode != null)
                strValue = xnode.SelectSingleNode("settingdesc").InnerText;
            xnode = null;
            xmldoc = null;
        }
        catch
        { 
        }
        return strValue;
    }

    public static void EditValueByKey(string SettingKey, string Value)
    {
        try
        {
            System.Xml.XmlDocument xmldoc = new System.Xml.XmlDocument();
            //System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(System.Web.HttpContext.Current.Server.MapPath("_messages.xml"));
            System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(AppDomain.CurrentDomain.BaseDirectory + "/XML/SiteSettings.xml");
            reader.WhitespaceHandling = System.Xml.WhitespaceHandling.None;
            //Load the file into the XmlDocument            
            xmldoc.Load(reader);
            reader.Close(); reader = null;
            //xmldoc.InnerXml = xmldoc.InnerXml.Trim().ToLower();

            System.Xml.XmlNode xnode = xmldoc.SelectSingleNode("//eTransport//setting[settingid='" + SettingKey + "']");
            if (xnode != null)
            {
                System.Xml.XmlNode childNode = xnode.SelectSingleNode("settingdesc");
                childNode.InnerText = Value;
                xnode.ReplaceChild(childNode, xnode.SelectSingleNode("settingdesc"));
                childNode = null;
            }
            xmldoc.Save(AppDomain.CurrentDomain.BaseDirectory + "/XML/SiteSettings.xml");
            xnode = null;
            xmldoc = null;
        }
        catch
        { }  
    }
}
