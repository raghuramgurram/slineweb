<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NewChassisCharge.aspx.cs" Inherits="Manager_NewChassisCharge" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>
<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td>
        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="tblform1">
            <tr>
                <td>
                    <asp:ValidationSummary ID="ReqChassisCharges" runat="server" Width="526px"/>   <asp:Label ID="lblNameError" runat="server" Font-Bold="False" ForeColor="Red" Text="Effective Date already exists." Visible="False"/>
                </td>
            </tr>
        </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblform">
          <tr>
              <td align="right" colspan="2">
                  &nbsp;</td>         
          </tr>
        <tr id="row">
          <td width="30%" align="right" style="height: 20px">Effective Date :     
         </td>
          <td style="height: 20px">
              <uc1:DatePicker ID="txtEffectiveDate" runat="server" IsDefault="true" IsRequired="true" CountNextYears="2" CountPreviousYears="0" />
              <asp:HiddenField runat="server" ID="hdnEffectiveDate" />
          </td>
        </tr>
        <tr id="altrow">
            <td width="30%" align="right" style="height: 20px">Chassis Charge :     
            </td>
            <td style="height: 20px">
              <asp:TextBox ID="txtChassisCharge" runat="Server" Width="150px"/>  
              <asp:RequiredFieldValidator ID="ReqChassisCharge" runat="server" ControlToValidate="txtChassisCharge"
                  Display="Dynamic" ErrorMessage="Please enter chassis charge.">*</asp:RequiredFieldValidator>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtChassisCharge"
                        Display="Dynamic" ErrorMessage="Please enter a valid charge." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
          <td colspan="2"><img src="../Images/pix.gif" width="1" height="5" alt=""/></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../Images/pix.gif" alt="" width="1" height="3" /></td>
        </tr>
      </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
          <tr>
            <td align="right">
                <asp:Button ID="btnSave" runat="Server" CssClass="btnstyle" Text="Save" OnClick="btnSave_Click"/>
                <asp:Button ID="btnCancel" runat="Server" CssClass="btnstyle" Text="Cancel"  CausesValidation="False" UseSubmitBehavior="False" OnClick="btnCancel_Click"/>
            </td>                
          </tr>
        </table>        
      </td>
    </tr>
</table>
</asp:Content>
