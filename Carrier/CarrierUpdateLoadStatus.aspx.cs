using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Carrier_UpdateLoadStatus : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Update Load Status";

        if (!Page.IsPostBack)
        {
            if (Request.QueryString.Count > 0 && Session["CarrierId"]!=null)
            {                
                ViewState["ID"] = Request.QueryString[0].Trim();
                FillDetails(Convert.ToInt64(Request.QueryString[0].Trim()));
                GridBar1.HeaderText = "Update Load Status ( " + Request.QueryString[0].Trim() + " )";
            }
            
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (dgrLoad.Items.Count > 0)
        {
            for (int i = 0; i < dgrLoad.Items.Count; i++)
            {
                if ((i % 2) == 0)
                {
                    dgrLoad.Items[i].BackColor = System.Drawing.Color.Red;
                }
                else
                {
                    dgrLoad.Items[i].BackColor = System.Drawing.Color.Blue;
                }
            }
        }        
    }

    private void FillDetails(long loadId)
    {
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "sp_Carrier_GetAllStatusInfoByLoadId", new object[] { loadId,Convert.ToInt64(Session["CarrierId"]) });
        if (ds.Tables.Count > 0)
        {
            #region Dummy Row Addition
            DataRow dr = ds.Tables[0].NewRow();
            for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
            {
                if (ds.Tables[0].Columns[i].DataType == typeof(string))
                {
                    dr[i] = "test";
                }
                else if (ds.Tables[0].Columns[i].DataType == typeof(System.Int64))
                {
                    dr[i] = 0;
                }
                else if (ds.Tables[0].Columns[i].DataType == typeof(DateTime))
                {
                    dr[i] = DateTime.Now;
                }
            }
            ds.Tables[0].Rows.InsertAt(dr, 0);
            ds.Tables[0].AcceptChanges();
            dr = null;
            #endregion
            if (ds.Tables[0].Rows.Count > 0)
            {
                dgrLoad.DataSource = ds.Tables[0].Copy();
                dgrLoad.DataBind();                
            }
        }
        if (ds != null) { ds.Dispose(); ds = null; }
        #region Formatting Grid
        if (dgrLoad.Items.Count > 0)
        {
            for (int i = 0; i < dgrLoad.Items.Count; i++)
            {               
                foreach (Control ctl in dgrLoad.Items[i].Cells[0].Controls)
                {
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.DropDownList")
                    {
                        ctl.Visible = (i == 0) ? true : false;
                    }
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.Label")
                    {
                        ctl.Visible = (i == 0) ? false : true;
                    }
                }
                foreach (Control ctl in dgrLoad.Items[i].Cells[1].Controls)
                {
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.TextBox")
                    {
                        ctl.Visible = (i == 0) ? true : false;
                    }
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.Label")
                    {
                        ctl.Visible = (i == 0) ? false : true;
                    }
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.RequiredFieldValidator")
                    {
                        ctl.Visible = (i == 0) ? true : false;
                    }  
                }
                foreach (Control ctl in dgrLoad.Items[i].Cells[2].Controls)
                {
                    if (ctl.GetType().ToString() == "ASP.usercontrols_datepicker_ascx")
                    {
                        ctl.Visible = (i == 0) ? true : false;
                    }
                    if (ctl.GetType().ToString() == "ASP.usercontrols_timepicker_ascx")
                    {
                        ctl.Visible = (i == 0) ? true : false;
                    }
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.Label")
                    {
                        ctl.Visible = (i == 0) ? false : true;
                    }
                }                    

                foreach (Control ctl in dgrLoad.Items[i].Cells[3].Controls)
                {
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.TextBox")
                    {
                        ctl.Visible = (i == 0) ? true : false;
                    }
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.Label")
                    {
                        ctl.Visible = (i == 0) ? false : true;
                    }
                }
                foreach (Control ctl in dgrLoad.Items[i].Cells[4].Controls)
                {
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.Button")
                    {
                        ctl.Visible = (i == 0) ? true : false;
                        //Button btn = (Button)ctl;
                        //btn.CausesValidation = (i == 0) ? true : false;
                        //btn = null;
                    }
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.ImageButton")
                    {
                        ctl.Visible = (i == 0) ? false : true;
                        //ImageButton imgbtn = (ImageButton)ctl;
                        //imgbtn.CausesValidation = (i == 0) ? false : false;
                        //imgbtn = null;
                    }
                }                
            }
        }
        #endregion
        #region Format Items of LoadStatus.
        string strQuery = "SELECT [bint_LoadStatusId] FROM [eTn_TrackLoad] where [bint_LoadId]="+loadId;
        DataTable dt=DBClass.returnDataTable(strQuery);
        if (dt.Rows.Count > 0)
        {
            DropDownList dname = (DropDownList)dgrLoad.Items[0].Cells[0].FindControl("dropstatus");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dname.Items.Contains(dname.Items.FindByValue(Convert.ToString(dt.Rows[i][0]))))
                {
                    if (Convert.ToInt64(dt.Rows[i][0]) == 5)
                        continue;
                    dname.Items.Remove(dname.Items.FindByValue(Convert.ToString(dt.Rows[i][0])));
                }
            }
        }
        #endregion
    }

    protected void dgrLoad_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (Session["CarrierId"] != null)
        {
            if (string.Compare(e.CommandName, "ADD", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                //TextBox Cdate = (TextBox)e.Item.FindControl("txtDate");
                //try
                //{
                //    DateTime dat = Convert.ToDateTime(Cdate.Text.Trim());
                //}
                //catch
                //{
                //    Response.Write("<script>alert('Invalid Date & Time.')</script>");
                //    return;
                //}
                if (Page.IsPostBack)
                {
                    DatePicker Dpicker = (DatePicker)e.Item.FindControl("txtDate");

                    if (!Dpicker.IsValidDate)
                        return;
                    if (dgrLoad.Items.Count > 1)
                    {
                        Label lbl1 = (Label)dgrLoad.Items[1].FindControl("lblID");
                        if (Convert.ToDateTime(DBClass.executeScalar("Select top 1 date_CreateDate from eTn_TrackLoad where bint_LoadId = " +
                            "(select bint_LoadId from eTn_TrackLoad where bint_TrackLoadId = " + lbl1.Text.Trim() + ") order by date_CreateDate desc ")) >= Convert.ToDateTime(Dpicker.Date))
                        {
                            Response.Write("<script>alert('Date entered must be greater than the date of previous status dates.')</script>");
                            return;
                        }
                    }
                    DropDownList dname = (DropDownList)e.Item.FindControl("dropstatus");
                    TextBox Cloc = (TextBox)e.Item.FindControl("txtLoc");
                    TextBox Cnotes = (TextBox)e.Item.FindControl("txtComments");
                    TimePicker Tpicker = (TimePicker)e.Item.FindControl("txtTime");


                    if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_Carrier_Add_Update_TrackLoad", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(dname.SelectedItem.Value),
                        Cloc.Text.Trim(),CommonFunctions.CheckDateTimeNull((Dpicker.Date !=null)?Dpicker.Date+" "+Tpicker.Time.Trim():null),Cnotes.Text.Trim(),Convert.ToInt64(Session["CarrierId"]),"I"}) > 0)
                    {
                        ChangeLoadStatus cls = new ChangeLoadStatus();
                        cls.UpdateLoadStatusToPostman(Convert.ToInt64(ViewState["ID"]), dname.SelectedItem.Text, (DateTime)CommonFunctions.CheckDateTimeNull((Dpicker.Date != null) ? Dpicker.Date + " " + Tpicker.Time.Trim() : null));
                        ////FillDetails(Convert.ToInt64(ViewState["ID"]));
                    }
                    Response.Redirect("~/Carrier/CarrierTrackLoad.aspx?" + Convert.ToString(ViewState["ID"]));
                }
            }
            if (string.Compare(e.CommandName, "Delete", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                if (Page.IsPostBack)
                {
                    //if (System.Windows.Forms.MessageBox.Show("Do you want to delete this record?", "Message", System.Windows.Forms.MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    //{
                    Label lbl = (Label)e.Item.FindControl("lblID");
                    if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_Delete_TrackLoad", new object[] { lbl.Text.Trim(), Convert.ToInt64(Session["UserLoginId"]) }) > 0)
                    {
                        FillDetails(Convert.ToInt64(ViewState["ID"]));
                    }
                }
                //}
            }
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Carrier/CarrierTrackLoad.aspx?" + Convert.ToString(ViewState["ID"]));
    }
}
