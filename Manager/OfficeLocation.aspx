﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OfficeLocation.aspx.cs" Inherits="OfficeLocation"
    MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/Grid.ascx" TagName="Grid" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="ContentTbl">
        <tr valign="top">
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td align="left" width="100%">
                            <uc2:GridBar ID="barCurrent" runat="server" HeaderText="Office Location" LinksList="Add New Office Location"
                                PagesList="NewOfficeLocation.aspx" Visible="true" />
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img src="../Images/pix.gif" alt="" width="1" height="5" />
                        </td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <uc3:Grid ID="gridCurrent" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
