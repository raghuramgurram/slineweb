<%@ Page AutoEventWireup="true" CodeFile="~/Carrier/CarrierLoadDetails.aspx.cs" Inherits="CarrierLoadDetails"
    Language="C#" Title="S Line Transport Inc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>S Line Transport Inc</title>
    <link href="../Styles/StyleNoBody.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <table id="ContentTbl" align="center" border="0" cellpadding="0" cellspacing="0"
            width="700">
            <tr valign="top">
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td bgcolor="#CCCCCC">
                                <img alt="" height="1" src="../images/pix.gif" width="1" /></td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <img alt="" height="10" src="../images/pix.gif" width="1" /></td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td width="15%">
                                <img alt="" src="../Images/logo.gif" style="width: 200px; height: 80px" />
                            </td>
                            <td align="center" class="head1" width="85%">
                                Job Order#
                                <asp:Label ID="lblLoad" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <img alt="" height="10" src="../Images/pix.gif" width="1" /></td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="background-color: Black;">
                                <img alt="" height="2" src="../images/pix.gif" width="1" /></td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <img alt="" height="20" src="../images/pix.gif" width="1" /></td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td valign="top" width="50%">
                                <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr id="row">
                                        <td valign="top" width="50%">
                                            <strong>
                                                <asp:Label ID="lblAssigned" runat="server" Font-Underline="True"></asp:Label></strong><br />
                                            <asp:Label ID="lblDriverOrCarrier" runat="server"></asp:Label>
                                            <br /><br />
                                            <asp:Label ID="lblTotal" runat="server"></asp:Label></td>
                                        <td style="width: 433px" valign="top">
                                            <strong>&nbsp;<asp:Label ID="Label1" runat="server" Font-Underline="True" Text="Load Number:"></asp:Label>&nbsp;<asp:Label
                                                ID="lblLoadId" runat="server"></asp:Label></strong><br />
                                            PO#:&nbsp;<asp:Label ID="lblPO" runat="server"></asp:Label><br />
                                            Type:&nbsp;<asp:Label ID="lblType" runat="server"></asp:Label><br />
                                            Commodity:&nbsp;<asp:Label ID="lblCommodity" runat="server"></asp:Label><br />
                                            Booking#:&nbsp;<asp:Label ID="lblBooking" runat="server"></asp:Label><br />
                                            Pickup#:&nbsp;<asp:Label ID="lblPickup" runat="server"></asp:Label><br />
                                            Container ID:&nbsp;<asp:Label ID="lblContainerId" runat="server"></asp:Label><br />
                                            Empty/Chasis#: :&nbsp;<asp:Label ID="lblChasis" runat="server"></asp:Label><br />
                                            Remarks:&nbsp;<asp:Label ID="lblRemarks" runat="server"></asp:Label><br />
                                            Description:&nbsp;<asp:Label ID="lblDescription" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr id="altrow">
                                        <td colspan="2" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <img alt="" height="5" src="../images/pix.gif" width="1" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr >
                                        <td colspan="2">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td style="background-color: Black;">
                                                        <img alt="" height="2" src="../images/pix.gif" width="1" /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <img alt="" height="2" src="../images/pix.gif" width="1" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="row">
                                        <td valign="top">
                                            <strong>&nbsp;<asp:Label ID="Label2" runat="server" Font-Underline="True" Text="Origin:"></asp:Label></strong>:<br />
                                            <asp:Label ID="lblOrigin" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 433px" valign="top">
                                            <strong>
                                                <asp:Label ID="Label3" runat="server" Font-Underline="True" Text="Destination:"></asp:Label></strong><br />
                                            <asp:Label ID="lblDestination" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <img alt="" height="5" src="../images/pix.gif" width="1" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td style="background-color: Black;">
                                                        <img alt="" height="2" src="../images/pix.gif" width="1" /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <img alt="" height="2" src="../images/pix.gif" width="1" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="altrow">
                                        <td valign="top">
                                            <strong>&nbsp;</strong>:<br />
                                            &nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:Label ID="Label5" runat="server" Font-Underline="True" Text="Consent:"></asp:Label>
                                            <br />
                                            * Please fax rate confirmation back asap<br />
                                            * Re-brokering, assigning or interlining of this shipment without prior written
                                            consent will void our obligation to pay your frieght bill * Pallet charges, Lumper
                                            charges, and any type of fuel surcharge are included unless otherwise noted.
                                            <br />
                                            * Any changes must be confirmed by S Line Transportation, Inc.with a replacement
                                            load request sheet.
                                            <br />
                                            * For temperature sensitive products, carrier will be responsible for maintaining
                                            the temperature requirements from the shipper and follow their instructions.
                                            <br />
                                            * All invoices must include a signed rate confirmation sheet, assigned load number
                                            and a signed delivery receipt.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <img alt="" height="5" src="../images/pix.gif" width="1" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td bgcolor="#CCCCCC">
                                                        <img alt="" height="1" src="../images/pix.gif" width="1" /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="row">
                                        <td>
                                            <asp:Label ID="lblAddress" runat="server" Text="Label"></asp:Label>
                                            <br />
                                            <br />
                                            Signature: __________________________________________
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblCarrier" runat="server"></asp:Label></b>
                                            <br />
                                            <br />
                                            Signature: __________________________________________
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <img alt="" height="5" src="../images/pix.gif" width="1" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="altrow">
                                        <td>
                                            Print Name:
                                            <br />
                                            Date:
                                        </td>
                                        <td>
                                            Print Name:<br />
                                            Date:
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="right" valign="middle">
                                <asp:Button ID="btnBack" runat="server" CausesValidation="false" CssClass="btnstyle"
                                    OnClick="btnBack_Click" Text="Back" Visible="true" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
