<%@ Control AutoEventWireup="true" CodeFile="PaymentsGrid.ascx.cs" Inherits="PaymentsGrid"
    Language="C#" %>
<link type="text/css" href="../Styles/OwnStyle.css" rel="stylesheet" />

<%--<table cellpadding="0" cellspacing="0" border="0" width="100%" id="tbldisplay">
<tr>
    <td>
        <asp:DataGrid ID="dggrid" Width="100%" runat="server" OnItemCommand="dggrid_ItemCommand" OnPageIndexChanged="dggrid_PageIndexChanged" PageSize="2" CssClass="Grid">
            <PagerStyle Mode="NumericPages" />
            <ItemStyle CssClass="redtext" BorderColor="White" />
            <HeaderStyle CssClass="GridHeader" BorderColor="White" />
            <AlternatingItemStyle CssClass="grntext" />
        </asp:DataGrid>
    </td>
</tr>
</table>--%>
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding-right:0px;padding-right:0px;">
<tr>
    <td>
        <asp:DataGrid ID="dggrid" Width="100%" runat="server" OnItemCommand="dggrid_ItemCommand" OnPageIndexChanged="dggrid_PageIndexChanged" PageSize="2" CssClass="Grid">
            <PagerStyle Mode="NumericPages" />
            <ItemStyle BorderColor="Black" BorderWidth="1px"/>
            <HeaderStyle CssClass="GridHeader" BorderColor="White" />
        </asp:DataGrid>
    </td>
</tr>
</table>

