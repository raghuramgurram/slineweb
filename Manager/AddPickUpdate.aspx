<%@ Page AutoEventWireup="true" CodeFile="AddPickUpdate.aspx.cs" Inherits="AddPickUpdate"
    Language="C#" Title="S Line Transport Inc" %>

<%@ Register Src="../UserControls/TimePicker.ascx" TagName="TimePicker" TagPrefix="uc2" %>

<%@ Register Src="../UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>S Line Transport Inc</title>
    <meta content="revealTrans(Duration=0.5)" http-equiv="Page-Enter" />
    <meta content="revealTrans(Duration=0.5)" http-equiv="Page-Exit" />
    <link href="../Styles/StyleNoBody.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">        
        <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                </td>
            </tr>
        </table>
        <table id="tblform" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <th colspan="2">
                    Pick Up Details
                </th>
            </tr>
            <tr id="row">
                <td align="right" width="30%">
                    Load Number :
                </td>
                <td>
                    <asp:Label ID="lblLoadId" runat="server" /></td>
            </tr>
             <tr id="altrow">
                <td align="right" width="30%">
                    Container# :
                </td>
                <td>
                    <asp:Label ID="lblContainer" runat="server"></asp:Label></td>
            </tr>
            <tr id="altrow">
                <td align="right">
                    Origin :
                </td>
                <td>
                    <asp:Label ID="lblOrigin" runat="server"></asp:Label></td>
            </tr>
            <tr id="row">
                <td align="right">
                    Logged in User :
                </td>
                <td>
                    <asp:Label ID="lblUserId" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="altrow">
                <td align="right" style="height: 20px">
                    PickUp Date :
                </td>
                <td style="height: 20px">
                   <uc1:DatePicker ID="dtpPickUp" runat="server" IsDefault="true" IsRequired="true" CountNextYears="2" CountPreviousYears="0" SetInitialDate="false" />
                   From Time : 
                    <uc2:TimePicker ID="TimePicker1" runat="server" ReqFieldValidation="true" Visible="true" />
                   To Time : 
                    <uc2:TimePicker ID="TimePicker2" runat="server" ReqFieldValidation="false" Visible="true" />
                </td>
            </tr>
            <tr id="row">
                <td align="right">
                    Appt.# :
                </td>
                <td>
                    <asp:TextBox ID="txtAppt" runat="server" />   
                 </td>
            </tr>
            <tr id="altrow">
                <td align="right">
                    Appt Given By :
                </td>
                <td>
                    <asp:TextBox ID="txtApptGivenBy" runat="server" />   
                 </td>
            </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="height: 5px">
                    <img alt="" height="8" src="../Images/pix.gif" width="1" /></td>
            </tr>
        </table>
        <table id="tblBottomBar" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="right">
                    <asp:Button ID="btnSave" CssClass="btnstyle" Text="Save" runat="server" OnClick="btnSave_Click"/>
                    <asp:Button ID="btnClose" runat="server" CssClass="btnstyle" Text="Cancel" CausesValidation="False" OnClick="btnClose_Click" UseSubmitBehavior="False" />
                 </td>
            </tr>
        </table>
    </form>
</body>
</html>
