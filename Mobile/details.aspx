<%@ Page Language="C#" MasterPageFile="~/Mobile/eTransportMobile.master" AutoEventWireup="true" CodeFile="details.aspx.cs" Inherits="Mobile_details" Title="Untitled Page" %>
<%@ Register Src="MobileMenu.ascx" TagName="Menu"
    TagPrefix="mu" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<mu:Menu ID="MuPageMenu" runat="server" />


       <div class="main-details status ">
        <p>Current Status:  <asp:Literal ID="ltlLoadstatus" runat="server"></asp:Literal></p>
         <p class="orange">Load details:</p>
          <div class="address-details">
            <div class="lft-details">
              <ul>
                <li> Container# : </li>
                <li> Booking# :</li>
                <li> Pickup# :</li>
                <li> Container Location :</li>
                <li>Chassis#: </li>
              </ul>
            </div>
            <div class="right-details">
              <ul>
                <li><asp:Literal ID="ltlContainer" runat="server"></asp:Literal></li>
                <li><asp:Literal ID="ltlBooking" runat="server"></asp:Literal></li>
                <li><asp:Literal ID="ltlPickup" runat="server"></asp:Literal></li>
                <li><asp:Literal ID="ltlcontainerLocation" runat="server"></asp:Literal></li>
                <li><asp:Literal ID="ltlChassiss" runat="server"></asp:Literal>
                <asp:TextBox ID="txtchassisnumber" runat="server" style="display:none; width:20%"></asp:TextBox> 
                <asp:HiddenField ID="hfilChissisnumber" runat="server" />
                 <asp:LinkButton ID="lnkaddorUpdatechassisnumber" runat="server" Text="Update" CssClass="f12" OnClick="lnkaddorUpdatechassisnumber_click" style="display:inline;"></asp:LinkButton>
                 <span id="spnchassissupprater" class="f12" runat="server" style="display:none;">|</span>
                 <asp:LinkButton ID="lnkCancelupdatechassisnumber" runat="server" Text ="Cancel" CssClass="f12" OnClick="lnkCancelupdatechassisnumber_click" runat="server" style="display:none;"></asp:LinkButton>
                 </li>
              </ul>
            </div>
          </div>
          <asp:Repeater ID="rpeaddressfromdetails" OnItemDataBound="rpeaddressfromdetails_databound" runat="server">
          <HeaderTemplate>
          </HeaderTemplate>
          <ItemTemplate>
        <div class="orange block">From:</div>
        <div class="address-details">
          <ul>
            <li><%# Eval("nvar_CustomerName").ToString()%></li>
            <li><asp:Literal ID="ltlorignaddress" runat="server"></asp:Literal>
            <asp:HiddenField ID="hfieldaddress" runat="server" Value='<%# Eval("nvar_From").ToString()%>' />
            <asp:HiddenField ID="hfieldcity" runat="server" Value='<%# Eval("nvar_FromCity").ToString()%>' />
             <asp:HiddenField ID="hfieldstate" runat="server" Value='<%# Eval("nvar_FromState").ToString()%>' />
            </li>
            <li>TEL :<asp:Literal ID="ltlorignphone" runat="server" Text='<%# Eval("nvar_FromPO").ToString()%>'></asp:Literal></li>
          </ul>
          <div class="lft-details">
            <ul>
              <li> Appt. Date/Time:</li>
              <li> Appt.#:</li>
              <li> Appt.Given By:</li>
            </ul>
          </div>
          <div class="rgt-details">
            <ul>
              <li> <asp:Literal ID="ltlorigndatetime" runat="server" Text='<%# Eval("date_PickupDateTime").ToString()%>'></asp:Literal></li>
              <li> <asp:Literal ID="ltlorignappt" runat="server" Text='<%# Eval("nvar_FromApp").ToString()%>'></asp:Literal></li>
              <li><asp:Literal ID="ltlorigngivenby" runat="server" Text='<%# Eval("nvar_FromGivenby").ToString()%>'></asp:Literal></li>
            </ul>
          </div>
           </div>
           </ItemTemplate>
          <FooterTemplate>
          </FooterTemplate>
          </asp:Repeater>  
          <asp:Repeater ID="rpeaddresstodetails" OnItemDataBound="rpeaddresstodetails_databound" runat="server">
          <HeaderTemplate>
          </HeaderTemplate>
          <ItemTemplate>
          <p class="orange">To:</p>
          <div class="address-details">
            <ul>
              <li> <%# Eval("nvar_CustomerName").ToString()%></li>
              <li><asp:Literal ID="ltltoaddress" runat="server"></asp:Literal>
               <asp:HiddenField ID="hfieldaddress" runat="server" Value='<%# Eval("nvar_To").ToString()%>' />
            <asp:HiddenField ID="hfieldcity" runat="server" Value='<%# Eval("nvar_Tocity").ToString()%>' />
             <asp:HiddenField ID="hfieldstate" runat="server" Value='<%# Eval("nvar_Tostate").ToString()%>' />
              </li>
              <li>TEL :<asp:Literal ID="ltltophone" runat="server" Text='<%# Eval("nvar_toPO").ToString()%>'></asp:Literal></li>
            </ul>
            <div class="lft-details">
              <ul>
                <li> Appt. Date/Time:</li>
                <li> Appt.#:</li>
                <li> Appt.Given By:</li>
              </ul>
            </div>
            <div class="rgt-details">
              <ul>
                <li> <asp:Literal ID="ltltodatetime" runat="server" Text='<%# Eval("date_DeliveryAppointmentDate").ToString()%>'></asp:Literal></li>
                <li> <asp:Literal ID="ltltoappt" runat="server" Text='<%# Eval("nvar_ToApp").ToString()%>'></asp:Literal></li>
                <li><asp:Literal ID="ltltogivenby" runat="server" Text='<%# Eval("nvar_ToGivenby").ToString()%>'></asp:Literal></li>
              </ul>
            </div>
          </div>     
           </ItemTemplate>
          <FooterTemplate>
          </FooterTemplate>
          </asp:Repeater>  
      </div>
        <asp:LinkButton ID="lnkgoback" runat="server" data-role="button" data-inline="true" data-theme="b" 
        data-icon="arrow-l" data-iconpos="left" class="backbtn fl-right" rel="external" OnClick="lnkgoback_click"> Back </asp:LinkButton>
</asp:Content>

