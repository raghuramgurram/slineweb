using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class CustomerLoadDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Customer Load Details";
        if (!IsPostBack)
        {
            if (Request.QueryString.Count > 0)
            {
                try
                {
                    string[] strParams = Convert.ToString(Request.QueryString[0]).Trim().Split('^');
                    ViewState["ID"] = Convert.ToInt64(strParams[0]);
                    FillDetails(Convert.ToInt64(ViewState["ID"]));
                }
                catch
                {
                }
            }
        }
    }
    private void FillDetails(long ID)
    {
        string strAssigned = null, strEnterBy = null;
        long ShippingId = 0;
        string strQuery = "";
        DataTable dt;

        lblLoadId.Text = Convert.ToString(ID);

        Grid1.Visible = true;
        Grid1.DeleteVisible = false;
        Grid1.EditVisible = false;
        Grid1.TableName = "eTn_Origin";
        Grid1.Primarykey = "eTn_Origin.bint_OriginId";
        Grid1.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        Grid1.ColumnsList = "eTn_Origin.bint_OriginId as 'Id';eTn_Origin.nvar_PO as 'PO';eTn_Origin.bint_Pieces as 'Pieces';eTn_Origin.num_Weight as 'Weight';" + CommonFunctions.AddressQueryString("eTn_Company.nvar_CompanyName;eTn_Location.nvar_Street;eTn_Location.nvar_Suite;eTn_Location.nvar_City;eTn_Location.nvar_State;eTn_Location.nvar_Zip", "Origin") +
            ";eTn_Origin.date_PickupDateTime as 'Date';eTn_Origin.nvar_App as 'Appt';eTn_Origin.nvar_ApptGivenby as 'ApptGivenBy'";
        Grid1.VisibleColumnsList = "Origin;Date;Appt;ApptGivenBy";
        Grid1.VisibleHeadersList = "Origin(s);Pickup Date /Time;Appt.#;Appt. Given by";
        Grid1.InnerJoinClause = " inner join eTn_Location on eTn_Origin.bint_LocationId = eTn_Location.bint_LocationId " +
               " inner join eTn_Company on eTn_Location.bint_CompanyId = eTn_Company.bint_CompanyId ";
        Grid1.WhereClause = "eTn_Origin.[bint_LoadId]=" + ID;
        Grid1.IsPagerVisible = false;
        Grid1.BindGrid(0);

        //Destination Grid
        Grid2.Visible = true;
        Grid2.DeleteVisible = false;
        Grid2.EditVisible = false;
        Grid2.TableName = "eTn_Destination";
        Grid2.Primarykey = "eTn_Destination.bint_DestinationId";
        Grid2.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        Grid2.ColumnsList = "eTn_Destination.bint_DestinationId as 'Id';eTn_Destination.nvar_PO as 'PO';eTn_Destination.bint_Pieces as 'Pieces';eTn_Destination.num_Weight as 'Weight';" + CommonFunctions.AddressQueryString("eTn_Company.nvar_CompanyName;eTn_Location.nvar_Street;eTn_Location.nvar_Suite;eTn_Location.nvar_City;eTn_Location.nvar_State;eTn_Location.nvar_Zip", "Destination") +
            ";eTn_Destination.date_DeliveryAppointmentDate as 'Date';eTn_Destination.nvar_App as 'Appt';eTn_Destination.nvar_ApptGivenby as 'ApptGivenBy'";
        Grid2.VisibleColumnsList = "Destination;Date;Appt;ApptGivenBy";
        Grid2.VisibleHeadersList = "Destination(s);Appt. Date /Time;Appt.#;Appt. Given by";
        Grid2.InnerJoinClause = " inner join eTn_Location on eTn_Destination.bint_LocationId = eTn_Location.bint_LocationId " +
               " inner join eTn_Company on eTn_Location.bint_CompanyId = eTn_Company.bint_CompanyId ";
        Grid2.WhereClause = "eTn_Destination.[bint_LoadId]=" + ID;
        Grid2.IsPagerVisible = false;
        Grid2.BindGrid(0);

        //grid LoadStatu Information
        Grid4.Visible = true;
        Grid4.DeleteVisible = false;
        Grid4.EditVisible = false;
        Grid4.TableName = "eTn_TrackLoad";
        Grid4.Primarykey = "eTn_TrackLoad.bint_TrackLoadId";
        Grid4.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        Grid4.ColumnsList = "eTn_LoadStatus.nvar_LoadStatusDesc as 'Status';eTn_TrackLoad.nvar_Location as 'Loc';eTn_TrackLoad.date_CreateDate as 'Date';eTn_TrackLoad.nvar_Notes as 'Notes'";
        Grid4.VisibleColumnsList = "Status;Loc;Date;Notes";
        Grid4.VisibleHeadersList = "Status;Location;Date / Time;Comments";
        Grid4.InnerJoinClause = " inner join eTn_LoadStatus on eTn_LoadStatus.bint_LoadStatusId = eTn_TrackLoad.bint_LoadStatusId ";
        Grid4.WhereClause = "eTn_TrackLoad.[bint_LoadId]=" + ID;
        Grid4.IsPagerVisible = false;
        Grid4.BindGrid(0);

        //Grid Notes
        if (Session["UserLoginId"] != null)
        {
            Grid5.Visible = true;
            Grid5.DeleteVisible = false;
            Grid5.EditVisible = false;
            Grid5.IsPagerVisible = true;
            Grid5.TableName = "eTn_Notes";
            Grid5.Primarykey = "eTn_Notes.bint_NoteId";
            Grid5.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
            Grid5.ColumnsList = "eTn_UserLogin.nvar_FirstName as 'EnterBy';eTn_Notes.date_CreateDate as 'Date';eTn_Notes.nvar_NotesDesc as 'Notes'";
            Grid5.VisibleColumnsList = "EnterBy;Date;Notes";
            Grid5.VisibleHeadersList = "Entered By ;Date / Time;Comments";
            Grid5.InnerJoinClause = " inner join eTn_UserLogin on eTn_UserLogin.bint_UserLoginId = eTn_Notes.bint_UserLoginId ";
            Grid5.WhereClause = "eTn_Notes.[bint_LoadId]=" + ID + " and eTn_Notes.[bint_UserLoginId]=" + Convert.ToInt64(Session["UserLoginId"]);
            Grid5.BindGrid(0);
        }

        if (Session["CustomerId"] != null)
        {
            strQuery = "SELECT C.[nvar_CustomerName],L.[nvar_Pickup],L.[nvar_Container],L.[nvar_Container1],L.[nvar_Booking],CT.[nvar_CommodityTypeDesc],L.[nvar_Seal],L.[date_DateOfCreation], L.[nvar_Ref],L.[nvar_AssignTo],S.[nvar_LoadStatusDesc],L.[nvar_RailBill],convert(varchar(20),L.[date_OrderBillDate],101),L.[nvar_PersonalBill],L.[bint_ShippingId] FROM [eTn_Load] L " +
                            "inner join [eTn_Customer] C on L.[bint_CustomerId]=C.[bint_CustomerId] inner join [eTn_CommodityType] CT on L.[bint_CommodityTypeId]=CT.[bint_CommodityTypeId]  inner join [eTn_LoadStatus] S on L.[bint_LoadStatusId]=S.[bint_LoadStatusId] " +
                            " WHERE L.[bint_LoadId]=" + ID + " and L.[bint_CustomerId]=" + Convert.ToInt64(Session["CustomerId"]);
            dt = DBClass.returnDataTable(strQuery);
            if (dt.Rows.Count > 0)
            {
                lblCustomer.Text = Convert.ToString(dt.Rows[0][0]);
                lblPickUp.Text = Convert.ToString(dt.Rows[0][1]);
                if (Convert.ToString(dt.Rows[0][3]).Trim().Length > 0)
                    lblContainer.Text = Convert.ToString(dt.Rows[0][2]) + "<br/>" + Convert.ToString(dt.Rows[0][3]);
                else
                    lblContainer.Text = Convert.ToString(dt.Rows[0][2]);
                lblCommodity.Text = Convert.ToString(dt.Rows[0][5]);
                lblSeal.Text = Convert.ToString(dt.Rows[0][6]);
                lblCreated.Text = Convert.ToString(dt.Rows[0][7]);
                //lblPersong.Text = Convert.ToString(dt.Rows[0][8]);
                lblRef.Text = Convert.ToString(dt.Rows[0][8]);
                strAssigned = Convert.ToString(dt.Rows[0][9]);
                lblStatus.Text = Convert.ToString(dt.Rows[0][10]);
                lblRailBill.Text = Convert.ToString(dt.Rows[0][11]);
                lblBillDate.Text = Convert.ToString(dt.Rows[0][12]);
                //lblPersonBill.Text = Convert.ToString(dt.Rows[0][14]);
                strEnterBy = Convert.ToString(dt.Rows[0][13]);
                ShippingId = Convert.ToInt64(dt.Rows[0][14]);
            }
        }

        strQuery = "select '<b>'+[nvar_ShippingLineName]+'</b> <br/>'+[nvar_Street]+'&nbsp;'+ [nvar_Suite]+'<br/>'+[nvar_City]+','+[nvar_State] +'&nbsp;'+ [nvar_Zip]+'<br/><a href='+ [nvar_WebsiteURL]+'>'+[nvar_WebsiteURL]+'</a>',[nvar_PierTermination] from [eTn_Shipping] where [bint_ShippingId]=" + ShippingId;
        dt = DBClass.returnDataTable(strQuery);
        if (dt.Rows.Count > 0)
        {
            lblRoute.Text = Convert.ToString(dt.Rows[0][0]);
            lblPier.Text = Convert.ToString(dt.Rows[0][1]);
            strQuery = "select [nvar_Name]+'<br/>'+case when len(cast([num_WorkPhone]  as varchar(10)))=10 then substring(cast([num_WorkPhone]  as varchar(10)),1,3)+'-'+" +
                      "substring(cast([num_WorkPhone]  as varchar(10)),4,3)+'-'+substring(cast([num_WorkPhone]  as varchar(10)),7,4) else '' end +'<br/>'+case when len(cast([num_Mobile]  as varchar(10)))=10 then substring(cast([num_Mobile]  as varchar(10)),1,3)+'-'+" +
                      "substring(cast([num_Mobile]  as varchar(10)),4,3)+'-'+substring(cast([num_Mobile]  as varchar(10)),7,4) else '' end" +
                      " from [eTn_ShippingContacts] where bint_ShippingId=" + ShippingId;
            DataTable tempDt= DBClass.returnDataTable(strQuery);
            if (tempDt.Rows.Count > 0)
            {
                StringBuilder tempStr = new StringBuilder();
                tempStr.Append(string.Empty);
                foreach (DataRow dr in tempDt.Rows)
                {
                    if (tempStr.ToString().Trim().Length != 0)
                        tempStr.Append("<br/>");
                    tempStr.Append(Convert.ToString(dr[0]).Trim() + "<br/>");
                }
                lblShippingContact.Text = tempStr.ToString().Trim();
            }
        }

        GridBar3.HeaderText = "Notes";
        GridBar3.LinksList = "Add Note";
        GridBar3.PagesList = "AddNotes.aspx?" + Convert.ToInt64(ID);
        strQuery = "SELECT sum([bint_Pieces]),sum([num_Weight]) FROM [eTn_Origin] where [bint_LoadId]=" + ID;
        dt = DBClass.returnDataTable(strQuery);
        if (dt.Rows.Count > 0)
        {
            lblPieces.Text = Convert.ToString(dt.Rows[0][0]);
            lblWeigth.Text = Convert.ToString(dt.Rows[0][1]);
        }
        dt = DBClass.returnDataTable("select L.[nvar_RailBill],L.[date_OrderBillDate],L.[nvar_PersonalBill],S.[nvar_LoadStatusDesc] from [eTn_Load] L " +
                                     "inner join [eTn_LoadStatus] S on L.bint_LoadStatusId=S.bint_LoadStatusId WHERE L.[bint_LoadId] =" + ID);
        if (dt.Rows.Count > 0)
        {
            lblStatus.Text = Convert.ToString(dt.Rows[0][3]);
            lblRailBill.Text = Convert.ToString(dt.Rows[0][0]);
            lblBillDate.Text = Convert.ToString(dt.Rows[0][1]);
            lblPersonBill.Text = Convert.ToString(dt.Rows[0][2]);
        }    

        if (dt != null) { dt.Dispose(); dt = null; }

    }
}
