<%@ Page Language="C#" MasterPageFile="~/Manager/MasterPage.master" AutoEventWireup="true" CodeFile="UpLoadDocuments.aspx.cs" Inherits="Manager_Default" Title="Untitled Page" %>

<%@ Register Src="../UserControls/MulFileUpload1.ascx" TagName="MulFileUpload1" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td>
            <uc1:GridBar ID="GridBar1" runat="server" HeaderText="Load Number :"/>
          </td>
        </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../Images/pix.gif" alt="" width="1" height="3" /></td>
        </tr>
      </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblform">
      <tr>
            <td colspan="2">
                &nbsp;</td>
        </tr>
          
        <tr>
          <td align="left" style="width: 60%;padding-left:200px;">   
            &nbsp;<asp:DropDownList ID="ddlDocuments" runat="server" DataSourceID="SqlDataSource1" DataTextField="nvar_DocumentName" DataValueField="bint_DocumentId">
            </asp:DropDownList><asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:eTransportString %>"
                SelectCommand="SELECT [bint_DocumentId], [nvar_DocumentName] FROM [eTn_DocumentTable]">
            </asp:SqlDataSource>
             &nbsp;<asp:Button ID="btnShow" runat="server" Text="Show" CssClass="btnstyle" OnClick="btnShow_Click" />
             <br />
             <br />
          </td>
          <td>
           
          </td>
        </tr>
        <tr>
            <td style="height:100%;" width="70%" align="center" valign="top">
               <uc2:MulFileUpload1 ID="MulFileUpload1_1" runat="server" />                
            </td>
            <td align="right" style="height:100%;" valign="top">
                 <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                    <td><uc1:GridBar ID="GridBar2" runat="server" HeaderText="Documents"/>
                    </td>
                   </tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tbldisplay">
                  <tr>
                    <th style="width: 119px">Document Type </th>
                    <th>Date Received </th>
                  </tr>
                  <tr id="row">
                    <td align="right" style="height: 16px; width: 119px;">
                        Load Order</td>
                    <td style="height: 16px"><asp:Label ID="lblLoadOrder" runat="server" />&nbsp;</td>
                  </tr>
                  <tr id="altrow">
                    <td align="right" style="width: 119px">
                        Bare Chassis In-Gate/Out-Gate
                        </td>
                    <td><asp:Label ID="lblPOrder" runat="server" />&nbsp;</td>
                  </tr>
                  <tr id="row">
                    <td align="right" style="width: 119px">
                        Bill Of Lading</td>
                    <td><asp:Label ID="lblBillofLading" runat="server" />&nbsp;</td>
                  </tr>
                  <tr id="altrow">
                    <td align="right" style="width: 119px">
                        Accessorial Charges
                        </td>
                    <td><asp:Label ID="lblAccessorial" runat="server" />&nbsp;</td>
                  </tr>
                  <tr id="row">
                    <td align="right" style="width: 119px">
                        Out-Gate Interchange</td>
                    <td><asp:Label ID="lblOutGate" runat="server" />&nbsp;</td>
                  </tr>
                  <tr id="altrow">
                    <td align="right" style="width: 119px">
                        In-Gate Interchange</td>
                    <td><asp:Label ID="lblInGate" runat="server" />&nbsp;</td>
                  </tr>
                  <tr id="row">
                    <td align="right" style="width: 119px">
                        Proof of Delivery</td>
                    <td><asp:Label ID="lblDelivery" runat="server" />&nbsp;</td>
                  </tr>
                  <tr id="altrow">
                    <td align="right" style="width: 119px">
                        Scale Ticket</td>
                    <td><asp:Label ID="lblTicket" runat="server" />&nbsp;</td>
                  </tr>       
                  <tr id="row">
                    <td align="right" style="width: 119px">
                        Lumper Receipt</td>
                    <td><asp:Label ID="lblLumperReceipt" runat="server" />&nbsp;</td>
                  </tr>
                  <tr id="altrow">
                    <td align="right" style="width: 119px">
                        Misc.</td>
                    <td><asp:Label ID="lblMisc" runat="server" />&nbsp;</td>
                  </tr>  
                  <tr id="row">
                    <td align="right" style="width: 119px">
                        Invoice</td>
                    <td><asp:Label ID="lblInvoice" runat="server" />&nbsp;</td>
                  </tr>                           
              </table>
                <br />
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
                    <tr>
                        <td align="right">
                        <asp:Button ID="btnupload" runat="server" CssClass="btnstyle" Text="Save" OnClick="btnupload_Click" />&nbsp;<asp:Button
                        ID="btncancel" runat="server" CssClass="btnstyle" Text="Cancel" CausesValidation="False" OnClick="btncancel_Click" /></td>
                    </tr>
                </table>
            </td>
        </tr>
      </table>
    </td>
  </tr>
 </table>
</asp:Content>

