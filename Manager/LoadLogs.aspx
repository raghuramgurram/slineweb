<%@ Page AutoEventWireup="true" CodeFile="LoadLogs.aspx.cs" Inherits="LoadLogs" Language="C#"
    MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td colspan="3">
           
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                <asp:Button ID="btnBack" runat="server" CausesValidation="false" CssClass="btnstyle"
                            OnClick="btnBack_Click" Text="Back" Visible="true" />
                            </td>
                    </tr>
                     <tr>
                    <td>&nbsp;</td>
                    </tr>               
                
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblLoadLogsHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle"></td>
                        <td>   
                        &nbsp;        
                        </td>
                    </tr>
                   
                    <tr><td colspan="2">
                      <asp:DataGrid ID="dgLoadLogDetails" Width="100%" runat="server" CssClass="Grid" > 
        <ItemStyle CssClass="GridItem" BorderColor="White" />
        <HeaderStyle CssClass="GridHeader1" BorderColor="White" />
        <AlternatingItemStyle CssClass="GridAltItem" />
       
    </asp:DataGrid>
                    </td></tr>
                    </table>      
                            </td>   
                            </tr>           
        <tr valign="top">
            <td style="width:18%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                    <tr>
                    <td>&nbsp;</td>
                    </tr>
                    <tr>
                    <td>
                        <asp:Label ID="lblLoadViewHeader" runat="server" Text="" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                    <td>&nbsp;</td>
                    </tr>
                    <tr><td>   
                     
                      <asp:DataGrid ID="dgLoadViewDetails" Width="100%" runat="server" CssClass="Grid" >          
        <ItemStyle CssClass="GridItem" BorderColor="White" />
        <HeaderStyle CssClass="GridHeader1" BorderColor="White" />
        <AlternatingItemStyle CssClass="GridAltItem" />        
    </asp:DataGrid>
                    </td></tr>
                </table>
                </td>
                <td style="width:45%">
                
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                    <td>&nbsp;</td>
                    </tr>
                    <tr>
                    <td>
                        <asp:Label ID="lblTrackLoadHeader" runat="server" Text="" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                    <td>&nbsp;</td>
                    </tr>
                    <tr><td>                    
                      <asp:DataGrid ID="dgTrackLoadDetails" Width="100%" runat="server" CssClass="Grid" AutoGenerateColumns="false">          
        <ItemStyle CssClass="GridItem" BorderColor="White" />
        <HeaderStyle CssClass="GridHeader1" BorderColor="White" />
        <AlternatingItemStyle CssClass="GridAltItem"/>   
        <Columns>
        <asp:BoundColumn DataField="Role" HeaderText="Role" ></asp:BoundColumn>        
        <asp:BoundColumn DataField="Updated By" HeaderText="Updated By" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Action" HeaderText="Action" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Updated On" HeaderText="Updated On" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Location" HeaderText="Location" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Created On" HeaderText="Status Date Time" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Notes" HeaderText="Notes" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Load Status" HeaderText="Load Status"></asp:BoundColumn>
        </Columns>     
    </asp:DataGrid>
    
                    </td></tr>
                </table>
            </td>
            <td></td>
        </tr>
        <tr valign="top">
        <td colspan="3"> 
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr  valign="top">
            <td style="width:47%">
            
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                    <tr>
                    <td>&nbsp;</td>
                    </tr>
                    <tr>
                    <td>
                        <asp:Label ID="lblOriginLogsHeader" runat="server" Text="" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                    <td>&nbsp;</td>
                    </tr>
                    <tr  valign="top"><td>   
                     
                      <asp:DataGrid ID="dgOriginLogDetails" Width="100%" runat="server" CssClass="Grid" AutoGenerateColumns="false">          
        <ItemStyle CssClass="GridItem" BorderColor="White" />
        <HeaderStyle CssClass="GridHeader1" BorderColor="White" />
        <AlternatingItemStyle CssClass="GridAltItem" /> 
         <Columns>
        <asp:BoundColumn DataField="Origin Id" HeaderText="Origin Id" Visible="false"></asp:BoundColumn>
        <asp:BoundColumn DataField="Role" HeaderText="Role" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Action Done By" HeaderText="Action Done By" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Action" HeaderText="Action" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Action Done Date" HeaderText="Action Done Date" ></asp:BoundColumn>
        <asp:BoundColumn DataField="P.O.#" HeaderText="P.O.#" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Pieces" HeaderText="Pieces"></asp:BoundColumn>
        <asp:BoundColumn DataField="Weight" HeaderText="Weight" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Stop Number" HeaderText="Stop Number" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Pickup" HeaderText="Pickup Date" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Appt. Given by" HeaderText="Appt. Given by" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Appt.#" HeaderText="Appt.#" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Address" HeaderText="Address" ></asp:BoundColumn>
        </Columns>            
    </asp:DataGrid>
                    </td></tr>
                </table>
                </td>
                        <td style="width:1%"></td>
            <td style="width:52%">
                
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                    <td>&nbsp;</td>
                    </tr>
                    <tr>
                    <td>
                        <asp:Label ID="lblDestinationLogsHeader" runat="server" Text="" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                    <td>&nbsp;</td>
                    </tr>
                    <tr  valign="top"><td>                    
                      <asp:DataGrid ID="dgDestinationLogDetails" Width="100%" runat="server" CssClass="Grid" AutoGenerateColumns="false">          
        <ItemStyle CssClass="GridItem" BorderColor="White" />
        <HeaderStyle CssClass="GridHeader1" BorderColor="White" />
        <AlternatingItemStyle CssClass="GridAltItem"/>   
         <Columns>
        <asp:BoundColumn DataField="Destination Id" HeaderText="Destination Id" Visible="false"></asp:BoundColumn>
        <asp:BoundColumn DataField="Role" HeaderText="Role" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Action Done By" HeaderText="Action Done By" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Action" HeaderText="Action" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Action Done Date" HeaderText="Action Done Date" ></asp:BoundColumn>
        <asp:BoundColumn DataField="P.O.#" HeaderText="P.O.#" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Pieces" HeaderText="Pieces"></asp:BoundColumn>
        <asp:BoundColumn DataField="Weight" HeaderText="Weight" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Stop Number" HeaderText="Stop Number" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Delivery" HeaderText="Delivery Appt. Date" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Appt. Given by" HeaderText="Appt. Given by" ></asp:BoundColumn>        
        <asp:BoundColumn DataField="Appt.#" HeaderText="Appt.#" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Origins" HeaderText="Origins" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Address" HeaderText="Address" ></asp:BoundColumn>
        </Columns>       
       
    </asp:DataGrid>
    
                    </td></tr>
                </table>
            </td> 
        </tr>
            </table>           
            </td>
        </tr>
        <tr valign="top">
            <td colspan="3">
           
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
               
                     <tr>
                    <td>&nbsp;</td>
                    </tr>               
                
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblEdTrackLoad" runat="server" Text="" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle"></td>
                        <td>   
                        &nbsp;        
                        </td>
                    </tr>
                   
                    <tr><td colspan="2">
                      <asp:DataGrid ID="dgEdiTrackLoadDetails" Width="100%" runat="server" CssClass="Grid" AutoGenerateColumns="false"> 
        <ItemStyle CssClass="GridItem" BorderColor="White" />
        <HeaderStyle CssClass="GridHeader1" BorderColor="White" />
        <AlternatingItemStyle CssClass="GridAltItem" />
                          <Columns>
       <asp:BoundColumn DataField="Role" HeaderText="Role" ></asp:BoundColumn>        
        <asp:BoundColumn DataField="Updated By" HeaderText="Updated By" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Action" HeaderText="Action" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Updated On" HeaderText="Updated On" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Created On" HeaderText="Status Date Time" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Notes" HeaderText="Notes" ></asp:BoundColumn>
        <asp:BoundColumn DataField="Load Status" HeaderText="Load Status"></asp:BoundColumn>
        <asp:BoundColumn DataField="Type" HeaderText="Type"></asp:BoundColumn>
                              </Columns>
    </asp:DataGrid>
                    </td></tr>
                    </table>      
                            </td>   
                            </tr>    
    </table>   
   
</asp:Content>

