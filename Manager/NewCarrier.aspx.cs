using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class NewCarrier : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblNameError.Visible = false;        
        //Session["TopHeaderText"] = "New / Edit Carrier";
        lblInsurance.Visible = false; lnkLink.Visible = false;        
        Session["ContactType"] = "Carrier";
        if (!Page.IsPostBack)
        {
            if (Request.QueryString.Count > 0)
            {
                lblCarrierID.Text = Request.QueryString[0].ToString();
                ViewState["ID"] = Request.QueryString[0].Trim();
                try
                {
                    lblCarrierID.Visible = true;
                    lblCarrier.Visible = true;
                    ViewState["Mode"] = "Edit";
                    Session["TopHeaderText"] = "Edit Carrier";
                    FillDetails(Convert.ToInt64(ViewState["ID"]));
                    lblInsurance.Visible = false; lnkLink.Visible = true;
                    lnkLink.PostBackUrl = "~/Manager/InsuranceCarrier.aspx?" + Convert.ToString(ViewState["ID"]);
                }
                catch
                {
                }
            }
            else
            {
                lnkLink.Visible = false;
                if (ViewState["ID"] == null)
                {
                    ViewState["Mode"] = "New";
                    Grid1.Visible = false;
                    lblCarrierID.Visible = false;
                    lblCarrier.Visible = false;
                    Session["TopHeaderText"] = "New Carrier";
                    lnkBar.EnableLinksList = "false";
                    lblInsurance.Visible = true; lnkLink.Visible = false;
                }
                else
                {
                    try
                    {
                        ViewState["Mode"] = "Edit";
                        lblCarrierID.Visible = true;
                        lblCarrier.Visible = true;
                        FillDetails(Convert.ToInt64(ViewState["ID"]));
                        Session["TopHeaderText"] = "Edit Carrier";
                        lblInsurance.Visible = false; lnkLink.Visible = true;
                        lnkLink.PostBackUrl = "~/Manager/InsuranceCarrier.aspx?" + Convert.ToString(ViewState["ID"]);
                    }
                    catch
                    {
                    }
                }
            }
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (ViewState["Mode"] != null && ViewState["ID"] != null)
        {
            if (Convert.ToString(ViewState["Mode"]).StartsWith("New", true, System.Globalization.CultureInfo.CurrentCulture))
            {
                lnkBar.EnableLinksList = "false";
                lblInsurance.Visible = true; lnkLink.Visible = false;
            }
            else if (Convert.ToString(ViewState["Mode"]).StartsWith("Edit", true, System.Globalization.CultureInfo.CurrentCulture))
            {
                lblInsurance.Visible = false; lnkLink.Visible = true;
                lnkLink.PostBackUrl = "~/Manager/InsuranceCarrier.aspx?" + Convert.ToString(ViewState["ID"]);
            }
        }
    }
    private void FillDetails(long CarrierId)
    {
        //string strQuery = "SELECT [nvar_CarrierName],[num_Phone],[num_Fax],[nvar_MCLic],[bit_Status],[nvar_Dotlic],[nvar_TaxId]," +
        //                "[nvar_WebsiteURL],[nvar_Notes],[nvar_Street],[nvar_Suite],[nvar_City],[nvar_State],[nvar_Zip]" +
        //                " FROM [eTn_Carrier] where [bint_CarrierId] = " + ID;
        //DataTable dt = DBClass.returnDataTable(strQuery);
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCarrierDetails", new object[] { CarrierId });
        if (ds.Tables.Count > 0)
        {
            ViewState["Name"] = Convert.ToString(ds.Tables[0].Rows[0][0]).Trim();
            txtCarrierName.Text = Convert.ToString(ds.Tables[0].Rows[0][0]).Trim();
            txtPhone.Text = Convert.ToString(ds.Tables[0].Rows[0][1]).Trim();
            txtFax.Text =Convert.ToString(ds.Tables[0].Rows[0][2]).Trim();
            txtMCLic.Text = Convert.ToString(ds.Tables[0].Rows[0][3]).Trim();
            chkActive.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0][4]);
            txtDOTLic.Text = Convert.ToString(ds.Tables[0].Rows[0][5]).Trim();
            txtTaxID.Text = Convert.ToString(ds.Tables[0].Rows[0][6]).Trim();
            txtWebsiteURL.Text = Convert.ToString(ds.Tables[0].Rows[0][7]).Trim();
            txtNotes.Text = Convert.ToString(ds.Tables[0].Rows[0][8]).Trim();
            txtStreet.Text = Convert.ToString(ds.Tables[0].Rows[0][9]).Trim();
            txtSuite.Text = Convert.ToString(ds.Tables[0].Rows[0][10]).Trim();
            txtCity.Text = Convert.ToString(ds.Tables[0].Rows[0][11]).Trim();
            ddlState.Text = Convert.ToString(ds.Tables[0].Rows[0][12]).Trim();
            txtZip.Text = Convert.ToString(ds.Tables[0].Rows[0][13]).Trim();
            Grid1.Visible = true;
            Grid1.DeleteVisible = true;
            Grid1.EditVisible = true;
            Grid1.TableName = "eTn_CarrierContacts";
            Grid1.Primarykey = "bint_ContactId";
            Grid1.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
            Grid1.ColumnsList = "bint_ContactId;nvar_Name;" + CommonFunctions.PhoneQueryString("num_Mobile", "Mobile") + ";" +
                CommonFunctions.PhoneQueryString("num_WorkPhone", "WorkPhone") + ";" + CommonFunctions.PhoneQueryString("num_Fax", "Fax") + ";nvar_Email";
            Grid1.VisibleColumnsList = "nvar_Name;Mobile;WorkPhone;Fax;nvar_Email";
            Grid1.VisibleHeadersList = "Name;Mobile;Work Phone;Fax;Email";
            Grid1.DeleteTablesList = "eTn_CarrierContacts";
            Grid1.WhereClause = "[bint_CarrierId]=" + CarrierId;
            Grid1.EditPage = "~/Manager/AddContact.aspx";
            Grid1.BindGrid(0);
            lnkBar.PagesList = "AddContact.aspx?" + CarrierId + "New";
            lnkBar.EnableLinksList = "true";
        }
        if(ds != null) { ds.Dispose(); ds = null; }

    }    
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Manager/SearchCarrier.aspx");
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strQuery = "";
            if (string.Compare(Convert.ToString(ViewState["Name"]).Trim(),txtCarrierName.Text.Trim(),true,System.Globalization.CultureInfo.CurrentCulture)!=0)            
            {
                if (Convert.ToInt32(DBClass.executeScalar("Select Count(bint_CarrierId) from eTn_Carrier Where nvar_CarrierName ='" + txtCarrierName.Text.Trim() + "'")) > 0)
                {
                    lblNameError.Visible = true;
                    return;
                }
            }
            if (ViewState["Mode"] != null)
            {
                //SLine 2017 Enhancement for Office Location Starts
                long officeLocationId = 0;
                if (Session["OfficeLocationID"] != null)
                {
                    officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
                }
                //SLine 2017 Enhancement for Office Location Ends
                //SLine 2017 Enhancement for Office Location added officeLocationId to objParams
                object[] objParams = new object[] { txtCarrierName.Text.Trim(), txtPhone.Text,txtFax.Text, txtMCLic.Text.Trim(),(chkActive.Checked ? 1 : 0),txtDOTLic.Text.Trim(),txtTaxID.Text.Trim(),
                                                       txtWebsiteURL.Text.Trim(),txtNotes.Text.Trim(),txtStreet.Text.Trim(),txtSuite.Text.Trim(), txtCity.Text.Trim(),ddlState.Text,txtZip.Text.Trim(),officeLocationId,Convert.ToInt64(ViewState["ID"]) };
                System.Data.SqlClient.SqlParameter[] objsqlparams = null;
                if (ViewState["ID"] != null)
                {                    
                    if (Convert.ToString(ViewState["Mode"]).StartsWith("Edit", true, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        objsqlparams =  SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Update_Carrires", objParams);
                        if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Update_Carrires", objsqlparams) > 0)
                        {
                            Response.Redirect("~/Manager/SearchCarrier.aspx");
                        }
                    }                   
                }
                else if (Convert.ToString(ViewState["Mode"]).StartsWith("New", true, System.Globalization.CultureInfo.CurrentCulture))
                {
                   objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Add_Carrires", objParams);
                    lnkLink.Visible = false;
                    if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Add_Carrires", objsqlparams) > 0)
                    {
                        ViewState["ID"] =  Convert.ToInt64(objsqlparams[objsqlparams.Length - 1].Value);
                        ViewState["Mode"] = "Edit";
                        lnkBar.PagesList = "AddContact.aspx?" + Convert.ToString(ViewState["ID"]) + "New";
                        lnkBar.EnableLinksList = "true";
                        ViewState["Name"] = txtCarrierName.Text.Trim();
                        lblInsurance.Visible = false; lnkLink.Visible = true;
                        lnkLink.PostBackUrl = "~/Manager/InsuranceCarrier.aspx?" + Convert.ToString(ViewState["ID"]);
                    }
                }
            }
        } 
    }
}
