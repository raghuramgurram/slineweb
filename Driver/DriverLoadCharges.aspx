<%@ Page AutoEventWireup="true" CodeFile="DriverLoadCharges.aspx.cs" Inherits="DriverLoadCharges"
    Language="C#" MasterPageFile="~/Driver/DriverMaster.master" Title="S Line Transport Inc" %>

<%@ Register Src="../UserControls/LoadPayments.ascx" TagName="LoadPayments" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>
                <table id="GridBar" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <asp:Label ID="lblBarText" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="3" src="../images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <uc1:LoadPayments ID="LoadPayments1" runat="server" ViewClose="false" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

