<%@ Page Language="C#" AutoEventWireup="true" CodeFile="printreport.aspx.cs" Inherits="printreport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>    
    <script type="text/javascript">        
    
    </script>
</head>
<body>        
    <form id="form1" runat="server">    
    <div>         
   <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td valign="middle">
                <table  border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, sans-serif;
                    color: #4D4B4C; font-size: 12px;" width="100%">
                    <tr>
                        <td style="padding-left:10px;padding-top:10px;">
                            <asp:Label ID="lblDisplay" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                 </table>
            </td>
          </tr>
          <tr>
            <td style="padding-top:10px;">
            <table id="tblResult" runat="server" width="100%" visible="false">
        <tr style="height:25px;padding-top:20px;">
            <td colspan="2" align="right" Style="font-family: Arial, sans-serif; font-size: 12px;font-weight:bold;">
                <asp:Label ID="lblPaymentTotal" runat="server"></asp:Label>
            </td>
        </tr>
        <tr style="height:25px;padding-top:5px;">
            <td align="right" colspan="2" Style="font-family: Arial, sans-serif; font-size: 12px;font-weight:bold;">
                <asp:Label ID="lblPaid" runat="server"></asp:Label>
            </td>
        </tr>
        <tr style="height:25px;padding-top:5px;">
            <td align="right" colspan="2" Style="font-family: Arial, sans-serif; font-size: 12px;font-weight:bold;">
                <asp:Label ID="lblPending" runat="server"></asp:Label>
            </td>
        </tr>
        <tr style="height:25px;padding-top:10px;">
            <td colspan="2" Style="font-family: Arial, sans-serif; font-size: 14px;color:Red;font-weight:bold;">
                Total  deduction $ ______________________________
            </td>
        </tr>
        <tr style="height:25px;padding-top:10px;">
            <td colspan="2" Style="font-family: Arial, sans-serif; font-size: 14px;color:Red;font-weight:bold;">
                Paid  check # __________________________________
            </td>
        </tr>
        <tr style="height:25px;padding-top:10px;">
            <td colspan="2" Style="font-family: Arial, sans-serif; font-size: 14px;color:Red;font-weight:bold;">
               Total  Loads  on this sheet : <asp:Label ID="lblcount" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
            </td>
          </tr>
    </table>
    </div>
    </form>
</body>
</html>
