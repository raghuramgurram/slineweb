﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_TenderLoads : System.Web.UI.Page
{
    private int PageSize = 10;
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.AppendHeader("Refresh", "300");
        Session["TopHeaderText"] = "Tendered Loads";
        if (!IsPostBack)
        {
            LoadRepeter();
        }
    }

    public void LoadRepeter()
    {
        Session["LoadData"] = null;
        List<Customer> objlist = new List<Customer>();
        using (WebClient client = new WebClient())
        {
            string requestUrl = ConfigurationManager.AppSettings["PostmanURL"] + "api/GetTenderedLoads/" + ConfigurationManager.AppSettings["PartnerCode"]+"/"+ ConfigurationManager.AppSettings["PartnerLocationCode"]; //"http://192.168.124.166/postman/api/GetTenderedLoads/";
            string json = client.DownloadString(requestUrl);
            List<LoadDetail> objLoad = (new JavaScriptSerializer()).Deserialize<List<LoadDetail>>(json);
            //ParamsAddNewLoadDetails objparams = new ParamsAddNewLoadDetails();
            string siteName = "Move to " + (ConfigurationManager.AppSettings["PartnerLocationCode"].ToString() == "SL" ? "Integra" : "Sline");
            foreach (LoadDetail param in objLoad)
            {
                Customer objcust = CommonFunctions.ConvertEDILoadDetails(param, siteName);                
                objlist.Add(objcust);
            }
        }
        Session["LoadData"] = objlist;
        rptrCustomer.DataSource = objlist.OrderByDescending(x=>x.TransactionId);
        rptrCustomer.DataBind();
    }

    

   

    private void SendInvoiceNumber(string aggregator, string strshipmentno, string strInvoiceNumber)
    {
        string url = ConfigurationManager.AppSettings["PostmanURL"] + "api/ReciveInvoiceNumber/";
        var request = (HttpWebRequest)WebRequest.Create(url);
        request.Method = "POST";
        request.Headers.Add("token", ConfigurationManager.AppSettings["EDIToken"]);
        request.Headers.Add("aggregator", aggregator);        
        request.Headers.Add("strshipmentno", strshipmentno);
        request.Headers.Add("strInvoiceNumber", strInvoiceNumber);
        request.ContentLength = 0;
        var response = (HttpWebResponse)request.GetResponse();
        string content = string.Empty;
        if(response.StatusCode==HttpStatusCode.OK)
        {
        }
    }

    private object NullCheck(object obj)
    {
        return obj != null ? obj : DBNull.Value;
    }
    protected void rptrCustomer_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        
        if (e.CommandName == "Redirect")
        {
            //Session["LoadData"]
            string cid = e.CommandArgument.ToString();
            Response.Redirect("~/Manager/TenderLoadDetails.aspx?CID=" + cid);
        }
        if (e.CommandName == "MOVE")
        {
            string cid = e.CommandArgument.ToString();
            string url = ConfigurationManager.AppSettings["PostmanURL"] + "api/UpdateLoadLocation/";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.Headers.Add("token", ConfigurationManager.AppSettings["EDIToken"]);
            request.Headers.Add("partner", ConfigurationManager.AppSettings["PartnerCode"].ToString());
            request.Headers.Add("loadnumber", cid);
            request.Headers.Add("partnerlocation", ConfigurationManager.AppSettings["PartnerLocationCode"].ToString());
            request.ContentLength = 0;
            var response = (HttpWebResponse)request.GetResponse();
            string content = string.Empty;
            using (var stream = response.GetResponseStream())
            {
                using (var sr = new StreamReader(stream))
                {
                    content = sr.ReadToEnd();
                }
            }
            LoadRepeter();
        }
        if (e.CommandName == "ACCEPT" || e.CommandName == "REJECT")
        {

            string cid = e.CommandArgument.ToString();
            List<Customer> ld = (List<Customer>)Session["LoadData"];
            Customer loadDeatils = ld.Select(x => x).Where(x => x.orderNumber == cid).FirstOrDefault();
            string url = ConfigurationManager.AppSettings["PostmanURL"] + "api/AcknowldgeTenderedLoad/";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.Headers.Add("token", ConfigurationManager.AppSettings["EDIToken"]);
            request.Headers.Add("aggregator", loadDeatils.name);
            request.Headers.Add("partner", loadDeatils.PartnerCode);
            request.Headers.Add("strshipmentno", loadDeatils.orderNumber);
            request.Headers.Add("acceptstatus", e.CommandName == "ACCEPT" ? "True" : "False");
            request.ContentLength = 0;
            var response = (HttpWebResponse)request.GetResponse();
            string content = string.Empty;
            using (var stream = response.GetResponseStream())
            {
                using (var sr = new StreamReader(stream))
                {
                    content = sr.ReadToEnd();
                }
            }
            if (content == "0" && e.CommandName == "ACCEPT")
            {
                try
                {

                    ViewState["LoadID"] = 0;
                    long officeLocationId = 0;
                    if (Session["OfficeLocationID"] != null)
                    {
                        officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
                    }

                    string AT5 = string.Empty;
                    if (loadDeatils.AT5 != null)
                        loadDeatils.AT5.ForEach(x => AT5 = AT5 + (!string.IsNullOrWhiteSpace(AT5) ? "~":string.Empty) + x);
                    object[] objParams = new object[]{"I",loadDeatils.customerName,loadDeatils.orderNumber.Trim(),loadDeatils.remarks,string.IsNullOrWhiteSpace(AT5)?string.Empty:AT5,1,(loadDeatils.TripType=="ROUND"?2:1),string.Empty,string.IsNullOrWhiteSpace(loadDeatils.SealNumber)?string.Empty:loadDeatils.SealNumber,
                                                    string.Empty,0,loadDeatils.TripType,"40HC",(string.IsNullOrWhiteSpace(loadDeatils.TrailerNumber)?string.Empty:loadDeatils.TrailerNumber),string.Empty,officeLocationId,Convert.ToInt64(ViewState["LoadID"]),Convert.ToString(Session["UserLoginId"])};
                    System.Data.SqlClient.SqlParameter[] objsqlparams = null;
                    ViewState["LoadID"] = "0";
                    objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationManager.AppSettings["conString"], "Add_Load_Details_New", objParams);
                    
                    if (SqlHelper.ExecuteNonQuery(ConfigurationManager.AppSettings["conString"], CommandType.StoredProcedure, "Add_Load_Details_New", objsqlparams) > 0)
                    {
                        ViewState["LoadID"] = Convert.ToInt64(objsqlparams[objsqlparams.Length - 2].Value);
                        
                       // string[] Values = Convert.ToString(DBClass.executeScalar("select Case when Len([eTn_Load].[nvar_Container])>0 and Len([eTn_Load].[nvar_Container1])>0 then [eTn_Load].[nvar_Container] when Len([eTn_Load].[nvar_Container])>0 and Len([eTn_Load].[nvar_Container1])=0 then [eTn_Load].[nvar_Container] when Len([eTn_Load].[nvar_Container1])>0 then [eTn_Load].[nvar_Container1] else '' end+'^'+Case when Len([eTn_Load].[nvar_Chasis])>0 and Len([eTn_Load].[nvar_Chasis1])>0 then [eTn_Load].[nvar_Chasis] when Len([eTn_Load].[nvar_Chasis])>0 and Len([eTn_Load].[nvar_Chasis1])=0 then [eTn_Load].[nvar_Chasis] when Len([eTn_Load].[nvar_Chasis1])>0 then [eTn_Load].[nvar_Chasis1] else '' end+'^'+(isnull((select top 1 case when Len(nvar_Email)>0 then nvar_Email else '' end from eTn_ShippingContacts where bint_ShippingId=[eTn_Load].bint_ShippingId),(isnull((select top 1 case when Len(nvar_Email)>0 then nvar_Email else '' end from eTn_Shipping where bint_ShippingId=[eTn_Load].bint_ShippingId),'')))) from [eTn_Load] where [eTn_Load].[bint_LoadId]=" + Convert.ToInt64(ViewState["LoadID"]))).Trim().Split('^');
                        //if (Values != null)
                        //{
                        //    // //CommonFunctions.SendEmail(GetFromXML.AdminEmail, (Values.Length == 3 ? Values[2] : ""), GetFromXML.AdminEmail, "", "Pre Authorization for empty UNIT # " + Values[0] + " ( OffHire ) ", GetPickedUpBodyDetails(Values[0], Values[1]), new string[] { "<font color=red>This email was not sent to the shipping line,because Contact email address is not available for this shipping line.</font><br/> " }, null);
                        //    //CommonFunctions.SendEmail(GetFromXML.AdminEmail, (Values.Length == 3 ? Values[2] : ""), "", "", "Pre Authorization for empty UNIT # " + Values[0] + " ( OffHire ) ", GetPickedUpBodyDetails(Values[0], Values[1]), new string[] { "<font color=red>This email was not sent to the shipping line,because Contact email address is not available for this shipping line.</font><br/> " }, null);
                        //}
                        //Values = null;
                    }
                    if (Convert.ToString(ViewState["LoadID"]).Trim().Length > 0 || Convert.ToString(ViewState["LoadID"]) != "0")
                    {
                        if (loadDeatils.OriginDetails != null && loadDeatils.OriginDetails.Count > 0)
                        {
                            foreach (OriginDestination org in loadDeatils.OriginDetails)
                            {
                                object[] objParamsorg = new object[]{org.CompanyName,"O", officeLocationId,NullCheck(org.Address),NullCheck(org.Address1),NullCheck(org.City),NullCheck(org.StateorProvinceCode),
                                NullCheck(org.PostalCode),  CommonFunctions.TelNumber(org.Tel),NullCheck(CommonFunctions.FaxNumber(org.Fax)),NullCheck(org.Email),ViewState["LoadID"].ToString(),NullCheck(org.FromDate),NullCheck(org.ContactPerson), NullCheck(org.ToDate),org.sequenceNumber,Convert.ToString(Session["UserLoginId"])};
                                System.Data.SqlClient.SqlParameter[] objsqlparamsorg = null;
                                objsqlparamsorg = SqlHelper.PrepareSqlParamsFromSP(ConfigurationManager.AppSettings["conString"], "SP_Insert_Org_Des", objParamsorg);
                                SqlHelper.ExecuteNonQuery(ConfigurationManager.AppSettings["conString"], CommandType.StoredProcedure, "SP_Insert_Org_Des", objsqlparamsorg).ToString();
                            }

                        }
                        if (loadDeatils.DestinationDetails != null && loadDeatils.DestinationDetails.Count > 0)
                        {
                            foreach (OriginDestination org in loadDeatils.DestinationDetails)
                            {
                                object[] objParamsorg = new object[]{org.CompanyName,"D", officeLocationId,NullCheck(org.Address),NullCheck(org.Address1),NullCheck(org.City),NullCheck(org.StateorProvinceCode),
                                NullCheck(org.PostalCode),  CommonFunctions.TelNumber(org.Tel),NullCheck(CommonFunctions.FaxNumber(org.Fax)),NullCheck(org.Email),ViewState["LoadID"].ToString(),NullCheck(org.FromDate.ToString()),NullCheck(org.ContactPerson), NullCheck(org.ToDate.ToString()),org.sequenceNumber,Convert.ToString(Session["UserLoginId"])};
                                System.Data.SqlClient.SqlParameter[] objsqlparamsorg = null;
                                objsqlparamsorg = SqlHelper.PrepareSqlParamsFromSP(ConfigurationManager.AppSettings["conString"], "SP_Insert_Org_Des", objParamsorg);
                                SqlHelper.ExecuteNonQuery(ConfigurationManager.AppSettings["conString"], CommandType.StoredProcedure, "SP_Insert_Org_Des", objsqlparamsorg).ToString();
                            }
                        }
                        if (loadDeatils.Charges != null && !string.IsNullOrWhiteSpace(loadDeatils.Charges.Charge))
                        {
                            object[] objParamsorg = new object[] { ViewState["LoadID"].ToString(), NullCheck(CommonFunctions.RemoveCurencySymbol(loadDeatils.Charges.Charge)), NullCheck(CommonFunctions.RemoveCurencySymbol(loadDeatils.Charges.Advances)), NullCheck(CommonFunctions.RemoveCurencySymbol(loadDeatils.Charges.FreightRate)) };
                            System.Data.SqlClient.SqlParameter[] objsqlparamsorg = null;
                            objsqlparamsorg = SqlHelper.PrepareSqlParamsFromSP(ConfigurationManager.AppSettings["conString"], "SP_InsertUpdate_Receivables", objParamsorg);
                            SqlHelper.ExecuteNonQuery(ConfigurationManager.AppSettings["conString"], CommandType.StoredProcedure, "SP_InsertUpdate_Receivables", objsqlparamsorg).ToString();
                        }
                        //SendInvoiceNumber(loadDeatils.customerName, loadDeatils.orderNumber, ConfigurationManager.AppSettings["InvoiceNumberPrefix"].ToString() + ViewState["LoadID"].ToString());
                        Response.Write("<script>alert('Load saved successully with load number "+ ViewState["LoadID"].ToString() + ".')</script>");
                    }
                    objsqlparams = null;
                    objParams = null;

                }
                catch (Exception ex)
                { }
                finally
                {
                   
                }
            }
            LoadRepeter();
        }
    }
}