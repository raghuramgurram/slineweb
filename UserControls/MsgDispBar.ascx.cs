using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class MsgDispBar : System.Web.UI.UserControl
{
    public string HeaderText
    {
        set { lblText.Text = Convert.ToString(value).Trim(); }
    }
    public string RedirectPage
    {
        set { lnlLink.PostBackUrl = Convert.ToString(value).Trim(); }
    }
    public string Message
    {
        set {ViewState["Message"]=value.Trim();}
    }
    public string LinkText
    {
        set
        {
            lnkMsg.Text = Convert.ToString(value).Trim();
            lnlLink.Text = Convert.ToString(value).Trim();
        }
    }
    public bool LinkVisible
    {
        set 
        {
            lnkMsg.Visible = value;
            lnlLink.Visible = (!lnkMsg.Visible);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void lnkMsg_Click(object sender, EventArgs e)
    {
        if(ViewState["Message"]!=null)
            Response.Write("<script>alert('"+Convert.ToString(ViewState["Message"]).Trim()+"')</script>");
    }
}
