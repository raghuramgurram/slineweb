using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CustomerAccessorialCharges : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Accessorial Charges";
        this.Page.Title = lblCompanyName.Text = GetFromXML.CompanyName;
        //this.Page.Title = lblCompanyName.Text = Convert.ToString(ConfigurationSettings.AppSettings["CompanyName"]);
        lblPhone.Text = GetFromXML.CompanyPhone + " " + GetFromXML.CompanyFax;

        if (!IsPostBack)
        {
            if (Request.QueryString.Count > 0)
            {
                ViewState["ID"] = Convert.ToInt64(Request.QueryString[0]);
                FillDetails();
            }
        }
    }

    private void FillDetails()
    {
        if (ViewState["ID"] != null)
        {
            lblLoad.Text = Convert.ToString(ViewState["ID"]);
            string strQuery = "SELECT C.[nvar_CustomerName],C.[nvar_Street],C.[nvar_Suite],C.[nvar_City],C.[nvar_State],C.[nvar_Zip],L.[nvar_Ref],T.[nvar_LoadTypeDesc],CT.[nvar_CommodityTypeDesc],L.[nvar_Booking]," +
                               "L.[nvar_Container],L.[nvar_Container1],L.[nvar_Chasis],L.[nvar_Chasis1],L.[nvar_Remarks],L.[nvar_Description]" +
                               "FROM [eTn_Load] L inner join [eTn_Customer] C on L.[bint_CustomerId]=C.[bint_CustomerId] inner join [eTn_LoadType] T on L.[bint_LoadTypeId]=T.[bint_LoadTypeId]  " +
                               "inner join [eTn_CommodityType] CT on L.[bint_CommodityTypeId]=CT.[bint_CommodityTypeId] where L.[bint_LoadId]=" + Convert.ToString(ViewState["ID"]);
            DataTable dt = DBClass.returnDataTable(strQuery);
            if (dt.Rows.Count > 0)
            {
                lblBillTo.Text = Convert.ToString(dt.Rows[0][0]) + "<br/>" + Convert.ToString(dt.Rows[0][1]) + "  " + Convert.ToString(dt.Rows[0][2]) + "<br/>";
                lblBillTo.Text += Convert.ToString(dt.Rows[0][3]) + ", " + Convert.ToString(dt.Rows[0][4]) + " " + Convert.ToString(dt.Rows[0][5]);
                lblBillingRef.Text = Convert.ToString(dt.Rows[0][6]);
                lblType.Text = Convert.ToString(dt.Rows[0][7]);
                lblCommodity.Text = Convert.ToString(dt.Rows[0][8]);
                lblBooking.Text = Convert.ToString(dt.Rows[0][9]);
                lblContainer.Text = Convert.ToString(dt.Rows[0][10]);
                lblChassie.Text = Convert.ToString(dt.Rows[0][12]);
                lblremarks.Text = Convert.ToString(dt.Rows[0][14]);
                lblDesc.Text = Convert.ToString(dt.Rows[0][15]);
            }
            LoadReceivables1.LoadId = Convert.ToString(ViewState["ID"]);
            LoadReceivables1.BackButtonVisible = false;
            LoadReceivables1.RedirectBackURL = "~/Customer/CustomerTrackLoad.aspx?" + Convert.ToString(ViewState["ID"]);
        } 
    }

    //private void FillDetails(long ID)
    //{
    //    lblLoad.Text = ID.ToString();
    //    string strQuery = "SELECT C.[nvar_CustomerName],C.[nvar_Street],C.[nvar_Suite],C.[nvar_City],C.[nvar_State],C.[nvar_Zip],L.[nvar_Ref],T.[nvar_LoadTypeDesc],CT.[nvar_CommodityTypeDesc],L.[nvar_Booking]," +
    //                       "L.[nvar_Container],L.[nvar_Container1],L.[nvar_Chasis],L.[nvar_Chasis1],L.[nvar_Remarks],L.[nvar_Description]" +
    //                       "FROM [eTn_Load] L inner join [eTn_Customer] C on L.[bint_CustomerId]=C.[bint_CustomerId] inner join [eTn_LoadType] T on L.[bint_LoadTypeId]=T.[bint_LoadTypeId]  " +
    //                       "inner join [eTn_CommodityType] CT on L.[bint_CommodityTypeId]=CT.[bint_CommodityTypeId] where L.[bint_LoadId]="+ID;
    //    DataTable dt = DBClass.returnDataTable(strQuery);
    //    if (dt.Rows.Count > 0)
    //    {
    //        lblBillTo.Text = Convert.ToString(dt.Rows[0][0]) + "<br/>" + Convert.ToString(dt.Rows[0][1]) + "  " + Convert.ToString(dt.Rows[0][2])+"<br/>";
    //        lblBillTo.Text += Convert.ToString(dt.Rows[0][3]) + ", " + Convert.ToString(dt.Rows[0][4]) + " " + Convert.ToString(dt.Rows[0][5]);
    //        lblBillingRef.Text = Convert.ToString(dt.Rows[0][6]);
    //        lblType.Text = Convert.ToString(dt.Rows[0][7]);
    //        lblCommodity.Text = Convert.ToString(dt.Rows[0][8]);
    //        lblBooking.Text = Convert.ToString(dt.Rows[0][9]);
    //        lblContainer.Text = Convert.ToString(dt.Rows[0][10]);
    //        lblChassie.Text = Convert.ToString(dt.Rows[0][12]);
    //        lblremarks.Text = Convert.ToString(dt.Rows[0][14]);
    //        lblDesc.Text = Convert.ToString(dt.Rows[0][15]);
    //    }

    //        strQuery = "SELECT Isnull([num_DryRun],0),Isnull([bint_ExtraStops],0),Isnull([num_LumperCharges],0),Isnull([num_DetentionCharges],0),Isnull([num_ChassisSplit],0), " +
    //                    "Isnull([num_ChassisRent],0),Isnull([num_Pallets],0),Isnull([num_ContainerWashout],0),Isnull([num_YardStorage],0),Isnull([num_RampPullCharges],0)," +
    //                    "Isnull([num_TriaxleCharge],0),Isnull([num_TransLoadCharges],0),Isnull([num_ScaleTicketCharges],0),Isnull([num_HeavyLightScaleCharges],0), " +
    //                    "Isnull([num_OtherCharges1],0)+Isnull([num_OtherCharges2],0)+Isnull([num_OtherCharges3],0)+Isnull([num_OtherCharges4],0), " +
    //                    "Isnull([nvar_OtherChargesReason],''),Isnull([num_DryRun],0)+Isnull([bint_ExtraStops],0)+Isnull([num_LumperCharges],0)+ " +
    //                    "Isnull([num_DetentionCharges],0)+Isnull([num_ChassisSplit],0)+Isnull([num_ChassisRent],0)+Isnull([num_Pallets],0)+Isnull([num_ContainerWashout],0)+" +
    //                    "Isnull([num_YardStorage],0)+Isnull([num_RampPullCharges],0)+Isnull([num_TriaxleCharge],0)+Isnull([num_TransLoadCharges],0)+" +
    //                    "Isnull([num_ScaleTicketCharges],0)+Isnull([num_HeavyLightScaleCharges],0)+Isnull([num_OtherCharges1],0)+" +
    //                    "Isnull([num_OtherCharges2],0)+Isnull([num_OtherCharges3],0)+Isnull([num_OtherCharges4],0)," +
    //                    "Isnull([num_GrossAmount],0)+Isnull(num_AdvanceReceived,0)+Isnull([num_DryRun],0)+Isnull([num_FuelSurchargeAmount],0)+"+
    //                    "Isnull([num_SurchargeAmount],0)+Isnull([num_PierTermination],0)+Isnull([bint_ExtraStops],0)+Isnull([num_LumperCharges],0)+ " +
    //                    "Isnull([num_DetentionCharges],0)+Isnull([num_ChassisSplit],0)+Isnull([num_ChassisRent],0)+"+
    //                    "Isnull([num_Pallets],0)+Isnull([num_ContainerWashout],0)+" +
    //                    "Isnull([num_YardStorage],0)+Isnull([num_RampPullCharges],0)+"+
    //                    "Isnull([num_TriaxleCharge],0)+Isnull([num_TransLoadCharges],0)+" +
    //                    "Isnull([num_HeavyLightScaleCharges],0)+Isnull([num_ScaleTicketCharges],0)+Isnull([num_OtherCharges1],0)+Isnull([num_OtherCharges2],0)+" +
    //                    "Isnull([num_OtherCharges3],0)+Isnull([num_OtherCharges4],0) " +
    //                    "FROM [eTn_Receivables] WHERE [bint_LoadId]=" + ID;
    //        dt = DBClass.returnDataTable(strQuery);
    //        if (dt.Rows.Count > 0)
    //        {
    //            lblDryRun.Text = Convert.ToString(dt.Rows[0][0]).Trim();
    //            lblExtraStops.Text =  Convert.ToString(dt.Rows[0][1]).Trim();
    //            lblLumCharges.Text = Convert.ToString(dt.Rows[0][2]).Trim();
    //            lblDetention.Text = Convert.ToString(dt.Rows[0][3]).Trim();
    //            lblChassisSplit.Text = Convert.ToString(dt.Rows[0][4]).Trim();
    //            lblChassisRent.Text = Convert.ToString(dt.Rows[0][5]).Trim();
    //            lblPallet.Text = Convert.ToString(dt.Rows[0][6]).Trim();
    //            lblContainerWash.Text =  Convert.ToString(dt.Rows[0][7]).Trim();
    //            lblYardStorage.Text = Convert.ToString(dt.Rows[0][8]).Trim();
    //            lblRampPull.Text =  Convert.ToString(dt.Rows[0][9]).Trim();
    //            lblTriaxle.Text = Convert.ToString(dt.Rows[0][10]).Trim();
    //            lblTransLoad.Text =  Convert.ToString(dt.Rows[0][11]).Trim();
    //            lblWaitingTime.Text =  Convert.ToString(dt.Rows[0][12]).Trim();
    //            lblScaleCharges.Text =  Convert.ToString(dt.Rows[0][13]).Trim();
    //            lblOtherCharges.Text =  Convert.ToString(dt.Rows[0][14]).Trim();
    //            lblExplanation.Text = Convert.ToString(dt.Rows[0][15]).Trim();
    //            lblTotal.Text =  Convert.ToString(dt.Rows[0][16]).Trim();
    //            lblTotalCharges.Text = Convert.ToString(dt.Rows[0][17]).Trim();
    //    }
    //}
    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (ViewState["ID"] != null)
        {
            Response.Redirect("~/Customer/CustomerTrackLoad.aspx?" + Convert.ToString(ViewState["ID"]));
        }
    }
}
