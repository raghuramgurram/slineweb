﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage.master" AutoEventWireup="true" CodeFile="TenderLoads.aspx.cs" Inherits="Manager_TenderLoads" %>

<%--<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/LoadGridManager.ascx" TagName="LoadGridManager" TagPrefix="uc1" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <link rel="stylesheet" type="text/css" href="../Styles/DataTable.css" />
    <script type="text/javascript" charset="utf8" src="../Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" charset="utf8" src="../Scripts/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            $('#tableCustomer').dataTable(
                { "bSort": false }
                );
        });
    </script>
    <div>
        <asp:Repeater ID="rptrCustomer" runat="server" OnItemCommand="rptrCustomer_ItemCommand">
            <HeaderTemplate>
                <table id="tableCustomer" class="display Grid" style="border-style:solid; border-width:1px">
                    <thead>
                        <tr>
                            <th>
                                OrderNumber
                            </th>
                            <th>
                                Name
                            </th>      
                            <th>
                                Expiry Date & Time
                            </th>                   
                                 <th>
                               Rate/Charge
                            </th>                     
                             <th>
                                ShippingLine
                            </th>  
                              <th>
                                Origins
                            </th>  
                             <th>
                                Pickup Appt. Date
                            </th>  
                             <th>
                                Destinations
                            </th> 
                            <th>
                                Delivery Appt. Date
                            </th>                             
                            <th>
                                Action
                            </th>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                     <asp:LinkButton ID="lnkbtnloaddtls" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "orderNumber")%>'  CommandName="Redirect" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "orderNumber")%>'></asp:LinkButton>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "name")%>
                    </td>                                   
                   <td>
                        <%# DataBinder.Eval(Container.DataItem, "ExpireDateString")%>
                    </td>  
                     <td>
                        <%# DataBinder.Eval(Container.DataItem, "Charges.Charge")%>
                    </td>                      
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "EquipmentProvider")%>
                    </td> 
                    <td>
                        <div> <%# DataBinder.Eval(Container.DataItem, "Origins")%></div>
                    </td>
                    <td>
                        <div><%# DataBinder.Eval(Container.DataItem, "PickupDates")%></div>
                    </td>   
                        <td>
                         <div> <%# DataBinder.Eval(Container.DataItem, "Destinations")%></div>
                    </td>
                    <td>
                         <div> <%# DataBinder.Eval(Container.DataItem, "DeliveryDates")%></div>
                    </td>                    
                   <td>
                       <asp:Button ID="btnMove" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SiteName")%>' CommandName="MOVE" CssClass="btnstyle" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "orderNumber")%>' /> 
                          <%-- <asp:Button ID="btnacpt" runat="server" Text="ACCEPT" CommandName="ACCEPT" CssClass="btnstyle" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "orderNumber")%>' /> 
                         <asp:Button ID="btnrjct" runat="server" Text="DECLINE" CommandName="REJECT" CssClass="btnstyle" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "orderNumber")%>' />
                    --%></td> 
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>

</asp:Content>

