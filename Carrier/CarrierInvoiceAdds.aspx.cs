using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CarrierInvoiceAdds : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Track Load";
        if (!IsPostBack)
        {
            if (Request.QueryString.Count > 0 && Session["CarrierId"] != null)
            {
                try
                {
                    ViewState["LoadId"] = Convert.ToInt64(Request.QueryString[0]);
                    FillDetails(Convert.ToInt64(ViewState["LoadId"]));
                }
                catch
                {
                }
            }
        }
    }
    private void FillDetails(long LoadId)
    {
        lblLoadNo.Text = LoadId.ToString();
        lblInvoice.Text = DBClass.executeScalar("select [nvar_Invoice] from [eTn_AssignCarrier] where [bint_LoadId]=" + LoadId + " and [bint_CarrierId]=" + Convert.ToInt64(Session["CarrierId"]));
        DataTable dt = DBClass.returnDataTable("Select [bint_TripTypeId],isnull([nvar_Container],''),isnull([nvar_Container1],''),isnull([nvar_Chasis],''),isnull([nvar_Chasis1],''),isnull([eTn_LoadStatus].nvar_LoadStatusDesc,'') from [eTn_Load] inner join [eTn_LoadStatus] on [eTn_LoadStatus].bint_LoadStatusId = [eTn_Load].bint_LoadStatusId  where [bint_LoadId]=" + LoadId);
        if (dt.Rows.Count > 0)
        {
            if (string.Compare(Convert.ToString(dt.Rows[0][5]), "Assigned", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                if (Convert.ToInt64(dt.Rows[0][0]) == 1)
                {
                    lblCon.Visible = true;
                    lblChas.Visible = true;
                    if (Convert.ToString(dt.Rows[0][1]).Trim().Length == 0)
                        txtContainer.Visible = true;
                    else
                    {
                        lblContainer.Text = Convert.ToString(dt.Rows[0][1]);
                        lblContainer.Visible = true;
                    }
                    if (Convert.ToString(dt.Rows[0][3]).Trim().Length == 0)
                        txtChasis.Visible = true;
                    else
                    {
                        lblChasis.Text = Convert.ToString(dt.Rows[0][3]);
                        lblChasis.Visible = true;
                    }
                }
                else if (Convert.ToInt64(dt.Rows[0][0]) == 2)
                {
                    lblCon.Visible = true;
                    lblChas.Visible = true;
                    lblCon1.Visible = true;
                    lblChas1.Visible = true;
                    if (Convert.ToString(dt.Rows[0][1]).Trim().Length == 0)
                        txtContainer.Visible = true;
                    else
                    {
                        lblContainer.Text = Convert.ToString(dt.Rows[0][1]);
                        lblContainer.Visible = true;
                    }
                    if (Convert.ToString(dt.Rows[0][3]).Trim().Length == 0)
                        txtChasis.Visible = true;
                    else
                    {
                        lblChasis.Text = Convert.ToString(dt.Rows[0][3]);
                        lblChasis.Visible = true;
                    }
                    if (Convert.ToString(dt.Rows[0][2]).Trim().Length == 0)
                        txtContainer1.Visible = true;
                    else
                    {
                        lblContainer1.Text = Convert.ToString(dt.Rows[0][2]);
                        lblContainer1.Visible = true;
                    }
                    if (Convert.ToString(dt.Rows[0][3]).Trim().Length == 0)
                        txtChasis1.Visible = true;
                    else
                    {
                        lblChasis1.Text = Convert.ToString(dt.Rows[0][4]);
                        lblChasis1.Visible = true;
                    }
                }
            }
            else
            {
                if (Convert.ToInt64(dt.Rows[0][0]) == 1)
                {
                    lblContainer.Text = Convert.ToString(dt.Rows[0][1]);
                    lblChasis.Text = Convert.ToString(dt.Rows[0][3]);
                    MakeLabelSingleTrip();
                }
                else if (Convert.ToInt64(dt.Rows[0][0]) == 2)
                {
                    lblContainer.Text = Convert.ToString(dt.Rows[0][1]);
                    lblContainer1.Text = Convert.ToString(dt.Rows[0][2]);
                    lblChasis.Text = Convert.ToString(dt.Rows[0][3]);
                    lblChasis1.Text = Convert.ToString(dt.Rows[0][4]);
                    MakeLabelRoundTrip();
                }
            }            
        }
        if (txtContainer.Visible || txtContainer1.Visible || txtChasis.Visible || txtChasis1.Visible)
        {
            btnsave.Visible = true;
        }
        else
            btnsave.Visible = false;
    }

    private void MakeLabelSingleTrip()
    {
        lblContainer.Visible = true;
        lblCon.Visible = true;
        lblChasis.Visible = true;
        lblChas.Visible = true;
    }
    private void MakeLabelRoundTrip()
    {
        lblContainer.Visible = true;
        lblCon.Visible = true;
        lblContainer1.Visible = true;
        lblCon1.Visible = true;
        lblChasis.Visible = true;
        lblChas.Visible = true;
        lblChasis1.Visible = true;
        lblChas1.Visible = true;
    }


    protected void btncancel_Click(object sender, EventArgs e)
    {
        if (ViewState["LoadId"] != null)
        {
            Response.Redirect("CarrierTrackLoad.aspx?" + Convert.ToString(ViewState["LoadId"]));
        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (ViewState["LoadId"] != null)
        {
            if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_AddContainerAndChasisForLoad", new object[] { Convert.ToInt64(ViewState["LoadId"]), 
                ((lblContainer.Visible)?lblContainer.Text.Trim():txtContainer.Text.Trim()), 
                ((lblChasis.Visible)?lblChasis.Text.Trim():txtChasis.Text.Trim()),
                ((lblContainer1.Visible)?lblContainer1.Text.Trim():txtContainer1.Text.Trim()),                  
                ((lblChasis1.Visible)?lblChasis1.Text.Trim():txtChasis1.Text.Trim())}) > 0)
            {
                Response.Redirect("CarrierTrackLoad.aspx?" + Convert.ToString(ViewState["LoadId"]));
            }
        }
    }
}
