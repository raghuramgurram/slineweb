﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage.master" AutoEventWireup="true" CodeFile="DisplayInvoiceProcessingReport.aspx.cs" Inherits="Manager_DisplayInvoiceProcessingReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    function goBack() {
        window.location.href = "../Manager/InvoiceProcessing.aspx";
        return window.location.href;
    }
</script>
<div id="ContentTbl" class="auto-height">
<table width="100%" style="position: absolute; top: 108px;" cellpadding="0" cellspacing="0">
<tr>
<td align="center" >

<input type="button" value="Back" style="height:21px;left: 685px; position: absolute;" class="btnstyle" onclick="goBack()"/>
<asp:Button ID="btnExport" runat="server" CssClass="btnstyle" Text="Export to Excel" Height="21px" style="left: 735px; position: absolute;" OnClick="btnExport_Click"/>
<asp:Button ID="btnMakeInvoiceable" runat="server" CssClass="btnstyle" style="left: 843px; position: absolute;" Text="Change the Status to Invoiceable"
        OnClientClick="return confirm('Are you sure you want to change status to Invoiceable, for selected loads?');"
        OnClick="btnMakeInvoiceable_Click" Height="21px" 
        Visible="true" />
</td>
</tr>
</table>
 <asp:Label ID="msg" runat="server" Visible="true" Font-Bold="True" ForeColor="Red"
        Style="font-family: Arial, sans-serif; font-size: 12px;"></asp:Label>
<table id="tbldispaly" runat="server" cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr id="row">
        <td>
    <table  border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, sans-serif;
        color: #4D4B4C; font-size: 12px; height: 1px;" width="100%">
        <tr>
            <td valign="top">
                <asp:Label ID="lblHeader" runat="server" Visible="false" Font-Bold="True" ForeColor="Blue"
        Style="font-family: Arial, sans-serif; font-size: 12px;"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="height: 4px" valign="top">
                &nbsp;<asp:Label ID="lblSearchCriteria" runat="server" Font-Bold="True" ForeColor="Blue"
        Style="font-family: Arial, sans-serif; font-size: 12px;" Visible="false">
    </asp:Label>
    </td>            
        </tr>
        <tr><td>&nbsp;</td></tr>
    </table>
    </td>
    </tr>
    <tr>
    <td>
    <table  border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, sans-serif;
        color: #4D4B4C; font-size: 12px; height: 1px;" width="100%" id="Table1">
        <tr><td>
        <asp:DataGrid ID="dggridInvoiceProcessing" Width="100%" runat="server" CssClass="Grid" AllowPaging="false" AutoGenerateColumns="false" GridLines="Both" > 
        <ItemStyle CssClass="GridItem" BorderColor="#000000" />
        <HeaderStyle CssClass="GridHeader" BorderColor="#000000" />
        <AlternatingItemStyle CssClass="GridAltItem" />
         <Columns>
         <asp:BoundColumn HeaderText="Customer" DataField="Customer" 
                 HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Center" 
                 ItemStyle-VerticalAlign="Middle">
<HeaderStyle Width="60px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Load#" DataField="LoadId" 
                 ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Container#" DataField="Container" 
                 ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Origin" DataField="Origin" 
                 ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Destination" DataField="Destination" 
                 ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Delivered on" DataField="DeliveredDate" 
                 ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Driver One" DataField="Driver1" 
                 HeaderStyle-Width="100px"  ItemStyle-HorizontalAlign="Center" 
                 ItemStyle-VerticalAlign="Middle">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Driver Two" DataField="Driver2" 
                 HeaderStyle-Width="100px"  ItemStyle-HorizontalAlign="Center" 
                 ItemStyle-VerticalAlign="Middle">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Driver Three" DataField="Driver3" 
                 HeaderStyle-Width="100px"  ItemStyle-HorizontalAlign="Center" 
                 ItemStyle-VerticalAlign="Middle">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Driver Four" DataField="Driver4" 
                 HeaderStyle-Width="100px"  ItemStyle-HorizontalAlign="Center" 
                 ItemStyle-VerticalAlign="Middle">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Receivable" DataField="Receivable" 
                 HeaderStyle-Width="80px"  ItemStyle-HorizontalAlign="Center" 
                 ItemStyle-VerticalAlign="Middle">
<HeaderStyle Width="80px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Pay Driver1" DataField="PayDriver1" 
                 HeaderStyle-Width="80px"  ItemStyle-HorizontalAlign="Center" 
                 ItemStyle-VerticalAlign="Middle">
<HeaderStyle Width="80px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Pay Driver2" DataField="PayDriver2" 
                 HeaderStyle-Width="80px"  ItemStyle-HorizontalAlign="Center" 
                 ItemStyle-VerticalAlign="Middle">
<HeaderStyle Width="80px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Pay Driver3" DataField="PayDriver3" 
                 HeaderStyle-Width="80px"  ItemStyle-HorizontalAlign="Center" 
                 ItemStyle-VerticalAlign="Middle">
<HeaderStyle Width="80px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Pay Driver4" DataField="PayDriver4" 
                 HeaderStyle-Width="80px"  ItemStyle-HorizontalAlign="Center" 
                 ItemStyle-VerticalAlign="Middle">
<HeaderStyle Width="80px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Chassis Days" DataField="ChassisDays" 
                 HeaderStyle-Width="80px"  ItemStyle-HorizontalAlign="Center" 
                 ItemStyle-VerticalAlign="Middle">
<HeaderStyle Width="80px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Chassis Charge" DataField="ChassisCharge" 
                 HeaderStyle-Width="80px"  ItemStyle-HorizontalAlign="Center" 
                 ItemStyle-VerticalAlign="Middle">
<HeaderStyle Width="80px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Miscellaneous" DataField="Misslinius" 
                 HeaderStyle-Width="80px"  ItemStyle-HorizontalAlign="Center" 
                 ItemStyle-VerticalAlign="Middle">
<HeaderStyle Width="80px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Charges Per Deim" DataField="ChargesPerDeim" 
                 HeaderStyle-Width="80px"  ItemStyle-HorizontalAlign="Center" 
                 ItemStyle-VerticalAlign="Middle">
<HeaderStyle Width="80px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Margin" DataField="Margin" HeaderStyle-Width="80px"  
                 ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
<HeaderStyle Width="80px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Invoiced" DataField="Invoiced" 
                 HeaderStyle-Width="80px"  ItemStyle-HorizontalAlign="Center" 
                 ItemStyle-VerticalAlign="Middle">
<HeaderStyle Width="80px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Load Status" DataField="LoadStatus" 
                 HeaderStyle-Width="80px"  ItemStyle-HorizontalAlign="Center" 
                 ItemStyle-VerticalAlign="Middle">
<HeaderStyle Width="80px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         <asp:BoundColumn HeaderText="Notes" DataField="Notes" HeaderStyle-Width="80px"  
                 ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
         
<HeaderStyle Width="80px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
             </asp:BoundColumn>
         
         <asp:TemplateColumn HeaderStyle-Width="50px" HeaderText="Invoiceable" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
         <ItemTemplate >
            <asp:CheckBox ID="chkInvoiceable" runat="server" CommandArgument='<%# Eval("LoadId") %>'/>
         </ItemTemplate>

<HeaderStyle Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
         </asp:TemplateColumn>
         <asp:TemplateColumn HeaderStyle-Width="70px" HeaderText="Action" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
         <ItemTemplate>
           <asp:LinkButton ID="lnkInvoiceable" runat="server" onclientclick="return confirm('Are you sure you want to change status to Invoiceable, for selected loads?');" OnClick="lnkInvoiceable_Click" CommandArgument='<%# Eval("LoadId") %>' >Invoiceable</asp:LinkButton>
         </ItemTemplate>

<HeaderStyle Width="70px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
         </asp:TemplateColumn>
         </Columns>
    </asp:DataGrid>
    </td></tr><tr><td>
    
       
    <asp:Label ID="lblDisplay" runat="server" Text="" Width="100%"></asp:Label>&nbsp;

    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
</div>


</asp:Content>

