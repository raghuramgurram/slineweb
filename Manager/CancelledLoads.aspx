<%@ Page Language="C#" MasterPageFile="~/Manager/MasterPage.master" AutoEventWireup="true"
    CodeFile="CancelledLoads.aspx.cs" Inherits="Manager_Default" Title="Untitled Page" %>

<%@ Register Src="../UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>
                <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblText" runat="server" Text="Shipping Line : "></asp:Label>
                            <asp:DropDownList ID="ddlShipingLines" runat="server" Width="31%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr></tr>
                    <tr id="row">
                        <td align="left" width="35%">
                            From Date&nbsp;:&nbsp;
                            <uc1:DatePicker ID="DatePicker1" runat="server" IsRequired="true" Visible="true"
                                SetInitialDate="true" />
                        </td>
                        <td align="left" colspan="2">
                            To Date&nbsp;: &nbsp;<uc1:DatePicker ID="DatePicker2" runat="server" IsRequired="true"
                                Visible="true" SetInitialDate="true" />
                        </td>
                    </tr>
                    <tr id="altrow">
                        <td align="left">
                            From Load&nbsp;: &nbsp;<asp:TextBox ID="txtFromLoad" runat="server"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromLoad"
                                Display="Dynamic" ErrorMessage="Please enter Load#" ToolTip="Please enter Load#">*</asp:RequiredFieldValidator>--%>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtFromLoad"
                                Display="Dynamic" ErrorMessage="Please enter a valid Load #" ToolTip="Please enter a valid Load #"
                                ValidationExpression="^\d*">*</asp:RegularExpressionValidator>
                        </td>
                        <td align="left" width="30%">
                            To Load&nbsp;:&nbsp;<asp:TextBox ID="txtToLoad" runat="server"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToLoad"
                                Display="Dynamic" ErrorMessage="Please enter Load#" ToolTip="Please enter Load#">*</asp:RequiredFieldValidator>--%>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtToLoad"
                                Display="Dynamic" ErrorMessage="Please enter a valid Load #" ToolTip="Please enter a valid Load #"
                                ValidationExpression="^\d*">*</asp:RegularExpressionValidator>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtFromLoad"
                                ControlToValidate="txtToLoad" Display="Dynamic" ErrorMessage="To load must gether than From Load"
                                Operator="GreaterThan" ToolTip="To load must gether than From Load" Type="Integer">*</asp:CompareValidator></td>
                        <td>
                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnstyle" OnClick="btnSearch_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
