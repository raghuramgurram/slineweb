﻿// This Page was added in SLine 2016 Updates
// It shows the no of Container's (Container Type), which are in the "Street Turn" Load Status
// This is a Public Page. (Does not need login to redirect to)



using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Drawing;



public partial class StreetTurn : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetContainerType");
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 1)
            {                
                gridviewContainers.DataSource = ds.Tables[0];
                gridviewContainers.DataBind();
                CheckCells();
            }
            
            if (ds != null) { ds.Dispose(); ds = null; }
        }
    }

    //This method, checks for Highlighting the cells
    private void CheckCells()
    {
        int intIsAllNull = 0;
        foreach (GridViewRow row in gridviewContainers.Rows)
        {
            for (int i = 1; i < gridviewContainers.Rows[0].Cells.Count; i++)
            {               
                if (row.Cells[i].Text == "" || row.Cells[i].Text == "&nbsp;" )
                {
                    row.Cells[i].BackColor = Color.White;
                    //row.Cells[0].Font.Bold = false;                   
                }
                else
                {
                    row.Cells[i].BackColor = Color.Turquoise;
                    intIsAllNull++;
                    //row.Cells[0].BackColor = Color.White;
                    //row.Cells[0].Font.Bold = true;
                }
            }
        }
        if (intIsAllNull != 0)
        {
            gridviewContainers.Visible = true;
            lblDisplay.Visible = false;
        }
        else
        {
            gridviewContainers.Visible = false;
            lblDisplay.Visible = true;
            lblDisplay.Text = "<table width=100% border=0 cellpadding=0 cellspacing=0 id=ContentTbl><tr valign=top><td>" +
            "<table border=0 width=100% height=200px border=0 cellpadding=0 cellspacing=0 align=center valign=middle><tr><td width=100% align=center valign=middle>" +
            "<b><font color=blue>No Street Turn containers available at this time. Please check later.<font></b></td></tr></table><br/><br/><br/></td></tr></table>";
        }
    }
}