﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class Manager_NewOfficeLocation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Convert.ToBoolean(Session["IsAdmin"]))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert",
                "alert('You do not have permission to view this page.');window.location ='Dashboard.aspx';", true);
        }
        else
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString.Count > 0)
                {
                    ViewState["ID"] = Request.QueryString[0].Trim();
                    try
                    {
                        ViewState["Mode"] = "Edit";
                        FillDetails(Convert.ToInt64(ViewState["ID"]));
                        Session["TopHeaderText"] = "Edit Office Location ";
                    }
                    catch
                    {
                    }
                }
                else
                {
                    if (ViewState["ID"] == null)
                    {
                        ViewState["Mode"] = "New";
                        Session["TopHeaderText"] = "New Office Location ";
                    }
                    else
                    {
                        try
                        {
                            ViewState["Mode"] = "Edit";
                            FillDetails(Convert.ToInt64(ViewState["ID"]));
                            Session["TopHeaderText"] = "Edit Office Location ";
                        }
                        catch
                        {
                        }
                    }
                }
            }
        }
    }
    private void FillDetails(long ID)
    {
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["conString"], "SP_OfficeLocationDetails", new object[] { ID });
        if (ds.Tables.Count > 0)
        {
            txtOfficeLocationName.Text = Convert.ToString(ds.Tables[0].Rows[0][1]).Trim();
            txtOfficeLocationShortName.Text = Convert.ToString(ds.Tables[0].Rows[0][2]);
            txtDescription.Text = Convert.ToString(ds.Tables[0].Rows[0][3]).Trim();
            ddlActive.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0][4]);
            txtAddress.Text = Convert.ToString(ds.Tables[0].Rows[0][5]).Trim();
            txtCity.Text = Convert.ToString(ds.Tables[0].Rows[0][6]).Trim();
            txtState.Text = Convert.ToString(ds.Tables[0].Rows[0][7]).Trim();
            txtZip.Text = Convert.ToString(ds.Tables[0].Rows[0][8]).Trim();
            txtPhone.Text = Convert.ToString(ds.Tables[0].Rows[0][9]).Trim();
            txtFax.Text = Convert.ToString(ds.Tables[0].Rows[0][10]).Trim();
        }
    }    
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Manager/OfficeLocation.aspx");
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (ViewState["Mode"] != null)
            {
                object[] objParams = new object[] { Convert.ToInt64(ViewState["ID"]), txtOfficeLocationName.Text, txtOfficeLocationShortName.Text, txtDescription.Text, (ddlActive.SelectedItem.Value == "True" ? 1 : 0), txtAddress.Text, txtCity.Text, txtState.Text, txtZip.Text,
                                                    Convert.ToDouble(txtPhone.Text),Convert.ToDouble(txtFax.Text), Convert.ToInt64(Session["UserLoginId"]) };
                System.Data.SqlClient.SqlParameter[] objsqlparams = null;
                if (ViewState["ID"] != null)
                {
                    if (Convert.ToString(ViewState["Mode"]).StartsWith("Edit", true, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationManager.AppSettings["conString"], "SP_Edit_Office_Location", objParams);
                        if (SqlHelper.ExecuteNonQuery(ConfigurationManager.AppSettings["conString"], CommandType.StoredProcedure, "SP_Edit_Office_Location", objsqlparams) > 0)
                        {
                            DataSet locationDs = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["conString"], "Get_OfficeLocation_ForTopMenu", new object[] { Convert.ToInt64(Session["UserLoginId"]) });
                            Session["LocationDS"] = locationDs;
                            Response.Redirect("~/Manager/OfficeLocation.aspx");
                        }
                    }
                }
                else if (Convert.ToString(ViewState["Mode"]).StartsWith("New", true, System.Globalization.CultureInfo.CurrentCulture))
                {
                    objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationManager.AppSettings["conString"], "SP_Add_Office_Location", objParams);
                    if (SqlHelper.ExecuteNonQuery(ConfigurationManager.AppSettings["conString"], CommandType.StoredProcedure, "SP_Add_Office_Location", objsqlparams) > 0)
                    {
                        DataSet locationDs = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["conString"], "Get_OfficeLocation_ForTopMenu", new object[] { Convert.ToInt64(Session["UserLoginId"]) });
                        Session["LocationDS"] = locationDs;
                        Response.Redirect("~/Manager/OfficeLocation.aspx");
                    }
                }
            }
        }
    }
}