<%@ Page AutoEventWireup="true" CodeFile="Reports.aspx.cs" Inherits="Reports" Language="C#"
    MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td style="height: 30%;" valign="top" width="50%">
                <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%" style="height: 100%;">
                    <tr>
                        <td style="height: 30%;" valign="top">
                            <table id="tblform" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <th>
                                        Load Status Reports
                                    </th>
                                </tr>
                                <tr id="row">
                                    <td style="height: 20px">
                                        <a href="ShipingLineWiseOpenLoads.aspx?Type=1">Open Loads - Shipping Line Wise</a>
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td style="height: 20px">
                                        <a href="OriginWiseOpenLoads.aspx">Open Loads - Origin Wise</a>
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td>
                                        <asp:LinkButton ID="lnkAllOpenLoads" runat="server" CausesValidation="False" OnClick="lnkAllOpenLoads_Click">Total Open Loads</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td>
                                        <asp:LinkButton ID="lnkPaperWorkPending" runat="server" CausesValidation="False"
                                            OnClick="lnkPaperWorkPending_Click">Loads with Paper work Pending</asp:LinkButton></td>
                                </tr>
                                <tr id="row">
                                    <td>
                                     <a href="PaperworkOverdueLoads.aspx">Loads with Paper work Pending Overdue</a>
                                        <%--<asp:LinkButton ID="lnkPaperWorkPendingOverdue" runat="server" CausesValidation="False"
                                            OnClick="lnkPaperWorkPendingOverdue_Click">Loads with Paper work Pending Overdue</asp:LinkButton>--%>
                                            </td>
                                </tr>
                                 <%--SLine 2016 ---- Added New REPORT for Invoice Processing--%>
                                <tr id="altrow">
                                    <td>
                                        <a id="link_InvoiceProcessing" href="InvoiceProcessing.aspx">Invoice Processing</a>
                                    </td>
                                </tr>
                                
                                <tr id="row">
                                    <td>
                                        <asp:LinkButton ID="lnkInvoiceable" runat="server" CausesValidation="False"
                                            OnClick="lnkInvoiceable_Click">Loads with Invoiceable</asp:LinkButton></td>
                                </tr>
                                
                                <tr id="altrow">
                                    <td style="height: 20px">
                                        <a href="ShipingLineWiseOpenLoads.aspx?Type=2">Shipping Line Wise Loads</a></td>
                                </tr>  
                                
                                 <tr id="row">
                                    <td style="height: 20px">
                                        <a href="InvoiceableLoads.aspx?Type=2">Accessorial loads - Customer wise</a></td>
                                </tr> 
                                
                                <tr id="altrow">
                                    <td>
                                        <asp:LinkButton ID="lnkLoadedIinYard" runat="server" CausesValidation="False" OnClick="lnkLoadedInYard_Click">Loaded In Yard</asp:LinkButton><a
                                            href="#"></a></td>
                                </tr>
                                <tr id="row">
                                    <td>
                                        <asp:LinkButton ID="lnlEmptiesInYard" runat="server" CausesValidation="False" OnClick="lnlEmptiesInYard_Click">Empties In Yard</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td>
                                        <a href="CancelledLoads.aspx?Type=Cancel">Cancelled Loads</a>
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td>
                                        <a href="CancelledLoads.aspx?Type=Close">Closed Loads</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="padding-top:5px">
                        <td style="height: 40%;" valign="top">
                            <table id="tblform" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <th>
                                        Daily Reports
                                    </th>
                                </tr>
                                <tr id="row">
                                    <td>
                                        <a href="DailyEnteredLoads.aspx">Daily Entered Loads</a>
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td>
                                        <asp:LinkButton ID="lnkDailyDispatchedLoads" runat="server" CausesValidation="False"
                                            OnClick="lnkDailyDispatchedLoads_Click">Daily Dispatched Loads</asp:LinkButton>
                                    </td>
                                </tr>
                                 <%--SLine 2016 Updates ----- Added a new Report for Dail Delivered Loads--%>
                                <tr id="row">
                                    <td>
                                       <a href="DailyDeliveredLoads.aspx">Daily Delivered Loads</a>
                                    </td>
                                </tr>
                                <%--SLine 2016 Updates ----- Added a new Report for Dail Delivered Loads--%>
                                <tr id="altrow">
                                    <td>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False"
                                            OnClick="lnkReadyToDispatchLoads_Click">Ready to Dispatch Loads</asp:LinkButton>
                                    </td>
                                </tr>                               
                                <tr id="row">
                                    <td>
                                        <asp:LinkButton ID="lnkLoadsWithLastFreeDay" runat="server" CausesValidation="False"
                                            OnClick="lnkLoadsWithLastFreeDay_Click">Loads With Last Free Day</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td>
                                        <asp:LinkButton ID="lnkLoadsWithNoLastFreeDay" runat="server" CausesValidation="False"
                                            OnClick="lnkLoadsWithNoLastFreeDay_Click">Loads with No Last Free Day</asp:LinkButton></td>
                                </tr>
                                <tr id="row">
                                    <td>
                                        <asp:LinkButton ID="lnkLoadswithnoapptDate" runat="server" CausesValidation="False"
                                            OnClick="lnkLoadswithnoapptDate_Click">Loads with no appt. Date</asp:LinkButton></td>
                                </tr>
                                <tr id="altrow">
                                    <td>
                                        <asp:LinkButton ID="lnkLoadswithnoPickup" runat="server" CausesValidation="False"
                                            OnClick="lnkLoadswithnoPickup_Click">Loads with no Pickup#</asp:LinkButton></td>
                                </tr>
                                <tr id="row">
                                    <td>
                                        <asp:LinkButton ID="lnkLoadwithAppDate" runat="server" OnClick="lnkLoadwithAppDate_Click">Loads  with Appt. Date </asp:LinkButton>
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td>
                                        <asp:LinkButton ID="lnkLoadwithNoTag" runat="server" OnClick="lnkLoadwithNoTag_Click">Loads with No Tag# </asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="padding-top:5px">
                        <td style="height:20%;" valign="top">
                            <table id="tblform" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <th>
                                        Driver/Customer/Carrier Reports
                                    </th>
                                </tr>
                                <tr id="row">
                        <td>
                            <a href="CustomerwiseLoads.aspx">Customer wise loads </a>
                        </td>
                    </tr>
                    <tr id="altrow">
                        <td style="height: 20px">
                            <a href="CarrierwiseLoads.aspx">Carrier wise loads</a>
                        </td>
                    </tr>
                    <tr id="row">
                        <td>
                            <a href="DriverwiseLoads.aspx">Driver wise loads </a>
                        </td>
                    </tr>
                    </table>
                        </td>
                    </tr>     
                </table>
            </td>
            <td style="height: 100%;">
                <table id="Table1" border="0" cellpadding="0" cellspacing="0" width="100%" style="height: 100%;">
                    <tr>
                    <%--SLine 2016 Enhancements--%>
                    <asp:Panel runat="server" ID="panel_PaymentReports">
                       <td style="height: 30%;" valign="top">
                            <table id="tblform" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <th>
                                        Payment Reports
                                    </th>
                                </tr>
                                <tr id="row">
                                    <td>
                                        <a id="link_Receivable" runat="server" href="ReceivableEntries.aspx">Receivable Entries</a></td>
                                </tr>
                                <tr id="altrow">
                                    <td>
                                        <a id="link_PayableEntries" href="PayableEntries.aspx">Payable Entries</a>
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td>
                                        <a id="link_ProfitLoss" href="ProfitLoss.aspx">Profit and Loss</a>
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td>
                                        <a id="link_ProfitLossCharts" href="ProfitLossCharts.aspx">Profit and Loss Charts</a>
                                    </td>
                                </tr>                               
                            </table>
                        </td>
                        </asp:Panel>
                        <%--SLine 2016 Enhancements Added panel control  --%>

                    </tr>
                    <tr style="padding-top:45px">
                        <td style="height: 40%;" valign="top">
                            <table id="tblform" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <th>
                                        Export Reports
                                    </th>
                                </tr>
                                <tr id="row">
                                    <td>
                                        <a href="ReceivableExport.aspx">Export Receivables</a>
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td>
                                        <a href="PayableExport.aspx">Export Payables</a>
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td style="height: 5px;">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td>
                                        <a href="DownloadFiles.aspx?Id=R">Download Receivable Files</a>
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td>
                                        <a href="DownloadFiles.aspx?Id=P">Download Payable Files</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="padding-top:60px">
                        <td>
                            <table id="tblform" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <th>
                                        Misc. Reports
                                    </th>
                                </tr>
                                <tr id="row">
                                    <td>
                                        <a href="UserDayLogs.aspx">User Logs</a>
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td>
                                        <a href="SMSLogs.aspx">SMS Logs</a>
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td>
                                        <a href="InBoundSMSLogs.aspx">InBound SMS Logs</a>
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td>
                                        <a href="FaxLogs.aspx">FAX Logs</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
