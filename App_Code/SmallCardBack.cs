using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;

/// <summary>
/// Summary description for SmallCard
/// </summary>
public class SmallCardBack
{
    // Public properties (all read-only).
    public string Text
    {
        get { return this.text; }
    }
    public Bitmap Image
    {
        get { return this.image; }
    }
    public int Width
    {
        get { return this.width; }
    }
    public int Height
    {
        get { return this.height; }
    }

    // Internal properties.
    private string text;
    private int width;
    private int height;
    private string familyName;
    private Bitmap image;

    // For generating random numbers.
    private Random random = new Random();

    // ====================================================================
    // Initializes a new instance of the CaptchaImage class using the
    // specified text, width and height.
    // ====================================================================
    //public SmallCardBack(string s, int width, int height)
    //{
    //    this.text = s;
    //    this.SetDimensions(width, height);
    //    this.GenerateImage();
    //}

    // ====================================================================
    // Initializes a new instance of the CaptchaImage class using the
    // specified text, width, height and font family.
    // ====================================================================
    public SmallCardBack(string s, int width, int height, string familyName,Int64 LoadId)
    {
        this.text = s;
        this.SetDimensions(width, height);
        this.SetFamilyName(familyName);
        this.GenerateImage(LoadId);
    }

    // ====================================================================
    // This member overrides Object.Finalize.
    // ====================================================================
    ~SmallCardBack()
    {
        Dispose(false);
    }

    // ====================================================================
    // Releases all resources used by this object.
    // ====================================================================
    public void Dispose()
    {
        GC.SuppressFinalize(this);
        this.Dispose(true);
    }

    // ====================================================================
    // Custom Dispose method to clean up unmanaged resources.
    // ====================================================================
    protected virtual void Dispose(bool disposing)
    {
        if (disposing)
            // Dispose of the bitmap.
            this.image.Dispose();
    }

    // ====================================================================
    // Sets the image width and height.
    // ====================================================================
    private void SetDimensions(int width, int height)
    {
        // Check the width and height.
        if (width <= 0)
            throw new ArgumentOutOfRangeException("width", width, "Argument out of range, must be greater than zero.");
        if (height <= 0)
            throw new ArgumentOutOfRangeException("height", height, "Argument out of range, must be greater than zero.");
        this.width = width;
        this.height = height;
    }

    // ====================================================================
    // Sets the font used for the image text.
    // ====================================================================
    private void SetFamilyName(string familyName)
    {
        // If the named font is not installed, default to a system font.
        try
        {
            Font font = new Font(this.familyName, 12F);
            this.familyName = familyName;
            font.Dispose();
        }
        catch (Exception ex)
        {
            this.familyName = System.Drawing.FontFamily.GenericSerif.Name;
        }
    }

    // ====================================================================
    // Creates the bitmap image.
    // ====================================================================
    private void GenerateImage(Int64 LoadIdValue)
    {
        DataTable dt = SqlHelper.ExecuteDataset(System.Configuration.ConfigurationManager.ConnectionStrings["eTransportString"].ConnectionString, "SP_GetTCardLoadDetails", new object[] { LoadIdValue }).Tables[0];
        if (dt != null && dt.Rows.Count > 0)
        {
            try
            {
                // Create a new 32-bit bitmap image.
                Bitmap bitmap = new Bitmap(this.width, this.height, PixelFormat.Format32bppArgb);

                // Create a graphics object for drawing.
                Graphics g = Graphics.FromImage(bitmap);
                g.SmoothingMode = SmoothingMode.AntiAlias;
                Rectangle rect = new Rectangle(0, 0, this.width, this.height);

                // Fill in the background.
                HatchBrush hatchBrush = new HatchBrush(HatchStyle.SmallConfetti, Color.White, Color.White);
                g.FillRectangle(hatchBrush, rect);

                // Set up the text font.
                SizeF size;
                float fontSize = rect.Height + 1;
                Font font;
                // Adjust the font size until the text fits within the image.
                do
                {
                    fontSize--;
                    font = new Font(this.familyName, fontSize, FontStyle.Bold);
                    size = g.MeasureString(this.text, font);
                } while (size.Width > rect.Width);
                int heightoffset = Convert.ToInt32(ConfigurationSettings.AppSettings["SmallTCardBackPrintOffset"]);
                // Set up the text format.
                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;

                //Create a path using the text and warp it randomly.
                //GraphicsPath path = new GraphicsPath();
                //path.AddString("DMU471959", font.FontFamily, (int)font.Style, 13, new Point(158, 26 + heightoffset), format);
                //float v = 4F;
                //PointF[] points =
                //{
                //    new PointF(this.random.Next(rect.Width) / v, this.random.Next(rect.Height) / v),
                //    new PointF(rect.Width - this.random.Next(rect.Width) / v, this.random.Next(rect.Height) / v),
                //    new PointF(this.random.Next(rect.Width) / v, rect.Height - this.random.Next(rect.Height) / v),
                //    new PointF(rect.Width - this.random.Next(rect.Width) / v, rect.Height - this.random.Next(rect.Height) / v)
                //};
                //PointF[] points =
                //{
                //    new PointF(20, 20),
                //    new PointF(40, 40),
                //    new PointF(100, 100),
                //    new PointF(200, 200)
                //};

                //Matrix matrix = new Matrix();
                //matrix.Translate(0F, 0F);
                //path.Warp(points, rect, matrix, WarpMode.Perspective, 0F);

                // Draw the text.
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                // g.FillPath(hatchBrush, path);

                SolidBrush BillNo = new SolidBrush(Color.Black);
                g.DrawString(Convert.ToString(dt.Rows[0][20]).Length > 18 ? Convert.ToString(dt.Rows[0][20]).Substring(0, 18) : Convert.ToString(dt.Rows[0][20]), new Font(font.FontFamily, 8, FontStyle.Regular), BillNo, new PointF(41, 289 + heightoffset));

                //GraphicsPath path2 = new GraphicsPath();
                //path2.AddString("This is text two", font.FontFamily, (int)font.Style, 12, new Point(160, 120), format);
                //path2.AddString("10:00", font.FontFamily, (int)font.Style, 11, new Point(346, 26 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path2);

                SolidBrush Rate = new SolidBrush(Color.Black);
                g.DrawString(Convert.ToString(dt.Rows[0][21]), new Font(font.FontFamily, 8, FontStyle.Regular), Rate, new PointF(207, 289 + heightoffset));

                //GraphicsPath path212 = new GraphicsPath();
                //path212.AddString("20/07/2007", font.FontFamily, (int)font.Style, 11, new Point(425, 26 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path212);

                SolidBrush Ref = new SolidBrush(Color.Black);
                g.DrawString(Convert.ToString(dt.Rows[0][23]), new Font(font.FontFamily, 8, FontStyle.Regular), Ref, new PointF(41, 430 + heightoffset));

                //GraphicsPath path3 = new GraphicsPath();
                //path3.AddString("FORZ400345", font.FontFamily, (int)font.Style, 11, new Point(133, 64 + heightoffset), format);
                //hatchBrush = new HatchBrush(HatchStyle.Percent90, Color.Black, Color.DarkGray);
                //g.FillPath(hatchBrush, path3);

                SolidBrush Total = new SolidBrush(Color.Black);
                g.DrawString(Convert.ToString(dt.Rows[0][22]), new Font(font.FontFamily, 8, FontStyle.Regular), Total, new PointF(207, 430 + heightoffset));

                //SolidBrush brush34 = new SolidBrush(Color.Black);
                //g.DrawString("123456", new Font(font.FontFamily, 8, FontStyle.Regular), brush34, new PointF(251, 659 + heightoffset));

                ///////////////
                // Add some random noise.
                //int m = Math.Max(rect.Width, rect.Height);
                //for (int i = 0; i < (int)(rect.Width * rect.Height / 30F); i++)
                //{
                //    int x = this.random.Next(rect.Width);
                //    int y = this.random.Next(rect.Height);
                //    int w = this.random.Next(m / 50);
                //    int h = this.random.Next(m / 50);
                //    g.FillEllipse(hatchBrush, x, y, w, h);
                //}

                // Clean up.
                font.Dispose();
                hatchBrush.Dispose();
                g.Dispose();

                // Set the image.
                this.image = bitmap;
            }
            catch (Exception ex)
            {
                Bitmap bitmap = new Bitmap(this.width, this.height, PixelFormat.Format32bppArgb);
                // Create a graphics object for drawing.
                Graphics g = Graphics.FromImage(bitmap);
                g.SmoothingMode = SmoothingMode.AntiAlias;
                Rectangle rect = new Rectangle(0, 0, this.width, this.height);

                // Fill in the background.
                HatchBrush hatchBrush = new HatchBrush(HatchStyle.SmallConfetti, Color.White, Color.White);
                g.FillRectangle(hatchBrush, rect);
                SizeF size;
                float fontSize = rect.Height + 1;
                Font font;
                // Adjust the font size until the text fits within the image.
                do
                {
                    fontSize--;
                    font = new Font(this.familyName, fontSize, FontStyle.Bold);
                    size = g.MeasureString(this.text, font);
                }
                while (size.Width > rect.Width);
                int heightoffset = Convert.ToInt32(ConfigurationSettings.AppSettings["SmallTCardPrintOffset"]);
                // Set up the text format.
                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;
                SolidBrush Time = new SolidBrush(Color.Black);
                g.DrawString("There is Error in data. Please check and try again.", new Font(font.FontFamily, 10, FontStyle.Regular), Time, new PointF(0, 0));

                font.Dispose();
                hatchBrush.Dispose();
                g.Dispose();
                // Set the image.
                this.image = bitmap;
            }
        }
    }
}
