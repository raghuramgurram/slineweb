<%@ Page Language="C#" MasterPageFile="~/Mobile/eTransportMobile.master" AutoEventWireup="true" CodeFile="ChangeStatus.aspx.cs" Inherits="Mobile_ChangeStatus" Title="Untitled Page" %>
<%@ Register Src="MobileMenu.ascx" TagName="Menu"
    TagPrefix="mu" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<mu:Menu ID="MuPageMenu" runat="server" />
   <div class="main-wraper">
        <div class="main-content"></div>
        <div class="left-cot2">
          <p>Change Status To </p>
        </div>
        <div class="rigt-cot2 btn">        
          <fieldset data-role="controlgroup" >
          <input type="radio" name="radio-choice-1" id="radpickedup" value="choice-7" runat="server" data-theme="e"/>
          <label runat='server' id="lblRadpickedup" for="ctl00_ContentPlaceHolder1_radpickedup">Pickedup</label>
          <input type="radio" name="radio-choice-1" id="radenroute" value="choice-1" runat="server" data-theme="e"/>
          <label runat='server' id="lblradenroute" for="ctl00_ContentPlaceHolder1_radenroute">En-Route</label>
           <input type="radio" name="radio-choice-1" id="radrecheddestination" value="choice-8" runat="server"  data-theme="e"/>
          <label runat='server' id="lblradrecheddestination" for="ctl00_ContentPlaceHolder1_radrecheddestination">Reached Destination</label>
          <input type="radio" name="radio-choice-1" id="raddriveronwaiting" value="choice-2" runat="server"  data-theme="e"/>
          <label runat="server" id="lblraddriveronwaiting" for="ctl00_ContentPlaceHolder1_raddriveronwaiting">Driver On waiting</label>
          <input type="radio" name="radio-choice-1" id="raddelievered" value="choice-3" runat="server"  data-theme="e"/>
          <label runat="server" id="lblraddelivered" for="ctl00_ContentPlaceHolder1_raddelievered">Delivered </label>
          <input type="radio" name="radio-choice-1" id="raddropinwarehouse" value="choice-9" runat="server" data-theme="e" />
          <label runat="server" id="lblreddropinwarehouse" for="ctl00_ContentPlaceHolder1_raddropinwarehouse">Drop in Warehouse</label>
           <input type="radio" name="radio-choice-1" id="rademptyinyard" value="choice-5" runat="server" data-theme="e" />
          <label runat="server" id="lblrademptyinyard" for="ctl00_ContentPlaceHolder1_rademptyinyard">Empty In Yard</label>
          <input type="radio" name="radio-choice-1" id="radloadinyard" value="choice-4" runat="server" data-theme="e" />
          <label runat="server" id="lblradloadinyard" for="ctl00_ContentPlaceHolder1_radloadinyard">Load In Yard</label>
          </fieldset>
        </div>
        <div class="clear"></div>
        <div class="left-cot2"> </div>
       

         <div class="rigt-cot2">
          <label for="date"> Date Time</label>
           <input  name ="datetime" runat="server" id="datetimepick" />
        </div>

        <div class="rigt-cot2">
          <label for="date"> Comments</label>
          <asp:TextBox ID="txtDiscription" runat="server" TextMode="MultiLine"></asp:TextBox>
        </div>
        <div class="clear"></div>
        <div class="left-cot2"></div>
        <div class="rigt-cot2">
          <ul>
            <li>
        <asp:LinkButton ID="lnkUpdateLoadStatus" runat="server" data-role="button" data-theme="b" data-icon="arrow-r"
        data-iconpos="right"  data-transition="flip" OnClick="lnkUpdateLoadStatus_click" OnClientClick="javascript:return settadetimepickers()"> Update </asp:LinkButton>
        </li>
          </ul>
        </div>
      </div>
      
      <asp:LinkButton ID="lnkgoback" runat="server" data-role="button" data-inline="true" data-theme="b" 
        data-icon="arrow-l" data-iconpos="left" class="backbtn fl-right" rel="external" OnClick="lnkgoback_click"> Back </asp:LinkButton>
</asp:Content>


