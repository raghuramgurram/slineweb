using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class AddPickUpdate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString.Count > 0 && Session["UserId"] != null)
            {
                string[] strParams = Convert.ToString(Request.QueryString[0]).Trim().Split('^');
                ViewState["LoadId"] = strParams[0];
                ViewState["OriginId"] = strParams[1];
                FillDeatails(strParams[0], strParams[1]);
                strParams = null;
            }
        }
    }

    private void FillDeatails(string LoadId, string OriginId)
    {
        if (Session["UserId"] != null)
        {
            string strQuery = "select " + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName;eTn_Location.nvar_Street;eTn_Location.nvar_Suite;eTn_Location.nvar_City;eTn_Location.nvar_State;eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") +
                            " ,eTn_Origin.date_PickupDateTime,eTn_Origin.nvar_App,eTn_Origin.[nvar_ApptGivenby],eTn_Origin.[date_PickupToDateTime] from eTn_Origin inner join eTn_Location on eTn_Origin.bint_LocationId = eTn_Location.bint_LocationId  inner join eTn_Company on eTn_Location.bint_CompanyId = eTn_Company.bint_CompanyId " +
                            " where eTn_Origin.[bint_LoadId]=" + LoadId + " and eTn_Origin.bint_OriginId=" + OriginId;
            DataTable dt = DBClass.returnDataTable(strQuery);
            if (dt.Rows.Count > 0)
            {
                lblOrigin.Text = dt.Rows[0][0].ToString();
                lblLoadId.Text = LoadId;
                lblContainer.Text = DBClass.executeScalar("select case when len(isnull([eTn_Load].nvar_Container,''))>0 and len(isnull([eTn_Load].[nvar_Container1],''))>0 then [eTn_Load].[nvar_Container]+'<br/>'+[eTn_Load].[nvar_Container1] when len([eTn_Load].[nvar_Container])>0 and len([eTn_Load].[nvar_Container1])=0 then [eTn_Load].[nvar_Container] when len([eTn_Load].[nvar_Container])=0 and len([eTn_Load].[nvar_Container1])>0 then [eTn_Load].[nvar_Container1] else '&nbsp;' end from eTn_Load where bint_LoadId=" + LoadId);
                lblUserId.Text = Convert.ToString(Session["UserId"]);
                txtAppt.Text = Convert.ToString(dt.Rows[0][2]);
                if (dt.Rows[0][1] != null && Convert.ToString(dt.Rows[0][1]).Length > 0)
                {
                    DateTime Dtime = Convert.ToDateTime(dt.Rows[0][1]);
                    // dtpPickUp.SetDate(Dtime);
                    dtpPickUp.Date = Dtime.ToString();
                    TimePicker1.Time = Dtime.ToShortTimeString();
                }
                if (dt.Rows[0][4] != null && Convert.ToString(dt.Rows[0][4]).Length > 0)
                {
                    TimePicker2.Time = Convert.ToDateTime(dt.Rows[0][4]).ToShortTimeString();
                }
                txtApptGivenBy.Text = Convert.ToString(dt.Rows[0][3]);
            }
        }
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        if (ViewState["LoadId"] != null && ViewState["OriginId"] != null)
        {
            Response.Redirect("~/Manager/TrackLoad.aspx?" + Convert.ToInt64(ViewState["LoadId"]));
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ViewState["LoadId"] != null && ViewState["OriginId"] != null)
        {
            if (!dtpPickUp.IsValidDate)
                return;

            if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Update_OriginPickUpDate", new object[] { Convert.ToString(Session["UserLoginId"]), Convert.ToInt64(ViewState["OriginId"]), Convert.ToInt64(ViewState["LoadId"]), CommonFunctions.CheckDateTimeNull(dtpPickUp.Date + " " + TimePicker1.Time), txtAppt.Text.Trim(), txtApptGivenBy.Text.Trim(), CommonFunctions.CheckDateTimeNull((TimePicker2.Time.Length > 0 && TimePicker2.Time != null) ? dtpPickUp.Date + " " + TimePicker2.Time.Trim() : null) }) > 0)
            {
                CommonFunctions.SendEmail(Convert.ToString(Session["UserEmailId"]), GetFromXML.NotificationToEmail, "", "",
                    "Addition of new Pickup date for Load Id = " + lblLoadId.Text.Trim() + " and Origin id = " + Convert.ToString(ViewState["OriginId"]),
                    "<br/>" + Convert.ToString(Session["UserId"]) + " added a Pickup date " + Convert.ToString(dtpPickUp.Date + " " + TimePicker1.Time) + " for Load Id = " + lblLoadId.Text.Trim() + " and for origin <br/>" +
                         lblOrigin.Text.Trim(), null, null);
                Response.Redirect("~/Manager/TrackLoad.aspx?" + Convert.ToInt64(ViewState["LoadId"]));
            }
        }
    }
}
