using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Url : System.Web.UI.UserControl
{
    public bool IsRequired
    {
        set { ReqUrl.Enabled = value; }
    }
    public string Text
    {
        get 
        {
            return txtUrl.Text.Trim();
        }
        set 
        {
            txtUrl.Text=Convert.ToString(value).Trim();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        RegUrl.Enabled = true;
        RegUrl.Visible = true;
    }
}
