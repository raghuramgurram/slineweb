<%@ Page Language="C#" MasterPageFile="~/Manager/MasterPage.master" AutoEventWireup="true" CodeFile="NewLocation.aspx.cs" Inherits="NewLocation" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/USState.ascx" TagName="USState" TagPrefix="uc3" %>

<%@ Register Src="~/UserControls/Url.ascx" TagName="Url" TagPrefix="uc2" %>

<%@ Register Src="~/UserControls/Phone.ascx" TagName="Phone" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td>
        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="tblform1">
            <tr>
                <td>
                    <asp:ValidationSummary ID="ReqLocation" runat="server" Width="526px"/>
                </td>
            </tr>
        </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblform">
          <tr>
              <td align="right" colspan="2">
                  &nbsp;</td>         
          </tr>
        <tr id="row">
          <td width="30%" align="right" style="height: 20px"><strong>Company Name :     
          </strong><strong>
            </strong></td>
          <td style="height: 20px"><strong>
          <asp:Label ID="lblCompanyName" runat="server" Text="Advanced MP Tech" Visible="false"></asp:Label>                     
            </strong> 
              <asp:TextBox ID="txtCompanyName" runat="server" Visible="false" MaxLength="150"></asp:TextBox>       
              <asp:RequiredFieldValidator ID="ReqComp" runat="server" ControlToValidate="txtCompanyName"
                  Display="Dynamic" ErrorMessage="Please enter a Company name.">*</asp:RequiredFieldValidator>
                  <asp:Label ID="lblShowError" runat="server" Text="*Company name already exists." Visible="false" Font-Bold="true" ForeColor="red"></asp:Label></td>
        </tr>
        <tr id="altrow">
            <td  align="right">
               <asp:Label ID="Label1" runat="server" Text="Location Type : "/>
            </td>
            <td valign="middle" align="left" style="padding-top:10px;">
                <asp:RadioButtonList ID="radLocationType" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Origin" Value="O"></asp:ListItem>
                    <asp:ListItem Text="Destination" Value="D"></asp:ListItem>
                    <asp:ListItem Text="Both" Value="B" Selected="True"></asp:ListItem>
                  </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
          <td colspan="2"><img src="../Images/pix.gif" width="1" height="5" alt=""/></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../Images/pix.gif" alt="" width="1" height="3" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblform">
          <tr>
            <th colspan="2">Address Details </th>
          </tr>
          <%--<tr id="row">
            <td width="30%" align="right" valign="middle"><asp:Label ID="Label1" runat="server" Text="Location Type : "/></td>
              <td align="left">
                  <asp:RadioButtonList ID="radLocationType" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Origin" Value="O"></asp:ListItem>
                    <asp:ListItem Text="Destination" Value="D"></asp:ListItem>
                    <asp:ListItem Text="Both" Value="B" Selected="True"></asp:ListItem>
                  </asp:RadioButtonList></td>
          </tr>--%>
          <tr id="row">
            <td width="30%" align="right"><asp:Label ID="lblStreet" runat="server" Text="Street : "/></td>
              <td><asp:TextBox ID="txtStreet" runat="Server" MaxLength="200"/>
                  <asp:RequiredFieldValidator ID="ReqStreet" runat="server" ControlToValidate="txtStreet"
                      Display="Dynamic" ErrorMessage="Please enter a Street.">*</asp:RequiredFieldValidator></td>
          </tr>
          <tr id="altrow">
            <td align="right"><asp:Label ID="lblSuite" runat="server" Text="Suite# : "/></td>
              <td><asp:TextBox ID="txtSuite" runat="Server" MaxLength="100"/></td>
          </tr>
          <tr id="row">
            <td align="right"><asp:Label ID="lblCity" runat="server" Text="City : "/></td>
              <td><asp:TextBox ID="txtCity" runat="Server" MaxLength="100"/></td>
          </tr>
          <tr id="altrow">
            <td align="right" style="height: 22px"><asp:Label ID="lblState" runat="server" Text="State : "/></td>
              <td style="height: 22px">
                 <uc3:USState id="ddlState" runat="server"></uc3:USState></td>
          </tr>
          <tr id="row">
            <td align="right"><asp:Label ID="lblZip" runat="server" Text="Zip : "/></td>
              <td><asp:TextBox ID="txtZip" runat="Server" MaxLength="20"/></td>
          </tr>
          <tr id="altrow">
            <td align="right" style="height: 19px"><asp:Label ID="lblPhone" runat="server" Text="Phone : "/></td>
              <td style="height: 19px"><%--<asp:TextBox ID="txtPhone" runat="Server" MaxLength="18"/>
                  <asp:RegularExpressionValidator ID="RegPhone" runat="server" ControlToValidate="txtPhone"
                      Display="Dynamic" ErrorMessage="Please enter a valid Phone number in the format (123) 123-1234 or 123-123-1234." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">*</asp:RegularExpressionValidator>--%>
                  <uc1:Phone ID="txtPhone" runat="server" />
              </td>
          </tr>
          <tr id="row">
            <td align="right"><asp:Label ID="lblFax" runat="server" Text="Fax : "/></td>
              <td><%--<asp:TextBox ID="txtFax" runat="Server" MaxLength="18"/>
                  <asp:RegularExpressionValidator ID="RegFax" runat="server" ControlToValidate="txtFax"
                      Display="Dynamic" ErrorMessage="Please enter a valid Fax number in the format (123) 123-1234 or 123-123-1234." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">*</asp:RegularExpressionValidator>--%>
                  <uc1:Phone ID="txtFax" runat="server" />
              </td> 
          </tr>
          <tr id="altrow">
            <td align="right"><asp:Label ID="lblWebsiteURL" runat="server" Text="Website URL : "/></td>
              <td>
                  <uc2:Url ID="txtWebsiteURL" runat="server" IsRequired="false"/>
              </td>
          </tr>
          <tr id="row">
            <td align="right" valign="top"><asp:Label ID="lblDirections" runat="server" Text="Directions : "/></td>
              <td><asp:TextBox ID="txtDirections" runat="Server" Height="76px" TextMode="MultiLine" Width="332px" MaxLength="1000"/></td>
          </tr>
        <tr >
            <td style="height:10px;" align="left" colspan="2"></td>
          </tr>
        <tr >
            <td  align="left" colspan="2">            &nbsp;</td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
          <tr>
            <td align="right">
                <asp:Button ID="btnSave" runat="Server" CssClass="btnstyle" Text="Save" OnClick="btnSave_Click" />
                <asp:Button ID="btnCancel" runat="Server" CssClass="btnstyle" Text="Cancel" OnClick="btnCancel_Click" CausesValidation="False" UseSubmitBehavior="False"/>
            </td>                
          </tr>
        </table>        
      </td>
    </tr>
</table>
</asp:Content>

