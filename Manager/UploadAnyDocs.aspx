<%@ Page AutoEventWireup="true" CodeFile="UploadAnyDocs.aspx.cs" Inherits="UploadAnyDocs"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/MulFileUpload.ascx" TagName="MulFileUpload" TagPrefix="uc2" %>

<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td>
              <uc1:GridBar ID="GridBar1" runat="server" HeaderText="Load Number :"/>
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../Images/pix.gif" alt="" width="1" height="3" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblform">
        <tr>
          <td align="center">             
                 <uc2:MulFileUpload ID="LoadOrder" runat="server" BrowseLabelName="Load Order :" DocumnetID="1"
                     Name="LoadOrder" Visible="true" /><br />   
              <uc2:MulFileUpload ID="PurchaseOrder" runat="server" BrowseLabelName="Purchase Order :"
                  DocumnetID="2" Name="PurchaseOrder" Visible="true" /><br />
              <uc2:MulFileUpload ID="BillofLading" runat="server" BrowseLabelName="Bill of Lading :"
                  DocumnetID="3" Name="BillofLading" Visible="true" /><br />
              <uc2:MulFileUpload ID="AccessorialCharges" runat="server" BrowseLabelName="Accessorial Charges :"
                  DocumnetID="4" Name="AccessorialCharges" Visible="true" /> <br />                           
              <uc2:MulFileUpload ID="OutGateInterchange" runat="server" BrowseLabelName="Out-GateInterchange :"
                  DocumnetID="5" Name="OutGateInterchange" Visible="true" /> <br />
              <uc2:MulFileUpload ID="InGateInterchange" runat="server" BrowseLabelName="In-Gate Interchange :"
                  DocumnetID="6" Name="InGateInterchange" Visible="true" /><br />
              <uc2:MulFileUpload ID="ProofofDelivery" runat="server" BrowseLabelName="Proof of Delivery :"
                  DocumnetID="7" Name="ProofofDelivery" Visible="true" /><br />
              <uc2:MulFileUpload ID="MiscScaleTicket" runat="server" BrowseLabelName="Misc. / Scale Ticket :"
                  DocumnetID="8" Name="MiscScaleTicket" Visible="true" /> <br />             
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../images/pix.gif" alt="" width="1" height="2" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
        <tr>
          <td align="right">
              <asp:Button ID="btnupload" runat="server" CssClass="btnstyle" Text="Save" OnClick="btnupload_Click" />&nbsp;<asp:Button
                  ID="btncancel" runat="server" CssClass="btnstyle" Text="Cancel" OnClick="btncancel_Click" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../images/pix.gif" alt="" width="1" height="10" /></td>
        </tr>
      </table></td>
  </tr>
</table>

</asp:Content>

