<%@ Page Language="C#" MasterPageFile="~/Manager/MasterPage.master" AutoEventWireup="true" CodeFile="NewLoad.aspx.cs" Inherits="newload" Title="Untitled Page" %>

<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc4" %>
<%@ Register Src="~/UserControls/LinksBar.ascx" TagName="LinksBar" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/Grid.ascx" TagName="Grid" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%--<script type="text/javascript" language="javascript">
window.onunload=Validatetrip();
function Validatetrip()
{
    alert(document.getElementById('<%=ddltrip.ClientID %>'));
}
</script>--%>
<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">  
    <td style="width: 984px">
        <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    &nbsp;<asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                    
               </td>
            </tr>
        </table>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td>
              <uc3:LinksBar ID="lnkBarUpload" runat="server" HeaderText="New Load" LinksList="Upload Load Order;Load Charges " Visible="true" />
          </td>
        </tr>
      </table>
       
      <table width="100%" border="0" cellspacing="0" cellpadding="0" id="TABLE1">
        <tr>
          <td><img src="../Images/pix.gif" alt="" width="1" height="3" /></td>
        </tr>
      </table>
      
         <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblform">
          <tr>
            <td width="50%" valign="top"><table width="100%" border="0" cellpadding="3" cellspacing="0" id="tblform">
              <tr >
                <th colspan="4" align="left" background="../Images/thbgr.gif" height="25">
                    Billing Details </th>
              </tr>
              <tr>
                <td align="right" style="width: 107px">Customer : </td>
                <td width="25%">
                    <asp:DropDownList ID="ddlcustomer" runat="server" AutoPostBack="true" OnSelectedIndexChanged ="ddlcustomer_SelectedIndexChanged">
                        <asp:ListItem Value="Select Customer">Select Customer</asp:ListItem>
                    </asp:DropDownList>
                    <asp:LinkButton ID="lblCustomer" runat="server" CausesValidation="False" OnClick="lblCustomer_Click"
                        Text="New Customer"></asp:LinkButton>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlcustomer"
                        Display="Dynamic" ErrorMessage="Please select customer." Operator="NotEqual"
                        ValueToCompare="Select Customer" Width="16px">*</asp:CompareValidator></td>
                <td width="15%" align="right"><strong>Ref# : </strong></td>
                <td>
                    <asp:TextBox ID="txtref" runat="server" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtref"
                        Display="Dynamic" ErrorMessage="Please enter Reference number.">*</asp:RequiredFieldValidator></td>
              </tr>             
            
              
              <tr>
                <td align="right" valign="top" style="width: 107px">Description : </td>
                <td valign="top">
                    <asp:TextBox ID="txtdescription" runat="server" Height="60px" TextMode="MultiLine" Width="80%" MaxLength="500"></asp:TextBox></td>
                <td align="right" valign="top">Remarks : </td>
                <td valign="top">
                    <asp:TextBox ID="txtremarks" runat="server" Height="60px" TextMode="MultiLine" Width="70%" MaxLength="500"></asp:TextBox></td>
              </tr>
              
                <%--new coloumns add by raghu--%>
              
                 <tr>
                <td align="right" style="width: 107px">Accessorial Charges Email : </td>
                <td width="25%">
                <asp:TextBox ID="txtAccessorialChargesEmailAddress" runat="server" MaxLength="100" Width="200px"/>
                  <%--<asp:RequiredFieldValidator ID="rfvAccessorialChargesEmailAddress" runat="server" ErrorMessage="Please enter Accessorial Charges Email Address." ControlToValidate="txtAccessorialChargesEmailAddress" Display="Dynamic">*</asp:RequiredFieldValidator>--%>
                  <asp:RegularExpressionValidator ID="revAccessorialChargesEmailAddress" runat="server" ErrorMessage="Please enter a valid Accessorial Charges Email Address." ControlToValidate="txtAccessorialChargesEmailAddress" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                   </td>
                <td width="15%" align="right">POD Email : </td>
                <td>
                <asp:TextBox ID="txtPODEmailAddress" runat="server" MaxLength="100" Width="200px"/>
                   <%--<asp:RequiredFieldValidator ID="rfvPODEmailAddress" runat="server" ErrorMessage="Please enter POD Email Address." ControlToValidate="txtPODEmailAddress" Display="Dynamic">*</asp:RequiredFieldValidator>--%>
                   <asp:RegularExpressionValidator ID="revPODEmailAddress" runat="server" ErrorMessage="Please enter a valid POD Email Address." ControlToValidate="txtPODEmailAddress" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                    </td>
              </tr>
              
                 <tr>
                <td align="right" style="width: 107px">Delivery Email : </td>
                <td width="25%">
                <asp:TextBox ID="txtDeliveryEmailAddress" runat="server" MaxLength="100" Width="200px"/>
                  <%--<asp:RequiredFieldValidator ID="rfvDeliveryEmailAddress" runat="server" ErrorMessage="Please enter Delivery Email Address." ControlToValidate="txtDeliveryEmailAddress" Display="Dynamic">*</asp:RequiredFieldValidator>--%>
                  <asp:RegularExpressionValidator ID="revDeliveryEmailAddress" runat="server" ErrorMessage="Please enter a valid Delivery Email Address." ControlToValidate="txtDeliveryEmailAddress" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                   </td>
                <td width="15%" align="right">Billing EMail :</td>
                <td> &nbsp;<asp:TextBox ID="txtBillingEMail" runat="server" Width="200px"></asp:TextBox></td>
              </tr>
              
              <%--End--%>
              
            </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
                  </tr>
              </table>
              <table width="100%" border="0" cellpadding="5" cellspacing="0" id="tblform">
                <tr>
                  <th colspan="6" background="../Images/thbgr.gif">Load Details </th>
                </tr>
                <tr id="row">
                  <td align="right">Last Free Date :</td>
                  <td style="width: 156px">
                      <uc4:DatePicker ID="txtlastfree" runat="server" Visible="true" IsDefault="true" IsRequired="false" CountNextYears="2" CountPreviousYears="0" />
                  </td>
                  <td align="right" >Trip :</td>
                  <td align="left" style="width: 249px">
                      <asp:DropDownList ID="ddltrip" runat="server" AutoPostBack="true" onselectedindexchanged="ddltrip_SelectedIndexChanged">
                          <asp:ListItem Value="Select">Select</asp:ListItem>
                      </asp:DropDownList>
                      <asp:Label ID="lblTripType" runat="server" Text="Trip Type"></asp:Label>
                      <asp:DropDownList ID="ddlTripType" runat="server">
                      <asp:ListItem Value="0" Selected="True">Select</asp:ListItem>
                          <asp:ListItem Value="PU">PickUp (PU)</asp:ListItem>
                          <asp:ListItem Value="DL">Delivery (DL)</asp:ListItem>
                          <asp:ListItem Value="DO">Drop (DO)</asp:ListItem>                          
                      </asp:DropDownList>
                      <asp:RequiredFieldValidator ID="reqddlTripType" runat="server" ControlToValidate="ddlTripType"
                        Display="Dynamic" ErrorMessage="Please select trip type." InitialValue="0">*</asp:RequiredFieldValidator>


                      <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="ddltrip"
                          Display="Dynamic" ErrorMessage="Please select trip." Operator="NotEqual" ValueToCompare="Select">*</asp:CompareValidator></td>
                   <td align="right" style="width: 135px">Container Size/Location:</td>
                  <td align="left"><asp:TextBox ID="txtRailBill" runat="server" MaxLength="50" Width="140px"></asp:TextBox></td>
                 </tr>
                <tr id="altrow">
                  <td align="right" style="width: 94px">Load Type :</td>
                    <td align="left" style="width: 280px">
                       <table style="width: 100%">
                       <tr>
                       <td>
                        <asp:DropDownList ID="ddlloadtype" runat="server">
                            <asp:ListItem Value="Select">Select</asp:ListItem>
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlloadtype"
                            Display="Dynamic" ErrorMessage="Please select load type." Operator="NotEqual"
                            ValueToCompare="Select">*</asp:CompareValidator>
                       </td>
                       <td align="right">Container Type :</td>
                       <td> <asp:DropDownList ID="ddlContainerType" runat="server">                            
                        </asp:DropDownList></td>
                       </tr>
                       </table>
                    </td>
                  <td align="right" >Container# :</td>
                  <td align="left" style="width: 249px">
                      <asp:TextBox ID="txtcontainer" runat="server" Width="118px" MaxLength="50" ></asp:TextBox>
                      <asp:TextBox ID="txtcontainer1" runat="server" Width="124px" MaxLength="50"></asp:TextBox></td>
                  <td align="right">Order Bill Date :</td>
                  <td style="width: 150px">
                      <uc4:DatePicker ID="txtOrderBillDate" runat="server" Visible="true" IsDefault="true" IsRequired="false" CountNextYears="2" CountPreviousYears="0" />
                  </td>
                </tr>
                <tr id="row">
                  <td align="right" style="width: 94px">Commodity :</td>
                  <td align="left" style="width: 156px">
                      <asp:DropDownList ID="ddlcommodity" runat="server" Width="84px">
                          <asp:ListItem Value="Select">Select</asp:ListItem>
                      </asp:DropDownList>
                      <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlcommodity"
                          Display="Dynamic" ErrorMessage="Please select commodity." Operator="NotEqual"
                          ValueToCompare="Select">*</asp:CompareValidator></td>
                  <td align="right" >Empty/Chasis# :</td>
                  <td align="left" style="width: 249px">
                      <asp:TextBox ID="txtempty" runat="server" Width="118px" MaxLength="50"></asp:TextBox>
                      <asp:TextBox ID="txtchasis" runat="server" Width="124px" MaxLength="50"></asp:TextBox></td>
                   <td align="right" style="width: 135px">Person Bill :</td>
                  <td><asp:TextBox ID="txtPersonBill" runat="server" Width="140px" MaxLength="100"></asp:TextBox></td>
                </tr>
                <tr id="altrow">
                  <td align="right" style="width: 94px">Booking# :</td>
                  <td align="left" style="width: 156px">
                      <asp:TextBox ID="txtbooking" runat="server" MaxLength="50"></asp:TextBox></td>
                  <td align="right" >
                      Pickup #:</td>
                         <%--new coloumns add by madhav--%>
                  <td align="left" style="width: 249px">
                      <asp:TextBox ID="txtpickup" runat="server" Width="120px" MaxLength="50"></asp:TextBox></td>  
                       <td align="right" style="width: 135px" >
                       Is a hazmat load :
                     </td>
                  <td><asp:CheckBox ID="chkIsHazmatLoad" runat="server" />
                       </td>  
                     <%--end--%>      
                </tr>
                <tr id="row">
                  <td align="right" style="width: 94px">Shipping Line :</td>
                  <td align="left" style="width: 156px">
                      <asp:DropDownList ID="ddlshipping" runat="server">
                          <asp:ListItem Value="Select">Select</asp:ListItem>
                      </asp:DropDownList>
                      <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlshipping"
                          Display="Dynamic" ErrorMessage="Please select shipping line." Operator="NotEqual"
                          ValueToCompare="Select">*</asp:CompareValidator></td>
                  <td align="right" >
                      Seal # :</td>
                  <td align="left" style="width: 249px">
                      <asp:TextBox ID="txtseal" runat="server" Width="119px" MaxLength="50"></asp:TextBox></td>
                  <td align="right" style="width: 135px" >
                       Chassis Rent :
                  </td>
                  <td><asp:CheckBox ID="chkChassisRent" runat="server" Checked="true"/></td>  
                </tr>


                <%--SLine 2016 Enhancements. Add the below Row and CHeckbox to fullfil the Rail Container Req---- --%>
                 <tr id="Tr1">
                  <td align="right" style="width: 94px"></td>
                  <td align="left" style="width: 156px"></td>
                  <td align="right" >Is Export Load :</td>
                  <td align="left" style="width: 249px">                      
                     
                     <asp:CheckBox ID="chkIsExport" runat="server" Checked="false"/></td> 
                  <td align="right" style="width: 135px" >
                       Rail Container :
                  </td>
                  <td><asp:CheckBox ID="chkRailContainer" runat="server" Checked="false"/></td>  
                </tr>


              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="height: 5px"><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
                </tr>
              </table>
               <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
                <tr>
                  <td align="right">
                      &nbsp;<asp:Button ID="Button1" runat="server" Height="22px" Text="Save" CssClass="btnstyle" OnClick="btnSave_Click"/>
                      <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="btnstyle" Height="22px"
                          Text="Cancel" OnClick="btnCancel_Click"/>
                  </td>        
                </tr>
             </table> 
     
             </table>
               <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td style="height: 15px">
                      <uc3:LinksBar ID="lnkBarOrigin" runat="server" HeaderText="Origin(s)" Visible="true" LinksList="Add Origin" />
                  </td>
                </tr>
              </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><img src="../Images/pix.gif" alt="" width="1" height="3" /></td>
                </tr>
              </table>
              
               <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td>
                      <uc2:Grid ID="Grid1" runat="server" Visible="true"/>
                  </td>
                </tr>
              </table>
              
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td><img src="../Images/pix.gif" alt="" width="1" height="3" />
                  </td>
                </tr>
              </table>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td>
                  <uc3:LinksBar ID="lnkBarDest" runat="server" HeaderText="Destination(s)" Visible="true" LinksList="Add Destination" />
              </td>
            </tr>
          </table>
           <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td>
                  <uc2:Grid ID="Grid2" runat="server" Visible="true" />
              </td>
            </tr>
          </table>
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><img src="../Images/pix.gif" alt="" width="1" height="3" /></td>
            </tr>
         </table>        
     </table>    
</asp:Content>

