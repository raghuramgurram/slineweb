<%@ Page AutoEventWireup="true" CodeFile="EditDriver.aspx.cs" Inherits="EditDriver"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="../UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/TimePicker.ascx" TagName="TimePicker" TagPrefix="uc3" %>

<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <uc2:GridBar ID="barCurrent" runat="server"/>
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="3" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table id="tblform" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <th colspan="2">
                            Driver Edit
                       </th>
                    </tr>
                    <tr id="row">
                        <td align="right" valign="middle" style="width:35%;">
                            Driver Name:
                        </td>
                        <td style="width: 65%;">
                          <strong> <asp:Label ID="lblDriver" runat="server" ></asp:Label></strong>
                            </td>
                    </tr>
                    <tr id="altrow">
                        <td align="right">
                            From :
                        </td>
                        <td>
                            <asp:TextBox ID="txtFrom" runat="server" MaxLength="50">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr id="row">
                    <td align="right">
                            City :
                        </td>
                        <td>
                            <asp:TextBox ID="txtFromCity" runat="server" MaxLength="50">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr id="altrow">
                        <td align="right">                                        
                        State :</td>
                        <td>
                        <asp:TextBox ID="txtFromstate" runat="server" MaxLength="50">
                    </asp:TextBox>  
                        </td>
                    </tr>         
                    <tr id="row">
                        <td align="right">
                        To :
                         </td>
                    <td>
                        <asp:TextBox ID="txtTO" runat="server" MaxLength="50">
                        </asp:TextBox>
                     </td>
                    </tr>
                <tr id="altrow">
                    <td align="right">
                        City :
                     </td>
                     <td>
                        <asp:TextBox ID="txtToCity" runat="server" MaxLength="50">
                        </asp:TextBox>
                    </td>
                 </tr>
                <tr id="row">
                    <td align="right">
                        State :</td>
                     <td>
                        <asp:TextBox ID="txtState" runat="server" MaxLength="50">
                        </asp:TextBox>
                     </td>
                </tr>
                <tr id="altrow">
                        <td align="right">
                            Tag# :</td>
                        <td>
                            <asp:TextBox ID="txtTag" runat="server" MaxLength="50">
                            </asp:TextBox></td>
                 </tr>
                 <tr id="row">
                    <td align="right">
                        Assigned Date / Time :</td>
                    <td>
                        <uc1:DatePicker ID="DatePicker1" runat="server" IsDefault="false" IsRequired="true" />
                        <uc3:TimePicker ID="TimePicker1" runat="server" />
                    
                    </td>
                </tr>
            </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table id="tblBottomBar" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnSave" runat="server" CssClass="btnstyle" Text="Save" OnClick="btnSave_Click"/>
                            <asp:Button ID="btnCancel" runat="server" CssClass="btnstyle" Text="Cancel" OnClick="btnCancel_Click" /></td>
                    </tr>
                </table>                   
            </td>
        </tr>
    </table>
</asp:Content>

