<%@ Page AutoEventWireup="true" CodeFile="CarrierInvoiceAdds.aspx.cs" Inherits="CarrierInvoiceAdds"
    Language="C#" MasterPageFile="~/Carrier/CarrierMaster.master" Title="S Line Transport Inc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="center" valign="top">
                <table id="tblform" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <th colspan="2">
                            Add #(s)
                        </th>
                    </tr>
                    <tr id="row">
                        <td align="right" valign="middle" width="30%">
                            Load Number :
                        </td>
                        <td>
                            <asp:Label ID="lblLoadNo" runat="server" Text=""></asp:Label></td>
                    </tr>
                    <tr id="altrow">
                        <td align="right" style="height: 20px" valign="middle">
                            Invoice# :
                        </td>
                        <td style="height: 20px">
                            <asp:Label ID="lblInvoice" runat="server" Text=""></asp:Label></td>
                    </tr>
                    <tr id="row1">
                        <td align="right" valign="middle">                            
                            <asp:Label ID="lblCon" runat="server" Text="Container/VAN# : " Visible="False"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtContainer" runat="server" TextMode="SingleLine" Visible="false">
                            </asp:TextBox>
                            <asp:Label ID="lblContainer" runat="server" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr id="row1">                        
                        <td align="right" valign="top">
                            <asp:Label ID="lblCon1" runat="server" Text="Extra Container/VAN# : " Visible="False"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtContainer1" runat="server" TextMode="SingleLine" Visible="False"></asp:TextBox>
                            <asp:Label ID="lblContainer1" runat="server" Visible="False"></asp:Label>                                             
                       </td>
                    </tr>
                    <tr id="row1">
                        <td align="right" valign="top">
                            <asp:Label ID="lblChas" runat="server" Text=" Chasis# : " Visible="False"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtChasis" runat="server" TextMode="SingleLine" Visible="false"></asp:TextBox>
                            <asp:Label ID="lblChasis" runat="server" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr id="row1">
                        <td align="right" valign="top">
                            <asp:Label ID="lblChas1" runat="server" Text=" Extra Chasis# : " Visible="False"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtChasis1" runat="server" TextMode="SingleLine" Visible="False"></asp:TextBox>
                            <asp:Label ID="lblChasis1" runat="server" Visible="False"></asp:Label>
                        </td>
                    </tr>                                          
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table id="tblBottomBar" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="right" style="height: 23px">
                            <asp:Button ID="btnsave" runat="server" CssClass="btnstyle" OnClick="btnsave_Click"
                                Text="Save" />&nbsp;<asp:Button ID="btncancel" runat="server" CausesValidation="False"
                                    CssClass="btnstyle" OnClick="btncancel_Click" Text="Cancel" UseSubmitBehavior="False" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

