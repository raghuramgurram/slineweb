<%@ Control AutoEventWireup="true" CodeFile="DatePicker.ascx.cs" Inherits="DatePicker"
    Language="C#" %>

<script language="javascript" type="text/javascript">     

     function OnChangeMonth(dropdown1,dropdown2,dropdown3)
     { 
         if(dropdown3.options !=null)
         {
             if(dropdown1.options[0].text == '---')         
             {
                OnChangeMonth1(dropdown1,dropdown2,dropdown3);
             }
             else
             {
              var year=dropdown3.options[dropdown3.selectedIndex].text       
              var day = dropdown2.selectedIndex              
              if(dropdown1.selectedIndex == '1' )
              {  
                    if(dropdown2.length==31)
                           dropdown2.remove(30)
                    if(dropdown2.length==30)      
                           dropdown2.remove(29)                 
                     if(year%4!=0)
                     {
                        if(dropdown2.length==29)   
                            dropdown2.remove(28) 
                     }   
                     else
                     {
                        dropdown2.length = 29
                        dropdown2.options[28].text=29                      
                     }                                                                                                             
              }
              else if(dropdown1.selectedIndex == '0' || dropdown1.selectedIndex == '02' ||  dropdown1.selectedIndex == '04' ||
                    dropdown1.selectedIndex == '06' || dropdown1.selectedIndex == '07' || dropdown1.selectedIndex == '09' ||
                    dropdown1.selectedIndex == '11')
              { 
                    dropdown2.length = 31
                    dropdown2.options[28].text=29
                    dropdown2.options[29].text=30
                    dropdown2.options[30].text=31
               
              } 
              else if(dropdown1.selectedIndex == '03' || dropdown1.selectedIndex == '05' || dropdown1.selectedIndex == '08' ||
                    dropdown1.selectedIndex == '10' || dropdown1.selectedIndex == '12')    
              { 
                    dropdown2.length = 30
                    dropdown2.options[28].text=29
                    dropdown2.options[29].text=30
                    if(dropdown2.length==31)
                        dropdown2.remove(30)     
              } 
              if(day <= dropdown2.length -1)
                 dropdown2.selectedIndex = day
              else 
                dropdown2.selectedIndex = '0' 
            }
          }
       }    
       function OnChangeYear(dropdown1,dropdown2,dropdown3)
       {    
             if(dropdown3.options !=null)
             {
              if(dropdown1.options[0].text == '---')         
              {
                OnChangeYear1(dropdown1,dropdown2,dropdown3);
              }
              else
              {
                 var year=dropdown3.options[dropdown3.selectedIndex].text  
                 if(year%4 == 0)
                 {          
                     if(dropdown1.selectedIndex == '1' && dropdown2.selectedIndex > '28')
                     {
                         //dropdown1.selectedIndex = '0'
                         dropdown2.selectedIndex = '0'   
                     } 
                 }   
                 else
                 {
                     if(dropdown1.selectedIndex == '1' && dropdown2.selectedIndex >= '28')
                     {
                         //dropdown1.selectedIndex = '0'
                         dropdown2.selectedIndex = '0'   
                     } 
                 }  
               }                               
             }        
       }      
       function OnClickDay(dropdown1,dropdown2,dropdown3)
       {            
         if(dropdown3.options !=null)
         {
             if(dropdown1.options[0].text == '---')         
             {
                OnClickDay1(dropdown1,dropdown2,dropdown3);
             }
             else
             {
              var year=dropdown3.options[dropdown3.selectedIndex].text          
              if(dropdown1.selectedIndex == '1' )
              {  
                    if(dropdown2.length==31)
                           dropdown2.remove(30)
                    if(dropdown2.length==30)      
                           dropdown2.remove(29)                  
                     if(year%4!=0)
                     {
                       if(dropdown2.length==29)   
                           dropdown2.remove(28)
                     }  
                     else
                     {
                        dropdown2.length = 29
                        dropdown2.options[28].text=29                      
                     }                                                                                                            
              }
              else if(dropdown1.selectedIndex == '0' || dropdown1.selectedIndex == '02' ||  dropdown1.selectedIndex == '04' ||
                    dropdown1.selectedIndex == '06' || dropdown1.selectedIndex == '07' || dropdown1.selectedIndex == '09' ||
                    dropdown1.selectedIndex == '11')
              { 
                    dropdown2.length = 31
                    dropdown2.options[28].text=29
                    dropdown2.options[29].text=30
                    dropdown2.options[30].text=31
               
              } 
              else if(dropdown1.selectedIndex == '03' || dropdown1.selectedIndex == '05' || dropdown1.selectedIndex == '08' ||
                    dropdown1.selectedIndex == '10' || dropdown1.selectedIndex == '12')    
                { 
                    dropdown2.length = 30
                    dropdown2.options[28].text=29
                    dropdown2.options[29].text=30
                    if(dropdown2.length==31)
                        dropdown2.remove(30)     
              }   
             }
          }   
       }  
        function OnChangeMonth1(dropdown1,dropdown2,dropdown3)
       { 
         if(dropdown3.options !=null)
         {
              var year=dropdown3.options[dropdown3.selectedIndex].text       
              var day = dropdown2.selectedIndex
              if(dropdown1.selectedIndex == '2' )
              {  
                    if(dropdown2.length==32)
                           dropdown2.remove(31)
                    if(dropdown2.length==31)      
                           dropdown2.remove(30)                 
                     if(year%4!=0)
                     {
                        if(dropdown2.length==30)   
                            dropdown2.remove(29) 
                     }   
                     else
                     {
                        dropdown2.length = 30
                        dropdown2.options[29].text=29                      
                     }                                                                                                             
              }
              else if(dropdown1.selectedIndex == '01' || dropdown1.selectedIndex == '03' ||  dropdown1.selectedIndex == '05' ||
                    dropdown1.selectedIndex == '07' || dropdown1.selectedIndex == '08' || dropdown1.selectedIndex == '10' ||
                    dropdown1.selectedIndex == '12')
              { 
                    dropdown2.length = 32
                    dropdown2.options[29].text=29
                    dropdown2.options[30].text=30
                    dropdown2.options[31].text=31
               
              } 
              else if(dropdown1.selectedIndex == '04' || dropdown1.selectedIndex == '06' || dropdown1.selectedIndex == '09' ||
                    dropdown1.selectedIndex == '11' || dropdown1.selectedIndex == '13')    
                { 
                    dropdown2.length = 31
                    dropdown2.options[29].text=29
                    dropdown2.options[30].text=30
                    if(dropdown2.length==32)
                        dropdown2.remove(31)     
              } 
              if(day <= dropdown2.length -1)
                 dropdown2.selectedIndex = day 
              else 
                dropdown2.selectedIndex = '02' 
          }
       }    
       function OnChangeYear1(dropdown1,dropdown2,dropdown3)
       {    
             if(dropdown3.options !=null)
             {
                 var year=dropdown3.options[dropdown3.selectedIndex].text  
                 if(year%4 == 0)
                 {          
                     if(dropdown1.selectedIndex == '02' && dropdown2.selectedIndex > '29')
                     {
                         //dropdown1.selectedIndex = '0'
                         dropdown2.selectedIndex = '01'   
                     } 
                 }   
                 else
                 {
                     if(dropdown1.selectedIndex == '02' && dropdown2.selectedIndex >= '29')
                     {
                         //dropdown1.selectedIndex = '0'
                         dropdown2.selectedIndex = '01'   
                     } 
                 }                                 
             }        
       }      
       function OnClickDay1(dropdown1,dropdown2,dropdown3)
       {            
         if(dropdown3.options !=null)
         {
              var year=dropdown3.options[dropdown3.selectedIndex].text          
              if(dropdown1.selectedIndex == '02' )
              {  
                    if(dropdown2.length==32)
                           dropdown2.remove(31)
                    if(dropdown2.length==31)      
                           dropdown2.remove(30)                  
                     if(year%4!=0)
                     {
                       if(dropdown2.length==30)   
                           dropdown2.remove(29)
                     }  
                     else
                     {
                        dropdown2.length = 30
                        dropdown2.options[29].text=29                      
                     }                                                                                                            
              }
             else if(dropdown1.selectedIndex == '01' || dropdown1.selectedIndex == '03' ||  dropdown1.selectedIndex == '05' ||
                    dropdown1.selectedIndex == '07' || dropdown1.selectedIndex == '08' || dropdown1.selectedIndex == '10' ||
                    dropdown1.selectedIndex == '12')
              { 
                    dropdown2.length = 32
                    dropdown2.options[29].text=29
                    dropdown2.options[30].text=30
                    dropdown2.options[31].text=31
               
              } 
              else if(dropdown1.selectedIndex == '04' || dropdown1.selectedIndex == '06' || dropdown1.selectedIndex == '09' ||
                    dropdown1.selectedIndex == '11' || dropdown1.selectedIndex == '13')    
                { 
                    dropdown2.length = 31
                    dropdown2.options[29].text=29
                    dropdown2.options[30].text=30
                    if(dropdown2.length==32)
                        dropdown2.remove(31)     
              }  
              
          }   
       }  
</script>
<select name="dropMonth" id="dropMonth" runat="server" style="width: 47px">
<option value="01">Jan</option>
<option value="02">Feb</option>
<option value="03">Mar</option>
<option value="04">Apr</option>
<option value="05">May</option>
<option value="06">Jun</option>
<option value="07">July</option>
<option value="08">Aug</option>
<option value="09">Sep</option>
<option value="10">Oct</option>
<option value="11">Nov</option>
<option value="12">Dec</option>
</select><asp:Label ID="lblSeperateChar1" runat="server" Text="-"></asp:Label><select name="dropDay" id="dropDay" runat="server" style="width: 40px">
    <option value="01">01</option>
    <option value="02">02</option>
    <option value="03">03</option>
    <option value="04">04</option>
    <option value="05">05</option>
    <option value="06">06</option>
    <option value="07">07</option>
    <option value="08">08</option>
    <option value="09">09</option>
    <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
    <option value="13">13</option>
    <option value="14">14</option>
    <option value="15">15</option>
    <option value="16">16</option>
    <option value="17">17</option>
    <option value="18">18</option>
    <option value="19">19</option>
    <option value="20">20</option>
    <option value="21">21</option>
    <option value="22">22</option>
    <option value="23">23</option>
    <option value="24">24</option>
    <option value="25">25</option>
    <option value="26">26</option>
    <option value="27">27</option>
    <option value="28">28</option>
    <option value="29">29</option>
    <option value="30">30</option>
    <option value="31">31</option>
</select><asp:Label ID="lblSeperateChar2" runat="server" Text="-"></asp:Label><select name="dropYear" id="dropYear" runat="server">

</select><asp:CompareValidator ID="cmpMonth" runat="server" ControlToValidate="dropMonth"
    Display="Dynamic" Enabled="False" ErrorMessage="Select Month." ValueToCompare="---"
    Visible="False" Operator="NotEqual">*</asp:CompareValidator><asp:CompareValidator ID="cmpDay" runat="server"
        ControlToValidate="dropDay" Display="Dynamic" Enabled="False" ErrorMessage="Select Day."
        ValueToCompare="--" Visible="False" Operator="NotEqual">*</asp:CompareValidator><asp:CompareValidator
            ID="cmpYear" runat="server" ControlToValidate="dropYear" Display="Dynamic" Enabled="False"
            ErrorMessage="Select Year." ValueToCompare="----" Visible="False" Operator="NotEqual">*</asp:CompareValidator><asp:Label
                ID="lblErrMessage" runat="server" ForeColor="Red" Text="Invalid Date." Visible="False"></asp:Label>
