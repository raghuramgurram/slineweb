using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class NewShippingLine : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblNameError.Visible = false;
        //Session["TopHeaderText"] = "New / Edit Shipping Line ";
        Session["ContactType"] = "Shipping";
        if (!Page.IsPostBack)
        {
            if (Request.QueryString.Count > 0)
            {
                ViewState["ID"] = Request.QueryString[0].Trim();
                try
                {
                    ViewState["Mode"] = "Edit";
                    FillDetails(Convert.ToInt64(ViewState["ID"]));
                    Session["TopHeaderText"] = "Edit Shipping Line ";
                }
                catch
                {
                }
            }
            else
            {
                if (ViewState["ID"] == null)
                {
                    ViewState["Mode"] = "New";
                    Grid1.Visible = false;
                    Session["TopHeaderText"] = "New Shipping Line ";
                    lnkBar.EnableLinksList = "false";
                }
                else
                {
                    try
                    {
                        ViewState["Mode"] = "Edit";
                        FillDetails(Convert.ToInt64(ViewState["ID"]));
                        Session["TopHeaderText"] = "Edit Shipping Line ";
                    }
                    catch
                    {
                    }
                }
            }
        }
    
    }

    private void FillDetails(long ID)
    {
        //string strQuery = "SELECT [nvar_ShippingLineName],[bit_Status],[nvar_PierTermination],[nvar_Street],[nvar_Suite],[nvar_City]," +
        //"[nvar_State],[nvar_Zip],[num_Phone],[num_Fax],[nvar_Email],[nvar_WebsiteURL],[nvar_Directions]" +
        //" FROM [eTn_Shipping] where [bint_ShippingId] = " + ID;
        //DataTable dt = DBClass.returnDataTable(strQuery);
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetShippingDetails", new object[] { ID });
        if (ds.Tables.Count > 0)
        {
            ViewState["Name"] = Convert.ToString(ds.Tables[0].Rows[0][0]).Trim();
            txtShippingLine.Text = Convert.ToString(ds.Tables[0].Rows[0][0]).Trim();
            chkActive.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0][1]);
            txtPier.Text = Convert.ToString(ds.Tables[0].Rows[0][2]);
            txtStreet.Text = Convert.ToString(ds.Tables[0].Rows[0][3]).Trim();
            txtSuite.Text = Convert.ToString(ds.Tables[0].Rows[0][4]).Trim();
            txtCity.Text = Convert.ToString(ds.Tables[0].Rows[0][5]).Trim();
            ddlState.Text = Convert.ToString(ds.Tables[0].Rows[0][6]).Trim();
            txtZip.Text = Convert.ToString(ds.Tables[0].Rows[0][7]).Trim();
            txtPhone.Text = Convert.ToString(ds.Tables[0].Rows[0][8]).Trim();
            txtFax.Text = Convert.ToString(ds.Tables[0].Rows[0][9]).Trim();
            txtEmail.Text = Convert.ToString(ds.Tables[0].Rows[0][10]).Trim();
            txtURL.Text = Convert.ToString(ds.Tables[0].Rows[0][11]).Trim();
            txtDirections.Text = Convert.ToString(ds.Tables[0].Rows[0][12]).Trim();
            txtEmptyReturnFreeDays.Text = Convert.ToString(ds.Tables[0].Rows[0][13]).Trim();
            Grid1.Visible = true;
            Grid1.DeleteVisible = true;
            Grid1.EditVisible = true;
            Grid1.TableName = "eTn_ShippingContacts";
            Grid1.Primarykey = "bint_ContactId";
            Grid1.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
            Grid1.ColumnsList = "bint_ContactId;nvar_Name;" + CommonFunctions.PhoneQueryString("num_Mobile", "Mobile") + ";" +
                CommonFunctions.PhoneQueryString("num_WorkPhone", "WorkPhone") + ";" + CommonFunctions.PhoneQueryString("num_Fax", "Fax") + ";nvar_Email";
            Grid1.VisibleColumnsList = "nvar_Name;Mobile;WorkPhone;Fax";
            Grid1.VisibleHeadersList = "Name;Mobile;Work Phone;Fax";
            Grid1.DeleteTablesList = "eTn_ShippingContacts";
            Grid1.WhereClause = "[bint_ShippingId]=" + ID;
            Grid1.EditPage = "~/Manager/AddContact.aspx";
            Grid1.BindGrid(0);
            lnkBar.PagesList = "AddContact.aspx?" + ID + "New";
            lnkBar.EnableLinksList = "true";            
        }
        if (ds != null) { ds.Dispose(); ds = null; }

    }    
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Manager/SearchShipping.aspx");
    }
    protected void btnSave_Click(object sender, EventArgs e)
     {
        if (Page.IsValid)
        {
            string strQuery = "";
            if (string.Compare(Convert.ToString(ViewState["Name"]).Trim(), txtShippingLine.Text.Trim(), true, System.Globalization.CultureInfo.CurrentCulture) != 0)
            {
                if (Convert.ToInt32(DBClass.executeScalar("Select Count(bint_ShippingId) from eTn_Shipping Where nvar_ShippingLineName ='" + txtShippingLine.Text.Trim() + "'")) > 0)
                {
                    lblNameError.Visible = true;
                    return;
                }
            }
            if (ViewState["Mode"] != null)
            {
                int freeDays=0;
                if (!string.IsNullOrEmpty(txtEmptyReturnFreeDays.Text.Trim()))
                    freeDays = int.Parse(txtEmptyReturnFreeDays.Text.Trim());
                //SLine 2017 Enhancement for Office Location Starts
                long OfficeLocationId = 0;
                if (Session["OfficeLocationID"] != null)
                {
                    OfficeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
                }
                object[] objParams = new object[] { txtShippingLine.Text.Trim(),chkActive.Checked ? 1 : 0,txtPier.Text.Trim(),txtStreet.Text.Trim(),txtSuite.Text.Trim(),txtCity.Text.Trim(),ddlState.Text,
                                                        txtZip.Text.Trim(),Convert.ToDouble(txtPhone.Text),Convert.ToDouble(txtFax.Text),txtEmail.Text.Trim(),txtURL.Text.Trim(),txtDirections.Text.Trim(),freeDays, OfficeLocationId, Convert.ToInt64(ViewState["ID"]) };
                //SLine 2017 Enhancement for Office Location Ends
                System.Data.SqlClient.SqlParameter[] objsqlparams = null;
                if (ViewState["ID"] != null)
                {
                    if (Convert.ToString(ViewState["Mode"]).StartsWith("Edit", true, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Update_Shippings", objParams);
                        if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Update_Shippings", objsqlparams) > 0)
                        {
                            Response.Redirect("~/Manager/SearchShipping.aspx");
                        }
                    }
                }
                else if (Convert.ToString(ViewState["Mode"]).StartsWith("New", true, System.Globalization.CultureInfo.CurrentCulture))
                {
                   objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Add_Shippings", objParams);

                    if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Add_Shippings", objsqlparams) > 0)
                    {
                        ViewState["ID"] = Convert.ToInt64(objsqlparams[objsqlparams.Length - 1].Value);
                        ViewState["Mode"] = "Edit";
                        lnkBar.EnableLinksList = "true";
                        lnkBar.PagesList = "AddContact.aspx?" + Convert.ToString(ViewState["ID"]) + "New";
                        ViewState["Name"] = txtShippingLine.Text.Trim();
                    }
                }
            }
        }
    }
}
