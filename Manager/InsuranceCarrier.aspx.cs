using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class InsuranceCarrier : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Insurance Information";
        txtExpDateLInsure.DisplayError = false;
        txtExpDateCInsure.DisplayError = false;
        txtExpDateGInsure.DisplayError = false;
        txtExpDateWCInsure.DisplayError = false;
        txtExpDatePInsure.DisplayError = false;
        if (!IsPostBack)
        {
            try
            {
                if (Request.QueryString.Count > 0)
                {
                    ViewState["ID"] = Convert.ToInt64(Request.QueryString[0]);
                    GridBar1.HeaderText = DBClass.executeScalar("select nvar_CarrierName from eTn_Carrier where bint_CarrierId=" + Convert.ToString(ViewState["ID"]));
                    if(Convert.ToInt32(DBClass.executeScalar("Select count([bint_CarrierId]) from [eTn_CarrierInsurance] where [bint_CarrierId]=" + Convert.ToString(ViewState["ID"])))>0)
                    {
                        ViewState["Mode"]="Edit";
                        FillDetails();
                    }
                    else
                        ViewState["Mode"]="New";
                }
            }
            catch
            { }
        }

    }

    private void FillDetails()
    {
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "sp_GetCarrierInsuranceDetails", new object[] { Convert.ToInt64(ViewState["ID"])});
        if (ds.Tables[0].Rows.Count > 0)
        {
            txtCompanyLInsure.Text = Convert.ToString(ds.Tables[0].Rows[0][0]);
            txtLimitLInsure.Text = Convert.ToString(ds.Tables[0].Rows[0][1]);
            txtPhoneLInsure.Text = Convert.ToString(ds.Tables[0].Rows[0][2]);
            txtExpDateLInsure.Date = Convert.ToString(ds.Tables[0].Rows[0][3]);
            txtCompanyCInsure.Text = Convert.ToString(ds.Tables[0].Rows[0][4]);
            txtLimitCInsure.Text = Convert.ToString(ds.Tables[0].Rows[0][5]);
            txtPhoneCInsure.Text = Convert.ToString(ds.Tables[0].Rows[0][6]);
            txtExpDateCInsure.Date = Convert.ToString(ds.Tables[0].Rows[0][7]);
            txtCompanyGInsure.Text = Convert.ToString(ds.Tables[0].Rows[0][8]);
            txtLimitGInsure.Text = Convert.ToString(ds.Tables[0].Rows[0][9]);
            txtPhoneGInsure.Text = Convert.ToString(ds.Tables[0].Rows[0][10]);
            txtExpDateGInsure.Date = Convert.ToString(ds.Tables[0].Rows[0][11]);
            txtCompanyWCInsure.Text = Convert.ToString(ds.Tables[0].Rows[0][12]);
            txtLimitWCInsure.Text = Convert.ToString(ds.Tables[0].Rows[0][13]);
            txtPhoneWCInsure.Text = Convert.ToString(ds.Tables[0].Rows[0][14]);
            txtExpDateWCInsure.Date = Convert.ToString(ds.Tables[0].Rows[0][15]);
            txtCompanyPInsure.Text = Convert.ToString(ds.Tables[0].Rows[0][16]);
            txtLimitPInsure.Text = Convert.ToString(ds.Tables[0].Rows[0][17]);
            txtPhonePInsure.Text = Convert.ToString(ds.Tables[0].Rows[0][18]);
            txtExpDatePInsure.Date = Convert.ToString(ds.Tables[0].Rows[0][19]);
        }
        if (ds != null) { ds.Dispose(); ds = null; }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (ViewState["ID"] != null)
            Response.Redirect("~/Manager/NewCarrier.aspx?" + Convert.ToString(ViewState["ID"]));
        else
            Response.Redirect("~/Manager/NewCarrier.aspx");
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ViewState["ID"] != null)
        {
            if (!txtExpDateLInsure.IsValidDate)
                return;
              if (!txtExpDatePInsure.IsValidDate)
                return;
              if (!txtExpDateCInsure.IsValidDate)
                return;
              if (!txtExpDateGInsure.IsValidDate)
                return;
              if (!txtExpDateWCInsure.IsValidDate)
                return;

            string strAction = "";
            if (string.Compare(Convert.ToString(ViewState["Mode"]),"New", true, System.Globalization.CultureInfo.CurrentCulture)==0)
                strAction = "I";
            else if (string.Compare(Convert.ToString(ViewState["Mode"]), "Edit", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                strAction = "U";
            checkNulls();
            object[] objParams = new object[]{Convert.ToInt64(ViewState["ID"]),txtCompanyLInsure.Text.Trim(),txtLimitLInsure.Text.Trim(),txtPhoneLInsure.Text.Trim(),CommonFunctions.CheckDateTimeNull(txtExpDateLInsure.Date),
            txtCompanyCInsure.Text.Trim(), txtLimitCInsure.Text.Trim(),txtPhoneCInsure.Text.Trim(), CommonFunctions.CheckDateTimeNull(txtExpDateCInsure.Date),
            txtCompanyGInsure.Text.Trim(),txtLimitGInsure.Text.Trim(), txtPhoneGInsure.Text.Trim(), CommonFunctions.CheckDateTimeNull(txtExpDateGInsure.Date),
            txtCompanyWCInsure.Text.Trim(),txtLimitWCInsure.Text.Trim(), txtPhoneWCInsure.Text.Trim(), CommonFunctions.CheckDateTimeNull(txtExpDateWCInsure.Date),
            txtCompanyPInsure.Text.Trim(), txtLimitPInsure.Text.Trim(), txtPhonePInsure.Text.Trim(), CommonFunctions.CheckDateTimeNull(txtExpDatePInsure.Date),strAction};
            if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_insert_update_CarrierInusurance", objParams) > 0)
            {
                objParams = null;
                Response.Redirect("~/Manager/NewCarrier.aspx?" + Convert.ToString(ViewState["ID"]));
            }
            objParams = null;
        }
    }

    private void checkNulls()
    {
        if (txtLimitLInsure.Text.Trim().Length == 0)
            txtLimitLInsure.Text = "0";
        if (txtLimitCInsure.Text.Trim().Length == 0)
            txtLimitCInsure.Text = "0";
        if (txtLimitGInsure.Text.Trim().Length == 0)
            txtLimitGInsure.Text = "0";
        if (txtLimitWCInsure.Text.Trim().Length == 0)
            txtLimitWCInsure.Text = "0";
        if (txtLimitPInsure.Text.Trim().Length == 0)
            txtLimitPInsure.Text = "0";
    }
}
