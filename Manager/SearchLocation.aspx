<%@ Page AutoEventWireup="true" CodeFile="SearchLocation.aspx.cs" Inherits="SearchLocation"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/Grid.ascx" TagName="Grid" TagPrefix="uc3" %>

<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc2" %>
<%--<%@ Register Src="~/UserControls/DynamicList.ascx" TagName="DynamicList" TagPrefix="uc1" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="ContentTbl">
<tr valign="top">
    <td>
        <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                    <asp:Label ID="lblErrMessage" runat="server" Text="Company already exists." Visible="False" ForeColor="Red" ></asp:Label>
                </td>
            </tr>
        </table>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblbox1">
        <tr style="background-color:#FFFFFF;" valign="middle" >
            <td align="left" valign="middle" style="width:50%;">
             <asp:Label ID="lblText" runat="server">Select Company :&nbsp;</asp:Label>            
              <asp:DropDownList ID="ddlList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlList_SelectedIndexChanged" >
                </asp:DropDownList>
            </td>
            <td align="left" style="width: 50%;" valign="middle">    
                <asp:Label ID="lblOperations" runat="server" Visible="False">Select Operation :&nbsp;</asp:Label>
                <asp:DropDownList ID="dropOperations" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dropOperations_SelectedIndexChanged"
                    Visible="False">
                    <asp:ListItem>--Select--</asp:ListItem>
                    <asp:ListItem>Edit</asp:ListItem>
                    <asp:ListItem>Delete</asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="btnDelete" runat="server" CausesValidation="false" CssClass="btnstyle"
                    OnClick="btnDelete_Click" 
                    Text="Delete" UseSubmitBehavior="false" Visible="false" />
                <asp:TextBox ID="txtEdit" runat="server" Visible="false"></asp:TextBox>
                <asp:RequiredFieldValidator ID="ReqCompanyName" runat="server" ControlToValidate="txtEdit"
                    Display="Dynamic" ErrorMessage="Please enter a Company name.">*</asp:RequiredFieldValidator>
                <asp:Button ID="btnEdit" runat="server" CssClass="btnstyle" OnClick="btnEdit_Click"
                    Text="Edit" UseSubmitBehavior="False" Visible="false" />
            </td>
        </tr>
       </table>   
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
          </tr>
        </table> 
       <table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="left" width="100%">
                <uc2:GridBar ID="barCompany" runat="server" HeaderText="Locations" LinksList="Add New Company & Location;Add New Location" PagesList="NewLocation.aspx;NewLocation.aspx" Visible="true" />
            </td>
         </tr>   
         </table>  
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
          </tr>
        </table>
        <table cellpadding="0" cellspacing="0" width="100%">
          <tr>
            <td>
                <uc3:Grid ID="Grid1" runat="server" PageSize="5"  />
             </td>
          </tr>
       </table>
    </td>
  </tr>
</table>

</asp:Content>

