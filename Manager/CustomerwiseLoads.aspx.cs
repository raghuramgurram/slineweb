using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CustomerwiseLoads : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Customer Wise Loads";
        if (!IsPostBack)
        {
            FillLists();
        }
    }

    private void FillLists()
    {
        //ddlCustomers.DataSource = DBClass.returnDataTable("Select bint_CustomerId,nvar_CustomerName from eTn_Customer order by [nvar_CustomerName]");
         DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCustomerNames");
         if (ds.Tables.Count > 0)
         {
             ddlCustomers.DataSource = ds.Tables[0];
             ddlCustomers.DataValueField = "bint_CustomerId";
             ddlCustomers.DataTextField = "nvar_CustomerName";
             ddlCustomers.DataBind();
         }
        ddlCustomers.Items.Insert(0, new ListItem("All", "0"));
        ds.Tables.Clear();

        //dropStatus.DataSource = DBClass.returnDataTable("Select bint_LoadStatusId,nvar_LoadStatusDesc from eTn_LoadStatus");
        ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadStatusDetails");
        if (ds.Tables.Count > 0)
        {
            dropStatus.DataSource = ds.Tables[0];
            dropStatus.DataValueField = "bint_LoadStatusId";
            dropStatus.DataTextField = "nvar_LoadStatusDesc";
            dropStatus.DataBind();
        }
        if (dropStatus.Items.Count > 0)
        {
            dropStatus.Items.Remove(dropStatus.Items.FindByText("Delivered"));
            dropStatus.Items.Remove(dropStatus.Items.FindByText("Closed"));
            dropStatus.Items.Insert(1, new ListItem("All Transit Loads", dropStatus.Items.Count + 1.ToString()));
        }
        if (ds != null) { ds.Dispose(); ds = null; }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Session["SerchCriteria"] = null;
            Session["ReportParameters"] = null;
            Session["ReportName"] = null;
            Session["ReportParameters"] = ddlCustomers.SelectedValue + "~~^^^^~~" + dropStatus.SelectedItem.Text + "~~^^^^~~" + (string.IsNullOrWhiteSpace(txtDate.Date) ? null : txtDate.Date);
            Session["SerchCriteria"] = (string.IsNullOrWhiteSpace(txtDate.Date) ? string.Empty : "Delivery appt. date : " + txtDate.Date + "&nbsp;&nbsp;&nbsp;") + ((Convert.ToInt64(ddlCustomers.SelectedValue) > 0) ? "Customer name : " + ddlCustomers.SelectedItem.Text.Trim() : "All Customers ") + "&nbsp;&nbsp;&nbsp; Status : " + dropStatus.SelectedItem.Text;
            Session["ReportName"] = "GetCustomerwiseLoads";
            Response.Redirect("~/Manager/DisplayReport.aspx");
        }
    }
    //private void DisplayResport()
    //{
    //    DataSet ds;
    //    object[] objParams = new object[] { txtName.Text.Trim(),ddlList.SelectedItem.Value};
    //    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCustomerwiseLoads", objParams);
    //    string[] strColumnsList = new string[] { "Name", "Phone", "Fax", "Created" };
    //    string[] colWidths = new string[] { "50%", "20%", "20%", "10%" };

    //    objParams = null;
    //    if (ds.Tables.Count > 0)
    //    {
    //        DataTable dt = ds.Tables[0];
    //        CommonFunctions.FormatDataTable(ref dt);
    //        lblDisplay.Text=CommonFunctions.ReportDisplay(dt, strColumnsList, colWidths);
    //        if (dt != null) { dt.Dispose(); dt = null; }
    //    }
    //    else
    //    {
    //        lblDisplay.Text = "<table width=100% border=0 cellpadding=0 cellspacing=0 id=ContentTbl><tr valign=top><td>" +
    //            "<table border=0 width=100% height=200px border=0 cellpadding=0 cellspacing=0 align=center valign=middle><tr><td width=100% align=center valign=middle>" +
    //            "<b><font color=blue>No Records To Display.<font></b></td></tr></table><br/><br/><br/></td></tr></table>";
    //    }
    //    if (ds != null) { ds.Dispose(); ds = null; }
    //    strColumnsList = null; colWidths = null;       
    //}    
   
}
