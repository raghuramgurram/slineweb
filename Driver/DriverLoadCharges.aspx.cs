using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class DriverLoadCharges : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Load Charges";
        if (!IsPostBack)
        {
            string BackPage = "";
            if (Request.QueryString.Count > 0)
            {
                try
                {
                    string[] strValues = Convert.ToString(Request.QueryString[0]).Split('^');
                    ViewState["LoadId"] = Convert.ToInt64(strValues[0]);
                    LoadPayments1.LoadId = Convert.ToString(ViewState["LoadId"]);
                    if(strValues.Length>1)
                    LoadPayments1.PlayerId = Convert.ToString(strValues[1]);
                    lblBarText.Text ="Load: "+ Convert.ToString(ViewState["LoadId"]);
                    if (Session["RedirectURL"] != null && ViewState["LoadId"] != null)
                    {
                        BackPage = Convert.ToString(Session["RedirectURL"]);
                        Session["RedirectURL"] = null;
                        LoadPayments1.RedirectBackURL = BackPage + Convert.ToInt64(ViewState["LoadId"]);
                    }
                    else 
                        LoadPayments1.RedirectBackURL = Convert.ToString(("~/Driver/DriverTrackLoad.aspx?" + Convert.ToInt64(ViewState["LoadId"])));
                }
                catch
                {
                }
            }
        }
    }
}
