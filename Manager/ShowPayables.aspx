<%@ Page AutoEventWireup="true" CodeFile="ShowPayables.aspx.cs" Inherits="showpayables"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblform1">
          <tr>
            <td>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="504px" />
            
            </td>
          </tr>
        </table>
      <table width="100%" border="0" cellpadding="3" cellspacing="0" id="tblbox1">
          <tr>
            <td align="left" bgcolor="#FFFFFF">Select
                <asp:Label ID="lblAssigned" runat="server" />
                <asp:DropDownList ID="ddldriver" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddldriver_SelectedIndexChanged">
                    <asp:ListItem>---select---</asp:ListItem>
                </asp:DropDownList></td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="../Images/pix.gif" alt="" width="1" height="3" /></td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="3" cellspacing="0" id="tblform">
          <tr>
            <th colspan="6">Payables ($) </th>
          </tr>
          <tr>
            <td width="15%" align="right">
                &nbsp;Charge   : </td>
            <td width="20%">
                <asp:TextBox ID="txtcharges" runat="server" MaxLength="18"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtcharges"
                    Display="Dynamic" ErrorMessage="Please enter a valid charge." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
            <td width="15%" align="right">Chassis Split : </td>
            <td style="width: 144px">
                <asp:TextBox ID="txtchassis" runat="server" MaxLength="18"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtchassis"
                    Display="Dynamic" ErrorMessage="Please enter a valid chassis split." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
            <td align="right" style="width: 148px">Heavy & Light Scale  Charges : </td>
            <td style="width: 138px">
                <asp:TextBox ID="txtheavy" runat="server" MaxLength="18"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server"
                    ControlToValidate="txtheavy" Display="Dynamic" ErrorMessage="Please enter a valid heavy charges."
                    ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
          </tr>
          <tr>
            <td align="right">Advance Paid (-) :</td>
            <td>
                <asp:TextBox ID="txtadvancepaid" runat="server" MaxLength="18"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtadvancepaid"
                    Display="Dynamic" ErrorMessage="Please enter a valid Advance paid." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
            <td align="right">Chassis Rent : </td>
            <td style="width: 144px">
                <asp:TextBox ID="txtchassisrent" runat="server" MaxLength="18"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                    ControlToValidate="txtchassisrent" Display="Dynamic" ErrorMessage="Please enter a valid chassis rent."
                    ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
            <td align="right" style="width: 148px">Scale Ticket Charges : </td>
            <td style="width: 138px">
                <asp:TextBox ID="txtscaleticket" runat="server" MaxLength="18"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server"
                    ControlToValidate="txtscaleticket" Display="Dynamic" ErrorMessage="Please enter a valid scale ticket charges."
                    ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
          </tr>
          <tr>
            <td align="right" style="height: 26px">Dry run : </td>
            <td style="height: 26px">
                <asp:TextBox ID="txtdryrun" runat="server" MaxLength="18"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtdryrun"
                    Display="Dynamic" ErrorMessage="Please enter a valid dry run charges." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
            <td align="right" style="height: 26px">Pallets : </td>
            <td style="width: 144px; height: 26px">
                <asp:TextBox ID="txtpallets" runat="server" MaxLength="18"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                    ControlToValidate="txtpallets" Display="Dynamic" ErrorMessage="Please enter a valid pallets."
                    ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
            <td align="right" style="height: 26px; width: 148px;">Paid to another driver (-) : </td>
            <td style="width: 138px; height: 26px">
                <asp:TextBox ID="txtPaidAnotherDriver" runat="server" MaxLength="18"></asp:TextBox><asp:RegularExpressionValidator
                    ID="RegularExpressionValidator24" runat="server" ControlToValidate="txtPaidAnotherDriver"
                    Display="Dynamic" ErrorMessage="Please enter a valid paid to another driver." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
          </tr>
          <tr>
            <td align="right">Fuel Surcharge (auto calc%) : </td>
            <td>
                <asp:TextBox ID="txtfuel" runat="server" Width="30px" MaxLength="18"></asp:TextBox>&nbsp;
                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtfuel"
                    Display="Dynamic" ErrorMessage="Please enter a valid fuel charges." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator><strong>(OR)</strong> Amount ($) :
                <asp:TextBox ID="txtamount" runat="server" Width="30px" MaxLength="18"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator23" runat="server"
                    ControlToValidate="txtamount" Display="Dynamic" ErrorMessage="Please enter a valid  Fuel surcharge."
                    ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
            <td align="right">Container wash out : <br />
            </td>
            <td style="width: 144px">
                
                <asp:TextBox ID="txtcontainer" runat="server" MaxLength="18"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server"
                    ControlToValidate="txtcontainer" Display="Dynamic" ErrorMessage="Please enter a valid container charges."
                    ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
            <td align="right" style="width: 148px">Toll Charges : </td>
            <td style="width: 138px">
                <asp:TextBox ID="txtTollCharges" runat="server" MaxLength="18"></asp:TextBox><asp:RegularExpressionValidator
                    ID="RegularExpressionValidator25" runat="server" ControlToValidate="txtTollCharges"
                    Display="Dynamic" ErrorMessage="Please enter a valid toll charges." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
          </tr>
          <tr>
            <td align="right">Pier Termination :</td>
            <td>
                <asp:TextBox ID="txtpier" runat="server" MaxLength="18"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtpier"
                    Display="Dynamic" ErrorMessage="Please enter a valid Pier Termination." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
            <td align="right">Yard storage : <br /></td>
            <td style="width: 144px">
               
                <asp:TextBox ID="txtyardstorage" runat="server" MaxLength="18"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server"
                    ControlToValidate="txtyardstorage" Display="Dynamic" ErrorMessage="Please enter a valid yash storage."
                    ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
            <td align="right" style="width: 148px">Flip Charges : </td>
            <td style="width: 138px">
                <asp:TextBox ID="txtSlipCharges" runat="server" MaxLength="18"></asp:TextBox><asp:RegularExpressionValidator
                    ID="RegularExpressionValidator26" runat="server" ControlToValidate="txtSlipCharges"
                    Display="Dynamic" ErrorMessage="Please enter a valid slip charges." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
          </tr>
          <tr>
            <td align="right">Extra Stops : </td>
            <td>
                
                <asp:TextBox ID="txtextrastops" runat="server" MaxLength="18"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtextrastops"
                    Display="Dynamic" ErrorMessage="Please enter a valid extra stop charges." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
            <td align="right">Ramp pull charges : <br /></td>
            <td style="width: 144px">
              
                <asp:TextBox ID="txtramp" runat="server" MaxLength="18"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server"
                    ControlToValidate="txtramp" Display="Dynamic" ErrorMessage="Please enter a valid ramp charges."
                    ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
            <td align="right" style="width: 148px">Repair Charges : </td>
            <td style="width: 138px">
                <asp:TextBox ID="txtRepairCharges" runat="server" MaxLength="18"></asp:TextBox><asp:RegularExpressionValidator
                    ID="RegularExpressionValidator27" runat="server" ControlToValidate="txtRepairCharges"
                    Display="Dynamic" ErrorMessage="Please enter a valid repair charges." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
          </tr>
          <tr>
            <td align="right" width="15%">Lumper Charges : </td>
            <td>
              
                <asp:TextBox ID="txtlumper" runat="server" MaxLength="18"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtothercharges"
                    Display="Dynamic" ErrorMessage="Please enter a valid lumper charges." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
            <td align="right">Triaxle charge : </td>
            <td style="width: 144px">
                
                <asp:TextBox ID="txttriaxle" runat="server" MaxLength="18"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                    ControlToValidate="txttriaxle" Display="Dynamic" ErrorMessage="Please enter a valid triaxle charges."
                    ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                    
                      <td align="right" style="width: 148px">Hazmat Charges : </td>
            <td style="width: 144px">
                <asp:TextBox ID="txtcharges4" runat="server" MaxLength="18"></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator22" runat="server"
                    ControlToValidate="txtcharges4" Display="Dynamic" ErrorMessage="Please enter a valid other charges4."
                    ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                    
           
          </tr>
          <tr>
            <td align="right">Detention Charges : </td>
            <td width="15%">
               <asp:TextBox ID="txtdetention" runat="server" MaxLength="18"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtdetention"
                    Display="Dynamic" ErrorMessage="Please enter a valid detention charges." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
            <td align="right" width="15%">Trans load charges : </td>
            <td style="width: 144px">
                <asp:TextBox ID="txttrans" runat="server" MaxLength="18"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator16" runat="server"
                    ControlToValidate="txttrans" Display="Dynamic" ErrorMessage="Please enter a valid trans load charges."
                    ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                    
                    
                     <td align="right" style="width: 148px">Other Charges 1 : </td>
            <td style="width: 138px">
               
                <asp:TextBox ID="txtothercharges" runat="server" MaxLength="18"></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator19" runat="server"
                    ControlToValidate="txtothercharges" Display="Dynamic" ErrorMessage="Please enter a valid other charges."
                    ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
                    
          
          </tr>
          
          
          <tr>
           
          
            <td align="right" >Other Charges 2 : </td>
            <td style="width: 138px">
                <strong>
                
                <asp:TextBox ID="txtother" runat="server" MaxLength="18"></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator20" runat="server"
                    ControlToValidate="txtother" Display="Dynamic" ErrorMessage="Please enter a valid other charges2."
                    ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></strong></td>
                     <td align="right" width="15%">Other Charges 3 : </td>
            <td>
                <asp:TextBox ID="txtcharges3" runat="server" MaxLength="18"></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator21" runat="server"
                    ControlToValidate="txtcharges3" Display="Dynamic" ErrorMessage="Please enter a valid other charges3."
                    ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
          
          
            <td align="right" style="width: 148px">Other Charges Reason : </td>
            <td style="width: 138px">
                
                <asp:TextBox ID="txtreason" runat="server" MaxLength="100"></asp:TextBox></td>
          </tr>
          <tr>
            <td align="right">Check Number : </td>
            <td width="15%">
               <asp:TextBox ID="txtCheckNumber" runat="server" MaxLength="50"></asp:TextBox>
                </td>
            <td align="right" width="15%">
                Charges per Diem:</td>
            <td style="width: 144px">
                <asp:TextBox ID="txtChargesPerDiem" runat="server" MaxLength="50"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator28" runat="server" ControlToValidate="txtChargesPerDiem"
                    Display="Dynamic" ErrorMessage="Please enter a valid detention charges." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
            <td align="right" style="width: 148px"><strong><asp:Label ID="lblTotalText" runat="server" Text="Total :"></asp:Label> 
            </strong>
            </td>
            <td style="width: 138px">
                <strong>&nbsp;<asp:Label ID="lblTotal" runat="server" /></strong></td>
          </tr>
          
          
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="../Images/pix.gif" alt="" width="1" height="2" /></td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
          <tr>
            <td align="right" style="height: 23px">
                <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="btnstyle" OnClick="btnSave_Click"/>&nbsp;<asp:Button ID="btncancel"
                    runat="server" Text="Cancel" CssClass="btnstyle" OnClick="btnCancle_Click" CausesValidation="False" UseSubmitBehavior="False"/></td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="../Images/pix.gif" alt="" width="1" height="3" /></td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td style="height: 28px"> 
                <uc1:GridBar ID="GridBar1" runat="server" Visible="true" />
            </td>
          </tr>
        </table>        
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="../Images/pix.gif" alt="" width="1" height="3" /></td>
          </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="tblform">
            <tr>
                <td id="row">
                    <asp:Label ID="lblDriver" runat="server" Text=""></asp:Label></td>
            </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
            </tr>
        </table>
    </td>
       </tr>
     </table>      
</asp:Content>

