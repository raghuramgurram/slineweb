using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class logintop : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblHeadText.Text = Convert.ToString(Session["TopHeaderText"]);
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        lblHeadText.Text = Convert.ToString(Session["TopHeaderText"]);
    }
}
