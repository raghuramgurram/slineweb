using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public partial class Manager_DriverWisePrint : System.Web.UI.Page
{   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataSet ds = new DataSet();
            string[] strColumnsList = null;
            string[] colWidths = null;
            object[] objParams = null;
            if (Session["DriverWiseReport"] != null && !string.IsNullOrEmpty(Session["DriverWiseReport"].ToString().Trim()))
            {

                List<DriverWiseReport> driverWiseReportList = new List<DriverWiseReport>();
                string driverIdandNames = null;
                driverIdandNames = Session["DriverWiseReport"].ToString().Trim();
                Session["TopHeaderText"] = "Payable Entries";
                //SLine 2017 Enhancement for Office Location Starts
                long officeLocationId = 0;
                if (Session["OfficeLocationID"] != null)
                {
                    officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
                }
                //SLine 2017 Enhancement for Office Location Ends
                foreach (string idAndName in driverIdandNames.Split('~'))
                {

                    string id = idAndName.Split('`')[0].Trim();
                    string Name = idAndName.Split('`')[1].Trim();
                    objParams = (Convert.ToString(Session["ReportParameters"]) + "~~^^^^~~" + (" and [eTn_AssignDriver].[bint_EmployeeId]=" + id + "") + "~~^^^^~~" + id + "~~^^^^~~" + officeLocationId).Trim().Replace(":", " like ").Replace("~~^^^^~~", "^").Split('^');
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], Convert.ToString(Session["PayableSPName"]), objParams);
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        DriverWiseReport driverWiseReport = new DriverWiseReport();
                        strColumnsList = new string[] { "Load", "Container", "Invoice/Tag", "Driver/Carrier Name", "Total", "Total Report", "From", "To", "QB Status" };
                        colWidths = new string[] { "60px", "100px", "60px", "130px", "50px", "130px", "130px", "130px", "60px" };


                        string[] strPayments = Convert.ToString(ds.Tables[1].Rows[0][0]).Replace("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "^").Split('^');
                        if (strPayments.Length == 3)
                        {
                            driverWiseReport.Total = strPayments[0];
                            driverWiseReport.Paid = strPayments[1];
                            driverWiseReport.Pending = strPayments[2];
                            driverWiseReport.Count = ds.Tables[0].Rows.Count.ToString();
                        }
                        driverWiseReport.SearchCriteria = "<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + (Convert.ToString(Session["SerchCriteria"]) + " Driver name : " + Name);
                        driverWiseReport.Header = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Payment  settlement Sheet<br/>";

                        objParams = null;

                        DataTable dt = ds.Tables[0];
                        FormatDataTable(ref dt);
                        driverWiseReport.InnerHtml = ReportDisplay(dt, strColumnsList, colWidths);

                        driverWiseReport.Email = getEmailId(Convert.ToInt32(id));
                        if (dt != null) { dt.Dispose(); dt = null; }
                        driverWiseReportList.Add(driverWiseReport);
                    }
                    if (ds != null) { ds.Dispose(); ds = null; }
                    strColumnsList = null; colWidths = null;
                }

                rptDrivers.DataSource = driverWiseReportList;
                rptDrivers.DataBind();
                lblNorecords.Visible = (driverWiseReportList.Count == 0);
            }
        }
    }

    protected void FormatDataTable(ref DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    if (Convert.ToString(dt.Rows[i][j]).Contains("^^^^"))
                    {
                        if (Convert.ToString(Session["ReportName"]) == "GetPayableEntries")
                        {
                            if (Convert.ToInt32(dt.Rows[i]["LoadStatus"]) != 12)
                            {
                                if (j == 4)
                                {
                                    dt.Rows[i][j] = FormatMultipleRowColumnsred(Convert.ToString(dt.Rows[i][j]).Trim());
                                }
                                else
                                {
                                    dt.Rows[i][j] = FormatMultipleRowColumns(Convert.ToString(dt.Rows[i][j]).Trim());
                                }
                            }
                            else
                            {
                                dt.Rows[i][j] = FormatMultipleRowColumns(Convert.ToString(dt.Rows[i][j]).Trim());
                            }

                        }
                        else
                        {
                            dt.Rows[i][j] = FormatMultipleRowColumns(Convert.ToString(dt.Rows[i][j]).Trim());
                        }
                    }
                }
            }
        }
    }

    public string FormatMultipleRowColumns(string strColValue)
    {
        string strFormat = "";
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0)
        {
            strFormat = "<table cellpadding=0 cellspacing=0 border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;>";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (i == 0)
                    strFormat += "<tr ><td  width=100% align=left valign=middle style=color:black;font-size:12;height:90px;>" + strMuls[i].Trim() + "</td></tr>";
                else
                {
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:black;font-size:12;height:90px;>" + strMuls[i].Trim() + "</td></tr>";
                }
            }
            strFormat += "</table>";
        }
        strMuls = null;
        return strFormat;
    }

    public string FormatMultipleRowColumnsred(string strColValue)
    {
        string strFormat = "";
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0)
        {
            strFormat = "<table cellpadding=0 cellspacing=0 border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;>";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (i == 0)
                    strFormat += "<tr ><td  width=100% align=left valign=middle style=color:black;font-size:12;height:90px;><span style='color:red;'>" + strMuls[i].Trim() + "</span></td></tr>";
                else
                {
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:black;font-size:12;height:90px;><span style='color:red;'>" + strMuls[i].Trim() + "</span></td></tr>";
                }
            }
            strFormat += "</table>";
        }
        strMuls = null;
        return strFormat;
    }

    protected string ReportDisplay(DataTable dt, string[] strColumnsList, string[] colWidths)
    {
        string strDisplay=string.Empty;
        if (dt.Rows.Count > 0)
        {
            string strColProperties = " align=left valign=middle style=font-size:12px;border-top-width:0px;border-right-width:0px;border-color:black;padding-left:3px;padding-right:3px;background-color:#F3F8FC;font-family:Arial,Helvetica,sans-serif;";
            string strHeadProperties = " align=left valign=middle style=font-size:14px;border-top-width:0px;border-right-width:0px;border-color:black;background-color:lightblue;padding-left:4px;padding-right:4px;font-family:Arial,Helvetica,sans-serif;";
            string strTableProperties = " border=1 cellpading=0 cellspacing=0 width=100% align=center valign=top style=border-color:black;border-left-width:0px;border-bottom-width:0px;";//padding-left:2px;padding-right:2px;";
            strDisplay = "<table width=100% border=0 cellpadding=0 cellspacing=0 id=ContentTbl><tr valign=top><td>" +
                "<table " + strTableProperties + ">";
            for (int i = 0; i < strColumnsList.Length; i++)
            {
                if (i == 0)
                    strDisplay += "<tr>";
                strDisplay += "<th " + strHeadProperties + "width:" + colWidths[i] + "; align=left >" + strColumnsList[i] + "</th>";
                if (i == strColumnsList.Length - 1)
                    strDisplay += "</tr>";
            }
            if (Convert.ToString(Session["ReportName"]) == "GetPayableEntries")
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    strDisplay += "<tr >";
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        if (Convert.ToInt32(dt.Rows[i]["LoadStatus"]) != 12)
                        {
                            int x = 8;
                            if (dt.Columns.Count > 9)
                            {
                                x = 9;
                            }
                            if (j != x)
                            {
                                if (j == 0 || j == 4)
                                {
                                    strDisplay += "<td" + strColProperties + ">";
                                    strDisplay += "<span style='color:red;'>" + Convert.ToString(dt.Rows[i][j]) + "</span>";
                                    strDisplay += "</td>";
                                }
                                else
                                {
                                    strDisplay += "<td" + strColProperties + ">";
                                    strDisplay += Convert.ToString(dt.Rows[i][j]);
                                    strDisplay += "</td>";
                                }
                            }
                        }
                        else
                        {
                            int x = 8;
                            if (dt.Columns.Count > 9)
                            {
                                x = 9;
                            }
                            if (j != x)
                            {
                                strDisplay += "<td" + strColProperties + ">";
                                strDisplay += Convert.ToString(dt.Rows[i][j]);
                                strDisplay += "</td>";
                            }
                        }
                    }
                    strDisplay += "</tr>";
                }
            }
            else
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    strDisplay += "<tr >";
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        strDisplay += "<td" + strColProperties + ">";
                        strDisplay += Convert.ToString(dt.Rows[i][j]);
                        strDisplay += "</td>";
                    }
                    strDisplay += "</tr>";
                }
            }
            strDisplay += "</table>" +
                "</td></tr></table>";
        }
        return strDisplay;
    }

    protected void btnSendEmail_Click(object sender, EventArgs e)
    {
        var btnSendEmail = sender as Button;
        string toEmailAddress = btnSendEmail.CommandArgument;
        if (!string.IsNullOrEmpty(toEmailAddress))
        {
            RepeaterItem rp1 = ((RepeaterItem)(btnSendEmail.NamingContainer));
            Label lblDisplay = rp1.FindControl("lblDisplay") as Label;
            Label lblHeader = rp1.FindControl("lblHeader") as Label;
            Label lblSearchCriteria = rp1.FindControl("lblSearchCriteria") as Label;
            HtmlTable tblResult = rp1.FindControl("tblResult") as HtmlTable;
            Label lblPaymentTotal = rp1.FindControl("lblPaymentTotal") as Label;
            Label lblPaid = rp1.FindControl("lblPaid") as Label;
            Label lblPending = rp1.FindControl("lblPending") as Label;
            Label lblcount = rp1.FindControl("lblcount") as Label;
            Label lblEmailNotAvailable = rp1.FindControl("lblEmailNotAvailable") as Label; 
            string subject = string.Empty;
            string emailBody = string.Empty;
            string reportContent = lblDisplay.Text;

            string footerHTMReplaced = getFooterhtmlReplaced(lblPaymentTotal.Text, lblPaid.Text, lblPending.Text, lblcount.Text);

            subject = StripHTML(lblHeader.Text + " - " + lblSearchCriteria.Text);

            if (!string.IsNullOrEmpty(reportContent) && reportContent.Length > 1)
            {
                emailBody = "<span style='color:blue;font-weight:bold;'>" + lblHeader.Text + " </br>" + lblSearchCriteria.Text + "</span></br></br>" + reportContent + footerHTMReplaced;
                CommonFunctions.SendEmail(GetFromXML.CcEmail, toEmailAddress, GetFromXML.CcEmail, "", subject, emailBody.ToString(), null, null);
                btnSendEmail.Visible = false;
                lblEmailNotAvailable.Text = "Email Sent Successfully";
                lblEmailNotAvailable.Attributes.Remove("style");
                lblEmailNotAvailable.Attributes.Add("Style", "color: green; font - weight:bold");
                lblEmailNotAvailable.Visible = true;

            }
        }
        updpage.Update();
    }
    protected string getFooterhtmlReplaced(string lblPaymentTotal, string lblPaid, string lblPending, string lblcount)
    {
        string footerHTML = @" <table width='100%'>
            <tr>
                <td colspan='2' align='right' Style='font-family: Arial, sans-serif; font-size: 12px;'>"
                  + lblPaymentTotal +
                @"</td>
            </tr>
            <tr>
                <td align='right' colspan='2' Style='font-family: Arial, sans-serif; font-size: 12px;'>"
                  + lblPaid +
                @"</td>
            </tr>
            <tr>
                <td align='right' colspan='2' Style='font-family: Arial, sans-serif; font-size: 12px;'>"
                  + lblPending +
                @"</td>
            </tr>
        
            <tr>
                <td colspan='2' Style='font-family: Arial, sans-serif; font-size: 14px;color:Red;font-weight:bold;'>
                    Total  deduction $ ______________________________
                </td>
            </tr>
            <tr>
                <td colspan='2' Style='font-family: Arial, sans-serif; font-size: 14px;color:Red;font-weight:bold;'>
                    Paid  check # __________________________________
                </td>
            </tr>
            <tr>
                <td colspan='2' Style='font-family: Arial, sans-serif; font-size: 14px;color:Red;font-weight:bold;'>
                   Total  Loads  on this sheet : "
                  + lblcount +
                @"</td>
            </tr>
        </table>";
        return footerHTML;
    }
    public static string StripHTML(string input)
    {
        string noHTML = Regex.Replace(input, @"<[^>]+>|&nbsp;", "").Trim();
        string noHTMLNormalised = Regex.Replace(noHTML, @"\s{2,}", " ");
        return noHTMLNormalised;
    }
    private string getEmailId(long employeeId)
    {
        string emailAddress = string.Empty;
        object[] param = new object[] { employeeId };
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "Get_Employee_Details", param);
        if (ds.Tables.Count > 0)
        {
            emailAddress = Convert.ToString(ds.Tables[0].Rows[0][15]).Trim();
        }
        return emailAddress;
    }
}
public class DriverWiseReport
{
    private string innerHtml;

    public string InnerHtml
    {
        get { return innerHtml; }
        set { innerHtml = value; }
    }

    private string header;

    public string Header
    {
        get { return header; }
        set { header = value; }
    }

    private string searchCriteria;

    public string SearchCriteria
    {
        get { return searchCriteria; }
        set { searchCriteria = value; }
    }

    private string total;

    public string Total
    {
        get { return total; }
        set { total = value; }
    }

    private string paid;

    public string Paid
    {
        get { return paid; }
        set { paid = value; }
    }

    private string pending;

    public string Pending
    {
        get { return pending; }
        set { pending = value; }
    }

    private string count;

    public string Count
    {
        get { return count; }
        set { count = value; }
    }
    private string email;
    public string Email
    {
        get { return email; }
        set { email = value; }
    }
}
