using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class insuranceequipment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Insurance Information";
        txteliaility.DisplayError = false;
        txtecargo.DisplayError = false;
        txtephysical.DisplayError = false;
        if (!IsPostBack)
        {
            try
            {
                if (Request.QueryString.Count > 0)
                {
                    ViewState["ID"] = Convert.ToInt64(Request.QueryString[0]);
                    GridBar1.HeaderText = DBClass.executeScalar("select [nvar_EquipmentName] from [eTn_Equipment] where bint_EquipmentId=" + Convert.ToInt64(ViewState["ID"]));
                    if (Convert.ToInt32(DBClass.executeScalar("Select count([bint_EquipmentId]) from [eTn_EquipInsurance] where [bint_EquipmentId]=" + Convert.ToString(ViewState["ID"]))) > 0)
                    {
                        ViewState["Mode"] = "Edit";
                        FillDetails();
                    }
                    else
                        ViewState["Mode"] = "New";
                }
            }
            catch
            { }
        }
    }
    private void FillDetails()
    {
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "sp_GetEquipmentInsuranceDetails", new object[] { Convert.ToInt64(ViewState["ID"]) });
        if (ds.Tables[0].Rows.Count > 0)
        {
            txtcliability.Text = Convert.ToString(ds.Tables[0].Rows[0][0]);
            txtpliability.Text = Convert.ToString(ds.Tables[0].Rows[0][1]);
            txtlliability.Text = Convert.ToString(ds.Tables[0].Rows[0][2]);
            txtbliability.Text = Convert.ToString(ds.Tables[0].Rows[0][3]);
            txtphliabity.Text = Convert.ToString(ds.Tables[0].Rows[0][4]);
            txtfliability.Text = Convert.ToString(ds.Tables[0].Rows[0][5]);
            txtemailliabi.Text = Convert.ToString(ds.Tables[0].Rows[0][6]);
            txteliaility.Date = Convert.ToString(ds.Tables[0].Rows[0][7]);
            txtccargo.Text = Convert.ToString(ds.Tables[0].Rows[0][8]);
            txtpcargo.Text = Convert.ToString(ds.Tables[0].Rows[0][9]);
            txtlcargo.Text = Convert.ToString(ds.Tables[0].Rows[0][10]);
            txtbcargo.Text = Convert.ToString(ds.Tables[0].Rows[0][11]);
            txtphcargo.Text = Convert.ToString(ds.Tables[0].Rows[0][12]);
            txtfcargo.Text = Convert.ToString(ds.Tables[0].Rows[0][13]);
            txtemailcargo.Text = Convert.ToString(ds.Tables[0].Rows[0][14]);
            txtecargo.Date = Convert.ToString(ds.Tables[0].Rows[0][15]);
            txtcphysical.Text = Convert.ToString(ds.Tables[0].Rows[0][16]);
            txtpphysical.Text = Convert.ToString(ds.Tables[0].Rows[0][17]);
            txtlphysical.Text = Convert.ToString(ds.Tables[0].Rows[0][18]);
            txtbphysical.Text = Convert.ToString(ds.Tables[0].Rows[0][19]);
            txtphphysical.Text = Convert.ToString(ds.Tables[0].Rows[0][20]);
            txtfphysical.Text = Convert.ToString(ds.Tables[0].Rows[0][21]);
            txtemailphy.Text = Convert.ToString(ds.Tables[0].Rows[0][22]);
            txtephysical.Date = Convert.ToString(ds.Tables[0].Rows[0][23]);
        }
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        if (ViewState["ID"] != null)
            Response.Redirect("~/Manager/NewEquipment.aspx?" + Convert.ToInt64(ViewState["ID"]));
        else
            Response.Redirect("~/Manager/NewEquipment.aspx");
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (ViewState["ID"] != null)
        {
            txteliaility.DisplayError = false;
            txtecargo.DisplayError = false;
            txtephysical.DisplayError = false;
            if (!txteliaility.IsValidDate)
                return;
            if (!txtecargo.IsValidDate)
                return;
            if (!txtephysical.IsValidDate)
                return;

            string strAction = "";
            if (string.Compare(Convert.ToString(ViewState["Mode"]), "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                strAction = "I";
            else if (string.Compare(Convert.ToString(ViewState["Mode"]), "Edit", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                strAction = "U";
            checkNulls();
            object[] objParams = new object[] { Convert.ToInt64(ViewState["ID"]),
                txtcliability.Text.Trim(),txtpliability.Text.Trim(),txtlliability.Text.Trim(),txtbliability.Text.Trim(),txtphliabity.Text.Trim(),txtfliability.Text.Trim(),txtemailliabi.Text.Trim(),CommonFunctions.CheckDateTimeNull(txteliaility.Date),
            txtccargo.Text,txtpcargo.Text.Trim().Trim(),txtlcargo.Text,txtbcargo.Text.Trim(),txtphcargo.Text.Trim(),txtfcargo.Text.Trim(),txtemailcargo.Text.Trim(),CommonFunctions.CheckDateTimeNull(txtecargo.Date),
            txtcphysical.Text.Trim(),txtpphysical.Text.Trim(),txtlphysical.Text.Trim(),txtbphysical.Text.Trim(),txtphphysical.Text.Trim(),txtfphysical.Text.Trim(),txtemailphy.Text.Trim(),CommonFunctions.CheckDateTimeNull(txtephysical.Date),strAction};

            if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_insert_update_EquipInsurance", objParams) > 0)
            {
                objParams = null;
                Response.Redirect("~/Manager/NewEquipment.aspx?" + Convert.ToString(ViewState["ID"]));
            }
            objParams = null;
        }
    }
    private void checkNulls()
    {
        if (txtlliability.Text.Trim().Length == 0)
            txtlliability.Text = "0";
        if (txtlphysical.Text.Trim().Length == 0)
            txtlphysical.Text = "0";
        if (txtlcargo.Text.Trim().Length == 0)
            txtlcargo.Text = "0";
    }
}
