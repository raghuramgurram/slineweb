<%@ Page AutoEventWireup="true" CodeFile="ShipingLineWiseOpenLoads.aspx.cs" Inherits="ShipingLineWiseOpenLoads"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>
    <%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>
                <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="middle">
                        <asp:Label ID="lblFromdate" runat="server" Text="From : "></asp:Label>
                            <uc1:DatePicker ID="dtpFromDate" runat="server" IsDefault="false" IsRequired="false" CountNextYears="2" CountPreviousYears="0" SetInitialDate="true" />
                            &nbsp; &nbsp;&nbsp; &nbsp;<asp:Label ID="lblToDate" runat="server" Text="To : "></asp:Label>
                            <uc1:DatePicker ID="dtpToDate" runat="server" IsDefault="false" IsRequired="false" CountNextYears="50" CountPreviousYears="10" SetInitialDate="true" />                            
                            &nbsp; &nbsp;
                            <asp:Label ID="lblText" runat="server" Text="Shipping Line Name : "></asp:Label>
                            <asp:DropDownList ID="ddlShipingLines" runat="server">
                            </asp:DropDownList>
                            <asp:Button ID="btnSearch" runat="server" CssClass="btnStyle" Text="Search" OnClick="btnSearch_Click" />
                        </td>
                    </tr>
                </table>                
            </td>
        </tr>
    </table>
</asp:Content>

