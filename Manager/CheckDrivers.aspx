<%@ Page AutoEventWireup="true" CodeFile="~/Manager/CheckDrivers.aspx.cs" Inherits="CheckDriver"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>
<%@ Register Src="../UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc4" %>
<%@ Register Src="../UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="tblform1">
                    <tr>
                    <td>    
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                    </td>
                    </tr>
                </table>
               <table id="tblbox1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr style="background-color: #FFFFFF;">
                        <td align="left" valign="middle">
                           Enter Date :&nbsp;
                            <uc4:DatePicker ID="dtpdate" runat="server" IsDefault="true" IsRequired="true"
                                SetInitialDate="false" Visible="true" CountNextYears="2" CountPreviousYears="0" />
                            <asp:Button ID="btnSearch" runat="server" CssClass="btnStyle" Text="Search" OnClick="btnSearch_Click"/>                            
                   </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="height: 5px">
                            <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <uc2:GridBar ID="GridBar1" runat="server" HeaderText="All Drivers" />                            
                       </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="3" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:GridView ID="gridDrivers" runat="server" AllowPaging="True"
                                AutoGenerateColumns="False" CellPadding="4" CssClass="Grid"
                                PageSize="15" Width="100%" OnRowDataBound="gridDrivers_RowDataBound" OnPageIndexChanging="gridDrivers_PageIndexChanging" >
                                <PagerStyle CssClass="GridPage" Font-Size="Smaller" HorizontalAlign="Center" />
                                <RowStyle BorderColor="White" CssClass="GridItem" />
                                <HeaderStyle BorderColor="White" CssClass="GridHeader" ForeColor="Black" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                                <Columns>                                                                    
                                    <asp:BoundField DataField="DriveName" HeaderText="Driver Name">
                                    <itemstyle width="40%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Address" HeaderText="Contact#, Address ">     
                                    <itemstyle width="40%" />
                                    </asp:BoundField>    
                                    <asp:TemplateField HeaderText="Select">
                                    <ItemTemplate>
                                    <asp:CheckBox ID="chkAssigned" runat="server"/>                                    
                                    </ItemTemplate>
                                        <ItemStyle Width="20%" />
                                    </asp:TemplateField>  
                                    <asp:BoundField DataField="DriverId">
                                        <ItemStyle Width="0%" />
                                    </asp:BoundField>                                      
                                 </Columns>                               
                            </asp:GridView>                            
                        </td>
                    </tr>
                </table>   
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="3" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <asp:Panel ID="pnlSave" runat="server" Width="100%" Visible="false">
                <table id="tblbox1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnSave" CssClass="btnstyle" runat="server"  Text="Save" OnClick="btnSave_Click" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="btnstyle" Text="Cancel" OnClick="btnCancel_Click"/>
                        </td>
                    </tr>
                </table>    
                </asp:Panel>                 
            </td>
        </tr>
    </table>
</asp:Content>

