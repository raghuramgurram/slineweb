using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Mobile_eTransportMobile : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session[Keys.LOGIN_ID_KEY] == null || Session[Keys.EMPLOY_ID] == null)
        {
            Response.Redirect(Keys.PAGE_LOAGIN);
        }

        if (!IsPostBack)
        {
            ltlyearfordisplay.Text = DateTime.Now.Year.ToString();
            ltlcompanynameTitle.Text = ltlcompanyname.Text = ltlcompanynameSub.Text = ConfigurationManager.AppSettings["CompanyName"].ToString();
        }
    }
    public void ShowErrors(string Error)
    {
        if (Error != string.Empty)
        {
            errorDiv.InnerHtml = Error;
            errorDiv.Visible = true;
        }
        else
        {
            errorDiv.InnerHtml = Error;
            errorDiv.Visible = false;
        }
    }
    public void ShowMessage(string message)
    {
        if (message != string.Empty)
        {
            messageDiv.InnerHtml = message;
            messageDiv.Visible = true;
        }
        else
        {
            messageDiv.InnerHtml = message;
            messageDiv.Visible = false;
        }

    }
}
