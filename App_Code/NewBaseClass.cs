using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NewBaseClass
/// </summary>
public class NewBaseClass
{

    private Int64 _bint_LoadId;
    public Int64 @bint_LoadId
    {
        get{return _bint_LoadId;}
        set{_bint_LoadId = value;}
    }

    private Int64 _bint_DocumentId;
    public Int64 @bint_DocumentId
    {
        get{return _bint_DocumentId;}
        set{_bint_DocumentId = value;}
    }

    private Int64 _bint_FileNo;
    public Int64 @bint_FileNo
    {
        get{return _bint_FileNo;}
        set{_bint_FileNo = value;}
    }

    private string _nvar_DocumentName;
    public string @nvar_DocumentName
    {
        get{return _nvar_DocumentName;}
        set{_nvar_DocumentName = value;}
    }

    private Int64 _bint_LoadStatusId;
    public Int64 @bint_LoadStatusId
    {
        get{return _bint_LoadStatusId;}
        set { _bint_LoadStatusId = value; }
    }

    private DateTime _date_CreateDate;
    public DateTime @date_CreateDate
    {
       get{return _date_CreateDate;}
        set{_date_CreateDate = value;}
    }

    private string _nvar_Location;
    public string @nvar_Location
    {
        get{return _nvar_Location;}
        set{_nvar_Location = value;}
    }

    private string _nvar_Notes;
    public string @nvar_Notes
    {
        get{return _nvar_Notes;}
        set { _nvar_Notes = value; }
    }

    private char _Action;
    public char @Action
    {
        get{return _Action;}
        set { _Action = value; }
    }

    private Int64 _UserId;
    public Int64 @UserId
    {
        get{return _UserId;}
        set { _UserId = value; }
    }

    private string _nvar_UserId;
    public string @nvar_UserId
    {
        get{return _nvar_UserId;}
        set { _nvar_UserId = value; }
    }

    private string _nvar_Password;
    public string @nvar_Password
    {
        get{return _nvar_Password;}
        set { _nvar_Password = value; }
    }

    private string _bint_Chasis;
    public string @bint_Chasis
    {
        get{return _bint_Chasis;}
        set { _bint_Chasis = value; }
    }

    private Int64 _bint_LoginId;
    public Int64 @bint_LoginId
    {
        get{return _bint_LoginId;}
        set { _bint_LoginId = value; }
    }

    private Int64 _bint_Employid;
    public Int64 @bint_Employid
    {
        get{return _bint_Employid;}
        set { _bint_Employid = value; }
    }
}
