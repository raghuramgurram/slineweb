using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class UploadAnyDocs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Upload Documents";
        if (Request.QueryString.Count > 0)
        {
            ViewState["ID"] = Convert.ToString(Request.QueryString[0]).Trim();
             LoadOrder.LoadID = Request.QueryString[0].Trim();
             PurchaseOrder.LoadID = Request.QueryString[0].Trim();
             BillofLading.LoadID = Request.QueryString[0].Trim();
             AccessorialCharges.LoadID = Request.QueryString[0].Trim();
             OutGateInterchange.LoadID = Request.QueryString[0].Trim();
             InGateInterchange.LoadID = Request.QueryString[0].Trim();
             ProofofDelivery.LoadID = Request.QueryString[0].Trim();
             MiscScaleTicket.LoadID = Request.QueryString[0].Trim();
             GridBar1.HeaderText = "Load Number : " + Request.QueryString[0].Trim();
             if (!Page.IsPostBack)
             {
                 LoadOrder.Fill();
                 PurchaseOrder.Fill();
                 BillofLading.Fill();
                 AccessorialCharges.Fill();
                 OutGateInterchange.Fill();
                 InGateInterchange.Fill();
                 ProofofDelivery.Fill();
                 MiscScaleTicket.Fill();                 
             }
        }
    }
    protected void btnupload_Click(object sender, EventArgs e)
    {
        if (ViewState["ID"] != null)
        {
            LoadOrder.Save();
            PurchaseOrder.Save();
            BillofLading.Save();
            AccessorialCharges.Save();
            OutGateInterchange.Save();
            InGateInterchange.Save();
            ProofofDelivery.Save();
            MiscScaleTicket.Save();
            CheckBackPage();
            Response.Redirect("~/Manager/TrackLoad.aspx?" + Convert.ToString(ViewState["ID"]),false);
        }
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        if (ViewState["ID"] != null)
        {
            CheckBackPage();
            Response.Redirect("~/Manager/TrackLoad.aspx?" + Convert.ToString(ViewState["ID"]));
        }
    }
    private void CheckBackPage()
    {
        if (Session["BackPage3"] != null)
        {
            if (Convert.ToString(Session["BackPage3"]).Trim().Length > 0)
            {
                string strTemp = Convert.ToString(Session["BackPage3"]).Trim();
                Session["BackPage3"] = null;
                Response.Redirect(strTemp, false);
            }
        }  
    }
}
