﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage.master" AutoEventWireup="true" CodeFile="TenderLoadDetails.aspx.cs" Inherits="Manager_TenderLoadDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link rel="stylesheet" type="text/css" href="../Styles/DataTable.css" />
    <div class="dataTables_wrapper no-footer bg-white">
         <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    &nbsp;<asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                    
               </td>
            </tr>
        </table>
                <table style="border-style:solid; border-width:1px; width:700px;" class="display Grid dataTable no-footer">
         <tr><td  colspan="2" class="customer-name">   <%=((Customer)ViewState["Load"]).customerName%></td>    </tr>
        <tr><td  colspan="2">Order Number :  <%=((Customer)ViewState["Load"]).orderNumber%></td>    </tr>
<tr><td  colspan="2" style="color:red"><div style="float:left" >
            <span>Office Location : </span>
            <asp:DropDownList ID="ddlTopLocation" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTopLocation_SelectedIndexChanged" />
     <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlTopLocation"
                          Display="Dynamic" ErrorMessage="Please select office location." Operator="NotEqual"
                          ValueToCompare="Select">*</asp:CompareValidator></div>
         <div style="float:right"><asp:Button ID="btnMove" runat="server" CssClass="btnstyle mset" Text="" CommandName="MOVE" OnClick="btnAction_Click" CausesValidation="false"/> </div></td>  </tr>
        <%--<tr><td>Segment Type </td>   <td> Stay With   </td></tr>--%>
                        
        <tr><td><b>Origin : </b><%=((Customer)ViewState["Load"]).Origins%></td>   <td><b>Origin Date : </b><%=((Customer)ViewState["Load"]).PickupDates%></td></tr>       

        <tr><td><b>Destination : </b><%=((Customer)ViewState["Load"]).Destinations%></td>  <td><b>Destination Date : </b><%=((Customer)ViewState["Load"]).DeliveryDates%></td></tr>
        <tr><td>Commodity : Dry </td>  <td>Rate : <%=((Customer)ViewState["Load"]).Charges.Charge%> &nbsp; <%=((Customer)ViewState["Load"]).Charges.Advances%> &nbsp; <%=((Customer)ViewState["Load"]).Charges.FreightRate%></td></tr>
        <tr><td>Equipment :  <%=((Customer)ViewState["Load"]).Equipment%></td>  <td> Trip Type : <%=((Customer)ViewState["Load"]).TripType%>  </td></tr>
        <tr><td>Equipment Provider : <%=((Customer)ViewState["Load"]).EquipmentProvider%></td>   <td style="color:red">Shipping Line : <b>(<%=((Customer)ViewState["Load"]).EquipmentProvider%>) </b> <br /> <asp:DropDownList ID="ddlshipping" runat="server">
                          <asp:ListItem Value="Select">Select</asp:ListItem>
                      </asp:DropDownList>
                      <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlshipping"
                          Display="Dynamic" ErrorMessage="Please select shipping line." Operator="NotEqual"
                          ValueToCompare="Select">*</asp:CompareValidator></td></tr>
        <tr><td>Pickup Number : <%=((Customer)ViewState["Load"]).RailPickupNum%></td>   <td>Booking Number : </td></tr>
        <tr><td>Trailer Number :  <%=((Customer)ViewState["Load"]).TrailerNumber%> </td>   <td> Seal Number : <%=((Customer)ViewState["Load"]).SealNumber%></td>       
       <tr><td colspan="2">&nbsp;</td></tr>
                 
        <tr ><td colspan="2" style="text-align:center">
                <asp:Button ID="btnacpt" runat="server" CssClass="btnstyle mset" Text="ACCEPT" CommandName="ACCEPT" OnClick="btnAction_Click"/></td></tr>
                    <tr><td colspan="2" style="text-align:center"> <asp:Button ID="btnrjct" runat="server" CssClass="btnstyle mset" Text="DECLINE" CommandName="REJECT" OnClick="btnAction_Click" CausesValidation="false"/>
                        <tr><td colspan="2" style="text-align:center"> <asp:Button ID="btnback" runat="server" CssClass="btnstyle mset" Text="Back" CommandName="BACK" OnClick="btnAction_Click" CausesValidation="false"/>
                </td></tr>
                    </table></div>
</asp:Content>

