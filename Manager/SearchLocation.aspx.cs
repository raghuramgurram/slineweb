using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class SearchLocation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Search Location";
        //btnDelete.OnClientClick = "return confirm('Are you sureyou want to delete this record?');";
        lblErrMessage.Visible = false;
        Session["BackPage"] = null;
        if (!IsPostBack)
        {
            AssignListValues();
            if (ddlList.Items.Count > 0)
            {
                Grid1.Visible = true;
                Grid1.EditVisible = true;
                Grid1.DeleteVisible = true;
                Grid1.TableName = "eTn_Location";
                Grid1.Primarykey = "bint_LocationId";
                Grid1.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
                Grid1.ColumnsList = "bint_LocationId," + CommonFunctions.AddressQueryString("nvar_Street;nvar_Suite;nvar_City;nvar_State;nvar_Zip", "Address") + ";" +
                    CommonFunctions.PhoneQueryString("num_Phone", "Phone") + ";" + CommonFunctions.PhoneQueryString("num_Fax", "Fax");
                Grid1.VisibleColumnsList = "Address;Phone;Fax";
                Grid1.VisibleHeadersList = "Address;Phone;Fax";
                Grid1.DeleteTablesList = "eTn_Location";
                Grid1.DeleteTablePKList = "bint_LocationId";
                Grid1.DependentTablesList = "eTn_Origin;eTn_Destination";
                Grid1.DependentTableKeysList = "bint_LocationId;bint_LocationId";
                if (Request.QueryString.Count > 0)
                    ddlList.SelectedIndex = ddlList.Items.IndexOf(ddlList.Items.FindByValue(Request.QueryString[0].Trim()));
                Grid1.WhereClause = "bint_CompanyId=" + Convert.ToString(ddlList.SelectedItem.Value).Trim();
                Grid1.EditPage = "~/Manager/NewLocation.aspx";
                Grid1.BindGrid(0);
                if (Convert.ToInt32(ddlList.SelectedValue) == 0)
                    barCompany.PagesList = "NewLocation.aspx;NewLocation.aspx";
                else
                    barCompany.PagesList = "NewLocation.aspx;NewLocation.aspx?" + ddlList.SelectedValue + "^" + ddlList.SelectedItem.Text;
            }
            SetOperationVisibilty();
        }
    }

    protected void AssignListValues()
    {
        //ddlList.DataSource = DBClass.returnDataTable("Select bint_CompanyId,nvar_CompanyName from eTn_Company order by [nvar_CompanyName]");
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCompanyNames", new object[] { "", officeLocationId });
        //SLine 2017 Enhancement for Office Location Ends
        if (ds.Tables.Count > 0)
        {
            ddlList.DataSource = ds.Tables[0];
            ddlList.DataValueField = "bint_CompanyId";
            ddlList.DataTextField = "nvar_CompanyName";
            ddlList.DataBind();
        }
        ddlList.Items.Insert(0, new ListItem("--Select--", "0"));
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    protected void ddlList_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["GridPageIndex"] = 0;
        if (string.Compare(ddlList.SelectedItem.Text, "--Select--", true, System.Globalization.CultureInfo.CurrentCulture) != 0)
        {
            Grid1.WhereClause = "bint_CompanyId=" + Convert.ToString(ddlList.SelectedItem.Value).Trim();
            Grid1.BindGrid(0);
            Grid1.Visible = true;
            barCompany.PagesList = "NewLocation.aspx;NewLocation.aspx?" + ddlList.SelectedValue + "^" + ddlList.SelectedItem.Text;
            barCompany.AssignPagesToLinks();
            lblOperations.Visible = true;
            dropOperations.Visible = true;
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            txtEdit.Visible = false;
            lblErrMessage.Visible = false; 
            dropOperations.SelectedIndex = 0;
        }
        else
        {
            Grid1.Visible = false;
            barCompany.PagesList = "NewLocation.aspx;NewLocation.aspx";
            barCompany.AssignPagesToLinks();
            lblOperations.Visible = false;
            dropOperations.Visible = false;
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            txtEdit.Visible = false;
            lblErrMessage.Visible = false;
            dropOperations.SelectedIndex = 0;
        }
    }
    private void SetOperationVisibilty()
    {
        if (string.Compare(ddlList.SelectedItem.Text, "--Select--", true, System.Globalization.CultureInfo.CurrentCulture) != 0)
        {
            lblOperations.Visible = true;
            dropOperations.Visible = true;
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            txtEdit.Visible = false;
            lblErrMessage.Visible = false;
            dropOperations.SelectedIndex = 0;
        }
        else
        {
            lblOperations.Visible = false;
            dropOperations.Visible = false;
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            txtEdit.Visible = false;
            lblErrMessage.Visible = false;
            dropOperations.SelectedIndex = 0;
        }
    }
    protected void dropOperations_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (string.Compare(dropOperations.SelectedItem.Text, "--Select--", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            txtEdit.Visible = false;
            lblErrMessage.Visible = false;
        }
        else
        {
            if (string.Compare(dropOperations.SelectedItem.Text, "Edit", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                btnDelete.Visible = false;
                btnEdit.Visible = true;
                txtEdit.Visible = true;
                txtEdit.Text = ddlList.SelectedItem.Text.Trim();
                ViewState["EditCompany"] = ddlList.SelectedItem.Text.Trim();
                lblErrMessage.Visible = false;  
            }
            else if (string.Compare(dropOperations.SelectedItem.Text, "Delete", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                btnEdit.Visible = false;
                btnDelete.Visible = true;
                txtEdit.Visible = false;
                txtEdit.Text = "";
                lblErrMessage.Visible = false;              
            }
        }
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        lblErrMessage.Visible = false;
        if (string.Compare(Convert.ToString(ViewState["EditCompany"]), txtEdit.Text.Trim(), true, System.Globalization.CultureInfo.CurrentCulture) != 0)
        {
            if (Convert.ToInt32(DBClass.executeScalar("Select Count(bint_CompanyId) from [eTn_Company] where Lower(nvar_CompanyName)='" + txtEdit.Text.Trim().ToLower() + "'")) > 0)
            {
                lblErrMessage.Visible = true;
                return;
            }
            if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_Update_Company", new object[] { ddlList.SelectedItem.Value.Trim(), txtEdit.Text.Trim() }) > 0)
            {
                Response.Redirect("~/Manager/SearchLocation.aspx");
                //AssignListValues();
                //ddlList.SelectedIndex = ddlList.Items.IndexOf(new ListItem(txtEdit.Text.Trim()));
            }
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(DBClass.executeScalar("Select Count(bint_CompanyId) from eTn_Location where bint_CompanyId=" + ddlList.SelectedItem.Value.Trim())) > 0)
            Response.Write("<script>alert('You can not delete this company due to its dependencies.')</script>");
        else
        {
            if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_Delete_Company", new object[] { ddlList.SelectedItem.Value.Trim() }) > 0)
            {
                Response.Redirect("~/Manager/SearchLocation.aspx");
            }
        }
    }
}
