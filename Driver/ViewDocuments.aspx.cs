using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Carrier_ViewDocuments : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //this.Page.Title = Convert.ToString(ConfigurationSettings.AppSettings["CompanyName"]);
        this.Page.Title = GetFromXML.CompanyName;
        if (!IsPostBack)
        {
            if (Request.QueryString["ID"] != null)
            {
                try
                {
                    ViewState["ID"] = Request.QueryString["ID"];
                    GetDocumentsbyID(7);
                }
                catch
                { }
            }
        }
    }

    private void GetDocumentsbyID(long DocumentId)
    {
        if (ViewState["ID"] != null)
        {
            DataTable dt = DBClass.returnDataTable("select [nvar_DocumentName],isnull([bint_FileNo],0),[date_ReceivedDate] from [eTn_UploadDocuments] where [bint_LoadId]=" + Convert.ToInt64(ViewState["ID"]) + " and [bint_DocumentId]=" + DocumentId + " and [nvar_DocumentName] is not null");
            if (dt.Rows.Count > 0)
            {
                string strFormat = "<table border=1 cellpadding=0 cellspacing=0 id=tbldisplay width='100%'><tr><th>View Documents</th></tr>";
                string docpath = "../Documents" + "\\" + Convert.ToString(ViewState["ID"]).Trim() + "\\";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    strFormat += "<tr id=" + ((i % 2 == 0) ? "row" : "altrow") + "><td align=left>";
                    strFormat += "<a style=font-size:12;color:blue;height:15; href='" + docpath + dt.Rows[i][0].ToString() + "' target='_blank'>" + dt.Rows[i][0].ToString() + "</a>";
                    strFormat += "</td></tr>";
                }
                strFormat += "</table>";
                plhDocs.Controls.Add(new LiteralControl(strFormat));
            }
            else
            {
                plhDocs.Controls.Add(new LiteralControl("<table id=ContentTbl border=0 cellpadding=0 cellspacing=0 width=100%><tr valign=top><td style='font-size:14px;' align=center><strong>No documents Available for the Load Number : " + ViewState["ID"].ToString() + "</strong></td></tr></table>"));
            }
            if (dt != null) { dt.Dispose(); dt = null; }
        }
    }        
}
