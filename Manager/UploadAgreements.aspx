<%@ Page Language="C#" MasterPageFile="~/Manager/MasterPage.master" AutoEventWireup="true" CodeFile="UploadAgreements.aspx.cs" Inherits="Manager_Default" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td>
    <table width="100%" border="0" cellpadding="3" cellspacing="0" id="tblform">
              <tr>
                <th colspan="3">Agreement Papers</th>
              </tr>                       
            </table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="tbldisplay">
<tr id="altrow">    
    <td valign="top" width="50%">       
      UpLoad Agreement Document : <asp:FileUpload ID="fileUploadDocuments" runat="Server" />                  
        <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="btnstyle" CausesValidation="false" OnClick="btnUpload_Click"/>
     </td>
     <td>
        <asp:GridView ID="grdAggrements" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="Grid" DataKeyNames="bint_AgreementId">
            <RowStyle CssClass="GridItem" BorderColor="White" />
            <HeaderStyle CssClass="GridHeader" BorderColor="White" />
            <AlternatingRowStyle CssClass="GridAltItem" />
            <Columns>
                <asp:TemplateField HeaderText="Document Name">
                    <ItemTemplate>
                        <a href="../Agreement/<%#DataBinder.Eval(Container.DataItem,"nvar_DocumentName")%>" target="_blank"><%#DataBinder.Eval(Container.DataItem,"nvar_DocumentName")%></a>
                    </ItemTemplate>
                </asp:TemplateField>
<%--               <asp:HyperLinkField DataTextField="nvar_DocumentName" HeaderText="Document Name" DataNavigateUrlFields="nvar_DocumentName" DataNavigateUrlFormatString="../Agreement/{0}"/>
--%>               <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="imgDelete" runat="Server" ImageUrl="~/Images/delete_icon.gif" Width="12" Height="13" OnClick="OnDelete" ToolTip="Delete"/>
                </ItemTemplate>
               </asp:TemplateField>
            </Columns>           
        </asp:GridView>
        &nbsp;&nbsp;&nbsp;
     </td>
  </tr>    
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
        <tr>
          <td align="right">
              <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="btnstyle" Height="22px" OnClick="btnCancel_Click" CausesValidation="False" UseSubmitBehavior="False" /></td>
        </tr>
      </table>
</td>
</tr>
</table>
</asp:Content>

