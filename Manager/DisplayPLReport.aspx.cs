﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Drawing;

public partial class Manager_DisplayPLReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            btnPrintReport.Attributes.Add("onclick", "javascript:window.open('printreport.aspx','print','width=1000,height=650,scrollbars=yes,menubar=yes,resizable=yes');");
        }
        object[] objParams = null;
        Session["TopHeaderText"] = "Profit and Loss";
        DataSet ds = new DataSet();
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        string reportParam = Convert.ToString(Session["ReportParameters"]) + "^" + officeLocationId.ToString();
        objParams = reportParam.Trim().Replace("~~^^^^~~", "^").Split('^');
        //objParams = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
        //SLine 2017 Enhancement for Office Location Ends
        lblSearchCriteria.Text = "Month : " + objParams[2].ToString() + " Year : " + objParams[1].ToString();
        Session["SerchCriteria"] = lblSearchCriteria.Text;
        ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetProfitLoss", objParams);
        PLGrid.DataSource = ds;
        PLGrid.DataBind();
        btnPrintReport.Visible = true;
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count == 1)
            {
                lblDisplay.Text = "<table width=100% border=0 cellpadding=0 cellspacing=0 id=ContentTbl><tr valign=top><td>" +
                    "<table border=0 width=100% height=200px border=0 cellpadding=0 cellspacing=0 align=center valign=middle><tr><td width=100% align=center valign=middle>" +
                    "<b><font color=blue>No Records To Display.<font></b></td></tr></table><br/><br/><br/></td></tr></table>";
                lblDisplay.Visible = true;
                PLGrid.Visible = false;
                Session["PrintDetails"] = lblDisplay.Text;
            }
            else
            {
                StringWriter tw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(tw);
                lblDisplay.RenderControl(hw);
                Session["PrintDetails"] = tw.ToString();
            }
        }
    }

    protected void PLGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.DataItem != null)
            {
                if (e.Row.Cells[22].Text != "Closed")
                    e.Row.Cells[22].ForeColor = Color.Red;
                if(e.Row.Cells[21].Text == "0.00" || e.Row.Cells[21].Text.Contains("-"))
                    e.Row.Cells[21].ForeColor = Color.Red;
                if (e.Row.Cells[0].Text == "Total")
                {
                    e.Row.Font.Bold = true;                    
                    e.Row.Cells[11].Text = string.Format("{0:N}", Convert.ToString(e.Row.Cells[11].Text) == "&nbsp;" ? 0.00 : Double.Parse(e.Row.Cells[11].Text));
                    e.Row.Cells[12].Text = string.Format("{0:N}", Convert.ToString(e.Row.Cells[12].Text) == "&nbsp;" ? 0.00 : Double.Parse(e.Row.Cells[12].Text));
                    e.Row.Cells[13].Text = string.Format("{0:N}", Convert.ToString(e.Row.Cells[13].Text) == "&nbsp;" ? 0.00 : Double.Parse(e.Row.Cells[13].Text));
                    e.Row.Cells[14].Text = string.Format("{0:N}", Convert.ToString(e.Row.Cells[14].Text) == "&nbsp;" ? 0.00 : Double.Parse(e.Row.Cells[14].Text));
                    e.Row.Cells[16].Text = string.Format("{0:N}", Convert.ToString(e.Row.Cells[16].Text) == "&nbsp;" ? 0.00 : Double.Parse(e.Row.Cells[16].Text));
                    e.Row.Cells[17].Text = string.Format("{0:N}", Convert.ToString(e.Row.Cells[17].Text) == "&nbsp;" ? 0.00 : Double.Parse(e.Row.Cells[17].Text));
                    e.Row.Cells[18].Text = string.Format("{0:N}", Convert.ToString(e.Row.Cells[18].Text) == "&nbsp;" ? 0.00 : Double.Parse(e.Row.Cells[18].Text));
                    e.Row.Cells[19].Text = string.Format("{0:N}", Convert.ToString(e.Row.Cells[19].Text) == "&nbsp;" ? 0.00 : Double.Parse(e.Row.Cells[19].Text));
                    e.Row.Cells[20].Text = string.Format("{0:N}", Convert.ToString(e.Row.Cells[20].Text) == "&nbsp;" ? 0.00 : Double.Parse(e.Row.Cells[20].Text));
                    e.Row.Cells[21].Text = string.Format("{0:N}", Convert.ToString(e.Row.Cells[21].Text) == "&nbsp;" ? 0.00 : Double.Parse(e.Row.Cells[21].Text));
                }
            }
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            ExportToExcel();
        }
        catch
        { }
    }

    private void ExportToExcel()
    {
        string fileName = string.Empty;
        if (Master.FindControl("Top1").FindControl("lblHeadText") != null)
        {
            fileName = ((Label)Master.FindControl("Top1").FindControl("lblHeadText")).Text;
        }
        if (string.IsNullOrEmpty(fileName))
        {
            fileName = "ExcelFile";
        }
        fileName = fileName.Replace(" ", "");
        fileName = fileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
        Response.ContentType = "application/x-msexcel";
        Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + ".xls"); Response.ContentEncoding = Encoding.UTF8;
        StringWriter tw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(tw);
        tbldispaly.RenderControl(hw);
        Response.Write(tw.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {

    } 
}