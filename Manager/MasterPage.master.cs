using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CommonFunctions.InsertIntoInfoLog("MasterPage.master :: Page_Load :: Start");
        //this.Page.Title = Convert.ToString(ConfigurationSettings.AppSettings["CompanyName"]);
        this.Page.Title = GetFromXML.CompanyName;
        if (Session["UserId"] != null)
        {
           // Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "text/html";
            Response.AddHeader("pragma", "no-cache");
            Response.AddHeader("cache-control", "private, no-cache, must-revalidate");
        }
        else
        {
            Response.Redirect("~/Login.aspx");
        }
        CommonFunctions.InsertIntoInfoLog("MasterPage.master :: Page_Load :: END");
    }
}
