﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class Manager_DailyDeliveredLoads : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Daily Delivered Loads";
        if (!IsPostBack)
        {
            FillLists();
        }
    }

    private void FillLists()
    {
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCustomerNames");
        if (ds.Tables.Count > 0)
        {
            ddlCustomers.DataSource = ds.Tables[0];
            ddlCustomers.DataValueField = "bint_CustomerId";
            ddlCustomers.DataTextField = "nvar_CustomerName";
            ddlCustomers.DataBind();
        }
        ddlCustomers.Items.Insert(0, new ListItem("All", "0"));
        ds.Tables.Clear();
        if (ds != null) { ds.Dispose(); ds = null; }
    }
   
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Session["SerchCriteria"] = "Daily Delivered Loads";
            Session["ReportParameters"] = null;
            Session["ReportName"] = null;
            Session["ReportParameters"] = dtpFromDate.Date + "~~^^^^~~" + dtpToDate.Date + "~~^^^^~~" + ddlCustomers.SelectedValue; ;
            //Session["SerchCriteria"] = " From Date : " + dtpFromDate.Date + "&nbsp;&nbsp;&nbsp; To Date : " + dtpToDate.Date + "&nbsp;&nbsp;&nbsp;" + (ddlCustomer.SelectedIndex > 0 ? "&nbsp; Customer Name : " + ddlCustomer.SelectedItem.Text : " All Customers ");
            Session["ReportName"] = "GetDailyDeliveredLoads";
            Response.Redirect("~/Manager/DisplayReport.aspx");
        }
    }
}