<%@ Control Language="C#" AutoEventWireup="true" CodeFile="top.ascx.cs" Inherits="top" %>
<%@ Register Src="~/UserControls/Phone.ascx" TagName="Phone" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>S Line Transport Inc</title>
<link href="../Styles/style.css" rel="stylesheet" type="text/css"/>
<script language="JavaScript" type="text/javascript" src="../mm_menu.js"></script>
</head>
 
<BODY>
<script type="text/javascript" language="JavaScript1.2">mmLoadMenus();</script>
<script type="text/javascript">
        function Check(textBox, maxLength) {
        maxLength = document.getElementById('<%=hfealimaxcount.ClientID %>').value;
            if (textBox.value.length > maxLength) {
                alert("Max characters allowed are " + maxLength);
                textBox.value = textBox.value.substr(0, maxLength);
            }
        }    
        function myFun(sel)
{   
var lst='';
for(i=0; i < sel.options.length; i++)
      if(sel.options[i].selected)
          lst =lst+ sel.options[i].text+'; ';
          document.getElementById('<%=txtNames.ClientID %>').value =lst;
}    
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tbody>
  <tr>
    <td width="231"><img src="../Images/logo.gif" alt="Logo" width="231" height="78" /></td>
    
    <td valign="bottom">
    <div id="divTopLocation" runat="server" class="select" style="margin:10 20px 5px 0;">
            <span>Office Location</span>
            <asp:DropDownList ID="ddlTopLocation" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTopLocation_SelectedIndexChanged" />
        </div>
        <asp:Panel ID="pnlManager" runat="server" Visible="false">
        
     <table width="100%" border="0" cellpadding="0" cellspacing="0" style="background:#4C4C4C;">          
      <tr>          
        <td width="100%">&nbsp;</td>
        <td width="70"><a href="dashboard.aspx"><img id="Img1" alt="Loads" border="0" height="32" name="image1" src="../Images/loads.gif"
                width="70" /></a></td>
        <td width="88"><a href="SearchLocation.aspx"><img id="Img2" alt="Locations" border="0" height="32" name="image2" src="../Images/locations.gif"
                width="88" /></a></td>
        <td width="96"><a href="SearchCustomer.aspx"><img id="Img3" alt="Customers" border="0" height="32" name="image3" src="../Images/customers.gif"
                width="96" /></a></td>
        <td width="79"><a href="SearchCarrier.aspx"><img id="Img4" alt="Carriers" border="0" height="32" name="image5" src="../Images/carriers.gif"
                width="79" /></a></td>
          <td width="111"><a href="SearchShipping.aspx"><img alt="Shippingline" border="0" height="32" src="../Images/shippingline.gif" width="111" /></a></td>
        <%--<td width="95"><a href="SearchEquipment.aspx"><img id="Img5" alt="Equipment" border="0" height="32" name="image4" src="../Images/equipment.gif"
                width="95" /></a></td>--%>
        <td ><img  id="image6" runat="server" visible="false" alt="Users" height="32" name="image6" onmouseout="MM_startTimeout();"
                onmouseover="MM_showMenu(window.mm_menu_0406015752_0,0,32,null,'image6')" src="../Images/users.gif"
                width="66" /></td>
        <td width="78"><a href="Reports.aspx"><img alt="Reports" border="0" height="32" src="../Images/reports.gif" width="78" /></a></td>
        </tr>
    </table>
    </asp:Panel>
        <asp:Panel ID="pnlDispatcher" runat="server" Visible="false">
            <table border="0" cellpadding="0" cellspacing="0" style="background: #4C4C4C;" width="100%">
                <tr align="right">
                
                <td width="100%">&nbsp;</td>
        <td width="70"><a href="dashboard.aspx"><img id="Img16" alt="Loads" border="0" height="32" name="image1" src="../Images/loads.gif"
                width="70" /></a></td>
        <td width="88"><a href="SearchLocation.aspx"><img id="Img17" alt="Locations" border="0" height="32" name="image2" src="../Images/locations.gif"
                width="88" /></a></td>
        <td width="96"><a href="SearchCustomer.aspx"><img id="Img18" alt="Customers" border="0" height="32" name="image3" src="../Images/customers.gif"
                width="96" /></a></td>
        <td width="79"><a href="SearchCarrier.aspx"><img id="Img19" alt="Carriers" border="0" height="32" name="image5" src="../Images/carriers.gif"
                width="79" /></a></td>
          <td width="111"><a href="SearchShipping.aspx"><img alt="Shippingline" border="0" height="32" src="../Images/shippingline.gif" width="111" /></a></td>
        
                
                    
                    <td width="79">
                        <a href="SearchDriver.aspx">
                            <img id="Img9" alt="Drivers" border="0" height="32" name="image5" src="../Images/drivers.gif"
                                width="95" /></a></td>
                    <td width="95">
                        <a href="SearchEquipment.aspx">
                            <img id="Img10" alt="Equipment" border="0" height="32" name="image4" src="../Images/equipment.gif"
                                width="95" /></a></td>
                                 <td width="78"><a href="Reports.aspx"><img alt="Reports" border="0" height="32" src="../Images/reports.gif" width="78" /></a></td>
               </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnlStaff" runat="server" Visible="false">
            <table border="0" cellpadding="0" cellspacing="0" style="background: #4C4C4C;" width="100%">
                <tr align="right">
                    <td width="100%">
                        &nbsp;</td>
                    <td width="70">
                        <a href="dashboard.aspx">
                            <img id="Img11" alt="Loads" border="0" height="32" name="image1" src="../Images/loads.gif"
                                width="70" /></a></td>
                    <td width="88">
                        <a href="SearchLocation.aspx">
                            <img id="Img12" alt="Locations" border="0" height="32" name="image2" src="../Images/locations.gif"
                                width="88" /></a></td>
                    <td width="96">
                        <a href="SearchCustomer.aspx">
                            <img id="Img13" alt="Customers" border="0" height="32" name="image3" src="../Images/customers.gif"
                                width="96" /></a></td>
                    <td width="79">
                        <a href="SearchCarrier.aspx">
                            <img id="Img14" alt="Carriers" border="0" height="32" name="image5" src="../Images/carriers.gif"
                                width="79" /></a></td>
                                <td width="111"><a href="SearchShipping.aspx"><img alt="Shippingline" border="0" height="32" src="../Images/shippingline.gif" width="111" /></a></td>
                    <td width="95">
                        <a href="SearchEquipment.aspx">
                            <img id="Img15" alt="Equipment" border="0" height="32" name="image4" src="../Images/equipment.gif"
                                width="95" /></a></td>
                                 <td width="78"><a href="Reports.aspx"><img alt="Reports" border="0" height="32" src="../Images/reports.gif" width="78" /></a></td>
                </tr>
            </table>
        </asp:Panel>    
    </td>
  </tr>
  <tr>
    <td align="left" style="font-family: Arial, Helvetica, sans-serif;font-size: 11px;color: #000000;vertical-align:middle;height: 25px;padding-left:15px;"> 
        <asp:LinkButton CausesValidation="false" ID="lnkAppSettings" runat="server" Visible="False" PostBackUrl="~/manager/appsettings.aspx" >Site Settings</asp:LinkButton>       
        <span runat="server" id="spntunnel" visible="false">|</span>
        <asp:LinkButton CausesValidation="false" ID="lnkShowSendFax" runat="server" Visible="False" OnClick="lnkShowSendFax_click" >Send Fax</asp:LinkButton>       
        <span runat="server" id="spntunnel4" visible="false">|</span>
        <asp:LinkButton CausesValidation='false' ID="lnkShowSendSms" runat="Server" Visible="False" OnClick="lnkShowSendSms_click" >Send SMS</asp:LinkButton>
        <span runat="server" id="spntunnel2" visible="false">|</span>
        <asp:LinkButton CausesValidation='false' ID="lnkBroadcast" runat="Server" Visible="False" OnClick="lnkBroadcast_click" >Broadcast</asp:LinkButton>
        <span runat="server" id="spntunnel3" visible="false">|</span>
        <asp:LinkButton CausesValidation='false' ID="lnkReadInBoundSMS" runat="Server" Visible="False" OnClick="lnkReadInBoundSMS_click" >Read InBound SMS</asp:LinkButton>
        <span runat="server" id="spntunnel5">|</span>
        <asp:LinkButton CausesValidation='false' ID="lnkOfficeLocation" runat="Server" Visible="False" OnClick="lnkOfficeLocation_click" >Manage Office Location</asp:LinkButton>
         <span runat="server" id="spntunnel6" visible="false">|</span>
        <asp:LinkButton CausesValidation='false' ID="lnkTenderedLoads" runat="Server"  Visible="False" OnClick="lnkTenderedLoads_click" ></asp:LinkButton>
        <asp:Label ID="lblSmssendmessage" runat="server" Text="SMS sent successfully." Visible="false"></asp:Label>
        <asp:Label ID="lblFaxsendmessage" runat="server" Text="FAX sent successfully." Visible="false"></asp:Label>
        <asp:Label ID="lblNoReadSMS" runat="server" Text="No InBound Unread SMS." Visible="false"></asp:Label>
        <asp:Label ID="lblBroadcast" runat="server" Text="Broadcast successfully." Visible="false"></asp:Label>
    </td>
      <td class="welcome1" align="right">   
      <asp:LinkButton ID="lnkChassisCharges" runat="server" CausesValidation="false" OnClick="lnkChassisCharges_Click">Chassis Charges</asp:LinkButton>
       <asp:LinkButton ID="lnkMistakes" runat="server" CausesValidation="false" OnClick="lnkMistakes_Click">My Mistakes</asp:LinkButton>
       <asp:LinkButton ID="lnkMistakesRpt" runat="server" CausesValidation="false" OnClick="lnkMistakesRpt_Click">Mistakes Report</asp:LinkButton>
       <asp:Label ID="lblWelcome" runat="server">  Welcome ! </asp:Label>&nbsp;<asp:Label ID="lblUserId" runat="server" Text=""></asp:Label> &nbsp;
          <asp:LinkButton ID="lnlLogOut" runat="server" CausesValidation="false" OnClick="lnlLogOut_Click">Logout</asp:LinkButton>
      </td>
  </tr>  
  

  
  <tr>
    <td colspan="2" class="pagehead">
        <asp:Label ID="lblHeadText" runat="server" Text="Manager Dashboard"></asp:Label>
        &nbsp;- <asp:Label ID="lblHeadLocation" ForeColor="Black" runat="server" Text=""></asp:Label>
    </td>
  </tr>
  
  
  
 </tbody>
</table>
<table style="background:#fff; font-family:Arial; color:#555; font-size:12px; padding-left:5px;" width="100%"> 
 <tr id="trsendsmsforothere" runat="server" style="display:none;" >
  <td colspan="2" valign="middle">
   <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="SmsValidation" runat="server" />
   <asp:Label ID="lblErMessSms" runat="server" ForeColor="Red" 
                        Visible="False"></asp:Label>
  <asp:Panel ID="pnlsendsmsforothers" runat="server">
  <table style="background:#fff; font-family:Arial; color:#555; font-size:12px; padding-left:5px; vertical-align:top;" width="100%">
  <tr id="trdriverselection" runat="server" style="font-family:Arial; color:#555; font-size:12px;">
  <td  width="50%" valign="top" colspan="4" style="padding-left:5px;">
    <span style="margin-right:13px;"> Drivers :</span><asp:DropDownList ID="ddlDriver" runat="Server" OnSelectedIndexChanged="ddlDriver_SelectedIndexChanged" AutoPostBack="true" ></asp:DropDownList>
    <span style="margin-right:13px;"> Country :</span><asp:DropDownList ID="ddlCountry" runat="Server" >
    <asp:ListItem Text="USA" Value="1" Selected="True"></asp:ListItem>
    <asp:ListItem Text="India" Value="91"></asp:ListItem>
    </asp:DropDownList>
    
  </td>
  </tr>
  <tr>
    <td width="20%" valign="top">
    <table style="font-family:Arial; color:#555; font-size:12px;">
    <tr><td> To Name: </td><td><asp:TextBox ID="txtusername" runat="Server"></asp:TextBox> 
    <asp:RequiredFieldValidator ID="requsername" runat="server" ValidationGroup="SmsValidation" ControlToValidate="txtusername" Display="dynamic" ErrorMessage="Please Enter the To Name.">*</asp:RequiredFieldValidator></td></tr>
    <tr><td>To Type:</td><td><asp:DropDownList ID="ddlsendertype" runat="Server" style="width:100%">
   <asp:ListItem Text="Dispatcher" Value="Dispatcher"></asp:ListItem>
    <asp:ListItem Text="Driver" Value="Driver"></asp:ListItem>
     <asp:ListItem Text="Manager" Value="Manager"></asp:ListItem>
    <asp:ListItem Text="Customer" Value="Customer"></asp:ListItem>
    <asp:ListItem Text="Staff" Value ="Staff"></asp:ListItem>      
    </asp:DropDownList></td></tr>
    </table>
    </td>
    <td width="18%" valign="top">  To Number: <uc2:Phone ID="idPhoneNumber" runat="server" IsRequiredField="true" ValidationGroupforUser="SmsValidation"  /></td>
    <td width="40%" valign="top">  <span style="vertical-align:top;"> Message:</span> <asp:TextBox ID="txtmessageforsms" runat="Server" TextMode="multiLine" style=" margin-bottom:-1px; width:80%;height:50px" Wrap="true" onKeyUp="javascript:Check(this, 10);" onChange="javascript:Check(this, 10);"></asp:TextBox>
     <asp:RequiredFieldValidator ID="requserMessage" ValidationGroup="SmsValidation" runat="server" ControlToValidate="txtmessageforsms" Display="dynamic" ErrorMessage="Please Enter the Message." MaxLength="160">*</asp:RequiredFieldValidator>
    <asp:HiddenField ID="hfealimaxcount" runat="server" /></td>
     <td width="22%" valign="top">
      <asp:Button ID="lnksendsms" runat="server" Text="Send SMS" CssClass="btnstyle" ValidationGroup="SmsValidation" OnClick="lnksendsms_click" style="display:block;margin-bottom:5px;" />
      <asp:Button runat="Server" Text="   Cancel   " ID="lnkSmsCancel"  CssClass="btnstyle" CausesValidation="False" UseSubmitBehavior="False" OnClick="lnkSmsCancel_click" /> 
    </td>
  </tr>
  </table>
  </asp:Panel>
  </td>
  </tr>
  
  <tr id="trBroadcast" runat="server" style="display:none;">
  <td colspan="2" valign="middle">
   <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="BroadcastValidation" runat="server" />
   <asp:Label ID="lblBrodcastError" runat="server" ForeColor="Red" 
                        Visible="False"></asp:Label>
  <asp:Panel ID="pnlBroadcast" runat="server">
  <table style="background:#fff; font-family:Arial; color:#555; font-size:12px; padding-left:5px; vertical-align:top;" width="100%">
  <tr id="tr1" runat="server" style="font-family:Arial; color:#555; font-size:12px;">
  <td valign="top" style="padding-left:5px; width:10%;" align="right">
    <span style="margin-right:13px;" > Drivers :</span>
  </td>  
  <td valign="top" style="padding-left:5px; width:25%;" align="left">
    <asp:ListBox ID="ltbDrivers" runat="server" SelectionMode="Multiple" Height="100px" Width="260px" onchange="myFun(this);"></asp:ListBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="BroadcastValidation" runat="server" ControlToValidate="ltbDrivers" Display="dynamic" ErrorMessage="Please Select Drivers.">*</asp:RequiredFieldValidator>
    <br /><asp:TextBox ID="txtNames" runat="server" Text="" TextMode="MultiLine" ReadOnly="true" style=" margin-bottom:-1px; width:80%;height:30px" Wrap="true" Enabled="false"></asp:TextBox></td>
  
    <td valign="top"  align="right" style="width:10%;">  <span style="vertical-align:top;"> Message:</span> </td>
  <td valign="top"  align="left" style="width:30%;">  
  <asp:TextBox ID="txtBroadcast" runat="Server" TextMode="multiLine" style=" margin-bottom:-1px; width:80%;height:50px" Wrap="true" onKeyUp="javascript:Check(this, 10);" onChange="javascript:Check(this, 10);"></asp:TextBox>
     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="BroadcastValidation" runat="server" ControlToValidate="txtBroadcast" Display="dynamic" ErrorMessage="Please Enter the Message." MaxLength="160" >*</asp:RequiredFieldValidator>
       </td>
  <td valign="top" style="width:25%;" align="left">
      <asp:Button ID="lnkBroadcastsms" runat="server" Text="  Broadcast  " CssClass="btnstyle" ValidationGroup="BroadcastValidation" OnClick="lnkBroadcastsms_click" style="display:block;margin-bottom:5px;" />
      <asp:Button runat="Server" Text="    Cancel     " ID="lnkBroadcastCancel"  CssClass="btnstyle" CausesValidation="False" UseSubmitBehavior="False" OnClick="lnkBroadcastCancel_click" /> 
    </td>
  </tr>
  </table>
  </asp:Panel>
  
  </td>
  </tr>
  
  <tr id="trReadInBoundSMS" runat="server" style="display:none;" >
  <td colspan="2" valign="middle">
   <asp:Panel ID="pnlReadInBoundSMS" runat="server">
  <table style="background:#fff; font-family:Arial; color:#555; font-size:12px; padding-left:5px; vertical-align:top;" width="100%">
  <tr>
    <td width="20%" valign="top" style="font-size:medium;font-weight:bold">InBound SMS List</td>
    <td width="20%" valign="top"><asp:Button ID="btnRefresh" runat="server" Text="Refresh" OnClick="lnkReadInBoundSMS_click" CssClass="btnstyle" CausesValidation="False"/>
    <asp:Button ID="btnReadSMSClose" runat="server" Text="Close" OnClick="btnReadSMSClose_click" CssClass="btnstyle" CausesValidation="False" style="margin-left:20px"/></td>
    <td width="40%" valign="top"></td></tr>
     <tr>
  <td colspan="3" >
  <asp:DataGrid AutoGenerateColumns="false" ID="dggrid" Width="100%" runat="server" AllowPaging="true" CssClass="Grid" PageSize="10" OnPageIndexChanged="dggrid_PageIndexChanged">
  <PagerStyle Mode="NumericPages" HorizontalAlign="Center"/>
        <ItemStyle CssClass="GridItem" BorderColor="White" />
        <HeaderStyle CssClass="GridHeader" BorderColor="White" />
        <AlternatingItemStyle CssClass="GridAltItem" />
        <Columns>
        
        <asp:BoundColumn HeaderText="Message Id" DataField="nvar_MessageId"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="From #" DataField="nvar_From"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Message Text" DataField="nvar_Text" ItemStyle-Width="40%"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Received Date" DataField="date_ReceivedDate"></asp:BoundColumn>    
        <asp:TemplateColumn HeaderText="Action">
            <ItemTemplate>
            <asp:LinkButton id="lnkMakeRead" runat="server" Text="Mark as Read" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "bint_inboundSMSLogID")%>' OnClick="lnkMakeRead_click"></asp:LinkButton>               
             | <asp:LinkButton ID="lnkReply" runat="server" Text="Reply" CommandArgument= '<%# DataBinder.Eval(Container.DataItem, "bint_inboundSMSLogID")%>' CommandName='<%# DataBinder.Eval(Container.DataItem, "nvar_From")%>' OnClick="lnkReply_click"></asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateColumn>    
        </Columns>
  </asp:DataGrid>
  </td>
  </tr>
    </table>
   </asp:Panel></td>
    <td width="20%" valign="top"> </td>
  </tr>
  
  <tr id="trSendFax" runat="server" style="display:none;">
  <td colspan="2" valign="middle">
   <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="FaxValidation" runat="server" />
   <asp:Label ID="lblFaxError" runat="server" ForeColor="Red" 
                        Visible="False"></asp:Label>
  <asp:Panel ID="Panel1" runat="server">
<%--  <table style="background:#fff; font-family:Arial; color:#555; font-size:12px; padding-left:5px; vertical-align:top;" width="100%">
  <tr id="tr3" runat="server" style="font-family:Arial; color:#555; font-size:12px;">
  <td  width="200px" valign="top" align="left" style="padding-left:5px;">
    <span style="margin-right:15px;"> Country :</span><asp:DropDownList ID="ddlCountryFax" runat="Server" >
    <asp:ListItem Text="USA" Value="1" Selected="True"></asp:ListItem>
    <asp:ListItem Text="India" Value="91"></asp:ListItem>
    </asp:DropDownList>   
  </td>
  <td width="250px" valign="top" align="left" valign="top"> <span style="margin-right:5px;">  To Number : </span><uc2:Phone ID="PhFax" runat="server" IsRequiredField="true" ValidationGroupforUser="FaxValidation"  /></td>
  <td width="" valign="top" align="left" valign="top" align="left" rowspan="2">
      <asp:Button ID="lnkSendFax" runat="server" Text="Send FAX" CssClass="btnstyle" ValidationGroup="FaxValidation" OnClick="lnkSendFax_click" style="display:block;margin-bottom:5px;" />
      <asp:Button runat="Server" Text="   Cancel   " ID="lnkCancelFax"  CssClass="lnkstyle" CausesValidation="False" UseSubmitBehavior="False" OnClick="lnkCancelFax_click" /> 
    </td>
  </tr>
  <tr>
    <td width="400px" valign="top" align="left" style="padding-left:5px;" colspan="2">  <span style="margin-right:5px;">   Name/Description : </span><asp:TextBox ID="txtuUernameFax" runat="Server" TextMode="MultiLine" Height="50" Width ="300" MaxLength="255"></asp:TextBox> 
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="FaxValidation" ControlToValidate="txtuUernameFax" Display="dynamic" ErrorMessage="Please Enter the To Name.">*</asp:RequiredFieldValidator>
    </td>
    
    <td width="" valign="top" colspan="2">  <span style="vertical-align:top;"> File : </span><asp:FileUpload ID="fldUpload" runat="server" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="FaxValidation" runat="server" ControlToValidate="fldUpload" Display="dynamic" ErrorMessage="Please Select File.">*</asp:RequiredFieldValidator> </td>     
  </tr>
  </table>--%>
 <table width="100%" border="0" cellspacing="2" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333;">
  <tr>
    <td width="250" height="35" align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="110" align="left" valign="middle" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333;">Country : </td>
        <td align="left" valign="middle" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333;"><asp:DropDownList ID="ddlCountryFax" runat="Server" Width="100px">
        <asp:ListItem Text="USA" Value="1" Selected="True"></asp:ListItem>
        <asp:ListItem Text="India" Value="91"></asp:ListItem>
      </asp:DropDownList></td>
      </tr>
    </table></td>
    <td width="250" height="35" align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="100" align="left" valign="middle" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333;">To Number :</td>
        <td align="left" valign="middle" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333;"><uc2:Phone ID="PhFax" runat="server" IsRequiredField="true" ValidationGroupforUser="FaxValidation"  /></td>
      </tr>
    </table></td>
    <td height="35" align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="100" align="left" valign="middle" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333;">File Upload</td>
        <td align="left" valign="middle" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333;"><asp:FileUpload ID="fldUpload" runat="server" />
      <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="FaxValidation" runat="server" ControlToValidate="fldUpload" Display="dynamic" ErrorMessage="Please Select File.">*</asp:RequiredFieldValidator></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2" align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="110" align="left" valign="middle" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333;">Name/Description :</td>
        <td align="left" valign="middle"> <asp:TextBox ID="txtuUernameFax" runat="Server" TextMode="MultiLine" Height="50" Width ="350" MaxLength="255"></asp:TextBox>
      <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="FaxValidation" ControlToValidate="txtuUernameFax" Display="dynamic" ErrorMessage="Please Enter the Name/Description.">*</asp:RequiredFieldValidator></td>
        </tr>
    </table>      </td>
    <td align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
      <td width="100" align="left" valign="middle" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333;">&nbsp;</td>
        <td align="left" valign="middle" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333;"><asp:Button ID="lnkSendFax" runat="server" Text="Send FAX" CssClass="btnstyle" ValidationGroup="FaxValidation" OnClick="lnkSendFax_click" style="display:block;margin-bottom:5px;" />      
            <asp:Button runat="Server" Text="   Cancel   " ID="lnkCancelFax"  CssClass="lnkstyle" CausesValidation="False" UseSubmitBehavior="False" OnClick="lnkCancelFax_click" /></td>
      </tr>

    </table></td>
  </tr>
</table>
  </asp:Panel>
  </td>
  </tr>
 
  </table>       
  
<%--<table cellspacing="0" cellpadding="0" width="100%" border="0">
  <tbody>
  <tr>
    <td width="231" style="height: 78px"><IMG height="78" alt="Logo" src="../Images/logo.gif" 
    width="231"></td>
    <td valign=bottom style="height: 78px">
      <table cellspacing="0" cellpadding="0" width="100%" bgColor="#4c4c4c" 
        border="0"><tbody>
       <tr>
            <asp:PlaceHolder ID="plhImages" runat="server"></asp:PlaceHolder>
          <TD width="100%">&nbsp;</TD>
          <TD width=70><A 
            href="dashboard.aspx"><IMG 
            id="image1" height="32" alt=Loads src="../Images/loads.gif" width="70"
            border="0" name="image1"></A></TD>
          <TD width="88"><A 
            href="searchlocation.aspx"><IMG 
            id="image2" height="32" alt="Locations" src="../Images/locations.gif" 
            width="88" border=0 name="image2"></A></TD>
          <TD width=96><A 
            href="searchcustomer.aspx"><IMG 
            id=image3 height=32 alt=Customers src="../Images/customers.gif" 
            width=96 border=0 name=image3></A></TD>
          <TD width=79><A 
            href="searchcarrier.aspx"><IMG 
            id=image5 height=32 alt=Carriers src="../Images/carriers.gif" 
            width=79 border=0 name=image5></A></TD>
          <TD width=111><A 
            href="searchshipping.aspx"><IMG 
            height=32 alt=Shippingline src="../Images/shippingline.gif" 
            width=111 border=0></A></TD>
          <TD width=95><A 
            href="searchequipment.aspx"><IMG 
            id=image4 height=32 alt=Equipment src="../Images/equipment.gif" 
            width=95 border=0 name=image4></A></TD>
          <TD width=66><IMG id=image6  style="cursor:hand"
            onmouseover="MM_showMenu(window.mm_menu_0406015752_0,0,32,null,'image6')" 
            onmouseout="MM_startTimeout();" height="32" alt="Users" 
            src="../Images/users.gif" width="66" name="image6"></TD>
          <TD width="78"><A 
            href="reports.aspx"><IMG 
            height="32" alt="Reports" src="../Images/reports.gif" width="78" 
            border="0"></A></TD>
           </tr>
           </tbody></table>
           </td>
           </tr>
           <tr><td class="welcome" colspan="2">Welcome !
               <asp:Label ID="lblwelcome" runat="server"></asp:Label>
               <a href="login.aspx">Logout</a>
           </td></tr>
     </tbody></table>
  <script language="javascript">mmLoadMenus();</script>--%>
</BODY>
</html>