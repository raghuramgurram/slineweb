using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class DatePicker : System.Web.UI.UserControl
{
    public string Date
    {
        get 
        {
            if (dropMonth.Items.Count > 0 && dropDay.Items.Count > 0 && dropYear.Items.Count > 0)
            {
                if (ViewState["IsDefault"] != null)
                {
                    if (Convert.ToBoolean(ViewState["IsDefault"]))
                    {
                        if (dropMonth.SelectedIndex == 0 || dropDay.SelectedIndex == 0 || dropYear.SelectedIndex == 0)
                        {
                            return null;
                        }
                        return Convert.ToString(dropMonth.SelectedIndex) + "/" + Convert.ToString(dropDay.SelectedIndex) + "/" + Convert.ToString(dropYear.Items[dropYear.SelectedIndex].Text);
                    }
                    else
                        return Convert.ToString(dropMonth.SelectedIndex + 1) + "/" + Convert.ToString(dropDay.SelectedIndex + 1) + "/" + Convert.ToString(dropYear.Items[dropYear.SelectedIndex].Text);
                }
                else
                    return Convert.ToString(dropMonth.SelectedIndex + 1) + "/" + Convert.ToString(dropDay.SelectedIndex + 1) + "/" + Convert.ToString(dropYear.Items[dropYear.SelectedIndex].Text);
            }
            else
            {
                return string.Empty;
            }
        }
        set
        {
            if (value.Trim().Length > 0)
            {
                ViewState["Date"] = value.Trim();
                //SetDate(value.Trim());
            }
        }
    }
    public string SeperateCharater
    {
        get
        {
            return lblSeperateChar1.Text.Trim();
        }
        set
        {
            if (value.Trim().Length > 0)
            {
                lblSeperateChar1.Text = value.Trim();
                lblSeperateChar2.Text = value.Trim();
            }
        }
    }
    public bool IsDefault
    {
        set
        {
            ViewState["IsDefault"] = value;
        }
    }
    public bool IsValidDate
    {
        get
        {
            lblErrMessage.Visible = false;
            if (ViewState["IsDefault"] != null)
            {                
                if (Convert.ToBoolean(ViewState["IsDefault"]))
                {
                    if (ViewState["IsRequired"] != null)
                    {
                        if (!Convert.ToBoolean(ViewState["IsRequired"]))
                        {
                            if (dropYear.SelectedIndex == 0 && dropMonth.SelectedIndex == 0 && dropDay.SelectedIndex == 0)
                            {
                                return true;
                            }
                        }
                    }
                    else
                    {
                        if (dropYear.SelectedIndex == 0 && dropMonth.SelectedIndex == 0 && dropDay.SelectedIndex == 0)
                        {
                            return true;
                        }
                    }
                    if (dropYear.SelectedIndex == 0 || dropMonth.SelectedIndex == 0 || dropDay.SelectedIndex == 0)
                    {
                        lblErrMessage.Visible = true;
                        return false;
                    }
                    else
                        return true;
                }
                else
                    return true;
            }
            else return true;
        }
    }
    public bool DisplayError
    {
        set { lblErrMessage.Visible = value; }
    }
    public bool IsRequired
    {
        set
        {
            ViewState["IsRequired"] = value;
            cmpDay.Visible = value;
            cmpMonth.Visible = value;
            cmpYear.Visible = value;
            cmpDay.Enabled = value;
            cmpMonth.Enabled = value;
            cmpYear.Enabled = value; 
        }
    }
    public bool SetInitialDate
    {
        set
        {
            ViewState["SetInitialDate"] = value;
        }
    }
    public int CountPreviousYears
    {
        set
        {
            if (value != null)
                ViewState["CountPreviousYears"] = value;
            else
                ViewState["CountPreviousYears"] = 50;
        }
    }
    public int CountNextYears
    {
        set
        {
            if (value != null)
                ViewState["CountNextYears"] = value;
            else
                ViewState["CountNextYears"] = 50;
        }
    }
    public bool IsDOB
    {
        set
        {
            ViewState["IsDOB"] = value;
        }
    }

    public bool IsPrvYears
    {
        set
        {
            ViewState["IsPrvYears"] = value;
        }
    }
    public bool IsExpireDate
    {
        set { ViewState["IsExpireDate"] = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        lblErrMessage.Visible = false;
        //if (!Page.IsPostBack)
        //{
            //dropYear.Items.Clear();
            //for (int i = 50; i > 0; i--)
            //{
            //    ListItem lst = new ListItem(Convert.ToString(DateTime.Now.Date.Year - i), Convert.ToString(DateTime.Now.Date.Year - i));
            //    dropYear.Items.Add(lst);
            //    lst = null;
            //}
            //for (int i = 0; i < 50; i++)
            //{
            //    ListItem lst = new ListItem(Convert.ToString(DateTime.Now.Date.Year + i), Convert.ToString(DateTime.Now.Date.Year + i));
            //    dropYear.Items.Add(lst);
            //    lst = null;
            //}
              
            dropDay.Attributes.Add("onClick", "javascript:OnClickDay(" + dropMonth.ClientID + "," + dropDay.ClientID + "," + dropYear.ClientID + ")");
            dropMonth.Attributes.Add("onChange", "javascript:OnChangeMonth(" + dropMonth.ClientID + "," + dropDay.ClientID + "," + dropYear.ClientID + ")");
            dropYear.Attributes.Add("onChange", "javascript:OnChangeYear(" + dropMonth.ClientID + "," + dropDay.ClientID + "," + dropYear.ClientID + ")");
        //}
            if (Page.IsPostBack)
            {
                ViewState["YearIndex"] = dropYear.SelectedIndex;
                ViewState["MonthIndex"] = dropMonth.SelectedIndex;
                ViewState["DayIndex"] = dropDay.SelectedIndex;
            }

    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            dropYear.Items.Clear();
            for (int i = 50; i > 0; i--)
            {
                ListItem lst = new ListItem(Convert.ToString(DateTime.Now.Date.Year - i), Convert.ToString(DateTime.Now.Date.Year - i));
                dropYear.Items.Add(lst);
                lst = null;
            }
            for (int i = 0; i < 50; i++)
            {
                ListItem lst = new ListItem(Convert.ToString(DateTime.Now.Date.Year + i), Convert.ToString(DateTime.Now.Date.Year + i));
                dropYear.Items.Add(lst);
                lst = null;
            }
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {        
        if (Convert.ToBoolean(ViewState["IsDOB"]))
        {
            ViewState["CountPreviousYears"] = 120;
            ViewState["CountNextYears"] = 0;
        }
        else if(Convert.ToBoolean(ViewState["IsPrvYears"]))
        {
            if (ViewState["IsExpireDate"] == null)
                ViewState["CountNextYears"] = 2;
            ViewState["CountPreviousYears"] = DateTime.Now.Year-2006;
        }
        else
        {
            if(ViewState["IsExpireDate"]==null)
                ViewState["CountNextYears"] = 2;
            ViewState["CountPreviousYears"] = GetFromXML.PreviousYearsSelection;
        }
        if (ViewState["CountPreviousYears"] != null && ViewState["CountNextYears"] != null)
        {
            if (dropYear.Items.Count != 0)
            {
                dropYear.Items.Clear();
            }
            for (int i = Convert.ToInt32(ViewState["CountPreviousYears"]); i > 0; i--)
            {
                ListItem lst = new ListItem(Convert.ToString(DateTime.Now.Date.Year - i), Convert.ToString(DateTime.Now.Date.Year - i));
                dropYear.Items.Add(lst);
                lst = null;
            }
            for (int i = 0; i < Convert.ToInt32(ViewState["CountNextYears"]); i++)
            {
                ListItem lst = new ListItem(Convert.ToString(DateTime.Now.Date.Year + i), Convert.ToString(DateTime.Now.Date.Year + i));
                dropYear.Items.Add(lst);
                lst = null;
            }
        }
        if (dropYear.Items.Count == 0)
        {
            for (int i = 50; i > 0; i--)
            {
                ListItem lst = new ListItem(Convert.ToString(DateTime.Now.Date.Year - i), Convert.ToString(DateTime.Now.Date.Year - i));
                dropYear.Items.Add(lst);
                lst = null;
            }
            for (int i = 0; i < 50; i++)
            {
                ListItem lst = new ListItem(Convert.ToString(DateTime.Now.Date.Year + i), Convert.ToString(DateTime.Now.Date.Year + i));
                dropYear.Items.Add(lst);
                lst = null;
            }           
        }
        if (ViewState["IsDefault"] != null)
        {
            if (Convert.ToBoolean(ViewState["IsDefault"]))
            {
                if (!dropYear.Items.Contains(new ListItem("----")))
                    dropYear.Items.Insert(0, "----");
                if(!dropMonth.Items.Contains(new ListItem("---")))
                    dropMonth.Items.Insert(0, "---");
                if (!dropDay.Items.Contains(new ListItem("--")))
                    dropDay.Items.Insert(0, "--");
            }
        }
        if (ViewState["Date"] == null)
        {
            //this.SetCurrentDate();
            if (!Page.IsPostBack)
            {
                dropDay.SelectedIndex = 0;
                dropYear.SelectedIndex = 0;
                dropMonth.SelectedIndex = 0;
            }            
        }
        else
        {
            if (!lblErrMessage.Visible)
            {
                this.SetDate(Convert.ToString(ViewState["Date"]));
            }
        }
        if (ViewState["SetInitialDate"] != null)
        {            
            if (Convert.ToBoolean(ViewState["SetInitialDate"]))
            {
                SetCurrentDate();
            }
        }
        if (ViewState["YearIndex"] != null)
        {
            if (dropYear.Items.Count >= Convert.ToInt32(ViewState["YearIndex"]))
            {
                dropYear.SelectedIndex = Convert.ToInt32(ViewState["YearIndex"]);
                dropMonth.SelectedIndex = Convert.ToInt32(ViewState["MonthIndex"]);
                dropDay.SelectedIndex = Convert.ToInt32(ViewState["DayIndex"]);
            }
        }
    }
    public void SetCurrentDate()
    {
        if (ViewState["IsDefault"] != null)
        {
            if (Convert.ToBoolean(ViewState["IsDefault"]))
            {
                dropMonth.SelectedIndex = Convert.ToInt32(DateTime.Now.Date.Month);
                dropDay.SelectedIndex = Convert.ToInt32(DateTime.Now.Date.Day);
            }
            else
            {
                dropMonth.SelectedIndex = Convert.ToInt32(DateTime.Now.Date.Month)-1;
                dropDay.SelectedIndex = Convert.ToInt32(DateTime.Now.Date.Day)-1;
            }
        }
        else
        {
            dropMonth.SelectedIndex = Convert.ToInt32(DateTime.Now.Date.Month) - 1;
            dropDay.SelectedIndex = Convert.ToInt32(DateTime.Now.Date.Day) - 1;
        }

        ListItem lst1 = new ListItem(Convert.ToString(DateTime.Now.Date.Year), Convert.ToString(DateTime.Now.Date.Year));
        dropYear.SelectedIndex = dropYear.Items.IndexOf(lst1);
        lst1 = null;

    }
    public void SetDate(DateTime dat)
    {
        this.SetDate(dat.ToShortDateString());
    }
    public void SetDate(string strDate)
    {
        string[] strtemp = strDate.Split(' ');
        if (strtemp.Length > 0)
        {
            string[] values = strtemp[0].Trim().Split('/');
            if (values.Length == 3)
                this.SetDate(values[0], values[1], values[2]);
            values = null;
        }
        strtemp = null;
    }
    public void SetDate(string strMonth, string strDay, string strYear)
    {
        try
        {
            this.SetDate(Convert.ToInt32(strMonth), Convert.ToInt32(strDay), Convert.ToInt32(strYear));
        }
        catch
        {
            //Response.Write("<script>alert('Supplied Date is Wrong')</script>");
            SetCurrentDate();
        }
    }
    public void SetDate(int iMonth, int iDay, int iYear)
    {
        if (ViewState["IsDefault"] != null)
        {
            if (Convert.ToBoolean(ViewState["IsDefault"]))
            {
                dropMonth.SelectedIndex = iMonth;
                dropDay.SelectedIndex = iDay;
            }
            else
            {
                dropMonth.SelectedIndex = iMonth - 1;
                dropDay.SelectedIndex = iDay - 1;
            }
        }
        else
        {
            dropMonth.SelectedIndex = iMonth-1;
            dropDay.SelectedIndex = iDay-1;
        }
        ListItem lst1 = new ListItem(Convert.ToString(iYear), Convert.ToString(iYear));
        dropYear.SelectedIndex = dropYear.Items.IndexOf(lst1);
        lst1 = null;
    }

}
