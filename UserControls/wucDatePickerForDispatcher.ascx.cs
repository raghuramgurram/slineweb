﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class UserControls_wucDatePickerForDispatcher : System.Web.UI.UserControl
{
    private int Day
    {
        get
        {            
             return int.Parse(ddlDay.SelectedItem.Value);
        }
        set
        {
            if (ddlDay.Items.Count == 0)
                this.PopulateDay();
            ddlDay.SelectedValue = value.ToString();
        }
    }

    private int Month
    {
        get
        {
            return int.Parse(ddlMonth.SelectedItem.Value);
        }
        set
        {
            ddlMonth.SelectedValue=value.ToString();
        }
    }

    private int Year
    {
        get
        {
            return int.Parse(ddlYear.SelectedItem.Value);
        }
        set
        {
            ddlYear.SelectedValue = value.ToString();
        }
    }

    public DateTime SelectedDate
    {
        get
        {
            try
            {
                return DateTime.Parse(this.Month + "/" + this.Day + "/" + this.Year);
            }
            catch
            {
                return DateTime.MinValue;
            }
        }
        set
        {
            if (!value.Equals(DateTime.MinValue))
            {
                this.Year = value.Year;
                this.Month = value.Month;
                this.Day = value.Day;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PopulateMonth();
            PopulateYear();
        }
    }

    private void PopulateDay()
    {
        string value = ddlDay.SelectedValue == null ? "0" : ddlDay.SelectedValue;        
        ddlDay.Items.Clear();
        ListItem lt = new ListItem();
        lt.Text = "--";
        lt.Value = "0";
        ddlDay.Items.Add(lt);
        int days = 31;
        if(this.Year!=0 && this.Month!=0)
        days = DateTime.DaysInMonth(this.Year, this.Month);
        for (int i = 1; i <= days; i++)
        {
            lt = new ListItem();
             lt.Text = i.ToString();
            lt.Value = i.ToString();
            ddlDay.Items.Add(lt);
        }
        try
        {
            ddlDay.SelectedValue = value;
        }
        catch
        {
            ddlDay.SelectedValue = "1";
        }        
    }

    private void PopulateMonth()
    {
        ddlMonth.Items.Clear();
        ListItem lt = new ListItem();
        lt.Text = "--";
        lt.Value = "0";
        ddlMonth.Items.Add(lt);
        for (int i = 1; i <= 12; i++)
        {
            lt = new ListItem();
            //taking any year, just to create a date for generating Month
            lt.Text = Convert.ToDateTime(i.ToString() + "/1/1900").ToString("MMM");
            lt.Value = i.ToString();
            ddlMonth.Items.Add(lt);
        }
    }

    private void PopulateYear()
    {
       
        ddlYear.Items.Clear();
        ListItem lt = new ListItem();
        lt.Text = "----";
        lt.Value = "0";
        ddlYear.Items.Add(lt);
        for (int i = 2013; i <= 2050; i++)
        {
            lt = new ListItem();
            lt.Text = i.ToString();
            lt.Value = i.ToString();
            ddlYear.Items.Add(lt);
        }
    }

    protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.PopulateDay();
    }

    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.PopulateDay();
    }   
}