<%@ Page Language="C#" MasterPageFile="~/Manager/MasterPage.master" AutoEventWireup="true" CodeFile="NewEquipment.aspx.cs" Inherits="NewEquipment" Title="Untitled Page" %>

<%@ Register Src="~/UserControls/USState.ascx" TagName="USState" TagPrefix="uc4" %>

<%@ Register Src="~/UserControls/Phone.ascx" TagName="Phone" TagPrefix="uc3" %>

<%@ Register Src="~/UserControls/TimePicker.ascx" TagName="TimePicker" TagPrefix="uc2" %>

<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblform1">
        <tr>
          <td>
              <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="507px" />          <asp:Label ID="lblNameError" runat="server" Font-Bold="False" ForeColor="Red" Text="Equipment name already exists." Visible="False"/></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="3" cellspacing="0" id="tblform">
        <tr>
          <th colspan="4">Equipment</th>
        </tr>
        <tr>
          <td width="15%" align="right">Equipment Name   : </td>
          <td width="25%">
              <asp:TextBox ID="txteqpname" runat="server" Width="130px" MaxLength="150"></asp:TextBox>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txteqpname"
                  Display="Dynamic" ErrorMessage="Please enter equipment name.">*</asp:RequiredFieldValidator></td>
          <td width="15%" align="right">Lessor : </td>
          <td>
              <asp:CheckBox ID="cblessor" runat="server" />&nbsp;
          </td>
        </tr>
        <tr>
          <td align="right" valign="top">Type    : </td>
          <td>
              <asp:DropDownList ID="ddltype" runat="server">
                  <asp:ListItem>---select---</asp:ListItem>
              </asp:DropDownList>
              <br />
                    <asp:CheckBox ID="cbreef" runat="server" Text="Reefer : " TextAlign="Left"/> </td>
          <td align="right">Owner : </td>
          <td>
              <asp:TextBox ID="txtowner" runat="server" MaxLength="100"></asp:TextBox>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                  ControlToValidate="txtowner" Display="Dynamic" ErrorMessage="Please enter a valid number."
                  ValidationExpression="\d*">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr>
          <td align="right" style="height: 27px">Status : </td>
          <td style="height: 27px">
              <asp:DropDownList ID="ddlstatus" runat="server">
                  <asp:ListItem Value="1">Active</asp:ListItem>
                  <asp:ListItem Value="0">In Active</asp:ListItem>
              </asp:DropDownList></td>
          <td align="right" style="height: 27px">Last Bit Inspection   : </td>
          <td style="height: 27px">
              <%--<asp:TextBox ID="txtlastbit" runat="server"></asp:TextBox>--%>
              <uc1:DatePicker ID="txtlastbit" runat="server" IsDefault="true" IsRequired="false" CountNextYears="2" CountPreviousYears="0" />
          </td>
        </tr>
        <tr>
          <td align="right" valign="top">State : </td>
          <td valign="top">
              <asp:DropDownList ID="ddlstate" runat="server">
                  <asp:ListItem Value="1">Active</asp:ListItem>
                  <asp:ListItem Value="0">In Active</asp:ListItem>
              </asp:DropDownList></td>
          <td align="right" valign="top">Tag Expiration  : </td>
          <td>
              <%--<asp:TextBox ID="txttag" runat="server"></asp:TextBox>--%>
              <uc1:DatePicker ID="txttag" runat="server" IsDefault="true" IsRequired="true" CountNextYears="2" CountPreviousYears="0" />
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="3" cellspacing="0" id="tblform">
        <tr>
          <th colspan="2">Last Location </th>
        </tr>
        <tr>
          <td width="15%" align="right" style="height: 24px">City & State    : </td>
          <td style="width: 649px">
              <asp:TextBox ID="txtcity" runat="server" MaxLength="200"></asp:TextBox>&nbsp;
              <uc4:USState ID="ddlcity" runat="server" IsRequired="false" Visible="true" />
          </td>
        </tr>
        <tr>
          <td align="right">Date & Time     : </td>
          <td style="width: 649px">
              <%--<asp:TextBox ID="txtdate" runat="server"></asp:TextBox>--%>
              <%--<asp:DropDownList ID="ddldate" runat="server">
                  <asp:ListItem>---select---</asp:ListItem>
              </asp:DropDownList>--%>
              <uc1:DatePicker ID="txtdate" runat="server" IsDefault="true" IsRequired="false" CountNextYears="2" CountPreviousYears="0" />
              <uc2:TimePicker ID="ddldate" runat="server" ReqFieldValidation="false" />
          </td>
        </tr>
      </table><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="3" cellspacing="0" id="tblform">
        <tr>
          <th colspan="2">Mileage Information </th>
        </tr>
        <tr>
          <td width="15%" align="right">Initial Odometer     : </td>
          <td>
              <asp:TextBox ID="txtinitial" runat="server" MaxLength="18"></asp:TextBox>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtinitial"
                  Display="Dynamic" ErrorMessage="Please enter a valid odometer." ValidationExpression="\d*">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr>
          <td align="right">Current Odometer      : </td>
          <td>
              <asp:TextBox ID="txtcurrent" runat="server" MaxLength="18"></asp:TextBox>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtcurrent"
                  Display="Dynamic" ErrorMessage="Please enter a valid odometer." ValidationExpression="\d*">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr>
          <td align="right">Total Miles : </td>
          <td>
              <asp:TextBox ID="txtmiles" runat="server" MaxLength="18"></asp:TextBox>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtmiles"
                  Display="Dynamic" ErrorMessage="Please enter a valid miles." ValidationExpression="\d*">*</asp:RegularExpressionValidator></td>
        </tr>
      </table><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="4" cellspacing="0" id="tblform">
        <tr>
          <th colspan="4" style="height: 22px">Equipment Details (<strong>&nbsp;<asp:LinkButton ID="lnlInsurenceEqup" runat="server" Text="Insurance Information" OnClick="lnlInsurenceEqup_Click" ForeColor="Chocolate" CausesValidation="false"/>&nbsp;</strong> ) </th>
        </tr>
        <tr>
          <td width="15%" align="right">License #    :</td>
          <td width="25%">
              <asp:TextBox ID="txtlicense" runat="server" MaxLength="100"></asp:TextBox></td>
          <td width="15%" align="right">Model :</td>
          <td width=""45%">
              <asp:TextBox ID="txtmodel" runat="server" MaxLength="100"></asp:TextBox>
              &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;
              Year:&nbsp;<asp:TextBox ID="txtyear" runat="server" MaxLength="4"></asp:TextBox>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtyear"
                  Display="Dynamic" ErrorMessage="Please enter a valid year." ValidationExpression="\d*">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr>
          <td align="right" width="15%">VIN #     :</td>
          <td width="25%"> 
              <asp:TextBox ID="txtvin" runat="server" MaxLength="100"></asp:TextBox></td>
          <td align="right" width="15%">Weight   : </td>   
          <td width=""><asp:TextBox ID="txtweight" runat="server" MaxLength="15"></asp:TextBox>&nbsp; 
              <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtinitial"
                  Display="Dynamic" ErrorMessage="Please enter a valid odometer." ValidationExpression="\d*">*</asp:RegularExpressionValidator>
              Height :<asp:TextBox ID="txtheight" runat="server" MaxLength="15"></asp:TextBox>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtheight"
                  Display="Dynamic" ErrorMessage="Please enter a valid height." ValidationExpression="\d*">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr>
          <td align="right">Plate# : </td>
          <td>
              <asp:TextBox ID="txtplate" runat="server" MaxLength="100"></asp:TextBox></td>
          <td align="right">Tire Size   :</td>
          <td style="width: 246px">
              <asp:TextBox ID="txttiresize" runat="server" MaxLength="30"></asp:TextBox></td>
        </tr>
        <tr>
          <td align="right">Colour : </td>
          <td>
              <asp:TextBox ID="txtcolour" runat="server" MaxLength="100"></asp:TextBox></td>
          <td align="right">Make : </td>
          <td style="width: 246px">
              <asp:TextBox ID="txtmake" runat="server" MaxLength="100"></asp:TextBox></td>
        </tr>
        <tr>
          <td align="right">Title : </td>
          <td>
              <asp:TextBox ID="txttitle" runat="server" MaxLength="100"></asp:TextBox></td>
          <td align="right"></td>
          <td style="width: 246px"></td>
        </tr>
        <tr>
          <td align="right" valign="top">Date Bought  : </td>
          <td valign="top">
              <%--<asp:TextBox ID="txtbought" runat="server"></asp:TextBox>--%>
              <uc1:DatePicker ID="txtbought" runat="server" IsDefault="true" IsRequired="false" />
          </td>
          <td align="right" valign="top"></td>
          <td style="width: 246px"></td>
        </tr>
        <tr>
          <td align="right" valign="top">Notes :</td>
          <td valign="top">
              <asp:TextBox ID="txtnotes" runat="server" TextMode="MultiLine" MaxLength="1000" Width="131px"></asp:TextBox></td>
          <td align="right" valign="top"></td>
          <td style="width: 246px"></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="3" cellspacing="0" id="tblform">
        <tr>
          <th colspan="2">Financial Details </th>
        </tr>
        <tr id="row">
          <td align="right" width="8%">Cost($)     : </td>
          <td width="40%">
             <asp:TextBox ID="txtcost" runat="server" MaxLength="18"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator
                 ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtcost" Display="Dynamic"
                 ErrorMessage="Please enter a valid cost." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator>&nbsp;&nbsp;&nbsp;&nbsp;Down($) :
              <asp:TextBox ID="txtdown" runat="server" MaxLength="18"></asp:TextBox>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtdown"
                  Display="Dynamic" ErrorMessage="Please enter a valid down amount." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr id="altrow">
          <td align="right" style="height: 20px">Current Loan($)      : </td>
            <td width="45%" style="height: 20px">
              <asp:TextBox ID="txtloan" runat="server" MaxLength="18"></asp:TextBox>
              &nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server"
                    ControlToValidate="txtloan" Display="Dynamic" ErrorMessage="Please enter a valid loan."
                    ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator>
                &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
            Loan# :
              <asp:TextBox ID="txtlon" runat="server" MaxLength="100"></asp:TextBox></td>
        </tr>
        <tr id="row">
          <td align="right">Creditor Name  : </td>
          <td>
              <asp:TextBox ID="txtcreditor" runat="server" MaxLength="100"></asp:TextBox></td>
        </tr>
        <tr id="altrow">
          <td align="right">Creditor Phone  :</td>
          <td>
              <%--<asp:TextBox ID="txtcreditorph" runat="server" MaxLength="18"></asp:TextBox>--%>
              <uc3:Phone ID="txtcreditorph" runat="server" />
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
        <tr>
          <td align="right">
              &nbsp;<asp:Button ID="btnaddeqp" runat="server" Text="Add Equipment" CssClass="btnstyle" OnClick="btnSave_Click"/>&nbsp;
                            <asp:Button ID="btnCancel" runat="server" CssClass="btnstyle" Text="Cancel" CausesValidation="false" UseSubmitBehavior="false" OnClick="btnCancel_Click"/>

              <%--<asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="btnstyle"  CausesValidation="False" UseSubmitBehavior="False"/>--%></td>
        </tr>
      </table>
    
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../Images/pix.gif" alt="" width="1" height="10" /></td>
        </tr>
    </table></td>
  </tr>
</table>
</asp:Content>

