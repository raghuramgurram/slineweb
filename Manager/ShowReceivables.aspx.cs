using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class showreceivables : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Load Charges";
        if (!Page.IsPostBack)
        {
            string[] strQueryStrings = null;
           
            if (Request.QueryString.Count > 0)
            {
                strQueryStrings = Convert.ToString(Request.QueryString[0]).Split('^');
                ViewState["ID"] = Convert.ToInt64(strQueryStrings[0]);
                FillDetails(Convert.ToInt64(ViewState["ID"]));
                BarReceivables.HeaderText = "Load Number : " + Convert.ToString(ViewState["ID"]);
                BarReceivables.LinksList = "Show Payables;Accessorial Charges;Upload Accepted Accessorial Charges";
                BarReceivables.PagesList = "ShowPayables.aspx?" + Convert.ToInt64(ViewState["ID"]) + ";AccessorialCharges.aspx?" + Convert.ToInt64(ViewState["ID"]) + ";UploadAccessorial.aspx?" + Convert.ToInt64(ViewState["ID"]);
                Session["BackPage5"]="ShowReceivables.aspx?"+Convert.ToInt64(ViewState["ID"]);
            }
        }
    }
    private void FillDetails(long ID)
    {
        //string strQuery="SELECT [num_GrossAmount],[num_AdvanceReceived],[num_Dryrun],[num_FuelSurchargeAmount],[num_SurchargeAmount],[num_PierTermination],[bint_ExtraStops],[num_LumperCharges],"+
        //                "[num_DetentionCharges],[num_ChassisSplit],[num_ChassisRent],[num_Pallets],[num_ContainerWashout],[num_YardStorage],[num_RampPullCharges],[num_TriaxleCharge],[num_TransLoadCharges],"+
        //                "[num_HeavyLightScaleCharges],[num_ScaleTicketCharges],[num_OtherCharges1],[num_OtherCharges2],[num_OtherCharges3],[num_OtherCharges4],[nvar_OtherChargesReason],"+
        //                "isnull([num_GrossAmount],0)+isnull([num_AdvanceReceived],0)+isnull([num_Dryrun],0)+isnull([num_FuelSurchargeAmount],0)+isnull([num_SurchargeAmount],0)+isnull([num_PierTermination],0)+isnull([bint_ExtraStops],0)+isnull([num_LumperCharges],0)+" +
        //                "isnull([num_DetentionCharges],0)+isnull([num_ChassisSplit],0)+isnull([num_ChassisRent],0)+isnull([num_Pallets],0)+isnull([num_ContainerWashout],0)+isnull([num_YardStorage],0)+isnull([num_RampPullCharges],0)+isnull([num_TriaxleCharge],0)+isnull([num_TransLoadCharges],0)+" +
        //                "isnull([num_HeavyLightScaleCharges],0)+isnull([num_ScaleTicketCharges],0)+isnull([num_OtherCharges1],0)+isnull([num_OtherCharges2],0)+isnull([num_OtherCharges3],0)+isnull([num_OtherCharges4],0) AS 'Total'" +
        //                "FROM [eTn_Receivables] where [bint_LoadId]=" + ID;
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetReceivablesDetails", new object[] { ID });
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtGrossAmount.Text = Convert.ToString(ds.Tables[0].Rows[0][0]);
                txtAdvanceReceived.Text = Convert.ToString(ds.Tables[0].Rows[0][1]);
                txtDryRun.Text = Convert.ToString(ds.Tables[0].Rows[0][2]);
                txtFuelSurcharge.Text = "";
                txtAmount.Text = Convert.ToString(ds.Tables[0].Rows[0][4]);
                txtPierTermination.Text = Convert.ToString(ds.Tables[0].Rows[0][5]);
                txtextraStops.Text = Convert.ToString(ds.Tables[0].Rows[0][6]);
                txtLumperCharges.Text = Convert.ToString(ds.Tables[0].Rows[0][7]);
                txtDetentionCharges.Text = Convert.ToString(ds.Tables[0].Rows[0][8]);
                txtChassisSplit.Text = Convert.ToString(ds.Tables[0].Rows[0][9]);
                txtChassisRent.Text = Convert.ToString(ds.Tables[0].Rows[0][10]);
                txtPallets.Text = Convert.ToString(ds.Tables[0].Rows[0][11]);
                txtContainerWashOut.Text = Convert.ToString(ds.Tables[0].Rows[0][12]);
                txtYardStorage.Text = Convert.ToString(ds.Tables[0].Rows[0][13]);
                txtRampPullCharges.Text = Convert.ToString(ds.Tables[0].Rows[0][14]);
                txtTriaxle.Text = Convert.ToString(ds.Tables[0].Rows[0][15]);
                txtTransLoad.Text = Convert.ToString(ds.Tables[0].Rows[0][16]);
                txtHeavyLight.Text = Convert.ToString(ds.Tables[0].Rows[0][17]);
                txtScaleTicketCharges.Text = Convert.ToString(ds.Tables[0].Rows[0][18]);
                txtCharges1.Text = Convert.ToString(ds.Tables[0].Rows[0][19]);
                txtcharges2.Text = Convert.ToString(ds.Tables[0].Rows[0][20]);
                txtChrges3.Text = Convert.ToString(ds.Tables[0].Rows[0][21]);
                txtCharges4.Text = Convert.ToString(ds.Tables[0].Rows[0][22]);
                if (Convert.ToString(ds.Tables[0].Rows[0][23]) != "0")
                    txtOtherChargesReason.Text = Convert.ToString(ds.Tables[0].Rows[0][23]);
                else
                    txtOtherChargesReason.Text = "";
                lblTotal.Text = Convert.ToString(ds.Tables[0].Rows[0][24]);
                lblTotaltext.Visible = true;
                ViewState["Mode"] = "Edit";
            }
            else
            {
                lblTotaltext.Visible = false;
                ViewState["Mode"] = "New";
            }
        }
        else
        {
            lblTotaltext.Visible = false;
            ViewState["Mode"] = "New";
        }
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    protected void btnCancle_Click(object sender, EventArgs e)
    {
        CheckBackPage();
        Response.Redirect("~/Manager/NewLoad.aspx?" + Convert.ToString(ViewState["ID"]));
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (txtFuelSurcharge.Text.Trim().Length > 0 && txtAmount.Text.Trim().Length > 0)
            {
                Response.Write("<script>alert('You can not enter both Fuel surcharge percentage and Fuel surcharge amount.Enter only one.')</script>");
                return;
            }
            if (txtFuelSurcharge.Text.Trim().Length > 0 && txtGrossAmount.Text.Trim().Length==0)
            {
                Response.Write("<script>alert('Enter Gross amount to calculate Fuel surcharge percentage.')</script>");
                return;
            }
            if (txtFuelSurcharge.Text.Trim().Length > 0 && txtGrossAmount.Text.Trim().Length > 0)
            {
                txtAmount.Text = Convert.ToString((Convert.ToDouble(txtGrossAmount.Text.Trim()) / 100) * Convert.ToDouble(txtFuelSurcharge.Text.Trim()));
                txtFuelSurcharge.Text = "0";
            }
            if (ViewState["Mode"] != null)
            {
                object[] objParams = new object[] {Convert.ToInt64(ViewState["ID"]),txtGrossAmount.Text,txtAdvanceReceived.Text,txtDryRun.Text.Trim(),txtFuelSurcharge.Text.Trim(),txtAmount.Text.Trim(),txtPierTermination.Text.Trim(),txtextraStops.Text.Trim(),txtLumperCharges.Text.Trim(),
                                            txtDetentionCharges.Text.Trim(),txtChassisSplit.Text.Trim(),txtChassisRent.Text.Trim(),txtPallets.Text.Trim(),txtContainerWashOut.Text.Trim(),txtYardStorage.Text.Trim(),txtRampPullCharges.Text.Trim(),txtTriaxle.Text.Trim(),
                                            txtTransLoad.Text.Trim(),txtHeavyLight.Text.Trim(),txtScaleTicketCharges.Text.Trim(),txtCharges1.Text.Trim(),txtcharges2.Text.Trim(),txtChrges3.Text.Trim(),txtCharges4.Text.Trim(),txtOtherChargesReason.Text.Trim() };
                CheckNull(ref objParams);
                if (ViewState["ID"] != null)
                {
                    if (string.Compare(Convert.ToString(ViewState["Mode"]), "Edit", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                    {
                        if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Update_Receivables",objParams)>0)
                        {
                            CheckBackPage();
                            Response.Redirect("~/Manager/NewLoad.aspx?" + Convert.ToString(ViewState["ID"]));
                        }
                    }
                    else if (string.Compare(Convert.ToString(ViewState["Mode"]), "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                    {
                        if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Add_Receivables", SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Add_Receivables", objParams)) > 0)
                        {
                            CheckBackPage();
                            Response.Redirect("~/Manager/NewLoad.aspx?" + Convert.ToString(ViewState["ID"]));
                        }
                    }
                }
            }
        }
    }

    private void CheckNull(ref object[] objParams)
    {
        for (int i = 0; i < objParams.Length;i++ )
        {
            if (i == objParams.Length - 1)
            {
                if (Convert.ToString(objParams[i]).Trim().Length == 0)
                    objParams[i] = "";
            }
            else if (objParams[i].ToString().Trim().Length == 0)
            {
                objParams[i] = "0";
            }
        }
    }
    private void CheckBackPage()
    {
        if (Session["BackPage4"] != null)
        {
            if (Convert.ToString(Session["BackPage4"]).Trim().Length > 0)
            {
                string strTemp = Convert.ToString(Session["BackPage4"]).Trim();
                Session["BackPage4"] = null;
                Response.Redirect(strTemp);
            }
        }
    }
}
