<%@ Page Language="C#" MasterPageFile="~/Customer/CustomerMaster.master" AutoEventWireup="true" CodeFile="CustomerPayments.aspx.cs" Inherits="CustomerPayments" Title="S Line Transport Inc" %>

<%@ Register Src="../UserControls/PaymentsGrid.ascx" TagName="PaymentsGrid" TagPrefix="uc4" %>

<%@ Register Src="../UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc3" %>

<%@ Register Src="../UserControls/Grid.ascx" TagName="Grid" TagPrefix="uc2" %>

<%@ Register Src="../UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script language="javascript" type="text/javascript">

</script>

<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblbox1">
          <tr>
            <td  bgcolor="#FFFFFF"  align="left" width="65%">&nbsp;From :<uc1:DatePicker ID="datFromDate" runat="server" IsDefault="false" IsRequired="false" SetInitialDate="true" CountNextYears="2" CountPreviousYears="0" />
                &nbsp;To :<uc1:DatePicker ID="datToDate" runat="server" IsDefault="false" IsRequired="false" SetInitialDate="true" CountNextYears="2" CountPreviousYears="0" />
                &nbsp;
                <asp:Button ID="btnShow" runat="server" CssClass="btnstyle" Text="Show" UseSubmitBehavior="False" OnClick="btnShow_Click" /></td>
            <td align="right" bgcolor="#FFFFFF" width="35%">Track Payment:
                <asp:TextBox ID="txtSearchText" runat="server" Width="70px" />
                <asp:DropDownList ID="ddlTypes" runat="server" >
                    <asp:ListItem Selected="True">Load#</asp:ListItem>
                    <asp:ListItem>Ref#</asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="btnSearch" runat="server" CssClass="btnstyle" Text="Search" UseSubmitBehavior="False" OnClick="btnSearch_Click" /></td>
          </tr>
        </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../images/pix.gif" alt="" width="1" height="3" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td>
              <%--<uc3:GridBar ID="barHeader" runat="server" HeaderText=""/>--%>
              <table id="GridBar" border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr>
                      <td>
                          <asp:Label ID="lblText" Text="Pending Payments" runat="server"></asp:Label>
                      </td>
                      <td align="right">
                          <asp:Label ID="lblTotalDisplay" runat="server"></asp:Label>
                      </td>
                  </tr>
              </table>
              
              
          </td>
        </tr> 
        </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">     
        <tr>
          <td>
              <uc4:PaymentsGrid ID="grdCurrent" runat="server" />
          
          </td>
        </tr>
    </table>    
    </td>
  </tr>
</table>
</asp:Content>

