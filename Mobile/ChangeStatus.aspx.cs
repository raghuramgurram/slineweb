using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

public partial class Mobile_ChangeStatus : System.Web.UI.Page
{
    #region pageLoad
    public loadStatus status
    {
        get
        {
            if (ViewState[Keys.LOAD_STATUS_CHANGE] == null)
            {
                loadStatus stat = loadStatus.Assigned;
                ViewState[Keys.LOAD_STATUS_CHANGE] = stat;
            }
            return (loadStatus)ViewState[Keys.LOAD_STATUS_CHANGE];
        }
        set
        {
            ViewState[Keys.LOAD_STATUS_CHANGE] = value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Keys.LOGIN_ID_KEY] == null || Session[Keys.EMPLOY_ID] == null)
        {
            Response.Redirect(Keys.PAGE_LOAGIN);
        }
        
        if (!IsPostBack)
        {
            try
            {
                SetMinValue();
            }

            catch { }
            if (Session[Keys.LOAD_ID] != null)
            {
                try
                {
                    LoadDetailsBase load = new LoadDetailsBase();
                    DataTable loaddetails = load.loadloaddetails(Convert.ToInt32(Session[Keys.LOAD_ID]));
                    if (loaddetails != null)
                    {
                        int statue = 0;
                        if (loaddetails.Rows[0]["bint_LoadStatusId"] != null) statue = Convert.ToInt32(loaddetails.Rows[0]["bint_LoadStatusId"].ToString()); else statue = 0;
                        loadStatus value = ((loadStatus)statue);
                        string status0 = string.Empty;
                        if (value == loadStatus.En_route)
                            status0 = value.ToString().Replace("_", "-");
                        else
                            status0 = value.ToString().Replace("_", " ");

                        MuPageMenu.SubTitle = ConfigurationManager.AppSettings["CompanyName"].ToString()+ " - Change Load Status For Load -" + Session[Keys.LOAD_ID].ToString() + ", Container #: " + loaddetails.Rows[0]["nvar_Container"].ToString() + ", Current Status: " + status0;
                        MuPageMenu.LoadMenu();
                        ShowRediobuttons(value);
                    }
                }
                catch (Exception ex)
                {

                }

            }
            else
            {
                Response.Redirect(Keys.PAGE_LOAD_HOME);
            }

        }

        //SLine 2016 Updates
       
    }
    protected void ShowRediobuttons(loadStatus status)
    {
        switch (status)
        {
            case loadStatus.Assigned:
                break;
            case loadStatus.Pickedup:
                lblRadpickedup.Visible = false;
                radpickedup.Visible = false;
                break;
            case loadStatus.En_route:
                lblRadpickedup.Visible = false;
                radpickedup.Visible = false;
                ChangeLoadStatus Bl = new ChangeLoadStatus();
                if (!Bl.LoadIsReachedDestination(Convert.ToInt32(Session[Keys.LOAD_ID])))
                {
                    radrecheddestination.Visible = false;
                    lblradrecheddestination.Visible = false;
                }
                if (!Bl.LoadIsReachedDestinationandwait(Convert.ToInt32(Session[Keys.LOAD_ID])))
                {
                    lblraddriveronwaiting.Visible = false;
                    raddriveronwaiting.Visible = false;
                }
                break;
            case loadStatus.Reached_Destination:
                lblRadpickedup.Visible = false;
                radpickedup.Visible = false;
                lblradrecheddestination.Visible = false;
                radrecheddestination.Visible = false;
                break;
            case loadStatus.Driver_on_Waiting:
                lblRadpickedup.Visible = false;
                radpickedup.Visible = false;
                lblradrecheddestination.Visible = false;
                radrecheddestination.Visible = false;
                lblraddriveronwaiting.Visible = false;
                raddriveronwaiting.Visible = false;
                break;
            case loadStatus.Delivered:
                lblRadpickedup.Visible = false;
                radpickedup.Visible = false;
                lblradrecheddestination.Visible = false;
                radrecheddestination.Visible = false;
                lblraddriveronwaiting.Visible = false;
                raddriveronwaiting.Visible = false;
                lblraddelivered.Visible = false;
                raddelievered.Visible = false;
                lblradenroute.Visible = false;
                radenroute.Visible = false;
                break;
            case loadStatus.Empty_in_Yard:
                lblRadpickedup.Visible = false;
                radpickedup.Visible = false;
                lblradrecheddestination.Visible = false;
                radrecheddestination.Visible = false;
                lblraddriveronwaiting.Visible = false;
                raddriveronwaiting.Visible = false;
                lblraddelivered.Visible = false;
                raddelievered.Visible = false;
                lblradenroute.Visible = false;
                radenroute.Visible = false;
                lblradloadinyard.Visible = false;
                radloadinyard.Visible = false;
                lblrademptyinyard.Visible = false;
                rademptyinyard.Visible = false;
                break;
            case loadStatus.Drop_in_Warehouse:
                lblRadpickedup.Visible = false;
                radpickedup.Visible = false;
                lblradrecheddestination.Visible = false;
                radrecheddestination.Visible = false;
                lblraddriveronwaiting.Visible = false;
                raddriveronwaiting.Visible = false;
                lblraddelivered.Visible = false;
                raddelievered.Visible = false;
                lblradenroute.Visible = false;
                radenroute.Visible = false;
                raddropinwarehouse.Visible = false;
                lblreddropinwarehouse.Visible = false;
                break;
            case loadStatus.Loaded_in_Yard:
                lblRadpickedup.Visible = false;
                radpickedup.Visible = false;
                lblradrecheddestination.Visible = false;
                radrecheddestination.Visible = false;
                lblraddriveronwaiting.Visible = false;
                raddriveronwaiting.Visible = false;
                lblraddelivered.Visible = false;
                raddelievered.Visible = false;
                lblradenroute.Visible = false;
                radenroute.Visible = false;
                lblradloadinyard.Visible = false;
                radloadinyard.Visible = false;
                break;
            default:
                try
                {
                    PageType page = (PageType)Session[Keys.TAB_SELECTED];
                    if (page == PageType.Loads)
                    {
                        Response.Redirect(Keys.PAGE_LOAD_HOME);
                    }
                    else
                    {
                        Response.Redirect(Keys.PAGE_PAPER_WORK_PENDING);
                    }
                }
                catch (Exception ex)
                {
                    // Response.Redirect(Keys.PAGE_LOAD_HOME);
                }
                break;
        }
    }
    #endregion

    #region button clicks
    public void lnkgoback_click(object sender, EventArgs e)
    {
        try
        {
            PageType page = (PageType)Session[Keys.TAB_SELECTED];
            if (page == PageType.Loads)
            {
                Response.Redirect(Keys.PAGE_LOAD_HOME);
            }
            else
            {
                Response.Redirect(Keys.PAGE_PAPER_WORK_PENDING);
            }
        }
        catch (Exception ex)
        {
            // Response.Redirect(Keys.PAGE_LOAD_HOME);
        }
    }
    public void lnkUpdateLoadStatus_click(object sender, EventArgs e)
    {
        try
        {
            ChangeLoadStatus Bl = new ChangeLoadStatus();
            if (!validate(Bl.LoadLastDate(Convert.ToInt32(Session[Keys.LOAD_ID]))))
            {                
                DateTime dateoftime = Convert.ToDateTime(datetimepick.Value);
                if (Bl.UpdateLoadStatus(Convert.ToInt32(Session[Keys.LOAD_ID]), (int)status, dateoftime, Convert.ToInt32(Session[Keys.LOGIN_ID_KEY]), txtDiscription.Text.ToString()))
                {
                    int statusid = (int)status;
                    ChangeLoadStatus cls = new ChangeLoadStatus();                     
                    if((",3,14,8,6,5,").Contains(","+ status.ToString()+","))                    
                    cls.UpdateLoadStatusToPostman(Convert.ToInt32(Session[Keys.LOAD_ID]), (statusid == 3? "Pickedup":(statusid == 14 ? "Reached Destination" : (statusid == 8 ? "Delivered" :(statusid == 5 ? "En-route" : "Drop in Warehouse")))), dateoftime);
                    LoadDetailsBase load = new LoadDetailsBase();
                    DataTable loaddetails = load.loadloaddetails(Convert.ToInt32(Session[Keys.LOAD_ID]));
                    if (loaddetails != null)
                    {
                        int statue = 0;
                        if (loaddetails.Rows[0]["bint_LoadStatusId"] != null) statue = Convert.ToInt32(loaddetails.Rows[0]["bint_LoadStatusId"].ToString()); else statue = 0;
                        loadStatus value = ((loadStatus)statue);
                        string status1 = string.Empty;
                        if (value == loadStatus.En_route)
                            status1 = value.ToString().Replace("_", "-");
                        else
                            status1 = value.ToString().Replace("_", " ");

                        MuPageMenu.SubTitle = ConfigurationManager.AppSettings["CompanyName"].ToString() + " - Change Load Status For Load -" + Session[Keys.LOAD_ID].ToString() + ", Container #: " + loaddetails.Rows[0]["nvar_Container"].ToString() + ", Current Status: " + status1;
                        MuPageMenu.LoadMenu();
                        ShowRediobuttons(value);
                        string message = " <p>" + ErrorMessages.CHANGE_STATUS_UPDATE_OK + " </p>";
                        SetMinValue();
                        Mobile_eTransportMobile MasterPage = (Mobile_eTransportMobile)Page.Master;
                        MasterPage.ShowMessage(message);
                        lnkgoback_click(null, null);                        
                    }
                }
            }
            else
            {
                LoadDetailsBase load = new LoadDetailsBase();
                DataTable loaddetails = load.loadloaddetails(Convert.ToInt32(Session[Keys.LOAD_ID]));
                if (loaddetails != null)
                {
                    int statue = 0;
                    if (loaddetails.Rows[0]["bint_LoadStatusId"] != null) statue = Convert.ToInt32(loaddetails.Rows[0]["bint_LoadStatusId"].ToString()); else statue = 0;
                    loadStatus value = ((loadStatus)statue);
                    string status1 = string.Empty;
                    if (value == loadStatus.En_route)
                        status1 = value.ToString().Replace("_", "-");
                    else
                        status1 = value.ToString().Replace("_", " ");

                    MuPageMenu.SubTitle = ConfigurationManager.AppSettings["CompanyName"].ToString() + " - Change Load Status For Load -" + Session[Keys.LOAD_ID].ToString() + ", Container #: " + loaddetails.Rows[0]["nvar_Container"].ToString() + ", Current Status: " + status1;
                    MuPageMenu.LoadMenu();
                    ShowRediobuttons(value);
                }
            }
        }
        catch (Exception ex)
        {
            string errors = " <p>" + ErrorMessages.CHANGE_STATUS_UPDATE_ERROR + " </p>";
            Mobile_eTransportMobile MasterPage = (Mobile_eTransportMobile)Page.Master;
            MasterPage.ShowErrors(errors);
        }
    }
    public bool validate(DateTime Lastdate)
    {

        string errors = string.Empty;

        bool valid = false;
        if (string.IsNullOrEmpty(datetimepick.Value))
        {
            errors += "<p>" + ErrorMessages.CHANGE_STATUS_VALID_DATE + " </p>";
            valid = true;
        }

        if (radpickedup.Visible == true && radpickedup.Checked)
        {
            status = loadStatus.Pickedup;
        }
        else if (radloadinyard.Visible == true && radloadinyard.Checked)
        {
            status = loadStatus.Loaded_in_Yard;
        }
        else if (radrecheddestination.Visible == true && radrecheddestination.Checked)
        {
            status = loadStatus.Reached_Destination;
        }
        else if (radenroute.Visible == true && radenroute.Checked)
        {
            status = loadStatus.En_route;
        }
        else if (rademptyinyard.Visible == true && rademptyinyard.Checked)
        {
            status = loadStatus.Empty_in_Yard;
        }
        else if (raddropinwarehouse.Visible == true && raddropinwarehouse.Checked)
        {
            status = loadStatus.Drop_in_Warehouse;
        }
        else if (raddriveronwaiting.Visible == true && raddriveronwaiting.Checked)
        {
            ChangeLoadStatus Bl = new ChangeLoadStatus();
            if (Bl.LoadIsReachedDestination(Convert.ToInt32(Session[Keys.LOAD_ID])))
            {
                errors += "<p>" + ErrorMessages.CHANGE_STATUS_DRIVER_ON_WAITING + " </p>";
                valid = true;
            }
            else
            {
                status = loadStatus.Driver_on_Waiting;
            }
        }
        else if (raddelievered.Visible == true && raddelievered.Checked)
        {
            status = loadStatus.Delivered;
        }
        else
        {
            errors += "<p>" + ErrorMessages.CHANGE_STATUS_NO_OPTION + " </p>";
            valid = true;
        }

        if (valid)
        {            
            Mobile_eTransportMobile MasterPage = (Mobile_eTransportMobile)Page.Master;
            MasterPage.ShowErrors(errors);
        }

        return valid;
    }   

    private void SetMinValue()
    {
        ChangeLoadStatus Bl = new ChangeLoadStatus();
        DateTime dt = Bl.LoadLastDate(Convert.ToInt32(Session[Keys.LOAD_ID]));
        datetimepick.Attributes.Add("min", dt.ToString("yyyy-MM-ddTHH:mm:ss"));
        datetimepick.Attributes.Add("type", "datetime-local");
        datetimepick.Attributes.Add("value", System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
    }
    #endregion
}
