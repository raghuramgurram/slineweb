using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using PdfSharp;
using PdfSharp.Pdf;
using PdfSharp.Drawing;

public partial class Mobile_DocumentsUpload : System.Web.UI.Page
{
    #region PageLoad
    public DocumentType DocumentTypeToUpdate
    {
        get
        {
            if (ViewState[Keys.LOAD_DOCUMENT_TYPE] == null)
            {
                DocumentType type = DocumentType.Bill_of_Lading;
                if (Request.QueryString["Type"] != null)
                {
                    int inttype = Convert.ToInt32(Request.QueryString["Type"].ToString());
                    type = (DocumentType)inttype;
                }
                ViewState[Keys.LOAD_DOCUMENT_TYPE] = type;
            }
            return (DocumentType)ViewState[Keys.LOAD_DOCUMENT_TYPE];


        }
        set
        {
            ViewState[Keys.LOAD_DOCUMENT_TYPE] = value;
        }
    }
    public string MainFolder
    {
        get
        {
            return DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString();
        }

    }
    public string subfolder
    {
        get
        {
            return Session[Keys.LOAD_ID].ToString() + "_" + (int)DocumentTypeToUpdate + "_" + timestampforfoulder;
        }

    }
    public string timestampforfoulder
    {
        get
        {
            if (ViewState[Keys.LOAD_DOCUMENT_TIME_STAMP] == null)
            {
                ViewState[Keys.LOAD_DOCUMENT_TIME_STAMP] = DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
            }
            return ViewState[Keys.LOAD_DOCUMENT_TIME_STAMP].ToString();
        }
        set
        {
            ViewState[Keys.LOAD_DOCUMENT_TIME_STAMP] = value;
        }
    }
    public List<FileNameDetails> FilesUploaded
    {
        get
        {
            if (Session[Keys.LOAD_DOCUMENT_SESSION_FILES] == null)
            {
                List<FileNameDetails> list = new List<FileNameDetails>();
                Session[Keys.LOAD_DOCUMENT_SESSION_FILES] = list;
            }
            return (List<FileNameDetails>)Session[Keys.LOAD_DOCUMENT_SESSION_FILES];
        }
        set
        {
            Session[Keys.LOAD_DOCUMENT_SESSION_FILES] = value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Keys.LOGIN_ID_KEY] == null || Session[Keys.EMPLOY_ID] == null)
        {
            Response.Redirect(Keys.PAGE_LOAGIN);
        }

        if (!IsPostBack)
        {           
            if (Session[Keys.LOAD_ID] != null)
            {
                try
                {
                    //Divoverlay.Style.Add("display", "none");
                    FilesUploaded = new List<FileNameDetails>();
                    timestampforfoulder = null;
                    LoadDetailsBase load = new LoadDetailsBase();
                    DataTable loaddetails = load.loadloaddetails(Convert.ToInt32(Session[Keys.LOAD_ID]));
                    if (loaddetails != null)
                    {
                        string doctype = string.Empty;
                        switch (DocumentTypeToUpdate)
                        {
                            case DocumentType.Bill_of_Lading:
                                doctype = "Bill of Lading";
                                break;
                            case DocumentType.In_Gate_Interchange:
                                doctype = "In-Gate Interchange";
                                break;
                            case DocumentType.Proof_of_Delivery:
                                doctype = "Proof of Delivery";
                                break;
                            case DocumentType.Out_Gate_Interchange:
                                doctype = "Out-Gate Interchange";
                                break;
                            case DocumentType.Scale_Ticket:
                                doctype = "Scale Ticket";
                                break;
                            default:

                                break;
                        }
                        spanTitle.InnerText = doctype;
                        MuPageMenu.SubTitle = ConfigurationManager.AppSettings["CompanyName"].ToString() + " - Upload Documents For Load -" + Session[Keys.LOAD_ID].ToString() + " " + doctype + ", Container #: " + loaddetails.Rows[0]["nvar_Container"].ToString();
                        MuPageMenu.LoadMenu();
                    }
                }
                catch (Exception ex)
                {

                }

            }
            else
            {
                Response.Redirect(Keys.PAGE_LOAD_HOME);
            }
        }
    }
    #endregion

    #region button clicks
    public void lnkgoback_click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(Keys.PAGE_UPLOAD_DOCUMENTS);
        }
        catch (Exception ex)
        {

        }
    }
    public void lnkaddImagetoTemp_click(object sender, EventArgs e)
    {
        if (ImageFileUpload.PostedFile.ContentLength > 0)
        {
            try
            {                
                int filecount = FilesUploaded.Count;
                try
                {
                    System.Drawing.Bitmap img = new System.Drawing.Bitmap(ImageFileUpload.PostedFile.InputStream);
                    FileNameDetails fil = new FileNameDetails();
                    fil.FileName = ImageFileUpload.PostedFile.FileName;
                    fil.fileid = filecount + 1;
                    if (img.Width > img.Height)
                        img.RotateFlip(System.Drawing.RotateFlipType.Rotate90FlipNone);
                    double newWidth;
                    double newHeight;
                    if (img.Width < 800 && img.Height < 1100)
                    {
                        newWidth = img.Width;
                        newHeight = img.Height;
                    }
                    else
                    {
                        newWidth = 800;
                        newHeight = Math.Round((img.Height * (newWidth / img.Width)), 0);
                        if (newHeight > 1100)
                        {
                            newWidth = Math.Round((newWidth * (1100 / newWidth)), 0);
                            newHeight = 1100;
                        }
                    }
                    System.Drawing.Bitmap newImage = new System.Drawing.Bitmap(Convert.ToInt32(newWidth), Convert.ToInt32(newHeight), System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                    using (System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage(newImage))
                    {
                        graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                        graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                        graphics.DrawImage(img, 0, 0, Convert.ToInt32(newWidth), Convert.ToInt32(newHeight));
                    }
                    System.IO.MemoryStream mse = new System.IO.MemoryStream();
                    // newImage.Save(System.Configuration.ConfigurationSettings.AppSettings["FileUploadPath"].ToString() + Guid.NewGuid() + ".png", System.Drawing.Imaging.ImageFormat.Png);
                    newImage.Save(mse, System.Drawing.Imaging.ImageFormat.Png);
                    fil.Image = GrayScale(new System.Drawing.Bitmap(mse));
                    //fil.Image.SetResolution(72, 72);


                    FilesUploaded.Add(fil);
                    Mobile_eTransportMobile MasterPage = (Mobile_eTransportMobile)Page.Master;
                    string error = string.Empty;
                    MasterPage.ShowErrors(error);

                }
                catch (Exception ex)
                {
                    Mobile_eTransportMobile MasterPage = (Mobile_eTransportMobile)Page.Master;
                    string error = "<p>" + ErrorMessages.FILE_UPLOAD_FILE_TYPE_ERROR + " </p>";
                    MasterPage.ShowErrors(error);
                }
                ShowFilesUploaded();
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
        }
    }
    public void ShowFilesUploaded()
    {
        rptFileNames.DataSource = FilesUploaded;
        rptFileNames.DataBind();
    }
    public void lnkCreatePDFWithImags_click(object sender, EventArgs e)
    {
        PdfDocument doc = new PdfDocument();
        try
        {
            int i = 0;
            if (FilesUploaded.Count > 0)
            {
                try
                {
                    foreach (FileNameDetails file in FilesUploaded)
                    {
                        doc.Pages.Add(new PdfPage());
                        XGraphics xgr = XGraphics.FromPdfPage(doc.Pages[i]);
                        XImage img = XImage.FromGdiPlusImage(file.Image);
                        if (file.Image.Width < 800)
                        {
                            xgr.DrawImage(img, 0, 0, file.Image.Width, file.Image.Height);

                        }
                        else
                        {

                            xgr.PdfPage.Size = PageSize.A4;
                            //System.Drawing.Rectangle srcrect=new System.Drawing.Rectangle(0,0,file.Image.Width,file.Image.Height);
                            //System.Drawing.Rectangle recrect=new System.Drawing.Rectangle(0,0,800,nImgHeight);

                            xgr.DrawImage(img, 0, 0);
                        }
                        i++;
                    }

                    UploadDocuments busDoc = new UploadDocuments();
                    int filecount = busDoc.GetDocumentCount(Convert.ToInt32(Session[Keys.LOAD_ID]), (int)DocumentTypeToUpdate);
                    filecount += 1;
                    string PdfName = Session[Keys.LOAD_ID].ToString() + "_" + (int)(DocumentTypeToUpdate) + "_" + filecount + "_" + DocumentTypeToUpdate.ToString() + ".pdf";
                    System.IO.DirectoryInfo DirInfo = new System.IO.DirectoryInfo(Server.MapPath("~/Documents") + "\\" + Session[Keys.LOAD_ID].ToString() + "\\");
                    if (!DirInfo.Exists)
                    {
                        DirInfo.Create();
                    }

                    doc.Save(Server.MapPath("~/Documents") + "\\" + Session[Keys.LOAD_ID].ToString() + "\\" + PdfName);
                    busDoc.InsertDocument(Convert.ToInt32(Session[Keys.LOAD_ID]), (int)(DocumentTypeToUpdate), PdfName, filecount);
                    doc.Close();
                    deleteFolder();
                    Response.Redirect(Keys.PAGE_UPLOAD_DOCUMENTS + "?id=1");
                }
                catch (Exception ex)
                {
                    doc.Close();
                    Mobile_eTransportMobile MasterPage = (Mobile_eTransportMobile)Page.Master;
                    string error = "<p>" + ErrorMessages.FILE_UPLOAD_ERROR + " </p>";
                    MasterPage.ShowErrors(error);
                }

            }
            else
            {
                Mobile_eTransportMobile MasterPage = (Mobile_eTransportMobile)Page.Master;
                string error = "<p>" + ErrorMessages.FILE_UPLOAD_NO_FILES_TO_UPLOAD + " </p>";
                MasterPage.ShowErrors(error);
            }

        }
        catch (Exception ex)
        {

        }

    }

    public System.Drawing.Bitmap GrayScale(System.Drawing.Bitmap Bmp)
    {
        int rgb;
        System.Drawing.Color c;

        for (int y = 0; y < Bmp.Height; y++)
            for (int x = 0; x < Bmp.Width; x++)
            {
                c = Bmp.GetPixel(x, y);
                rgb = (int)((c.R + c.G + c.B) / 3);
                Bmp.SetPixel(x, y, System.Drawing.Color.FromArgb(rgb, rgb, rgb));
            }
        return Bmp;
    }

    public void lnkDeletImage_click(object sender, EventArgs e)
    {
        LinkButton li = (LinkButton)sender;
        if (li != null)
        {
            int listcount = Convert.ToInt32(li.CommandArgument.ToString());
            //int index = 0;
            foreach (FileNameDetails item in FilesUploaded)
                if (item.fileid == listcount)
                {
                    FilesUploaded.Remove(item);
                    break;
                }
            
            //int index = FilesUploaded.FindIndex(x => x.fileid == listcount);
            //FilesUploaded.RemoveAt(index);
            rptFileNames.DataSource = FilesUploaded;
            rptFileNames.DataBind();
        }
    }
    public void deleteFolder()
    {
        try
        {
            FilesUploaded = new List<FileNameDetails>();
        }
        catch (Exception ex)
        {
        }
    }
    #endregion
}

public class FileNameDetails
{
    public FileNameDetails()
    {
    }
    private int _fileid;
    public int fileid
    {
        get { return _fileid; }
        set { _fileid = value; }
    }

    private string _FileName;
    public string FileName
    {
        get { return _FileName; }
        set { _FileName = value; }
    }

    private System.Drawing.Bitmap _Image;
    public System.Drawing.Bitmap Image
    {
        get { return _Image; }
        set { _Image = value; }
    }
}
