<%@ Import Namespace="System.Security.Principal" %>
<%@ Import Namespace="System.Reflection" %>
<%@ Import Namespace="System.Diagnostics" %>
<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup
       /* Application["CompanyAddress"] =
            "<strong>S Line Transportation, Inc. </strong>" + "<br/>" +
            "780 B West Grand" + "<br/>" +
            "Oakland, California 94612" + "<br/>" +
            "Phone : 510-625-0396" + "<br/>" +
            "Fax: 510-625-0635";*/
    }

    public void Application_End()
    {
        HttpRuntime runtime = (HttpRuntime)typeof(System.Web.HttpRuntime).InvokeMember("_theRuntime",                                                                               BindingFlags.NonPublic

                                                                                        | BindingFlags.Static

                                                                                        | BindingFlags.GetField,

                                                                                        null,

                                                                                        null,

                                                                                        null);
        if (runtime == null)
            return;       
        
        string shutDownMessage = (string)runtime.GetType().InvokeMember("_shutDownMessage",

                                                                         BindingFlags.NonPublic

                                                                         | BindingFlags.Instance

                                                                         | BindingFlags.GetField,

                                                                         null,

                                                                         runtime,

                                                                         null);
        string shutDownStack = (string)runtime.GetType().InvokeMember("_shutDownStack",

                                                                       BindingFlags.NonPublic

                                                                       | BindingFlags.Instance

                                                                       | BindingFlags.GetField,

                                                                       null,

                                                                       runtime,

                                                                       null);

        string strpath = System.Web.HttpRuntime.AppDomainAppPath + "\\ErrorLog.txt";
       // System.IO.FileInfo fileinfo = new System.IO.FileInfo(@"D:\eTransport\Shiva\eTansport\etransport\ErrorLog.txt");
        System.IO.FileInfo fileinfo = new System.IO.FileInfo(strpath);

        if (fileinfo.Exists)
        {
            System.IO.StreamWriter writer = fileinfo.AppendText();
            writer.Write(shutDownStack);
            writer.Write(DateTime.Now);
            writer.Close();
        }
        else
        {
            System.IO.StreamWriter writer = System.IO.File.CreateText(fileinfo.FullName);
            writer.Write(shutDownStack);
            writer.Write(DateTime.Now);
            writer.Close();
        }     


        //if (!EventLog.SourceExists(".NET Runtime"))
        //{

        //    EventLog.CreateEventSource(".NET Runtime", "Application");

        //}
        //EventLog log = new EventLog();

        //log.Source = ".NET Runtime";

        //log.WriteEntry(String.Format("\r\n\r\n_shutDownMessage={0}\r\n\r\n_shutDownStack={1}",

        //                             shutDownMessage,

        //                             shutDownStack),

        //                             EventLogEntryType.Error);

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }
    void Page_Init(object sender, EventArgs e)
    {
        //if (Session["UserLoginId"] == null || Convert.ToString(Session["UserLoginId"]).Trim().Length == 0)
        //{
        //    Response.Redirect("~/SessionExpired.aspx");
        //}
    }
    void Session_Start(object sender, EventArgs e) 
    {
        Session.Timeout = 120;        
    }
        
    void Session_End(object sender, EventArgs e) 
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
    protected void Application_AuthenticateRequest(Object sender,EventArgs e)
    {
        if (HttpContext.Current.User != null)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                if (HttpContext.Current.User.Identity is FormsIdentity)
                {
                    FormsIdentity id =
                        (FormsIdentity)HttpContext.Current.User.Identity;
                    FormsAuthenticationTicket ticket = id.Ticket;

                    // Get the stored user-data, in this case, our roles
                    string userData = ticket.UserData;
                    string[] roles = userData.Split(',');
                    HttpContext.Current.User = new GenericPrincipal(id, roles);
                }
            }
        }
    }
</script>
