<%@ Page AutoEventWireup="true" CodeFile="NewCarrierAccount.aspx.cs" Inherits="NewCarrierAccount"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblform1">
      <tr>
        <td>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="635px" />
        
        </td>
      </tr>
    </table>
    
       <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblform">
        <tr>
          <th colspan="2">Please Enter the Details </th>
        </tr>
        <tr id="row">
          <td align="right" style="width: 276px">First Name   : </td>
          <td><asp:TextBox ID="txtFirstName" runat="server" MaxLength="200"/>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFirstName"
                  Display="Dynamic" ErrorMessage="Please enter first Name.">*</asp:RequiredFieldValidator></td>
        </tr>
        <tr id="altrow">
          <td align="right" style="width: 276px">Last Name     : </td>
          <td><asp:TextBox ID="txtLastName" runat="server" MaxLength="200"/></td>
        </tr>
        <tr id="row">
          <td align="right" style="width: 276px">User ID  : </td>
          <td><asp:TextBox ID="txtUserID" runat="server" MaxLength="100"/>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserID"
                  Display="Dynamic" ErrorMessage="Please enter User Name.">*</asp:RequiredFieldValidator>
                  <asp:Label ID="lblNameError" runat="server" Font-Bold="False" ForeColor="Red" Text="User Id already exists."
                  Visible="False"/>
           </td>
        </tr>
        <tr id="altrow">
          <td align="right" style="width: 276px">Password : </td>
          <td><asp:TextBox ID="txtPassword" runat="server" TextMode="Password" MaxLength="100"/></td>
        </tr>
        <tr id="row">
          <td align="right" style="width: 276px">Confirm Password  : </td>
          <td><asp:TextBox ID="txtConfirmpsw" runat="server" TextMode="Password"/>
              <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword"
                  ControlToValidate="txtConfirmpsw" ErrorMessage="Passwor mismatched.">*</asp:CompareValidator></td>
        </tr>
        <tr id="altrow">
          <td align="right" style="height: 20px; width: 276px;">User Email  : </td>
          <td style="height: 20px"><asp:TextBox ID="txtEmail" runat="server" MaxLength="100"/>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                  ErrorMessage="Please enter valid emial id" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
        </tr>
      </table><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../images/pix.gif" alt="" width="1" height="5" /></td>
        </tr>
      </table>
      
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblform">
      <tr>
        <th colspan="2">User Permissions </th>
      </tr>
      <tr id="row">
        <td width="30%" align="right" style="height: 20px">Access to Payments : </td>
        <td style="height: 20px"><asp:CheckBox ID="chkPayments" runat="server"/></td>
      </tr>
      <tr id="altrow">
        <td width="30%" align="right" style="height: 20px">Access to Documents  : </td>
        <td style="height: 20px"><asp:CheckBox ID="chkDocuments" runat="server"/></td>
      </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="../images/pix.gif" alt="" width="1" height="5" /></td>
      </tr>
    </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
        <tr>
         <td align="right">
              <asp:Button ID="btnSave" runat="server" Height="22px" Text="Save" CssClass="btnstyle" OnClick="btnSave_Click"/>
              &nbsp;<asp:Button ID="btnCancel" runat="server" CausesValidation="False"
                  Text="Cancel" UseSubmitBehavior="False" Height="22px"  CssClass="btnstyle" OnClick="btnCancel_Click"/>&nbsp;
          </td>      
        </tr>
      </table>
    </td>
  </tr>
</table>
</asp:Content>

