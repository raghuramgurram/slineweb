﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DisplayPLReport.aspx.cs" Inherits="Manager_DisplayPLReport" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" EnableEventValidation="false"%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="ContentTbl" class="auto-height">
<table width="100%" style=" position: absolute; top: 108px;" cellpadding="0" cellspacing="0">
<tr>
<td align="center" >
<input type="button" value="Back" style="height:21px;left: 685px; position: absolute;" class="btnstyle" onclick="window.history.go(-1)"/>
<asp:Button ID="btnExport" runat="server" CssClass="btnstyle" Text="Export to Excel" Height="21px" style="left: 735px; position: absolute;" OnClick="btnExport_Click"/>
</td>
</tr>
</table>

<table id="tbldispaly" runat="server" cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr id="row" style="padding-bottom:10px">
       <td style="height: 4px" valign="top">
                &nbsp;<asp:Label ID="lblSearchCriteria" runat="server" Font-Bold="True" ForeColor="Blue"
        Style="font-family: Arial, sans-serif; font-size: 12px;" Visible="true">
    </asp:Label></td>
    </tr>
    <tr>
    <td>
     <table border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, sans-serif;
        color: #4D4B4C; font-size: 12px; height: 1px;" width="100%" id="Table1">
        <tr><td>
       
    <asp:Label ID="lblDisplay" runat="server" Width="100%" Visible="true">
         <asp:GridView ID="PLGrid" runat="server" AutoGenerateColumns="false" RowStyle-BackColor="#f3f8fc" OnRowDataBound="PLGrid_RowDataBound" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="14px" HeaderStyle-BorderColor="Black" HeaderStyle-BackColor="LightBlue" HeaderStyle-Font-Names="Arial,Helvetica,sans-serif;" BorderColor="black" Font-Size="12px" Font-Names="Arial,Helvetica,sans-serif">
        <Columns>
             <asp:BoundField  HeaderText="Customer" DataField="Customer"/>
             <asp:BoundField  HeaderText="Load#" DataField="LoadId"/>
             <asp:BoundField  HeaderText="Container#" DataField="Container"/>
             <asp:BoundField  HeaderText="Origin" DataField="Origin"/>
             <asp:BoundField  HeaderText="Destination" DataField="Destination"/>
             <asp:BoundField  HeaderText="Delivered on" DataField="DeliveredDate"/>
             <asp:BoundField  HeaderText="Driver One" DataField="Driver1"/>
             <asp:BoundField  HeaderText="Driver Two" DataField="Driver2"/>
             <asp:BoundField  HeaderText="Driver Three" DataField="Driver3"/>
             <asp:BoundField  HeaderText="Driver Four" DataField="Driver4"/>
             <asp:BoundField  HeaderText="Carrier" DataField="Carrier"/>
             <asp:BoundField  HeaderText="Receivable" DataField="Receivable"/>
             <asp:BoundField  HeaderText="Pay Driver1" DataField="PayDriver1"/>
             <asp:BoundField  HeaderText="Pay Driver2" DataField="PayDriver2"/>
             <asp:BoundField  HeaderText="Pay Driver3" DataField="PayDriver3"/>
             <asp:BoundField  HeaderText="Pay Driver4" DataField="PayDriver4"/>
             <asp:BoundField  HeaderText="Pay Carrier" DataField="PayCarrier"/>
             <asp:BoundField  HeaderText="Chassis Days" DataField="ChassisDays"/>
             <asp:BoundField  HeaderText="Chassis Charge" DataField="ChassisCharge"/>
             <asp:BoundField  HeaderText="Miscellaneous" DataField="Misslinius"/>
             <asp:BoundField  HeaderText="Charges Per Deim" DataField="ChargesPerDeim"/>
             <asp:BoundField  HeaderText="Margin" DataField="Margin"/>
             <asp:BoundField  HeaderText="Invoiced" DataField="Invoiced"/>
             <asp:BoundField  HeaderText="Load Status" DataField="LoadStatus"/>
             <asp:BoundField  HeaderText="Notes" DataField="Notes"/>
        </Columns>
  </asp:GridView>
    </asp:Label>&nbsp;
    </td>
    </tr>
    </table>
    </td></tr>
</table>
<input id="btnPrintReport" type="button" value="Print"  class="btnstyle" runat="server" style="left: 891px; position: absolute; top: 108px;height: 21px;"/>
</div>
</asp:Content>
