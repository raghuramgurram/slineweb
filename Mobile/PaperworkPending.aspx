<%@ Page Language="C#" MasterPageFile="~/Mobile/eTransportMobile.master" AutoEventWireup="true" CodeFile="PaperworkPending.aspx.cs" Inherits="Mobile_PaperworkPending" Title="Untitled Page" %>
<%@ Register Src="MobileMenu.ascx" TagName="Menu"
    TagPrefix="mu" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<mu:Menu ID="MuPageMenu" runat="server" />
   <div class="main-wraper">
      <asp:Repeater ID="rptLoads" runat="server" OnItemDataBound="rptLoads_databind">
        <HeaderTemplate>
        </HeaderTemplate>
        <ItemTemplate>
        <div class="main-content bor">
        <div class="left-conte">
          <ul>
            <li> <span class="load-number"><%# Eval("bint_LoadId").ToString()%></span> <br>
              <span class="load-id"><%# Eval("nvar_Container").ToString()%></span> <br>
              <span class="load-lfd">Delivered: <asp:Literal ID="ltlDelivereddatetime" Text='<%# Eval("date_DeliveryAppointmentDate").ToString()%>' runat="server">
              </asp:Literal></span> <br>
            </li>
          </ul>                
        <asp:LinkButton ID="lnkloadetails" data-role="button" data-theme="a" data-icon="arrow-r" data-iconpos="right"
          data-transition="flip" rel="external" runat="server" OnClick="lnkloadetails_click" CommandArgument='<%# Eval("bint_LoadId").ToString()%>'>
        Load Details</asp:LinkButton>
        <asp:LinkButton ID="lnkupdateloadestatus" runat='server' data-role="button" data-theme="e" data-icon="arrow-r"
        data-iconpos="right"  data-transition="flip" rel="external" Visible="false" OnClick="lnkupdateloadestatus_click" CommandArgument='<%# Eval("bint_LoadId").ToString()%>' > Update Load Status </asp:LinkButton>
        <asp:LinkButton ID="lnkuploaddocuments" runat="server" data-role="button" data-theme="b" data-icon="arrow-r"
        data-iconpos="right"  data-transition="flip" rel="external" OnClick="lnkuploaddocuments_click" CommandArgument='<%# Eval("bint_LoadId").ToString()%>' > Upload Documents </asp:LinkButton>
         </div>
      </div>
        </ItemTemplate>
        <FooterTemplate>
        </FooterTemplate>
        </asp:Repeater>    
   </div>
</asp:Content>


