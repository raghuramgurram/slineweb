﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;


    public class LoginBase
    {
        public LoginBase()
        {
        }
        public DataTable Login(string userid, string pwd)
        {
            DataTable table = new DataTable();

            try
            {
                ParametersforLogin parameter = new ParametersforLogin();
                parameter.nvar_UserId = userid;
                parameter.nvar_Password = pwd;
                string[] str = { "nvar_UserId", "nvar_Password" };
                BaseClass bis = new BaseClass();
                DataSet ds = bis.Loaddata(parameter, "SP_Check_Users", str);
                table = ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return table;
        }
    }
public class ParametersforLogin : NewBaseClass
    {
        public ParametersforLogin()
        {
        }
        
    }