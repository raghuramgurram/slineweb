using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class AddNotes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Add Notes";
        btnsave.Attributes.Add("onclick", "this.style.display='none';");
        if (!IsPostBack)
        {
            if (Request.QueryString.Count > 0 && Session["UserId"]!=null)
            {
                ViewState["ID"] = Convert.ToString(Request.QueryString[0]).Trim();
                lblLoadNo.Text = Convert.ToString(ViewState["ID"]);
                lblEnterBy.Text = Convert.ToString(Session["UserId"]);
            }
        }
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        if (Session["CustomerNoteBackPage"] != null && ViewState["ID"]!=null)
        {
            if (string.Compare(Convert.ToString(Session["CustomerNoteBackPage"]),"TrackLoad",true,System.Globalization.CultureInfo.CurrentCulture)==0)
                Response.Redirect("CustomerTrackLoad.aspx?" + Convert.ToString(ViewState["ID"]));               
        }
        else
            Response.Redirect("CustomerLoadDetails.aspx?" + Convert.ToString(ViewState["ID"]) + "^");
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (ViewState["ID"] != null && Session["UserLoginId"]!=null)
        {
            object[] objParams = new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(Session["UserLoginId"]), txtNotes.Text.Trim() };
            if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"],"SP_Add_Notes",objParams)>0)
            {
                btnsave.Visible = false;
                CommonFunctions.SendEmail(Convert.ToString(Session["UserEmailId"]), GetFromXML.NotificationToEmail, "", "",
                "Addition of new Note for Load Id = " + lblLoadNo.Text.Trim(),
                "<br/>" + Convert.ToString(Session["UserId"]) + " added a Note " + txtNotes.Text.Trim() + " for Load Id = " + lblLoadNo.Text.Trim(), null,null);
                objParams = null;
                if (Session["CustomerNoteBackPage"] != null && ViewState["ID"]!=null)
                {
                    if (string.Compare(Convert.ToString(Session["CustomerNoteBackPage"]), "TrackLoad", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        Response.Redirect("CustomerTrackLoad.aspx?" + Convert.ToString(ViewState["ID"]));
                }
                else
                    Response.Redirect("CustomerLoadDetails.aspx?" + Convert.ToString(ViewState["ID"]) + "^");
            }           
        }
    }
}
