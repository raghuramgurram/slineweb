﻿using System;
using System.Collections.Generic;
using System.Web;


    public class Keys
    {
        public static string LOGIN_ID_KEY = "LoginID";
        public static string COOKES_USERNAME = "slineTransportUsername";
        public static string COOKES_PWD = "slineTransportPWD";
        public static string LOAD_ID = "Loadid";
        public static string TAB_SELECTED = "TabeSelectedByUser";
        public static string EMPLOY_ID = "EmployeeIdsession";
        
        // pages

        public static string PAGE_LOAD_DETAILS = "details.aspx";
        public static string PAGE_LOAD_HOME = "MyLoads.aspx";
        public static string PAGE_PAPER_WORK_PENDING = "PaperworkPending.aspx";
        public static string PAGE_CHANGE_STATUS = "ChangeStatus.aspx";
        public static string PAGE_UPLOAD_DOCUMENTS = "UploadDocuments.aspx";
        public static string PAGE_DOCUMENTS_UPLOAD = "DocumentsUpload.aspx";
        public static string PAGE_LOAGIN = "Login.aspx";

        //View State

        public static string LOAD_STATUS_CHANGE = "LoadStatusChange";
        public static string LOAD_DOCUMENT_TYPE = "LoadDocumentTypeForUpload";
        public static string LOAD_DOCUMENT_FOLDER_NAME = "LoadDoucmentfolderName";
        public static string LOAD_DOCUMENT_TIME_STAMP = "LoadDocumentTimeStamp";
        public static string LOAD_DOCUMENT_SESSION_FILES = "loadDocumentsessionfiles";
       
    }