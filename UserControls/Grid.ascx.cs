using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class Grid : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CommonFunctions.InsertIntoInfoLog("Grid.ascx :: Page_Load :: Start");
        if (ViewState["GridPageIndex"] == null)
            BindGrid(0);
        else
        {
            BindGrid(Convert.ToInt32(ViewState["GridPageIndex"]));
        }
        CommonFunctions.InsertIntoInfoLog("Grid.ascx :: Page_Load :: END");
    }

    public DataGrid GridControl
    {
        get
        {
            return dggrid;
        }
    }

    public string TableName
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["TableName"] = Convert.ToString(value).Trim();
            }
        }
    }
    public string Primarykey
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["Primarykey"] = Convert.ToString(value).Trim();
            }
        }
    }
    public string ColumnsList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["ColumnsList"] = Convert.ToString(value).Trim().Replace(";",",");
            }
        }
    }
    public string WhereClause
    {
        set
        {
            //if (Convert.ToString(value).Trim().Length > 0)
            //{
                ViewState["WhereClause"] = Convert.ToString(value).Trim();
            //}
        }
    }
    public int PageSize
    {
        set
        {
            if (value == 0 && value!=null)
                ViewState["PageSize"] = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"].ToString());
            else
                ViewState["PageSize"] = value;
        }
        get
        {
            return Convert.ToInt32(ViewState["PageSize"]);
        }
    }
    public string VisibleColumnsList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                string[] strColumns = Convert.ToString(value).Trim().Replace(",",";").Split(';');
                ViewState["VisibleColumns"] = strColumns;
            }            
        }
    }
    public string VisibleHeadersList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                string[] strColumns = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
                ViewState["VisibleHeaders"] = strColumns;
            }
        }
    }
    public bool EditVisible
    {
        set
        {
            ViewState["EditVisible"] = value;
        }
     }
    public bool DeleteVisible
    {
        set
        {
            ViewState["DeleteVisible"] = value;
        }
    }
    public string EditPage
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["EditPage"] = Convert.ToString(value).Trim();
            }
        }
    }
    public bool PrintVisible
    {
        set
        {
            ViewState["PrintVisible"] = value;
        }
    }
    public string PrintPage
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["PrintPage"] = Convert.ToString(value).Trim();
            }
        }
    }   
    public string DependentTablesList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                string[] DependentTables = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
                ViewState["DependentTablesList"] = DependentTables;
                DependentTables = null;
            }
        }
    }
    public string DependentTableKeysList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                string[] DependentTableKeys = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
                ViewState["DependentTableKeysList"] = DependentTableKeys;
                DependentTableKeys = null;
            }
        }
    }
    public string DependentTableAddtionialWhereClauseList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                string[] DependentTableAddtionialWhereClauses = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
                ViewState["DependentTableAddtionialWhereClauseList"] = DependentTableAddtionialWhereClauses;
                DependentTableAddtionialWhereClauses = null;
            }
        }
    }
    public string DependentTablesInnerJoinsList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                string[] DependentTablesInnerJoins = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
                ViewState["DependentTablesInnerJoinsList"] = DependentTablesInnerJoins;
                DependentTablesInnerJoins = null;
            }
        }
    }
    public string DeleteTablesList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                string[] DeleteTables = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
                ViewState["DeleteTablesList"] = DeleteTables;
                DeleteTables = null;
            }
        }
    }
    public string DeleteTablePKList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                string[] DeleteTablePK = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
                ViewState["DeleteTablePKList"] = DeleteTablePK;
                DeleteTablePK = null;
            }
        }
    }
    public string DeleteTableAddtionialWhereClauseList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                string[] DeleteTableAddtionialWhereClauses = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
                ViewState["DeleteTableAddtionialWhereClauseList"] = DeleteTableAddtionialWhereClauses;
                DeleteTableAddtionialWhereClauses = null;
            }
        }
    }
    public string InnerJoinClause
    {
        set
        {
            ViewState["InnerJoinClause"] = Convert.ToString(value).Trim();
        }
    }
    public string OrderByClause
    {
        set
        {
            ViewState["OrderByClause"] = Convert.ToString(value).Trim();
        }
    }
    public bool IsPagerVisible
    {
        set { dggrid.PagerStyle.Visible = value; }
    }
    public bool IsDistinctRequired
    {
        set { ViewState["IsDistinctRequired"] = value; }
    }
    public int RowCount
    {
        get
        {
            return dggrid.Items.Count;
        }
    }
    public string SortBy
    {
        set         
        {
            ViewState["SortBy"] = Convert.ToString(value).Trim();
        }
    }
    public int PageIndex
    {
        set
        {
            ViewState["PageIndex"] = value;
        }
        get
        {
            return Convert.ToInt32(ViewState["PageIndex"]);
        }
    }
    public string FunctionName
    {
        set { ViewState["FunctionName"] = value; }
    }
    public int ReturnValue
    {
        get 
        {
            if (ViewState["ReturnValue"] != null)
                return Convert.ToInt32(ViewState["ReturnValue"]);
            else
                return 0;
        }
    }
    public bool AutoIncrement
    {
        set 
        {
            if(value!=null)
                ViewState["AutoIncrement"] = value; 
            else
                ViewState["AutoIncrement"] = false; 
        }
    }
    public void BindGrid(int iPageNumber)
    {
        CommonFunctions.InsertIntoInfoLog("Grid.ascx :: BindGrid :: Start");
        if (Convert.ToString(ViewState["TableName"]).Trim().Length > 0)
        {
            if (ViewState["VisibleHeaders"] != null && ViewState["VisibleColumns"] != null)
                if (((string[])ViewState["VisibleHeaders"]).Length != ((string[])ViewState["VisibleColumns"]).Length)
                    return;
            object[] parms = null;
            string strProdName = "";
            if (Convert.ToString(ViewState["InnerJoinClause"]).Trim().Length == 0)
            {
                if (ViewState["OrderByClause"] != null)
                {
                    parms = new object[] { Convert.ToString(ViewState["TableName"]), Convert.ToString(ViewState["Primarykey"]), Convert.ToString(ViewState["ColumnsList"]), Convert.ToInt32(ViewState["PageSize"]), iPageNumber, Convert.ToString(ViewState["WhereClause"]), Convert.ToString(ViewState["OrderByClause"]) };
                    strProdName = "GetPagedDataWithOrderBy";
                }
                else
                {
                    parms = new object[] { Convert.ToString(ViewState["TableName"]), Convert.ToString(ViewState["Primarykey"]), Convert.ToString(ViewState["ColumnsList"]), Convert.ToInt32(ViewState["PageSize"]), iPageNumber, Convert.ToString(ViewState["WhereClause"]) };
                    strProdName = "GetPagedData";
                }
            }
            else
            {
                if (ViewState["OrderByClause"] == null)
                {
                    parms = new object[] { Convert.ToString(ViewState["TableName"]), Convert.ToString(ViewState["Primarykey"]), Convert.ToString(ViewState["ColumnsList"]), Convert.ToInt32(ViewState["PageSize"]), iPageNumber, Convert.ToString(ViewState["InnerJoinClause"]), Convert.ToString(ViewState["WhereClause"]) };
                    if (ViewState["IsDistinctRequired"] != null)
                    {
                        if (Convert.ToBoolean(ViewState["IsDistinctRequired"]))
                            strProdName = "GetDistinctPagedDataWithJoins";
                        else
                            strProdName = "GetPagedDataWithJoins";
                    }
                    else
                    {
                        strProdName = "GetPagedDataWithJoins";
                    }

                }
                else
                {
                    parms = new object[] { Convert.ToString(ViewState["TableName"]), Convert.ToString(ViewState["Primarykey"]), Convert.ToString(ViewState["ColumnsList"]), Convert.ToInt32(ViewState["PageSize"]), iPageNumber, Convert.ToString(ViewState["InnerJoinClause"]), Convert.ToString(ViewState["WhereClause"]), Convert.ToString(ViewState["OrderByClause"]) };
                    strProdName = "GetPagedDataWithJoinsAndOrder";
                }
            }
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], strProdName, parms);
            parms = null;
            dggrid.Controls.Clear();
            dggrid.Columns.Clear();
            if(ds.Tables.Count>1)
                ViewState["RowCount"] = ds.Tables[0].Rows.Count;
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    FormatGrid();

                    foreach (DataColumn c in ds.Tables[0].Columns)
                    {
                        dggrid.Columns.Add(CreateBoundColumn(c));
                    }
                    if (ViewState["EditVisible"] != null || ViewState["DeleteVisible"] != null)
                    {
                        // Change the Recivables status for the Load  (Madhav)
                        if (Convert.ToBoolean(ViewState["EditVisible"]) || Convert.ToBoolean(ViewState["DeleteVisible"]) || ChangeStatusLink)
                        {
                            TemplateColumn tcol = new TemplateColumn();
                            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                            {
                                tcol.ItemTemplate = new DataGridTemplate(ListItemType.Item, Convert.ToBoolean(ViewState["EditVisible"]), Convert.ToBoolean(ViewState["DeleteVisible"]), Convert.ToBoolean(ViewState["PrintVisible"]), ChangeStatusLink);
                            }
                            tcol.ItemStyle.CssClass = "GridItem";
                            tcol.HeaderStyle.CssClass = "GridHeader";
                            if (!ChangeStatusLink)
                                tcol.ItemStyle.Width = 60;
                            dggrid.Columns.Add(tcol);
                        }
                    }
                    // ADD status Collumn for status Change (Madhav)
                    
                    //#region Sort By
                    //if (Convert.ToString(ViewState["SortBy"]).Trim().Length > 0)
                    //{
                    //    DataRow[] drc1 = ds.Tables[0].Select("", Convert.ToString(ViewState["SortBy"]).Trim());
                    //    if (drc1.Length <= Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]))
                    //    {
                    //        DataTable dtTemp = ds.Tables[0].Clone();
                    //        for (int p = 0; p < drc1.Length; p++)
                    //        {
                    //            dtTemp.ImportRow(drc1[p]);
                    //            dtTemp.AcceptChanges();
                    //        }
                    //        ds.Tables[0].Rows.Clear();
                    //        for (int p = 0; p < dtTemp.Rows.Count; p++)
                    //        {
                    //            ds.Tables[0].ImportRow(dtTemp.Rows[p]);
                    //            ds.Tables[0].AcceptChanges();
                    //        }
                    //        if (dtTemp != null) { dtTemp.Dispose(); dtTemp = null; }
                    //    }
                    //    drc1 = null; 
                    //}
                    //#endregion
                    dggrid.AllowCustomPaging = true;
                    dggrid.VirtualItemCount = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                    dggrid.DataSource = ds.Tables[0];
                    dggrid.CurrentPageIndex = iPageNumber;
                    dggrid.DataBind();
                    for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                    {
                        if (index % 2 == 0)
                        {
                            for (int k = 0; k < dggrid.Columns.Count; k++)
                            {
                                dggrid.Items[index].Cells[k].CssClass = "GridItem";
                            }
                        }
                        else
                        {
                            for (int k = 0; k < dggrid.Columns.Count; k++)
                            {
                                dggrid.Items[index].Cells[k].CssClass = "GridAltItem";
                            }
                        }
                    }
                }
            }
            if (ds != null) { ds.Dispose(); ds = null; }
        }
        CommonFunctions.InsertIntoInfoLog("Grid.ascx :: BindGrid :: END");
    }
    //ADD Status Collumn for Load receivables posted to qb button (Madhav)
    public bool ChangeStatusLink
    {
        get
        {
            if (ViewState["ChangeStatusLink"] == null)
            {
                ViewState["ChangeStatusLink"] = false;
            }
            return Convert.ToBoolean(ViewState["ChangeStatusLink"]);
        }
        set
        {
            ViewState["ChangeStatusLink"] = value;
        }
    }

    private void FormatGrid()
    {
        dggrid.AutoGenerateColumns = false;
        try
        {
            if (Convert.ToInt32(ViewState["PageSize"]) <= Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]))
                dggrid.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
            else
                dggrid.PageSize = Convert.ToInt32(ViewState["PageSize"]);
        }
        catch
        {
            dggrid.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        }
        dggrid.AllowCustomPaging = true;
        dggrid.AllowPaging = true;
        dggrid.AllowSorting = true;
        dggrid.PagerStyle.Mode = PagerMode.NumericPages;
        dggrid.PagerStyle.HorizontalAlign = HorizontalAlign.Center;
    }
    protected DataGridColumn CreateBoundColumn(DataColumn c)
    {
        BoundColumn column = new BoundColumn();
        column.DataField =c.ColumnName;
        column.Visible = IsColumnVisible(c.ColumnName);
        column.HeaderText = GetHeaderText(c.ColumnName);
        column.ItemStyle.CssClass = "GridItem";
        column.HeaderStyle.CssClass = "GridHeader";
        return column;
    }
    protected DataGridColumn CreateAutoGenaratedNoColumn(DataColumn c)
    {
        BoundColumn column = new BoundColumn();        
        column.DataField = "1";
        c.AutoIncrement=true;
        column.HeaderText = GetHeaderText("Order No");
        column.ItemStyle.CssClass = "GridItem";
        column.HeaderStyle.CssClass = "GridHeader";
        return column;
    }
    protected bool IsColumnVisible(string strColName)
    {
        if (ViewState["VisibleColumns"] != null)
        {
            string[] strColumns = (string[])ViewState["VisibleColumns"];
            bool blVal = false;
            for (int i = 0; i < strColumns.Length; i++)
            {
                if (string.Compare(strColumns[i], strColName, true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    blVal = true;
                    break;
                }
            }
            strColumns = null;
            return blVal;
        }
        else return true;
    }
    protected string GetHeaderText(string strColName)
    {
        string strHeaderText = strColName.Replace("_", " ");
        if (ViewState["VisibleHeaders"] != null)
        {
            string[] strHeaders = (string[])ViewState["VisibleHeaders"];
            string[] strColumns = (string[])ViewState["VisibleColumns"];
            for (int i = 0; i < strHeaders.Length; i++)
            {
                if (string.Compare(strColumns[i], strColName, true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    strHeaderText = "  "+strHeaders[i];
                    break;
                }
            }
            strHeaders = null;
        }
        return strHeaderText;
    }
    protected void dggrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        // Change the Recivables status for the Load to Posted to QB (Madhav)
        if (e.CommandName == "ChangeStatus")
        {
            if (Request.QueryString.Count > 0)
                SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Update_ReceivablesStatus_To_PostedToQB", new object[] { Convert.ToInt64(Request.QueryString[0]) });
            Response.Redirect(Request.RawUrl);
        }
        // Change the Recivables status for the Load to Posted to QB (Madhav)
        if (string.Compare(e.CommandName, "btnPrint", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            if (ViewState["PrintPage"] != null)
                Response.Redirect(Convert.ToString(ViewState["PrintPage"]) + "?" + e.Item.Cells[0].Text.Trim());
        }
        if (string.Compare(e.CommandName, "btnEdit", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            if (ViewState["EditPage"] != null)
                Response.Redirect(Convert.ToString(ViewState["EditPage"]) + "?" + e.Item.Cells[0].Text.Trim());
        }
        if (string.Compare(e.CommandName, "btnDelete", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {            
            if (string.Compare(Convert.ToString(ViewState["FunctionName"]), "DeleteDriver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                object[] objArray=e.Item.Cells[0].Text.Trim().Split('^');
                int iRet=0;
                SqlParameter[] objParams=SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"],"SP_Delete_Driver",new object[]{objArray[1],iRet});
                if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Delete_Driver", objParams) > 0)
                {
                    if (objParams[objParams.Length-1].Value.ToString() == "0")
                    {
                        Response.Write("<script>alert('This driver can not be deleted because Load(s) are assigned.')</script>");
                        return;
                    }
                    objArray = null;
                    objParams = null;
                }
            }
            else if (string.Compare(Convert.ToString(ViewState["FunctionName"]), "DeleteCustomer", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                object[] objArray=e.Item.Cells[0].Text.Trim().Split('^');
                int iRet=0;
                SqlParameter[] objParams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Delete_Customer", new object[] { objArray[0], iRet });
                if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Delete_Customer", objParams) > 0)
                {
                    if (objParams[objParams.Length - 1].Value.ToString() == "0")
                    {
                        Response.Write("<script>alert('This Customer can not be deleted because Load(s) are assigned.')</script>");
                        return;
                    }
                    objArray = null;
                    objParams = null;
                }
                else
                {
                    Response.Write("<script>alert('This Customer can not be deleted because Load(s) are assigned.')</script>");
                    return;
                }
            }
            else if (ViewState["DeleteTablesList"] != null)
            {
                string[] strID = e.Item.Cells[0].Text.Trim().Split('^');
                string[] DeleteTables = (string[])ViewState["DeleteTablesList"];
                if (DeleteTables.Length > 0)
                {
                    #region Check Dependencies
                    bool blCanWeDelete = true;
                    if (ViewState["DependentTablesList"] != null)
                    {
                        string[] DependentTables = (string[])ViewState["DependentTablesList"];
                        string[] DependentTableKeys = (string[])ViewState["DependentTableKeysList"];
                        string[] DependentTableAddtionialWhereClauses = null;
                        string[] DependentTablesInnerJoins = null;
                        if (ViewState["DependentTableAddtionialWhereClauseList"] != null)
                            DependentTableAddtionialWhereClauses = (string[])ViewState["DependentTableAddtionialWhereClauseList"];
                        if (ViewState["DependentTablesInnerJoinsList"] != null)
                            DependentTablesInnerJoins = (string[])ViewState["DependentTablesInnerJoinsList"];
                        if (DependentTables.Length == DependentTableKeys.Length)
                        {
                            string strQuery = "";
                            for (int i = 0; i < DependentTableKeys.Length; i++)
                            {
                                strQuery = "Select Count(" + DependentTableKeys[i].Trim() + ") from " + DependentTables[i].Trim();
                                if (DependentTablesInnerJoins != null)
                                {
                                    if (DependentTables.Length == DependentTablesInnerJoins.Length)
                                    {
                                        if (Convert.ToString(DependentTablesInnerJoins[i]).Trim().Length > 0)
                                            strQuery += " " + DependentTablesInnerJoins[i];
                                    }
                                }
                                strQuery += " where " + DependentTableKeys[i].Trim() + "=" + strID[0];
                                if (DependentTableAddtionialWhereClauses != null)
                                {
                                    if (DependentTables.Length == DependentTableAddtionialWhereClauses.Length)
                                    {
                                        if (Convert.ToString(DependentTableAddtionialWhereClauses[i]).Trim().Length > 0)
                                            strQuery += " and " + DependentTableAddtionialWhereClauses[i].Replace("~", ",");
                                    }
                                }
                                if (Convert.ToInt32(DBClass.executeScalar(strQuery)) > 0)
                                {
                                    blCanWeDelete = false;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            blCanWeDelete = false;
                        }
                        DependentTables = null; DependentTableKeys = null; DependentTableAddtionialWhereClauses = null; DependentTablesInnerJoins = null;
                        if (!blCanWeDelete)
                        {
                            Response.Write("<script>alert('Unable to delete due to dependencies.')</script>");
                            return;
                        }
                    }
                    #endregion
                    #region Delete
                    string strWhereClause = "";
                    string[] strQueries = new string[DeleteTables.Length];
                    if (ViewState["DeleteTablePKList"] == null)
                    {
                        strWhereClause = " where " + Convert.ToString(ViewState["Primarykey"]).Trim() + " = " + strID[0];
                        for (int i = 0; i < DeleteTables.Length; i++)
                        {
                            strQueries[i] = "delete from " + DeleteTables[i] + strWhereClause;
                        }
                    }
                    else
                    {
                        string[] DeleteTablesPK = (string[])ViewState["DeleteTablePKList"];
                        string[] DeleteTableAddtionialWhereClauses = null;
                        if (ViewState["DeleteTableAddtionialWhereClauseList"] != null)
                            DeleteTableAddtionialWhereClauses = (string[])ViewState["DeleteTableAddtionialWhereClauseList"];
                        for (int i = 0; i < DeleteTables.Length; i++)
                        {
                            if (DeleteTablesPK[i].Trim().Length > 0)
                                strWhereClause = " where " + DeleteTablesPK[i] + " = " + strID[0];
                            else
                                strWhereClause = string.Empty;
                            if (DeleteTableAddtionialWhereClauses != null)
                            {
                                if (DeleteTables.Length == DeleteTableAddtionialWhereClauses.Length)
                                {
                                    if (Convert.ToString(DeleteTableAddtionialWhereClauses[i]).Trim().Length > 0)
                                    {
                                        if (strWhereClause.Trim().Length > 0)
                                            strWhereClause += " and " + DeleteTableAddtionialWhereClauses[i].Replace("~", ",");
                                        else
                                            strWhereClause = " where " + DeleteTableAddtionialWhereClauses[i].Replace("~", ",");
                                    }
                                }
                            }
                            strQueries[i] = "delete from " + DeleteTables[i] + strWhereClause;
                        }
                    }
                    if (string.Compare(Convert.ToString(ViewState["FunctionName"]), "DeleteOrigin", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                    {
                        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Insert_Origin_Log", new object[] { strID[0], Convert.ToString(Session["UserLoginId"]), "Delete" });
                    }
                    else if (string.Compare(Convert.ToString(ViewState["FunctionName"]), "DeleteDestination", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                    {
                        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Insert_Destination_Log", new object[] { strID[0], Convert.ToString(Session["UserLoginId"]), "Delete" });
                    }
                    if (DBClass.executeQueries(strQueries) > 0)
                    {
                        Session["GridPageIndex"] = 0;
                        BindGrid(0);
                        if (ViewState["RowCount"] != null)
                        {
                            if (Convert.ToInt32(ViewState["RowCount"]) == 0)
                                Session["ReturnValue"] = 1;
                            else
                                Session["ReturnValue"] = 2;
                        }
                    }
                    strQueries = null;
                    #endregion
                }
                DeleteTables = null;
                strID = null;
            }
           
        }
    }
    protected void dggrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        ViewState["GridPageIndex"] = e.NewPageIndex;
        BindGrid(e.NewPageIndex);
    }
    protected void DataGrid1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (Convert.ToBoolean(ViewState["AutoIncrement"]))
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                e.Item.Cells[1].Text = Convert.ToString(e.Item.ItemIndex + 1);
            }
        }
    }
}
public class DataGridTemplate : System.Web.UI.Page, ITemplate
{
    // Change the Recivables status for the Load to Posted to QB (Madhav)
    ListItemType templateType;
    bool blEdit = false, blDelete = false, blPrint = false , ChangStatus = false;
    public DataGridTemplate(ListItemType type, bool bEdit, bool bDelete, bool bPrint , bool ChangeStatus)
    {
        templateType = type;
        blEdit = bEdit;
        blDelete = bDelete;
        blPrint = bPrint;
        ChangStatus = ChangeStatus;
    }
    public void InstantiateIn(System.Web.UI.Control container)
    {
        Literal lc = new Literal();
        switch (templateType)
        {
            case ListItemType.Header:                
                lc.Text = "<B>Edit</B>";
                LinkButton lb = new LinkButton();
                lb.ID = "lb";
                lb.Text = "Edit";
                lb.CommandName = "EditButton";
                container.Controls.Add(lb);
                container.Controls.Add(lc);
                lc = null; lb = null;
                break;
            case ListItemType.Item:
                if (!blPrint)
                {
                    int iWidth = (blEdit && blDelete) ? 35 : 80;
                    container.Controls.Add(new LiteralControl("<table border=0 width=100%><tr><td width=10%></td>"));
                    if (blEdit)
                    {
                        container.Controls.Add(new LiteralControl("<td width=" + iWidth + "%  align=center valign=middle>"));
                        ImageButton imgEdit = new ImageButton();
                        imgEdit.ID = "imgEdit";
                        imgEdit.ImageUrl = "~/images/edit_icon.gif";
                        imgEdit.Width = 15; imgEdit.Height = 15;
                        imgEdit.ToolTip = "Edit";
                        imgEdit.CausesValidation = false;
                        imgEdit.CommandName = "btnEdit";
                        container.Controls.Add(imgEdit);
                        container.Controls.Add(new LiteralControl("</td>"));
                    }
                    if (blDelete)
                    {
                        if (blEdit)
                            container.Controls.Add(new LiteralControl("<td width=10%></td>"));
                        container.Controls.Add(new LiteralControl("<td width=" + iWidth + "%  align=center valign=middle>"));
                        ImageButton imgDel = new ImageButton();
                        imgDel.ID = "imgDel";
                        imgDel.ImageUrl = "~/images/delete_icon.gif";
                        imgDel.Width = 13; imgDel.Height = 12;
                        imgDel.CausesValidation = false;
                        imgDel.ToolTip = "Delete";
                        imgDel.CommandName = "btnDelete";
                        imgDel.OnClientClick = "return confirm('Are you sure you want to delete this record?');";
                        container.Controls.Add(imgDel);
                        container.Controls.Add(new LiteralControl("</td>"));
                    }
                    // Change the Recivables status for the Load to Posted to QB (Madhav)
                    if (ChangStatus)
                    {
                        container.Controls.Add(new LiteralControl("<td width=" + iWidth + "%  align=center valign=middle>"));                        
                        LinkButton lnkStatusChange = new LinkButton();
                        lnkStatusChange.Text = "Posted to QB";
                        lnkStatusChange.Font.Bold = true;
                        lnkStatusChange.CommandName = "ChangeStatus";
                        lnkStatusChange.Font.Size = 8;
                        container.Controls.Add(lnkStatusChange);
                        container.Controls.Add(new LiteralControl("</td>"));
                    }

                    container.Controls.Add(new LiteralControl("<td width=10%></td></tr></table>"));
                    //imgEdit = null; imgDel = null;
                }
                else
                {
                    int iWidth = (blEdit && blDelete) ? 20 : 35;
                    container.Controls.Add(new LiteralControl("<table border=0 width=100%><tr><td width=10%></td>"));
                    container.Controls.Add(new LiteralControl("<td width=" + iWidth + "%  align=center valign=middle>"));
                    ImageButton imgPrint = new ImageButton();
                    imgPrint.ID = "imgPrint";
                    imgPrint.ImageUrl = "~/images/print-icon.gif";
                    imgPrint.Width = 16; imgPrint.Height = 15;
                    imgPrint.CausesValidation = false;
                    imgPrint.CommandName = "btnPrint";
                    container.Controls.Add(imgPrint);
                    container.Controls.Add(new LiteralControl("</td>"));
                    if (blEdit)
                    {
                        if (blPrint)
                            container.Controls.Add(new LiteralControl("<td width=10%></td>"));
                        container.Controls.Add(new LiteralControl("<td width=" + iWidth + "%  align=center valign=middle>"));
                        ImageButton imgEdit = new ImageButton();
                        imgEdit.ID = "imgEdit";
                        imgEdit.ImageUrl = "~/images/edit_icon.gif";
                        imgEdit.Width = 15; imgEdit.Height = 15;
                        imgEdit.CausesValidation = false;
                        imgEdit.CommandName = "btnEdit";
                        container.Controls.Add(imgEdit);
                        container.Controls.Add(new LiteralControl("</td>"));
                    }
                    if (blDelete)
                    {
                        if (blPrint || blEdit)
                            container.Controls.Add(new LiteralControl("<td width=10%></td>"));
                        //if (blEdit)
                        //    container.Controls.Add(new LiteralControl("<td width=10%></td>"));
                        container.Controls.Add(new LiteralControl("<td width=" + iWidth + "%  align=center valign=middle>"));
                        ImageButton imgDel = new ImageButton();
                        imgDel.ID = "imgDel";
                        imgDel.ImageUrl = "~/images/delete_icon.gif";
                        imgDel.Width = 13; imgDel.Height = 12;
                        imgDel.CausesValidation = false;
                        imgDel.CommandName = "btnDelete";
                        imgDel.OnClientClick = "return confirm('Are you sure you want to delete this record?');";
                        container.Controls.Add(imgDel);
                        container.Controls.Add(new LiteralControl("</td>"));
                    }
                    container.Controls.Add(new LiteralControl("<td width=10%></td></tr></table>"));
                    //imgEdit = null; imgDel = null;
                }
                break;
            case ListItemType.EditItem:
                TextBox tb = new TextBox();
                tb.Text = "";
                container.Controls.Add(tb);
                break;
            case ListItemType.Footer:
                lc.Text = "<I>Edit</I>";
                container.Controls.Add(lc);
                break;
        }
    }
}