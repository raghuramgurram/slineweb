<%@ Control Language="C#" AutoEventWireup="true" CodeFile="USState.ascx.cs" Inherits="UserControls_USState" %>
<asp:DropDownList ID="dropStates" runat="server">
    <asp:ListItem>---Select State---</asp:ListItem>
    <asp:ListItem>AK</asp:ListItem>
    <asp:ListItem>AL</asp:ListItem>
    <asp:ListItem>AR</asp:ListItem>
    <asp:ListItem>AZ</asp:ListItem>
    <asp:ListItem>BC</asp:ListItem>
    <asp:ListItem>CA</asp:ListItem>
    <asp:ListItem>CO</asp:ListItem>
    <asp:ListItem>CT</asp:ListItem>
    <asp:ListItem>DC</asp:ListItem>
    <asp:ListItem>DE</asp:ListItem>
    <asp:ListItem>FL</asp:ListItem>
    <asp:ListItem>GA</asp:ListItem>
    <asp:ListItem>HI</asp:ListItem>
    <asp:ListItem>IA</asp:ListItem>
    <asp:ListItem>ID</asp:ListItem>
    <asp:ListItem>IL</asp:ListItem>
    <asp:ListItem>IN</asp:ListItem>
    <asp:ListItem>KS</asp:ListItem>
    <asp:ListItem>KY</asp:ListItem>
    <asp:ListItem>LA</asp:ListItem>
    <asp:ListItem>MA</asp:ListItem>
    <asp:ListItem>MB</asp:ListItem>
    <asp:ListItem>MD</asp:ListItem>
    <asp:ListItem>ME</asp:ListItem>
    <asp:ListItem>MI</asp:ListItem>
    <asp:ListItem>MN</asp:ListItem>
    <asp:ListItem>MO</asp:ListItem>
    <asp:ListItem>MS</asp:ListItem>
    <asp:ListItem>MT</asp:ListItem>
    <asp:ListItem>NB</asp:ListItem>
    <asp:ListItem>NC</asp:ListItem>
    <asp:ListItem>ND</asp:ListItem>
    <asp:ListItem>NE</asp:ListItem>
    <asp:ListItem>NH</asp:ListItem>
    <asp:ListItem>NJ</asp:ListItem>
    <asp:ListItem>NL</asp:ListItem>
    <asp:ListItem>NM</asp:ListItem>
    <asp:ListItem>NS</asp:ListItem>
    <asp:ListItem>NT</asp:ListItem>
    <asp:ListItem>NU</asp:ListItem>
    <asp:ListItem>NV</asp:ListItem>
    <asp:ListItem>NY</asp:ListItem>
    <asp:ListItem>OH</asp:ListItem>
    <asp:ListItem>OK</asp:ListItem>
    <asp:ListItem>ON</asp:ListItem>
    <asp:ListItem>OR</asp:ListItem>
    <asp:ListItem>PA</asp:ListItem>
    <asp:ListItem>PE</asp:ListItem>
    <asp:ListItem>QC</asp:ListItem>
    <asp:ListItem>RI</asp:ListItem>
    <asp:ListItem>SC</asp:ListItem>
    <asp:ListItem>SD</asp:ListItem>
    <asp:ListItem>SK</asp:ListItem>
    <asp:ListItem>TN</asp:ListItem>
    <asp:ListItem>TX</asp:ListItem>
    <asp:ListItem>UT</asp:ListItem>
    <asp:ListItem>VA</asp:ListItem>
    <asp:ListItem>VT</asp:ListItem>
    <asp:ListItem>WA</asp:ListItem>
    <asp:ListItem>WI</asp:ListItem>
    <asp:ListItem>WV</asp:ListItem>
    <asp:ListItem>WY</asp:ListItem>
    <asp:ListItem>YT</asp:ListItem>
</asp:DropDownList><asp:CompareValidator ID="cmpState" runat="server"
    Display="Dynamic" Enabled="False" ErrorMessage="Please select state." ValueToCompare="---Select State---" ControlToValidate="dropStates" Operator="NotEqual">*</asp:CompareValidator>
<%--<asp:ListItem>Alabama</asp:ListItem>
<asp:ListItem>Alaska</asp:ListItem>
<asp:ListItem>Arizona</asp:ListItem>
<asp:ListItem>Arkansas</asp:ListItem>
<asp:ListItem>California</asp:ListItem>
<asp:ListItem>Colorado</asp:ListItem>
<asp:ListItem>Connecticut</asp:ListItem>
<asp:ListItem>D.C.</asp:ListItem>
<asp:ListItem>Delaware</asp:ListItem>
<asp:ListItem>Florida</asp:ListItem>
<asp:ListItem>Georgia</asp:ListItem>
<asp:ListItem>Hawaii</asp:ListItem>
<asp:ListItem>Idaho</asp:ListItem>
<asp:ListItem>Illinois</asp:ListItem>
<asp:ListItem>Indiana</asp:ListItem>
<asp:ListItem>Iowa</asp:ListItem>
<asp:ListItem>Kansas</asp:ListItem>
<asp:ListItem>Kentucky</asp:ListItem>
<asp:ListItem>Louisiana</asp:ListItem>
<asp:ListItem>Maine</asp:ListItem>
<asp:ListItem>Maryland</asp:ListItem>
<asp:ListItem>Massachusetts</asp:ListItem>
<asp:ListItem>Michigan</asp:ListItem>
<asp:ListItem>Minnesota</asp:ListItem>
<asp:ListItem>Mississippi</asp:ListItem>
<asp:ListItem>Missouri</asp:ListItem>
<asp:ListItem>Montana</asp:ListItem>
<asp:ListItem>Nebraska</asp:ListItem>
<asp:ListItem>Nevada</asp:ListItem>
<asp:ListItem>New Hampshire</asp:ListItem>
<asp:ListItem>New Jersey</asp:ListItem>
<asp:ListItem>New Mexico</asp:ListItem>
<asp:ListItem>New York</asp:ListItem>
<asp:ListItem>North Carolina</asp:ListItem>
<asp:ListItem>North Dakota</asp:ListItem>
<asp:ListItem>Ohio</asp:ListItem>
<asp:ListItem>Oklahoma</asp:ListItem>
<asp:ListItem>Oregon</asp:ListItem>
<asp:ListItem>Pennsylvania</asp:ListItem>
<asp:ListItem>Rhode Island</asp:ListItem>
<asp:ListItem>South Carolina</asp:ListItem>
<asp:ListItem>South Dakota</asp:ListItem>
<asp:ListItem>Tennessee</asp:ListItem>
<asp:ListItem>Texas</asp:ListItem>
<asp:ListItem>Utah</asp:ListItem>
<asp:ListItem>Vermont</asp:ListItem>
<asp:ListItem>Virginia</asp:ListItem>
<asp:ListItem>Washington</asp:ListItem>
<asp:ListItem>West Virginia</asp:ListItem>
<asp:ListItem>Wisconsin</asp:ListItem>
<asp:ListItem>Wyoming</asp:ListItem>
<asp:ListItem>British Columbia</asp:ListItem>
<asp:ListItem>Manitoba</asp:ListItem>
<asp:ListItem>New Brunswick</asp:ListItem>
<asp:ListItem>Newfoundland & Labrador</asp:ListItem>
<asp:ListItem>Northwest Territories</asp:ListItem>
<asp:ListItem>Nova Scotia</asp:ListItem>
<asp:ListItem>Nunavut</asp:ListItem>
<asp:ListItem>Ontario</asp:ListItem>
<asp:ListItem>Prince Edward Island</asp:ListItem>
<asp:ListItem>Quebec</asp:ListItem>
<asp:ListItem>Saskatchewan</asp:ListItem>
<asp:ListItem>Yukon Territories</asp:ListItem>--%>