﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using System.Net;

public class ChangeLoadStatus
{
    public ChangeLoadStatus() { }
    public bool UpdateLoadStatus(int loadid, int LoadStatus, DateTime LoadstatusChangeTime, int loginid, string note)
    {
        bool returnvalue = false;
        try
        {
            int i = -1;
            parametereforChangestatus parameter = new parametereforChangestatus();
            parameter.bint_LoadStatusId = LoadStatus;
            parameter.bint_LoadId = loadid;
            parameter.Action = 'I';
            parameter.date_CreateDate = LoadstatusChangeTime;
            parameter.nvar_Notes = note;
            parameter.UserId = loginid;
            parameter.nvar_Location = string.Empty;
            string[] str = { "bint_LoadStatusId", "bint_LoadId", "Action", "date_CreateDate", "nvar_Notes", "UserId", "nvar_Location" };
            BaseClass bis = new BaseClass();
            i = bis.savedata(parameter, "sp_Insert_Update_TrackLoad", str);
            if (i >= 1)
            {
                returnvalue = true;
            }
            else
                returnvalue = false;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return returnvalue;
    }
    public DateTime LoadLastDate(int loadid)
    {
        DateTime dateupdate = new DateTime();
        try
        {
            parametereforChangestatus parameter = new parametereforChangestatus();
            parameter.bint_LoadId = loadid;
            string[] str = { "bint_LoadId" };
            BaseClass bis = new BaseClass();
            DataSet ds = bis.Loaddata(parameter, "Get_Load_tracking_LastDate", str);
            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            dateupdate = Convert.ToDateTime(dt.Rows[0]["date_CreateDate"].ToString());
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            dateupdate = new DateTime();
        }


        return dateupdate;
        //Get_Load_tracking_LastDate
    }
    public bool LoadIsReachedDestination(int loadid)
    {
        bool dateupdate = false;
        try
        {
            parametereforChangestatus parameter = new parametereforChangestatus();
            parameter.bint_LoadId = loadid;
            string[] str = { "bint_LoadId" };
            BaseClass bis = new BaseClass();
            DataSet ds = bis.Loaddata(parameter, "Get_Load_Get_Is_ReachedDestination", str);
            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            if (Convert.ToInt32(dt.Rows[0]["Load_Tracking_count"].ToString()) > 0)
                            {
                                dateupdate = false;
                            }
                            else
                            {
                                dateupdate = true;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            dateupdate = false;
        }


        return dateupdate;
        //Get_Load_tracking_LastDate
    }
    public bool LoadIsReachedDestinationandwait(int loadid)
    {
        bool dateupdate = false;
        try
        {
            parametereforChangestatus parameter = new parametereforChangestatus();
            parameter.bint_LoadId = loadid;
            string[] str = { "bint_LoadId" };
            BaseClass bis = new BaseClass();
            DataSet ds = bis.Loaddata(parameter, "Get_Load_Get_Is_DriveronWaiting", str);
            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            if (Convert.ToInt32(dt.Rows[0]["Load_Tracking_count"].ToString()) > 0)
                            {
                                dateupdate = false;
                            }
                            else
                            {
                                dateupdate = true;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            dateupdate = false;
        }


        return dateupdate;
        //Get_Load_tracking_LastDate
    }
    public bool SendShipmentStatus(string aggregator, string partner, string strshipmentno, string statuscode, string strStatusorAppointmentReasonCode, DateTime statusTime, string cityname, string stateorprovincecode, int stopsequencenumber)
    {
        try
        {
            string url = ConfigurationManager.AppSettings["PostmanURL"] + "api/SendShipmentStatus/";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.Headers.Add("token", ConfigurationManager.AppSettings["EDIToken"]);
            request.Headers.Add("aggregator", aggregator);
            request.Headers.Add("partner", partner);
            request.Headers.Add("strshipmentno", strshipmentno);
            request.Headers.Add("statuscode", statuscode);
            request.Headers.Add("strStatusorAppointmentReasonCode", strStatusorAppointmentReasonCode);
            request.Headers.Add("ststustime", statusTime.ToString());
            request.Headers.Add("cityname", cityname);
            request.Headers.Add("stateorprovincecode", stateorprovincecode);
            request.Headers.Add("stopsequencenumber", stopsequencenumber.ToString());
            request.ContentLength = 0;
            var response = (HttpWebResponse)request.GetResponse();
            string content = string.Empty;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }
            else
            {
                return false;
            }
           
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public void SendInvoice(string aggregator, string partner, string strshipmentno,string invoicenumber)
    {
        string url = ConfigurationManager.AppSettings["PostmanURL"] + "api/GenerateInvoice/";
        var request = (HttpWebRequest)WebRequest.Create(url);
        request.Method = "POST";
        request.Headers.Add("token", ConfigurationManager.AppSettings["EDIToken"]);
        request.Headers.Add("aggregator", aggregator);
        request.Headers.Add("partner", partner);
        request.Headers.Add("strshipmentno", strshipmentno);
        request.Headers.Add("invoicenumber", invoicenumber);
        request.ContentLength = 0;
        var response = (HttpWebResponse)request.GetResponse();
        string content = string.Empty;
        if (response.StatusCode == HttpStatusCode.OK)
        {
        }
    }

    public void UpdateLoadStatusToPostman(long LoadId, string Status, DateTime StatusTime)
     {
        const string SWP = "Stay with Pick";
        const string SWD = "Stay with Delivery";
        const string DEP = "Drop Empty at Pick";
        const string PLP = "Pull Loaded from Pick";
        const string DLD = "Drop Loaded at Delivery";
        const string OD = "Origin Drayage";
        const string DD = "Destination Drayage";

        try
        {
            string Statuses = "Pickedup,Reached Destination,Drop in Warehouse,Delivered,Invoiceable,En-route,Terminated";
            if (Statuses.Contains(Status))
            {
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["conString"], "SP_GetLoadDetailsforPostman", new object[] { LoadId });
                if (ds.Tables != null && ds.Tables.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(ds.Tables[0].Rows[0][0].ToString()) && !string.IsNullOrWhiteSpace(ds.Tables[0].Rows[0][1].ToString()) && !string.IsNullOrWhiteSpace(ds.Tables[0].Rows[0][2].ToString()))
                    {
                        string CustName = ds.Tables[0].Rows[0][0].ToString();
                        string OrderNum = ds.Tables[0].Rows[0][1].ToString();
                        string Remarks = ds.Tables[0].Rows[0][2].ToString();
                        bool IsEdi = bool.Parse(ds.Tables[0].Rows[0][3].ToString());
                        string partner = ConfigurationManager.AppSettings["PartnerCode"];
                        string StatusCode = string.Empty;
                        string StatusResonCode = "NA";
                        string CityName = string.Empty;
                        string StateorProvinceCode = string.Empty;
                        int sequenceNumber = 0;
                        if (ds.Tables.Count > 1 && ds.Tables[1].Rows != null && ds.Tables[1].Rows.Count < 2 && ds.Tables[1].Rows.Count > 0)
                        {
                            CityName = ds.Tables[1].Rows[0][0].ToString();
                            StateorProvinceCode = ds.Tables[1].Rows[0][1].ToString();
                            sequenceNumber = int.Parse(ds.Tables[1].Rows[0][2].ToString());
                            if (IsEdi)
                                if (Remarks.ToLower().Contains(DEP.ToLower()) && (Status == "Pickedup" || Status == "Drop in Warehouse"))
                                {
                                    StatusCode = Status == "Pickedup" ? "X8" : "DS";
                                    SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime, CityName, StateorProvinceCode, sequenceNumber);
                                }
                                else if (Remarks.ToLower().Contains(PLP.ToLower()) && (Status == "Reached Destination" || Status == "Pickedup" || Status == "Drop in Warehouse"))
                                {
                                    if (Status == "Reached Destination")
                                    {
                                        StatusCode = "X3";
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime, CityName, StateorProvinceCode, sequenceNumber);
                                        StatusCode = "AF";
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime, CityName, StateorProvinceCode, sequenceNumber);
                                    }
                                    else if (Status == "Pickedup")
                                    {
                                        StatusCode = "X8";
                                        DataSet ds1 = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "sp_GetAllStatusInfoByLoadId", new object[] { LoadId });
                                        if (ds1.Tables != null && ds1.Tables.Count > 0)
                                        {
                                            //bool isPuckup = false;
                                            foreach (DataRow row in ds1.Tables[0].Rows)
                                            {
                                                if (row[2].ToString() == "Drop in Warehouse") StatusCode = "CP";
                                            }
                                        }
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime, CityName, StateorProvinceCode, sequenceNumber);
                                    }
                                    else
                                    {
                                        StatusCode = "DS";
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime.AddMinutes(20), CityName, StateorProvinceCode, sequenceNumber);
                                    }
                                }
                                else if (Remarks.ToLower().Contains(SWP.ToLower()) && (Status == "Reached Destination" || Status == "Pickedup" || Status == "Delivered"))
                                {
                                    if (Status == "Reached Destination")
                                    {
                                        StatusCode = "X3";
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime, CityName, StateorProvinceCode, sequenceNumber);
                                    }
                                    else if (Status == "Pickedup")
                                    {
                                        StatusCode = "CP";
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime, CityName, StateorProvinceCode, sequenceNumber);
                                    }
                                    else
                                    {
                                        StatusCode = "AF";
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime.AddMinutes(20), CityName, StateorProvinceCode, sequenceNumber);
                                    }
                                }
                                else if (Remarks.ToLower().Contains(DLD.ToLower()) && (Status == "Reached Destination" || Status == "Drop in Warehouse" || Status == "Delivered"))
                                {
                                    if (Status == "Reached Destination")
                                    {
                                        StatusCode = "X1";
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime, CityName, StateorProvinceCode, sequenceNumber);
                                    }
                                    else if (Status == "Drop in Warehouse")
                                    {
                                        StatusCode = "S1";
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime, CityName, StateorProvinceCode, sequenceNumber);
                                    }
                                    else if (Status == "Delivered")
                                    {
                                        StatusCode = "D1";
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime, CityName, StateorProvinceCode, sequenceNumber);
                                        StatusCode = "CD";
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime.AddMinutes(20), CityName, StateorProvinceCode, sequenceNumber);
                                    }
                                }
                                else if (Remarks.ToLower().Contains(SWD.ToLower()) && (Status == "Reached Destination" || Status == "Delivered"))
                                {
                                    if (Status == "Reached Destination")
                                    {
                                        StatusCode = "X1";
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime, CityName, StateorProvinceCode, sequenceNumber);
                                    }
                                    else if (Status == "Delivered")
                                    {
                                        StatusCode = "D1";
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime, CityName, StateorProvinceCode, sequenceNumber);
                                        StatusCode = "CD";
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime.AddMinutes(20), CityName, StateorProvinceCode, sequenceNumber);
                                    }
                                }

                                else if (Remarks.ToLower().Contains(OD.ToLower()) && (Status == "Reached Destination" || Status == "En-route" || Status == "Pickedup"))
                                {
                                    StatusResonCode = "NS";
                                    if (Status == "Reached Destination")
                                    {
                                        StatusCode = "X3";
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime, CityName, StateorProvinceCode, sequenceNumber);
                                    }
                                    if (Status == "Pickedup")
                                    {
                                        StatusCode = "AF";
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime, CityName, StateorProvinceCode, sequenceNumber);
                                    }
                                    else if (Status == "En-route")
                                    {
                                        StatusCode = "X6";
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime, CityName, StateorProvinceCode, sequenceNumber);
                                        StatusCode = "AA";
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime.AddMinutes(20), CityName, StateorProvinceCode, sequenceNumber);
                                    }
                                }
                                else if (Remarks.ToLower().Contains(DD.ToLower()) && (Status == "Reached Destination" || Status == "Delivered" || Status == "En-route"))
                                {
                                    StatusResonCode = "NS";
                                    if (Status == "Reached Destination")
                                    {
                                        StatusCode = "X1";
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime, CityName, StateorProvinceCode, sequenceNumber);
                                    }
                                    if (Status == "Delivered")
                                    {
                                        StatusCode = "D1";
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime, CityName, StateorProvinceCode, sequenceNumber);
                                    }
                                    else if (Status == "En-route")
                                    {
                                        StatusCode = "X6";
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime, CityName, StateorProvinceCode, sequenceNumber);
                                        StatusCode = "AB";
                                        SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime.AddMinutes(20), CityName, StateorProvinceCode, sequenceNumber);
                                    }
                                }
                        }
                        if (IsEdi && Status == "Terminated" && ds.Tables.Count > 1 && ds.Tables[1].Rows != null && ds.Tables[1].Rows.Count > 0 && ((Remarks.ToLower().Contains(DEP.ToLower())) || (Remarks.ToLower().Contains(DLD.ToLower())) || (Remarks.ToLower().Contains(SWD.ToLower())) || (Remarks.ToLower().Contains(SWP.ToLower())) || (Remarks.ToLower().Contains(PLP.ToLower()))))
                        {                           
                            CityName = ds.Tables[1].Rows[0][0].ToString();
                            StateorProvinceCode = ds.Tables[1].Rows[0][1].ToString();
                            sequenceNumber = int.Parse(ds.Tables[1].Rows[0][2].ToString());
                            StatusCode = "TM";
                            SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, StatusTime, CityName, StateorProvinceCode, sequenceNumber);
                        }
                        if (IsEdi && Status == "Invoiceable")
                        {
                            SendInvoice(CustName, partner, OrderNum, ConfigurationManager.AppSettings["InvoiceNumberPrefix"].ToString() + LoadId);
                        }
                       
                    }
                }
            }
        }
        catch (Exception ex)
        { }

    }
}
public class parametereforChangestatus : NewBaseClass
    {
        public parametereforChangestatus()
        {
        }
        
    }