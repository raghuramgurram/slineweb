using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Manager_LoadswithPaperworkPending : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Loads With Paper Work Pending";

    }

    protected void RadDriver_CheckedChanged(object sender, EventArgs e)
    {
        //GetOldDates();
        ddlDriverorCarrier.Visible = true;
        FillDrivers();
    }
    protected void RadCarrier_CheckedChanged(object sender, EventArgs e)
    {
        //GetOldDates();
        ddlDriverorCarrier.Visible = true;
        FillCarriers();
    }
    protected void RadAll_CheckedChanged(object sender, EventArgs e)
    {
        //GetOldDates();
        ddlDriverorCarrier.Visible = false;
    }
    private void FillDrivers()
    {
        ddlDriverorCarrier.Items.Clear();
        //dt = DBClass.returnDataTable("select bint_EmployeeId as 'Id',nvar_NickName as 'Name' from eTn_Employee where bint_RoleId=(select R.bint_RoleId from eTn_Role R where lower(nvar_RoleName)='driver') order by [Name]");
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriverNamesForaLocation", new object[] { officeLocationId });
        //SLine 2017 Enhancement for Office Location Ends
        if (ds.Tables.Count > 0)
        {
            ddlDriverorCarrier.DataSource = ds.Tables[0];
            ddlDriverorCarrier.DataTextField = "Name";
            ddlDriverorCarrier.DataValueField = "Id";
            ddlDriverorCarrier.DataBind();
        }
        ListItem li = new ListItem("All", "0");
        ddlDriverorCarrier.Items.Insert(0, li);
        ddlDriverorCarrier.SelectedIndex = 0;
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    protected void FillCarriers()
    {
        ddlDriverorCarrier.Items.Clear();
        //DataTable dt = DBClass.returnDataTable("select bint_CarrierId,nvar_CarrierName from eTn_Carrier order by [nvar_CarrierName]");
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCarrierNames", new object[] { officeLocationId });
        //SLine 2017 Enhancement for Office Location Ends
        if (ds.Tables.Count > 0)
        {
            ddlDriverorCarrier.DataSource = ds.Tables[0];
            ddlDriverorCarrier.DataTextField = "nvar_CarrierName";
            ddlDriverorCarrier.DataValueField = "bint_CarrierId";
            ddlDriverorCarrier.DataBind();
        }
        ListItem li = new ListItem("All", "0");
        ddlDriverorCarrier.Items.Insert(0, li);
        ddlDriverorCarrier.SelectedIndex = 0;
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    protected void btnSearch_Click(object sender, EventArgs e)

    {
        string type= string.Empty;
         if (RadAll.Checked)
            type = "All";
        else if (RadDriver.Checked)
            type = "Driver";
        else if (RadCarrier.Checked)
            type = "Carrier";
         int id = 0;
        if (ddlDriverorCarrier.SelectedValue != "")
             id = Convert.ToInt32(ddlDriverorCarrier.SelectedValue.ToString());

         Session["ReportParameters"] = type + "~~^^^^~~" + id.ToString();
         Response.Redirect("~/Manager/DisplayReport.aspx");
    }
}
