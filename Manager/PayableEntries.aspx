<%@ Page AutoEventWireup="true" CodeFile="PayableEntries.aspx.cs" Inherits="PayableEntries"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>
                <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td valign="middle">
                            &nbsp;From: <uc1:DatePicker ID="dtpFromDate" runat="server" IsDefault="false" IsRequired="false" CountNextYears="2" CountPreviousYears="0" SetInitialDate="true" />
             &nbsp; &nbsp; &nbsp; To: <uc1:DatePicker ID="dtpToDate" runat="server" IsDefault="false" IsRequired="false" CountNextYears="2" CountPreviousYears="0" SetInitialDate="true" />
                           &nbsp; &nbsp; <asp:Button ID="btnDateSearch" runat="server" CssClass="btnstyle" OnClick="btnDateSearch_Click"
                                Text="Search" Width="51px" />
                                
                        </td>
             <td valign="middle">
     
           &nbsp; &nbsp;Track Load:<asp:TextBox ID="txtContainer" runat="server" Width="92px"></asp:TextBox>
            <asp:DropDownList ID="ddlContainer" runat="server">
                <asp:ListItem Value="Load#" Selected="True">Load#</asp:ListItem>
                <asp:ListItem Value="Container#">Container#</asp:ListItem>
                <asp:ListItem Value="Booking#">Booking#</asp:ListItem>
                <asp:ListItem Value="Chasis#">Chasis#</asp:ListItem>
                <asp:ListItem>Tag#</asp:ListItem>
            </asp:DropDownList>
            <asp:Button ID="btnSearch" runat="server" CssClass="btnstyle" Text="Search" OnClick="btnSearch_Click" Width="51px" CausesValidation="False" />
             <asp:Label ID="lblErrorMsg" runat="server" ForeColor="Red" Text="Invalid LoadId."
                 Visible="False"></asp:Label></td>
                        <td>
                        </td>
                    </tr>
                        <tr>
                            <td>
                                <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                        </tr>
                    <tr>
                        <td valign="middle" style="font-weight:bold">          
                        <table cellpadding="0" cellspacing="0" border="0" style="font-size:12px;">
                        <tr>
                        <td>              
                            &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;<asp:RadioButton ID="RadDriver" runat="server" Text="  Drivers " AutoPostBack="True" OnCheckedChanged="RadDriver_CheckedChanged" GroupName="driverorcarrier"/>
                            &nbsp; &nbsp;<asp:RadioButton ID="RadCarrier" runat="server" Text="  Carriers " AutoPostBack="True" OnCheckedChanged="RadCarrier_CheckedChanged" GroupName="driverorcarrier"/>
                            &nbsp; &nbsp;<asp:RadioButton ID="RadAll" runat="server" Text="  All " AutoPostBack="True" OnCheckedChanged="RadAll_CheckedChanged" GroupName="driverorcarrier"/>
                            
                            </td>
                            <td>
                            &nbsp; &nbsp;<asp:DropDownList ID="ddlDriverorCarrier" runat="server" Visible="false">
                            </asp:DropDownList>
                            </td>
                        
                        </tr>
                        
                         <tr>
                         <td><asp:Label ID="lblMsg" runat="server"  Text="Use 'Ctrl' or 'Shift' key to select multiple drivers." Visible="false"></asp:Label></td>
                    <td style="padding-top:10px">
                    &nbsp; &nbsp;<asp:ListBox ID="ltbDrivers" runat="server" Visible="false" SelectionMode="multiple" Height="100px"></asp:ListBox>                            
                    </td>
                    </tr>
                    <tr id="tblPrintCheckDate" runat="server" visible="false">
                      <td><asp:Label ID="lblPrintCheckDate" runat="server"  Text="Check Date (to Print):"></asp:Label></td>
                    <td style="padding-top:10px">
                    &nbsp; &nbsp;<uc1:DatePicker ID="dtpPrintCheckDate" runat="server" IsDefault="false" IsRequired="false" CountNextYears="2" CountPreviousYears="0" SetInitialDate="true"/>
                    </td>
                    </tr>
                    <tr style="padding-top:10px"><td></td>
                    <td style="padding-left:10px; padding-top:10px">
                    <asp:Button ID="btnExportDrives" runat="server" CssClass="btnstyle" OnClick="btnExportDrives_Click" Text="Print Payable Entries" visible="false"/>
                    <asp:Button ID="btnPrintDrivesChecks" runat="server" CssClass="btnstyle" OnClick="btnPrintDrivesChecks_Click" Text="Print Checks" visible="false" style="margin-left:60px"/>
                    </td></tr>
                        
                        </table>  
                        </td>
                    </tr>
                   
                </table> 
                </td>               
        </tr>
    </table>
   
</asp:Content>