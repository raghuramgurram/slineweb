using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Carriertop : System.Web.UI.UserControl
{
  
    protected void Page_Load(object sender, EventArgs e)
    {
        lblHeadText.Text = Convert.ToString(Session["TopHeaderText"]);
        lblUserId.Text = Convert.ToString(Session["UserId"]);        
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        lblHeadText.Text = Convert.ToString(Session["TopHeaderText"]);
    }
    protected void lnlLogOut_Click(object sender, EventArgs e)
    {
        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_UpdateLogOutDate", new object[] { Convert.ToString(Session["LogsId"]) });
        Response.Cookies.Clear();
        Request.Cookies.Clear();
        Session["UserId"] = null;
        Session.Clear();
        Session.Abandon();        
        FormsAuthentication.SignOut();        
        Response.Redirect("../Login.aspx");
    }
}
