<%@ Page AutoEventWireup="true" CodeFile="SearchEquipment.aspx.cs" Inherits="SearchEquipment"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/Grid.ascx" TagName="Grid" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="ContentTbl">
<tr valign="top">
    <td>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblbox1">
        <tr>
            <td align="left">
                &nbsp;<asp:Label ID="lblText" runat="server" Text="Equipment Name : "></asp:Label>&nbsp;
                <asp:DropDownList ID="ddlList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlList_SelectedIndexChanged">
                    <asp:ListItem Selected="True" Value="1">Active</asp:ListItem>
                    <asp:ListItem Value="0">In Active</asp:ListItem>
                </asp:DropDownList>&nbsp;
            </td>          
           </tr>
        </table> 
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
          </tr>
        </table> 
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="left" width="100%">
                  <uc2:GridBar ID="barCurrent" runat="server" HeaderText="Equipments" LinksList="Add New Equipment" PagesList="NewEquipment.aspx" Visible="true"/>                
              </td>
            </tr>
          </table>
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
          </tr>
        </table> 
       <table cellpadding="0" cellspacing="0" width="100%">
          <tr>
          <td>
             <uc3:Grid ID="gridCurrent" runat="server" />
          </td>
        </tr>
      </table>
     </td>
  </tr>
</table>

</asp:Content>

