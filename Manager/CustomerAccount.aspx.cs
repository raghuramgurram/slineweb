using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CustomerAccount : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Customer Accounts";
        Session["Type"] = "Customer";
        gridCurrent.EditPage = "NewCarrierAccount.aspx";
        if (!IsPostBack)
        {
            AssignListValues();
            if (ddlList.Items.Count > 0)
            {
                gridCurrent.Visible = true;
                gridCurrent.DeleteVisible = true;
                gridCurrent.EditVisible = true;
                gridCurrent.TableName = "eTn_Customer";
                gridCurrent.Primarykey = "eTn_UserLogin.bint_UserLoginId";
                gridCurrent.PageSize = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["GeneralPageSize"]);
                gridCurrent.ColumnsList = "eTn_UserLogin.bint_UserLoginId;eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName  As 'Name';" +
                    "eTn_UserLogin.nvar_UserId;eTn_UserLogin.nvar_Password;eTn_Role.nvar_RoleName;" + CommonFunctions.DateQueryString("eTn_Customer.date_CreatedDate", "Created");
                gridCurrent.VisibleColumnsList = "Name;nvar_RoleName;nvar_UserId;nvar_Password;Created";
                gridCurrent.VisibleHeadersList = "Name;Role;User Id;Password;Created";
                gridCurrent.DeleteTablesList = "eTn_UserLogin";
                gridCurrent.DeleteTablePKList = "bint_UserLoginId";
                gridCurrent.DeleteTableAddtionialWhereClauseList = "bint_RoleId=" + DBClass.executeScalar("select bint_RoleId from eTn_Role where Lower(nvar_RoleName)='customer'");
                gridCurrent.InnerJoinClause = " inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_Customer.bint_CustomerId " +
                    " inner join eTn_Role on eTn_Role.bint_RoleId = eTn_UserLogin.bint_RoleId ";
                gridCurrent.EditPage = "NewCarrierAccount.aspx";
                gridCurrent.BindGrid(0);
                if (Request.QueryString.Count > 0)
                {
                    ddlList.SelectedIndex = ddlList.Items.IndexOf(ddlList.Items.FindByValue(Convert.ToString(Request.QueryString[0])));
                }
                gridCurrent.WhereClause = "eTn_Customer.bint_CustomerId=" + Convert.ToString(ddlList.SelectedItem.Value).Trim() + " and "+
                    "eTn_UserLogin.bint_RoleId=" + DBClass.executeScalar("select bint_RoleId from eTn_Role where Lower(nvar_RoleName)='customer'");
                if (Convert.ToInt32(ddlList.SelectedValue) == 0)
                {
                    barCurrent.PagesList = "";
                    barCurrent.LinksList = "";
                }
                else
                {
                    barCurrent.LinksList = "Add Customer Account";
                    barCurrent.PagesList = "NewCarrierAccount.aspx?" + Convert.ToString(ddlList.SelectedItem.Value).Trim() + "New";
                }
            }
        }
    }
    protected void AssignListValues()
    {
        //ddlList.DataSource = DBClass.returnDataTable("Select bint_CustomerId,nvar_CustomerName from eTn_Customer order by [nvar_CustomerName]");
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCustomerNames");
        if(ds.Tables.Count>0)
        {
            ddlList.DataSource = ds.Tables[0];
            ddlList.DataValueField = "bint_CustomerId";
            ddlList.DataTextField = "nvar_CustomerName";
            ddlList.DataBind();
        }
        ddlList.Items.Insert(0, new ListItem("--Select--", "0"));
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    protected void ddlList_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["GridPageIndex"] = 0;
        if (string.Compare(ddlList.SelectedItem.Text, "--Select--", true, System.Globalization.CultureInfo.CurrentCulture) != 0)
        {
            gridCurrent.WhereClause = "eTn_Customer.bint_CustomerId=" + Convert.ToString(ddlList.SelectedItem.Value).Trim() + " and " +
                    "eTn_UserLogin.bint_RoleId=" + DBClass.executeScalar("select bint_RoleId from eTn_Role where Lower(nvar_RoleName)='customer'");
            gridCurrent.BindGrid(0);
            gridCurrent.Visible = true;
            barCurrent.LinksList = "Add Customer Account";
            barCurrent.PagesList = "NewCarrierAccount.aspx?" + Convert.ToString(ddlList.SelectedItem.Value).Trim() + "New";
            barCurrent.AssignPagesToLinks();
        }
        else
        {
            gridCurrent.Visible = false;
            barCurrent.PagesList = "";
            barCurrent.LinksList = "";
            barCurrent.AssignPagesToLinks();
        }
    }
}
