using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class EmployeeAccount : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Employee";
        Session["BackPage4"] = null;
        if (!IsPostBack)
        {
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetRoleIds");
            string strRoles = "";
            if (ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (i == 0)
                        strRoles += Convert.ToString(ds.Tables[0].Rows[i][0]).Trim();
                    else
                        strRoles += "~" + Convert.ToString(ds.Tables[0].Rows[i][0]).Trim();
                }
                ViewState["Roles"] = strRoles.Replace('~', ',');
                
            }
           if (ds != null) { ds.Dispose(); ds = null; }
            btnShow.Attributes.Add("OnClick", "javascript:return ValidateSearch('"+ddlList.ClientID+"');");
            btnSearch.Attributes.Add("OnClick", "javascript:return ValidateSearchText('"+txtSearch.ClientID+"');");
            barCurrent.PagesList = "NewEmployee.aspx";
            if (Request.QueryString["DDlVal"] != null)
            {
                ddlList.SelectedValue = Convert.ToString(Request.QueryString["DDlVal"].ToString());
                Session["ddlListValueselected"] = null;
                btnShow_Click(null, null);
            }
            else
            {
                Session["ddlListValueselected"] = null;
            }
        }
    }
    protected void ddlList_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["GridPageIndex"] = 0;
        gridCurrent.WhereClause = "eTn_Employee.bit_Status=" + Convert.ToString(ddlList.SelectedItem.Value).Trim()+
             " and eTn_Role.bint_RoleId in (" + Convert.ToString(ViewState["Roles"]).Trim() + ")";
        gridCurrent.BindGrid(0);
        gridCurrent.Visible = true;
        barCurrent.AssignPagesToLinks();
    }

    protected void btnShow_Click(object sender, EventArgs e)
    {
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        //SLine 2017 Enhancement for Office Location Ends
        Session["ddlListValueselected"] = ddlList.SelectedValue;
        gridCurrent.Visible = true;
        gridCurrent.DeleteVisible = true;
        gridCurrent.EditVisible = true;
        gridCurrent.IsPagerVisible = true;
        gridCurrent.PageSize = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["GeneralPageSize"]);
        gridCurrent.TableName = "eTn_Employee";
        gridCurrent.Primarykey = "eTn_Employee.bint_EmployeeId";
        gridCurrent.ColumnsList = "Convert(varchar(20),eTn_Employee.bint_EmployeeId)+'^'+Convert(varchar(20),eTn_UserLogin.bint_UserLoginId) As 'Id';eTn_Employee.bint_EmployeeId; eTn_Employee.nvar_NickName  As 'Name';" +
            "eTn_UserLogin.nvar_UserId;eTn_UserLogin.nvar_Password;eTn_Role.nvar_RoleName;eTn_Team.nvar_TeamName;" + CommonFunctions.DateQueryString("eTn_Employee.date_CreatedDate", "Created");
        gridCurrent.VisibleColumnsList = "bint_EmployeeId;Name;nvar_RoleName;nvar_TeamName;nvar_UserId;nvar_Password;Created";
        gridCurrent.VisibleHeadersList = "ID;Name;Role;Team;User Id;Password;Created";
        gridCurrent.DeleteTablesList = "eTn_UserLogin;eTn_Employee;eTn_UserLogs";
        gridCurrent.DeleteTablePKList = "bint_RolePlayerId;bint_EmployeeId;bint_UserLoginId";
        gridCurrent.DeleteTableAddtionialWhereClauseList = "bint_RoleId in (" + Convert.ToString(ViewState["Roles"]).Trim() + ");";
        gridCurrent.DependentTablesList = "eTn_Drivers;eTn_AssignDriver";
        gridCurrent.DependentTableKeysList = "DriverId;bint_EmployeeId";
        gridCurrent.InnerJoinClause = " inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_Employee.bint_EmployeeId " +
            " inner join eTn_Role on eTn_Role.bint_RoleId = eTn_UserLogin.bint_RoleId " +
            //SLine 2017 Enhancement for Office Location
            " inner join eTn_OfficeLocation_User on eTn_UserLogin.bint_UserLoginId = eTn_OfficeLocation_User.bint_UserLoginId " +
            //Sline 2021 enhancement
            "left join eTn_Team on eTn_Team.bint_TeamId = eTn_Employee.bint_TeamId";
        gridCurrent.EditPage = "~/Manager/NewEmployee.aspx";
        gridCurrent.WhereClause = "eTn_Employee.bit_Status=" + Convert.ToString(ddlList.SelectedItem.Value).Trim() +
            " and eTn_Role.bint_RoleId in (" + Convert.ToString(ViewState["Roles"]).Trim() + ")" +
            //SLine 2017 Enhancement for Office Location
            " and eTn_OfficeLocation_User.bint_OfficeLocationId = " + officeLocationId;
        gridCurrent.OrderByClause = "eTn_Employee.bint_EmployeeId asc";//" eTn_Employee.nvar_NickName ";
        gridCurrent.FunctionName = "DeleteDriver";
        gridCurrent.BindGrid(0);
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        //SLine 2017 Enhancement for Office Location Ends
        ddlList.SelectedIndex = 0;
        gridCurrent.Visible = true;
        gridCurrent.DeleteVisible = true;
        gridCurrent.EditVisible = true;
        gridCurrent.IsPagerVisible = true;
        gridCurrent.PageSize = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["GeneralPageSize"]);
        gridCurrent.TableName = "eTn_Employee";
        gridCurrent.Primarykey = "eTn_Employee.bint_EmployeeId";
        gridCurrent.ColumnsList = "Convert(varchar(20),eTn_Employee.bint_EmployeeId)+'^'+Convert(varchar(20),eTn_UserLogin.bint_UserLoginId) As 'Id';eTn_Employee.nvar_NickName  As 'Name';" +
            "eTn_UserLogin.nvar_UserId;eTn_UserLogin.nvar_Password;case when eTn_Employee.bit_Status=1 then 'Active' else 'In Active' end  as 'Status';eTn_Role.nvar_RoleName;" + CommonFunctions.DateQueryString("eTn_Employee.date_CreatedDate", "Created");
        gridCurrent.VisibleColumnsList = "Name;nvar_RoleName;nvar_UserId;nvar_Password;Status;Created";
        gridCurrent.VisibleHeadersList = "Name;Role;User Id;Password;Status;Created";
        gridCurrent.DeleteTablesList = "eTn_UserLogin;eTn_Employee;eTn_UserLogs";
        gridCurrent.DeleteTablePKList = "bint_RolePlayerId;bint_EmployeeId;bint_UserLoginId";
        gridCurrent.DeleteTableAddtionialWhereClauseList = "bint_RoleId=2;";
        gridCurrent.DependentTablesList = "eTn_Drivers;eTn_AssignDriver";
        gridCurrent.DependentTableKeysList = "DriverId;bint_EmployeeId";
        gridCurrent.InnerJoinClause = " inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_Employee.bint_EmployeeId left outer join [eTn_Equipment] on eTn_Equipment.bint_EquipmentId=eTn_Employee.bint_EquipmentId" +
            " inner join eTn_Role on eTn_Role.bint_RoleId = eTn_UserLogin.bint_RoleId " +
            //SLine 2017 Enhancement for Office Location
            " inner join eTn_OfficeLocation_User on eTn_UserLogin.bint_UserLoginId = eTn_OfficeLocation_User.bint_UserLoginId ";
        gridCurrent.EditPage = "~/Manager/NewEmployee.aspx";
        gridCurrent.WhereClause = "eTn_Role.bint_RoleId=2 and (eTn_Employee.var_License like '" + txtSearch.Text + "%' or eTn_Equipment.nvar_Plate like '"+txtSearch.Text.Trim()+"%')" +
            //SLine 2017 Enhancement for Office Location
            " and eTn_OfficeLocation_User.bint_OfficeLocationId = " + officeLocationId;
        gridCurrent.OrderByClause = " eTn_Employee.nvar_NickName ";
        gridCurrent.FunctionName = "DeleteDriver";
        gridCurrent.BindGrid(0);
    }
}
