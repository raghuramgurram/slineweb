<%@ Page AutoEventWireup="true" CodeFile="LoadDetails.aspx.cs" Inherits="LoadDetails"
    Language="C#" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/Grid.ascx" TagName="Grid" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<script type="text/javascript">

function WinClose()
{
    self.close();
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>S Line Transport Inc</title>
    <meta content="revealTrans(Duration=0.5)" http-equiv="Page-Enter"/>
    <meta content="revealTrans(Duration=0.5)" http-equiv="Page-Exit"/>
    <link href="../Styles/style.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/OwnStyle.css" rel="stylesheet" type="text/css" />

    <script language="JavaScript" src="../mm_menu.js"></script>
</head>
<body >
    <form id="form1" runat="server">
        <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr style="width:100%;">
                <th>
                    Track Load
                </th>
            </tr>
        </table>
       <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" style="width: 30%;" valign="top">
                                <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <th colspan="2">
                                            Load Identification -&nbsp;<asp:Label ID="lblLoadId" runat="server" Text="LoadId">
                                            </asp:Label>&nbsp;</th>
                                    </tr>
                                    <tr id="row">
                                        <td align="right" width="40%">
                                            Ref.# : &nbsp;
                                        </td>
                                        <td width="60%">
                                            <asp:Label ID="lblRef" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr id="altrow">
                                        <td align="right" width="40%">
                                            Assigned To :
                                        </td>
                                        <td width="60%">
                                            <asp:Label ID="lblAssigned" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <img alt="" height="8" src="../Images/pix.gif" width="1" /></td>
                                    </tr>
                                </table>
                                <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <th colspan="2">
                                            Order Status
                                        </th>
                                    </tr>
                                    <tr id="row">
                                        <td align="right" width="40%">
                                            Load Status :
                                        </td>
                                        <td>
                                            <asp:Label ID="lblStatus" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr id="altrow">
                                        <td align="right">
                                            Container Size/Location :</td>
                                        <td>
                                            <asp:Label ID="lblRailBill" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr id="row">
                                        <td align="right">
                                            Order Bill Date :</td>
                                        <td>
                                            <asp:Label ID="lblBillDate" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr id="altrow">
                                        <td align="right">
                                            Person Bill :
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPersonBill" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td style="height: 8px">
                                            <img alt="" height="8" src="../Images/pix.gif" width="1" /></td>
                                    </tr>
                                </table>
                                <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <th colspan="2">
                                            Routing</th>
                                    </tr>
                                    <tr id="row">
                                        <td colspan="2" style="height: 75px">
                                            <%--<strong>RAIL LINE-HAUL </strong><br />
                      EXEL TRANSPORTATION SERVICES, INC.<br />
                      P O BOX 844711 <br />
                      DALLAS , TX 75284-4650 <br />
                      <a href="#">www.tidework.com</a>--%>
                                            <asp:Label ID="lblRoute" runat="server" Text=""></asp:Label>&nbsp;
                                        </td>
                                    </tr>
                                    <tr id="altrow">
                                        <td align="right" width="45%">
                                            Pier Termination / Empty Return:</td>
                                        <td>
                                            <asp:Label ID="lblPier" runat="server" Text=""></asp:Label>&nbsp;
                                        </td>
                                    </tr>
                                    <tr id="row" valign="top">
                                        <td align="right">
                                            Contact Person(s) :
                                        </td>
                                        <td>
                                            <asp:Label ID="lblShippingContact" runat="server" Text=""></asp:Label>&nbsp;</td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <img alt="" height="8" src="../Images/pix.gif" width="1" /></td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <uc1:GridBar ID="GridBar1" runat="server" HeaderText="Documents" />
                                        </td>
                                    </tr>
                                </table>
                                <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <th style="width: 119px">
                                            Document Type
                                        </th>
                                        <th>
                                            Date Received
                                        </th>
                                    </tr>
                                    <tr id="row">
                                        <td align="right">
                                            <asp:LinkButton ID="lnkLoadOrder" runat="server" OnClick="lnkLoadOrder_Click">Load Order</asp:LinkButton></td>
                                        <td>
                                            <asp:Label ID="lblLoadOrder" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr id="altrow">
                                        <td align="right" style="height: 15px">
                                            <asp:LinkButton ID="lnlPoOrder" runat="server" OnClick="lnlPoOrder_Click">Purchase Order</asp:LinkButton>
                                        </td>
                                        <td style="height: 15px">
                                            <asp:Label ID="lblPOrder" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr id="row">
                                        <td align="right">
                                            <asp:LinkButton ID="lnkLading" runat="server" OnClick="lnkLading_Click">Bill Of Lading</asp:LinkButton>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblBillofLading" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr id="altrow">
                                        <td align="right">
                                            <asp:LinkButton ID="lnkAccessorial" runat="server" OnClick="lnkAccessorial_Click">Accessorial Charges</asp:LinkButton>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblAccessorial" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr id="row">
                                        <td align="right">
                                            <asp:LinkButton ID="lnkOutgate" runat="server" OnClick="lnkOutgate_Click">Out-Gate Interchange</asp:LinkButton></td>
                                        <td>
                                            <asp:Label ID="lblOutGate" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr id="altrow">
                                        <td align="right">
                                            <asp:LinkButton ID="lnkInGate" runat="server" OnClick="lnkInGate_Click">In-Gate Interchange</asp:LinkButton></td>
                                        <td>
                                            <asp:Label ID="lblInGate" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr id="row">
                                        <td align="right">
                                            <asp:LinkButton ID="lnkProof" runat="server" OnClick="lnkProof_Click">Proof of Delivery</asp:LinkButton></td>
                                        <td>
                                            <asp:Label ID="lblDelivery" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr id="altrow">
                                        <td align="right">
                                            <asp:LinkButton ID="lnkMisc" runat="server" OnClick="lnkMisc_Click">Misc. / Scale Ticket</asp:LinkButton></td>
                                        <td>
                                            <asp:Label ID="lblTicket" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <img alt="" height="8" src="../Images/pix.gif" width="1" /></td>
                                    </tr>
                                </table>
                                <asp:PlaceHolder ID="plhDocs" runat="server"></asp:PlaceHolder>
                            </td>
                            <td align="left" valign="top" width="1%">
                                <img alt="" height="1" src="../images/pix.gif" width="5" /></td>
                            <td align="left" style="width: 650px" valign="top">
                                <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <th colspan="4">
                                            Order Identification
                                        </th>
                                    </tr>
                                    <tr id="row">
                                        <td align="right" valign="middle" width="15%">
                                            Customer :
                                        </td>
                                        <td colspan="3" valign="middle">
                                            <asp:Label ID="lblCustomer" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr id="altrow">
                                        <td align="right" valign="middle">
                                            Pickup# :
                                        </td>
                                        <td valign="middle" width="25%">
                                            <asp:Label ID="lblPickUp" runat="server"></asp:Label>&nbsp;</td>
                                        <td align="right" valign="middle">
                                            Commodity :
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblCommodity" runat="server" Text=""></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr id="row">
                                        <td align="right" valign="middle">
                                            Container# :
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblContainer" runat="server"></asp:Label>&nbsp;</td>
                                        <td align="right" valign="middle">
                                            Seal# :
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblSeal" runat="server" Text=""></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr id="altrow">
                                        <td align="right" valign="middle">
                                            Chasis# :
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblChasis" runat="server" Text=""></asp:Label>&nbsp;</td>
                                        <td align="right" valign="middle">
                                            Created :
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblCreated" runat="server" Text=""></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr id="row">
                                        <td align="right" valign="middle">
                                            Pieces :
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblPieces" runat="server"></asp:Label>&nbsp;
                                        </td>
                                        <td align="right" valign="middle">
                                            Create Person :
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblPersong" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr id="altrow">
                                        <td align="right" valign="middle">
                                            Weight :
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblWeigth" runat="server"></asp:Label>&nbsp;</td>
                                        <td align="right" valign="middle">
                                            Booking# :
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblBooking" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <img alt="" height="8" src="../Images/pix.gif" width="1" /></td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <uc2:Grid ID="Grid1" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <img alt="" height="8" src="../Images/pix.gif" width="1" /></td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <uc2:Grid ID="Grid2" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <img alt="" height="8" src="../Images/pix.gif" width="1" /></td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <uc2:Grid ID="Grid3" runat="server" />
                                        </td>
                                    </tr>
                                 </table>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <img alt="" height="8" src="../Images/pix.gif" width="1" /></td>
                                        </tr>
                                    </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <uc1:GridBar ID="GridBar2" runat="server" HeaderText="Load Status Information" />
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <uc2:Grid ID="Grid4" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <img alt="" height="8" src="../Images/pix.gif" width="1" /></td>
                                        </tr>
                                    </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <uc1:GridBar ID="GridBar3" runat="server" HeaderText="Notes" />
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <uc2:Grid ID="Grid5" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <img alt="" height="8" src="../Images/pix.gif" width="1" /></td>
                                        </tr>
                                    </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <uc1:GridBar ID="GridBar4" runat="server" HeaderText="Contact Persons" />
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <uc2:Grid ID="Grid6" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <table id="tblbox1" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="right" colspan="2">
                                            <input class="btnstyle" onclick="WinClose()" type="button" value="Close" />
                                            <%--<asp:Button ID="btnClose" runat="server" CssClass="btnstyle" Text="Close" OnClick=""/>--%>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
            </tr>
        </table>
    </form>
</body>
</html>
