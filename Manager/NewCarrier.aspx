<%@ Page AutoEventWireup="true" CodeFile="NewCarrier.aspx.cs" Inherits="NewCarrier"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/USState.ascx" TagName="USState" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/LinksBar.ascx" TagName="LinksBar" TagPrefix="uc5" %>

<%@ Register Src="~/UserControls/Url.ascx" TagName="Url" TagPrefix="uc2" %>

<%@ Register Src="~/UserControls/Phone.ascx" TagName="Phone" TagPrefix="uc1" %>

<%@ Register Src="~/UserControls/MsgDispBar.ascx" TagName="MsgDispBar" TagPrefix="uc4" %>
<%@ Register Src="~/UserControls/Grid.ascx" TagName="Grid" TagPrefix="uc4" %>
<%--<%@ Register Src="~/UserControls/StaticList.ascx" TagName="StaticList" TagPrefix="uc1" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td>
        <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <asp:ValidationSummary ID="ReqCarrier" runat="server" Width="526px" />
                            <asp:Label ID="lblNameError" runat="server" Font-Bold="False" ForeColor="Red" Text="Carrier name already exists." /></td>
            </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
           <td class="mandatory"><strong>
               <asp:Label ID="lblCarrier" runat="server" Text="Carrier ID :"></asp:Label>
                    <asp:Label ID="lblCarrierID" runat="server"></asp:Label>
                    </strong></td>
                    <td align="right" valign="bottom">
                        <asp:Label ID="lblInsurance" runat="server" CssClass="welcome" Text="<strong>Insurance Information</strong>"
                            Visible="False" ForeColor="Blue"></asp:Label>
                        &nbsp;<asp:LinkButton ID="lnkLink" runat="server" CausesValidation="False" CssClass="welcome"
                            Visible="false" ForeColor="Blue"><strong>Insurance Information</strong></asp:LinkButton>
                    </td>
              </tr>
        </table>
    
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="50%" valign="top">
            <table width="100%"  border="0" cellspacing="2" cellpadding="0">
              <tr valign="top">
                <td width="60%" style="height: 152px"><table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblform">
                  <tr>
                    <th colspan="4">Carrier Details </th>
                  </tr>
                  <tr id="row">
                    <td valign="middle" align="right">
                       <asp:Label ID="lblCarrierName" runat="server" Text="Carrier Name:" ></asp:Label></td>
                    <td width="30%"><asp:TextBox ID="txtCarrierName" runat="server" Width="103px" MaxLength="150"/>
                        <asp:RequiredFieldValidator ID="rfvCarrierName" runat="server" ControlToValidate="txtCarrierName"
                            Display="Dynamic" ErrorMessage="Please Enter Carrier Name.">*</asp:RequiredFieldValidator>&nbsp;
                        </td>
                    <td align="right" width="20%" style="height: 20px">Active :</td>
                    <td width="30%">
                        <asp:CheckBox ID="chkActive" runat="server" Checked="True" /></td>
                  </tr>
                  <tr id="altrow">
                    <td align="right"><asp:Label ID="lblPhone" runat="server" Text="Phone :"/></td>
                    <td style="height: 20px"><%--<asp:TextBox ID="txtPhone" runat="server" Width="109px" MaxLength="18"/>
                        <asp:RegularExpressionValidator ID="revPhone" runat="server" ControlToValidate="txtPhone"
                            Display="Dynamic" ErrorMessage="Please enter a valid Phone number in the format (123) 123-1234 or 123-123-1234."
                            ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">*</asp:RegularExpressionValidator>--%>
                        <uc1:Phone ID="txtPhone" runat="server" />
                    </td>
                    <td align="right" style="height: 20px"><asp:Label ID="lblDOTLic" runat="server" Text="DOT Lic.# :"/></td>
                    <td><asp:TextBox ID="txtDOTLic" runat="server" Width="139px" MaxLength="100"/></td>
                  </tr>
                  <tr id="row">
                    <td align="right"><asp:Label ID="lblFax" runat="server" Text="Fax :"/></td>
                    <td>
                        <%--<asp:TextBox ID="txtFax" runat="server" Width="108px" MaxLength="18"/>
                        <asp:RegularExpressionValidator ID="revFax" runat="server" ControlToValidate="txtFax"
                            Display="Dynamic" ErrorMessage="Please enter a valid Fax number in the format (123) 123-1234 or 123-123-1234."
                            ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">*</asp:RegularExpressionValidator>--%>
                        <uc1:Phone ID="txtFax" runat="server" />
                    </td>
                    <td align="right"><asp:Label ID="lblTaxID" runat="server" Text="Tax ID :"/></td>
                    <td>
                        <asp:TextBox ID="txtTaxID" runat="server" MaxLength="100" Width="140px"/></td>                    
                  </tr>
                  <tr id="altrow">
                     <td align="right" style="width: 106px"><asp:Label ID="lblMCLic" runat="server" Text="MC Lic.# :"/></td>
                    <td>
                        <asp:TextBox ID="txtMCLic" runat="server" MaxLength="100"/></td>
                    <td align="right"><asp:Label ID="lblWebsiteURL" runat="server" Text="Website URL :"/></td>
                    <td>                        
                        <uc2:Url ID="txtWebsiteURL" runat="server" />
                     <%--   <asp:TextBox ID="txtWebsiteURL" runat="server" MaxLength="100"/>
                        <asp:RegularExpressionValidator ID="revWebSiteURL" runat="server" ControlToValidate="txtWebsiteURL"
                            Display="Dynamic" ErrorMessage="Please Enter A Valid Website URL." ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?">*</asp:RegularExpressionValidator>--%>
                    </td>   
                  </tr>
                  <tr id="row">
                    <td align="right" valign="top" style="width: 106px; height: 19px;"><asp:Label ID="lblNotes" runat="server" Text="Notes :"/></td>
                    <td colspan="3" valign="top" style="height: 19px">
                        <asp:TextBox ID="txtNotes" runat="server" Height="37px" MaxLength="1000" TextMode="MultiLine" Width="173px"/></td>
                    </tr>                    
                </table>                    
                </td>
                <td width="1%" style="height: 152px"><img src="../Images/pix.gif" width="5" height="1" alt=""/></td>
                <td style="height: 152px">                
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblform">
                      <tr>
                        <th colspan="4">Payment Address </th>
                      </tr>
                      <tr id="row">
                        <td width="20%" align="right"><asp:Label ID="lblStreet" runat="server" Text="Street :"/></td>
                        <td width="30%">
                            <asp:TextBox ID="txtStreet" runat="server" BackColor="White" Width="110px" MaxLength="200"/>
                            <asp:RequiredFieldValidator ID="rfvStreet" runat="server" ControlToValidate="txtStreet"
                                Display="Dynamic" ErrorMessage="Please Enter Street.">*</asp:RequiredFieldValidator></td>
                        <td width="20%" align="right">
                         <asp:Label ID="lblState" runat="server" Text="State :"/></td>
                         <td width="20%">  
                             <uc3:USState ID="ddlState" runat="server" IsRequired="false" />
                         </td>
                      </tr>
                      <tr id="altrow">
                        <td align="right"><asp:Label ID="lblSuite" runat="server" Text="Suite# : "/> </td>
                        <td>
                            <asp:TextBox ID="txtSuite" runat="Server" Width="110px" MaxLength="100"/></td>
                        <td align="right"><asp:Label ID="lblZip" runat="server" Text="Zip : "/>&nbsp;
                        </td>
                        <td>
                            <asp:TextBox ID="txtZip" runat="Server" Width="91px" MaxLength="20"/></td>
                      </tr>
                      
                      <tr id="row">
                        <td align="right"><asp:Label ID="lblCity" runat="server" Text=" City : "/> </td>
                        <td>
                            <asp:TextBox ID="txtCity" runat="Server" Width="135px" MaxLength="100"/></td>
                        <td align="right"></td>
                        <td></td>
                  </tr>
                </table>
                </td>
                </tr>
             </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table id="tblBottomBar" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="right">
                            &nbsp;<asp:Button ID="btnSave" runat="server" CssClass="btnstyle" Height="22px" OnClick="btnSave_Click"
                                Text="Save" />
                            <asp:Button ID="btnCancle" runat="server" CausesValidation="False" CssClass="btnstyle"
                                Height="22px" OnClick="btnCancel_Click" Text="Cancel" UseSubmitBehavior="False" /></td>
                    </tr>
                </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
                </tr>
              </table>
              <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td>
                      <uc5:LinksBar ID="lnkBar" runat="server" HeaderText="Contact Persons" LinksList="Add Contact">
                      </uc5:LinksBar>
                  </td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><img src="../Images/pix.gif" alt="" width="1" height="3" /></td>
                </tr>
              </table>              
              <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tbldisplay">
                <tr>
                  <td width="100%">
                      <uc4:Grid ID="Grid1" runat="server" />
                  </td>
                </tr>
              </table>             
             </td>
          </tr>
        </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
        </tr>
      </table>      
      </td>
    </tr>
  </table>
   </asp:Content>

