using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class SearchEquipment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Search Equipment";
        if (!IsPostBack)
        {
            gridCurrent.Visible = true;
            gridCurrent.DeleteVisible = true;
            gridCurrent.EditVisible = true;
            gridCurrent.TableName = "eTn_Equipment";
            gridCurrent.Primarykey = "bint_EquipmentId";
            gridCurrent.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
            gridCurrent.ColumnsList = "bint_EquipmentId;nvar_EquipmentName;nvar_Type;" + CommonFunctions.StatusString("bit_Status", "Status") + ";nvar_EquipState;" + CommonFunctions.DateQueryString("date_Created", "Created");
            gridCurrent.VisibleColumnsList = "nvar_EquipmentName;nvar_Type;Status;nvar_EquipState;Created";
            gridCurrent.VisibleHeadersList = "Name;Type;Status;State;Created";
            gridCurrent.DependentTablesList = "eTn_Employee";
            gridCurrent.DependentTableKeysList = "bint_EquipmentId";
            gridCurrent.DeleteTablesList = "eTn_EquipInsurance;eTn_Equipment";
            gridCurrent.DeleteTablePKList = "bint_EquipmentId;bint_EquipmentId";
            gridCurrent.EditPage = "~/Manager/NewEquipment.aspx";
            gridCurrent.OrderByClause = "nvar_EquipmentName";
            gridCurrent.WhereClause = "bit_Status=" + Convert.ToString(ddlList.SelectedItem.Value).Trim();
            gridCurrent.BindGrid(0);
            
            barCurrent.PagesList = "NewEquipment.aspx";
        }        
    }

    protected void ddlList_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["GridPageIndex"] = 0;
        gridCurrent.WhereClause = "bit_Status=" + Convert.ToString(ddlList.SelectedItem.Value).Trim();
        gridCurrent.BindGrid(0);
        gridCurrent.Visible = true;        
        barCurrent.AssignPagesToLinks();
    }
}
