using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class LoadLogs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Load Log";
        if (!IsPostBack)
        {
            ViewState["PreviousPage"] = Request.UrlReferrer;//Saves the Previous page url in ViewState
            if (Request.QueryString.Count > 0)
            {
                ViewState["LoadID"] = Convert.ToInt64(Request.QueryString[0]);
                LoadDetails(Convert.ToInt64(Request.QueryString[0]));
                Session["TopHeaderText"] = "Load Log for # " + Convert.ToInt64(Request.QueryString[0]);
            }
        }

    }

    private void LoadDetails(long loadId)
    {
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadLogDetailsByLoadId", new object[] { loadId });
        if (ds.Tables.Count > 2)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                int iRailRowCount = 0;
                int iHotShipment = 0;
                dgLoadLogDetails.DataSource = ds.Tables[0];
                dgLoadLogDetails.DataBind();
                foreach (DataGridItem item in dgLoadLogDetails.Items)
                {
                    if (item.Cells[0].Text == "Insert")
                        item.Cells[0].Text = "New";


                    //SLine 2016 Enhancements
                    //To show the Rail Container value in the Log table                    
                    if (Convert.ToBoolean(ds.Tables[0].Rows[iRailRowCount]["Rail"]))
                    { item.Cells[34].Text = "Rail"; }
                    else
                    { item.Cells[34].Text = ""; }
                    iRailRowCount++;


                    //To show the Hot Shipments Container value in the Log table                    
                    if (Convert.ToBoolean(ds.Tables[0].Rows[iHotShipment]["Hot Shipment"]))
                    { item.Cells[35].Text = "Hot Shipment"; }
                    else
                    { item.Cells[35].Text = ""; }
                    iHotShipment++;
                    //SLine 2016 Enhancements


                }
                lblLoadLogsHeader.Text = "Load Details Log" ;
                FormatGrid(ds.Tables[0],"Load");
            }
            else
            {
                lblLoadLogsHeader.Text = "Load Details Log - No records Found";
            }
            if (ds.Tables[1].Rows.Count > 0)
            {
                dgTrackLoadDetails.DataSource = ds.Tables[1];
                dgTrackLoadDetails.DataBind();
                foreach (DataGridItem item in dgTrackLoadDetails.Items)
                {
                    if (item.Cells[0].Text == "Insert")
                        item.Cells[0].Text = "Update";
                }
                lblTrackLoadHeader.Text = "Load Status Log";
            }
            else
            {
                lblTrackLoadHeader.Text = "Load Status Log - No records Found";
            }
            if (ds.Tables[2].Rows.Count > 0)
            {
                dgLoadViewDetails.DataSource = ds.Tables[2];
                dgLoadViewDetails.DataBind();
                lblLoadViewHeader.Text = "Load View Log";
            }
            else
            {
                lblLoadViewHeader.Text = "Load View Log - No records Found";
            }
            if (ds.Tables[3].Rows.Count > 0)
            {                
                dgOriginLogDetails.DataSource = ds.Tables[3];                
                dgOriginLogDetails.DataBind();
                foreach (DataGridItem item in dgOriginLogDetails.Items)
                {
                    if (item.Cells[3].Text == "Insert")
                        item.Cells[3].Text = "New";
                }
                lblOriginLogsHeader.Text = "Load Origin Log";
                FormatGrid(ds.Tables[3], "Origin");
            }
            else
            {
                lblOriginLogsHeader.Text = "Load Origin Log - No records Found";
            }
            if (ds.Tables[4].Rows.Count > 0)
            {
                dgDestinationLogDetails.DataSource = ds.Tables[4];
                dgDestinationLogDetails.DataBind();
                foreach (DataGridItem item in dgDestinationLogDetails.Items)
                {
                    if (item.Cells[3].Text == "Insert")
                        item.Cells[3].Text = "New";
                }
                lblDestinationLogsHeader.Text = "Load Destination Log";
                FormatGrid(ds.Tables[4], "Destination");
            }
            else
            {
                lblDestinationLogsHeader.Text = "Load Destination Log - No records Found";
            }
            if (ds.Tables[5].Rows.Count > 0)
            {
                dgEdiTrackLoadDetails.DataSource = ds.Tables[5];
                dgEdiTrackLoadDetails.DataBind();
                lblEdTrackLoad.Text = "EDI Load Status Log";
            }
            else
            {
                lblEdTrackLoad.Text = "EDI Load Status Log - No records Found";
            }

        }
    }   

    private void FormatGrid(DataTable dataTable,string name)
    {
         int rowCount=dataTable.Rows.Count;
        int colCount = dataTable.Columns.Count;
        if (name == "Load")
        {
            for (int i = (rowCount - 2); i >= 0; i--)
            {
                for (int j = 6; j < colCount; j++)
                {
                    if (dataTable.Rows[i][j].ToString() != dataTable.Rows[(i + 1)][j].ToString())
                    {
                        dgLoadLogDetails.Items[i].Cells[j].CssClass = "redrow";
                    }

                    //SLine 2016 Enhancements
                    //if (dataTable.Rows[i][j].ToString() == "Rail")
                    //{
                    //    dgLoadLogDetails.Items[i].Cells[j].CssClass = "redrow";
                    //}
                    //else { dgLoadLogDetails.Items[i].Cells[j].CssClass = "redrow"; }

                    //SLine 2016 Enhancements
                }
            }
        }
        else if (name == "Origin")
        {
            for (int i = (rowCount - 2); i >= 0; i--)
            {
                if (dataTable.Rows[i][0].ToString() == dataTable.Rows[(i + 1)][0].ToString())
                {
                    for (int j = 5; j < colCount; j++)
                    {
                        if (dataTable.Rows[i][j].ToString() != dataTable.Rows[(i + 1)][j].ToString())
                        {
                            dgOriginLogDetails.Items[i].Cells[j].CssClass = "redrow";
                        }
                    }
                }
            }
        }
        else if (name == "Destination")
        {
            for (int i = (rowCount - 2); i >= 0; i--)
            {
                if (dataTable.Rows[i][0].ToString() == dataTable.Rows[(i + 1)][0].ToString())
                {
                    for (int j = 5; j < colCount; j++)
                    {
                        if (dataTable.Rows[i][j].ToString() != dataTable.Rows[(i + 1)][j].ToString())
                        {
                            dgDestinationLogDetails.Items[i].Cells[j].CssClass = "redrow";
                        }
                    }
                }
            }
        }
    }

    private void RedirectToBackPage()
    {
        if (ViewState["PreviousPage"] != null)	//Check if the ViewState contains Previous page URL
        {
            Response.Redirect(ViewState["PreviousPage"].ToString());//Redirect to Previous page by retrieving the PreviousPage Url from ViewState.
        }
        else
        {
            Response.Redirect("DashBoard.aspx");
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        // CheckBackPage();
        //Response.Redirect("~/Manager/DashBoard.aspx");
        RedirectToBackPage();

    }
}
