using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class NewEmployee : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //lblErrEquip.Visible = false;
            txtdateofbirth.DisplayError = false;
            txtdatehired.DisplayError = false;
            lblErMess.Visible = false;
            // add tb value as 0
            //Session["TopHeaderText"] = "New / Edit Employee";
            if (!IsPostBack)
            {
                pnlDriveDetails.Visible = false;
                pnlLicenseDetails.Visible = false;
                pnlInsuranceDetails.Visible = false;
                txteqpname.Text = "0";
                //lnkAattchEque.Visible = false;
                //ddlEquipments.Visible = false;
                FillRoles();
                FillTeams();
                //SLine 2017 Enhancement for Office Location
                FillOfficeLocation();
                if (Request.QueryString.Count > 0)
                {
                    if (Session["BackPage4"] == null)
                    {
                        string[] Params = Convert.ToString(Request.QueryString[0]).Split('^');
                        //the below is user login id
                        ViewState["RoleId"] = Convert.ToInt64(Params[1].ToString());
                        ViewState["EmpployeeID"] = Convert.ToInt64(Params[0]);
                        FillDetails(Convert.ToInt64(ViewState["EmpployeeID"]));                       
                        Params = null;
                    }
                    else
                    {
                        ddlrole.SelectedIndex = ddlrole.Items.IndexOf(ddlrole.Items.FindByText("Driver"));
                        //lnkAattchEque.Visible = true;
                        //FillEquiments();
                    }
                    ViewState["Mode"] = "Edit";
                    Session["TopHeaderText"] = "Edit Employee";
                    btnAgreements.Visible = true;
                    btnAgreements.PostBackUrl = "UploadAgreements.aspx?" + ViewState["EmpployeeID"].ToString() + "^" + ViewState["RoleId"];
                }
                else
                {
                    ViewState["Mode"] = "New";
                    Session["TopHeaderText"] = "New Employee";
                    if (Session["BackPage4"] != null)
                    {
                        ddlrole.SelectedIndex = ddlrole.Items.IndexOf(ddlrole.Items.FindByText("Driver"));
                        ddlrole_SelectedIndexChanged(null, null);
                    }
                }
                //ddlDriverStatus.Attributes.Add("onchange", "javascript:VisibleDateControl('"+dtpWorkPermit.ClientID+"','"+ddlDriverStatus.ClientID+"')");
                // ddlrole.Attributes.Add("onchange", "javascript:VisibleDateControl('" + pnlDriveDetails.ClientID + "','" + ddlrole.ClientID + "')");
            }
        }
        catch
        {
        }
    }

    private void FillDetails(long ID)
    {
        object[] param = new object[] { ID };
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "Get_Employee_Details", param);
        if (ds.Tables.Count > 0)
        {
            txtfirstname.Text = Convert.ToString(ds.Tables[0].Rows[0][1]).Trim();
            txtmiddlename.Text = Convert.ToString(ds.Tables[0].Rows[0][2]).Trim();
            txtlastname.Text = Convert.ToString(ds.Tables[0].Rows[0][3]).Trim();
            txtnickname.Text = Convert.ToString(ds.Tables[0].Rows[0][4]).Trim();
            ddlrole.SelectedIndex = Convert.ToInt32(ddlrole.Items.IndexOf(ddlrole.Items.FindByValue(Convert.ToString(ds.Tables[0].Rows[0][5]))));
            ddlteam.SelectedIndex = Convert.ToInt32(ddlteam.Items.IndexOf(ddlteam.Items.FindByValue(Convert.ToString(ds.Tables[0].Rows[0]["bint_TeamId"]))));

            //SLine 2016 ---- Added a new Item for Dispatcher Special Permission
            if (ddlrole.SelectedValue == "4")
            {
                trDispatcherPermission.Style.Clear();
                trDispatcherPermission.Style.Add(HtmlTextWriterStyle.Display, "");

                if (Convert.ToBoolean(ds.Tables[0].Rows[0]["bit_PermissionFlag"]))
                    chkDispatcherPermission.Checked = true;
                else
                    chkDispatcherPermission.Checked = false;
            }
            else
            {
                //chkDispatcherPermission.Visible = false;
                chkDispatcherPermission.Checked = false;
                trDispatcherPermission.Style.Clear();
                trDispatcherPermission.Style.Add(HtmlTextWriterStyle.Display, "none");
            }
            //SLine 2016 ---- Added a new Item for Dispatcher Special Permission

            if (ddlrole.SelectedItem.Text == "Driver")
            {
                //lnkAattchEque.Visible = true;
                //FillEquiments();
                //ddlEquipments.SelectedIndex = ddlEquipments.Items.IndexOf(ddlEquipments.Items.FindByValue(Convert.ToString(ds.Tables[0].Rows[0]["bint_EquipmentId"])));
                txteqpname.Text = Convert.ToString(ds.Tables[0].Rows[0]["bint_EquipmentId"]);
                FillEquipmentDetails(Convert.ToInt64(txteqpname.Text));
                FillEqupmentInsuranceDetails(Convert.ToInt64(txteqpname.Text));
                pnlDriveDetails.Visible = true;
                pnlLicenseDetails.Visible = true;
                pnlInsuranceDetails.Visible = true;
                lblWorkPermit.Visible = false;
            }
            else
            {
                //lnkAattchEque.Visible = false;
                lblWorkPermit.Visible = false;
                pnlDriveDetails.Visible = false;
                pnlLicenseDetails.Visible = false;
                pnlInsuranceDetails.Visible = false;

            }
            lblEmpId.Text = ds.Tables[0].Rows[0][0].ToString();
            ddlstatus.SelectedIndex = Convert.ToString(ds.Tables[0].Rows[0][6]).Trim() == "False" ? 1 : 0;
            // ddlstatus.SelectedIndex = ddlstatus.Items.IndexOf(ddlstatus.Items.FindByText(Convert.ToString(ds.Tables[0].Rows[0][6]).Trim()));
            ddltype.SelectedIndex = ddltype.Items.IndexOf(ddltype.Items.FindByText(Convert.ToString(ds.Tables[0].Rows[0][7]).Trim()));
            txtdateofbirth.Date = Convert.ToString(ds.Tables[0].Rows[0][8]);
            txtdatehired.Date = Convert.ToString(ds.Tables[0].Rows[0][9]);
            txttaxid.Text = Convert.ToString(ds.Tables[0].Rows[0][10]).Trim();
            txtuserid.Text = Convert.ToString(ds.Tables[0].Rows[0][11]).Trim();
            ViewState["tempUserId"] = txtuserid.Text.Trim();
            txtpwd.Text = Convert.ToString(ds.Tables[0].Rows[0][12]).Trim();
            chkpayments.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0][13]);
            chkdocuments.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0][14]);
            txtemail.Text = Convert.ToString(ds.Tables[0].Rows[0][15]).Trim();
            txtworkph.Text = Convert.ToString(ds.Tables[0].Rows[0][16]);
            txtextn.Text = Convert.ToString(ds.Tables[0].Rows[0][17]).Trim();
            txtmobile.Text = Convert.ToString(ds.Tables[0].Rows[0][18]);
            txthomeph.Text = Convert.ToString(ds.Tables[0].Rows[0][19]);
            txtfax.Text = Convert.ToString(ds.Tables[0].Rows[0][20]);
            txtnotes.Text = Convert.ToString(ds.Tables[0].Rows[0][21]).Trim();
            txtstreet.Text = Convert.ToString(ds.Tables[0].Rows[0][22]).Trim();
            txtappt.Text = Convert.ToString(ds.Tables[0].Rows[0][23]).Trim();
            txtcity.Text = Convert.ToString(ds.Tables[0].Rows[0][24]).Trim();
            txtstate.Text = Convert.ToString(ds.Tables[0].Rows[0][25]).Trim();
            txtzipcode.Text = Convert.ToString(ds.Tables[0].Rows[0][26]).Trim();
            imgphoto.ImageUrl = "../Photos/" + Convert.ToString(ds.Tables[0].Rows[0][27]).Trim();
            imgphoto.Width = 100; imgphoto.Height = 100;
            ddlDriverStatus.SelectedIndex = ddlDriverStatus.Items.IndexOf(ddlDriverStatus.Items.FindByText(Convert.ToString(ds.Tables[0].Rows[0][29])));
            txtLicense.Text = Convert.ToString(ds.Tables[0].Rows[0][31]);
            //SLine 2017 Enhancement for Office Location
            PopulateOfficeLocation();
            DateTime Expire;
            if (ds.Tables[0].Rows[0][30] != null && Convert.ToString(ds.Tables[0].Rows[0][30]).Trim().Length > 0)
            {
                Expire = Convert.ToDateTime(ds.Tables[0].Rows[0][30]);
                dtpWorkPermit.Date = Expire.ToString();
            }//SLINE
            if (ds.Tables[0].Rows[0][32] != null && Convert.ToString(ds.Tables[0].Rows[0][32]).Trim().Length > 0)
            {
                Expire = Convert.ToDateTime(ds.Tables[0].Rows[0][32]);
                dtpLicenseExpiry.Date = Expire.ToString();
            }
            if (ds.Tables[0].Rows[0][33] != null && Convert.ToString(ds.Tables[0].Rows[0][33]).Trim().Length > 0)
            {
                Expire = Convert.ToDateTime(ds.Tables[0].Rows[0][33]);
                dtpMedicalCard.Date = Expire.ToString();
            }
            //Sline 
            if (ddlDriverStatus.SelectedItem.Text == "Work Permit")
            {
                dtpWorkPermit.Visible = true;
                lblWorkPermit.Visible = true;
            }
            // fill the hazmat details for the Employee.
            if (ds.Tables[0].Rows[0][34] != null && Convert.ToString(ds.Tables[0].Rows[0][34]).Trim().Length > 0)
            {
                Expire = Convert.ToDateTime(ds.Tables[0].Rows[0][34]);
                dtpHazmatCertigiedExpaires.Date = Expire.ToString();
            }//dtpHazmatCertigiedExpaires
            ChkHaxmatCertified.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0][35]);
            if (ChkHaxmatCertified.Checked)
            {
                dtpHazmatCertigiedExpaires.Visible = true;
                dtpHazmatCertigiedExpaires.IsRequired = true;
                spanHazmatexpirydate.Visible = true;
            }
            else
            {
                dtpHazmatCertigiedExpaires.Visible = false;
                dtpHazmatCertigiedExpaires.IsRequired = false;
                spanHazmatexpirydate.Visible = false;
            }
        }
    }
    //SLine 2017 Enhancement for Office Location Starts
    private void FillOfficeLocation()
    {
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "Get_Office_Locations");
        if (ds.Tables.Count > 0)
        {
            rptLocation.DataSource = ds.Tables[0];
            rptLocation.DataBind();
        }
    }
    private void PopulateOfficeLocation()
    {
        long userLoginId = (long)ViewState["RoleId"];
        object[] param = new object[] { userLoginId };
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "Get_Office_Locations_ForaUser", param);
        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < rptLocation.Items.Count; i++)
            {
                HiddenField hdn = (HiddenField)rptLocation.Items[i].FindControl("hdnLocation");
                DataRow[] result = ds.Tables[0].Select("bint_OfficeLocationId=" + hdn.Value);
                if (result.Length > 0)
                {
                    CheckBox chk = (CheckBox)rptLocation.Items[i].FindControl("cbLocation");
                    chk.Checked = true;
                }
            }
            if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
            {
                string defaultLocation = Convert.ToString(ds.Tables[1].Rows[0][0]);
                string jscript = "setCheckedValue(document.getElementsByName('rbDefaultLocation'), " + defaultLocation + ")";
                ScriptManager.RegisterStartupScript(this, typeof(Page), "UpdateDefaultLocation", jscript, true);
            }
        }

    }
    private void CheckSavedDefaultLocation(string defaultLocation)
    {
        //string defaultLocation = ViewState["defaultLocation"] as string;
        if (!string.IsNullOrEmpty(defaultLocation))
        {
            string jscript = "setCheckedValue(document.getElementsByName('rbDefaultLocation'), " + defaultLocation + ")";
            ScriptManager.RegisterStartupScript(this, typeof(Page), "UpdateDefaultLocation", jscript, true);
        }
    }
    //SLine 2017 Enhancement for Office Location Ends
    private void FillRoles()
    {
        //DataTable dtRoles = DBClass.returnDataTable("select [bint_RoleId],[nvar_RoleName] from [eTn_Role] order by [nvar_RoleName]");
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetRoleNames");
        if (ds.Tables.Count > 0)
        {
            ddlrole.DataSource = ds.Tables[0];
            ddlrole.DataTextField = "nvar_RoleName";
            ddlrole.DataValueField = "bint_RoleId";
            ddlrole.DataBind();
            ddlrole.Items.Insert(0, new ListItem("Select", "0"));
        }
        if (ddlrole.Items.Count > 0)
        {
            if (ddlrole.Items.Contains(ddlrole.Items.FindByText("Customer")))
                ddlrole.Items.Remove(ddlrole.Items.FindByText("Customer"));
            if (ddlrole.Items.Contains(ddlrole.Items.FindByText("Carrier")))
                ddlrole.Items.Remove(ddlrole.Items.FindByText("Carrier"));
            //ddlrole.Items.Insert(0,new ListItem("--Select--","0"));
        }
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    //Sline 2021 updating Team DDl
    private void FillTeams()
    {
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetTeamNames");
        //SLine 2017 Enhancement for Office Location Ends
        if (ds.Tables.Count > 0)
        {
            ddlteam.DataSource = ds.Tables[0];
            ddlteam.DataTextField = "nvar_TeamName";
            ddlteam.DataValueField = "bint_TeamId";
            ddlteam.DataBind();
        }
        ddlteam.Items.Insert(0, new ListItem("Select", "0"));
        ddlteam.SelectedIndex = 0;
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    private void FillLoadTypes()
    {
        //DataTable dtTypes= DBClass.returnDataTable("select [bint_LoadTypeId],[nvar_LoadTypeDesc] from [eTn_LoadType]");
        //ddltype.DataSource = dtTypes;
        //ddltype.DataTextField = "nvar_LoadTypeDesc";
        //ddltype.DataValueField = "bint_LoadTypeId";
        //ddltype.DataBind();
        ////ddlrole.Items.Insert(0,new ListItem("--Select--","0"));
        //if (dtTypes != null)
        //{ dtTypes.Dispose(); dtTypes = null; }
    }
    //private void FillEquiments()
    //{
    //    ddlEquipments.Visible = true;
    //    //ddlEquipments.DataSource = DBClass.returnDataTable("select nvar_EquipmentName,bint_EquipmentId from eTn_Equipment order by [nvar_EquipmentName]");
    //    DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetEquipmentNames");
    //    if (ds.Tables.Count > 0)
    //    {
    //        ddlEquipments.DataSource = ds.Tables[0];
    //        ddlEquipments.DataTextField = "nvar_EquipmentName";
    //        ddlEquipments.DataValueField = "bint_EquipmentId";
    //        ddlEquipments.DataBind();
    //    }
    //    ddlEquipments.Items.Insert(0, new ListItem("--Select Equipment--", "0"));
    //}
    private void FillEquipmentDetails(long equipmentId)
    {
        
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetEquipmentDetails", new object[] { equipmentId });
        if (ds.Tables.Count > 0)
        {
            txtlicensedriver.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_License"]).Trim();
            txtvin.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_VIN"]).Trim();
            txtplate.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_Plate"]).Trim();
            
        }
        if (ds != null) { ds.Dispose(); ds = null; }

    }
    private void FillEqupmentInsuranceDetails(long equipmentId)
    {
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "sp_GetEquipmentInsuranceDetails", new object[] { equipmentId });
        if (ds.Tables[0].Rows.Count > 0)
        {
            txtcliability.Text = Convert.ToString(ds.Tables[0].Rows[0][0]);
            txtpliability.Text = Convert.ToString(ds.Tables[0].Rows[0][1]);
            txtlliability.Text = Convert.ToString(ds.Tables[0].Rows[0][2]);
            txtbliability.Text = Convert.ToString(ds.Tables[0].Rows[0][3]);
            txtphliabity.Text = Convert.ToString(ds.Tables[0].Rows[0][4]);
            txtfliability.Text = Convert.ToString(ds.Tables[0].Rows[0][5]);
            txtemailliabi.Text = Convert.ToString(ds.Tables[0].Rows[0][6]);
            txteliaility.Date = Convert.ToString(ds.Tables[0].Rows[0][7]);
            txtccargo.Text = Convert.ToString(ds.Tables[0].Rows[0][8]);
            txtpcargo.Text = Convert.ToString(ds.Tables[0].Rows[0][9]);
            txtlcargo.Text = Convert.ToString(ds.Tables[0].Rows[0][10]);
            txtbcargo.Text = Convert.ToString(ds.Tables[0].Rows[0][11]);
            txtphcargo.Text = Convert.ToString(ds.Tables[0].Rows[0][12]);
            txtfcargo.Text = Convert.ToString(ds.Tables[0].Rows[0][13]);
            txtemailcargo.Text = Convert.ToString(ds.Tables[0].Rows[0][14]);
            txtecargo.Date = Convert.ToString(ds.Tables[0].Rows[0][15]);
            txtcphysical.Text = Convert.ToString(ds.Tables[0].Rows[0][16]);
            txtpphysical.Text = Convert.ToString(ds.Tables[0].Rows[0][17]);
            txtlphysical.Text = Convert.ToString(ds.Tables[0].Rows[0][18]);
            txtbphysical.Text = Convert.ToString(ds.Tables[0].Rows[0][19]);
            txtphphysical.Text = Convert.ToString(ds.Tables[0].Rows[0][20]);
            txtfphysical.Text = Convert.ToString(ds.Tables[0].Rows[0][21]);
            txtemailphy.Text = Convert.ToString(ds.Tables[0].Rows[0][22]);
            txtephysical.Date = Convert.ToString(ds.Tables[0].Rows[0][23]);
        }
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    protected void saveInsurance()
    {
        if (txteqpname.Text != null)
        {
            txteliaility.DisplayError = false;
            txtecargo.DisplayError = false;
            txtephysical.DisplayError = false;
            if (!txteliaility.IsValidDate)
                return;
            if (!txtecargo.IsValidDate)
                return;
            if (!txtephysical.IsValidDate)
                return;

            string strAction = "";
            if (string.Compare(Convert.ToString(ViewState["Mode"]), "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                strAction = "I";
            else if (string.Compare(Convert.ToString(ViewState["Mode"]), "Edit", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                strAction = "U";
            checkNulls();
            object[] objParams = new object[] { Convert.ToInt64(txteqpname.Text),
                txtcliability.Text.Trim(),txtpliability.Text.Trim(),txtlliability.Text.Trim(),txtbliability.Text.Trim(),txtphliabity.Text.Trim(),txtfliability.Text.Trim(),txtemailliabi.Text.Trim(),CommonFunctions.CheckDateTimeNull(txteliaility.Date),
            txtccargo.Text,txtpcargo.Text.Trim().Trim(),txtlcargo.Text,txtbcargo.Text.Trim(),txtphcargo.Text.Trim(),txtfcargo.Text.Trim(),txtemailcargo.Text.Trim(),CommonFunctions.CheckDateTimeNull(txtecargo.Date),
            txtcphysical.Text.Trim(),txtpphysical.Text.Trim(),txtlphysical.Text.Trim(),txtbphysical.Text.Trim(),txtphphysical.Text.Trim(),txtfphysical.Text.Trim(),txtemailphy.Text.Trim(),CommonFunctions.CheckDateTimeNull(txtephysical.Date),strAction};

            if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_insert_update_EquipInsurance", objParams) > 0)
            {
                objParams = null;
                //Response.Redirect("~/Manager/NewEquipment.aspx?" + Convert.ToString(ViewState["ID"]));
            }
            objParams = null;
        }
    }
    private void checkNulls()
    {
        if (txtlliability.Text.Trim().Length == 0)
            txtlliability.Text = "0";
        if (txtlphysical.Text.Trim().Length == 0)
            txtlphysical.Text = "0";
        if (txtlcargo.Text.Trim().Length == 0)
            txtlcargo.Text = "0";
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        lblErMess.Visible = false;
        //lblErrEquip.Visible = false;//remove Equipment
        lblErLoc.Visible = false;
        if (ViewState["Mode"] != null)
        {
            if (txtextn.Text.Trim().Length == 0)
                txtextn.Text = "0";
            if (string.Compare(Convert.ToString(ViewState["tempUserId"]), txtuserid.Text.Trim(), true, System.Globalization.CultureInfo.CurrentCulture) != 0)
            {
                string result = DBClass.executeScalar("select count(nvar_UserId) from eTn_UserLogin where lower(nvar_UserId) ='" + txtuserid.Text.Trim().ToLower() + "'");
                if (result.Trim().Length > 0)
                {
                    if (Convert.ToInt32(result) > 0)
                    {
                        lblErMess.Visible = true;
                        return;
                    }
                }
            }
            if (!txtdateofbirth.IsValidDate)
                return;
            if (!txtdatehired.IsValidDate)
                return;
            //remove Equipment
            // if (ddlEquipments.SelectedIndex == 0 && string.Compare(ddlrole.SelectedItem.Text.Trim(),"Driver",true,System.Globalization.CultureInfo.CurrentCulture)==0 )
            //{
            //    lblErrEquip.Visible = true;
            //    return;
            //}         

            if (string.Compare(Convert.ToString(ViewState["Mode"]), "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                ViewState["RoleId"] = ddlrole.SelectedValue;
            }

            //SLine 2017 Enhancement for Office Location Starts
            string locations = "";
            string defaultLocation = "";
            for (int i = 0; i < rptLocation.Items.Count; i++)
            {
                CheckBox chk = (CheckBox)rptLocation.Items[i].FindControl("cbLocation");
                if (chk.Checked == true)
                {
                    HiddenField hdn = (HiddenField)rptLocation.Items[i].FindControl("hdnLocation");
                    if (locations == "")
                    {
                        locations = hdn.Value;
                    }
                    else
                    {
                        locations += "," + hdn.Value;
                    }
                }
            }
            defaultLocation = Request.Form["rbDefaultLocation"];
            string jscript = "setCheckedValue(document.getElementsByName('rbDefaultLocation'), " + defaultLocation + ")";
            ScriptManager.RegisterStartupScript(this, typeof(Page), "UpdateDefaultLocation", jscript, true);
            if (locations == "")
            {
                lblErLoc.Text = "Please select a location.";
                lblErLoc.Visible = true;
                return;
            }

            if (defaultLocation == "" || defaultLocation == null)
            {
                lblErLoc.Text = "Please select a default location.";
                lblErLoc.Visible = true;
                return;
            }
            if (!locations.Contains(defaultLocation))
            {
                lblErLoc.Text = "Please select the location for the selected default location.";
                lblErLoc.Visible = true;
                return;
            }
            //SLine 2017 Enhancement for Office Location Ends
            // Checking equipment licence is there or not
            // txtlicense.Text.Trim(),txtvin.Text.Trim(),txtplate.Text.Trim()
            // (ddlEquipments.Visible ? Convert.ToInt64(ddlEquipments.SelectedItem.Value) : 0)
            object[] objEqpParams = new object[] { txtfirstname.Text.Trim(),"",0,1,"",0,0, txtlicensedriver.Text.Trim(),txtvin.Text.Trim(),txtplate.Text.Trim(),txteqpname.Text};
            System.Data.SqlClient.SqlParameter[] objsqleqpparams = null;
            if (txteqpname.Text.Contains("0"))
            {
                objsqleqpparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Add_Equipment", objEqpParams);
                if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Add_Equipment", objsqleqpparams) > 0)
                {
                    txteqpname.Text = Convert.ToString(objsqleqpparams[objsqleqpparams.Length - 1].Value);
                    saveInsurance();
                }
            }
            else
            {
                objsqleqpparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Update_Equipment", objEqpParams);
                if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Update_Equipment", objsqleqpparams) > 0)
                {
                    saveInsurance();
                }
            }
            // add new collumn for Hazmat.
            //SLine 2017 Enhancement for Office Location added new two parameters
            object[] objParams = new object[]{txtnickname.Text.Trim(),Convert.ToInt32(ddlstatus.SelectedValue),ddltype.SelectedItem.Text.Trim(),CommonFunctions.CheckDateTimeNull(txtdateofbirth.Date),CommonFunctions.CheckDateTimeNull(txtdatehired.Date),
                                    txttaxid.Text.Trim(),txtemail.Text.Trim(),CommonFunctions.FormatPhone(txtworkph.Text),CommonFunctions.FormatPhone(txtextn.Text),CommonFunctions.FormatPhone(txtmobile.Text),CommonFunctions.FormatPhone(txthomeph.Text),
                                    CommonFunctions.FormatPhone(txtfax.Text),txtnotes.Text.Trim(),txtstreet.Text.Trim(),txtappt.Text.Trim(),txtcity.Text.Trim(),txtstate.Text,txtzipcode.Text.Trim(),
                                    txtfirstname.Text,txtlastname.Text,txtmiddlename.Text,txtuserid.Text,txtpwd.Text,txtemail.Text,Convert.ToInt64(ddlrole.SelectedItem.Value),(chkpayments.Checked?1:0),(chkpayments.Checked?1:0),
                                   txteqpname.Text.Trim(),Convert.ToInt64(ViewState["EmpployeeID"]),Convert.ToInt64(ViewState["RoleId"]),(ddlDriverStatus.SelectedIndex>0?ddlDriverStatus.SelectedItem.Value:""),CommonFunctions.CheckDateTimeNull(dtpWorkPermit.Date),txtLicense.Text.Trim(),CommonFunctions.CheckDateTimeNull(dtpLicenseExpiry.Date),CommonFunctions.CheckDateTimeNull(dtpMedicalCard.Date),CommonFunctions.CheckDateTimeNull(dtpHazmatCertigiedExpaires.Date),(ChkHaxmatCertified.Checked?1:0), (chkDispatcherPermission.Checked?1:0), locations, defaultLocation , ddlteam.SelectedItem.Value};


            if (ViewState["EmpployeeID"] != null)
            {
                if (string.Compare(Convert.ToString(ViewState["Mode"]), "Edit", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Update_Employee", SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Update_Employee", objParams)) > 0)
                    {
                        if (ViewState["TempPhotoPath"] != null)
                        {
                            string Ext = "";
                            string[] strExt = Convert.ToString(ViewState["TempPhotoPath"]).Trim().Split('.');
                            string[] FilesList = System.IO.Directory.GetFiles(Server.MapPath(ConfigurationSettings.AppSettings["PhotosPath"]));
                            if (FilesList.Length > 0)
                            {
                                string[] FileExt = null, TFile = null;
                                for (int i = 0; i < FilesList.Length; i++)
                                {
                                    FileExt = FilesList[i].Split('.');
                                    if (FileExt.Length > 0)
                                    {
                                        TFile = FileExt[0].Split('\\');
                                        if (string.Compare(Convert.ToString(ViewState["EmpployeeID"]), TFile[TFile.Length - 1], true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                                        {
                                            Ext = FileExt[FileExt.Length - 1]; break;
                                        }
                                        TFile = null;
                                    }
                                }
                                FileExt = null;
                            }
                            FilesList = null;
                            if (System.IO.File.Exists(Server.MapPath(ConfigurationSettings.AppSettings["PhotosPath"]) + "\\" + Convert.ToString(ViewState["EmpployeeID"]) + "." + Ext))
                                System.IO.File.Delete(Server.MapPath(ConfigurationSettings.AppSettings["PhotosPath"]) + "\\" + Convert.ToString(ViewState["EmpployeeID"]) + "." + Ext);
                            if (System.IO.File.Exists(Convert.ToString(ViewState["TempPhotoPath"])))
                            {
                                System.IO.File.Move(Convert.ToString(ViewState["TempPhotoPath"]), Server.MapPath(ConfigurationSettings.AppSettings["PhotosPath"]) + "\\" + Convert.ToString(ViewState["EmpployeeID"]) + "." + strExt[strExt.Length - 1]);
                            }
                            object[] objtParams = new object[] { Convert.ToString(ViewState["EmpployeeID"]), Convert.ToString(ViewState["EmpployeeID"]) + "." + strExt[strExt.Length - 1] };
                            SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_Update_Employee_PhotoPath", objtParams);
                            objtParams = null;
                            if (System.IO.File.Exists(Convert.ToString(ViewState["TempPhotoPath"])))
                                System.IO.File.Delete(Convert.ToString(ViewState["TempPhotoPath"]));
                            strExt = null;
                        }
                        CheckBackPage();
                        btnAgreements.Visible = true;
                        btnAgreements.PostBackUrl = "UploadAgreements.aspx?" + ViewState["EmpployeeID"].ToString() + "^" + ViewState["RoleId"];
                        string ddlvalue = string.Empty;
                        if (Session["ddlListValueselected"] != null)
                        {
                            ddlvalue = Session["ddlListValueselected"].ToString();
                            Response.Redirect("~/Manager/EmployeeAccount.aspx?DDlVal=" + ddlvalue);
                        }
                        Response.Redirect("~/Manager/EmployeeAccount.aspx");
                    }
                }
            }
            else if (string.Compare(Convert.ToString(ViewState["Mode"]), "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                System.Data.SqlClient.SqlParameter[] objsqlparams = null;
                objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Add_Employee", objParams);
                if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Add_Employee", objsqlparams) > 0)
                {
                    // IMPORTANT NOTE:
                    // Whenever a new control value or anything is added to the Stored procedure or
                    //below params List, Parameter values, the Logic is to increase the Value Substracting from IT to get 
                    //Correct "EmpID"

                    // added collumn for hazmat charges
                    //SLine 2016 Enhancements. Added New Permission to the EMployee checkbox to the objParams 
                    //SLine 2017 Enhancement for Office Location added two new parameters
                    ViewState["EmpployeeID"] = Convert.ToInt64(objsqlparams[objsqlparams.Length - 13].Value);
                    if (ViewState["TempPhotoPath"] != null)
                    {
                        string[] strExt = Convert.ToString(ViewState["TempPhotoPath"]).Trim().Split('.');
                        if (System.IO.File.Exists(Convert.ToString(ViewState["TempPhotoPath"])))
                            System.IO.File.Move(Convert.ToString(ViewState["TempPhotoPath"]), Server.MapPath(ConfigurationSettings.AppSettings["PhotosPath"]) + "\\" + Convert.ToString(ViewState["EmpployeeID"]) + "." + strExt[strExt.Length - 1]);
                        object[] objtParams = new object[] { Convert.ToString(ViewState["EmpployeeID"]), Convert.ToString(ViewState["EmpployeeID"]) + "." + strExt[strExt.Length - 1] };
                        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_Update_Employee_PhotoPath", objtParams);
                        //fileUploadDocuments.SaveAs(Server.MapPath("~/Agreement") + "\\" + Convert.ToString(ViewState["EmpployeeID"]) + "_" + fileUploadDocuments.PostedFile.FileName);

                        objtParams = null;
                        if (System.IO.File.Exists(Convert.ToString(ViewState["TempPhotoPath"])))
                            System.IO.File.Delete(Convert.ToString(ViewState["TempPhotoPath"]));
                        strExt = null;
                    }
                    CheckBackPage();
                    string ddlvalue = string.Empty;
                    if (Session["ddlListValueselected"] != null)
                    {
                        ddlvalue = Session["ddlListValueselected"].ToString();
                        Response.Redirect("~/Manager/EmployeeAccount.aspx?DDlVal=" + ddlvalue);
                    }
                    Response.Redirect("~/Manager/EmployeeAccount.aspx");
                }
            }
            objParams = null;
            objEqpParams = null;
        }

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (ViewState["TempPhotoPath"] != null)
        {
            if (System.IO.File.Exists(Convert.ToString(ViewState["TempPhotoPath"])))
                System.IO.File.Delete(Convert.ToString(ViewState["TempPhotoPath"]));
        }
        CheckBackPage();
        string ddlvalue = string.Empty;
        if (Session["ddlListValueselected"] != null)
        {
            ddlvalue = Session["ddlListValueselected"].ToString();
            Response.Redirect("~/Manager/EmployeeAccount.aspx?DDlVal=" + ddlvalue);
        }
        Response.Redirect("~/Manager/EmployeeAccount.aspx");
    }
    protected void ddlrole_SelectedIndexChanged(object sender, EventArgs e)
    {
        //SLine 2017 Enhancement for Office Location Starts
        string dloc = Request.Form["rbDefaultLocation"];
        CheckSavedDefaultLocation(dloc);
        //SLine 2017 Enhancement for Office Location Ends
        if (ddlrole.SelectedItem.Text == "Driver")
        {
            //lnkAattchEque.Visible = true;
            pnlDriveDetails.Visible = true;
            pnlLicenseDetails.Visible = true;
            pnlInsuranceDetails.Visible = true;
            lblWorkPermit.Visible = false;

            //SLine 2016 Enhancements
            //chkDispatcherPermission.Visible = false;
            trDispatcherPermission.Style.Clear();
            trDispatcherPermission.Style.Add(HtmlTextWriterStyle.Display, "none");
        }
        else if (ddlrole.SelectedItem.Text == "Dispatcher")
        {
            lblWorkPermit.Visible = false;
            pnlDriveDetails.Visible = false;
            pnlLicenseDetails.Visible = false;
            pnlInsuranceDetails.Visible = false;
            //lnkAattchEque.Visible = false;
            //ddlEquipments.Visible = false;
            //SLine 2016 Enhancements
            // chkDispatcherPermission.Visible = true;
            trDispatcherPermission.Style.Clear();
            trDispatcherPermission.Style.Add(HtmlTextWriterStyle.Display, "");
        }
        else
        {
            lblWorkPermit.Visible = false;
            pnlDriveDetails.Visible = false;
            pnlLicenseDetails.Visible = false;
            pnlInsuranceDetails.Visible = false;
            //lnkAattchEque.Visible = false;
            //ddlEquipments.Visible = false;

            //SLine 2016 Enhancements
            //chkDispatcherPermission.Visible = false;
            trDispatcherPermission.Style.Clear();
            trDispatcherPermission.Style.Add(HtmlTextWriterStyle.Display, "none");
        }
    }
    protected void lnkUploadPhoto_Click(object sender, EventArgs e)
    {
        pnlPhoto.Visible = true;
    }
    protected void btnAddPhoto_Click(object sender, EventArgs e)
    {
        if (pnlPhoto.Visible)
        {
            if (UpPhoto.FileName.Trim().Length > 0)
            {
                HttpPostedFile tfile = UpPhoto.PostedFile;
                if (tfile.FileName.EndsWith(".jpg", true, System.Globalization.CultureInfo.CurrentCulture) ||
                    tfile.FileName.EndsWith(".gif", true, System.Globalization.CultureInfo.CurrentCulture) ||
                    tfile.FileName.EndsWith(".bmp", true, System.Globalization.CultureInfo.CurrentCulture) ||
                    tfile.FileName.EndsWith(".png", true, System.Globalization.CultureInfo.CurrentCulture) ||
                    tfile.FileName.EndsWith(".jpeg", true, System.Globalization.CultureInfo.CurrentCulture))
                {
                    if (ViewState["TempPhotoPath"] != null)
                    {
                        if (System.IO.File.Exists(Convert.ToString(ViewState["TempPhotoPath"])))
                            System.IO.File.Delete(Convert.ToString(ViewState["TempPhotoPath"]));
                    }
                    if (!System.IO.Directory.Exists(Server.MapPath(ConfigurationSettings.AppSettings["TempFolderPath"])))
                        System.IO.Directory.CreateDirectory(Server.MapPath(ConfigurationSettings.AppSettings["TempFolderPath"]));
                    string[] strName = tfile.FileName.Split('\\');
                    tfile.SaveAs(Server.MapPath(ConfigurationSettings.AppSettings["TempFolderPath"]) + "\\" + strName[strName.Length - 1]);
                    imgphoto.ImageUrl = "../Temp/" + strName[strName.Length - 1];
                    imgphoto.Width = 100;
                    imgphoto.Height = 100;
                    imgphoto.Visible = true;
                    pnlPhoto.Visible = false;
                    ViewState["TempPhotoPath"] = Server.MapPath(ConfigurationSettings.AppSettings["TempFolderPath"]) + "\\" + strName[strName.Length - 1];
                    strName = null;
                }
                else
                {
                    Response.Write("Invalid photo path.");
                }
                tfile = null;
            }
        }
    }
    //protected void lnkAattchEque_Click(object sender, EventArgs e)
    //{
    //    FillEquiments();
    //}
    private void CheckBackPage()
    {
        if (Session["BackPage4"] != null)
        {
            if (Convert.ToString(Session["BackPage4"]).Trim().Length > 0)
            {
                string strTemp = Convert.ToString(Session["BackPage4"]).Trim();
                Session["BackPage4"] = null;
                Response.Redirect(strTemp);
            }
        }
    }
    protected void ddlDriverStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlDriverStatus.SelectedItem.Text == "Work Permit")
        {
            lblWorkPermit.Visible = true;
            dtpWorkPermit.Visible = true;
        }
        else
        {
            lblWorkPermit.Visible = false;
            dtpWorkPermit.Visible = false;
        }
    }
    // if hazmat checkbox click make hazmat expaire date requried.
    protected void ChkHaxmatCertified_Changed(object sender, EventArgs e)
    {
        //SLine 2017 Enhancement for Office Location Starts
        string dloc = Request.Form["rbDefaultLocation"];
        CheckSavedDefaultLocation(dloc);
        //SLine 2017 Enhancement for Office Location Ends
        if (ChkHaxmatCertified.Checked == true)
        {
            dtpHazmatCertigiedExpaires.Visible = true;
            dtpHazmatCertigiedExpaires.IsRequired = true;
            spanHazmatexpirydate.Visible = true;
        }
        else
        {
            dtpHazmatCertigiedExpaires.Visible = false;
            dtpHazmatCertigiedExpaires.IsRequired = false;
            spanHazmatexpirydate.Visible = false;
        }
    }
    //protected void btnUpload_Click(object sender, EventArgs e)
    //{
    //    if (fileUploadDocuments.PostedFile.ContentLength > 0)
    //    {            
    //        DataTable dtAggrements=null;
    //        if (ViewState["Aggrements"] != null)
    //            dtAggrements = (DataTable)ViewState["Aggrements"];
    //        else
    //            dtAggrements=getAggrements();
    //        DataRow drow = dtAggrements.NewRow();
    //        if (((DataTable)ViewState["Aggrements"]).Rows.Count > 0)
    //            drow[0] = ((DataTable)ViewState["Aggrements"]).Rows.Count+1;
    //        else
    //            drow[0] = 1;
    //        drow[1] = fileUploadDocuments.FileName;
    //        drow[2] = "N";
    //        drow[3] = "N";
    //        dtAggrements.Rows.Add(drow);
    //        ViewState["Aggrements"] = dtAggrements;
    //        BindAggrements(dtAggrements);
    //        if (dtAggrements != null) { dtAggrements.Dispose(); dtAggrements = null; }
    //    }
    //    else
    //        Response.Write("<script>alert('Please select document.')</script>");
    //}
    //private void BindAggrements(DataTable dt)
    //{
    //    DataView dv = new DataView(dt, "Type='U' or Type='N'", "bint_AgreementId ASC", DataViewRowState.CurrentRows);
    //    grdAggrements.DataSource = dv;
    //    grdAggrements.DataBind();
    //}
    //protected void OnDelete(object sender, EventArgs e)
    //{
    //    ImageButton imgBtn = (ImageButton)sender;
    //    if (imgBtn != null)
    //    {
    //        GridViewRow gridrow = (GridViewRow)imgBtn.Parent.Parent;
    //        long Id=Convert.ToInt64(grdAggrements.DataKeys[gridrow.RowIndex].Value);
    //        foreach (DataRow drow in ((DataTable)ViewState["Aggrements"]).Select("bint_AgreementId=" + Id))
    //        {
    //            if (drow[2].ToString() == "N" || drow[2].ToString()=="U")
    //            {
    //                drow[3] = "D";
    //                drow.AcceptChanges();
    //                ((DataTable)ViewState["Aggrements"]).AcceptChanges();
    //                break;
    //            }
    //        }           
    //        BindAggrements((DataTable)ViewState["Aggrements"]);
    //       gridrow = null;
    //    }
    //    imgBtn = null;
    //}
    //private DataTable getAggrements()
    //{
    //    DataTable dt = new DataTable();
    //    dt.Columns.Add(new DataColumn("bint_AgreementId", typeof(long)));
    //    dt.Columns.Add(new DataColumn("nvar_DocumentName", typeof(string)));
    //    dt.Columns.Add(new DataColumn("RowType", typeof(string)));
    //    dt.Columns.Add(new DataColumn("Type", typeof(string)));
    //    return dt.Clone();
    //}
}
