<%@ Page AutoEventWireup="true" CodeFile="DisplayInvoiceableLoadsReport.aspx.cs" Inherits="DisplayInvoiceableLoadsReport"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" EnableEventValidation="false"%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
function goBack()
{
   window.location.href = "../Manager/InvoiceableLoads.aspx?Type=1";
   return window.location.href;
}
</script>
<div id="ContentTbl" class="auto-height">
<table width="100%" style="/* position: absolute;*/ top: 108px;" cellpadding="0" cellspacing="0">
<tr>
<td align="center" >
<input type="button" value="Back" style="height:21px;left: 600px; position: absolute;" class="btnstyle" onclick="goBack()"/>
<asp:Button ID="btnExport" runat="server" CssClass="btnstyle" Text="Zip and Download" OnClick="btnZipandDownload_Click" Height="21px" style="left: 650px; position: absolute;" Visible="true"/>
<asp:Button ID="btnCloseLoads" runat="server" CssClass="btnstyle" Text="Close Selected Loads" onclientclick="return confirm('Are you sure you want to close the selected loads?');" OnClick="btnCloseLoads_Click" Height="21px" style="left: 805px; position: absolute;" Visible="true"/>
</td>
</tr>
</table>
 <asp:Label ID="msg" runat="server" Visible="true" Font-Bold="True" ForeColor="Red"
        Style="font-family: Arial, sans-serif; font-size: 12px;"></asp:Label>
<table id="tbldispaly" runat="server" cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr id="row">
        <td>
    <table  border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, sans-serif;
        color: #4D4B4C; font-size: 12px; height: 1px;" width="100%">
        <tr>
            <td valign="top">
                <asp:Label ID="lblHeader" runat="server" Visible="false" Font-Bold="True" ForeColor="Blue"
        Style="font-family: Arial, sans-serif; font-size: 12px;"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="height: 4px" valign="top">
                &nbsp;<asp:Label ID="lblSearchCriteria" runat="server" Font-Bold="True" ForeColor="Blue"
        Style="font-family: Arial, sans-serif; font-size: 12px;" Visible="false">
    </asp:Label>
    </td>            
        </tr>
        <tr><td>&nbsp;</td></tr>
    </table>
    </td>
    </tr>
    <tr>
    <td>
    <table  border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, sans-serif;
        color: #4D4B4C; font-size: 12px; height: 1px;" width="100%" id="Table1">
        <tr><td>
        <asp:DataGrid ID="dggrid" Width="100%" runat="server" CssClass="Grid" AllowPaging="false" AutoGenerateColumns="false" GridLines="Both" > 
        <ItemStyle CssClass="GridItem" BorderColor="#000000" />
        <HeaderStyle CssClass="GridHeader" BorderColor="#000000" />
        <AlternatingItemStyle CssClass="GridAltItem" />
         <Columns>
         <asp:BoundColumn HeaderText="Load" DataField="Load" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"/>
         <asp:BoundColumn HeaderText="Container" DataField="Container" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"/>
         <asp:BoundColumn HeaderText="Customer" DataField="Customer" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"/>
         <asp:BoundColumn HeaderText="Driver/Carrier" DataField="Driver/Carrier" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"/>
         <asp:BoundColumn HeaderText="Origin" DataField="Origin" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"/>
         <asp:BoundColumn HeaderText="Destination" DataField="Destination" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"/>
         <asp:BoundColumn HeaderText="Delivered Date" DataField="DelivareDate" HeaderStyle-Width="100px"  ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"/>
         <asp:BoundColumn HeaderText="No.of Days" DataField="NoofDays" HeaderStyle-Width="80px"  ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"/>
         <asp:TemplateColumn HeaderStyle-Width="50px" HeaderText="Print" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
         <ItemTemplate >
            <asp:CheckBox ID="chkPrint" runat="server"/>
         </ItemTemplate>
         </asp:TemplateColumn>
         <asp:TemplateColumn HeaderStyle-Width="70px" HeaderText="Action" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
         <ItemTemplate>
           <asp:LinkButton ID="lnkClose" runat="server" onclientclick="return confirm('Are you sure you want to close this load?');" OnClick="lnkClose_Click" CommandArgument='<%# Eval("Load") %>' >Close</asp:LinkButton>
         </ItemTemplate>
         </asp:TemplateColumn>
         </Columns>
    </asp:DataGrid>
    </td></tr><tr><td>
    
       
    <asp:Label ID="lblDisplay" runat="server" Text="" Width="100%"></asp:Label>&nbsp;
    <%--<input id="btnPrintReport" type="button" value="Print"  class="btnstyle" runat="server" style="left: 891px; position: absolute; top: 108px;height: 21px;"/>--%>
   
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
</div>
</asp:Content>


