using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class SMSLogs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "SMS Log";
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            //Session["ReportParameters"] = dtpFromDate.Date + "~~^^^^~~" + dtpToDate.Date;
            ViewState["FromDate"] = dtpFromDate.Date;
            ViewState["ToDate"] = dtpToDate.Date;
            LoadGrid(1);
        }
    }

    protected void lbnSMSId_Click(object sender, EventArgs e)
    {
        SmsPanel.Visible = false;
        LinkButton lnk=(LinkButton)sender;
        LblSMSDetailsHeader.Text = "SMS Details - " + lnk.Text;
        DataSet dt= SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "sp_GetSMSLogDetailsBySMSId", new object[] {Int64.Parse(lnk.Text) });
        if (dt.Tables.Count > 0 && dt.Tables[0].Rows.Count  > 0)
        {
            foreach (DataRow dr in dt.Tables[0].Rows)
            {
                if (dr["DeliveryStatus"] == null || string.IsNullOrEmpty(Convert.ToString(dr["DeliveryStatus"])))
                {
                    dr["DeliveryStatus"] = "Delivery Receipt Not Available";
                }
            }
            SmsPanel.Visible = true;            
            dgSmsDetails.DataSource = dt.Tables[0];
            dgSmsDetails.DataBind();            
        }
    }

    protected void dggrid_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        LoadGrid(e.NewPageIndex + 1);
    }

    private void LoadGrid(int currentPageNumber)
    {
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        //SLine 2017 Enhancement for Office Location Ends
        SmsPanel.Visible = false;
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "sp_GetSMSLogs_RowNumber", new object[] { currentPageNumber, dggrid.PageSize, Convert.ToString(ViewState["FromDate"]), Convert.ToString(ViewState["ToDate"]), officeLocationId });
        if(ds.Tables.Count==2)
        {
            dggrid.CurrentPageIndex = currentPageNumber - 1;
            dggrid.VirtualItemCount = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            dggrid.DataSource = ds.Tables[0];
            dggrid.DataBind();
            
        }
    }
}
