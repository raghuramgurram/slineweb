using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;

public partial class JobRunner : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                DeleteLogDetails();
            }
            catch (Exception ex)
            {
                LogError(ex.Message);
            }

            try
            {
                if (bool.Parse(ConfigurationSettings.AppSettings["DocumentsToDelete"]))
                    DeleteOldFiles();
                else
                    LogOldFiles();
            }
            catch (Exception ex)
            {
                LogError(ex.Message);
            }
        }

    }
    private void DeleteLogDetails()
    {
        FileStream fs = null;
        if (!File.Exists(Server.MapPath("~/LogDeleteDetails.txt")))
            using (fs = File.Create(Server.MapPath("~/LogDeleteDetails.txt"))) { }
        TextWriter tw = new StreamWriter(Server.MapPath("~/LogDeleteDetails.txt"), true);
        try
        {           
            StringBuilder sb = new StringBuilder();
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_Delete_Old_Log_Records", new object[] { int.Parse(ConfigurationSettings.AppSettings["NoOfDaysRecordsToDelete"]) });
            if (ds.Tables.Count > 0)
            {
                sb.AppendLine(ds.Tables[0].Rows[0][0].ToString());
            }
            // write a line of text to the file
            tw.WriteLine(sb.ToString());
            // close the stream
            tw.Close();
        }
        catch (Exception ex)
        {
            tw.Close();
            LogError(ex.Message);
        }
        
    }

    private void DeleteOldFiles()
    {
        FileStream fs = null;
        if (!File.Exists(Server.MapPath("~/LogDeleteDetails.txt")))
            using (fs = File.Create(Server.MapPath("~/LogDeleteDetails.txt"))) { }
        TextWriter tw = new StreamWriter(Server.MapPath("~/LogDeleteDetails.txt"), true);
        try
        {
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetOldLoad", new object[] { int.Parse(ConfigurationSettings.AppSettings["LoadsToDeleteDocuments"]), int.Parse(ConfigurationSettings.AppSettings["DaysToDeleteDocuments"]) });

            StringBuilder sb = new StringBuilder();
            string tempString = string.Empty;
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (Directory.Exists(Server.MapPath("Documents/" + dr[0])))
                    {
                        DirectoryInfo dtInfo = new DirectoryInfo(Server.MapPath("Documents/" + dr[0]));
                        tempString = tempString + ((string.IsNullOrEmpty(tempString) ? string.Empty : ", ") + DeleteFilesAndFolders(dtInfo));
                    }
                }
            sb.Append("Documents deleted for the loads on " + DateTime.Now + " : " + (string.IsNullOrEmpty(tempString) ? "No documents Deleted." : tempString));
            sb.AppendLine();
            // write a line of text to the file
            tw.WriteLine(sb.ToString());
            tw.Close(); 
        }
        catch (Exception ex)
        {
            tw.Close(); 
            LogError(ex.Message);
        }
        
    }

    private void LogOldFiles()
    {
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetOldLoad", new object[] { int.Parse(ConfigurationSettings.AppSettings["LoadsToDeleteDocuments"]), int.Parse(ConfigurationSettings.AppSettings["DaysToDeleteDocuments"]) });
        //DirectoryInfo directory = new DirectoryInfo(Server.MapPath("Documents"));
        //DirectoryInfo[] directoryList = directory.GetDirectories();
        //DateTime maxDate = DateTime.Now.AddDays(-(int.Parse(ConfigurationSettings.AppSettings["DaysToDeleteDocuments"])));
        FileStream fs = null;
        if (!File.Exists(Server.MapPath("~/LogDeleteDetails.txt")))
            using (fs = File.Create(Server.MapPath("~/LogDeleteDetails.txt"))) { }
        TextWriter tw = new StreamWriter(Server.MapPath("~/LogDeleteDetails.txt"), true);
        StringBuilder sb = new StringBuilder();
        string tempString = string.Empty;
        if(ds.Tables.Count>0 && ds.Tables[0].Rows.Count>0)
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (Directory.Exists(Server.MapPath("Documents/" + dr[0])))
                {
                    DirectoryInfo dtInfo = new DirectoryInfo(Server.MapPath("Documents/" + dr[0]));                    
                        FileInfo[] tempFileList = dtInfo.GetFiles();
                        foreach (FileInfo fInfo in tempFileList)
                        {
                            //fInfo.Delete();
                        }
                        string[] tempArrey = dtInfo.FullName.Split('\\');
                        string tempFileName = tempArrey[tempArrey.Length - 1];
                        tempString = tempString + ((string.IsNullOrEmpty(tempString) ? string.Empty : ", ") + tempFileName);
                        //dtInfo.Delete();
                }
            }
        sb.Append("Documents loged for the loads on " + DateTime.Now + " : "+(string.IsNullOrEmpty(tempString) ? "No documents Loged." : tempString));
        sb.AppendLine();
        // write a line of text to the file
        tw.WriteLine(sb.ToString());
        // close the stream
        tw.Close();
    }

    private void LogError(string msg)
    {
        FileStream fs = null;
        if (!File.Exists(Server.MapPath("~/LogDeleteDetails.txt")))
            using (fs = File.Create(Server.MapPath("~/LogDeleteDetails.txt"))) { }
        TextWriter tw = new StreamWriter(Server.MapPath("~/LogDeleteDetails.txt"), true);
        // write a line of text to the file
        tw.WriteLine(msg);
        // close the stream
        tw.Close();
    }

    private string DeleteFilesAndFolders(DirectoryInfo dtInfo)
    {
        FileInfo[] tempFileList = dtInfo.GetFiles();
        foreach (FileInfo fInfo in tempFileList)
        {
            fInfo.Delete();
        }
        DirectoryInfo[] tempDirList = dtInfo.GetDirectories();
        foreach (DirectoryInfo dInfo in tempDirList)
        {
           string tempstr= DeleteFilesAndFolders(dInfo);
        }
        string[] tempArrey = dtInfo.FullName.Split('\\');
        string tempFileName = tempArrey[tempArrey.Length - 1];        
        dtInfo.Delete();
        return tempFileName;
    }
}
