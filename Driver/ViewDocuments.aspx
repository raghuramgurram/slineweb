<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewDocuments.aspx.cs" Inherits="Carrier_ViewDocuments" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link type="text/css" rel="stylesheet" href="../Styles/style.css" />
    <script type="text/javascript" language="javascript">
    function WindowOpen()
    {
        window.screenLeft=screen.availHeight-300;
        window.screenTop=screen.availWidth=400;
    }
</script>
</head>
<body leftmargin="0" topmargin="0">
    <form id="form1" runat="server">
            <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr valign="top">
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <uc1:GridBar ID="GridBar1" runat="server" HeaderText="P.O.D Documents" />
                                </td>
                            </tr>
                        </table>                        
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                <asp:PlaceHolder ID="plhDocs" runat="server"></asp:PlaceHolder>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>               
                <tr>
                    <td>
                        <table id="tblBottomBar" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="right">
                                    <input type="button" value="Close" onclick="window.close();" class="btnstyle"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
    </form>
</body>
</html>
