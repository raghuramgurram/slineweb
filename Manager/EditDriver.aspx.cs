using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class EditDriver : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Assigned Driver Edit";
        if (!IsPostBack)
        {
            if (Request.QueryString.Count > 0)
            {
                try
                {
                    string[] strParam = Convert.ToString(Request.QueryString[0]).Trim().Split('?');                    
                    //ViewState["LoadId"] = Convert.ToInt64(strParam[0]);
                    //ViewState["DriverId"] = Convert.ToInt64(strParam[1]);
                    //barCurrent.HeaderText = "Load Id : " + Convert.ToString(strParam[0]);
                    ViewState["AssignedID"] = Convert.ToInt64(strParam[1]);
                    FillDriversDeatils(Convert.ToInt64(strParam[1]));
                }
                catch { }
            }
        }
    }

    private void FillDriversDeatils(long AssignedId)
    {
        //lblDriver.Text = DBClass.executeScalar("select [nvar_NickName] from [eTn_Employee] where [bint_EmployeeId]="+EmployeeId);
        DataTable dt = DBClass.returnDataTable("SELECT [eTn_AssignDriver].[nvar_From],[eTn_AssignDriver].[nvar_FromCity],[eTn_AssignDriver].[nvar_FromState],[eTn_AssignDriver].[nvar_To],[eTn_AssignDriver].[nvar_Tocity],[eTn_AssignDriver].[nvar_Tostate],[eTn_AssignDriver].[nvar_Tag],[eTn_AssignDriver].[date_DateAssigned],eTn_Employee.[nvar_NickName],[eTn_AssignDriver].[bint_LoadId],[eTn_AssignDriver].bint_EmployeeId,[eTn_AssignDriver].date_DateAssigned" +
                                                " FROM [eTn_AssignDriver]	inner join [eTn_Employee] on eTn_Employee.[bint_EmployeeId]=[eTn_AssignDriver].bint_EmployeeId Where [eTn_AssignDriver].[bint_AssignedId]=" + AssignedId);
       // DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetAssignedDriverDetails", new object[] { LoadId,EmployeeId });
        if (dt.Rows.Count > 0)
        {
            txtFrom.Text = Convert.ToString(dt.Rows[0][0]);
            txtFromCity.Text = Convert.ToString(dt.Rows[0][1]);
            txtFromstate.Text = Convert.ToString(dt.Rows[0][2]);
            txtTO.Text = Convert.ToString(dt.Rows[0][3]);
            txtToCity.Text = Convert.ToString(dt.Rows[0][4]);
            txtState.Text = Convert.ToString(dt.Rows[0][5]);
            txtTag.Text = Convert.ToString(dt.Rows[0][6]);
            lblDriver.Text = Convert.ToString(dt.Rows[0][8]);
            ViewState["LoadId"] = Convert.ToString(dt.Rows[0][9]);
            barCurrent.HeaderText = "Load Id : " + Convert.ToString(dt.Rows[0][9]);
            ViewState["DriverId"] = Convert.ToString(dt.Rows[0][10]);
            if (dt.Rows[0][11]!=null &&  Convert.ToString(dt.Rows[0][11]).Trim().Length > 0)
            {
                DateTime Dtime = Convert.ToDateTime(dt.Rows[0][11]);
                DatePicker1.Date = Convert.ToString(Dtime);
                TimePicker1.Time = Dtime.ToShortTimeString();
            }
        }
        if (dt != null) { dt.Dispose(); dt = null; }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ViewState["LoadId"] != null && ViewState["DriverId"] != null && ViewState["AssignedID"]!=null)
        {
             object[] objParams = new object[] { Convert.ToInt64(ViewState["LoadId"]),
                                Convert.ToInt64(ViewState["DriverId"]),txtFrom.Text.Trim(), txtFromCity.Text.Trim(), 
                                txtFromstate.Text.Trim(), txtTO.Text.Trim(), txtToCity.Text.Trim(), txtState.Text.Trim(), 
                                txtTag.Text.Trim(),Convert.ToInt64(ViewState["AssignedID"]),CommonFunctions.CheckDateTimeNull(DatePicker1.Date+" "+TimePicker1.Time)};
                if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Update_AssignDrivers", objParams) > 0)
                {
                    objParams = null;
                    Response.Redirect("~/Manager/Dispatch.aspx?" + Convert.ToString(ViewState["LoadId"]));
                }             
        }                
    }   
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (ViewState["LoadId"] != null)
        {
            Response.Redirect("~/Manager/Dispatch.aspx?" + Convert.ToString(ViewState["LoadId"]));
        }
    }
}

