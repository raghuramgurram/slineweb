using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections.Generic;

public partial class Manager_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Payable Export";       
        if (!IsPostBack)
        {
            ViewState["bSelect"] = true;
            RadDriver.Checked = true;
            //RadCarrier_CheckedChanged(null, null);
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (ViewState["FromDate"] != null)
            dtpFromDate.SetDate(Convert.ToString(ViewState["FromDate"]));
        if (ViewState["ToDate"] != null)
            dtpToDate.SetDate(Convert.ToString(ViewState["ToDate"]));

        for (int index = 0; index < gvPayables.Rows.Count; index++)
        {
            if (index % 2 == 0)
            {
                for (int k = 0; k < gvPayables.Columns.Count; k++)
                {
                    gvPayables.HeaderRow.Cells[k].CssClass = "GridHeader";
                    gvPayables.Rows[index].Cells[k].CssClass = "GridItem";
                }
            }
            else
            {
                for (int k = 0; k < gvPayables.Columns.Count; k++)
                {
                    gvPayables.Rows[index].Cells[k].CssClass = "GridAltItem";
                }
            }
        }
    }
    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{

            object[] objParams = null;
            DataSet ds = new DataSet();
            List<DriverWiseReportExport> driverWiseReportList = new List<DriverWiseReportExport>();
            string employeeId = gvPayables.DataKeys[e.Row.RowIndex].Value.ToString();
            long officeLocationId = 0;
            if (Session["OfficeLocationID"] != null)
            {
                officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
            }
            GridView gvOrders = e.Row.FindControl("gvOrders") as GridView;
            if (RadDriver.Checked)
            {
                objParams = (Convert.ToString(dtpFromDate.Date) + "~~^^^^~~" + (dtpToDate.Date) + "~~^^^^~~" + ("inner join [eTn_AssignDriver] on [eTn_AssignDriver].[bint_LoadId]=") + "~~^^^^~~" + (" and [eTn_AssignDriver].[bint_EmployeeId]=" + employeeId + "") + "~~^^^^~~" + employeeId + "~~^^^^~~" + officeLocationId).Trim().Replace(":", " like ").Replace("~~^^^^~~", "^").Split('^');
            }
            else
            {
                objParams = (Convert.ToString(dtpFromDate.Date) + "~~^^^^~~" + (dtpToDate.Date) + "~~^^^^~~" + ("inner join [eTn_AssignCarrier] on [eTn_AssignCarrier].[bint_LoadId]=") + "~~^^^^~~" + (" and [eTn_AssignCarrier].[bint_CarrierId]=" + employeeId + "") + "~~^^^^~~" + employeeId + "~~^^^^~~" + officeLocationId).Trim().Replace(":", " like ").Replace("~~^^^^~~", "^").Split('^');
            }
            ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetPayableEntriesWithDatesOnlyPending", objParams);
            if (ds != null && ds.Tables.Count > 0)
            {
              

                DataTable dt = ds.Tables[0];
                FormatDataTable(ref dt);
                //    driverWiseReport.InnerHtml = ReportDisplay(dt, strColumnsList, colWidths);
                gvOrders.DataSource = ViewState["tblChildSummary"] = dt;
                gvOrders.DataBind();
                
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnDateSearch_Click(object sender, EventArgs e)
    {
        ViewState["FromDate"] = dtpFromDate.Date;
        ViewState["ToDate"] = dtpToDate.Date;
        BindGridData();
    }
    protected void chkAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkLoadID = (sender as CheckBox);
        GridViewRow row = ChkLoadID.NamingContainer as GridViewRow;
        GridView gv = row.FindControl("gvOrders") as GridView;        
        foreach (GridViewRow gvRow in gv.Rows)
        {
            CheckBox ChkDriverID = gvRow.FindControl("ChkDriverID") as CheckBox;
           

            if (ChkLoadID.Checked)
            {
                ChkDriverID.Checked = true;
            }
            else
            {
                ChkDriverID.Checked = false;
            }           
        }           
       
        updpage.Update();
      
    }
    protected void Payments_OnCheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkDriverID = (sender as CheckBox);       
        int noOfRowsChecked = 0;
        GridViewRow row = ChkDriverID.NamingContainer as GridViewRow;
        GridView gv = row.NamingContainer as GridView;
        foreach (GridViewRow gvRow in gv.Rows)
        {
            CheckBox ChkRowDriverID = gvRow.FindControl("ChkDriverID") as CheckBox;

            if (ChkRowDriverID.Checked)
            {
                noOfRowsChecked++;
            }           
        }

        if (gv.Rows.Count > 0)
        {
            CheckBox ChkLoadID = gv.Parent.Parent.FindControl("ChkLoadID") as CheckBox;
            ChkLoadID.Checked = gv.Rows.Count == noOfRowsChecked;
        }

        updpage.Update();
    }
    private void BindGridData()
    {
        try
        {
            lblError.Text = " No records exist for the selected dates.";
            //SLine 2017 Enhancement for Office Location Starts
            long officeLocationId = 0;
            if (Session["OfficeLocationID"] != null)
            {
                officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
            }
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetExportPayableEntries", new object[] { dtpFromDate.Date, dtpToDate.Date, (ddlDriverorCarrier.SelectedIndex > 0 ? ddlDriverorCarrier.SelectedItem.Value : "0"), (RadDriver.Checked ? "D" : "C"), officeLocationId });
            //SLine 2017 Enhancement for Office Location Ends
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ViewState["tblPayables"] = ds.Tables[0];
                        gvPayables.DataSource = ViewState["tblSummary"] = ds.Tables[1];
                        gvPayables.DataBind();
                        gvPayables.Visible = true;
                        lblError.Visible = false;
                        if (ViewState["bExport"] != null)
                            this.pnlCreateFile.Visible = true;
                    }
                    else
                    {
                       
                        this.lblError.Visible = true;
                        gvPayables.Visible = false;
                    }
                }
                else
                {
                    if (ViewState["bExport"] != null)
                        this.pnlCreateFile.Visible = true;

                    this.lblError.Visible = true;
                    gvPayables.Visible = false;
                }
            }
            else
                lblError.Visible = true;
            if (ds != null) { ds.Dispose(); ds = null; }
        }
        catch (Exception ex)
        {

        }
        updpage.Update();
    }

    //private bool ExportPayablefile(DataTable dt, DataTable dtSummary)
    //{
    //    string qbendTransHeader = string.Empty;
    //    string qbsplheader1;
    //    string qbspl1;
    //    string qbendtran1;
    //    string qbTransHeader;
    //    string fname = string.Empty;
    //    bool bdownoad = true;
    //    DataRow TempRow;
    //    ViewState["FileName"] = Convert.ToString(Session["UserId"]) + "_" + CreateFileName();

    //    System.Text.StringBuilder objBulider = new System.Text.StringBuilder();
    //    StreamWriter objSR1 = null;
    //    try
    //    {
    //        FileInfo fileinfo = new FileInfo(Server.MapPath("~/ExportFiles") + "\\" + Convert.ToString(ViewState["FileName"]));
    //        objSR1 = File.CreateText(fileinfo.FullName);

    //        qbTransHeader = "!TRNS" + "\t" + "TRNSID" + "\t" + "TRNSTYPE" + "\t" + "DATE" + "\t" + "ACCNT" + "\t" + "NAME" + "\t" + "AMOUNT" + "\t" + "DOCNUM" + "\t" + "CLEAR" + "\t" + "TOPRINT" + "\t" + "ADDR1" + "\t" + "TERMS";
    //        qbsplheader1 = "!SPL" + "\t" + "SPLID" + "\t" + "TRNSTYPE" + "\t" + "DATE" + "\t" + "ACCNT" + "\t" + "AMOUNT" + "\t" + "DOCNUM" + "\t" + "MEMO";
    //        qbendTransHeader = "!ENDTRNS" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
    //        objSR1.WriteLine(qbTransHeader);
    //        objSR1.WriteLine(qbsplheader1);
    //        objSR1.WriteLine(qbendTransHeader);

    //        qbendtran1 = "ENDTRNS" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
    //        if (dt.Rows.Count > 0)
    //        {
    //            foreach (DataRow drowSummary in dtSummary.Rows)
    //            {
    //                DataRow[] drows = dt.Select("PlayerName='" + Convert.ToString(drowSummary["PlayerName"]) + "'", "CurrentDate ASC");
    //                TempRow = drows[0];

    //                objBulider.Append("TRNS" + "\t" + "" + "\t" + "BILL" + "\t" + Convert.ToString(TempRow["CurrentDate"]) + "\t" + "Accounts Payable" + "\t");
    //                objBulider.Append(Convert.ToString(TempRow["PlayerName"]) + "\t" + "-" + Convert.ToString(drowSummary["NetAmount"]) + "\t" + "" + "\t");
    //                objBulider.Append("N" + "\t" + "Y" + "\t" + Convert.ToString(TempRow["PlayerName"]) + "\t" + "NET 15");
    //                objSR1.WriteLine(objBulider.ToString());
    //                objBulider.Remove(0, objBulider.Length);

    //                foreach (DataRow Drow in drows)
    //                {
    //                    //!SPL	SPLID	TRNSTYPE	DATE	ACCNT	INVITEM	AMOUNT	DOCNUM	MEMO	QNTY	PRICE
    //                    if (Convert.ToDouble(Drow["NetAmount"]) > 0)
    //                    {
    //                        qbspl1 = "SPL" + "\t" + "" + "\t" + "BILL" + "\t" + Convert.ToString(Drow["CurrentDate"]) + "\t" + "Payroll Expenses" + "\t" + Convert.ToString(Drow["NetAmount"]) + "\t" + Convert.ToString(Drow["LoadId"]) + "\t";
    //                        qbspl1 = qbspl1 + Convert.ToString(Drow["Memo"]);
    //                        objSR1.WriteLine(qbspl1);
    //                        qbspl1 = string.Empty;
    //                    }
    //                }
    //                objSR1.WriteLine(qbendtran1);
    //                drows = null; TempRow = null;
    //                this.pnlCreateFile.Visible = true;
    //            }
    //        }
    //        objSR1.Close();
    //        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_AddExportFiles", new object[] { fileinfo.Name, txtNote.Text, "P" });
    //        fileinfo = null;
    //    }
    //    catch (Exception ex)
    //    {
    //        bdownoad = false;
    //    }
    //    return bdownoad;
    //}

    private string CreateFileName()
    {
        string filename = "BILL_" + Convert.ToDateTime(DateTime.Now).ToString("MMddyy") + "_" + (DateTime.Now.Hour.ToString().Length == 1 ? "0" + DateTime.Now.Hour.ToString() : DateTime.Now.Hour.ToString()) + (DateTime.Now.Minute.ToString().Length == 1 ? "0" + DateTime.Now.Minute.ToString() : DateTime.Now.Minute.ToString()) + ".iif";
        return filename;
    }

    protected void FillCarriers()
    {
        ddlDriverorCarrier.Items.Clear();
        //DataTable dt = DBClass.returnDataTable("select bint_CarrierId,nvar_CarrierName from eTn_Carrier order by [nvar_CarrierName]");
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCarrierNames", new object[] { officeLocationId });
        //SLine 2017 Enhancement for Office Location Ends
        if (ds.Tables.Count > 0)
        {
            ddlDriverorCarrier.DataSource = ds.Tables[0];
            ddlDriverorCarrier.DataTextField = "nvar_CarrierName";
            ddlDriverorCarrier.DataValueField = "bint_CarrierId";
            ddlDriverorCarrier.DataBind();
        }
        ddlDriverorCarrier.Items.Insert(0, "All");
        ddlDriverorCarrier.SelectedIndex = 0;
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    private void FillDrivers()
    {
        ddlDriverorCarrier.Items.Clear();
        //dt = DBClass.returnDataTable("select bint_EmployeeId as 'Id',nvar_NickName as 'Name' from eTn_Employee where bint_RoleId=(select R.bint_RoleId from eTn_Role R where lower(nvar_RoleName)='driver') order by [Name]");
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriverNames");
        if (ds.Tables.Count > 0)
        {
            ddlDriverorCarrier.DataSource = ds.Tables[0];
            ddlDriverorCarrier.DataTextField = "Name";
            ddlDriverorCarrier.DataValueField = "Id";
            ddlDriverorCarrier.DataBind();
        }
        ddlDriverorCarrier.Items.Insert(0, "All");
        ddlDriverorCarrier.SelectedIndex = 0;
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    protected void RadDriver_CheckedChanged(object sender, EventArgs e)
    {
        ddlDriverorCarrier.Visible = true;
        FillDrivers();
    }
    protected void RadCarrier_CheckedChanged(object sender, EventArgs e)
    {
        ddlDriverorCarrier.Visible = true;
        FillCarriers();
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        LinkButton lnk = null;
        try
        {
            lnk = (LinkButton)e.CommandSource;
        }
        catch
        {

        }
        if (lnk != null)
        {
            if (lnk.ID == "lnkDetails")
            {
                GridViewRow row = lnk.NamingContainer as GridViewRow;
                GridView gv = row.FindControl("gvOrders") as GridView;
                Panel pnlOrders = gv.Parent.Parent.FindControl("pnlOrders") as Panel;
                Image imgToggle = lnk.FindControl("imgToggle") as Image;
                if (lnk.CommandArgument == "Hide")
                {
                    pnlOrders.Visible = true;
                    lnk.CommandArgument = "Show";
                    imgToggle.ImageUrl = "../Images/minus.png";
                }
                else
                {
                    pnlOrders.Visible = false;
                    lnk.CommandArgument = "Hide";
                    imgToggle.ImageUrl = "../Images/plus.png";
                }
            }
            else
            {
                if (lnk.Text == "Select" && Convert.ToBoolean(ViewState["bSelect"]))
                {
                    foreach (GridViewRow gridrow in gvPayables.Rows)
                    {

                        ((System.Web.UI.WebControls.CheckBox)gvPayables.Rows[gridrow.RowIndex].Cells[0].FindControl("ChkLoadID")).Checked = true;
                        GridView gv = gridrow.FindControl("gvOrders") as GridView;
                        foreach (GridViewRow gvRow in gv.Rows)
                        {
                            CheckBox ChkDriverID = gvRow.FindControl("ChkDriverID") as CheckBox;
                            ChkDriverID.Checked = true;

                        }
                    }
                    ViewState["bSelect"] = false;
                    lnk.Text = "Un Select";
                }
                else
                {
                    foreach (GridViewRow gridrow in gvPayables.Rows)
                    {
                        ((System.Web.UI.WebControls.CheckBox)gvPayables.Rows[gridrow.RowIndex].Cells[0].FindControl("ChkLoadID")).Checked = false;
                        GridView gv = gridrow.FindControl("gvOrders") as GridView;
                        foreach (GridViewRow gvRow in gv.Rows)
                        {
                            CheckBox ChkDriverID = gvRow.FindControl("ChkDriverID") as CheckBox;
                            ChkDriverID.Checked = false;
                        }
                    }
                    ViewState["Text"] = "Select";
                    ViewState["bSelect"] = true;
                    lnk.Text = "Select";
                }
            }
        }
       updpage.Update();
    }
    protected void btnPay_Click(object sender, EventArgs e)
    {
        try
        {
            Button btnpay = sender as Button;
            object[] objParams = null;
            DataSet ds = new DataSet();
            string employeeId = btnpay.CommandArgument;
            long officeLocationId = 0;
            if (Session["OfficeLocationID"] != null)
            {
                officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
            }
            if (RadDriver.Checked)
            {
                objParams = (Convert.ToString(dtpFromDate.Date) + "~~^^^^~~" + (dtpToDate.Date) + "~~^^^^~~" + ("inner join [eTn_AssignDriver] on [eTn_AssignDriver].[bint_LoadId]=") + "~~^^^^~~" + (" and [eTn_AssignDriver].[bint_EmployeeId]=" + employeeId + "") + "~~^^^^~~" + employeeId + "~~^^^^~~" + officeLocationId).Trim().Replace(":", " like ").Replace("~~^^^^~~", "^").Split('^');
            }
            else
            {
                objParams = (Convert.ToString(dtpFromDate.Date) + "~~^^^^~~" + (dtpToDate.Date) + "~~^^^^~~" + ("inner join [eTn_AssignCarrier] on [eTn_AssignCarrier].[bint_LoadId]=") + "~~^^^^~~" + (" and [eTn_AssignCarrier].[bint_CarrierId]=" + employeeId + "") + "~~^^^^~~" + employeeId + "~~^^^^~~" + officeLocationId).Trim().Replace(":", " like ").Replace("~~^^^^~~", "^").Split('^');
            }
            ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetPayableEntriesWithDatesOnlyPending", objParams);
            if (ds != null && ds.Tables.Count > 0)
            {
                bool bTrue = false;
                //if (ViewState["tblChildSummary"] != null)
                //{
                //DataTable table = (DataTable)ViewState["tblChildSummary"];
                DataTable dtTemp = ds.Tables[0].Clone();
                GridViewRow row = btnpay.NamingContainer as GridViewRow;
                GridView gv = row.FindControl("gvOrders") as GridView;
                foreach (GridViewRow gridrow in gv.Rows)
                {

                    {
                        if (((System.Web.UI.WebControls.CheckBox)gv.Rows[gridrow.RowIndex].Cells[0].FindControl("ChkDriverID")).Checked)
                        {
                            DataRow drow = dtTemp.NewRow();
                            drow.ItemArray = ds.Tables[0].Rows[gridrow.RowIndex].ItemArray;
                            dtTemp.Rows.Add(drow);
                            drow = null;
                            bTrue = true;
                        }
                    }
                }

                if (bTrue)
                {
                    //if (ExportAndEmailPayablefile((DataTable)ViewState["tblPayables"], dtTemp))
                    //{
                    string strLoads = string.Empty;
                    foreach (DataRow drowSummary in dtTemp.Rows)
                    {
                        //DataRow[] drows = ((DataTable)ViewState["tblChildSummary"]).Select("PlayerName='" + Convert.ToString(drowSummary["PlayerName"]) + "'");
                        string driverOrCarrierId = drowSummary["DriverOrCarrierId"].ToString();
                        if (!string.IsNullOrEmpty(driverOrCarrierId))
                        {
                            string[] selectedIds = driverOrCarrierId.Split(',');
                            for (int i = 0; i < selectedIds.Length; i++)
                            {
                                strLoads += drowSummary["Load"].ToString() + "^" + selectedIds[i].ToString() + ";";
                            }

                        }

                        if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Update_PayableStatus", new object[] { strLoads }) > 0) ;
                    }
                    SendEmailPayable(dtTemp, Convert.ToInt32(btnpay.CommandArgument));
                    BindGridData();
                }
                else
                {
                    if (RadDriver.Checked)
                    {
                        //Response.Write("<script>alert('Please select Driver(s).')</script>");
                        lblError.Text = "Please select Driver(s).";
                        lblError.Visible = true;
                    }
                    else if (RadCarrier.Checked)
                    {
                       // Response.Write("<script>alert('Please select Carrier(s).')</script>");
                        lblError.Text = "Please select Carrier(s).";
                        lblError.Visible = true;
                    }
                   // return;
                }
                if (ds != null) { ds.Dispose(); ds = null; }
                if (dtTemp != null) { dtTemp.Dispose(); dtTemp = null; }
            }
            else
                lblError.Visible = true;
        }
        catch (Exception ex)
        {
        }
        updpage.Update();
    }
    protected void SendEmailPayable(DataTable selectedLoadIds, int id)
    {

        string[] strColumnsList = null;
        string[] colWidths = null;
        DriverWiseReportExport driverWiseReport = new DriverWiseReportExport();
        strColumnsList = new string[] { "Load", "Container", "Invoice/Tag", "Driver/Carrier Name", "Total", "Total Report", "From", "To" };
        colWidths = new string[] { "60px", "100px", "60px", "130px", "50px", "130px", "130px", "130px" };
        string subject = "Payment Details : From Date : " + dtpFromDate.Date + " To Date : " + dtpToDate.Date;
        FormatDataTable(ref selectedLoadIds);
        driverWiseReport.InnerHtml = ReportDisplay(selectedLoadIds, strColumnsList, colWidths);
        string Email = string.Empty;
        if (RadDriver.Checked)
        {
            Email = getEmailId(Convert.ToInt32(id));
        }
        else
        {
            Email = getCarrierEmailId(Convert.ToInt32(id));
        }
        //if (!string.IsNullOrEmpty(driverWiseReport.InnerHtml))
        //{
        //    driverWiseReport.InnerHtml = driverWiseReport.InnerHtml.Replace('^',' ');
        //}
        if (string.IsNullOrEmpty(Email))
        {
            string emailBody = "&nbsp;&nbsp;&nbsp;&nbsp;<span style='color:blue;font-weight:bold;'> Payment Details :  </br></br> &nbsp;&nbsp;&nbsp;&nbsp; From Date : " + dtpFromDate.Date + " To Date : " + dtpToDate.Date + "</br></br> </span>" + driverWiseReport.InnerHtml;
            CommonFunctions.SendEmail(GetFromXML.CcEmail, Email, GetFromXML.CcEmail, "", subject, emailBody.ToString(), null, null);
        }


    }
    //protected void btnExport_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        bool bTrue = false;
    //        if (ViewState["tblSummary"] != null)
    //        {
    //            DataTable table = (DataTable)ViewState["tblSummary"];
    //            DataTable dtTemp = table.Clone();
    //            foreach (GridViewRow gridrow in gvPayables.Rows)
    //            {
    //                if (((System.Web.UI.WebControls.CheckBox)gvPayables.Rows[gridrow.RowIndex].Cells[0].FindControl("ChkLoadID")).Checked)
    //                {
    //                    DataRow drow = dtTemp.NewRow();
    //                    drow.ItemArray = table.Rows[gridrow.RowIndex].ItemArray;
    //                    dtTemp.Rows.Add(drow);
    //                    drow = null;
    //                    bTrue = true;
    //                }
    //            }

    //            if (bTrue)
    //            {
    //                if (ExportPayablefile((DataTable)ViewState["tblPayables"], dtTemp))
    //                {
    //                    string strLoads = string.Empty;
    //                    foreach (DataRow drowSummary in dtTemp.Rows)
    //                    {
    //                        DataRow[] drows = ((DataTable)ViewState["tblPayables"]).Select("PlayerName='" + Convert.ToString(drowSummary["PlayerName"]) + "'");
    //                        foreach (DataRow Drow in drows)
    //                        {
    //                            strLoads += Drow["LoadId"].ToString() + "^" + Drow["DriverOrCarrierId"].ToString() + ";";
    //                        }
    //                        drows = null;
    //                    }

    //                    if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Update_PayableStatus", new object[] { strLoads }) > 0)
    //                        BindGridData();
    //                }
    //            }
    //            else
    //            {
    //                if (RadDriver.Checked)
    //                    Response.Write("<script>alert('Please select Driver(s).')</script>");
    //                else if (RadCarrier.Checked)
    //                    Response.Write("<script>alert('Please select Carrier(s).')</script>");
    //                return;
    //            }
    //            if (table != null) { table.Dispose(); table = null; }
    //            if (dtTemp != null) { dtTemp.Dispose(); dtTemp = null; }
    //        }
    //        else
    //            lblError.Visible = true;
    //    }
    //    catch
    //    {
    //    }
    //}
    protected void lnkDownload_Click(object sender, EventArgs e)
    {
        ViewState["bExport"] = null;
        if (ViewState["FileName"] == null)
        {
            System.IO.DirectoryInfo objinfo = new DirectoryInfo(Server.MapPath("~/ExportFiles"));
            System.IO.FileInfo[] fileinfo = objinfo.GetFiles();
            for (int i = 0; i < fileinfo.Length; i++)
            {
                string[] strUser = fileinfo[i].Name.Split('_');
                if (Convert.ToString(Session["UserId"]).Trim().Contains(strUser[0]) && strUser[1] == "BILL")
                {
                    ViewState["bExport"] = null;
                    Downoloadfile(fileinfo[i].FullName, fileinfo[i].Name);
                    break;
                }
            }
            objinfo = null; fileinfo = null;
            lnkDownload.Visible = false;
        }
        else
        {
            FileInfo file = new FileInfo(Server.MapPath("~/ExportFiles") + "\\" + Convert.ToString(ViewState["FileName"]));
            if (file.Exists)
            {
                Downoloadfile(Server.MapPath("~/ExportFiles") + "\\" + Convert.ToString(ViewState["FileName"]), Convert.ToString(ViewState["FileName"]));
            }
            else
            {
                System.IO.DirectoryInfo objinfo = new DirectoryInfo(Server.MapPath("~/ExportFiles"));
                FileInfo[] fileinfo = objinfo.GetFiles();
                for (int i = 0; i < fileinfo.Length; i++)
                {
                    if (fileinfo[i].Name == Convert.ToString(ViewState["FileName"]))
                    {
                        string[] strUser = fileinfo[i].Name.Split('_');
                        if (Convert.ToString(Session["UserId"]).Trim().Contains(strUser[0]) && strUser[1] == "BILL")
                        {
                            ViewState["bExport"] = null;
                            Downoloadfile(fileinfo[i].FullName, fileinfo[i].Name);
                            break;
                        }
                    }
                }
                objinfo = null; fileinfo = null;
                lnkDownload.Visible = false;
            }
            file = null;
        }
    }

    private void Downoloadfile(string filename, string name)
    {
        try
        {
            FileInfo fileinfo = new FileInfo(filename);
            if (fileinfo.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileinfo.Name);
                Response.AddHeader("Content-Length", fileinfo.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(fileinfo.FullName);
                Response.End();
            }
            else
            {
                Response.Write("<script>alert('file doesnot exist.');<script>");
            }
            fileinfo = null;
        }
        catch
        { }
    }
    protected void FormatDataTable(ref DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    if (Convert.ToString(dt.Rows[i][j]).Contains("^^^^"))
                    {
                        if (Convert.ToString(Session["ReportName"]) == "GetPayableEntries")
                        {
                            if (Convert.ToInt32(dt.Rows[i]["LoadStatus"]) != 12)
                            {
                                if (j == 4)
                                {
                                    dt.Rows[i][j] = FormatMultipleRowColumnsred(Convert.ToString(dt.Rows[i][j]).Trim());
                                }
                                else
                                {
                                    dt.Rows[i][j] = FormatMultipleRowColumns(Convert.ToString(dt.Rows[i][j]).Trim());
                                }
                            }
                            else
                            {
                                dt.Rows[i][j] = FormatMultipleRowColumns(Convert.ToString(dt.Rows[i][j]).Trim());
                            }

                        }
                        else
                        {
                            dt.Rows[i][j] = FormatMultipleRowColumns(Convert.ToString(dt.Rows[i][j]).Trim());
                        }
                    }
                }
            }
        }
    }

    public string FormatMultipleRowColumns(string strColValue)
    {
        string strFormat = "";
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0)
        {
            strFormat = "<table cellpadding=0 cellspacing=0 border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;>";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (i == 0)
                    strFormat += "<tr ><td  width=100% align=left valign=middle style=color:black;font-size:12;height:90px;>" + strMuls[i].Trim() + "</td></tr>";
                else
                {
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:black;font-size:12;height:90px;>" + strMuls[i].Trim() + "</td></tr>";
                }
            }
            strFormat += "</table>";
        }
        strMuls = null;
        return strFormat;
    }

    public string FormatMultipleRowColumnsred(string strColValue)
    {
        string strFormat = "";
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0)
        {
            strFormat = "<table cellpadding=0 cellspacing=0 border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;>";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (i == 0)
                    strFormat += "<tr ><td  width=100% align=left valign=middle style=color:black;font-size:12;height:90px;><span style='color:red;'>" + strMuls[i].Trim() + "</span></td></tr>";
                else
                {
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:black;font-size:12;height:90px;><span style='color:red;'>" + strMuls[i].Trim() + "</span></td></tr>";
                }
            }
            strFormat += "</table>";
        }
        strMuls = null;
        return strFormat;
    }

    protected string ReportDisplay(DataTable dt, string[] strColumnsList, string[] colWidths)
    {
        string strDisplay = string.Empty;
        if (dt.Rows.Count > 0)
        {
            string strColProperties = " align=left valign=middle style=font-size:12px;border-top-width:0px;border-right-width:0px;border-color:black;padding-left:3px;padding-right:3px;background-color:#F3F8FC;font-family:Arial,Helvetica,sans-serif;";
            string strHeadProperties = " align=left valign=middle style=font-size:14px;border-top-width:0px;border-right-width:0px;border-color:black;background-color:lightblue;padding-left:4px;padding-right:4px;font-family:Arial,Helvetica,sans-serif;";
            string strTableProperties = " border=1 cellpading=0 cellspacing=0 width=100% align=center valign=top style=border-color:black;border-left-width:0px;border-bottom-width:0px;";//padding-left:2px;padding-right:2px;";
            strDisplay = "<table width=100% border=0 cellpadding=0 cellspacing=0 id=ContentTbl><tr valign=top><td>" +
                "<table " + strTableProperties + ">";
            for (int i = 0; i < strColumnsList.Length; i++)
            {
                if (i == 0)
                    strDisplay += "<tr>";
                strDisplay += "<th " + strHeadProperties + "width:" + colWidths[i] + "; align=left >" + strColumnsList[i] + "</th>";
                if (i == strColumnsList.Length - 1)
                    strDisplay += "</tr>";
            }
            //if (Convert.ToString(Session["ReportName"]) == "GetPayableEntries")
            //{
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    strDisplay += "<tr >";
                    for (int j = 0; j < (dt.Columns.Count -4); j++)
                    {
                        if (Convert.ToInt32(dt.Rows[i]["LoadStatus"]) != 12)
                        {
                            int x = 8;
                            //if (dt.Columns.Count > 9)
                            //{
                            //    x = 9;
                            //}
                            if (j != x)
                            {
                                if (j == 0 || j == 4)
                                {
                                    strDisplay += "<td" + strColProperties + ">";
                                    strDisplay += "<span style='color:red;'>" + Convert.ToString(dt.Rows[i][j]) + "</span>";
                                    strDisplay += "</td>";
                                }
                                else
                                {
                                    strDisplay += "<td" + strColProperties + ">";
                                    strDisplay += Convert.ToString(dt.Rows[i][j]);
                                    strDisplay += "</td>";
                                }
                            }
                        }
                        else
                        {
                            int x = 8;
                            //if (dt.Columns.Count > 9)
                            //{
                            //    x = 9;
                            //}
                            if (j != x)
                            {
                                strDisplay += "<td" + strColProperties + ">";
                                strDisplay += Convert.ToString(dt.Rows[i][j]);
                                strDisplay += "</td>";
                            }
                        }
                    }
                    strDisplay += "</tr>";
                }
            //}
            //else
            //{
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        strDisplay += "<tr >";
            //        for (int j = 0; j < dt.Columns.Count; j++)
            //        {
            //            strDisplay += "<td" + strColProperties + ">";
            //            strDisplay += Convert.ToString(dt.Rows[i][j]);
            //            strDisplay += "</td>";
            //        }
            //        strDisplay += "</tr>";
            //    }
            //}
            strDisplay += "</table>" +
                "</td></tr></table>";
        }
        return strDisplay;
    }
    private string getEmailId(long employeeId)
    {
        string emailAddress = string.Empty;
        object[] param = new object[] { employeeId };
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "Get_Employee_Details", param);
        if (ds.Tables.Count > 0)
        {
            emailAddress = Convert.ToString(ds.Tables[0].Rows[0][15]).Trim();
        }
        return emailAddress;
    }
    private string getCarrierEmailId(long employeeId)
    {
        string emailAddress = string.Empty;
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetContactDetails", new object[] { employeeId, "Carrier" });

        if (ds.Tables.Count > 0)
        {
            emailAddress = Convert.ToString(ds.Tables[0].Rows[0][2]).Trim();
        }
        return emailAddress;
    }
}
public class DriverWiseReportExport
{
    private string innerHtml;

    public string InnerHtml
    {
        get { return innerHtml; }
        set { innerHtml = value; }
    }

    private string header;

    public string Header
    {
        get { return header; }
        set { header = value; }
    }

    private string searchCriteria;

    public string SearchCriteria
    {
        get { return searchCriteria; }
        set { searchCriteria = value; }
    }

    private string total;

    public string Total
    {
        get { return total; }
        set { total = value; }
    }

    private string paid;

    public string Paid
    {
        get { return paid; }
        set { paid = value; }
    }

    private string pending;

    public string Pending
    {
        get { return pending; }
        set { pending = value; }
    }

    private string count;

    public string Count
    {
        get { return count; }
        set { count = value; }
    }
}
