using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Collections.Generic;
using System.Web.Script.Serialization;

public partial class top : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
        CommonFunctions.InsertIntoInfoLog("Top.ascx :: Page_Load :: START");
        lblHeadText.Text = Convert.ToString(Session["TopHeaderText"]);
        lblUserId.Text = Convert.ToString(Session["UserId"])+",";
        //hiding the error message label (Madhav)
        lblErMessSms.Visible = false;
        lblBrodcastError.Visible = false;
        lblSmssendmessage.Visible = false;
        lblFaxsendmessage.Visible = false;
        lblFaxError.Visible = false;
        lblNoReadSMS.Visible = false;
        lblBroadcast.Visible = false;
        //SLine 2017 Enhancement for Office Location Starts
        if (!IsPostBack)
        {
            FillLocationMenu();
        }
        //SLine 2017 Enhancement for Office Location Ends
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["conString"], "SP_GetAllUnReadSMS", new object[] { });

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            lnkReadInBoundSMS.Enabled=true;
            lnkReadInBoundSMS.Text = "<blink> New InBound SMS(" + ds.Tables[0].Rows.Count + ")</blink>";
        }
        else
        {
            lnkReadInBoundSMS.Enabled = false;
            lnkReadInBoundSMS.Text = "New InBound SMS(0)";
        }
        //SLine 2017 Enhancement for Office Location Starts
        string path = HttpContext.Current.Request.Url.AbsolutePath;       
        if (path.Contains("NewLoad.aspx") || path.Contains("NewCarrier.aspx") || path.Contains("NewLocation.aspx") || path.Contains("NewShipping.aspx") || path.Contains("DisplayReport.aspx") || path.Contains("DisplayPLReport.aspx") || path.Contains("DisplayInvoiceProcessingReport.aspx") || path.Contains("DisplayInvoiceableLoadsReport.aspx") || path.Contains("ProfitLossCharts.aspx"))
        {
            ddlTopLocation.Enabled = false;
        }
        else
        {
            ddlTopLocation.Enabled = true;
        }
        bool AllOfficeLocChecked = false;
        if (Session["AllOfficeLocationChecked"] != null)
        {
            AllOfficeLocChecked = Convert.ToBoolean(Session["AllOfficeLocationChecked"]);
        }
        if (AllOfficeLocChecked || path.Contains("SearchCustomer.aspx") || path.Contains("NewCustomer.aspx") || path.Contains("AddContact.aspx") || path.Contains("SearchEquipment.aspx") || path.Contains("NewEquipment.aspx") || path.Contains("CustomerAccount.aspx"))
        {
            lblHeadLocation.Text = "All Office Locations";
        }
        else
        {
            lblHeadLocation.Text = ddlTopLocation.SelectedItem.Text;
        }
        if (path.Contains("TenderLoads.aspx") || path.Contains("TenderLoadDetails.aspx"))
        {
            lblHeadLocation.Text = "All Office Locations";
            divTopLocation.Visible = false;
        }
        else
            divTopLocation.Visible = true;
        if (Convert.ToBoolean(Session["IsAdmin"]))
        {
            spntunnel5.Style.Add(HtmlTextWriterStyle.Display, "");
            lnkOfficeLocation.Visible = true;
        }
        else
        {
            spntunnel5.Style.Add(HtmlTextWriterStyle.Display, "none");
            lnkOfficeLocation.Visible = false;
        }
        //SLine 2017 Enhancement for Office Location Ends
        CommonFunctions.InsertIntoInfoLog("Top.ascx :: Page_Load :: END");
    }

    private int GetTenderedLoadsCount()
    {
        CommonFunctions.InsertIntoInfoLog("Top.ascx :: GetTenderedLoadsCount :: Start");
        int count = 0;
        using (WebClient client = new WebClient())
        {
            string requestUrl = ConfigurationManager.AppSettings["PostmanURL"] + "api/GetTenderedLoadsCount/" + ConfigurationManager.AppSettings["PartnerCode"] + "/" + ConfigurationManager.AppSettings["PartnerLocationCode"] + "/" + ConfigurationManager.AppSettings["EDIToken"]; //"http://192.168.124.166/postman/api/GetTenderedLoads/";
            string json = client.DownloadString(requestUrl);
            count = (new JavaScriptSerializer()).Deserialize<int>(json);
        }
        CommonFunctions.InsertIntoInfoLog("Top.ascx :: GetTenderedLoadsCount :: END");
        return count;

    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        CommonFunctions.InsertIntoInfoLog("Top.ascx :: Page_PreRender :: Start");
        lblHeadText.Text = Convert.ToString(Session["TopHeaderText"]);
        if (string.Compare(lblHeadText.Text, "Update Load Status", true, System.Globalization.CultureInfo.CurrentCulture) == 0 || string.Compare(lblHeadText.Text, "Invoice", true, System.Globalization.CultureInfo.CurrentCulture) == 0 || string.Compare(lblHeadText.Text, "Accessorial Charges", true, System.Globalization.CultureInfo.CurrentCulture) == 0 )
        {
            pnlManager.Visible = false;
            pnlStaff.Visible = false;
            pnlDispatcher.Visible = false;
            lblUserId.Visible = false;
            lnlLogOut.Visible = false;
            lblWelcome.Visible = false;
        }
        else if (Session["Role"] != null)
        {
            if (string.Compare(Convert.ToString(Session["Role"]).Trim(), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                pnlManager.Visible = true;
                pnlStaff.Visible = false;
                pnlDispatcher.Visible = false;
                if (Session["IsAdmin"] != null && Convert.ToBoolean(Session["IsAdmin"]))
                {
                    lnkAppSettings.Visible = true;
                    image6.Visible = true;
                }
                else
                {
                    lnkAppSettings.Visible = false;
                    image6.Visible = false;
                }
                //new sms Menu (Madhav)
                lnkShowSendSms.Visible = true;
                spntunnel.Visible = true;
                lnkBroadcast.Visible = true;
                spntunnel3.Visible = true;
                //New inbound SMS (Raghu)
                lnkReadInBoundSMS.Visible = true;
                spntunnel2.Visible = true;
                lnkMistakesRpt.Visible = true;
                lnkShowSendFax.Visible = true;
                spntunnel4.Visible = true;
                if (ConfigurationManager.AppSettings["IsEdi"].ToString().ToLower()=="true")
                {
                    lnkTenderedLoads.Visible = true;
                    spntunnel6.Visible = true;
                    lnkTenderedLoads.Text = "Tendered Loads (" + GetTenderedLoadsCount() + ")";
                }
            }
            else if (string.Compare(Convert.ToString(Session["Role"]).Trim(), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                pnlDispatcher.Visible = true;
                pnlManager.Visible = false;
                pnlStaff.Visible = false;
                lnkAppSettings.Visible = false;
                //new sms Menu (Madhav)
                lnkShowSendSms.Visible = true;                
                spntunnel.Visible = false;
                lnkReadInBoundSMS.Visible = true;
                spntunnel2.Visible = true;
                lnkBroadcast.Visible = true;
                spntunnel3.Visible = true;
                lnkMistakesRpt.Visible = false;
                lnkShowSendFax.Visible = true;
                spntunnel4.Visible = true;
                lnkTenderedLoads.Visible = false;
                spntunnel6.Visible = false;
            }
            else if (string.Compare(Convert.ToString(Session["Role"]).Trim(), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                pnlStaff.Visible = true;
                pnlManager.Visible = false;
                pnlDispatcher.Visible = false;
                lnkAppSettings.Visible = false;
                //new sms Menu (Madhav)
                lnkShowSendSms.Visible = true;                
                spntunnel.Visible = false;
                lnkReadInBoundSMS.Visible = true;
                spntunnel2.Visible = true;
                lnkBroadcast.Visible = true;
                spntunnel3.Visible = true;
                lnkMistakesRpt.Visible = false;
                lnkShowSendFax.Visible = true;
                spntunnel4.Visible = true;
                if (ConfigurationManager.AppSettings["IsEdi"].ToString().ToLower() == "true")
                {
                    lnkTenderedLoads.Visible = true;
                    spntunnel6.Visible = true;
                    lnkTenderedLoads.Text = "Tendered Loads (" + GetTenderedLoadsCount() + ")";
                }
            }
        }
        bool isenableSMS = Convert.ToBoolean(ConfigurationSettings.AppSettings["AlowSendSMS"]);
        if (!isenableSMS)
        {
            lnkShowSendSms.Visible = false;
            spntunnel.Visible = false;
            lnkReadInBoundSMS.Visible = false;
            spntunnel2.Visible = false;
            lnkBroadcast.Visible = false;
            spntunnel3.Visible = false;
        }
        CommonFunctions.InsertIntoInfoLog("Top.ascx :: Page_PreRender :: END");
    }
    protected void lnlLogOut_Click(object sender, EventArgs e)
    {
        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_UpdateLogOutDate", new object[] { Convert.ToString(Session["LogsId"]) });
        FormsAuthentication.SignOut();
        Response.Cookies.Clear();
        Request.Cookies.Clear();
        Session.Clear();
        Session.Abandon();
        HttpContext.Current.Session.Abandon();
        HttpContext.Current.Response.Cookies.Clear();
        HttpContext.Current.Response.ContentType = "text/html";
        HttpContext.Current.Response.AddHeader("pragma", "no-cache");
        HttpContext.Current.Response.AddHeader("cache-control", "private, no-cache, must-revalidate");
        Response.Redirect("~/Login.aspx");
    }
    //Sms Functions region (Madhav)
    #region SmsDetails
    //new sms Menu (Madhav)
    protected void lnkShowSendSms_click(object sender, EventArgs e)
    {
        trReadInBoundSMS.Style.Clear();
        trReadInBoundSMS.Style.Add(HtmlTextWriterStyle.Display, "none");
        trBroadcast.Style.Clear();
        trBroadcast.Style.Add(HtmlTextWriterStyle.Display, "none");
        trSendFax.Style.Clear();
        trSendFax.Style.Add(HtmlTextWriterStyle.Display, "none");
        clearfealds(0);
        FillDrivers();
        hfealimaxcount.Value = ConfigurationSettings.AppSettings["TextMessageLength"];
        trsendsmsforothere.Style.Clear();
        trsendsmsforothere.Style.Add(HtmlTextWriterStyle.Display, "");
        ddlsendertype.SelectedIndex = 0;
    }   

    protected void lnkBroadcast_click(object sender, EventArgs e)
    {
        trReadInBoundSMS.Style.Clear();
        trReadInBoundSMS.Style.Add(HtmlTextWriterStyle.Display, "none");
        trsendsmsforothere.Style.Clear();
        trsendsmsforothere.Style.Add(HtmlTextWriterStyle.Display, "none");
        trSendFax.Style.Clear();
        trSendFax.Style.Add(HtmlTextWriterStyle.Display, "none");
        hfealimaxcount.Value = ConfigurationSettings.AppSettings["TextMessageLength"];
        trBroadcast.Style.Clear();
        trBroadcast.Style.Add(HtmlTextWriterStyle.Display, "");
        FillDriversForBroadcast();
        txtNames.Text = "";
        txtBroadcast.Text = "";
    }

    private void FillDriversForBroadcast()
    {
        ltbDrivers.Items.Clear();
        //dt = DBClass.returnDataTable("select bint_EmployeeId as 'Id',nvar_NickName as 'Name' from eTn_Employee where bint_RoleId=(select R.bint_RoleId from eTn_Role R where lower(nvar_RoleName)='driver') order by [Name]");
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetActiveDriverNames");
        if (ds.Tables.Count > 0)
        {
            for(int i=0;i<ds.Tables[0].Rows.Count; i++)
            {
                string[] number = ds.Tables[0].Rows[i]["Number"].ToString().Split('_');
                if (number[0].Length < 10)
                {
                    ds.Tables[0].Rows.RemoveAt(i);
                    i=i-1;
                }
            }
            ltbDrivers.DataSource = ds.Tables[0];
            ltbDrivers.DataTextField = "Name";
            ltbDrivers.DataValueField = "Number";
            ltbDrivers.DataBind();
            
        }
        if (ds != null) { ds.Dispose(); ds = null; }
    }

    private void FillDrivers()
    {
        ddlDriver.Items.Clear();
        //dt = DBClass.returnDataTable("select bint_EmployeeId as 'Id',nvar_NickName as 'Name' from eTn_Employee where bint_RoleId=(select R.bint_RoleId from eTn_Role R where lower(nvar_RoleName)='driver') order by [Name]");
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetActiveDriverNames");
        if (ds.Tables.Count > 0)
        {
            ddlDriver.DataSource = ds.Tables[0];
            ddlDriver.DataTextField = "Name";
            ddlDriver.DataValueField = "Number";
            ddlDriver.DataBind();           

            trdriverselection.Style.Clear();
            trdriverselection.Style.Add(HtmlTextWriterStyle.Display, "");
            trdriverselection.Style.Add("font-family", "Arial");
            trdriverselection.Style.Add("color", "#555");
            trdriverselection.Style.Add("font-size", "12px");
            trdriverselection.Style.Add("margin-left", "5px");
        }
        else
        {
            trdriverselection.Style.Clear();
            trdriverselection.Style.Add(HtmlTextWriterStyle.Display, "none");
            trdriverselection.Style.Add("font-family", "Arial");
            trdriverselection.Style.Add("color", "#555");
            trdriverselection.Style.Add("font-size", "12px");
            trdriverselection.Style.Add("margin-left", "5px");
        }
        ddlDriver.Items.Insert(0, "-- Select a Driver --");
        ddlDriver.SelectedIndex = 0;
        if (ds != null) { ds.Dispose(); ds = null; }
    }

    protected void ddlDriver_SelectedIndexChanged(object sender, EventArgs e)
    {
        clearfealds(1);
        if (!string.IsNullOrEmpty(ddlDriver.SelectedValue) && ddlDriver.SelectedValue != "0" && ddlDriver.SelectedValue.ToString().Split('_')[0] != "0")
        {
            string[] number1 = ddlDriver.SelectedValue.ToString().Split('_');
            string number = number1[0].Substring(number1[0].Length - 10, 10);
            idPhoneNumber.Text = number;           
        }

        if (!string.IsNullOrEmpty(ddlDriver.SelectedValue) && ddlDriver.SelectedIndex != 0)
        {
             txtusername.Text = ddlDriver.SelectedItem.Text;
        }
        ddlsendertype.SelectedIndex = 1;
    }


    protected void lnkSmsCancel_click(object sender, EventArgs e)
    {
        trsendsmsforothere.Style.Clear();
        trsendsmsforothere.Style.Add(HtmlTextWriterStyle.Display, "none");
    }    

    protected void lnkBroadcastCancel_click(object sender, EventArgs e)
    {
        trBroadcast.Style.Clear();
        trBroadcast.Style.Add(HtmlTextWriterStyle.Display, "none");
        txtBroadcast.Text = string.Empty;
        ltbDrivers.SelectedIndex = -1;
    }

    protected void lnkBroadcastsms_click(object sender, EventArgs e)
    {
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        //SLine 2017 Enhancement for Office Location Ends
        string driversList = string.Empty;
        foreach (ListItem item in ltbDrivers.Items)
        {
            if (item.Selected)
            {
                string[] number1 = item.Value.Split('_');
                string number = number1[0].Substring(number1[0].Length - 10, 10);
                int userid = Convert.ToInt32(Session["UserLoginId"]);
                //SLine 2017 Enhancement for Office Location
                string error = CommonFunctions.SendSms(0, item.Text, ConfigurationSettings.AppSettings["CountryCode"].ToString() + number, "Driver", txtBroadcast.Text, userid, officeLocationId, false);
                if (!string.IsNullOrEmpty(error))
                {
                    driversList = driversList + (driversList == string.Empty ? string.Empty : ", ") + item.Text;
                }
            } 
        }
        if (driversList != string.Empty)
        {
            lblBrodcastError.Text ="Broadcast to " +driversList+ " failed.";
            lblBrodcastError.Visible = true;
            spntunnel.Visible = true;
            txtNames.Text = driversList;
        }
        else
        {
            lblBrodcastError.Visible = false;
            lblBroadcast.Visible = true;
            spntunnel.Visible = true;
            trBroadcast.Style.Clear();
            trBroadcast.Style.Add(HtmlTextWriterStyle.Display, "none");
        }
        //ltbDrivers.SelectedIndex = -1;
    }

    protected void lnksendsms_click(object sender, EventArgs e)
    {
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        //SLine 2017 Enhancement for Office Location Ends
        int userid = Convert.ToInt32(Session["UserLoginId"]);
        //SLine 2017 Enhancement for Office Location
        string error = CommonFunctions.SendSms(0, txtusername.Text, (ddlCountry.SelectedValue + idPhoneNumber.Text), ddlsendertype.SelectedValue.ToString(), txtmessageforsms.Text, userid, officeLocationId,false);
        if (!string.IsNullOrEmpty(error))
        {
            lblErMessSms.Text = error;
            lblErMessSms.Visible = true;
            spntunnel.Visible = true;
        }
        else
        {
            lblErMessSms.Visible = false;
            lblSmssendmessage.Visible = true;
            spntunnel.Visible = true;
            trsendsmsforothere.Style.Clear();
            trsendsmsforothere.Style.Add(HtmlTextWriterStyle.Display, "none");
        }
    }

    protected void lnkclearsendsms_click(object sender, EventArgs e)
    {
        clearfealds(0);
    }

    protected void clearfealds(int type)
    {
        txtmessageforsms.Text = string.Empty;
        txtusername.Text = string.Empty;
        ddlCountry.SelectedIndex = 0;
        if (ddlDriver.Items != null && ddlDriver.Items.Count > 0 && type!=1)
            ddlDriver.SelectedIndex = 0;
        idPhoneNumber.ClearText();
    }

    #endregion

    #region Read InBound SMS

    protected void lnkReadInBoundSMS_click(object sender, EventArgs e)
    {
        trsendsmsforothere.Style.Clear();
        trsendsmsforothere.Style.Add(HtmlTextWriterStyle.Display, "none");
        trBroadcast.Style.Clear();
        trBroadcast.Style.Add(HtmlTextWriterStyle.Display, "none");
        trSendFax.Style.Clear();
        trSendFax.Style.Add(HtmlTextWriterStyle.Display, "none");
        FillReadSMSList();
    }

    protected void lnkMakeRead_click(object sender, EventArgs e)
    {
        Int64 smsId=Int64.Parse(((LinkButton)sender).CommandArgument);
        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_MakeSMSasRead", new object[] { smsId, Session["UserId"].ToString() });
        FillReadSMSList();
    }
    protected void lnkReply_click(object sender, EventArgs e)
    {
        Int64 smsId = Int64.Parse(((LinkButton)sender).CommandArgument);
        string number1 = ((LinkButton)sender).CommandName;
        string number = number1.Substring(number1.Length - 10, 10);
        ddlCountry.SelectedValue = number1.Substring(0, number1.Length - 10);
        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_MakeSMSasRead", new object[] { smsId, Session["UserId"].ToString() });



        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["conString"], "SP_GetAllUnReadSMS", new object[] { });

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            lnkReadInBoundSMS.Enabled = true;
            lnkReadInBoundSMS.Text = "<blink> New InBound SMS(" + ds.Tables[0].Rows.Count + ")</blink>";
        }
        else
        {
            lnkReadInBoundSMS.Enabled = false;
            lnkReadInBoundSMS.Text = "New InBound SMS(0)";
        }

        trsendsmsforothere.Style.Clear();
        trsendsmsforothere.Style.Add(HtmlTextWriterStyle.Display, "none");

        trReadInBoundSMS.Style.Clear();
        trReadInBoundSMS.Style.Add(HtmlTextWriterStyle.Display, "none");
        trSendFax.Style.Clear();
        trSendFax.Style.Add(HtmlTextWriterStyle.Display, "none");
        clearfealds(0);
        hfealimaxcount.Value = ConfigurationSettings.AppSettings["TextMessageLength"];
        trsendsmsforothere.Style.Clear();
        trsendsmsforothere.Style.Add(HtmlTextWriterStyle.Display, "");
        FillDrivers();
        idPhoneNumber.Text = number;
        
        
    }

    protected void btnReadSMSClose_click(object sender, EventArgs e)
    {
        trReadInBoundSMS.Style.Clear();
        trReadInBoundSMS.Style.Add(HtmlTextWriterStyle.Display, "none");
    }

    protected void dggrid_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        dggrid.CurrentPageIndex = e.NewPageIndex;
        FillReadSMSList();
    }

    private void FillReadSMSList()
    {
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["conString"], "SP_GetAllUnReadSMS", new object[] { });

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            dggrid.DataSource = ds.Tables[0];
            dggrid.DataBind();
            trReadInBoundSMS.Style.Clear();
            trReadInBoundSMS.Style.Add(HtmlTextWriterStyle.Display, "");
        }
        else
        {
            lblNoReadSMS.Visible = true;
            trReadInBoundSMS.Style.Clear();
            trReadInBoundSMS.Style.Add(HtmlTextWriterStyle.Display, "none");
        }
    }

    #endregion

    #region Old
    //ResolveUrl(
//        string strUserType = "";
//        string strCode = "";
//        if (!Page.IsPostBack)
//        {
//            plhImages.Controls.Clear();
//                if (strUserType.Trim().Length > 0)
//                {
                    
//                if (string.Compare(strUserType, "manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
//                {
//                    #region Manager
//                    strCode = @"<TD width=100%>&nbsp;</TD>
//                  <TD width=70><A 
//                    href=dashboard.aspx><IMG 
//                    id=image1 height=32 alt=Loads src=images/loads.gif width=70 
//                    border=0 name=image1></A></TD>
//                  <TD width=88><A 
//                    href=searchlocation.aspx><IMG 
//                    id=image2 height=32 alt=Locations src=images/locations.gif 
//                    width=88 border=0 name=image2></A></TD>
//                  <TD width=96><A 
//                    href=searchcustomer.aspx><IMG 
//                    id=image3 height=32 alt=Customers src=images/customers.gif 
//                    width=96 border=0 name=image3></A></TD>
//                  <TD width=79><A 
//                    href=searchcarrier.aspx><IMG 
//                    id=image5 height=32 alt=Carriers src=images/carriers.gif 
//                    width=79 border=0 name=image5></A></TD>
//                  <TD width=111><A 
//                    href=searchshipping.aspx><IMG 
//                    height=32 alt=Shippingline src=images/shippingline.gif 
//                    width=111 border=0></A></TD>
//                  <TD width=95><A 
//                    href=searchequipment.aspx><IMG 
//                    id=image4 height=32 alt=Equipment src=images/equipment.gif 
//                    width=95 border=0 name=image4></A></TD>
//                  <TD width=66><IMG id=image6  style=cursor:hand
//                    onmouseover=MM_showMenu(window.mm_menu_0406015752_0,0,32,null,'image6') 
//                    onmouseout=MM_startTimeout(); height=32 alt=Users 
//                    src=images/users.gif width=66 name=image6></TD>
//                  <TD width=78><A 
//                    href=reports.aspx><IMG 
//                    height=32 alt=Reports src=images/reports.gif width=78 
//                    border=0></A></TD>";
//                   plhImages.Controls.Add(new LiteralControl(strCode));
//#endregion
//                }
//                else if (string.Compare(strUserType, "customer", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
//                {
//                    #region customer
//                    strCode = @"<TD width=100%>&nbsp;</TD>
//                  <TD></TD>
//                  <TD></TD>
//                  <TD></TD>
//                  <TD></TD>
//                  <TD></TD>
//                  <TD width=122><A 
//                    href=customerpayments.aspx><IMG 
//                    id=image2 height=32 alt=Locations src=images/payments.gif
//                    width=122 height=32 border=0 name=image2></A></TD>
//                  <TD width=129><A 
//                    href=customertrackload.aspx><IMG 
//                    id=image3 height=32 alt=Customers src=images/trackload.gif
//                    width=129 height=32 border=0 name=image3></A></TD>
//                  <TD width=124><A 
//                    href=mailto:info@slinetransport.com ><IMG 
//                    id=image5 height=32 alt=Carriers src=images/sendmail.gif 
//                    width=124 height=32 border=0 name=image5></A></TD>";
//                    plhImages.Controls.Add(new LiteralControl(strCode));
//                    #endregion
//                }
//                else if (string.Compare(strUserType, "driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
//                {
//                    #region driver
//                    strCode = @"<TD width=100%>&nbsp;</TD>
//                  <TD></TD>
//                  <TD></TD>
//                  <TD></TD>
//                  <TD></TD>
//                  <TD></TD>
//                   <TD></TD>
//                  <TD width=70><A 
//                    href=driverload.aspx><IMG 
//                    id=image2 height=32 alt=Locations src=images/loads.gif
//                    width=70 height=32 border=0 name=image2></A></TD>
//                  <TD width=129><A 
//                    href=driverpayments.aspx><IMG 
//                    id=image3 height=32 alt=Customers src=images/payments.gif
//                    width=129 height=32 border=0 name=image3></A></TD>";
//                    plhImages.Controls.Add(new LiteralControl(strCode));
//                    #endregion
//                }
//                else if (string.Compare(strUserType, "carrier", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
//                {
//                    #region carrier
//                    strCode = @"<TD width=100%>&nbsp;</TD>
//                  <TD></TD>
//                  <TD></TD>
//                  <TD></TD>
//                  <TD></TD>
//                  <TD></TD>
//                   <TD></TD>
//                  <TD width=70><A 
//                    href=driverload.aspx><IMG 
//                    id=image2 height=32 alt=Locations src=images/loads.gif
//                    width=70 height=32 border=0 name=image2></A></TD>
//                  <TD width=129><A 
//                    href=driverpayments.aspx><IMG 
//                    id=image3 height=32 alt=Customers src=images/payments.gif
//                    width=129 height=32 border=0 name=image3></A></TD>";
//                    plhImages.Controls.Add(new LiteralControl(strCode));
//                    #endregion
//                }
//                else if (string.Compare(strUserType, "dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
//                {
//                    #region dispatcher
//                    strCode = @"<TD width=100%>&nbsp;</TD>
//                  <TD></TD>
//                  <TD></TD>
//                  <TD></TD>
//                   <TD width=70><A 
//                    href=dashboard.aspx><IMG 
//                    id=image1 height=32 alt=Loads src=images/loads.gif width=70 
//                    border=0 name=image1></A></TD>
//                  <TD width=88><A 
//                    href=searchlocation.aspx><IMG 
//                    id=image2 height=32 alt=Locations src=images/locations.gif 
//                    width=88 border=0 name=image2></A></TD>
//                  <TD width=96><A 
//                    href=searchcustomer.aspx><IMG 
//                    id=image3 height=32 alt=Customers src=images/customers.gif 
//                    width=96 border=0 name=image3></A></TD>
//                   <TD width=95><A 
//                    href=searchdrivers.aspx><IMG 
//                    height=32 alt=Reports src=images/drivers.gif width=95  
//                    border=0></A></TD>
//                  <TD width=95><A 
//                    href=searchequipment.aspx><IMG 
//                    id=image4 height=32 alt=Equipment src=images/equipment.gif 
//                    width=95 border=0 name=image4></A></TD>";
//                    plhImages.Controls.Add(new LiteralControl(strCode));
//                    #endregion
//                }
//            }
        //}
        #endregion

    #region Send FAX

    protected void lnkShowSendFax_click(object sender, EventArgs e)
    {
        trReadInBoundSMS.Style.Clear();
        trReadInBoundSMS.Style.Add(HtmlTextWriterStyle.Display, "none");
        trBroadcast.Style.Clear();
        trBroadcast.Style.Add(HtmlTextWriterStyle.Display, "none");
        trsendsmsforothere.Style.Clear();
        trsendsmsforothere.Style.Add(HtmlTextWriterStyle.Display, "none");
        trSendFax.Style.Clear();
        trSendFax.Style.Add(HtmlTextWriterStyle.Display, "");
        txtuUernameFax.Text = string.Empty;

    }

    protected void lnkSendFax_click(object sender, EventArgs e)
    {
        try
        {
            //SLine 2017 Enhancement for Office Location Starts
            long officeLocationId = 0;
            if (Session["OfficeLocationID"] != null)
            {
                officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
            }
            //SLine 2017 Enhancement for Office Location Ends
            string fileType = fldUpload.FileName.Substring(fldUpload.FileName.LastIndexOf('.') + 1, (fldUpload.FileName.Length - fldUpload.FileName.LastIndexOf('.') - 1));
            //SLine 2017 Enhancement for Office Location
            long status = CommonFunctions.SendFax(ddlCountryFax.SelectedValue + PhFax.Text, txtuUernameFax.Text, Int64.Parse(Session["UserLoginId"].ToString()), fldUpload.FileBytes, fileType, 0, fldUpload.FileName, officeLocationId);
           
            if (status > 0)
            {
                lblFaxError.Visible = false;
                lblFaxsendmessage.Visible = true;
                spntunnel.Visible = true;
                trSendFax.Style.Clear();
                trSendFax.Style.Add(HtmlTextWriterStyle.Display, "none");
            }
            else
            {
                lblFaxError.Text = "Fax sending failure. "+status.ToString();
                lblFaxError.Visible = true;
                spntunnel.Visible = true;
            }
        }
        catch (Exception ex)
        {
            lblFaxError.Text = "Fax sending failure. ";
            lblFaxError.Visible = true;
            spntunnel.Visible = true;
        }
    }

    protected void lnkCancelFax_click(object sender, EventArgs e)
    {
        trSendFax.Style.Clear();
        trSendFax.Style.Add(HtmlTextWriterStyle.Display, "none");
    }

    #endregion

    //SLine 2017 Enhancement for Office Location Starts
    protected void FillLocationMenu()
    {
        DataSet ds;
        if (Session["LocationDS"] == null)
        {
            long UserLoginId = (long)Session["UserLoginId"];
            ds = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["conString"], "Get_OfficeLocation_ForTopMenu", new object[] { UserLoginId });
        }
        else
        {
            ds = Session["LocationDS"] as DataSet;
        }
        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            ddlTopLocation.DataSource = ds.Tables[0];
            ddlTopLocation.DataTextField = "nvar_OfficeLocationName";
            ddlTopLocation.DataValueField = "bint_OfficeLocationId";
            ddlTopLocation.DataBind();
            if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
            {
                if (Session["OfficeLocationID"] == null)
                {
                    string selectedLocId = Convert.ToString(ds.Tables[1].Rows[0][0]);
                    Session["OfficeLocationID"] = selectedLocId;
                    ddlTopLocation.Items.FindByValue(selectedLocId).Selected = true;
                }
                else
                {
                    string selectedLocId = (string)Session["OfficeLocationID"];
                    ddlTopLocation.Items.FindByValue(selectedLocId).Selected = true;
                }
            }
        }
    }
    protected void lnkOfficeLocation_click(object sender, EventArgs e)
    {
        Response.Redirect("~/Manager/OfficeLocation.aspx");
    }

    protected void lnkTenderedLoads_click(object sender, EventArgs e)
    {
        Response.Redirect("~/Manager/TenderLoads.aspx");
    }
    //SLine 2017 Enhancement for Office Location Ends
    protected void lnkMistakes_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Manager/StaffMistakes.aspx");
    }

    protected void lnkMistakesRpt_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Manager/Mistakes.aspx");
    }

    protected void lnkChassisCharges_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Manager/ChassisCharges.aspx");
    }

    //SLine 2017 Enhancement for Office Location Starts
    protected void ddlTopLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["OfficeLocationID"] = ddlTopLocation.SelectedItem.Value;
        Response.Redirect(Request.RawUrl);
    }
    //SLine 2017 Enhancement for Office Location Ends
}
