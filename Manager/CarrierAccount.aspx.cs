using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CarrierAccount : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Carrier Accounts";
        Session["Type"] = "Carrier";
        if (!IsPostBack)
        {
            AssignListValues();
            if (ddlList.Items.Count > 0)
            {
                gridCurrent.Visible = true;
                gridCurrent.DeleteVisible = true;
                gridCurrent.EditVisible = true;
                gridCurrent.PageIndex = 0;
                gridCurrent.IsPagerVisible = true;
                gridCurrent.TableName = "eTn_Carrier";
                gridCurrent.Primarykey = "eTn_UserLogin.bint_UserLoginId";
                gridCurrent.PageSize = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["GeneralPageSize"]);
                gridCurrent.ColumnsList = "eTn_UserLogin.bint_UserLoginId;eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName  As 'Name';" +
                    "eTn_UserLogin.nvar_UserId;eTn_UserLogin.nvar_Password;eTn_Role.nvar_RoleName;" + CommonFunctions.DateQueryString("eTn_Carrier.date_CreatedDate", "Created");
                gridCurrent.VisibleColumnsList = "Name;nvar_RoleName;nvar_UserId;nvar_Password;Created";
                gridCurrent.VisibleHeadersList = "Name;Role;User Id;Password;Created";
                gridCurrent.DeleteTablesList = "eTn_UserLogin";
                gridCurrent.DeleteTablePKList = "bint_UserLoginId";
                gridCurrent.DeleteTableAddtionialWhereClauseList = "bint_RoleId=" + DBClass.executeScalar("select bint_RoleId from eTn_Role where Lower(nvar_RoleName)='carrier'");
                gridCurrent.InnerJoinClause = " inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_Carrier.bint_CarrierId " +
                    " inner join eTn_Role on eTn_Role.bint_RoleId = eTn_UserLogin.bint_RoleId ";
                gridCurrent.EditPage = "NewCarrierAccount.aspx";
                gridCurrent.BindGrid(0);
                if (Request.QueryString.Count > 0)
                {
                    ddlList.SelectedIndex = ddlList.Items.IndexOf(ddlList.Items.FindByValue(Convert.ToString(Request.QueryString[0])));
                }
                gridCurrent.WhereClause = "eTn_Carrier.bint_CarrierId=" + Convert.ToString(ddlList.SelectedItem.Value).Trim() + " and " +
                    "eTn_UserLogin.bint_RoleId=" + DBClass.executeScalar("select bint_RoleId from eTn_Role where Lower(nvar_RoleName)='carrier'");
                if (Convert.ToInt32(ddlList.SelectedValue) == 0)
                {
                    barCurrent.PagesList = "";
                    barCurrent.LinksList = "";
                }
                else
                {
                    barCurrent.LinksList = "Add New Carrier Account";
                    barCurrent.PagesList = "NewCarrierAccount.aspx?" + Convert.ToString(ddlList.SelectedItem.Value).Trim() + "New";
                }
            }            
        }
    }
    protected void AssignListValues()
    {
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        //SLine 2017 Enhancement for Office Location Ends
        //ddlList.DataSource = DBClass.returnDataTable("Select bint_CarrierId,nvar_CarrierName from eTn_Carrier order by [nvar_CarrierName]");
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCarrierNames", new object[] { officeLocationId });
        if (ds.Tables.Count > 0)
        {
            ddlList.DataSource = ds.Tables[0];
            ddlList.DataValueField = "bint_CarrierId";
            ddlList.DataTextField = "nvar_CarrierName";
            ddlList.DataBind();
            ddlList.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    protected void ddlList_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["GridPageIndex"] = 0;
        if (string.Compare(ddlList.SelectedItem.Text, "--Select--", true, System.Globalization.CultureInfo.CurrentCulture) != 0)
        {
            gridCurrent.WhereClause = "eTn_Carrier.bint_CarrierId=" + Convert.ToString(ddlList.SelectedItem.Value).Trim() + " and " +
                    "eTn_UserLogin.bint_RoleId=" + DBClass.executeScalar("select bint_RoleId from eTn_Role where Lower(nvar_RoleName)='carrier'");
            gridCurrent.BindGrid(0);
            gridCurrent.Visible = true;
            barCurrent.LinksList = "Add Carrier Account";
            barCurrent.PagesList = "NewCarrierAccount.aspx?" + Convert.ToString(ddlList.SelectedItem.Value).Trim() + "New";
            barCurrent.AssignPagesToLinks();
        }
        else
        {
            gridCurrent.Visible = false;
            barCurrent.PagesList = "";
            barCurrent.LinksList = "";
            barCurrent.AssignPagesToLinks();
        }
    }
}
