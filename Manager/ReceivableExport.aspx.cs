using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;


public partial class Manager_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Receivable Export";
        if (!IsPostBack)
        {
            ViewState["bSelect"] = true;
            btnExport.OnClientClick = "return confirm('Are you sure you want to export the selected loads?');";
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (ViewState["FromDate"] != null)
            dtpFromDate.SetDate(Convert.ToString(ViewState["FromDate"]));
        if(ViewState["ToDate"]!=null)
            dtpToDate.SetDate(Convert.ToString(ViewState["ToDate"]));

        for (int index = 0; index < gvReceiable.Rows.Count; index++)
        {            
            if (index % 2 == 0)
            {
                for (int k = 0; k < gvReceiable.Columns.Count; k++)
                {
                    gvReceiable.HeaderRow.Cells[k].CssClass = "GridHeader";
                    gvReceiable.Rows[index].Cells[k].CssClass = "GridItem";
                }
            }
            else
            {
                for (int k = 0; k < gvReceiable.Columns.Count; k++)
                {
                    gvReceiable.Rows[index].Cells[k].CssClass = "GridAltItem";
                }
            }
        }
             
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        LinkButton lnk = (LinkButton)e.CommandSource;
        if (lnk.Text == "Select" && Convert.ToBoolean(ViewState["bSelect"]))
        {
            foreach (GridViewRow gridrow in gvReceiable.Rows)
            {
                ((System.Web.UI.WebControls.CheckBox)gvReceiable.Rows[gridrow.RowIndex].Cells[0].FindControl("ChkLoadID")).Checked = true;
            }
            ViewState["bSelect"] = false;
            lnk.Text = "Un Select";
        }
        else
        {
            foreach (GridViewRow gridrow in gvReceiable.Rows)
            {
                ((System.Web.UI.WebControls.CheckBox)gvReceiable.Rows[gridrow.RowIndex].Cells[0].FindControl("ChkLoadID")).Checked = false;
            }
            ViewState["Text"] = "Select";
            ViewState["bSelect"] = true;
            lnk.Text = "Select";
        }
    }

    protected void OnLoadChecked(object sender, EventArgs e)
    {
        bool bTrue = false;        
        foreach (GridViewRow gridrow in gvReceiable.Rows)
        {
            if (((System.Web.UI.WebControls.CheckBox)gvReceiable.Rows[gridrow.RowIndex].Cells[0].FindControl("ChkLoadID")).Checked)
            {
                bTrue = true;                
            }
        }
        if (bTrue)
            btnExport.Visible = true;
        else
            btnExport.Visible = false;
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {        
        ViewState["FromDate"]=dtpFromDate.Date;
        ViewState["ToDate"] = dtpToDate.Date;
        BindDatatoGridView();
        this.pnlCreateFile.Visible = false;
    }

    private void BindDatatoGridView()
    {
        try
        {
            //SLine 2017 Enhancement for Office Location Starts
            long officeLocationId = 0;
            if (Session["OfficeLocationID"] != null)
            {
                officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
            }
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetExportReceivableEntries", new object[] { dtpFromDate.Date, dtpToDate.Date, officeLocationId });
            //SLine 2017 Enhancement for Office Location Ends
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvReceiable.DataSource = ViewState["tblReceivable"] = ds.Tables[0];
                    gvReceiable.DataBind();
                    gvReceiable.Visible = true;
                    lblError.Visible = false;
                    if (ViewState["bExport"] != null)
                        this.pnlCreateFile.Visible = true;
                }
                else
                {
                    if (ViewState["bExport"] != null)
                        this.pnlCreateFile.Visible = true;
                    this.lblError.Visible = false;
                    gvReceiable.Visible = false;
                }
            }
            else
                lblError.Visible = true;
            if (ds != null) { ds.Dispose(); ds = null; }
        }
        catch
        { }
    }

    private bool ExportReceivablefile(DataTable dt)
    {
        string qbendTransHeader = string.Empty;
        string qbsplheader1;
        string qbtran;
        string qbspl1;
        string qbendtran1;
        string qbTransHeader;
        string fname = string.Empty;
        bool bdownoad = true;

       
        ViewState["FileName"] = Convert.ToString(Session["UserId"]) + "_" + CreateFileName();

        System.Text.StringBuilder objBulider = new System.Text.StringBuilder();
        StreamWriter objSR1=null;
        try
        {
            FileInfo fileinfo = new FileInfo(Server.MapPath("~/ExportFiles") + "\\" + Convert.ToString(ViewState["FileName"]));            
            objSR1 = File.CreateText(fileinfo.FullName);            
            
            qbTransHeader = "!TRNS" + "\t" + "TRNSID" + "\t" + "TRNSTYPE" + "\t" + "DATE" + "\t" + "ACCNT" + "\t" + "NAME" + "\t" + "AMOUNT" + "\t" + "DOCNUM" + "\t" + "MEMO" + "\t" + "CLEAR" + "\t" + "TOPRINT" + "\t" + "ADDR1" + "\t" + "ADDR2" + "\t" + "ADDR3" + "\t" + "ADDR4" + "\t"+"PONUM" + "\t" + "TERMS" + "\t" + "FOB" + "\t" + "SADDR1" + "\t" + "SADDR2" + "\t" + "SADDR3";
            qbsplheader1 = "!SPL" + "\t" + "SPLID" + "\t" + "TRNSTYPE" + "\t" + "DATE" + "\t" + "ACCNT" + "\t" + "INVITEM" + "\t" + "AMOUNT" + "\t" + "DOCNUM" + "\t" + "MEMO" + "\t" + "QNTY" + "\t" + "PRICE";
            qbendTransHeader = "!ENDTRNS" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
            objSR1.WriteLine(qbTransHeader);
            objSR1.WriteLine(qbsplheader1);
            objSR1.WriteLine(qbendTransHeader);

            qbendtran1 = "ENDTRNS" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow Drow in dt.Rows)
                {
                    //!TRNS	TRNSID	TRNSTYPE	DATE	ACCNT	NAME	AMOUNT	DOCNUM	MEMO	
                    //CLEAR	TOPRINT	ADDR1	ADDR2	ADDR3	PONUM	TERMS	FOB	SADDR1	SADDR2	SADDR3
                    objBulider.Append("TRNS" + "\t" + "" + "\t" + "INVOICE" + "\t" + Convert.ToString(Drow["CurrentDate"]) + "\t" + "Accounts Receivable" + "\t");
                    objBulider.Append(Convert.ToString(Drow["CustomerName"]) + "\t" + Convert.ToString(Drow["NetAmount"]) + "\t" + Convert.ToString(Drow["LoadId"]) + "\t");
                    objBulider.Append("" + "\t" + "N" + "\t" + "Y" + "\t" + Convert.ToString(Drow["Addr1"]) + "\t" + Convert.ToString(Drow["Addr2"]) + "\t" + Convert.ToString(Drow["Addr3"]) + "\t" + Convert.ToString(Drow["Addr4"]) + "\t");
                    objBulider.Append(Convert.ToString(Drow["RefNo"]) + "\t" + "NET 15" + "\t" + Convert.ToString(Drow["Container"]) + "\t" + Convert.ToString(Drow["SAddr1"]) + "\t" + Convert.ToString(Drow["SAddr2"]) + "\t" + Convert.ToString(Drow["SAddr3"]));
                    objSR1.WriteLine(objBulider.ToString());
                    objBulider.Remove(0, objBulider.Length);

                    //!SPL	SPLID	TRNSTYPE	DATE	ACCNT	INVITEM	AMOUNT	DOCNUM	MEMO	QNTY	PRICE
                    if (Convert.ToDouble(Drow["Charges"]) > 0)
                    {
                        qbspl1 = "SPL" + "\t" + "" + "\t" + "INVOICE" + "\t" + Convert.ToString(Drow["CurrentDate"]) + "\t" + "Income Account" + "\t" + "PICKUP" + "\t" + "-" + Convert.ToString(Drow["Charges"]) + "\t" + Convert.ToString(Drow["LoadId"]) + "\t";
                        qbspl1 = qbspl1 + Convert.ToString(Drow["Origin"]) + " To " + Convert.ToString(Drow["Destination"]) + "\t" + "-1" + "\t" + Convert.ToString(Drow["Charges"]);
                        objSR1.WriteLine(qbspl1);
                        qbspl1 = string.Empty;
                    }

                    //Fuel Surcharge
                    if (Convert.ToDouble(Drow["FuelSurcharges"]) > 0)
                    {
                        qbspl1 = "SPL" + "\t" + "" + "\t" + "INVOICE" + "\t" + Convert.ToString(Drow["CurrentDate"]) + "\t" + "Income Account" + "\t" + "Fuel Surcharge" + "\t" + "-" + Convert.ToString(Drow["FuelSurcharges"]) + "\t" + Convert.ToString(Drow["LoadId"]) + "\t";
                        qbspl1 = qbspl1 + "FUEL SURCHARGE" + "\t" + "-1" + "\t" + Convert.ToString(Drow["FuelSurcharges"]);
                        objSR1.WriteLine(qbspl1);
                        qbspl1 = string.Empty;
                    }

                    //EXTRA Charges
                    if (Convert.ToDouble(Drow["ExtraCharges"]) > 0)
                    {
                        qbspl1 = "SPL" + "\t" + "" + "\t" + "INVOICE" + "\t" + Convert.ToString(Drow["CurrentDate"]) + "\t" + "Income Account" + "\t" + "EXTRA" + "\t" + "-"+Convert.ToString(Drow["ExtraCharges"]) + "\t" + Convert.ToString(Drow["LoadId"]) + "\t";
                        qbspl1 = qbspl1 + "EXTRA CHARGES" + "\t" + "-1" + "\t" + Convert.ToString(Drow["ExtraCharges"]);
                        objSR1.WriteLine(qbspl1);
                        qbspl1 = string.Empty;
                    }
                    objSR1.WriteLine(qbendtran1);
                }
                this.lnkDownload.Visible = true;
            }
            objSR1.Close();
            SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_AddExportFiles", new object[] { fileinfo.Name,txtNote.Text,"R"});
            fileinfo = null;
        }
        catch(Exception ex)
        {
            bdownoad = false;           
        }
        return bdownoad;
    }
    private string CreateFileName()
    {
        string filename = "INV_" + Convert.ToDateTime(DateTime.Now).ToString("MMddyy") + "_" + (DateTime.Now.Hour.ToString().Length == 1 ? "0" + DateTime.Now.Hour.ToString() : DateTime.Now.Hour.ToString()) + (DateTime.Now.Minute.ToString().Length == 1 ? "0" + DateTime.Now.Minute.ToString() : DateTime.Now.Minute.ToString()) + ".iif";
        return filename;
    }
    private void Downoloadfile(string filename,string name)
    {
        try
        {
            FileInfo fileinfo = new FileInfo(filename);
            if (fileinfo.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileinfo.Name);
                Response.AddHeader("Content-Length", fileinfo.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(fileinfo.FullName);
                Response.End();
            }
            else
            {
                Response.Write("<script>alert('file doesnot exist.');<script>");
            }
            fileinfo = null;
        }
        catch
        { }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        bool bTrue = false;
        foreach (GridViewRow gridrow in gvReceiable.Rows)
        {
            if (((System.Web.UI.WebControls.CheckBox)gvReceiable.Rows[gridrow.RowIndex].Cells[0].FindControl("ChkLoadID")).Checked)
            {
                bTrue = true;
                break;
            }
        }
        if (bTrue)
        {
            ViewState["bExport"] = true;
            if (ViewState["tblReceivable"] != null)
            {
                DataTable table = (DataTable)ViewState["tblReceivable"];
                DataTable dtTemp = table.Clone();
                foreach (GridViewRow gridrow in gvReceiable.Rows)
                {
                    if (((System.Web.UI.WebControls.CheckBox)gvReceiable.Rows[gridrow.RowIndex].Cells[0].FindControl("ChkLoadID")).Checked)
                    {
                        DataRow drow = dtTemp.NewRow();
                        drow.ItemArray = table.Rows[gridrow.RowIndex].ItemArray;
                        dtTemp.Rows.Add(drow);
                        drow = null;
                    }
                }
                if (ExportReceivablefile(dtTemp))
                {
                    String strLoads = string.Empty;
                    foreach (DataRow drow in dtTemp.Rows)
                    {
                        strLoads += drow["LoadId"].ToString() + ";";
                    }
                    if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Update_ReceivablesStatus", new object[] { strLoads }) > 0)
                    {
                        gvReceiable.Visible = false;
                        BindDatatoGridView();
                    }
                }
                if (table != null) { table.Dispose(); table = null; }
                if (dtTemp != null) { dtTemp.Dispose(); dtTemp = null; }
            }
        }
        else
        {
            Response.Write("<script>alert('Please select load(s).')</script>");
            return;
        }
    }
    protected void lnkDownload_Click(object sender, EventArgs e)
    {
        ViewState["bExport"] = null;
        if (ViewState["FileName"] == null)
        {
            System.IO.DirectoryInfo objinfo = new DirectoryInfo(Server.MapPath("~/ExportFiles"));
            System.IO.FileInfo[] fileinfo = objinfo.GetFiles();
            for (int i = 0; i < fileinfo.Length; i++)
            {
                string[] strUser = fileinfo[i].Name.Split('_');
                if (Convert.ToString(Session["UserId"]).Trim().Contains(strUser[0]))
                {
                    ViewState["bExport"] = null;
                    Downoloadfile(fileinfo[i].FullName, fileinfo[i].Name);
                    break;
                }
            }
            objinfo = null; fileinfo = null;
            lnkDownload.Visible = false;
        }
        else
        {
            FileInfo file= new FileInfo(Server.MapPath("~/ExportFiles") + "\\" + Convert.ToString(ViewState["FileName"]));
            if (file.Exists)
            {
                Downoloadfile(Server.MapPath("~/ExportFiles") + "\\" + Convert.ToString(ViewState["FileName"]), Convert.ToString(ViewState["FileName"]));
            }
            else
            {
                System.IO.DirectoryInfo objinfo = new DirectoryInfo(Server.MapPath("~/ExportFiles"));
                FileInfo[] fileinfo = objinfo.GetFiles();
                for (int i = 0; i < fileinfo.Length; i++)
                {
                    if (fileinfo[i].Name == Convert.ToString(ViewState["FileName"]))
                    {
                        string[] strUser = fileinfo[i].Name.Split('_');
                        if (Convert.ToString(Session["UserId"]).Trim().Contains(strUser[0]))
                        {
                            ViewState["bExport"] = null;
                            Downoloadfile(fileinfo[i].FullName, fileinfo[i].Name);
                            break;
                        }
                    }
                }
                objinfo = null; fileinfo = null;
                lnkDownload.Visible = false;
            }
            file = null;
        }
    }
}
