﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Mobile_Login" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><asp:Literal ID="ltlcompanynameTitle" runat='server'></asp:Literal></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="css/jquery.mobile.css">
<script src="js/jquery-1.8.2.min.js"></script>
<script src="js/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#frmLogin").validate({
            submitHandler: function (form) {

            }
        });
        $("a[data-role=tab]").each(function () {
            var anchor = $(this);
            anchor.bind("click", function () {
                $.mobile.changePage(anchor.attr("href"), {
                    transition: "none",
                    changeHash: false
                });
                return false;
            });
        });

        $("div[data-role=page]").bind("pagebeforeshow", function (e, data) {
            $.mobile.silentScroll(0);
            $.mobile.changePage.defaults.transition = 'slide';
        });
    });
 
 </script>
</head>
<body>
    <div class="wrapper">
  <div data-role="page" data-theme="b" id="login">
    <div data-theme="b" data-role="header" class="head"> <span class="ui-title">
      <div class="logo"><a href="#" rel="external"><img src="images/onlyLogo.png" width="51"  height="51" /> </a>
        <h1><asp:Literal ID="ltlcompanyname" runat='server'></asp:Literal></h1>
      </div>
      </span> </div>
      <div id="errorDiv" runat="server" visible="false" class="errormsg"></div>
    <div data-role="content">
      <form action="#" id="frmLogin" runat="server" class="validate" method="post"  data-transition="flip">
        <div data-role="fieldcontain">
          <label for="email"><em>* </em> User Name </label>
          <asp:TextBox ID="email" runat="server" CssClass="required email"></asp:TextBox>
        </div>
        <div data-role="fieldcontain">
          <label for="password"><em>* </em>Password: </label>
          <asp:TextBox ID="password" runat="server" CssClass="required" TextMode="Password"></asp:TextBox>
        </div>
        <asp:LinkButton ID="lnklogin" runat='server' rel="external"  data-role="button" data-rel="dialog" data-transition="flip" data-theme="b"
          data-icon="arrow-r"  data-iconpos="right" OnClick="lnklogin_click">Login</asp:LinkButton>
        <div id="checkboxes1" data-role="fieldcontain">
          <fieldset data-role="controlgroup" data-type="vertical" data-mini="true">
          <input id="checkbox1" runat='server' data-theme="h" type="checkbox" />
          <label for="checkbox1"> Remember Me </label>
          </fieldset>
        </div>       
      </form>
    </div>
    <div data-theme="f" data-role="footer" data-position="fixed"> <span class="ui-title"> <span class="footer">&copy; <asp:Literal ID="ltlcompanynameSub" runat='server'></asp:Literal> <asp:Literal ID="ltlYearloginfooter" runat="server"></asp:Literal></span> </span> </div>
  </div>
</div>
</body>
</html>

