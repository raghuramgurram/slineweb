<%@ Page AutoEventWireup="true" CodeFile="InBoundSMSLogs.aspx.cs" Inherits="InBoundSMSLogs" Language="C#"
    MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="tblform1">
                    <tr>
                        <td valign="middle">
                            <asp:Label ID="lblFromdate" runat="server" Text="From : "></asp:Label>
                            <uc1:DatePicker ID="dtpFromDate" runat="server" IsDefault="false" IsRequired="false" CountNextYears="2" CountPreviousYears="0" SetInitialDate="true" />
                            &nbsp; &nbsp;&nbsp; &nbsp;<asp:Label ID="lblToDate" runat="server" Text="To : "></asp:Label>
                            <uc1:DatePicker ID="dtpToDate" runat="server" IsDefault="false" IsRequired="false" CountNextYears="2" CountPreviousYears="0" SetInitialDate="true" />
                            &nbsp; &nbsp;
                            <asp:Button ID="btnSearch" runat="server" CssClass="btnstyle" Text="Search" OnClick="btnSearch_Click" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle"></td>
                        <td>   
                        &nbsp;        
                        </td>
                    </tr>
                   
                    <tr><td colspan="2">
                      <asp:DataGrid AutoGenerateColumns="false" ID="dggrid" Width="100%" runat="server" AllowPaging="true" OnPageIndexChanged="dggrid_PageIndexChanged"  CssClass="Grid" PageSize="10" AllowCustomPaging="true">          
        <PagerStyle Mode="NumericPages" HorizontalAlign="Center"/>
        <ItemStyle CssClass="GridItem" BorderColor="White" />
        <HeaderStyle CssClass="GridHeader" BorderColor="White" />
        <AlternatingItemStyle CssClass="GridAltItem" />
        <Columns>
        <asp:BoundColumn HeaderText="Log ID" DataField="bint_inboundSMSLogID"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Message Id" DataField="nvar_MessageId"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="From #" DataField="nvar_From"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Message Text" DataField="nvar_Text" ItemStyle-Width="30%"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Status" DataField="Status"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Read By" DataField="nvar_ReadBy"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Message Date (UTC)" DataField="date_MessageDate"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Received Date" DataField="date_ReceivedDate"></asp:BoundColumn>      
        <asp:BoundColumn HeaderText="Read Date" DataField="date_ReadDate"></asp:BoundColumn>     
        </Columns>
    </asp:DataGrid>
                    </td></tr>
                    </table>
            </td>
        </tr>
    </table>   
   
</asp:Content>

