using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class SearchShipping : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Search Shipping";      
        if (!IsPostBack)
        {
            gridCurrent.Visible = true;
            gridCurrent.DeleteVisible = true;
            gridCurrent.EditVisible = true;
            gridCurrent.TableName = "eTn_Shipping";
            gridCurrent.Primarykey = "bint_ShippingId";
            gridCurrent.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
            gridCurrent.ColumnsList = "bint_ShippingId;nvar_ShippingLineName+' ('+ CONVERT(VARCHAR(5), sint_EmptyReturnFreeDays)+')' as Name;" +
                CommonFunctions.PhoneQueryString("num_Phone", "Phone") + ";" + CommonFunctions.PhoneQueryString("num_Fax", "Fax") + ";" + CommonFunctions.DateQueryString("date_CreatedDate", "Created");
            gridCurrent.VisibleColumnsList = "Name;Phone;Fax;Created";
            gridCurrent.VisibleHeadersList = "Name;Phone;Fax;Created";
            gridCurrent.DependentTablesList = "eTn_Load";
            gridCurrent.DependentTableKeysList = "bint_ShippingId";
            gridCurrent.DeleteTablesList = "eTn_ShippingContacts;eTn_Shipping";
            gridCurrent.DeleteTablePKList = "bint_ShippingId;bint_ShippingId";
            gridCurrent.EditPage = "~/Manager/NewShipping.aspx";
            gridCurrent.OrderByClause = "nvar_ShippingLineName";
            long officeLocationId = 0;
            //SLine 2017 Enhancement for Office Location Starts
            if (Session["OfficeLocationID"] != null)
            {
                officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
            }
            gridCurrent.WhereClause = "bit_Status=" + ddlList.SelectedValue + " and bint_OfficeLocationId=" + officeLocationId;
            //SLine 2017 Enhancement for Office Location Ends
            gridCurrent.IsPagerVisible = true;
            gridCurrent.BindGrid(0);
            
            lblShowError.Visible = false;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Session["GridPageIndex"] = 0;
        lblShowError.Visible = false;
        string strWhereClause = "bit_Status=" + ddlList.SelectedValue;
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        //SLine 2017 Enhancement for Office Location Ends
        if (txtName.Text.Trim().Length > 0)
        {
            //SLine 2017 Enhancement for Office Location
            strWhereClause += " and nvar_ShippingLineName like '" + txtName.Text.Trim() + "%' and bint_OfficeLocationId=" + officeLocationId;
            int iCount = Convert.ToInt32(DBClass.executeScalar("select count(bint_ShippingId) from eTn_Shipping where " + strWhereClause));
            if (iCount > 1)
            {
                gridCurrent.Visible = true;
                gridCurrent.WhereClause = strWhereClause;
                gridCurrent.BindGrid(0);
            }
            else if (iCount == 1)
            {
                Response.Redirect("~/Manager/NewShipping.aspx?" + DBClass.executeScalar("select [bint_ShippingId] from eTn_Shipping where " + strWhereClause));
            }
            else if (iCount == 0)
            {
                lblShowError.Visible = true;
                gridCurrent.Visible = false;
            }
        }
        else
        {
            gridCurrent.Visible = true;
            gridCurrent.WhereClause = strWhereClause;
            gridCurrent.BindGrid(0);
        }
    }   
}
