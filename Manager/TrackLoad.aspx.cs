using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Linq;

public partial class TrackLoad : System.Web.UI.Page
{
    const string BEFOREDELIVARYIDS = "'1','15','2','3','5','14','7'";
    const string BEFOREASSIGNED = "'1','15'";
    const string SWP = "Stay with Pick";
    const string SWD = "Stay with Delivery";
    const string DEP = "Drop Empty at Pick";
    const string PLP = "Pull Loaded from Pick";
    const string DLD = "Drop Loaded at Delivery";
    const string OD = "Origin Drayage";
    const string DD = "Destination Drayage";
    protected void Page_Load(object sender, EventArgs e)
    {
        CommonFunctions.InsertIntoInfoLog("TrackLoad.aspx :: Page_Load :: Start");
        Session["TopHeaderText"] = "Track Load";
        lblFaxError.Text = string.Empty;
        lblFaxError.Visible = false;
        if (Session["Role"] != null)
        {
            if (Request.QueryString.Count > 0)
            {
                ViewState["LoadID"] = Convert.ToInt64(Request.QueryString[0]);
                ViewState["ID"] = Convert.ToInt64(Request.QueryString[0]);
                Session["BackPage4"] = "TrackLoad.aspx?" + Convert.ToString(ViewState["ID"]);
                Session["BackPage3"] = "TrackLoad.aspx?" + Convert.ToString(ViewState["ID"]);
                lblLoadId.Text = Convert.ToString(ViewState["ID"]);
                LinksBar.LinkLog = "LoadLogs.aspx?" + Convert.ToString(ViewState["ID"]);
                FillDetails();

                //grid changes
                //Session["TopHeaderText"] = "Update Load Status";
                if (!Page.IsPostBack)
                {
                    long loadId = Convert.ToInt64(Request.QueryString[0]);
                    ViewState["PreviousPage"] = Request.UrlReferrer;//Saves the Previous page url in ViewState
                    if (Request.QueryString.Count > 0)
                    {
                        string strRes = DBClass.executeScalar("Select Count(bint_LoadId) from eTn_Load where bint_LoadId=" + loadId);
                        // Session["BackPage4"] = "~/Manager/UpdateLoadStatus.aspx?" + Request.QueryString[0].Trim();
                        if (strRes != null)
                        {
                            if (strRes.Trim().Length > 0 && Convert.ToInt64(strRes) > 0)
                            {
                                // GridBar1.HeaderText = "Update Load Status ( " + Request.QueryString[0].Trim() + " )";
                                DataSet ds1 = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadTypeAndContainerNumber", new object[] { loadId });
                                if (ds1.Tables.Count != null && ds1.Tables[0].Rows.Count > 0)
                                {
                                    // GridBar1.HeaderRightText = (ds.Tables[0].Rows[0].ItemArray[0].ToString().Trim() + " : " + ds.Tables[0].Rows[0].ItemArray[1] + ((ds.Tables[0].Rows[0].ItemArray[2] != null && !string.IsNullOrEmpty(ds.Tables[0].Rows[0].ItemArray[2].ToString().Trim())) ? " - " : string.Empty) + ds.Tables[0].Rows[0].ItemArray[2].ToString().Trim());
                                    // GridBar1.HeaderRightTextVisible = true;
                                    ViewState["SendMails"] = bool.Parse(ds1.Tables[0].Rows[0][4].ToString());
                                }
                                //GridBar1.LinksList = "Upload Documents";
                                //GridBar1.PagesList = "UpLoadDocuments.aspx?" + Request.QueryString[0].Trim();
                                ViewState["ID"] = loadId;
                            }
                        }
                    }
                    FillDetailsLoadStatus(Convert.ToInt64(ViewState["ID"]));
                    FillEdiLoadStatusGrid(Convert.ToInt64(ViewState["ID"]));

                    ShowHideGrids();
                    CheckIsVerified();
                }
            }
        }
        CommonFunctions.InsertIntoInfoLog("TrackLoad.aspx :: Page_Load :: END");
    }

    private void FillEdiLoadStatusGrid(long LoadId)
    {
        string LoadType = "No Load Type";
        if (ViewState["IsEdi"] != null && bool.Parse(ViewState["IsEdi"].ToString()) && ViewState["LoadType"] != null && !string.IsNullOrWhiteSpace(ViewState["LoadType"].ToString()))
        {
            List<string> LoadTypeList = ViewState["LoadType"].ToString().Split('~').ToList();
            if (ViewState["LoadType"].ToString().ToLower().Contains(SWP.ToLower()))
                LoadType = SWP;
            else if (ViewState["LoadType"].ToString().ToLower().Contains(SWD.ToLower()))
                LoadType = SWD;
            else if (ViewState["LoadType"].ToString().ToLower().Contains(DEP.ToLower()))
                LoadType = DEP;
            else if (ViewState["LoadType"].ToString().ToLower().Contains(PLP.ToLower()))
                LoadType = PLP;
            else if (ViewState["LoadType"].ToString().ToLower().Contains(DLD.ToLower()))
                LoadType = DLD;
            else if (ViewState["LoadType"].ToString().ToLower().Contains(OD.ToLower()))
                LoadType = OD;
            else if (ViewState["LoadType"].ToString().ToLower().Contains(DD.ToLower()))
                LoadType = DD;
            lblloadType.Text = LoadTypeList.Where(x => x.ToLower().Contains(LoadType.ToLower())).FirstOrDefault();
            lblEdiLoadType.Text = "- EDI - " + lblloadType.Text;
            List<EdiLaodDetails> ediLaodDetails = new List<EdiLaodDetails>();
            List<EDIStatusDetails> eDIStatusDetails = new List<EDIStatusDetails>();
            List<ValueText> validStatusList = new List<ValueText>();
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetEdiTrackLoadDetails", new object[] { LoadId, LoadType });
            if (ds != null && ds.Tables != null && ds.Tables.Count > 2 && ds.Tables[1].Rows != null && ds.Tables[1].Rows.Count > 1 && ViewState["LoadState"].ToString() != "New")
            {
                pnlEdiGrid.Visible = true;
                if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
                    foreach (DataRow row in ds.Tables[0].Rows)
                        validStatusList.Add(new ValueText { Code = row["nvar_EDIStatusCode"].ToString(), Value = row["bint_EDIStatusId"].ToString(), Text = row["nvar_EDIStatusDescription"].ToString() });
                ViewState["ValidStatusList"] = validStatusList;
                if (ds.Tables[1].Rows != null && ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        EdiLaodDetails ediLaodDetailsItem = new EdiLaodDetails();
                        ediLaodDetailsItem.LocationID = long.Parse(row["LocationId"].ToString());
                        ediLaodDetailsItem.Location = row["Location"].ToString();
                        ediLaodDetailsItem.IsOrigin = row["IsOrigin"].ToString() == "1" ? true : false;
                        ediLaodDetailsItem.StopNumber = row["int_SequenceNumber"].ToString();
                        ediLaodDetailsItem.StateCode = row["nvar_State"].ToString();
                        ediLaodDetailsItem.City = row["nvar_City"].ToString();
                        ediLaodDetailsItem.EDIStatusDetailsList = new List<EDIStatusDetails>();
                        ediLaodDetails.Add(ediLaodDetailsItem);
                    }
                }
                if (ds.Tables[2].Rows != null && ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[2].Rows)
                    {
                        EDIStatusDetails EDIStatusDetailsItem = new EDIStatusDetails();
                        //EDIStatusDetailsItem.LocationID = long.Parse(row["LocationId"].ToString());
                        //EDIStatusDetailsItem.Location = row["Location"].ToString();
                        //EDIStatusDetailsItem.IsOrigin = bool.Parse(row["IsOrigin"].ToString());
                        //EDIStatusDetailsItem.StopNumber = row["int_SequenceNumber"].ToString();
                        //EDIStatusDetailsItem.StateCode = row["nvar_State"].ToString();
                        //EDIStatusDetailsItem.City = row["nvar_City"].ToString();
                        EDIStatusDetailsItem.EDITrackLoadId = long.Parse(row["bint_EDITrackLoadId"].ToString());
                        EDIStatusDetailsItem.EDIStatusID = long.Parse(row["bint_EDILoadStatusId"].ToString());
                        EDIStatusDetailsItem.LocationID = long.Parse(row["bint_LocationId"].ToString());
                        EDIStatusDetailsItem.Name = row["Name"].ToString();
                        EDIStatusDetailsItem.Status = row["nvar_EDIStatusDescription"].ToString();
                        EDIStatusDetailsItem.StatusTime = DateTime.Parse(row["date_CreateDate"].ToString());
                        EDIStatusDetailsItem.Comment = row["nvar_Notes"].ToString();
                        EDIStatusDetailsItem.IsOld = true;
                        EDIStatusDetailsItem.IsNew = false;
                        EDIStatusDetailsItem.DDlSource = new List<ValueText>();
                        eDIStatusDetails.Add(EDIStatusDetailsItem);
                    }
                }
                if (ediLaodDetails.Count > 0)
                {
                    foreach (EdiLaodDetails item in ediLaodDetails)
                    {
                        List<EDIStatusDetails> eDIStatusDetailsLoc = new List<EDIStatusDetails>();
                        List<ValueText> validStatusListLoc = new List<ValueText>();
                        eDIStatusDetailsLoc = eDIStatusDetails.Where(x => x.LocationID == item.LocationID).ToList();
                        if (validStatusList.Count > eDIStatusDetailsLoc.Count())
                        {
                            foreach (ValueText vItem in validStatusList)
                            {
                                if (eDIStatusDetailsLoc.Where(x => x.Status == vItem.Text).Count() == 0)
                                {
                                    validStatusListLoc.Add(vItem);
                                }
                            }
                            EDIStatusDetails EDIStatusDetailsempty = new EDIStatusDetails();
                            EDIStatusDetailsempty.City = item.City;
                            EDIStatusDetailsempty.IsNew = true;
                            EDIStatusDetailsempty.IsOld = false;
                            EDIStatusDetailsempty.StateCode = item.StateCode;
                            EDIStatusDetailsempty.StopNumber = item.StopNumber;
                            EDIStatusDetailsempty.LocationID = item.LocationID;
                            EDIStatusDetailsempty.IsOrigin = EDIStatusDetailsempty.IsOrigin;
                            EDIStatusDetailsempty.DDlSource = validStatusListLoc;
                            eDIStatusDetailsLoc.Insert(0, EDIStatusDetailsempty);
                        }
                        item.EDIStatusDetailsList = eDIStatusDetailsLoc;
                    }

                }
                dgrEdiTrackLoad.DataSource = ediLaodDetails;
                dgrEdiTrackLoad.DataBind();
            }
            else
                pnlEdiGrid.Visible = false;
        }
        
    }
    protected void dgrEdiLoad_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (string.Compare(e.CommandName, "ADD", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            if (!Page.IsValid)
                return;
            if (Page.IsPostBack)
            {
                DatePicker Dpicker = (DatePicker)e.Item.FindControl("txtDate");
                TimePicker Tpicker = (TimePicker)e.Item.FindControl("txtTime");
                if (!Dpicker.IsValidDate)
                    return;
                DataGrid dg = (DataGrid)source;
                Label lblLocationId = (Label)e.Item.FindControl("lblLocationId");
                if (dg.Items.Count > 1)
                {
                    Label lbl1 = (Label)dg.Items[1].FindControl("lblID");

                    string lastDate = DBClass.executeScalar("Select top 1 date_CreateDate from eTn_EDITrackLoad where bint_LoadId = " +
                    "(select bint_LoadId from eTn_EDITrackLoad where bint_EDITrackLoadId = " + lbl1.Text.Trim() + ") and bint_LocationId="+lblLocationId.Text+" order by date_CreateDate desc ");
                    if (!string.IsNullOrWhiteSpace(lastDate))
                        if (Convert.ToDateTime(lastDate) >= Convert.ToDateTime(Dpicker.Date + " " + Tpicker.Time.Trim()))
                        {
                            Response.Write("<script>alert('Date entered must be greater than the date of previous status dates.')</script>");
                            return;
                        }

                }
                DropDownList dname = (DropDownList)e.Item.FindControl("dropEdiStatus");
                TextBox Cnotes = (TextBox)e.Item.FindControl("txtComments");
                
                Label lblCity = (Label)e.Item.FindControl("lblCity");
                Label lblState = (Label)e.Item.FindControl("lblState");
                Label lblIsOrgin = (Label)e.Item.FindControl("lblIsOrgin");
                Label lblStopNumber = (Label)e.Item.FindControl("lblStopNumber");
                DataSet ds = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["conString"], "SP_GetLoadDetailsforPostman", new object[] { ViewState["LoadID"].ToString() });
                if (ds.Tables != null && ds.Tables.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(ds.Tables[0].Rows[0][0].ToString()) && !string.IsNullOrWhiteSpace(ds.Tables[0].Rows[0][1].ToString()) && !string.IsNullOrWhiteSpace(ds.Tables[0].Rows[0][2].ToString()))
                    {
                        string CustName = ds.Tables[0].Rows[0][0].ToString();
                        string OrderNum = ds.Tables[0].Rows[0][1].ToString();
                        bool IsEdi = bool.Parse(ds.Tables[0].Rows[0][3].ToString());
                        string partner = ConfigurationManager.AppSettings["PartnerCode"];
                        List<ValueText> validStatusList = (List<ValueText>)(ViewState["ValidStatusList"]);
                        string StatusCode = string.Empty;
                        StatusCode = validStatusList.Where(x => x.Value == dname.SelectedValue).First().Code;
                        string StatusResonCode = "NA";
                        string CityName = lblCity.Text;
                        string StateorProvinceCode = lblState.Text;
                        int sequenceNumber =int.Parse(lblStopNumber.Text);
                        ChangeLoadStatus clsObj = new ChangeLoadStatus();
                        if (clsObj.SendShipmentStatus(CustName, partner, OrderNum, StatusCode, StatusResonCode, Convert.ToDateTime(Dpicker.Date + " " + Tpicker.Time.Trim()), CityName, StateorProvinceCode, sequenceNumber))
                            if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_InsertEdiTrackLoad", new object[] { ViewState["LoadID"].ToString(), dname.SelectedValue, Convert.ToDateTime(Dpicker.Date + " " + Tpicker.Time.Trim()), Cnotes.Text, string.Empty, Convert.ToInt64(Session["UserLoginId"]), lblLocationId.Text, bool.Parse(lblIsOrgin.Text) })>0)
                            {
                                Response.Redirect("~/Manager/dummy.aspx");
                                FillEdiLoadStatusGrid(Convert.ToInt64(ViewState["ID"]));
                            }
                            else
                                Response.Redirect("~/Manager/dummy.aspx");

                    }
                }
            }
        }
        if (string.Compare(e.CommandName, "Delete", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            if (Page.IsPostBack)
            {
                Label lbl = (Label)e.Item.FindControl("lblID");
                if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_DeleteEdiTrackLoad", new object[] { lbl.Text.Trim(), Convert.ToInt64(Session["UserLoginId"]) }) > 0)
                {
                    Response.Redirect("~/Manager/dummy.aspx");
                    FillEdiLoadStatusGrid(Convert.ToInt64(ViewState["ID"]));
                }
                //Sline Added Else con to check the reload, if the Delete is done from any other browser, and 
                //Null value insertion error occurs
                else
                    Response.Redirect("~/Manager/dummy.aspx");
            }
        }
    }
    private void CheckIsVerified()
    {
        if (Convert.ToString(Session["Role"]) == "Manager" || Convert.ToString(Session["Role"]) == "Staff")
        {
            divVerifiedChkBox.Visible = true;
            //SLine 2016 Added Checkbox to Document Section for Is Verified
            if (GetIsVerified())
            {
                chkIsVerified.Checked = true;
                //chkIsVerified.Enabled = false;
            }
            else
            {
                chkIsVerified.Checked = false;
                //chkIsVerified.Enabled = true;
            }
        }
        else
        {
            divVerifiedChkBox.Visible = false;
        }
    }
    protected void chkBookingProblem_CheckedChanged(object sender, EventArgs e)
    {
        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_UpdateLoadBookingProblem", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToString(Session["UserLoginId"]), !chkBookingProblem.Checked });
        FillDetails();
    }
    protected void chkPutonHold_CheckedChanged(object sender, EventArgs e)
    {
        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_UpdateLoadPutOnHold", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToString(Session["UserLoginId"]), !chkPutonHold.Checked });
        FillDetails();
    }
    #region SLine 2016 Enhancements
    //SLine 2016 Enhancements
    protected void chkChassisRent_CheckedChanged(object sender, EventArgs e)
    {
        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_UpdateLoadChassisRent", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToString(Session["UserLoginId"]), !chkChassisRent.Checked });
        FillDetails();
    }
    protected void chkExportLoad_CheckedChanged(object sender, EventArgs e)
    {
        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_UpdateExportLoad", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToString(Session["UserLoginId"]), !chkExportLoad.Checked });
        FillDetails();
    }

    protected void chkHotShipment_CheckedChanged(object sender, EventArgs e)
    {
        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_UpdateLoadHotShipment", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToString(Session["UserLoginId"]), !chkHotShipment.Checked });
        FillDetails();
    }

    protected void chkAccessorialCharges_CheckedChanged(object sender, EventArgs e)
    {
        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_UpdateIsAccessorialCharges", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToString(Session["UserLoginId"]), !chkAccessorialCharges.Checked });
        FillDetails();
    }

    protected void chkIsVerified_CheckedChanged(object sender, EventArgs e)
    {
        int boolSelection = 0;
        if (chkIsVerified.Checked == true)
        {
            boolSelection = 1;
        }
        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Update_Documents_to_Verified", new object[] { Convert.ToInt64(ViewState["ID"]), 7, "loads", Convert.ToString(Session["UserLoginId"]), boolSelection });
        FillDetails(true);
        CheckIsVerified();
    }

    //SLine 2018 Enhancement
    protected void chkOverdue_CheckedChanged(object sender, EventArgs e)
    {

        int boolSelection = 0;
        if (chkOverdue.Checked == true)
        {
            boolSelection = 1;
        }
        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Update_Overdue", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToString(Session["UserLoginId"]), boolSelection });
        FillDetails(true);
        CheckIsVerified();
    }

    //SLine 2016 Enhancements 
    #endregion
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (gridPayables.RowCount == 0)
            gridbarPayables.Visible = false;
        if (Grid3.RowCount == 0)
            gridbarDrivers.Visible = false;


        //grid changes

        if (dgrLoad.Items.Count > 0)
        {
            for (int i = 0; i < dgrLoad.Items.Count; i++)
            {
                if ((i % 2) == 0)
                {
                    dgrLoad.Items[i].BackColor = System.Drawing.Color.Red;
                }
                else
                {
                    dgrLoad.Items[i].BackColor = System.Drawing.Color.Blue;
                }
            }
        }
        this.Master.FindControl("Top1").FindControl("pnlDispatcher").Visible = false;
        this.Master.FindControl("Top1").FindControl("pnlManager").Visible = false;
        this.Master.FindControl("Top1").FindControl("pnlStaff").Visible = false;
        //grid changes


    }
    private void AssignLinks(string LoadStatus)
    {
        string strLinks = string.Empty;
        string strPages = string.Empty;
        if (string.Compare(Convert.ToString(Session["Role"]).Trim(), "staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            strLinks = "Edit Load;" + (LoadStatus != "New" ? "Update Load Status;" : "") + "Load Request;Load Tender;Upload Documents;" + (LoadStatus != "New" ? "Show Payables;" : "") + "Show Receivables;Accessorial Charges;Invoice;T-Cards";
            strPages = "NewLoad.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + (LoadStatus != "New" ? "UpdateLoadStatus.aspx?" + Convert.ToString(ViewState["ID"]) + ";" : "") + "LoadRequest.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "LoadTender.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "UpLoadDocuments.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + (LoadStatus != "New" ? "ShowPayables.aspx?" + Convert.ToString(ViewState["ID"]) + ";" : "") + "ShowReceivables.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "AccessorialCharges.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "Invoice.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "SlineCards.aspx?LoadId=" + Convert.ToString(ViewState["ID"]);
            LinksBar.LoadStatus = LoadStatus;
            LinksBar.LoadId = Convert.ToInt64(ViewState["ID"]);
            LinksBar.IsReOpenVisible = false;
            if (LoadStatus == "Closed")
            {
                LinksBar.VisibleBar = false;
                strLinks = "Load Request;Load Tender;Accessorial Charges;Invoice;T-Cards";
                strPages = "LoadRequest.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "LoadTender.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "AccessorialCharges.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "Invoice.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "SlineCards.aspx?LoadId=" + Convert.ToString(ViewState["ID"]);
            }
            else if (LoadStatus == "Cancel")
            {
                strLinks = string.Empty;
                strPages = string.Empty;
                LinksBar.VisibleBar = false;
            }
        }
        else if (string.Compare(Convert.ToString(Session["Role"]).Trim(), "dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            strLinks = "Edit Load;" + (LoadStatus != "New" ? "Update Load Status;" : "") + "Load Request;Load Tender;Upload Documents;" + (LoadStatus == "Cancel" || LoadStatus == "Closed" ? "" : "Dispatch;") + (LoadStatus != "New" ? "Show Payables;" : "") + "Show Receivables;Accessorial Charges;Invoice;T-Cards";
            strPages = "NewLoad.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + (LoadStatus != "New" ? "UpdateLoadStatus.aspx?" + Convert.ToString(ViewState["ID"]) + ";" : "") + "LoadRequest.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "LoadTender.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "UpLoadDocuments.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + (LoadStatus == "Cancel" || LoadStatus == "Closed" ? "" : "Dispatch.aspx?" + Convert.ToString(ViewState["ID"]) + ";") + (LoadStatus != "New" ? "ShowPayables.aspx?" + Convert.ToString(ViewState["ID"]) + ";" : "") + "ShowReceivables.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "AccessorialCharges.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "Invoice.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "SlineCards.aspx?LoadId=" + Convert.ToString(ViewState["ID"]);
            LinksBar.LoadStatus = LoadStatus;
            LinksBar.LoadId = Convert.ToInt64(ViewState["ID"]);
            LinksBar.IsReOpenVisible = false;
            if (LoadStatus == "Closed")
            {
                LinksBar.VisibleBar = false;
                strLinks = "Load Request;Load Tender;Accessorial Charges;Invoice;T-Cards";
                strPages = "LoadRequest.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "LoadRequest.aspx?" + Convert.ToString(ViewState["ID"]) + ";"+ "AccessorialCharges.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "Invoice.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "SlineCards.aspx?LoadId=" + Convert.ToString(ViewState["ID"]);
            }
            else if (LoadStatus == "Cancel")
            {
                strLinks = string.Empty;
                strPages = string.Empty;
            }
        }
        else if (string.Compare(Convert.ToString(Session["Role"]).Trim(), "manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            strLinks = "Edit Load;" + (LoadStatus != "New" ? "Update Load Status;" : "") + "Load Request;Load Tender;Upload Documents;Dispatch;" + (LoadStatus != "New" ? "Show Payables;" : "") + "Show Receivables;Accessorial Charges;Invoice;T-Cards";
            strPages = "NewLoad.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + (LoadStatus != "New" ? "UpdateLoadStatus.aspx?" + Convert.ToString(ViewState["ID"]) + ";" : "") + "LoadRequest.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "LoadTender.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "UpLoadDocuments.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "Dispatch.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + (LoadStatus != "New" ? "ShowPayables.aspx?" + Convert.ToString(ViewState["ID"]) + ";" : "") + "ShowReceivables.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "AccessorialCharges.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "Invoice.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "SlineCards.aspx?LoadId=" + Convert.ToString(ViewState["ID"]);
            if (string.Compare(LoadStatus, "Closed", true, System.Globalization.CultureInfo.CurrentCulture) == 0 || string.Compare(LoadStatus, "Cancel", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                LinksBar.LoadStatus = LoadStatus;
                LinksBar.LoadId = Convert.ToInt64(ViewState["ID"]);
                LinksBar.IsReOpenVisible = true;
                if (LoadStatus == "Closed")
                {
                    LinksBar.VisibleBar = true;
                    strLinks = "Load Request;Load Tender;Accessorial Charges;Invoice;T-Cards";
                    strPages = "LoadRequest.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "LoadTender.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "AccessorialCharges.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "Invoice.aspx?" + Convert.ToString(ViewState["ID"]) + ";" + "SlineCards.aspx?LoadId=" + Convert.ToString(ViewState["ID"]);
                }
                else if (LoadStatus == "Cancel")
                {
                    strLinks = string.Empty;
                    strPages = string.Empty;
                    LinksBar.VisibleBar = false;
                }
            }
        }
        LinksBar.LinksList = strLinks;
        LinksBar.PagesList = strPages;
    }
    private void FillDetails(bool isOverdue = false)
    {
        long ID = Convert.ToInt64(ViewState["ID"]);
        string strAssigned = null, strEnterBy = null, strQuery = string.Empty, LoadStatus = string.Empty;
        long ShippingId = 0;
        bool IsApptDateVisible = false;
        #region Load details filling
        //Get hazmat and show if the load is hazmat
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadDetailsWithLoadId", new object[] { ID });
        if (Session["ViewLog"] != null)
        {
            Session["ViewLog"] = null;
            SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_LoadView_Log", new object[] { ID, Convert.ToString(Session["UserLoginId"]) });
        }
        if (ds.Tables.Count > 5)
        {
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblCustomer.Text = Convert.ToString(ds.Tables[0].Rows[0][0]);
                    lblPickUp.Text = Convert.ToString(ds.Tables[0].Rows[0][1]);
                    if (Convert.ToString(ds.Tables[0].Rows[0][2]).Trim().Length > 0)
                    {
                        if (Convert.ToString(ds.Tables[0].Rows[0][3]).Trim().Length > 0)
                            lblContainer.Text = Convert.ToString(ds.Tables[0].Rows[0][2]) + "<br/>" + Convert.ToString(ds.Tables[0].Rows[0][3]);
                        else
                            lblContainer.Text = Convert.ToString(ds.Tables[0].Rows[0][2]);
                    }
                    else
                        lblContainer.Text = Convert.ToString(ds.Tables[0].Rows[0][3]);
                    lblBooking.Text = Convert.ToString(ds.Tables[0].Rows[0][4]);
                    lblCommodity.Text = Convert.ToString(ds.Tables[0].Rows[0][5]);
                    lblSeal.Text = Convert.ToString(ds.Tables[0].Rows[0][6]);
                    lblCreated.Text = Convert.ToString(ds.Tables[0].Rows[0][7]);
                    //lblPersong.Text = Convert.ToString(ds.Tables[0].Rows[0][8]);
                    lblRef.Text = Convert.ToString(ds.Tables[0].Rows[0][8]);
                    strAssigned = Convert.ToString(ds.Tables[0].Rows[0][9]);
                    lblStatus.Text = LoadStatus = Convert.ToString(ds.Tables[0].Rows[0][10]);
                    ViewState["LoadState"] = Convert.ToString(ds.Tables[0].Rows[0][10]);

                    //SLine 2016 Enhancements ---- Bringing the Container Type
                    if (Convert.ToString(ds.Tables[0].Rows[0]["nvar_ContainerType"]) != "")
                    {
                        if (Convert.ToString(ds.Tables[0].Rows[0][11]) != "")
                        {
                            lblRailBill.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_ContainerType"]) + "/" + Convert.ToString(ds.Tables[0].Rows[0][11]);
                        }
                        else
                        {
                            lblRailBill.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_ContainerType"]);
                        }
                    }
                    else
                    {
                        lblRailBill.Text = Convert.ToString(ds.Tables[0].Rows[0][11]);
                    }
                    //SLine 2016 Enhancements

                    lblBillDate.Text = Convert.ToString(ds.Tables[0].Rows[0][12]);
                    lblPersonBill.Text = Convert.ToString(ds.Tables[0].Rows[0][13]);
                    //strEnterBy = Convert.ToString(ds.Tables[0].Rows[0][13]);
                    ShippingId = Convert.ToInt64(ds.Tables[0].Rows[0][14]);
                    if (Convert.ToString(ds.Tables[0].Rows[0][15]).Trim().Length > 0)
                    {
                        if (Convert.ToString(ds.Tables[0].Rows[0][16]).Trim().Length > 0)
                            lblChasis.Text = Convert.ToString(ds.Tables[0].Rows[0][15]) + "<br/>" + Convert.ToString(ds.Tables[0].Rows[0][16]);
                        else
                            lblChasis.Text = Convert.ToString(ds.Tables[0].Rows[0][15]);
                    }
                    else
                        lblChasis.Text = Convert.ToString(ds.Tables[0].Rows[0][16]);
                    lblLastfreedate.Text = Convert.ToString(ds.Tables[0].Rows[0][17]);
                    lblUpdatedOn.Text = Convert.ToString(ds.Tables[0].Rows[0][18]);
                    lblDescription.Text = Convert.ToString(ds.Tables[0].Rows[0][19]);
                    lblRemarks.Text = Convert.ToString(ds.Tables[0].Rows[0][20]);
                    //Get hazmat and show if the load is hazmat
                    bool ishazmat = Convert.ToBoolean(ds.Tables[0].Rows[0][21]);
                    if (ishazmat)
                    {
                        spanishazmat.Visible = true;
                    }
                    else
                    {
                        spanishazmat.Visible = false;
                    }
                    //Sline 2021
                    if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsExportLoad"]))
                    {
                        chkExportLoad.Checked = true;
                    }
                    else
                    {
                        chkExportLoad.Checked = false;
                    }
                    chkBookingProblem.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0][22]);
                    chkBookingProblem.Enabled = BEFOREDELIVARYIDS.Contains("'" + Convert.ToString(ds.Tables[0].Rows[0][23]).Trim() + "'");
                    chkPutonHold.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0][24]);
                    chkPutonHold.Enabled = BEFOREASSIGNED.Contains("'" + Convert.ToString(ds.Tables[0].Rows[0][23]).Trim() + "'");
                    chkChassisRent.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0][25]);

                    //SLine 2016 Enhancements

                    if (Convert.ToBoolean(ds.Tables[0].Rows[0][26]))
                    {
                        spanRailContainer.InnerText = "Rail";
                    }

                    else { spanRailContainer.InnerText = ""; }

                    chkHotShipment.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0][27]);

                    chkAccessorialCharges.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0][28]);


                    if (Convert.ToString(ds.Tables[0].Rows[0]["nvar_TripType"]) == "ROUND")
                    {
                        lblTripType.Visible = true;
                        imgTripType.Visible = false;
                    }

                    else if (Convert.ToString(ds.Tables[0].Rows[0]["nvar_TripType"]) == "DL")
                    {
                        lblTripType.Visible = false;
                        imgTripType.Visible = true;
                        imgTripType.ImageUrl = "../images/DL.png";
                    }

                    else if (Convert.ToString(ds.Tables[0].Rows[0]["nvar_TripType"]) == "DO")
                    {
                        lblTripType.Visible = false;
                        imgTripType.Visible = true;
                        imgTripType.ImageUrl = "../images/DO.png";
                    }

                    else if (Convert.ToString(ds.Tables[0].Rows[0]["nvar_TripType"]) == "PU")
                    {
                        lblTripType.Visible = false;
                        imgTripType.Visible = true;
                        imgTripType.ImageUrl = "../images/PU.png";
                    }
                    //SLine 2016 Enhancements

                    //Sline EDI 2018
                    ViewState["IsEdi"] = bool.Parse((Convert.ToString(ds.Tables[0].Rows[0]["bit_IsEDI"])));
                    ViewState["LoadType"]= (Convert.ToString(ds.Tables[0].Rows[0]["nvar_LoadType"]));

                    //SLine 2018 Enhancements                    
                    divOverdue.Visible = (Convert.ToString(ds.Tables[0].Rows[0]["bint_LoadStatusId"]) == "11");
                    if (!Page.IsPostBack || isOverdue)
                        chkOverdue.Checked = (Convert.ToString(ds.Tables[0].Rows[0]["bit_Is_Overdue"]) == "True");
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    lblRoute.Text = Convert.ToString(ds.Tables[1].Rows[0][0]);
                    lblPier.Text = Convert.ToString(ds.Tables[1].Rows[0][1]);
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    StringBuilder tempStr = new StringBuilder();
                    tempStr.Append(string.Empty);
                    foreach (DataRow dr in ds.Tables[2].Rows)
                    {
                        if (tempStr.ToString().Trim().Length != 0)
                            tempStr.Append("<br/>");
                        tempStr.Append(Convert.ToString(dr[0]).Trim() + "<br/>");
                    }
                    lblShippingContact.Text = tempStr.ToString().Trim();
                }
                if (ds.Tables[3].Rows.Count > 0)
                {
                    lblPieces.Text = Convert.ToString(ds.Tables[3].Rows[0][0]);
                    lblWeigth.Text = Convert.ToString(ds.Tables[3].Rows[0][1]);
                }
                if (ds.Tables[4].Rows.Count > 0)
                {
                    if (Convert.ToString(ds.Tables[4].Rows[0][0]).Trim().Length > 0)
                    {
                        lblAssigned.Text = Convert.ToString(ds.Tables[4].Rows[0][0]).Trim().Replace(",", "<br/>");
                    }
                }
                if (ds.Tables.Count > 6)
                {
                    try
                    {
                        lblPersong.Text = Convert.ToString(ds.Tables[6].Rows[0][0]);
                        lblUpdatedBy.Text = Convert.ToString(ds.Tables[7].Rows[0][0]);
                    }
                    catch
                    { }
                }
            }
            #region Document Data

            strQuery = "select isnull(bint_DocumentId,0) as 'Id',isnull(date_ReceivedDate,getdate()) as 'Date' from eTn_UploadDocuments where bint_LoadId=" + ID + " order by bint_DocumentId asc";
            //dt = DBClass.returnDataTable(strQuery);
            lnkInGateV.Visible = lnkLadingV.Visible = lnkOutGateV.Visible = lnkScaleV.Visible = lnkProofV.Visible = false;
            if (ds.Tables[5].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[5].Rows.Count; i++)
                {
                    switch (Convert.ToInt64(ds.Tables[5].Rows[i][0]))
                    {
                        case 1:
                            lblLoadOrder.Text = Convert.ToDateTime(ds.Tables[5].Rows[i][1]).ToShortDateString();
                            break;
                        case 2:
                            lblPOrder.Text = Convert.ToDateTime(ds.Tables[5].Rows[i][1]).ToShortDateString();
                            break;
                        case 3:
                            lblBillofLading.Text = Convert.ToDateTime(ds.Tables[5].Rows[i][1]).ToShortDateString();
                            if (!bool.Parse(ds.Tables[5].Rows[i][2].ToString()))
                                lnkLadingV.Visible = true;
                            break;
                        case 4:
                            lblAccessorial.Text = Convert.ToDateTime(ds.Tables[5].Rows[i][1]).ToShortDateString();
                            break;
                        case 5:
                            lblOutGate.Text = Convert.ToDateTime(ds.Tables[5].Rows[i][1]).ToShortDateString();
                            if (!bool.Parse(ds.Tables[5].Rows[i][2].ToString()))
                                lnkOutGateV.Visible = true;
                            break;
                        case 6:
                            lblInGate.Text = Convert.ToDateTime(ds.Tables[5].Rows[i][1]).ToShortDateString();
                            if (!bool.Parse(ds.Tables[5].Rows[i][2].ToString()))
                                lnkInGateV.Visible = true;
                            break;
                        case 7:
                            lblDelivery.Text = Convert.ToDateTime(ds.Tables[5].Rows[i][1]).ToShortDateString();
                            if (!bool.Parse(ds.Tables[5].Rows[i][2].ToString()))
                                lnkProofV.Visible = true;
                            break;
                        case 8:
                            lblTicket.Text = Convert.ToDateTime(ds.Tables[5].Rows[i][1]).ToShortDateString();
                            if (!bool.Parse(ds.Tables[5].Rows[i][2].ToString()))
                                lnkScaleV.Visible = true;
                            break;
                        case 9:
                            lblLumperReceipt.Text = Convert.ToDateTime(ds.Tables[5].Rows[i][1]).ToShortDateString();
                            break;
                        case 10:
                            lblMisc.Text = Convert.ToDateTime(ds.Tables[5].Rows[i][1]).ToShortDateString();
                            break;
                        case 11:
                            lblInvoiceDate.Text = Convert.ToDateTime(ds.Tables[5].Rows[i][1]).ToShortDateString();
                            break;
                    }
                }
            }
            #endregion
        }
        #endregion

        if (string.Compare(Convert.ToString(Session["Role"]).Trim(), "staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0 || string.Compare(Convert.ToString(Session["Role"]).Trim(), "manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            if (LoadStatus == "New" || LoadStatus == "Loaded in Yard")
                IsApptDateVisible = true;
        }
        int pageIndex = Convert.ToInt32(ConfigurationManager.AppSettings["GeneralPageSize"]);

        // BarOfficeNote.LinksList = "Add Office Notes";
        //BarOfficeNote.PagesList = "AddOfficeNotes.aspx?" + Convert.ToString(ViewState["ID"]);
        lnkOfficeNotes.PostBackUrl = "AddOfficeNotes.aspx?" + Convert.ToString(ViewState["ID"]);

        //Origion Grid
        Grid1.Visible = true;
        Grid1.DeleteVisible = false;
        Grid1.EditVisible = false;
        Grid1.TableName = "eTn_Origin";
        Grid1.Primarykey = "eTn_Origin.bint_OriginId";
        Grid1.PageSize = pageIndex;
        Grid1.ColumnsList = "eTn_Origin.bint_OriginId as 'Id';null as 'Order No';eTn_Origin.nvar_PO as 'PO';eTn_Origin.bint_Pieces as 'Pieces';eTn_Origin.num_Weight as 'Weight';" + CommonFunctions.AddressQueryStringWithLinks("eTn_Company.nvar_CompanyName;eTn_Location.nvar_Street;eTn_Location.nvar_Suite;eTn_Location.nvar_City;eTn_Location.nvar_State;eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "eTn_Origin.date_PickupDateTime", "Origin", "AddPickUpdate.aspx", "convert(varchar(20),eTn_Origin.[bint_LoadId])+'^'+convert(varchar(20),eTn_Origin.bint_OriginId)", "Add/Edit PickUp Date", IsApptDateVisible) +
            ";case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' To<br/> '+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Date';eTn_Origin.nvar_App as 'Appt';eTn_Origin.nvar_ApptGivenby as 'ApptGivenBy'";
        Grid1.VisibleColumnsList = "Order No;Origin;Date;Appt;ApptGivenBy";
        Grid1.VisibleHeadersList = "S.No.;Origin(s);Pickup Date /Time;Appt.#;Appt. Given by";
        Grid1.InnerJoinClause = " inner join eTn_Location on eTn_Origin.bint_LocationId = eTn_Location.bint_LocationId " +
               " inner join eTn_Company on eTn_Location.bint_CompanyId = eTn_Company.bint_CompanyId ";
        Grid1.WhereClause = "eTn_Origin.[bint_LoadId]=" + ID;
        Grid1.IsPagerVisible = false;
        Grid1.AutoIncrement = true;
        Grid1.BindGrid(0);

        //Destination Grid       
        Grid2.Visible = true;
        Grid2.DeleteVisible = false;
        Grid2.EditVisible = false;
        Grid2.TableName = "eTn_Destination";
        Grid2.Primarykey = "eTn_Destination.bint_DestinationId";
        Grid2.PageSize = pageIndex;
        Grid2.ColumnsList = "eTn_Destination.bint_DestinationId as 'Id';null as 'Order No';eTn_Destination.nvar_PO as 'PO';eTn_Destination.bint_Pieces as 'Pieces';eTn_Destination.num_Weight as 'Weight';" + CommonFunctions.AddressQueryStringWithLinks("eTn_Company.nvar_CompanyName;eTn_Location.nvar_Street;eTn_Location.nvar_Suite;eTn_Location.nvar_City;eTn_Location.nvar_State;eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "eTn_Destination.date_DeliveryAppointmentDate", "Destination", "AddAppointmentDate.aspx", "convert(varchar(20),eTn_Destination.[bint_LoadId])+'^'+convert(varchar(20),eTn_Destination.bint_DestinationId)", "Add/Edit Appointment Date ", IsApptDateVisible) +
            ";case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' To<br/> '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Date';eTn_Destination.nvar_App as 'Appt';eTn_Destination.nvar_ApptGivenby as 'ApptGivenBy'";
        Grid2.VisibleColumnsList = "Order No;Destination;Date;Appt;ApptGivenBy";
        Grid2.VisibleHeadersList = "S.No.;Destination(s);Appt. Date /Time;Appt.#;Appt. Given by";
        Grid2.InnerJoinClause = " inner join eTn_Location on eTn_Destination.bint_LocationId = eTn_Location.bint_LocationId " +
               " inner join eTn_Company on eTn_Location.bint_CompanyId = eTn_Company.bint_CompanyId ";
        Grid2.WhereClause = "eTn_Destination.[bint_LoadId]=" + ID;
        Grid2.IsPagerVisible = false;
        Grid2.AutoIncrement = true;
        Grid2.BindGrid(0);

        //Driver Grid
        Grid3.Visible = true;
        Grid3.DeleteVisible = false;
        Grid3.EditVisible = false;
        Grid3.TableName = "eTn_AssignDriver";
        Grid3.Primarykey = "eTn_AssignDriver.bint_AssignedId";
        Grid3.PageSize = pageIndex;
        Grid3.ColumnsList = "eTn_AssignDriver.bint_AssignedId as 'Id';eTn_Employee.nvar_NickName as 'Name';eTn_AssignDriver.nvar_Tag as 'Tag';" + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_FromState", "From") + ";" + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_Tocity;eTn_AssignDriver.nvar_ToState", "To") + ";    isnull(eTn_Payables.[num_Charges],0)-isnull(eTn_Payables.[num_AdvancedPaid],0)+isnull(eTn_Payables.[num_Dryrun],0)+isnull(eTn_Payables.[num_FuelSurcharge],0)+isnull(eTn_Payables.[num_FuelSurchargeAmounts],0)+" +
        "isnull(eTn_Payables.[num_PierTermination],0)+isnull(eTn_Payables.[bint_ExtraStops],0)+" +
        "isnull(eTn_Payables.[num_LumperCharges],0) +isnull(eTn_Payables.[num_DetentionCharges],0)+isnull(eTn_Payables.[num_ChassisSplit],0)+" +
        "isnull(eTn_Payables.[num_ChassisRent],0)+isnull(eTn_Payables.[num_Pallets],0)+isnull(eTn_Payables.[num_ContainerWashout],0)+" +
        "isnull(eTn_Payables.[num_YardStorage],0)+isnull(eTn_Payables.[num_RampPullCharges],0)+isnull(eTn_Payables.[num_TriaxleCharge],0)+" +
        "isnull(eTn_Payables.[num_TransLoadCharges],0)+isnull(eTn_Payables.[num_HLScaleCharges],0)+" +
        "isnull(eTn_Payables.[num_ScaleTicketCharges],0)+isnull(eTn_Payables.[num_OtherCharges1],0)+" +
        "isnull(eTn_Payables.[num_OtherCharges2],0)+isnull(eTn_Payables.[num_OtherCharges3],0)+isnull(eTn_Payables.[num_OtherCharges4],0)-" +
        "isnull(eTn_Payables.[num_PaidToAnotherDriver],0)+isnull(eTn_Payables.[num_TollCharges],0)+isnull(eTn_Payables.[num_SlipCharges],0)+isnull(eTn_Payables.[num_RepairCharges],0) as 'Charges';" +
        "case when eTn_Payables.nvar_PayableStatus is not null and eTn_Payables.nvar_PayableStatus='Paid' then 'Paid' else 'Pending' end as 'Status'";
        Grid3.VisibleColumnsList = "Name;Tag;From;To;Charges;Status";
        Grid3.VisibleHeadersList = "Name;Tag;From City State ;To City  State;Charges;Status";
        Grid3.InnerJoinClause = " inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId " +
               " left outer join eTn_Payables on eTn_Payables.bint_LoadId = eTn_AssignDriver.bint_LoadId and [eTn_AssignDriver].[bint_AssignedId]=[eTn_Payables].[bint_AssignedId]";
        Grid3.WhereClause = "eTn_AssignDriver.[bint_LoadId]=" + ID;
        Grid3.OrderByClause = "eTn_AssignDriver.[date_DateAssigned]";
        Grid3.IsPagerVisible = false;
        Grid3.BindGrid(0);

        //Receivable details
        // Check for the Paid or not for a Load to show Posted to QB button(Madhav)
        DataSet haspostedqp = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoad_Has_Receivables_Not_Paid", new object[] { ID });
        if (haspostedqp != null)
        {
            if (haspostedqp.Tables.Count > 0 && haspostedqp.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToInt32(haspostedqp.Tables[0].Rows[0]["PaidCount"]) == 0)
                {
                    gridPayables.ChangeStatusLink = false;
                }
                else
                {
                    gridPayables.ChangeStatusLink = true;
                }
            }
        }

        gridPayables.Visible = true;
        gridPayables.DeleteVisible = false;
        gridPayables.EditVisible = false;
        gridPayables.TableName = "eTn_Receivables";
        gridPayables.Primarykey = "eTn_Receivables.bint_ReceivablesId";
        gridPayables.PageSize = pageIndex;
        gridPayables.ColumnsList = "[eTn_Receivables].[num_GrossAmount] as 'GrossAmount';[eTn_Receivables].[num_SurchargeAmount] as 'FuelSurcharge';[eTn_Receivables].[num_Dryrun]-[eTn_Receivables].[num_AdvanceReceived]+[eTn_Receivables].[num_PierTermination]+[eTn_Receivables].[bint_ExtraStops]+[eTn_Receivables].[num_LumperCharges]+[eTn_Receivables].[num_DetentionCharges]+[eTn_Receivables].[num_ChassisSplit]+[eTn_Receivables].[num_ChassisRent]+[eTn_Receivables].[num_Pallets]+[eTn_Receivables].[num_ContainerWashout]+[eTn_Receivables].[num_YardStorage]+[eTn_Receivables].[num_RampPullCharges]+[eTn_Receivables].[num_TriaxleCharge]+[eTn_Receivables].[num_TransLoadCharges]+[eTn_Receivables].[num_HeavyLightScaleCharges]+[eTn_Receivables].[num_ScaleTicketCharges]+[eTn_Receivables].[num_OtherCharges1]+[eTn_Receivables].[num_OtherCharges2]+[eTn_Receivables].[num_OtherCharges3]+[eTn_Receivables].[num_OtherCharges4] as 'OtherCharges'; isnull([eTn_Receivables].[num_GrossAmount],0)-isnull([eTn_Receivables].[num_AdvanceReceived],0)+isnull([eTn_Receivables].[num_Dryrun],0)+isnull([eTn_Receivables].[num_FuelSurchargeAmount],0)+isnull([eTn_Receivables].[num_SurchargeAmount],0)+isnull([eTn_Receivables].[num_PierTermination],0)+isnull([eTn_Receivables].[bint_ExtraStops],0)+isnull([eTn_Receivables].[num_LumperCharges],0)+" +
                                    "isnull([eTn_Receivables].[num_DetentionCharges],0)+isnull([eTn_Receivables].[num_ChassisSplit],0)+isnull([eTn_Receivables].[num_ChassisRent],0)+isnull([eTn_Receivables].[num_Pallets],0)+isnull([eTn_Receivables].[num_ContainerWashout],0)+isnull([eTn_Receivables].[num_YardStorage],0)+isnull([eTn_Receivables].[num_RampPullCharges],0)+isnull([eTn_Receivables].[num_TriaxleCharge],0)+isnull([eTn_Receivables].[num_TransLoadCharges],0)+" +
                                    "isnull([eTn_Receivables].[num_HeavyLightScaleCharges],0)+isnull([eTn_Receivables].[num_ScaleTicketCharges],0)+isnull([eTn_Receivables].[num_OtherCharges1],0)+isnull([eTn_Receivables].[num_OtherCharges2],0)+isnull([eTn_Receivables].[num_OtherCharges3],0)+isnull([eTn_Receivables].[num_OtherCharges4],0) as 'Total';[eTn_Receivables].[nvar_OtherChargesReason] as 'OtherChargesReason'; case when eTn_Receivables.nvar_ReceivableStatus is not null and eTn_Receivables.nvar_ReceivableStatus='' then 'Pending' else eTn_Receivables.nvar_ReceivableStatus end as 'Status'";
        gridPayables.VisibleColumnsList = "GrossAmount;FuelSurcharge;OtherCharges;Total;OtherChargesReason;Status";
        gridPayables.VisibleHeadersList = "Gross;Fuel Surcharge;Other Charges;Total;OtherCharges Reason;Status";
        gridPayables.WhereClause = "eTn_Receivables.[bint_LoadId]=" + ID;
        gridPayables.IsPagerVisible = false;
        gridPayables.BindGrid(0);

        #region SLine 2016 Enhancements Commented region

        //SLine 2016 Enhancements
        //COMMENTED THE BELOW GRID ---- FOR Showing Upload Load Status in TRACK-LOAD-PAGE
        //Akhtar 05 May 2016


        //grid Load Status Information
        //
        Grid4.Visible = true;
        Grid4.DeleteVisible = false;
        Grid4.EditVisible = false;
        Grid4.TableName = "eTn_TrackLoad";
        Grid4.Primarykey = "eTn_TrackLoad.bint_TrackLoadId";
        // Grid4.PageSize = 20;
        Grid4.ColumnsList = "eTn_UserLogin.nvar_FirstName+' '+eTn_UserLogin.nvar_LastName as 'Name';eTn_LoadStatus.nvar_LoadStatusDesc as 'Status';eTn_TrackLoad.nvar_Location as 'Loc';eTn_TrackLoad.date_CreateDate as 'Date';eTn_TrackLoad.nvar_Notes as 'Notes'";
        Grid4.VisibleColumnsList = "Name;Status;Loc;Date;Notes";
        Grid4.VisibleHeadersList = "Updated By;Status;Location;Date / Time;Comments";
        Grid4.InnerJoinClause = " inner join eTn_LoadStatus on eTn_LoadStatus.bint_LoadStatusId = eTn_TrackLoad.bint_LoadStatusId left outer join eTn_UserLogin on eTn_UserLogin.bint_UserLoginId=eTn_TrackLoad.bint_RolePlayerId ";
        Grid4.WhereClause = "eTn_TrackLoad.[bint_LoadId]=" + ID;
        Grid4.IsPagerVisible = false;
        Grid4.BindGrid(0);

        if (Grid4.RowCount > 0)
        {
            string picupDateTime = null;
            //SLine 2016 ---- Updates
            //string terminatedDateTime = null;
            string FromDateTime = null;
            bool boolStreetTurnFlag = false;
            foreach (DataGridItem dgi in Grid4.GridControl.Items)
            {
                if (dgi.Cells[1].Text.ToUpper() == "PICKEDUP")
                {
                    picupDateTime = dgi.Cells[3].Text;
                }

                //SLine 2016 ---- Added a new Status "Street Turn" to the Condition
                //Old Condition : if (dgi.Cells[1].Text.ToUpper() == "TERMINATED")

                if (dgi.Cells[1].Text.ToUpper() == "STREET TURN")
                {
                    FromDateTime = dgi.Cells[3].Text;
                    boolStreetTurnFlag = true;
                }
                else if (dgi.Cells[1].Text.ToUpper() == "TERMINATED" && boolStreetTurnFlag == false)
                {
                    FromDateTime = dgi.Cells[3].Text;
                }
            }
            if (picupDateTime != null && FromDateTime != null)
            {
                TimeSpan ts = (DateTime.Parse(FromDateTime)).Date - (DateTime.Parse(picupDateTime)).Date;
                lblChassisDays.Text = (ts.Days + 1).ToString();
            }
            else
            {
                //lblChassisDays.Text = "Load is not picked up/terminated yet.";
                lblChassisDays.Text = "Load is not picked up/terminated/Street turn yet.";
            }
            //SLine 2016 ---- Updates
        }
        else
        {
            //lblChassisDays.Text = "Load is not picked up/terminated yet.";
            lblChassisDays.Text = "Load is not picked up/terminated/Street turn yet.";
        }
        ////Grid Notes 
        #endregion

        Grid5.Visible = true;
        Grid5.DeleteVisible = false;
        Grid5.EditVisible = false;
        Grid5.TableName = "eTn_Notes";
        Grid5.Primarykey = "eTn_Notes.bint_NoteId";
        Grid5.PageSize = pageIndex;
        Grid5.ColumnsList = "eTn_UserLogin.nvar_FirstName+' '+eTn_UserLogin.nvar_LastName as 'EnterBy';eTn_Notes.date_CreateDate as 'Date';eTn_Notes.nvar_NotesDesc as 'Notes'";
        Grid5.VisibleColumnsList = "EnterBy;Date;Notes";
        Grid5.VisibleHeadersList = "Entered By ;Date / Time;Comments";
        Grid5.InnerJoinClause = " inner join [eTn_Load] on [eTn_Load].bint_LoadId =[eTn_Notes].[bint_LoadId] inner join eTn_UserLogin on [eTn_UserLogin].bint_UserLoginId = [eTn_Notes].bint_UserLoginId";
        //Grid5.InnerJoinClause = " inner join [eTn_Load] on [eTn_Load].bint_LoadId =[eTn_Notes].[bint_LoadId] inner join [eTn_Load].[bint_UserLoginId]=[eTn_UserLogin].[bint_UserLoginId]";
        Grid5.WhereClause = "eTn_Notes.[bint_LoadId]=" + ID;
        Grid5.OrderByClause = "eTn_Notes.date_CreateDate desc";
        Grid5.IsPagerVisible = true;
        Grid5.BindGrid(0);

        //Office Notes Grid
        OfficeNoteGrid.Visible = true;
        OfficeNoteGrid.DeleteVisible = false;
        OfficeNoteGrid.EditVisible = false;
        OfficeNoteGrid.TableName = "eTn_OfficeNotes";
        OfficeNoteGrid.Primarykey = "eTn_OfficeNotes.bint_OfficeNoteId";
        OfficeNoteGrid.PageSize = pageIndex;
        OfficeNoteGrid.ColumnsList = "eTn_UserLogin.nvar_FirstName+' '+eTn_UserLogin.nvar_LastName as 'EnterBy';eTn_OfficeNotes.date_CreateDate as 'Date';eTn_OfficeNotes.nvar_Status as 'Status'";
        OfficeNoteGrid.VisibleColumnsList = "EnterBy;Date;Status";
        OfficeNoteGrid.VisibleHeadersList = "Entered By ;Date / Time;Status";
        OfficeNoteGrid.InnerJoinClause = " inner join [eTn_Load] on [eTn_Load].bint_LoadId =[eTn_OfficeNotes].[bint_LoadId] inner join eTn_UserLogin on [eTn_UserLogin].bint_UserLoginId = [eTn_OfficeNotes].bint_UserLoginId";
        OfficeNoteGrid.WhereClause = "eTn_OfficeNotes.[bint_LoadId]=" + ID;
        OfficeNoteGrid.IsPagerVisible = true;
        OfficeNoteGrid.BindGrid(0);


        //Rail/Equipment  Notes Grid
        //BarRailNotes.LinksList = "Add Rail/Equipment Tracing";
        //BarRailNotes.PagesList = "AddRailNotes.aspx?" + ID;        
        lnkRailNote.PostBackUrl = "AddRailNotes.aspx?" + ID;

        GridRailNotes.Visible = true;
        GridRailNotes.DeleteVisible = false;
        GridRailNotes.EditVisible = false;
        GridRailNotes.TableName = "eTn_RailNotes";
        GridRailNotes.Primarykey = "eTn_RailNotes.bint_RailNoteId";
        GridRailNotes.PageSize = pageIndex;
        GridRailNotes.ColumnsList = "eTn_UserLogin.nvar_FirstName+' '+eTn_UserLogin.nvar_LastName as 'EnterBy';eTn_RailNotes.date_CreateDate as 'Date';eTn_RailNotes.nvar_Status as 'Status'";
        GridRailNotes.VisibleColumnsList = "EnterBy;Date;Status";
        GridRailNotes.VisibleHeadersList = "Entered By ;Date / Time;Status";
        GridRailNotes.InnerJoinClause = " inner join [eTn_Load] on [eTn_Load].bint_LoadId =[eTn_RailNotes].[bint_LoadId] inner join eTn_UserLogin on [eTn_UserLogin].bint_UserLoginId = [eTn_RailNotes].bint_UserLoginId";
        GridRailNotes.WhereClause = "eTn_RailNotes.[bint_LoadId]=" + ID;
        GridRailNotes.IsPagerVisible = true;
        GridRailNotes.BindGrid(0);

        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        //SLine 2017 Enhancement for Office Location Ends

        //Grid Contact Persons
        Grid6.Visible = true;
        Grid6.DeleteVisible = false;
        Grid6.EditVisible = false;
        Grid6.TableName = "eTn_Load";
        Grid6.Primarykey = "eTn_Load.bint_LoadId";
        Grid6.PageSize = pageIndex;
        Grid6.ColumnsList = "eTn_CustomerContacts.nvar_Name as 'Name';" + CommonFunctions.PhoneQueryString("eTn_CustomerContacts.num_Mobile", "Mobile") + ";" + CommonFunctions.PhoneQueryString("eTn_CustomerContacts.num_WorkPhone", "Phone") + ";" + CommonFunctions.PhoneQueryString("eTn_CustomerContacts.num_Fax", "Fax") + ";'<a href=mailto:'+eTn_CustomerContacts.nvar_Email+'>'+eTn_CustomerContacts.nvar_Email+'</a>' as 'Email'";
        Grid6.VisibleColumnsList = "Name;Mobile;Phone;Fax;Email";
        Grid6.VisibleHeadersList = "Name ;Mobile;Work Phone;Fax;Email";
        Grid6.InnerJoinClause = " inner join eTn_CustomerContacts on eTn_CustomerContacts.bint_CustomerId = eTn_Load.bint_CustomerId ";
        Grid6.WhereClause = "eTn_Load.bint_LoadId=" + ID +
            //SLine 2017 Enhancement for Office Location
            " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
        Grid6.IsPagerVisible = false;
        Grid6.BindGrid(0);


        //GridBar3.HeaderText = "Notes";
        //GridBar3.LinksList = "Add Note";
        //GridBar3.PagesList = "AddNotes.aspx?" + Convert.ToInt64(ID);
        lnkNotes.PostBackUrl = "AddNotes.aspx?" + Convert.ToInt64(ID);

        if (ds != null) { ds.Dispose(); ds = null; }
        AssignLinks(LoadStatus);
    }
    private void GetDocumentsbyID(long DocumentId)
    {
        if (ViewState["ID"] != null)
        {
            DataTable dt = DBClass.returnDataTable("select [nvar_DocumentName],isnull([bint_FileNo],0),[date_ReceivedDate],[bit_Is_Verified] from [eTn_UploadDocuments] where [bint_LoadId]=" + Convert.ToInt64(ViewState["ID"]) + " and [bint_DocumentId]=" + DocumentId + " and [nvar_DocumentName] is not null");
            if (dt.Rows.Count > 0)
            {
                string strFormat = "<table border=1 cellpadding=0 cellspacing=0 id=tbldisplay><tr><th>View Documents</th></tr>";
                string docpath = "../Documents" + "\\" + Convert.ToString(ViewState["ID"]).Trim() + "\\";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string[] filename = dt.Rows[i][0].ToString().Split('^');
                    if (File.Exists(Server.MapPath("~/Documents/" + Convert.ToString(ViewState["ID"]).Trim() + "/" + filename[0])))
                    {
                        //string[] filename = dt.Rows[i][0].ToString().Split('^');
                        strFormat += "<tr id=" + ((i % 2 == 0) ? "row" : "altrow") + "><td align=left>";
                        strFormat += "<a style=font-size:12;color:blue;height:15; href='" + docpath + filename[0] + "' target='_blank'>" + filename[0] + "</a>"; strFormat += "</td></tr>";
                    }
                    else
                    {
                        //string[] filename = dt.Rows[i][0].ToString().Split('^');
                        strFormat += "<tr id=" + ((i % 2 == 0) ? "row" : "altrow") + "><td align=left>";
                        strFormat += "<a style=font-size:12;color:blue;height:15; target='_blank'>" + filename[0] + " - File Deleted" + "</a>";
                        strFormat += "</td></tr>";
                    }
                }
                strFormat += "</table>";
                plhDocs.Controls.Add(new LiteralControl(strFormat));
                plhDocs.Visible = true;
            }
            else
                plhDocs.Visible = false;
            if (dt != null) { dt.Dispose(); dt = null; }
        }
        /* Code Written By Sudha */
        pnlPhDocs.Visible = true;
        pnlUploads.Visible = false;
        /* Code Written By Sudha */


    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Session["ViewLog"] = true;
        if (txtContainer.Text.Trim().Length > 0)
        {
            //SLine 2017 Enhancement for Office Location Starts
            long officeLocationId = 0;
            if (Session["OfficeLocationID"] != null)
            {
                officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
            }
            else
            {
                if (Session["LocationDS"] != null)
                {
                    DataSet locDs = Session["LocationDS"] as DataSet;
                    officeLocationId = Convert.ToInt64(locDs.Tables[1].Rows[0][0]);
                }
            }
            //SLine 2017 Enhancement for Office Location Ends
            switch (ddlContainer.SelectedIndex)
            {
                case 0:
                    //SLine 2017 Enhancement for Office Location
                    Session["TrackWhereClause"] = "(eTn_Load.[nvar_Container] like '" + txtContainer.Text.Trim() + "' or eTn_Load.[nvar_Container1] like '" + txtContainer.Text.Trim() + "') and eTn_Load.bint_OfficeLocationId=" + officeLocationId;
                    break;
                case 1:
                    //SLine 2017 Enhancement for Office Location
                    Session["TrackWhereClause"] = "eTn_Load.[nvar_Ref] like '" + txtContainer.Text.Trim() + "%' and eTn_Load.bint_OfficeLocationId=" + officeLocationId;
                    break;
                case 2:
                    //SLine 2017 Enhancement for Office Location
                    Session["TrackWhereClause"] = "eTn_Load.[nvar_Booking] like '" + txtContainer.Text.Trim() + "%' and eTn_Load.bint_OfficeLocationId=" + officeLocationId;
                    break;
                case 3:
                    //SLine 2017 Enhancement for Office Location
                    Session["TrackWhereClause"] = "(eTn_Load.[nvar_Chasis] like '" + txtContainer.Text.Trim() + "%' or eTn_Load.[nvar_Chasis1] like '" + txtContainer.Text.Trim() + "%') and eTn_Load.bint_OfficeLocationId=" + officeLocationId;
                    break;
                case 4:
                    try
                    {
                        //SLine 2017 Enhancement for Office Location
                        Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=" + Convert.ToInt64(txtContainer.Text.Trim()) + " and eTn_Load.bint_OfficeLocationId=" + officeLocationId;
                    }
                    catch
                    {
                        Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=0";
                    }
                    break;
            }
        }
        else
        {
            Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=0";
        }
        bool blVal = false;
        try
        {
            if (Convert.ToInt32(DBClass.executeScalar("select count(bint_LoadId) from eTn_Load where " + Convert.ToString(Session["TrackWhereClause"]))) == 1)
            {
                blVal = true;
            }
        }
        catch
        {
        }
        if (blVal)
        {
            Response.Redirect("~/Manager/TrackLoad.aspx?" + DBClass.executeScalar("select bint_LoadId from eTn_Load where " + Convert.ToString(Session["TrackWhereClause"])));
        }
        Response.Redirect("~/Manager/SearchResults.aspx");
    }
    private void ViewDocumnets(string strFol)
    {
        if (ViewState["ID"] == null)
            return;
        strFol = strFol.Trim();
        string strPath = Server.MapPath(ConfigurationSettings.AppSettings["DocumentsFolder"]) + Convert.ToString(ViewState["ID"]).Trim();
        if (System.IO.Directory.Exists(strPath))
        {
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(strPath);
            System.IO.FileSystemInfo[] fi = di.GetFileSystemInfos();
            if (fi.Length > 0)
            {
                DateTime[] strTemp = new DateTime[fi.Length];
                string[] strSortFile = new string[fi.Length];
                for (int i = 0; i < fi.Length; i++)
                {
                    strTemp[i] = fi[i].CreationTime;
                }
                Array.Sort(strTemp);
                int index = 0; int flag = 0;
                for (int j = 0; j < strTemp.Length; j++)
                {
                    for (int i = 0; i < fi.Length; i++)
                    {
                        flag = 0;
                        if (fi[i].CreationTime == strTemp[j])
                        {
                            for (int k = 0; k < index; k++)
                            {
                                if (strSortFile[k] == fi[i].FullName)
                                {
                                    flag = 1; break;
                                }
                            }
                            if (flag == 0)
                            {
                                strSortFile[index] = fi[i].FullName;
                                index = index + 1;
                                break;
                            }
                            flag = 0;
                        }
                    }
                }
                if (strSortFile.Length > 0)
                {
                    string strFormat = "<table border=1 cellpadding=0 cellspacing=0 id=tbldisplay><tr><th>View Documents</th></tr>";
                    string[] strFileNames = null; string strOpenPath = "";
                    for (int i = 0; i < strSortFile.Length; i++)
                    {
                        strFileNames = strSortFile[i].Trim().Split('\\');
                        strFormat += "<tr id=" + ((i % 2 == 0) ? "row" : "altrow") + "><td align=left>";
                        strOpenPath = ConfigurationSettings.AppSettings["DocumentsFolder"] + Convert.ToString(ViewState["ID"]).Trim() + "\\" + strFileNames[strFileNames.Length - 1];
                        strOpenPath = strOpenPath.Replace("\\\\", "\\");
                        strFormat += "<a style=font-size:12;color:blue;height:15; href='" + strOpenPath + "' target='_blank'>" + strFileNames[strFileNames.Length - 1] + "</a>";
                        strFormat += "</td></tr>";
                    }
                    strFormat += "</table>";
                    plhDocs.Controls.Add(new LiteralControl(strFormat));
                    strFileNames = null;
                }
            }
            fi = null; di = null;
        }
    }
    protected void lnkLoadOrder_Click(object sender, EventArgs e)
    {
        GetDocumentsbyID(1);
    }
    protected void lnlPoOrder_Click(object sender, EventArgs e)
    {
        GetDocumentsbyID(2);
    }
    protected void lnkLading_Click(object sender, EventArgs e)
    {
        GetDocumentsbyID(3);
    }
    protected void lnkAccessorial_Click(object sender, EventArgs e)
    {
        GetDocumentsbyID(4);
    }
    protected void lnkOutgate_Click(object sender, EventArgs e)
    {
        GetDocumentsbyID(5);
    }
    protected void lnkInGate_Click(object sender, EventArgs e)
    {
        GetDocumentsbyID(6);
    }
    protected void lnkProof_Click(object sender, EventArgs e)
    {
        GetDocumentsbyID(7);
    }
    protected void lnkScale_Click(object sender, EventArgs e)
    {
        GetDocumentsbyID(8);
    }
    protected void lnkLumper_Click(object sender, EventArgs e)
    {
        GetDocumentsbyID(9);
    }
    protected void lnkMisc_Click(object sender, EventArgs e)
    {
        GetDocumentsbyID(10);
    }
    protected void lnkUpLoadDoc_Click(object sender, EventArgs e)
    {
        if (ViewState["ID"] != null)
        {
            Response.Redirect("~/Manager/UpLoadDocuments.aspx?" + Convert.ToString(ViewState["ID"]));
        }
    }
    protected void Verified_Click(object sender, EventArgs e)
    {
        //SLine 2016 ---- This SP has been changed and new parameters have been added
        //"doctbl" this is a Flag, and it will update the Document Table
        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Update_Documents_to_Verified", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(((LinkButton)sender).CommandArgument), "doctbl", Convert.ToInt64(Session["UserLoginId"]), false });
        FillDetails();
    }
    /* Code Written By Sudha */
    protected void FileUploadwithFiles(object sender, ImageClickEventArgs e)
    {
        ImageButton i = (ImageButton)sender;
        pnlUploads.Visible = true;
        pnlPhDocs.Visible = false;
        lblSelectedDocumentTypeText.Text = i.CommandName;
        object[] Vals = i.CommandArgument.Split('$');
        ViewState["LinkValue"] = Vals[0];
        ViewState["LabelName"] = Vals[1];
        Session["NoFiles"] = null;
        if (ViewState["LoadID"] != null)
        {
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["conString"], "SP_GetDocuments", new object[] { Convert.ToInt64(ViewState["LoadID"]), Convert.ToInt64(Vals[0]) });
            if (ds != null && ds.Tables.Count > 0)
            {
                fupLoad.BindData(ds.Tables[0]);
                if (ds.Tables[1].Rows.Count > 0)
                    fupLoad.FileNo = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                fupLoad.Files = string.Empty;
                Session["PostedFiles"] = null;
            }
            else
            {
                Label lbl = new Label();
                lbl.ID = ViewState["LabelName"].ToString();
                lbl.Text = string.Empty;
            }

        }
    }
    protected void btnupload_Click(object sender, EventArgs e)
    {
        //if (Session["NoFiles"] != null && Convert.ToBoolean(Session["NoFiles"].ToString())==false)
        //{
        if ((fupLoad.bAdd || fupLoad.bDelete))
        {
            try
            {
                if (SqlHelper.ExecuteNonQuery(ConfigurationManager.AppSettings["conString"], "SP_SaveUploadDocuments", new object[] { Convert.ToInt64(ViewState["LoadID"]), Convert.ToInt64(ViewState["LinkValue"].ToString()), fupLoad.Files }) > 0)
                {
                    DataSet ds = DBClass.returnDataSet("select [nvar_DocumentName],[bint_FileNo] from [eTn_UploadDocuments] where [bint_LoadId] =" + Convert.ToInt64(ViewState["LoadID"]) + " and [bint_DocumentId] =" + Convert.ToInt64(ViewState["LinkValue"].ToString()) + " and [bint_FileNo]>" + fupLoad.FileNo.ToString(), "tblDocuments");
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        System.IO.DirectoryInfo DirInfo = new System.IO.DirectoryInfo(Server.MapPath("~/Documents") + "\\" + Convert.ToString(ViewState["LoadID"]) + "\\");
                        if (!DirInfo.Exists)
                        {
                            DirInfo.Create();
                        }
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            List<HttpPostedFile> postedfiles = fupLoad.PostedFiles;
                            if (ds.Tables[0].Rows.Count == postedfiles.Count)
                            {
                                for (int i = 0; i < postedfiles.Count; i++)
                                {
                                    if (!System.IO.File.Exists(DirInfo.FullName + Convert.ToString(ds.Tables[0].Rows[i][0])))
                                        postedfiles[i].SaveAs(DirInfo.FullName + Convert.ToString(ds.Tables[0].Rows[i][0]));
                                }
                                Session["FilesSaved"] = true;
                            }
                            Session["PostedFiles"] = null;
                            //ViewState["LinkValue"] = "0";
                            fupLoad.Files = string.Empty;
                        }
                        if (fupLoad.bDelete)
                        {
                            string[] strfiles = fupLoad.Files.Trim().Split(';');
                            if (strfiles.Length > 0)
                            {
                                System.IO.FileInfo fileinfo = null;
                                for (int i = 0; i < strfiles.Length; i++)
                                {
                                    string[] strFileName = strfiles[i].Trim().Split('^');
                                    if (strFileName.Length > 1)
                                    {
                                        if (strFileName[1] == "D")
                                        {
                                            fileinfo = new System.IO.FileInfo(DirInfo.FullName + strFileName[0]);
                                            if (fileinfo.Exists)
                                                fileinfo.Delete();
                                        }
                                    }
                                    strFileName = null;
                                    fileinfo = null;
                                }
                                strfiles = null;
                            }
                        }
                    }
                }
            }
            catch
            { }
            finally
            {


            }
        }

        //if (ViewState["LoadID"] != null && Session["NoFiles"] != null && Convert.ToBoolean(Session["NoFiles"].ToString()) == true)
        //{
        if (ViewState["LoadID"] != null && Session["NoFiles"] != null)
        {
            Label lbl = Master.FindControl("ContentPlaceHolder1").FindControl(ViewState["LabelName"].ToString()) as Label;
            lbl.Text = string.Empty;

        }
        else
        {
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["conString"], "SP_GetDocuments", new object[] { Convert.ToInt64(ViewState["LoadID"]), Convert.ToInt64(ViewState["LinkValue"].ToString()) });
            Label lbl = Master.FindControl("ContentPlaceHolder1").FindControl(ViewState["LabelName"].ToString()) as Label;
            if (ds != null)
            {
                if (ds.Tables.Count != 0)
                {
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        DataView dv = ds.Tables[0].DefaultView;
                        dv.Sort = "date_ReceivedDate DESC";
                        lbl.Text = Convert.ToDateTime(dv[0][2].ToString()).ToShortDateString();
                    }
                }
            }
        }
        pnlUploads.Visible = false;
        //}
        //else
        //{
        //    string myStringVariable = string.Empty;
        //    myStringVariable = "Please Select atleast One File(s)!";
        //    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
        //}
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        pnlUploads.Visible = false;
        //tbldisplay.Visible = false;     
    }
    /* Code Written By Sudha */
    protected void SendMailwithFiles(object sender, ImageClickEventArgs e)
    {
        ImageButton img = (ImageButton)sender;
        if (ViewState["LoadID"] != null)
        {
            string[] filePaths;
            DataTable dt = DBClass.returnDataTable("select [nvar_DocumentName],isnull([bint_FileNo],0),[date_ReceivedDate] from [eTn_UploadDocuments] where [bint_LoadId]=" + Convert.ToInt64(ViewState["ID"]) + " and [bint_DocumentId]=" + Int64.Parse(img.CommandArgument) + " and [nvar_DocumentName] is not null");
            if (dt != null && dt.Rows.Count > 0)
            {
                filePaths = new string[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string[] filename = dt.Rows[i][0].ToString().Split('^');
                    filePaths[i] = Server.MapPath("../Documents" + "\\" + Convert.ToString(ViewState["ID"]).Trim() + "\\" + filename[0]);
                }
            }
            else
            {
                filePaths = new string[0];
            }
            if (filePaths.Length > 0)
            {
                DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriveronWaitingDetailsWithOutStstusId", new object[] { Convert.ToInt64(ViewState["ID"]) });
                DataSet ds1 = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadEmailsByLoadId", new object[] { Convert.ToInt64(ViewState["ID"]) });
                if (img.CommandArgument == "4")
                {
                    if (ds.Tables.Count == 5 && ds.Tables[4].Rows.Count > 0 && Convert.ToString(ds.Tables[4].Rows[0][0]).Trim() != "0.00" && Convert.ToString(ds.Tables[4].Rows[0][0]).Trim() != "0")
                    {
                        string toAddress = string.Empty;
                        if (ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
                        {
                            toAddress = !string.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0][0])) ? Convert.ToString(ds1.Tables[0].Rows[0][0]) : !string.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0][1])) ? Convert.ToString(ds1.Tables[0].Rows[0][1]) : Convert.ToString(ds.Tables[1].Rows[0][0]);
                        }
                        else
                        {
                            toAddress = Convert.ToString(ds.Tables[1].Rows[0][0]);
                        }
                        try
                        {
                            //CommonFunctions.SendEmail(GetFromXML.FromAccessrial, toAddress, GetFromXML.AdminEmail, "", GetAccerialSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetAccerialChargesBody(ds), null, filePaths);
                            CommonFunctions.SendEmail(GetFromXML.FromAccessrial, toAddress, "", "", GetAccerialSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetAccerialChargesBody(ds), null, filePaths);
                        }
                        catch (Exception ex)
                        { }

                    }
                }
                else if (img.CommandArgument == "7")
                {
                    string podAddress = string.Empty;
                    if (ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
                    {
                        podAddress = !string.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0][2])) ? Convert.ToString(ds1.Tables[0].Rows[0][2]) : Convert.ToString(ds1.Tables[0].Rows[0][3]);
                    }
                    if (!string.IsNullOrEmpty(podAddress))
                    {
                        try
                        {
                            //CommonFunctions.SendEmail(GetFromXML.FromAccessrial, podAddress, GetFromXML.AdminEmail, "", GetPODSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetBodyPODDetails(), null, filePaths);
                            CommonFunctions.SendEmail(GetFromXML.FromAccessrial, podAddress, "", "", GetPODSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetBodyPODDetails(), null, filePaths);
                        }
                        catch (Exception ex)
                        { }
                    }
                }

                //SLine 2016 ---- Added a new condition block for sending mail/ Invoice
                #region TempCommentedForConfirmation
                /*else if (img.CommandArgument == "11")
                {
                    string podAddress = string.Empty;
                    if (ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
                    {
                        podAddress = !string.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0][2])) ? Convert.ToString(ds1.Tables[0].Rows[0][2]) : Convert.ToString(ds1.Tables[0].Rows[0][3]);
                    }
                    if (!string.IsNullOrEmpty(podAddress))
                    {
                        try
                        {
                            //CommonFunctions.SendEmail(GetFromXML.FromAccessrial, podAddress, GetFromXML.AdminEmail, "", GetPODSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetBodyPODDetails(), null, filePaths);
                            CommonFunctions.SendEmail(GetFromXML.FromAccessrial, podAddress, "", "", GetPODSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetBodyPODDetails(), null, filePaths);
                        }
                        catch (Exception ex)
                        { }
                    }
                }*/

                #endregion

            }

        }
    }
    protected void SendFaxWithFiles(object sender, ImageClickEventArgs e)
    {
        ImageButton img = (ImageButton)sender;
        List<string> filePaths = new List<string>();
        DataTable dt = DBClass.returnDataTable("select [nvar_DocumentName],isnull([bint_FileNo],0),[date_ReceivedDate] from [eTn_UploadDocuments] where [bint_LoadId]=" + Convert.ToInt64(ViewState["ID"]) + " and [bint_DocumentId]=" + Int64.Parse(img.CommandArgument) + " and [nvar_DocumentName] is not null");
        if (dt != null && dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string[] filename = Convert.ToString(dt.Rows[i][0]).ToString().Split('^');
                if (File.Exists(Server.MapPath("../Documents" + "\\" + Convert.ToString(ViewState["ID"]).Trim() + "\\" + filename[0])))
                {
                    filePaths.Add(Server.MapPath("../Documents" + "\\" + Convert.ToString(ViewState["ID"]).Trim() + "\\" + filename[0]));
                }
            }
        }
        if (filePaths.Count > 0)
        {
            pnlSendFax.Visible = true;
            ViewState["Docs"] = filePaths;
        }
        else
        {
            pnlSendFax.Visible = false;
        }
    }
    protected void lnkCancelFax_click(object sender, EventArgs e)
    {
        pnlSendFax.Visible = false;
    }
    protected void lnkSendFax_click(object sender, EventArgs e)
    {
        try
        {
            //SLine 2017 Enhancement for Office Location Starts
            long officeLocationId = 0;
            if (Session["OfficeLocationID"] != null)
            {
                officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
            }
            //SLine 2017 Enhancement for Office Location Ends
            List<string> filesList = (List<string>)ViewState["Docs"];
            lblFaxError.Text = string.Empty;
            foreach (string file in filesList)
            {
                string fileType = file.Substring(file.LastIndexOf('.') + 1, (file.Length - file.LastIndexOf('.') - 1));
                string fileName = file.Substring(file.LastIndexOf('\\') + 1, (file.Length - file.LastIndexOf('\\') - 1));
                //SLine 2017 Enhancement for Office Location
                long status = CommonFunctions.SendFax(ddlCountryFax.SelectedValue + PhFax.Text, txtuUernameFax.Text, Int64.Parse(Session["UserLoginId"].ToString()), File.ReadAllBytes(file), fileType, Convert.ToInt64(ViewState["ID"]), fileName, officeLocationId);

                if (status <= 0)
                {
                    lblFaxError.Text = lblFaxError.Text + (string.IsNullOrEmpty(lblFaxError.Text) ? string.Empty : ", ") + file;
                }
            }
            if (!string.IsNullOrEmpty(lblFaxError.Text))
            {
                lblFaxError.Text = "File(s) " + lblFaxError.Text + "not send.";
            }
            else
            {
                pnlSendFax.Visible = false;
            }
        }
        catch (Exception ex)
        {
            lblFaxError.Text = "Fax sending failure. ";
            lblFaxError.Visible = true;
        }
    }
    private string GetAccerialChargesBody(DataSet ds)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();
        strBody.Append("<%@ Page AutoEventWireup=true CodeFile='AccessorialCharges.aspx.cs' Inherits='AccessorialCharges' Language='C#' MasterPageFile='~/Manager/MasterPage.master' %>");
        strBody.Append("<asp:Content ID='Content1' ContentPlaceHolderID='ContentPlaceHolder1' Runat='Server'>");
        strBody.Append("<table width='100%' border='0' cellpadding='3' cellspacing='0' id='ContentTbl'><tr valign='top'><td><table width='100%' border='0' cellspacing='0' cellpadding='0'>");
        strBody.Append("<tr><td align='center' class='head1'>ACCEPTANCE FORM <br/> Accessorial Charges </td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img alt='' height='10' src='../Images/pix.gif' width='1'/></td></tr></table>");
        strBody.Append("<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td style='background-color: #CCCCCC; left: 10px;'><img src='../Images/pix.gif' width='1' height='1' /></td></tr></table>");
        strBody.Append("<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td><img src='../Images/pix.gif' alt=' width='1' height='10' /></td></tr></table>");
        strBody.Append("<table border='0' cellpadding='0' cellspacing='0' style='padding-right: 5px; padding-left: 15px;height: 20px; padding-top: 1px; padding-bottom: 1px;' width='100%'><tr><td width='50%' valign='top'>");
        strBody.Append("<table border='0' cellpadding='0' cellspacing='0' style='font-family: Arial, sans-serif;color: #4D4B4C; font-size: 12px;' width='100%'><tr><td width='30%' valign='top'><p><strong>Bill To:</strong><br />");
        strBody.Append("<strong>" + Convert.ToString(ds.Tables[2].Rows[0][0]) + "</strong><br/>" + Convert.ToString(ds.Tables[2].Rows[0][1]) + "  " + Convert.ToString(ds.Tables[2].Rows[0][2]) + "<br/>" + Convert.ToString(ds.Tables[2].Rows[0][3]) + ", " + Convert.ToString(ds.Tables[2].Rows[0][4]) + " " + Convert.ToString(ds.Tables[2].Rows[0][5]) + Convert.ToString(ds.Tables[2].Rows[0][16]) + Convert.ToString(ds.Tables[2].Rows[0][17]));
        strBody.Append("<br /><br/><strong>Billing Ref.# </strong>:" + Convert.ToString(ds.Tables[2].Rows[0][6]) + "</p></td>");
        strBody.Append("<td valign='top'><strong>Load Number&nbsp;:&nbsp;" + Convert.ToString(ViewState["ID"]) + "</strong><br />");
        strBody.Append("Type&nbsp;: " + Convert.ToString(ds.Tables[2].Rows[0][7]) + "<br />");
        strBody.Append("Commodity&nbsp;: " + Convert.ToString(ds.Tables[2].Rows[0][8]) + "<br />");
        strBody.Append("Booking#&nbsp;: " + Convert.ToString(ds.Tables[2].Rows[0][9]) + "<br />");
        strBody.Append("Container#&nbsp;:&nbsp;" + Convert.ToString(ds.Tables[2].Rows[0][10]) + "<br />");
        strBody.Append("Chassis#&nbsp;:&nbsp;" + Convert.ToString(ds.Tables[2].Rows[0][12]) + "<br />");
        strBody.Append("Remarks&nbsp;:&nbsp;" + Convert.ToString(ds.Tables[2].Rows[0][14]) + "<br />");
        strBody.Append("Description&nbsp;:&nbsp;" + Convert.ToString(ds.Tables[2].Rows[0][15]) + "</td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0' style='font-family: Arial, sans-serif;color: #4D4B4C; font-size: 12px;'><tr><td><img src='../Images/pix.gif' alt='' width='1' height='10' /></td></tr><tr id='row'><td align=left><strong>" + GetFromXML.CompanyName + "<br/>" + GetFromXML.CompanyPhone + "</strong></td>");
        //strBody.Append("<td align='right'><strong>" + ConfigurationSettings.AppSettings["CompanyPhone"] + "&nbsp;&nbsp;&nbsp;" + ConfigurationSettings.AppSettings["CompanyFax"] + "</strong></td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0' id='TABLE1'><tr><td bgcolor='#CCCCCC'><img src='../Images/pix.gif' alt='' width='1' height='1' /></td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img src='../Images/pix.gif' alt='' width='1' height='10'/></td></tr></table>");
        strBody.Append("<table border='0' cellpadding='0' cellspacing='0' style='font-family: Arial, sans-serif;color: #4D4B4C; font-size: 12px;' width='100%'>");
        strBody.Append("<tr><td valign='top'>Dear Customer,<br />Our company request authorization for the additional charges. please sign below and " + GetFromXML.CompanyFax + " with in 24 hrs after receiving this form.</td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img src='../Images/pix.gif' alt=' width='1' height='10' /></td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td bgcolor='#CCCCCC'><img src='../Images/pix.gif' alt='' width='1' height='1' /></td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img src='../Images/pix.gif' alt='' width='1' height='10'/></td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td>" + DesginPaybleDetails(ds.Tables[3]) + "</td></tr></table>");
        //need to write code for charges
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img src='../Images/pix.gif' alt=' width='1' height=10'/></td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td bgcolor='#CCCCCC'><img src='../Images/pix.gif' alt='' width='1' height='1'/></td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img src='../Images/pix.gif' alt='' width='1' height='25' /></td></tr></table>");
        strBody.Append("<table border='0' cellpadding='0' cellspacing='0' style='font-family: Arial, sans-serif;font-size: 12px;' width='100%'><tr><td width='50%' valign='top'><p>Authorized by (Signature) : ________________________ </p></td><td valign='top'>Date : ________________________ </td></tr><tr><td valign='top'></td><td valign='top'></td></tr><tr><td><img alt='' height='25' src='../Images/pix.gif' width='1' /></td></tr>");
        strBody.Append("<tr><td valign='top'>Name : ________________________ </td><td valign='top'></td></tr></table></td></tr></table></td></tr></table></asp:Content>");
        return strBody.ToString();
    }
    private string GetAccerialSubject(string LoadID, string Container, string RefNo)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();

        strBody.Append("Accessorial Charges for Load# " + LoadID);
        if (RefNo.Length > 0)
            strBody.Append(" ,  Ref #  " + RefNo);
        if (Container.Length > 0)
            strBody.Append(" , Container# " + Container);

        return strBody.ToString();
    }
    private string GetPODSubject(string LoadID, string Container, string RefNo)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();

        strBody.Append("Re ");
        if (Container.Length > 0)
            strBody.Append("unit # " + Container);
        if (RefNo.Length > 0)
            strBody.Append(" ,  Ref #  " + RefNo);
        strBody.Append(". Proof of delivery is attached.");

        return strBody.ToString();
    }
    private string GetBodyPODDetails()
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();
        strBody.Append("Dear Valued Customer, <br/>");
        strBody.Append("<p>Please see attached proof of delivery. Your load is delivered.</p>");
        strBody.Append("<p>We know you have a choice of carriers. Thanks for choosing " + ConfigurationSettings.AppSettings["CompanyName"].ToString() + ". Please choose " + ConfigurationSettings.AppSettings["CompanyName"].ToString() + " for your future transportation needs.</p>");
        strBody.Append("<p></p>Regards<br/>" + GetFromXML.CompanyName + "<br/>Dispatch Dept.<br/>" + GetFromXML.CompanyPhone + "<br/>" + GetFromXML.CompanyFax);
        return strBody.ToString();
    }
    public string DesginPaybleDetails(DataTable dtPayables)
    {
        System.Text.StringBuilder strCode = new System.Text.StringBuilder();
        strCode.Append("");
        if (dtPayables.Rows.Count > 0)
        {
            bool blVal = false;
            for (int i = 0, j = 0; i < dtPayables.Columns.Count; i++)
            {
                #region Validations
                blVal = false;
                if (Convert.IsDBNull(dtPayables.Rows[0][i]))
                    dtPayables.Rows[0][i] = 0;
                if (Convert.ToString(dtPayables.Rows[0][i]).Length == 0)
                    dtPayables.Rows[0][i] = 0;
                try
                {
                    if (Convert.ToDouble(dtPayables.Rows[0][i]) > 0)
                    {
                        blVal = true;
                    }
                }
                catch
                {
                    if (Convert.ToString(dtPayables.Rows[0][i]).Trim().Length > 0)
                    {
                        blVal = true;
                    }
                }
                #endregion
                if (blVal)
                {
                    strCode.Append("<tr id=" + (((j % 2) == 0) ? "row" : "altrow") + " >");
                    if (i < (dtPayables.Columns.Count - 1))
                        strCode.Append("<td align=right width=30%>" + dtPayables.Columns[i].ColumnName + "</td>");
                    else
                        strCode.Append("<td align=right width=30%><b>" + dtPayables.Columns[i].ColumnName + "</b></td>");
                    strCode.Append("<td align=left>" + Convert.ToString(dtPayables.Rows[0][i]).Trim() + "</td>");
                    strCode.Append("</tr>");
                    j = j + 1;
                }
            }
            if (strCode.ToString().Length > 0)
            {
                strCode.Insert(0, "<table width=100% border=0 cellspacing=0 cellpadding=0 id=tblform> <tr><th colspan=2>Accessorial Charges($)</th></tr>");
                strCode.Append("</table>");
            }
            else
            {
                strCode.Append("<table width=100% border=0 cellspacing=0 cellpadding=0 id=tblform> <tr><th colspan=2>Accessorial Charges($)</th></tr>");
                strCode.Append("<tr id=row>");
                strCode.Append("<td align=center width=30%><b>No Charges Available for Load : " + Convert.ToString(ViewState["ID"]) + " </b></td>");
                strCode.Append("</tr>");
                strCode.Append("</table>");
            }
        }
        else
        {
            strCode.Append("<table width=100% border=0 cellspacing=0 cellpadding=0 id=tblform> <tr><th colspan=2>Accessorial Charges($)</th></tr>");
            strCode.Append("<tr id=row>");
            strCode.Append("<td align=center width=30%><b>No Charges Available for Load : " + Convert.ToString(ViewState["ID"]) + " </b></td>");
            strCode.Append("</tr>");
            strCode.Append("</table>");
        }
        return strCode.ToString();
    }
    #region SLine 2016 Enhancements for Showing Upload Load Status in TRACK-LOAD-PAGE
    //SLine 2016 Enhancements

    protected void ShowInvoicePdf(object sender, EventArgs e)
    {
        #region Old
        //try
        //{
        //    string strDocumentName = DBClass.executeScalar("select [nvar_DocumentName] as DocumentName from [eTn_UploadDocuments] where [bint_LoadId] = " + Convert.ToInt64(ViewState["ID"]) + " and [bint_DocumentId] = 11 and [bint_FileNo] = 1");
        //    string[] strName = strDocumentName.Split('^');
        //    string strFilePath = Server.MapPath("~/Documents/" + Convert.ToInt64(ViewState["ID"]) + "\\" + strName[0]);
        //    WebClient client = new WebClient();
        //    Byte[] buffer = client.DownloadData(strFilePath);
        //    Response.ContentType = "application/pdf";
        //    Response.AddHeader("content-length", buffer.Length.ToString());
        //    Response.BinaryWrite(buffer);


        //    //HttpContext.Current.Response.Clear();
        //    //HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=a.pdf");
        //    //HttpContext.Current.Response.Charset = "";
        //    //HttpContext.Current.Response.ContentType = "application/pdf";
        //    //StringWriter stringwrite = new StringWriter();
        //    //HtmlTextWriter htmlwrite = new HtmlTextWriter(stringwrite);

        //    //repeater1.RenderControl(htmlwrite);
        //    //StringReader sr = new StringReader(stringwrite.ToString());
        //    //Document pdfDoc = new Document(PageSize.A4, 50f, 50f, 40f, 40f);
        //    //PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        //    //HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        //    //pdfDoc.Open();
        //    //htmlparser.Parse(sr);
        //    //pdfDoc.NewPage();
        //    //pdfDoc.Close();
        //}
        //catch
        //{
        //    //Response.Write("<script>alert('Invoice not saved as pdf.')</script>");
        //} 
        #endregion
        GetDocumentsbyID(11);


    }

    protected void dropStatus_SelectedIndexChanged(object source, EventArgs e)
    {
        DropDownList ddl = (DropDownList)source;
        if ((ddl.SelectedValue == "8" || ddl.SelectedValue == "7" || ddl.SelectedValue == "12" || ddl.SelectedValue == "14") && ViewState["SendMails"] != null && bool.Parse(ViewState["SendMails"].ToString()))
        {
            ChkSendMail.Visible = true;
            ChkSendMail.Checked = true;
        }
        else
        {
            ChkSendMail.Visible = false;
            ChkSendMail.Checked = false;
        }
    }

    protected void dgrLoad_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (string.Compare(e.CommandName, "ADD", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            if (!Page.IsValid)
                return;
            if (Page.IsPostBack)
            {
                DatePicker Dpicker = (DatePicker)e.Item.FindControl("txtDate");
                TimePicker Tpicker = (TimePicker)e.Item.FindControl("txtTime");
                if (!Dpicker.IsValidDate)
                    return;

                Label lbl1 = (Label)dgrLoad.Items[1].FindControl("lblID");
                if (dgrLoad.Items.Count > 1)
                {
                    if (Convert.ToDateTime(DBClass.executeScalar("Select top 1 date_CreateDate from eTn_TrackLoad where bint_LoadId = " +
                        "(select bint_LoadId from eTn_TrackLoad where bint_TrackLoadId = " + lbl1.Text.Trim() + ") order by date_CreateDate desc ")) >= Convert.ToDateTime(Dpicker.Date + " " + Tpicker.Time.Trim()))
                    {
                        Response.Write("<script>alert('Date entered must be greater than the date of previous status dates.')</script>");
                        return;
                    }
                }

                DropDownList dname = (DropDownList)e.Item.FindControl("dropstatus");
                TextBox Cloc = (TextBox)e.Item.FindControl("txtLoc");
                TextBox Cnotes = (TextBox)e.Item.FindControl("txtComments");

                if (dname.SelectedItem.Text == "Driver On Waiting")
                {
                    if (Convert.ToInt32(DBClass.executeScalar("Select count(bint_LoadId) from eTn_TrackLoad where bint_LoadId = " + Convert.ToInt64(ViewState["ID"]) + " and bint_LoadStatusId=(select bint_LoadStatusId from eTn_LoadStatus where Lower(nvar_LoadStatusDesc)='reached destination')")) == 0)
                    {
                        dname.SelectedIndex = dname.Items.IndexOf(dname.Items.FindByText("Reached Destination"));
                        Response.Write("<script>alert('Driver On Waiting status can only be updated when Reached @ Destination status is entered.')</script>");
                        return;
                    }
                }

                if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_Insert_Update_TrackLoad", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(dname.SelectedItem.Value),
                 Cloc.Text.Trim(),CommonFunctions.CheckDateTimeNull((Dpicker.Date !=null)?Dpicker.Date+" "+Tpicker.Time.Trim():null),Cnotes.Text.Trim(),Convert.ToInt64(Session["UserLoginId"]),"I"}) > 0)
                {
                    ChangeLoadStatus cls = new ChangeLoadStatus();
                    cls.UpdateLoadStatusToPostman(Convert.ToInt64(ViewState["ID"]), dname.SelectedItem.Text, (DateTime)CommonFunctions.CheckDateTimeNull((Dpicker.Date != null) ? Dpicker.Date + " " + Tpicker.Time.Trim() : null));
                    DataSet ds = null;
                    if (string.Compare(dname.SelectedItem.Text, "Pickedup", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                    {
                        if (Convert.ToInt64(ViewState["ID"]) <= (Convert.ToInt64(ConfigurationSettings.AppSettings["LastLoadId"])))
                        {
                            string[] Values = Convert.ToString(DBClass.executeScalar("select Case when Len([eTn_Load].[nvar_Container])>0 and Len([eTn_Load].[nvar_Container1])>0 then [eTn_Load].[nvar_Container] when Len([eTn_Load].[nvar_Container])>0 and Len([eTn_Load].[nvar_Container1])=0 then [eTn_Load].[nvar_Container] when Len([eTn_Load].[nvar_Container1])>0 then [eTn_Load].[nvar_Container1] else '' end+'^'+Case when Len([eTn_Load].[nvar_Chasis])>0 and Len([eTn_Load].[nvar_Chasis1])>0 then [eTn_Load].[nvar_Chasis] when Len([eTn_Load].[nvar_Chasis])>0 and Len([eTn_Load].[nvar_Chasis1])=0 then [eTn_Load].[nvar_Chasis] when Len([eTn_Load].[nvar_Chasis1])>0 then [eTn_Load].[nvar_Chasis1] else '' end+'^'+(isnull((select top 1 case when Len(nvar_Email)>0 then nvar_Email else '' end from eTn_ShippingContacts where bint_ShippingId=[eTn_Load].bint_ShippingId),(isnull((select top 1 case when Len(nvar_Email)>0 then nvar_Email else '' end from eTn_Shipping where bint_ShippingId=[eTn_Load].bint_ShippingId),'')))) from [eTn_Load] where [eTn_Load].[bint_LoadId]=" + Convert.ToInt64(ViewState["ID"]))).Trim().Split('^');
                            if (Values != null)
                            {
                                //CommonFunctions.SendEmail(GetFromXML.AdminEmail, (Values.Length == 3 ? Values[2] : ""), GetFromXML.AdminEmail, "", "Pre Authorization for empty UNIT # " + Values[0]+" ( OffHire ) ", GetPickedUpBodyDetails(Values[0], Values[1]), new string[] { "<font color=red>This email was not sent to the shipping line,because Contact email address is not available for this shipping line.</font><br/> " },null);
                                CommonFunctions.SendEmail(GetFromXML.AdminEmail, (Values.Length == 3 ? Values[2] : ""), GetFromXML.CCPickup, "", "Pre Authorization for empty UNIT # " + Values[0] + " ( OffHire ) ", GetPickedUpBodyDetails(Values[0], Values[1]), new string[] { "<font color=red>This email was not sent to the shipping line,because Contact email address is not available for this shipping line.</font><br/> " }, null);
                            }
                            Values = null;
                        }
                    }
                    else if (string.Compare(dname.SelectedItem.Text, "Driver On Waiting", true, System.Globalization.CultureInfo.CurrentCulture) == 0 && ChkSendMail.Checked)
                    {
                        ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriveronWaitingDetails", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(dname.SelectedItem.Value) });
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            //CommonFunctions.SendEmail(GetFromXML.FromDriveronWaiting, Convert.ToString(ds.Tables[1].Rows[0][0]),GetFromXML.AdminEmail + ";" + GetFromXML.CcEmail, "", GetSubject(ds.Tables[0].Rows[0][0].ToString(), ds.Tables[0].Rows[0][1].ToString()), GetBodyDetails(ds.Tables[0].Rows[0][0].ToString(), ds.Tables[0].Rows[0][1].ToString(), ds.Tables[0].Rows[0][2].ToString()), null,null);
                            CommonFunctions.SendEmail(GetFromXML.FromDriveronWaiting, Convert.ToString(ds.Tables[1].Rows[0][0]), GetFromXML.CCDriveronWaiting, "", GetSubject(ds.Tables[0].Rows[0][0].ToString(), ds.Tables[0].Rows[0][1].ToString()), GetBodyDetails(ds.Tables[0].Rows[0][0].ToString(), ds.Tables[0].Rows[0][1].ToString(), ds.Tables[0].Rows[0][2].ToString()), null, null);
                        }
                    }
                    else if (string.Compare(dname.SelectedItem.Text, "Delivered", true, System.Globalization.CultureInfo.CurrentCulture) == 0 && ChkSendMail.Checked)
                    {
                        string[] filePaths;
                        DataTable dt = DBClass.returnDataTable("select [nvar_DocumentName],isnull([bint_FileNo],0),[date_ReceivedDate] from [eTn_UploadDocuments] where [bint_LoadId]=" + Convert.ToInt64(ViewState["ID"]) + " and [bint_DocumentId]=" + 4 + " and [nvar_DocumentName] is not null");
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            filePaths = new string[dt.Rows.Count];
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                filePaths[i] = Server.MapPath("../Documents" + "\\" + Convert.ToString(ViewState["ID"]).Trim() + "\\" + Convert.ToString(dt.Rows[i][0]));
                            }
                        }
                        else
                        {
                            filePaths = new string[0];
                        }
                        ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriveronWaitingDetails", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(dname.SelectedItem.Value) });
                        DataSet ds1 = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadEmailsByLoadId", new object[] { Convert.ToInt64(ViewState["ID"]) });
                        if (ds.Tables.Count == 5 && ds.Tables[4].Rows.Count > 0 && Convert.ToString(ds.Tables[4].Rows[0][0]).Trim() != "0.00" && Convert.ToString(ds.Tables[4].Rows[0][0]).Trim() != "0")
                        {
                            string toAddress = string.Empty;
                            if (ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
                            {
                                toAddress = !string.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0][0])) ? Convert.ToString(ds1.Tables[0].Rows[0][0]) : !string.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0][1])) ? Convert.ToString(ds1.Tables[0].Rows[0][1]) : Convert.ToString(ds.Tables[1].Rows[0][0]);
                            }
                            else
                            {
                                toAddress = Convert.ToString(ds.Tables[1].Rows[0][0]);
                            }
                            try
                            {
                                //CommonFunctions.SendEmail(GetFromXML.FromAccessrial, toAddress, GetFromXML.AdminEmail, "", GetAccerialSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetAccerialChargesBody(ds),null ,filePaths);
                                CommonFunctions.SendEmail(GetFromXML.FromAccessrial, toAddress, "", "", GetAccerialSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetAccerialChargesBody(ds), null, filePaths);
                            }
                            catch (Exception ex)
                            { }

                        }

                        string toDeliverAddress = string.Empty;
                        if (ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
                        {
                            toDeliverAddress = !string.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0][4])) ? Convert.ToString(ds1.Tables[0].Rows[0][4]) : Convert.ToString(ds1.Tables[0].Rows[0][5]);
                        }
                        if (!string.IsNullOrEmpty(toDeliverAddress) && ChkSendMail.Checked)
                        {
                            try
                            {
                                //CommonFunctions.SendEmail(GetFromXML.FromAccessrial, toDeliverAddress, GetFromXML.AdminEmail, "", GetDeliveredSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetBodyDeliveredDetails(ds.Tables[0].Rows[0][0].ToString(), ds.Tables[0].Rows[0][1].ToString(), ds.Tables[0].Rows[0][2].ToString(), ds.Tables[0].Rows[0][3].ToString()), null, null);
                                CommonFunctions.SendEmail(GetFromXML.FromAccessrial, toDeliverAddress, "", "", GetDeliveredSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetBodyDeliveredDetails(ds.Tables[0].Rows[0][0].ToString(), ds.Tables[0].Rows[0][1].ToString(), ds.Tables[0].Rows[0][2].ToString(), ds.Tables[0].Rows[0][3].ToString()), null, null);
                            }
                            catch (Exception ex)
                            { }
                        }
                        //ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriveronWaitingDetails", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(dname.SelectedItem.Value) });
                        //if (ds.Tables.Count == 5 && ds.Tables[4].Rows.Count > 0 && Convert.ToString(ds.Tables[4].Rows[0][0]).Trim() != "0.00" && Convert.ToString(ds.Tables[4].Rows[0][0]).Trim() != "0")
                        //{
                        //    CommonFunctions.SendEmail(GetFromXML.FromAccessrial, Convert.ToString(ds.Tables[1].Rows[0][0]), GetFromXML.AdminEmail, "", GetAccerialSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetAccerialChargesBody(ds), null);
                        //}
                    }

                    //SLine 2016 Enhancements
                    // Adding "Send Invoice Via Email" feature when the Load is Closed
                    #region "Closed"

                    else if (string.Compare(dname.SelectedItem.Text, "Closed", true, System.Globalization.CultureInfo.CurrentCulture) == 0 && ChkSendMail.Checked)
                    {
                        string[] filePaths;
                        DataTable dt = DBClass.returnDataTable("select [nvar_DocumentName],isnull([bint_FileNo],0),[date_ReceivedDate] from [eTn_UploadDocuments] where [bint_LoadId]=" + Convert.ToInt64(ViewState["ID"]) + " and [bint_DocumentId]=" + 7 + " and [nvar_DocumentName] is not null");
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            filePaths = new string[dt.Rows.Count];
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                filePaths[i] = Server.MapPath("../Documents" + "\\" + Convert.ToString(ViewState["ID"]).Trim() + "\\" + Convert.ToString(dt.Rows[i][0]));
                            }
                        }
                        else
                        {
                            filePaths = new string[0];
                        }
                        ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriveronWaitingDetails", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(dname.SelectedItem.Value) });
                        DataSet ds1 = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadEmailsByLoadId", new object[] { Convert.ToInt64(ViewState["ID"]) });
                        if (filePaths.Length > 0)
                        {
                            string podAddress = string.Empty;
                            if (ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
                            {
                                podAddress = !string.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0][2])) ? Convert.ToString(ds1.Tables[0].Rows[0][2]) : Convert.ToString(ds1.Tables[0].Rows[0][3]);
                            }
                            if (!string.IsNullOrEmpty(podAddress))
                            {
                                try
                                {
                                    //CommonFunctions.SendEmail(GetFromXML.FromAccessrial, podAddress, GetFromXML.AdminEmail, "", GetPODSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetBodyPODDetails(), null, filePaths);

                                    //SLine 2016 Enhancements
                                    #region Revoikng the Changes before Phase 1 ---- Hosting
                                    // as per the New Functionalities, when the Invoice is being sent, the POD Email is not to be sent
                                    //hence commenting the below code line 
                                    #endregion
                                    CommonFunctions.SendEmail(GetFromXML.FromAccessrial, podAddress, "", "", GetPODSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetBodyPODDetails(), null, filePaths);
                                    //SLine 2016 Enhancements
                                }
                                catch (Exception ex)
                                { }
                            }
                        }

                        //SLine 2016 Enhancements for Automatic Invoice via Email ---- Implementation block
                        //Checking if the Customer has opted for Send Invoice via Email, if yes all the four docs will be made into a single pdf file
                        #region Revoikng the Changes before Phase 1 ---- Hosting And COMMENTED BELOW CODE BLOCK 
                        /* if (!Convert.IsDBNull(ds1.Tables[0].Rows[0]["nvar_BillingEmail"]))
                         {
                             //Getting the Document names for that LoadId
                             DataSet ds_GetDocumentNames = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDocumentNames", new object[] { Convert.ToInt64(ViewState["ID"]) });

                             //Merging or Making a Single Pdf for all the 3-4 Documents and getting the name of the Invoice for that Customer/LoadId
                             string[] str_InvoicePath = new string[1];
                             str_InvoicePath[0] = Server.MapPath("../Documents" + "\\") + CommonFunctions.MergeAllPdf(ds_GetDocumentNames, Server.MapPath("../Documents" + "\\" + Convert.ToInt64(ViewState["ID"])), Convert.ToInt64(ViewState["ID"]));
                             //Sending the Mail
                             CommonFunctions.SendEmail(GetFromXML.FromAccessrial, Convert.ToString(ds1.Tables[0].Rows[0]["nvar_BillingEmail"]), "", "", GetInvoiceSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetInvoiceBodyDetails(), null, str_InvoicePath);
                         }  */
                        #endregion
                        //SLine 2016 Enhancements for Automatic Invoice via Email ---- Implementation block
                    }
                    #endregion
                    //SLine 2016 Enhancements

                    else if (string.Compare(dname.SelectedItem.Text, "Reached Destination", true, System.Globalization.CultureInfo.CurrentCulture) == 0 && ChkSendMail.Checked)
                    {
                        ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriveronWaitingDetails", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(dname.SelectedItem.Value) });

                        try
                        {
                            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                            {
                                //CommonFunctions.SendEmail(GetFromXML.FromDriveronWaiting, Convert.ToString(ds.Tables[1].Rows[0][0]), GetFromXML.AdminEmail, "", GetReachedDestinationSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetBodyReachedDestinationDetails(ds.Tables[0].Rows[0][0].ToString(), ds.Tables[0].Rows[0][1].ToString(), ds.Tables[0].Rows[0][2].ToString()), null, null);
                                CommonFunctions.SendEmail(GetFromXML.FromDriveronWaiting, Convert.ToString(ds.Tables[1].Rows[0][0]), "", "", GetReachedDestinationSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetBodyReachedDestinationDetails(ds.Tables[0].Rows[0][0].ToString(), ds.Tables[0].Rows[0][1].ToString(), ds.Tables[0].Rows[0][2].ToString()), null, null);
                            }
                        }
                        catch (Exception ex)
                        { }
                    }

                    AssignLinks(dname.SelectedItem.Text);
                    if (ds != null) { ds.Dispose(); ds = null; }
                }
                //CheckBackPage();
                //Response.Redirect("~/Manager/DashBoard.aspx");
                //Response.Write("<Script>this.close();</script>");
                //RedirectToBackPage();
                Response.Redirect("~/Manager/dummy.aspx");
                FillDetailsLoadStatus(Convert.ToInt64(ViewState["ID"]));
            }
        }
        if (string.Compare(e.CommandName, "Delete", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            if (Page.IsPostBack)
            {
                Label lbl = (Label)e.Item.FindControl("lblID");
                if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_Delete_TrackLoad", new object[] { lbl.Text.Trim(), Convert.ToInt64(Session["UserLoginId"]) }) > 0)
                {
                    Response.Redirect("~/Manager/dummy.aspx");
                    FillDetailsLoadStatus(Convert.ToInt64(ViewState["ID"]));
                }
                //Sline Added Else con to check the reload, if the Delete is done from any other browser, and 
                //Null value insertion error occurs
                else
                    Response.Redirect("~/Manager/dummy.aspx");
            }
        }


    }

    protected void dgrLoad_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DropDownList dllStatus = (DropDownList)e.Item.FindControl("dropStatus");
            TextBox txtBox = (TextBox)e.Item.FindControl("txtLoc");
            Button btnAdd = (Button)e.Item.FindControl("btnAdd");
            //if (dllStatus != null && txtBox != null && btnAdd != null)
            //{
            //    btnAdd.Attributes.Add("onClick", "Javascript:return ValidateControls('" + dllStatus.ClientID + "','" + txtBox.ClientID + "');");
            //}
            dllStatus = null; btnAdd = null; txtBox = null;
        }
    }

    private string GetSubject(string Container, string RefNo)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();

        strBody.Append("Regarding ");
        if (Container.Length > 0)
            strBody.Append(" Container# " + Container);
        if (RefNo.Length > 0 && Container.Length > 0)
            strBody.Append(" and  Ref #  " + RefNo);
        strBody.Append(" Driver is on detention");

        return strBody.ToString();
    }

    private string GetDeliveredSubject(string LoadID, string Container, string RefNo)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();

        strBody.Append("Re ");
        if (Container.Length > 0)
            strBody.Append("unit # " + Container);
        if (RefNo.Length > 0)
            strBody.Append(" ,  Ref #  " + RefNo);
        strBody.Append(" Delivered");

        return strBody.ToString();
    }

    private string GetReachedDestinationSubject(string LoadID, string Container, string RefNo)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();

        strBody.Append("Re ");
        if (Container.Length > 0)
            strBody.Append("unit # " + Container);
        if (RefNo.Length > 0)
            strBody.Append(" ,  Ref #  " + RefNo);
        strBody.Append(" Driver reached destination ");

        return strBody.ToString();
    }

    private void FillDetailsLoadStatus(long loadId)
    {
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "sp_GetAllStatusInfoByLoadId", new object[] { loadId });
        if (ds.Tables.Count > 0)
        {
            #region Dummy Row Addition
            DataRow dr = ds.Tables[0].NewRow();
            for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
            {
                if (ds.Tables[0].Columns[i].DataType == typeof(string))
                {
                    dr[i] = "test";
                }
                else if (ds.Tables[0].Columns[i].DataType == typeof(System.Int64))
                {
                    dr[i] = 0;
                }
                else if (ds.Tables[0].Columns[i].DataType == typeof(DateTime))
                {
                    dr[i] = DateTime.Now;
                }
            }
            ds.Tables[0].Rows.InsertAt(dr, 0);
            ds.Tables[0].AcceptChanges();
            dr = null;
            #endregion
            if (ds.Tables[0].Rows.Count > 0)
            {
                dgrLoad.DataSource = ds.Tables[0].Copy();
                dgrLoad.DataBind();
            }
        }
        if (ds != null) { ds.Dispose(); ds = null; }


        //SLine 2016 Enhancements
        //Added a new column to the Grid
        #region Formatting Grid
        if (dgrLoad.Items.Count > 0)
        {
            for (int i = 0; i < dgrLoad.Items.Count; i++)
            {
                foreach (Control ctl in dgrLoad.Items[i].Cells[0].Controls)
                {

                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.Label")
                    {
                        ctl.Visible = (i == 0) ? false : true;
                    }

                }

                foreach (Control ctl in dgrLoad.Items[i].Cells[1].Controls)
                {
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.DropDownList")
                    {
                        ctl.Visible = (i == 0) ? true : false;
                    }
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.Label")
                    {
                        ctl.Visible = (i == 0) ? false : true;
                    }
                }
                foreach (Control ctl in dgrLoad.Items[i].Cells[2].Controls)
                {
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.TextBox")
                    {
                        ctl.Visible = (i == 0) ? true : false;
                    }
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.Label")
                    {
                        ctl.Visible = (i == 0) ? false : true;
                    }
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.RequiredFieldValidator")
                    {
                        ctl.Visible = (i == 0) ? true : false;
                    }
                }
                foreach (Control ctl in dgrLoad.Items[i].Cells[3].Controls)
                {
                    if (ctl.GetType().ToString() == "ASP.usercontrols_datepicker_ascx")
                    {
                        ctl.Visible = (i == 0) ? true : false;
                    }
                    if (ctl.GetType().ToString() == "ASP.usercontrols_timepicker_ascx")
                    {
                        ctl.Visible = (i == 0) ? true : false;
                    }
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.Label")
                    {
                        ctl.Visible = (i == 0) ? false : true;
                    }
                }

                foreach (Control ctl in dgrLoad.Items[i].Cells[4].Controls)
                {
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.TextBox")
                    {
                        ctl.Visible = (i == 0) ? true : false;
                    }
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.Label")
                    {
                        ctl.Visible = (i == 0) ? false : true;
                    }
                }
                foreach (Control ctl in dgrLoad.Items[i].Cells[5].Controls)
                {
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.Button")
                    {
                        ctl.Visible = (i == 0) ? true : false;
                        //Button btn = (Button)ctl;
                        //btn.CausesValidation = (i == 0) ? true : false;
                        //btn = null;
                    }
                    if (ctl.GetType().ToString() == "System.Web.UI.WebControls.ImageButton")
                    {
                        ctl.Visible = (i == 0) ? false : true;
                        //ImageButton imgbtn = (ImageButton)ctl;
                        //imgbtn.CausesValidation = (i == 0) ? false : false;
                        //imgbtn = null;
                    }
                }
            }
        }
        //SLine 2016 Enhancements
        #endregion

        #region Format Items of LoadStatus.
        //Here the Status values from the Dropdown are removed, which are already updated for the Load
        string strQuery = "SELECT [bint_LoadStatusId] FROM [eTn_TrackLoad] where [bint_LoadId]=" + loadId;
        DataTable dt = DBClass.returnDataTable(strQuery);
        if (dt.Rows.Count > 0)
        {
            DropDownList dname = (DropDownList)dgrLoad.Items[0].Cells[0].FindControl("dropstatus");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dname.Items.Contains(dname.Items.FindByValue(Convert.ToString(dt.Rows[i][0]))))
                {
                    if (Convert.ToInt64(dt.Rows[i][0]) == 5)
                        continue;
                    dname.Items.Remove(dname.Items.FindByValue(Convert.ToString(dt.Rows[i][0])));
                }
            }
        }
        #endregion
    }

    private string GetInvoiceSubject(string LoadID, string Container, string RefNo)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();

        strBody.Append("Re ");
        if (Container.Length > 0)
            strBody.Append("unit # " + Container);
        if (RefNo.Length > 0)
            strBody.Append(" ,  Ref #  " + RefNo);
        strBody.Append(". Invoice_" + LoadID + " copy attached");

        return strBody.ToString();
    }

    private string GetBodyDetails(string Container, string RefNo, string DriverWaitingtime)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();
        strBody.Append("Hello Sir/Madam <br/><p></p>");
        strBody.Append("Regarding ");
        if (Container.Length > 0)
            strBody.Append("&nbsp;Container# " + Container);
        if (RefNo.Length > 0 && Container.Length > 0)
            strBody.Append("&nbsp;and&nbsp; Ref #&nbsp; " + RefNo);
        strBody.Append("<br/>Driver is on waiting time, <br/><br/>");
        strBody.Append("Driver Time in: " + DriverWaitingtime);
        strBody.Append("<br/>Driver   time out: Driver is still at warehouse.<br/> We will advise you when driver finishes unloading");
        strBody.Append("<p></p>Regards<br/>" + GetFromXML.CompanyName + "<br/>Dispatch Dept.<br/>" + GetFromXML.CompanyPhone + "<br/>" + GetFromXML.CompanyFax);
        return strBody.ToString();
    }

    private string GetBodyDeliveredDetails(string Container, string RefNo, string DriverWaitingtime, string DelevaryTime)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();
        strBody.Append("Hello Valued Customer , <br/><p>");
        if (Container.Length > 0)
            strBody.Append("Container# " + Container);
        if (RefNo.Length > 0 && Container.Length > 0)
            strBody.Append("&nbsp;and&nbsp; Ref #&nbsp; " + RefNo + "</p>");
        strBody.Append("Container is delivered. We will provide you p.o.d/bill of lading after we receive from the driver. Please note below driver Times, <br/><br/>");
        strBody.Append("Time in: " + FormatDate(DriverWaitingtime));
        strBody.Append("<br/>Time out: " + FormatDate(DelevaryTime));
        strBody.Append("<p>We know you have a choice of carriers. Thanks for choosing " + ConfigurationSettings.AppSettings["CompanyName"].ToString() + ". Please choose " + ConfigurationSettings.AppSettings["CompanyName"].ToString() + " for your future transportation needs.</p>");
        strBody.Append("<p></p>Regards<br/>" + GetFromXML.CompanyName + "<br/>Dispatch Dept.<br/>" + GetFromXML.CompanyPhone + "<br/>" + GetFromXML.CompanyFax);
        return strBody.ToString();
    }

    private string FormatDate(string dateTime)
    {
        if (!string.IsNullOrEmpty(dateTime.Trim()))
        {
            string[] dateTimeArr = dateTime.Split(' ');
            return dateTimeArr[0] + " <b>" + dateTimeArr[1] + " " + dateTimeArr[2] + "</b>";
        }
        else
            return string.Empty;

    }

    private string GetInvoiceBodyDetails()
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();
        strBody.Append("Dear Valued Customer, <br/>");
        strBody.Append("<p>Please find the attached copy of Invoice, including POD, Bill of Lading and Load Order.</p>");
        strBody.Append("<p>We know you have a choice of carriers. Thanks for choosing " + ConfigurationSettings.AppSettings["CompanyName"].ToString() + ". Please choose " + ConfigurationSettings.AppSettings["CompanyName"].ToString() + " for your future transportation needs.</p>");
        strBody.Append("<p></p>Regards<br/>" + GetFromXML.CompanyName + "<br/>Dispatch Dept.<br/>" + GetFromXML.CompanyPhone + "<br/>" + GetFromXML.CompanyFax);
        return strBody.ToString();
    }

    private string GetBodyReachedDestinationDetails(string Container, string RefNo, string DriverWaitingtime)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();
        strBody.Append("Valued Customer , <br/><p>");
        if (Container.Length > 0)
            strBody.Append("Please note re unit # " + Container);
        if (RefNo.Length > 0 && Container.Length > 0)
            strBody.Append("&nbsp;and&nbsp; Ref #&nbsp; " + RefNo + ". Driver reached warehouse. </p>");

        strBody.Append("Time in: " + FormatDate(DriverWaitingtime));
        strBody.Append("<p>We will advise you when driver finishes unloading. Please note that the free unloading time is 2 hrs. We will advise you if any detention on this load. Also we will send you another e-mail when container is unloaded.</p>");
        strBody.Append("<p></p>Regards<br/>" + GetFromXML.CompanyName + "<br/>Dispatch Dept.<br/>" + GetFromXML.CompanyPhone + "<br/>" + GetFromXML.CompanyFax);
        return strBody.ToString();
    }

    private string GetPickedUpBodyDetails(string Container, string Chasis)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();
        strBody.Append("Date :" + DateTime.Today.ToShortDateString() + "<br/><p></p>");
        strBody.Append("Dear Sir/Madam, <br/><p></p>");
        strBody.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Regarding ");
        if (Container.Length > 0)
            strBody.Append("&nbsp;Container# " + Container + ",");
        if (Chasis.Length > 0 && Chasis.Length > 0)
            strBody.Append("&nbsp;&nbsp;  CHASSIS #&nbsp; " + Chasis + ",");
        strBody.Append("&nbsp;Please  put this container in Terminal system for offhire. Container is arriving Oakland California soon. Also please advise us at which port we have to terminate this container when empty.");
        strBody.Append("<p>Please reply   this  e mail  asap. </p>");
        strBody.Append("</br>Container # " + Container + " </br>Chassis # " + Chasis + "</br></br><p>Your help is greatly appreciated.</p>");
        strBody.Append("<p></p>Regards<br/>" + GetFromXML.CompanyName + "<br/>Dispatch Dept.<br/>" + GetFromXML.CompanyPhone + "<br/>" + GetFromXML.CompanyFax + "<br/>E Mail:<a href='mailto:" + GetFromXML.AdminEmail + "'> " + GetFromXML.AdminEmail + "</a>");
        return strBody.ToString();
    }

    private void ShowHideGrids()
    {
        if (ViewState["LoadState"].ToString() == "New" || ViewState["LoadState"].ToString() == "Closed")
        {
            panelOldGrid.Visible = true;
            panelNewGrid.Visible = false;
        }

        else if (ViewState["LoadState"].ToString() != "New")
        {
            panelNewGrid.Visible = true;
            panelOldGrid.Visible = false;
        }
        //grid changes
    }

    private bool GetIsVerified()
    {
        bool boolIsVerified = false;
        try
        {
            return boolIsVerified = Convert.ToBoolean(DBClass.executeScalar("Select eTn_Load.bit_Is_Verified from eTn_Load where (bint_LoadId =" + ViewState["ID"] + ")"));
        }
        catch
        {
        }
        return boolIsVerified;
    }
    //SLine 2016 Enhancements 
    #endregion
    [Serializable]
    public class EdiLaodDetails
    {
        public long LocationID { get; set; }
        public string StopNumber { get; set; }
        public string Location { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public bool IsOrigin  { get; set; }
        public List<EDIStatusDetails> EDIStatusDetailsList { get; set; }
    }
    [Serializable]
    public class EDIStatusDetails
    {
        public long EDITrackLoadId { get; set; }
        public long EDIStatusID { get; set; }
        public long LocationID { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public DateTime StatusTime{ get; set; }
        public string StopNumber { get; set; }
        public string Status { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        public List<ValueText> DDlSource { get; set; }
        public string SelectedValue { get; set; }
        public bool IsNew { get; set; }
        public bool IsOld { get; set; }
        public bool IsOrigin { get; set; }
    }
    [Serializable]
    public class ValueText
    {
        public string Value { get; set; }
        public string Text { get; set; }
        public string Code { get; set; }
    }
}

