using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class UserControls_GridBar : System.Web.UI.UserControl
{
    public string HeaderText
    {
        set { lblText.Text = Convert.ToString(value).Trim(); }
    }
    public string HeaderRightText
    {
        set { lblRightText.Text = Convert.ToString(value).Trim(); }
    }
    public bool HeaderRightTextVisible
    {
        set { lblRightText.Visible = value; }
    }
    public long LoadId
    {
        set { ViewState["LoadId"] = value; }
    }
    public string LoadStatus
    {
        set { ViewState["LoadStatus"] = value; }
    }
    public string LinksList
    {
        set { ViewState["LinksList"] = Convert.ToString(value).Trim().Replace(',', ';'); }
    }    
    public string PagesList
    {
        set { ViewState["PagesList"] = Convert.ToString(value).Trim().Replace(',',';'); }
    }

    public string LinkLog
    {
        set { ViewState["LinkLog"] = Convert.ToString(value).Trim(); }
    }


    public bool VisibleBar
    {
        set { if (value != null) { lblLine.Visible = value; } }
    }
    public bool IsReOpenVisible
    {
        set 
        {
            if (value != null)
            {
                this.lnkReopen.Visible = value;
            }
            else
                this.lnkReopen.Visible = true;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        AssignPagesToLinks();
    }
    public void AssignPagesToLinks()
    {
        if (ViewState["LinksList"] != null && ViewState["PagesList"] != null)
        {
            string[] LinksList = Convert.ToString(ViewState["LinksList"]).Trim().Split(';');
            string[] PagesList = Convert.ToString(ViewState["PagesList"]).Trim().Split(';');
            string LinkLog =ViewState["LinkLog"]!=null? Convert.ToString(ViewState["LinkLog"]).Trim():string.Empty ;
            if (LinksList.Length == PagesList.Length)
            {
                plhLinks.Controls.Clear();
                //<a href="companyandnewlocation.html">Add New Company &amp; Location</a> | <a href="newlocation.html">Add New Location </a>  T-Cards
                for (int i = 0; i < LinksList.Length; i++)
                {
                    if (string.Compare(LinksList[i].ToString(), "Load Request", true, System.Globalization.CultureInfo.CurrentCulture) == 0 || string.Compare(LinksList[i].ToString(), "Accessorial Charges", true, System.Globalization.CultureInfo.CurrentCulture) == 0 || string.Compare(LinksList[i].ToString(), "Invoice", true, System.Globalization.CultureInfo.CurrentCulture) == 0 || string.Compare(LinksList[i].ToString(), "T-Cards", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        plhLinks.Controls.Add(new LiteralControl("<a href=" + PagesList[i] + " target='_New'>" + LinksList[i].Replace("&", "&amp;") + "</a>"));
                    else
                        plhLinks.Controls.Add(new LiteralControl("<a href=" + PagesList[i] + ">" + LinksList[i].Replace("&", "&amp;") + "</a>"));
                    if (i != LinksList.Length - 1)
                        plhLinks.Controls.Add(new LiteralControl(" | "));
                }
            }
            plhLogLink.Controls.Clear();
            if (!string.IsNullOrEmpty(LinkLog))
            {
                plhLogLink.Controls.Add(new LiteralControl(" | "));
                plhLogLink.Controls.Add(new LiteralControl("<a href=" + LinkLog + ">View Load Log</a>"));
            }
            LinksList = null; PagesList = null; LinkLog = null;
        }  
    }
    protected void lnkReopen_Click(object sender, EventArgs e)
    {
        if (ViewState["LoadId"] != null)
        {
            if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], (Convert.ToString(ViewState["LoadStatus"]) == "Cancel" ? "SP_ReopenCancelLoad" : "SP_ReopenLoad"), new object[] { Convert.ToInt64(ViewState["LoadId"]), Convert.ToInt64(Session["UserLoginId"]) }) > 0)
            {
                Response.Write("<script>alert('Load re-opened successfully.')</script>");
                Response.Redirect("TrackLoad.aspx?" + ViewState["LoadId"].ToString());
            }
        }
    }
}
