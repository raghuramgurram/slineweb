using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

public partial class Manager_LoadTender : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["TopHeaderText"] = "Load Tender";  
           //this.Page.Title = Convert.ToString(ConfigurationSettings.AppSettings["CompanyName"]);
            this.Page.Title = GetFromXML.CompanyName;
            if (Request.QueryString.Count > 0)
            {
                try
                {
                    if (Convert.ToString(Request.QueryString[0]).IndexOf("^") > 0)
                    {
                        string[] strValues = Convert.ToString(Request.QueryString[0]).Trim().Split('^');
                        ViewState["LoadId"] = Convert.ToString(strValues[0]);
                        ViewState["DriverId"] = Convert.ToString(strValues[1]);
                        ViewState["AssignedId"] = Convert.ToString(strValues[2]);        
                    }
                    else
                        ViewState["LoadId"] = Convert.ToString(Request.QueryString[0]);
                    FillDetails();
                }
                catch(Exception ex)
                {
                }
            }
        }

    }
    private void FillDetails()
    {
        if (ViewState["LoadId"] != null)
        {
            lblInvoiceEmail.Text = GetFromXML.FromInvoiceEmailAddress;
            lblCompany1.Text = lblCompany2.Text = lblCompany3.Text = lblCompany4.Text = lblCompanyHedder.Text= GetFromXML.CompanyName;
            lblCompanyFax.Text = lblCompanyFax1.Text = GetFromXML.CompanyFax.Replace("Fax : ",string.Empty);
            lblCompanyPhone1.Text = GetFromXML.CompanyPhone.Replace("Phone : ", string.Empty);
            DataSet ds=new DataSet();
                ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadTenderDetails", new object[] { Convert.ToInt64(ViewState["LoadId"]) });
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblPickupNum.Text =lblPuckupNum.Text= Convert.ToString(ds.Tables[0].Rows[0]["nvar_Pickup"]);
                    lblDiscription.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_Description"]);
                    lblLoadNumber.Text = (Convert.ToInt64(ViewState["LoadId"])).ToString();
                    lblEquipmentNum.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_Container"]);
                    lblSealNumber.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_Seal"]);
                }
            }
            if (ds.Tables.Count > 1)
            {
                if (ds.Tables[1].Rows.Count > 0)
                {
                    lblOrigin.Text = CommonFunctions.FormatMultipleOriginsNew(Convert.ToString(ds.Tables[1].Rows[0][0]).Trim());
                    lblDestination.Text = CommonFunctions.FormatMultipleOriginsNew(Convert.ToString(ds.Tables[1].Rows[0][1]).Trim());
                }
            }
            if (ds.Tables.Count > 2)
            {
                if (ds.Tables[2].Rows.Count > 0)
                {
                    lblCarrier.Text= lblCarrier1.Text = lblCarrier2.Text = lblCarrier3.Text = Convert.ToString(ds.Tables[2].Rows[0]["nvar_CarrierName"]);
                    lblDotlic.Text= Convert.ToString(ds.Tables[2].Rows[0]["nvar_Dotlic"]);
                    lblName.Text= Convert.ToString(ds.Tables[2].Rows[0]["nvar_Name"]);
                    lblPhone.Text = Convert.ToString(ds.Tables[2].Rows[0]["num_Phone"]);
                    lblCarrierFax.Text= lblFax.Text = Convert.ToString(ds.Tables[2].Rows[0]["num_Fax"]);
                    lblEmail.Text = Convert.ToString(ds.Tables[2].Rows[0]["nvar_Email"]);
                }
            }
            if (ds.Tables.Count > 3)
            {
                if (ds.Tables[3].Rows.Count > 0)
                {
                    lblName1.Text=lblAssignedUser.Text= Convert.ToString(ds.Tables[3].Rows[0]["Name"]);
                    lblAssignDate.Text = Convert.ToString(ds.Tables[3].Rows[0]["date_CreateDate"]);
                    lblDate.Text = (Convert.ToString(ds.Tables[3].Rows[0]["date_CreateDate"])).Split(' ')[0];
                }
            }
            if (ds.Tables.Count > 4)
            {
                List<KeyValuePair<string, decimal>> ChargesList = new List<KeyValuePair<string, decimal>>();
                if (ds.Tables[4].Rows.Count > 0)
                {
                    if (ds.Tables[4].Rows[0]["num_Charges"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_Charges"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Charge", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_Charges"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_AdvancedPaid"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_AdvancedPaid"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Advance Paid", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_AdvancedPaid"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_Dryrun"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_Dryrun"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Dry run", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_Dryrun"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_FuelSurcharge"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_FuelSurcharge"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Fuel Surcharge", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_FuelSurcharge"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_FuelSurchargeAmounts"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_FuelSurchargeAmounts"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Fuel Surcharge", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_FuelSurchargeAmounts"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_PierTermination"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_PierTermination"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Pier Termination", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_PierTermination"])));
                    }
                    if (ds.Tables[4].Rows[0]["bint_ExtraStops"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["bint_ExtraStops"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Extra Stops", Convert.ToDecimal(ds.Tables[4].Rows[0]["bint_ExtraStops"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_LumperCharges"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_LumperCharges"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Lumper Charges", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_LumperCharges"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_DetentionCharges"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_DetentionCharges"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Detention Charges", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_DetentionCharges"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_ChassisSplit"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_ChassisSplit"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Chassis Split", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_ChassisSplit"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_ChassisRent"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_ChassisRent"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Chassis Rent", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_ChassisRent"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_Pallets"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_Pallets"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Pallets", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_Pallets"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_ContainerWashout"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_ContainerWashout"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Container wash out", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_ContainerWashout"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_YardStorage"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_YardStorage"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Yard storage", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_YardStorage"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_RampPullCharges"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_RampPullCharges"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Ramp pull charges", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_RampPullCharges"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_TriaxleCharge"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_TriaxleCharge"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("num_TriaxleCharge", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_TriaxleCharge"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_TransLoadCharges"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_TransLoadCharges"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Trans load charges", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_TransLoadCharges"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_HLScaleCharges"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_HLScaleCharges"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Heavy & Light Scale Charges", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_HLScaleCharges"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_ScaleTicketCharges"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_ScaleTicketCharges"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Scale Ticket Charges", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_ScaleTicketCharges"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_PaidToAnotherDriver"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_PaidToAnotherDriver"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Paid to another driver", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_PaidToAnotherDriver"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_TollCharges"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_TollCharges"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Toll Charges", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_TollCharges"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_SlipCharges"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_SlipCharges"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Flip Charges", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_SlipCharges"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_RepairCharges"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_RepairCharges"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Repair Charges", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_RepairCharges"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_OtherCharges4"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_OtherCharges4"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Hazmat Charges", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_OtherCharges4"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_OtherCharges1"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_OtherCharges1"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Other Charges 1", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_OtherCharges1"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_OtherCharges2"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_OtherCharges2"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Other Charges 2", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_OtherCharges2"])));
                    }
                    if (ds.Tables[4].Rows[0]["num_OtherCharges3"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["num_OtherCharges3"]) != 0)
                    {
                        ChargesList.Add(new KeyValuePair<string, decimal>("Other Charges 3", Convert.ToDecimal(ds.Tables[4].Rows[0]["num_OtherCharges3"])));
                    }
                    if (ds.Tables[4].Rows[0]["Total"] != null && Convert.ToDecimal(ds.Tables[4].Rows[0]["Total"]) != 0)
                    {
                        lblTotal.Text= (Convert.ToDecimal(ds.Tables[4].Rows[0]["Total"])).ToString();
                    }
                    rptPayables.DataSource = ChargesList;
                    rptPayables.DataBind();
                        lblNoCharges.Visible = ChargesList.Count == 0;
                }
            }            
            if (ds != null) { ds.Dispose(); ds = null; }
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (ViewState["LoadId"] != null)
        {
            CheckBackPage();
            Response.Redirect("~/Manager/TrackLoad.aspx?" + Convert.ToInt64(ViewState["LoadId"]));
        }
    }
    private void CheckBackPage()
    {
        if (Session["BackPage4"] != null)
        {
            if (Convert.ToString(Session["BackPage4"]).Trim().Length > 0)
            {
                string strTemp = Convert.ToString(Session["BackPage4"]).Trim();
                Session["BackPage4"] = null;
                Response.Redirect(strTemp);
            }
        }  
    }
}
