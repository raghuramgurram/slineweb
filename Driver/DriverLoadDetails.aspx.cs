using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class DriverLoadDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           // this.Page.Title = Convert.ToString(ConfigurationSettings.AppSettings["CompanyName"]);
            this.Page.Title = GetFromXML.CompanyName;
            if (Request.QueryString.Count > 0 && Session["EmployeeId"]!=null)
            {
                try
                {
                    ViewState["LoadId"] = Convert.ToInt64(Request.QueryString[0]);
                    FillDetails(Convert.ToInt64(ViewState["LoadId"]));
                }
                catch
                {
                }
            }
        }
    }
    private void FillDetails(long ID)
    {
        if (ViewState["LoadId"] != null && Session["EmployeeId"]!=null)
        {
            //lblAddress.Text = "<strong>" + Convert.ToString(ConfigurationSettings.AppSettings["CompanyName"]) + "</strong><br/>" + Convert.ToString(ConfigurationSettings.AppSettings["ComapanyAddress"]) + "<br/>" + Convert.ToString(ConfigurationSettings.AppSettings["ComapanyState"]) + "<br/>" + Convert.ToString(ConfigurationSettings.AppSettings["CompanyPhone"]) + "<br/>" + Convert.ToString(ConfigurationSettings.AppSettings["CompanyFax"]);
            lblAddress.Text = "<strong>" + GetFromXML.CompanyName + "</strong><br/>" + GetFromXML.ComapanyAddress + "<br/>" + GetFromXML.ComapanyState + "<br/>" + GetFromXML.CompanyPhone + "<br/>" + GetFromXML.CompanyFax;

            //lblAddress.Text = Application["CompanyAddress"].ToString();
            DataSet ds = new DataSet();
            lblLoad.Text = Convert.ToString(ViewState["LoadId"]);
            ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriverLoadDetails", new object[] { Convert.ToInt64(ViewState["LoadId"]), Convert.ToInt64(Session["EmployeeId"]),"0" });
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblLoadId.Text = Convert.ToString(ViewState["LoadId"]);
                    lblType.Text = Convert.ToString(ds.Tables[0].Rows[0][0]);
                    lblCommodity.Text = Convert.ToString(ds.Tables[0].Rows[0][1]);
                    lblBooking.Text = Convert.ToString(ds.Tables[0].Rows[0][2]);
                    if (Convert.ToString(ds.Tables[0].Rows[0][3]).Trim().Length == 0)
                        lblContainerId.Text = Convert.ToString(ds.Tables[0].Rows[0][4]);
                    else
                        lblContainerId.Text = Convert.ToString(ds.Tables[0].Rows[0][3]);

                    if (Convert.ToString(ds.Tables[0].Rows[0][5]).Trim().Length == 0)
                        lblChasis.Text = Convert.ToString(ds.Tables[0].Rows[0][6]);
                    else
                        lblChasis.Text = Convert.ToString(ds.Tables[0].Rows[0][5]);

                    lblRemarks.Text = Convert.ToString(ds.Tables[0].Rows[0][7]);
                    lblDescription.Text = Convert.ToString(ds.Tables[0].Rows[0][8]);
                    //BillingTo = "<b>" + Convert.ToString(ds.Tables[0].Rows[0][9]) + " </b> <br/>&nbsp;&nbsp;" + Convert.ToString(ds.Tables[0].Rows[0][10]) + "  " + Convert.ToString(ds.Tables[0].Rows[0][11]) + "<br/>&nbsp;&nbsp;";
                    //BillingTo += Convert.ToString(ds.Tables[0].Rows[0][12]) + ", " + Convert.ToString(ds.Tables[0].Rows[0][13]) + " " + Convert.ToString(ds.Tables[0].Rows[0][14]);
                    //BillingTo += "<br/>" + Convert.ToString(ds.Tables[0].Rows[0][16]);
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    lblOrigin.Text = CommonFunctions.FormatMultipleOrigins(Convert.ToString(ds.Tables[1].Rows[0][0]).Trim());
                    lblDestination.Text = CommonFunctions.FormatMultipleOrigins(Convert.ToString(ds.Tables[1].Rows[0][1]).Trim());
                }
                if (ds.Tables.Count > 2)
                {
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        lblAssigned.Text = Convert.ToString(ds.Tables[2].Rows[0][0]).Trim();
                        lblAssigned.Font.Underline = true;
                        lblDriverOrCarrier.Text = Convert.ToString(ds.Tables[2].Rows[0][1]).Trim();
                        lblTotal.Text = "Total Charges:  $ <b>" + Convert.ToString(ds.Tables[2].Rows[0][2]).Trim() + "</b>";
                        lblCarrier.Text = Convert.ToString(ds.Tables[2].Rows[0][3]);
                    }
                }
            }
            if (ds != null) { ds.Dispose(); ds = null; }
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (ViewState["LoadId"] != null)
        {
            Response.Redirect("~/Driver/DriverTrackLoad.aspx?" + Convert.ToInt64(ViewState["LoadId"]));
        }
    }
}
