using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class TimePicker : System.Web.UI.UserControl
{  

    public string Time
    {
        get
        {
            if (enableDropDowns)
            {
                if (ddlHour.SelectedItem.Text.Trim().Length == 1)
                    ddlHour.SelectedItem.Text = "0" + ddlHour.SelectedItem.Text.Trim();
                if (ddlMin.SelectedItem.Text.Trim().Length == 1)
                    ddlMin.SelectedItem.Text = "0" + ddlMin.SelectedItem.Text.Trim();
                return ddlHour.SelectedItem.Text.Trim() + ":" + ddlMin.SelectedItem.Text.Trim() + " " + dropAM.SelectedItem.Text;
            }
            else
            {
                if (txtHour.Text.Trim().Length > 0 && txtMin.Text.Trim().Length > 0)
                {
                    if (txtHour.Text.Trim().Length == 1)
                        txtHour.Text = "0" + txtHour.Text.Trim();
                    if (txtMin.Text.Trim().Length == 1)
                        txtMin.Text = "0" + txtMin.Text.Trim();
                    return txtHour.Text.Trim() + ":" + txtMin.Text.Trim() + " " + dropAM.SelectedItem.Text;
                }
                else
                    return "";
            }
        }
        set 
        {
            if (value.Trim().Length > 0)
            {
                if (value.Trim().Contains(":") && value.Trim().Contains(" "))
                {
                    string[] strTime = value.Trim().Split(':');
                    if (enableDropDowns)
                        ddlHour.SelectedIndex = int.Parse(strTime[0].Trim())-1;
                    else
                        txtHour.Text = strTime[0].Trim();
                    string[] strTemp = strTime[1].Trim().Split(' ');
                    if (enableDropDowns)
                        ddlMin.SelectedIndex = int.Parse(strTemp[0].Trim());     
                    else
                        txtMin.Text = strTemp[0].Trim();                    
                    dropAM.SelectedIndex = dropAM.Items.IndexOf(dropAM.Items.FindByText(strTemp[1].Trim().ToUpper()));
                    strTime = null;
                    strTemp = null;
                }
            }
        }
    }
    public bool SetCurrentTime
    {
        set 
        {
            if (value != null && value == true)
            {
                string CurrentTime = DateTime.Now.ToShortTimeString();
                if (CurrentTime.Trim().Contains(":") && CurrentTime.Trim().Contains(" "))
                {
                    string[] strTime = CurrentTime.Trim().Split(':');
                    if (enableDropDowns)
                        ddlHour.SelectedIndex = int.Parse(strTime[0].Trim()) - 1;
                    else
                        txtHour.Text = strTime[0].Trim();
                    string[] strTemp = strTime[1].Trim().Split(' ');
                    if (enableDropDowns)
                        ddlMin.SelectedIndex = int.Parse(strTemp[0].Trim());
                    else
                        txtMin.Text = strTemp[0].Trim();
                    dropAM.SelectedIndex = dropAM.Items.IndexOf(dropAM.Items.FindByText(strTemp[1].Trim().ToUpper()));
                    strTime = null;
                    strTemp = null;
                }
            }
        }
    }
    public void Clear()
    {
        txtHour.Text = "";
        txtMin.Text = "";
        ddlHour.SelectedIndex = 0;
        ddlMin.SelectedIndex = 0;
    }
    public bool ReqFieldValidation
    {
        set 
        {
            if (value != null)
            {
                ReqValHours.Enabled = value;
                ReQValMin.Enabled = value;
            }
            else
            {
                ReqValHours.Enabled = false;
                ReQValMin.Enabled = false;
            }
        }
    }
   
    public void SetTime(int iHour,int iMin,string strAM)
    {
        if (enableDropDowns)
        {
            ddlHour.SelectedIndex = iHour - 1;
            ddlMin.SelectedIndex = iMin;
        }
        else
        {
            if (iHour <= 12)
                txtHour.Text = (iHour < 10) ? ("0" + iHour.ToString()) : iHour.ToString();
            if (iMin <= 59)
                txtMin.Text = (iMin < 10) ? ("0" + iMin.ToString()) : iMin.ToString();
        }
        dropAM.SelectedIndex = (string.Compare(strAM, "PM", true, System.Globalization.CultureInfo.CurrentCulture) == 0) ? 1 : 0;
    }

    public bool enableDropDowns;
    public bool EnableDropDowns
    {
        get
        {
           return enableDropDowns;
        }
        set
        {
           enableDropDowns=value;
            if(enableDropDowns)
            {
                ddlHour.Visible =true;
                ddlMin.Visible =true;
                txtHour.Visible =false;
                txtMin.Visible =false;
            }
            else
            {
                ddlHour.Visible =false;
                ddlMin.Visible =false;
                txtHour.Visible =true;
                txtMin.Visible =true;
            }
        }
    }
}
