using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections.Specialized;

public partial class SMSTracker : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        FileStream fs = null;
        if (!File.Exists(Server.MapPath("~/Urls.txt")))
            using (fs = File.Create(Server.MapPath("~/Urls.txt"))) { }
        TextWriter tw = new StreamWriter(Server.MapPath("~/Urls.txt"), true);
        // write a line of text to the file
        tw.WriteLine(HttpContext.Current.Request.Url.ToString());
        // close the stream
        
        string queryString = HttpContext.Current.Request.Url.Query;
        if (!string.IsNullOrEmpty(queryString))
        {
            NameValueCollection queryStringCollection = HttpUtility.ParseQueryString(queryString);
            if (!string.IsNullOrEmpty(Convert.ToString(queryStringCollection.Get("client-ref"))) && (queryStringCollection.Get("client-ref") == "slinetransport" || queryStringCollection.Get("client-ref") == "suttertransportation" || queryStringCollection.Get("client-ref") == "empiretpt"))
            {           
            if (ConfigurationSettings.AppSettings["SMSClientRef"] == queryStringCollection.Get("client-ref"))
            {
                SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_update_eTn_SMSLogDetails", new object[] { queryStringCollection.Get("messageId"), queryStringCollection.Get("to"), queryStringCollection.Get("network-code"), queryStringCollection.Get("msisdn"), queryStringCollection.Get("status"), queryStringCollection.Get("err-code"), queryStringCollection.Get("scts"), queryStringCollection.Get("message-timestamp"), queryString });
            }
            else 
            {              
                    tw.WriteLine(HttpContext.Current.Request.Url.ToString().Replace(ConfigurationSettings.AppSettings["SMSClientRef"], queryStringCollection.Get("client-ref")));
                    tw.Close();
                    Response.Redirect(HttpContext.Current.Request.Url.ToString().Replace(ConfigurationSettings.AppSettings["SMSClientRef"], queryStringCollection.Get("client-ref")));
                }             
                
            }
        }        
        tw.Close();
       
    }
}
