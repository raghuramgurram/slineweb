using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Mobile_UploadDocuments : System.Web.UI.Page
{
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Keys.LOGIN_ID_KEY] == null || Session[Keys.EMPLOY_ID] == null)
        {
            Response.Redirect(Keys.PAGE_LOAGIN);
        }
        if (Session[Keys.LOAD_ID] != null)
        {
            try
            {
                if (!IsPostBack)
                {
                    LoadDetailsBase load = new LoadDetailsBase();
                    DataTable loaddetails = load.loadloaddetails(Convert.ToInt32(Session[Keys.LOAD_ID]));
                    if (loaddetails != null)
                    {
                        MuPageMenu.SubTitle = ConfigurationManager.AppSettings["CompanyName"].ToString() + " - Upload Documents For Load -" + Session[Keys.LOAD_ID].ToString() + ", Container #: " + loaddetails.Rows[0]["nvar_Container"].ToString();
                        MuPageMenu.LoadMenu();
                    }

                    if (Request.QueryString["id"] != null)
                    {
                        Mobile_eTransportMobile MasterPage = (Mobile_eTransportMobile)Page.Master;
                        string error = "<p>" + ErrorMessages.FILE_UPLOAD_OK + " </p>";
                        MasterPage.ShowMessage(error);
                    }
                }
                setbuttons();
            }
            catch (Exception ex)
            {

            }


        }
        else
        {
            Response.Redirect(Keys.PAGE_LOAD_HOME);
        }

    }

    public void setbuttons()
    {
        UploadDocuments busDoc = new UploadDocuments();
        int PODcount = busDoc.GetDocumentCount(Convert.ToInt32(Session[Keys.LOAD_ID]), (int)DocumentType.Proof_of_Delivery);
        int ingatecount = busDoc.GetDocumentCount(Convert.ToInt32(Session[Keys.LOAD_ID]), (int)DocumentType.In_Gate_Interchange);
        int ontgatecount = busDoc.GetDocumentCount(Convert.ToInt32(Session[Keys.LOAD_ID]), (int)DocumentType.Out_Gate_Interchange);
        int billofladingcount = busDoc.GetDocumentCount(Convert.ToInt32(Session[Keys.LOAD_ID]), (int)DocumentType.Bill_of_Lading);
        int scaleticket = busDoc.GetDocumentCount(Convert.ToInt32(Session[Keys.LOAD_ID]), (int)DocumentType.Scale_Ticket);

        if (PODcount > 0)
        {
            lnkPOD.Attributes.Remove("data-theme");
            lnkPOD.Attributes.Add("data-theme", "g");
        }
        if (ingatecount > 0)
        {
            lnkInGate.Attributes.Remove("data-theme");
            lnkInGate.Attributes.Add("data-theme", "g");
        }
        if (ontgatecount > 0)
        {
            lnkOutGate.Attributes.Remove("data-theme");
            lnkOutGate.Attributes.Add("data-theme", "g");
        }

        if (billofladingcount > 0)
        {
            lnkBillOfLanding.Attributes.Remove("data-theme");
            lnkBillOfLanding.Attributes.Add("data-theme", "g");
        }

        if (scaleticket > 0)
        {
            lnkScaleTicket.Attributes.Remove("data-theme");
            lnkScaleTicket.Attributes.Add("data-theme", "g");
        }

    }
    #endregion

    #region button clicks
    public void lnkgoback_click(object sender, EventArgs e)
    {
        try
        {
            PageType page = (PageType)Session[Keys.TAB_SELECTED];
            if (page == PageType.Loads)
            {
                Response.Redirect(Keys.PAGE_LOAD_HOME);
            }
            else
            {
                Response.Redirect(Keys.PAGE_PAPER_WORK_PENDING);
            }
        }
        catch (Exception ex)
        {
            // Response.Redirect(Keys.PAGE_LOAD_HOME);
        }
    }
    public void lnkInGate_click(object sender, EventArgs e)
    {
        int type = (int)DocumentType.In_Gate_Interchange;
        Response.Redirect(Keys.PAGE_DOCUMENTS_UPLOAD + "?Type=" + type.ToString());
    }
    public void lnkOutGate_click(object sender, EventArgs e)
    {
        int type = (int)DocumentType.Out_Gate_Interchange;
        Response.Redirect(Keys.PAGE_DOCUMENTS_UPLOAD + "?Type=" + type.ToString());
        Response.Redirect(Keys.PAGE_DOCUMENTS_UPLOAD);
    }
    public void lnkBillOfLanding_click(object sender, EventArgs e)
    {
        int type = (int)DocumentType.Bill_of_Lading;
        Response.Redirect(Keys.PAGE_DOCUMENTS_UPLOAD + "?Type=" + type.ToString());
    }
    public void lnkPOD_click(object sender, EventArgs e)
    {
        int type = (int)DocumentType.Proof_of_Delivery;
        Response.Redirect(Keys.PAGE_DOCUMENTS_UPLOAD + "?Type=" + type.ToString());
    }
    public void lnkScaleTicket_click(object sender, EventArgs e)
    {
        int type = (int)DocumentType.Scale_Ticket;
        Response.Redirect(Keys.PAGE_DOCUMENTS_UPLOAD + "?Type=" + type.ToString());
    }
    #endregion
}
