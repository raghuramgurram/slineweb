using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Manager_ChassisCharges : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Chassis Charges";
        barCurrent.HeaderText = "Chassis Charges";
        if (!IsPostBack)
        {
            gridCurrent.Visible = true;
            gridCurrent.DeleteVisible = false;
            gridCurrent.EditVisible = true;
            gridCurrent.TableName = "eTn_ChassisCharges";
            gridCurrent.Primarykey = "bint_ChassisChargesId";
            gridCurrent.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
            gridCurrent.ColumnsList = "bint_ChassisChargesId;" + CommonFunctions.DateQueryString("date_EffectiveDate", "EffectiveDate") + ";num_ChassisCharges";
            gridCurrent.VisibleColumnsList = "EffectiveDate;num_ChassisCharges";
            gridCurrent.VisibleHeadersList = "Effective Date;Chassis Charge";
            gridCurrent.DeleteTablesList = "eTn_ChassisCharges";
            gridCurrent.DeleteTablePKList = "bint_ChassisChargesId";
            gridCurrent.EditPage = "~/Manager/NewChassisCharge.aspx";
            gridCurrent.OrderByClause = "date_EffectiveDate desc";
            gridCurrent.BindGrid(0);
            barCurrent.PagesList = "NewChassisCharge.aspx";
        }
    }
}
