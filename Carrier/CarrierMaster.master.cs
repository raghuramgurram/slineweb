using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CarrierMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //this.Page.Title = Convert.ToString(ConfigurationSettings.AppSettings["CompanyName"]);
        this.Page.Title = GetFromXML.CompanyName;
        Response.ContentType = "text/html";
        Response.AddHeader("pragma", "no-cache");
        Response.AddHeader("cache-control", "private, no-cache, must-revalidate");
    }
}
