<%@ Page AutoEventWireup="true" CodeFile="EmptyReturnFreeDays.aspx.cs" Inherits="EmptyReturnFreeDays"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/LoadGridManager.ascx" TagName="LoadGridManager"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">
    <table id="ContentTbl1" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td align="left" style="width: 100%">
                <asp:Label runat="server" ID="lblAccLoads" Visible="true">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblbox1">
                    <tr style="background-color: #FFFFFF;">
                        <td align="left" valign="middle">
                            <asp:Label ID="lblText" runat="server" Text="Customer Name : "></asp:Label>
                            <asp:DropDownList ID="ddlCustomers" runat="server">
                            </asp:DropDownList>&nbsp;
                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnStyle" OnClick="btnSearch_Click"/>
                        </td>
                    </tr>
                </table>
                </asp:Label>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <uc2:GridBar ID="barManager" runat="server" HeaderText="Loads" />
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="5" src="../images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <uc1:LoadGridManager ID="grdManager" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
