using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class InvoiceableLoads : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int Type = Convert.ToInt32(Request.QueryString["Type"]);
        if (Type == 1)
          Session["TopHeaderText"] = "Invoiceable Loads";
        else if (Type == 2)
          Session["TopHeaderText"] = "Accessorial loads - Customer wise";
        if (!IsPostBack)
        {
            FillLists();
        }
    }

    private void FillLists()
    {
        DataSet ds = new DataSet();
        if (Convert.ToInt32(Request.QueryString["Type"]) == 1)
          ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCustomerNames");
        else if (Convert.ToInt32(Request.QueryString["Type"]) == 2)
          ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetActiveCustomerNames");
        if (ds.Tables.Count > 0)
        {
            ddlCustomers.DataSource = ds.Tables[0];
            ddlCustomers.DataValueField = "bint_CustomerId";
            ddlCustomers.DataTextField = "nvar_CustomerName";
            ddlCustomers.DataBind();
        }
        ddlCustomers.Items.Insert(0, new ListItem("All", "0"));
        ds.Tables.Clear();       
        if (ds != null) { ds.Dispose(); ds = null; }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Session["SerchCriteria"] = null;
            Session["ReportParameters"] = null;
            Session["ReportName"] = null;
            Session["ReportParameters"] = ddlCustomers.SelectedValue;
            if (Convert.ToInt32(Request.QueryString["Type"]) == 1)
            {
                Session["SerchCriteria"] = ((Convert.ToInt64(ddlCustomers.SelectedValue) > 0) ? "Customer Name : " + ddlCustomers.SelectedItem.Text.Trim() : "All Customers ");
                Session["ReportName"] = "GetInvoiceableLoads";
                Response.Redirect("~/Manager/DisplayInvoiceableLoadsReport.aspx");
            }
            else
            {
                Session["SerchCriteria"] = "Customer Name : " + ddlCustomers.SelectedItem.Text.Trim();
                Session["ReportName"] = "GetAccessorialLoads";
                Response.Redirect("~/Manager/DisplayReport.aspx");
            }
        }
    }
}
