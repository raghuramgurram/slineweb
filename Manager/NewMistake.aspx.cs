using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Manager_NewMistake : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "New / Edit Mistake";
        if (!Page.IsPostBack)
        {
            if (Request.QueryString.Count > 0)
            {
                ViewState["ID"] = Request.QueryString[0].Trim();
                try
                {
                    ViewState["Mode"] = "Edit";
                    ViewState["ID"] = Convert.ToInt64(Request.QueryString[0]);
                    FillDetails();
                }
                catch
                {
                }
                Session["TopHeaderText"] = "Edit Mistake";
            }
            else
            {
                if (ViewState["ID"] == null)
                {
                    ViewState["Mode"] = "New";
                    Session["TopHeaderText"] = "New Mistake";
                }
                else
                {
                    try
                    {
                        ViewState["Mode"] = "Edit";
                        FillDetails();
                        Session["TopHeaderText"] = "Edit Mistake";
                    }
                    catch
                    {
                    }
                }
            }
        }
    }

    private void FillDetails()
    {
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetMistakeDetails", new object[] { Convert.ToInt64(ViewState["ID"]) });
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtMistakeDone.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_description"]);
                txtMistakeDoneDate.Date = Convert.ToString(ds.Tables[0].Rows[0]["date_MistakeDoneDate"]);
            }
        }
        if (ds != null) { ds.Dispose(); ds = null; }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (Convert.ToString(Session["UserLgnId"]) != "")
        {
            Response.Redirect("~/Manager/StaffMistakes.aspx?" + Convert.ToString(Session["UserLgnId"]));
        }
        else
        {
            Response.Redirect("~/Manager/StaffMistakes.aspx");
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (ViewState["Mode"] != null)
            {
                //SLine 2017 Enhancement for Office Location Starts
                long officeLocationId = 0;
                if (Session["OfficeLocationID"] != null)
                {
                    officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
                }
                //SLine 2017 Enhancement for Office Location Ends
                int empID;
                if (Convert.ToString(Session["UserLgnId"]) == "")
                    empID = 0;
                else
                    empID = Convert.ToInt32(Session["UserLgnId"]);
                //SLine 2017 Enhancement for Office Location added officeLocationId to objParams
                object[] objParams = new object[] { txtMistakeDone.Text.Trim(),CommonFunctions.CheckDateTimeNull(txtMistakeDoneDate.Date),Convert.ToInt64(Session["UserLoginId"]),
                                                       Convert.ToInt64(ViewState["ID"]),Convert.ToInt64(empID),officeLocationId};
                System.Data.SqlClient.SqlParameter[] objsqlparams = null;
                if (Convert.ToString(ViewState["Mode"]).StartsWith("Edit", true, System.Globalization.CultureInfo.CurrentCulture))
                {
                    objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Update_Mistake", objParams);
                    if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Update_Mistake", objsqlparams) > 0)
                    {
                        if (Convert.ToString(Session["UserLgnId"]) != "")
                        {
                            Response.Redirect("~/Manager/StaffMistakes.aspx?" + Convert.ToString(Session["UserLgnId"]));
                        }
                        else
                        {
                            Response.Redirect("~/Manager/StaffMistakes.aspx");
                        }
                    }
                }
                else if (Convert.ToString(ViewState["Mode"]).StartsWith("New", true, System.Globalization.CultureInfo.CurrentCulture))
                {
                    objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Add_Mistake", objParams);
                    if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Add_Mistake", objsqlparams) > 0)
                    {
                        if (Convert.ToString(Session["UserLgnId"]) != "")
                        {
                            Response.Redirect("~/Manager/StaffMistakes.aspx?" + Convert.ToString(Session["UserLgnId"]));
                        }
                        else
                        {
                            Response.Redirect("~/Manager/StaffMistakes.aspx");
                        }
                    }
                }
                objParams = null;
                objsqlparams = null;
            }
        }
    }
}
