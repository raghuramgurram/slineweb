using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class CustomerTrackLoad : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Track Load";
        if (!IsPostBack)
        {
            lnkAccessorialCharges.Visible = false;
            if (Request.QueryString.Count > 0)
            {
                try
                {
                    ViewState["ID"] = Convert.ToInt64(Request.QueryString[0]);
                    FillDetails(Convert.ToInt64(ViewState["ID"]));
                }
                catch 
                { 
                }
            }
        }
    }
    private void FillDetails(long ID)
    {
        string strAssigned = null, strEnterBy = null;
        long ShippingId = 0; bool IsApptDateVisible = true;
        string strQuery = "";
        DataTable dt;

        int pageIndex=Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        lblLoadId.Text = Convert.ToString(ID);
        if (Convert.ToInt32(DBClass.executeScalar("select count([bint_LoadId]) from [eTn_Receivables] where [bint_LoadId]="+ID))>0)
        {
            lnkAccessorialCharges.Visible = true;            
            lnkAccessorialCharges.PostBackUrl = "CustomerAccessorialCharges.aspx?" + ID;
        }

            //DBClass.executeScalar("select count([bint_LoadId]) from [eTn_Load] where [bint_LoadStatusId]=" +
            //                            "(select [bint_LoadStatusId] from [eTn_LoadStatus] where Lower([nvar_LoadStatusDesc])='new') and [bint_LoadId]=" + ID)) > 0)
        
        string Status=DBClass.executeScalar("select eTn_LoadStatus.[nvar_LoadStatusDesc] from [eTn_Load] inner join [eTn_LoadStatus] on [eTn_Load].[bint_LoadStatusId]=[eTn_LoadStatus].[bint_LoadStatusId] where [eTn_Load].[bint_LoadId]="+ID);
        string isHazmatLoad = DBClass.executeScalar("select [eTn_Load].[bit_IsHazmatLoad] from [eTn_Load] where [eTn_Load].[bint_LoadId]=" + ID);
        if (!string.IsNullOrEmpty(isHazmatLoad))
        {
            if (Convert.ToBoolean(isHazmatLoad))
            {
                spanishazmat.Visible = true;
            }
            else
            {
                spanishazmat.Visible = false;
            }
        }
        if (string.Compare(Status, "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0 || string.Compare(Status, "Loaded in Yard", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            IsApptDateVisible = true;
        }
        else
            IsApptDateVisible = false;

        Grid1.Visible = true;
        Grid1.DeleteVisible = false;
        Grid1.EditVisible = false;
        Grid1.TableName = "eTn_Origin";
        Grid1.Primarykey = "eTn_Origin.bint_OriginId";
        Grid1.PageSize = pageIndex;
        Grid1.ColumnsList = "eTn_Origin.bint_OriginId 'Id';" + CommonFunctions.AddressQueryStringWithLinks("eTn_Company.nvar_CompanyName;eTn_Location.nvar_Street;eTn_Location.nvar_Suite;eTn_Location.nvar_City;eTn_Location.nvar_State;eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "eTn_Origin.date_PickupDateTime", "Origin", "AddPickUpdate.aspx", "convert(varchar(20),eTn_Origin.[bint_LoadId])+'^'+convert(varchar(20),eTn_Origin.bint_OriginId)", "Add/Edit PickUp Date", IsApptDateVisible) + ";eTn_Origin.nvar_PO as 'PO';eTn_Origin.bint_Pieces as 'Pieces';eTn_Origin.num_Weight as 'Weight';case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' To '+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end  as 'Date';eTn_Origin.nvar_App as 'Appt';eTn_Origin.nvar_ApptGivenby as 'ApptGivenBy'";
        Grid1.VisibleColumnsList = "Origin;Date;Appt;ApptGivenBy";
        Grid1.VisibleHeadersList = "Origin(s);Pickup Date /Time;Appt.#;Appt. Given by";
        Grid1.InnerJoinClause = " inner join eTn_Location on eTn_Origin.bint_LocationId = eTn_Location.bint_LocationId " +
               " inner join eTn_Company on eTn_Location.bint_CompanyId = eTn_Company.bint_CompanyId ";
        Grid1.WhereClause = "eTn_Origin.[bint_LoadId]=" + ID;
        Grid1.IsPagerVisible = false;
        Grid1.BindGrid(0);

        //Destination Grid
        Grid2.Visible = true;
        Grid2.DeleteVisible = false;
        Grid2.EditVisible = false;
        Grid2.TableName = "eTn_Destination";
        Grid2.Primarykey = "eTn_Destination.bint_DestinationId";
        Grid2.PageSize = pageIndex;
        Grid2.ColumnsList = "eTn_Destination.bint_DestinationId as 'Id';" + CommonFunctions.AddressQueryStringWithLinks("eTn_Company.nvar_CompanyName;eTn_Location.nvar_Street;eTn_Location.nvar_Suite;eTn_Location.nvar_City;eTn_Location.nvar_State;eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "eTn_Destination.date_DeliveryAppointmentDate", "Destination", "AddAppointmentDate.aspx", "convert(varchar(20),eTn_Destination.[bint_LoadId])+'^'+convert(varchar(20),eTn_Destination.bint_DestinationId)", "Add/Edit Appointment Date ", IsApptDateVisible) + ";eTn_Destination.nvar_PO as 'PO';eTn_Destination.bint_Pieces as 'Pieces';eTn_Destination.num_Weight as 'Weight';case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' To '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Date';eTn_Destination.nvar_App as 'Appt';eTn_Destination.nvar_ApptGivenby as 'ApptGivenBy'";
        Grid2.VisibleColumnsList = "Destination;Date;Appt;ApptGivenBy";
        Grid2.VisibleHeadersList = "Destination(s);App. Date /Time;Appt.#;Appt. Given by";
        Grid2.InnerJoinClause = " inner join eTn_Location on eTn_Destination.bint_LocationId = eTn_Location.bint_LocationId " +
               " inner join eTn_Company on eTn_Location.bint_CompanyId = eTn_Company.bint_CompanyId ";
        Grid2.WhereClause = "eTn_Destination.[bint_LoadId]=" + ID;
        Grid2.IsPagerVisible = false;
        Grid2.BindGrid(0);

        //grid LoadStatus Information
        Grid4.Visible = true;
        Grid4.DeleteVisible = false;
        Grid4.EditVisible = false;
        Grid4.TableName = "eTn_TrackLoad";
        Grid4.Primarykey = "eTn_TrackLoad.bint_TrackLoadId";
        //Grid4.PageSize = pageIndex;
        Grid4.ColumnsList = "eTn_LoadStatus.nvar_LoadStatusDesc as 'Status';eTn_TrackLoad.nvar_Location as 'Loc';eTn_TrackLoad.date_CreateDate as 'Date';eTn_TrackLoad.nvar_Notes as 'Notes'";
        Grid4.VisibleColumnsList = "Status;Loc;Date;Notes";
        Grid4.VisibleHeadersList = "Status;Location;Date / Time;Comments";
        Grid4.InnerJoinClause = " inner join eTn_LoadStatus on eTn_LoadStatus.bint_LoadStatusId = eTn_TrackLoad.bint_LoadStatusId ";
        Grid4.WhereClause = "eTn_TrackLoad.[bint_LoadId]=" + ID;
        Grid4.IsPagerVisible = false;
        Grid4.BindGrid(0);

        //Grid Notes
        if (Session["UserLoginId"] != null)
        {
            Grid5.Visible = true;
            Grid5.DeleteVisible = false;
            Grid5.EditVisible = false;
            Grid5.PageSize = pageIndex;
            Grid5.TableName = "eTn_Notes";
            Grid5.Primarykey = "eTn_Notes.bint_NoteId";
            Grid5.PageSize = 10;
            Grid5.ColumnsList = "eTn_UserLogin.nvar_FirstName+' '+eTn_UserLogin.nvar_LastName as 'EnterBy';eTn_Notes.date_CreateDate as 'Date';eTn_Notes.nvar_NotesDesc as 'Notes'";
            Grid5.VisibleColumnsList = "EnterBy;Date;Notes";
            Grid5.VisibleHeadersList = "Entered By ;Date / Time;Comments";
            Grid5.InnerJoinClause = " inner join [eTn_Load] on [eTn_Load].bint_LoadId =[eTn_Notes].[bint_LoadId] inner join eTn_UserLogin on [eTn_UserLogin].bint_UserLoginId = [eTn_Notes].bint_UserLoginId";
            Grid5.WhereClause = "eTn_Notes.[bint_LoadId]=" + ID;
            Grid5.IsPagerVisible = true;
            Grid5.BindGrid(0);

            //Rail/Equipment  Notes Grid
            GridRailNotes.Visible = true;
            GridRailNotes.DeleteVisible = false;
            GridRailNotes.EditVisible = false;
            GridRailNotes.TableName = "eTn_RailNotes";
            GridRailNotes.Primarykey = "eTn_RailNotes.bint_RailNoteId";
            GridRailNotes.PageSize = pageIndex;
            GridRailNotes.ColumnsList = "eTn_UserLogin.nvar_FirstName+' '+eTn_UserLogin.nvar_LastName as 'EnterBy';eTn_RailNotes.date_CreateDate as 'Date';eTn_RailNotes.nvar_Status as 'Status'";
            GridRailNotes.VisibleColumnsList = "EnterBy;Date;Status";
            GridRailNotes.VisibleHeadersList = "Entered By ;Date / Time;Status";
            GridRailNotes.InnerJoinClause = " inner join [eTn_Load] on [eTn_Load].bint_LoadId =[eTn_RailNotes].[bint_LoadId] inner join eTn_UserLogin on [eTn_UserLogin].bint_UserLoginId = [eTn_RailNotes].bint_UserLoginId";
            GridRailNotes.WhereClause = "eTn_RailNotes.[bint_LoadId]=" + ID;
            GridRailNotes.IsPagerVisible = true;
            GridRailNotes.BindGrid(0);
        }

        if (Session["CustomerId"] != null)
        {
            strQuery = "SELECT C.[nvar_CustomerName],L.[nvar_Pickup],L.[nvar_Container],L.[nvar_Container1],L.[nvar_Booking],CT.[nvar_CommodityTypeDesc],L.[nvar_Seal],L.[date_DateOfCreation], L.[nvar_Ref],L.[nvar_AssignTo],S.[nvar_LoadStatusDesc],L.[nvar_RailBill],convert(varchar(20),L.[date_OrderBillDate],101),L.[nvar_PersonalBill],L.[bint_ShippingId],L.[nvar_Booking],L.[date_LastFreeDate], L.[bit_HotShipment] FROM [eTn_Load] L " +
                            "inner join [eTn_Customer] C on L.[bint_CustomerId]=C.[bint_CustomerId] inner join [eTn_CommodityType] CT on L.[bint_CommodityTypeId]=CT.[bint_CommodityTypeId]  inner join [eTn_LoadStatus] S on L.[bint_LoadStatusId]=S.[bint_LoadStatusId] " +
                            " WHERE L.[bint_LoadId]=" + ID + " and L.[bint_CustomerId]=" + Convert.ToInt64(Session["CustomerId"]);
            dt = DBClass.returnDataTable(strQuery);
            if (Session["ViewLog"] != null)
            {
                Session["ViewLog"] = null;
                SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_LoadView_Log", new object[] { ID, Convert.ToString(Session["UserLoginId"]) });
            }
            if (dt.Rows.Count > 0)
            {
                lblCustomer.Text = Convert.ToString(dt.Rows[0][0]);
                lblPickUp.Text = Convert.ToString(dt.Rows[0][1]);
                if (Convert.ToString(dt.Rows[0][3]).Trim().Length > 0)
                    lblContainer.Text = Convert.ToString(dt.Rows[0][2]) + "<br/>" + Convert.ToString(dt.Rows[0][3]);
                else
                    lblContainer.Text = Convert.ToString(dt.Rows[0][2]);
                lblCommodity.Text = Convert.ToString(dt.Rows[0][5]);
                lblSeal.Text = Convert.ToString(dt.Rows[0][6]);
                lblCreated.Text = Convert.ToString(dt.Rows[0][7]);
                //lblPersong.Text = Convert.ToString(dt.Rows[0][8]);
                lblRef.Text = Convert.ToString(dt.Rows[0][8]);
                strAssigned = Convert.ToString(dt.Rows[0][9]);
                lblStatus.Text = Convert.ToString(dt.Rows[0][10]);
                lblRailBill.Text = Convert.ToString(dt.Rows[0][11]);
                lblBillDate.Text = Convert.ToString(dt.Rows[0][12]);
                //lblPersonBill.Text = Convert.ToString(dt.Rows[0][14]);
                strEnterBy = Convert.ToString(dt.Rows[0][13]);
                ShippingId = Convert.ToInt64(dt.Rows[0][14]);
                lblBooking.Text = Convert.ToString(dt.Rows[0][15]);
                lblLastfreedate.Text = Convert.ToString(dt.Rows[0][16]);
                chkHotShipment.Checked = Convert.ToBoolean(dt.Rows[0][17]);
            }
            lblPersong.Text = DBClass.executeScalar("select [eTn_UserLogin].[nvar_UserId] from [eTn_UserLogin] inner join [eTn_Load] on [eTn_Load].bint_UserLoginId=[eTn_UserLogin].bint_UserLoginId where [eTn_Load].[bint_LoadId]=" + ID);
        }
        /*Commented on feb 06 2008*/
        //strQuery = "Select isnull(nvar_OtherChargesReason,&nbsp;) from eTn_Receivables where bint_LoadId = " + ID;
        //lblComments.Text = DBClass.executeScalar(strQuery);
        //if (lblComments.Text.Trim().Length == 0)
        //    lblComments.Text = "&nbsp;";

        strQuery = "select '<b>'+[nvar_ShippingLineName]+'</b> <br/>'+[nvar_Street]+'&nbsp;'+ [nvar_Suite]+'<br/>'+[nvar_City]+','+[nvar_State] +'&nbsp;'+ [nvar_Zip]+'<br/><a href='+ [nvar_WebsiteURL]+' target=_blank>'+[nvar_WebsiteURL]+'</a>',[nvar_PierTermination] from [eTn_Shipping] where [bint_ShippingId]=" + ShippingId;
        dt = DBClass.returnDataTable(strQuery);
        if (dt.Rows.Count > 0)
        {
            lblRoute.Text = Convert.ToString(dt.Rows[0][0]);
            lblPier.Text = Convert.ToString(dt.Rows[0][1]);
            strQuery = "select [nvar_Name]+'<br/>'+case when len(cast([num_WorkPhone]  as varchar(10)))=10 then substring(cast([num_WorkPhone]  as varchar(10)),1,3)+'-'+" +
                      "substring(cast([num_WorkPhone]  as varchar(10)),4,3)+'-'+substring(cast([num_WorkPhone]  as varchar(10)),7,4) else '' end +'<br/>'+case when len(cast([num_Mobile]  as varchar(10)))=10 then substring(cast([num_Mobile]  as varchar(10)),1,3)+'-'+" +
                      "substring(cast([num_Mobile]  as varchar(10)),4,3)+'-'+substring(cast([num_Mobile]  as varchar(10)),7,4) else '' end" +
                      " from [eTn_ShippingContacts] where bint_ShippingId=" + ShippingId;
            DataTable tempDt;
            tempDt = DBClass.returnDataTable(strQuery);
            if (tempDt.Rows.Count > 0)
            {
                StringBuilder tempStr = new StringBuilder();
                tempStr.Append(string.Empty);
                foreach (DataRow dr in tempDt.Rows)
                {
                    if (tempStr.ToString().Trim().Length != 0)
                        tempStr.Append("<br/>");
                    tempStr.Append(Convert.ToString(dr[0]).Trim() + "<br/>");
                }
                lblShippingContact.Text = tempStr.ToString().Trim();
            }
        }

       // GridBar3.HeaderText = "Notes";
       // GridBar3.LinksList = "Add Note";
       // GridBar3.PagesList = "AddNotes.aspx?" + Convert.ToInt64(ID);
        lnkNotes.PostBackUrl = "AddNotes.aspx?" + Convert.ToInt64(ID);

        Session["CustomerNoteBackPage"] = "TrackLoad";  
        strQuery = "SELECT sum([bint_Pieces]),sum([num_Weight]) FROM [eTn_Origin] where [bint_LoadId]=" + ID;
        dt = DBClass.returnDataTable(strQuery);
        if (dt.Rows.Count > 0)
        {
            lblPieces.Text = Convert.ToString(dt.Rows[0][0]);
            lblWeigth.Text = Convert.ToString(dt.Rows[0][1]);
        }
        dt = DBClass.returnDataTable("select L.[nvar_RailBill],L.[date_OrderBillDate],L.[nvar_PersonalBill],S.[nvar_LoadStatusDesc] from [eTn_Load] L " +
                                     "inner join [eTn_LoadStatus] S on L.bint_LoadStatusId=S.bint_LoadStatusId WHERE L.[bint_LoadId] =" + ID);
        if (dt.Rows.Count > 0)
        {
            lblStatus.Text = Convert.ToString(dt.Rows[0][3]);
            lblRailBill.Text = Convert.ToString(dt.Rows[0][0]);
            lblBillDate.Text = Convert.ToString(dt.Rows[0][1]);
            lblPersonBill.Text = Convert.ToString(dt.Rows[0][2]);
        }
        #region Document Data

        strQuery = "select isnull(bint_DocumentId,0) as 'Id',isnull(date_ReceivedDate,getdate()) as 'Date' from eTn_UploadDocuments where bint_LoadId=" + ID
            + " and bint_DocumentId <> (select bint_DocumentId from eTn_DocumentTable where lower(nvar_DocumentName)='load order') order by bint_DocumentId asc";
        dt = DBClass.returnDataTable(strQuery);
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (Convert.ToInt64(dt.Rows[i][0]))
                {
                    case 2:
                        lblPOrder.Text = Convert.ToDateTime(dt.Rows[i][1]).ToShortDateString();
                        break;
                    case 3:
                        lblBillofLading.Text = Convert.ToDateTime(dt.Rows[i][1]).ToShortDateString();
                        break;
                    case 4:
                        lblAccessorial.Text = Convert.ToDateTime(dt.Rows[i][1]).ToShortDateString();
                        break;
                    case 5:
                        lblOutGate.Text = Convert.ToDateTime(dt.Rows[i][1]).ToShortDateString();
                        break;
                    case 6:
                        lblInGate.Text = Convert.ToDateTime(dt.Rows[i][1]).ToShortDateString();
                        break;
                    case 7:
                        lblDelivery.Text = Convert.ToDateTime(dt.Rows[i][1]).ToShortDateString();
                        break;
                    case 8:
                        lblTicket.Text = Convert.ToDateTime(dt.Rows[i][1]).ToShortDateString();
                        break;
                }
            }

        }
        #endregion
        if (dt != null) { dt.Dispose(); dt = null; }

    }

    private void ViewDocumnets(string strFol)
    {
        if (ViewState["ID"] == null)
            return;
        strFol = strFol.Trim();
        string strPath = Server.MapPath(ConfigurationSettings.AppSettings["DocumentsFolder"]) + Convert.ToString(ViewState["ID"]).Trim() + "\\" + strFol;
        if (System.IO.Directory.Exists(strPath))
        {
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(strPath);
            System.IO.FileSystemInfo[] fi = di.GetFileSystemInfos();
            if (fi.Length > 0)
            {
                DateTime[] strTemp = new DateTime[fi.Length];
                string[] strSortFile = new string[fi.Length];
                for (int i = 0; i < fi.Length; i++)
                {
                    strTemp[i] = fi[i].CreationTime;
                }
                Array.Sort(strTemp);
                int index = 0; int flag = 0;
                for (int j = 0; j < strTemp.Length; j++)
                {
                    for (int i = 0; i < fi.Length; i++)
                    {
                        flag = 0;
                        if (fi[i].CreationTime == strTemp[j])
                        {
                            for (int k = 0; k < index; k++)
                            {
                                if (strSortFile[k] == fi[i].FullName)
                                {
                                    flag = 1; break;
                                }
                            }
                            if (flag == 0)
                            {
                                strSortFile[index] = fi[i].FullName;
                                index = index + 1;
                                break;
                            }
                            flag = 0;
                        }
                    }
                }
                if (strSortFile.Length > 0)
                {
                    string strFormat = "<table border=1 cellpadding=0 cellspacing=0 id=tbldisplay><tr><th>View Documents</th></tr>";
                    string[] strFileNames = null; string strOpenPath = "";
                    for (int i = 0; i < strSortFile.Length; i++)
                    {
                        strFileNames = strSortFile[i].Trim().Split('\\');
                        strFormat += "<tr id=" + ((i % 2 == 0) ? "row" : "altrow") + "><td align=left>";
                        strOpenPath = ConfigurationSettings.AppSettings["DocumentsFolder"] + Convert.ToString(ViewState["ID"]).Trim() + "\\" + strFol + "\\" + strFileNames[strFileNames.Length - 1];
                        strOpenPath = strOpenPath.Replace("\\\\", "\\");
                        strFormat += "<a style=font-size:12;color:blue;height:15; href='" + strOpenPath + "' target='_blank'>" + strFileNames[strFileNames.Length - 1] + "</a>";
                        strFormat += "</td></tr>";
                    }
                    strFormat += "</table>";
                    plhDocs.Controls.Add(new LiteralControl(strFormat));
                    strFileNames = null;
                }
            }
            fi = null; di = null;
        }
    }
    private void GetDocumentsbyID(long DocumentId)
    {
        if (ViewState["ID"] != null)
        {
            DataTable dt = DBClass.returnDataTable("select [nvar_DocumentName],isnull([bint_FileNo],0),[date_ReceivedDate] from [eTn_UploadDocuments] where [bint_LoadId]=" + Convert.ToInt64(ViewState["ID"]) + " and [bint_DocumentId]=" + DocumentId + " and [nvar_DocumentName] is not null");
            if (dt.Rows.Count > 0)
            {
                string strFormat = "<table border=1 cellpadding=0 cellspacing=0 id=tbldisplay><tr><th>View Documents</th></tr>";
                string docpath = "../Documents" + "\\" + Convert.ToString(ViewState["ID"]).Trim() + "\\";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    strFormat += "<tr id=" + ((i % 2 == 0) ? "row" : "altrow") + "><td align=left>";
                    strFormat += "<a style=font-size:12;color:blue;height:15; href='" + docpath + dt.Rows[i][0].ToString() + "' target='_blank'>" + dt.Rows[i][0].ToString() + "</a>";
                    strFormat += "</td></tr>";
                }
                strFormat += "</table>";
                plhDocs.Controls.Add(new LiteralControl(strFormat));
            }
            else
                plhDocs.Visible = false;
            if (dt != null) { dt.Dispose(); dt = null; }
        }

    }
    protected void lnkLoadOrder_Click(object sender, EventArgs e)
    {
        //ViewDocumnets("1");
        GetDocumentsbyID(1);
    }
    protected void lnlPoOrder_Click(object sender, EventArgs e)
    {
        //ViewDocumnets("2");3
        GetDocumentsbyID(2);
    }
    protected void lnkLading_Click(object sender, EventArgs e)
    {
        //ViewDocumnets("3");
        GetDocumentsbyID(3);
    }
    protected void lnkAccessorial_Click(object sender, EventArgs e)
    {
        //ViewDocumnets("4");
        GetDocumentsbyID(4);
    }
    protected void lnkOutgate_Click(object sender, EventArgs e)
    {
        //ViewDocumnets("5");
        GetDocumentsbyID(5);
    }
    protected void lnkInGate_Click(object sender, EventArgs e)
    {
        //ViewDocumnets("6");
        GetDocumentsbyID(6);
    }
    protected void lnkProof_Click(object sender, EventArgs e)
    {
        //ViewDocumnets("7");
        GetDocumentsbyID(7);
    }
    protected void lnkMisc_Click(object sender, EventArgs e)
    {
       // ViewDocumnets("8");
        GetDocumentsbyID(8);
    }
    protected void lnkDispatch_Click(object sender, EventArgs e)
    {
        Response.Redirect("Dispatch.aspx?" + Convert.ToString(ViewState["ID"]));
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (Session["CustomerId"] != null && ViewState["ID"]!=null)
        {
            Session["ViewLog"] = true;
            if (txtContainer.Text.Trim().Length > 0)
            {
                switch (ddlContainer.SelectedIndex)
                {
                    case 0:
                        Session["TrackWhereClause"] = "(eTn_Load.[nvar_Container] like '" + txtContainer.Text.Trim() + "' or eTn_Load.[nvar_Container1] like '" + txtContainer.Text.Trim() + "') and eTn_Load.[bint_CustomerId]=" + Convert.ToInt64(Session["CustomerId"]) + "";
                        break;
                    case 1:
                        Session["TrackWhereClause"] = "eTn_Load.[nvar_Ref] like '" + txtContainer.Text.Trim() + "%'  and eTn_Load.[bint_CustomerId]=" + Convert.ToInt64(Session["CustomerId"]);
                        break;
                    case 2:
                        Session["TrackWhereClause"] = "eTn_Load.[nvar_Booking] like '" + txtContainer.Text.Trim() + "%'  and eTn_Load.[bint_CustomerId]=" + Convert.ToInt64(Session["CustomerId"]);
                        break;
                    case 3:
                        try
                        {
                            Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=" + Convert.ToInt64(txtContainer.Text.Trim()) + "  and eTn_Load.[bint_CustomerId]=" + Convert.ToInt64(Session["CustomerId"]);
                        }
                        catch
                        {
                            Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=0";
                        }
                        break;
                }
            }
            else
            {
                Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=0";
            }
            bool blVal = false;
            try
            {
                if (Convert.ToInt32(DBClass.executeScalar("select count(bint_LoadId) from eTn_Load where " + Convert.ToString(Session["TrackWhereClause"]))) == 1)
                {
                    blVal = true;
                }
            }
            catch
            {                
            }
            if(blVal)
            {
                Response.Redirect("CustomerTrackLoad.aspx?" + DBClass.executeScalar("select bint_LoadId from eTn_Load where " + Convert.ToString(Session["TrackWhereClause"])));
            }
            
           Response.Redirect("CustomerTrackLoadResults.aspx");
        }
    }
    protected void chkHotShipment_CheckedChanged(object sender, EventArgs e)
    {
        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_UpdateLoadHotShipment", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToString(Session["UserLoginId"]), chkHotShipment.Checked });
        FillDetails(Convert.ToInt64(ViewState["ID"]));
    }
}
