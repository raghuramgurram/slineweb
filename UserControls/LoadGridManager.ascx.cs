using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;


public partial class LoadGridManager : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["LoadGridPageIndex"] == null)
                BindGrid(0);
            else
            {
                BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
            }
        }
        catch
        {
            string strpath = System.Web.HttpRuntime.AppDomainAppPath + "\\ErrorLog.txt";
            System.IO.FileInfo fileinfo = new System.IO.FileInfo(strpath);
            if (fileinfo.Exists)
            {
                System.IO.StreamWriter writer = fileinfo.AppendText();
                writer.Write("The Datetime Error is happend for Load#" + Convert.ToString(Session["ErrorLoad"]));
                writer.Write(DateTime.Now);
                writer.Close();
            }
        }
    }

    public string MainTableName
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["MainTableName"] = Convert.ToString(value).Trim();
            }
        }
    }
    public string MainTablePK
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["MainTablePK"] = Convert.ToString(value).Trim();
            }
        }
    }
    public string SingleRowColumnsList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["SingleRowColumnsList"] = Convert.ToString(value).Trim().Replace(";", ",");
            }
        }
    }
    public string InnerJoinClauseWithOnlySingleRowTables
    {
        set
        {
            ViewState["InnerJoinClauseWithOnlySingleRowTables"] = Convert.ToString(value).Trim();
        }
    }
    public string SingleRowColumnsWhereClause
    {
        set
        {
            ViewState["SingleRowColumnsWhereClause"] = Convert.ToString(value).Trim();
        }
    }
    public string SingleRowColumnsOrderByClause
    {
        set
        {
            ViewState["SingleRowColumnsOrderByClause"] = Convert.ToString(value).Trim();
        }
    }
    public string MultipleRowTablesList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["MultipleRowTableNames"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    public string MultipleRowTableKeyList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["MultipleRowTableKeyList"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    public string MultipleRowTableColumnsList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["MultipleRowTableColumnsList"] = Convert.ToString(value).Trim().Split(';');
            }
        }
    }
    public string MultipleRowTablesInnnerJoinClause
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["MultipleRowTablesInnnerJoinClause"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    public string MultipleRowTablesAdditionalWhereClause
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["MultipleRowTablesAdditionalWhereClause"] = Convert.ToString(value).Trim().Split(';');
            }
        }
    }
    public string OptionalRowTablesList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["OptionalRowTablesList"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    public string OptionalRowTableKeyList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["OptionalRowTableKeyList"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    public string OptionalRowTableColumnsList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["OptionalRowTableColumnsList"] = Convert.ToString(value).Trim().Split(';');
            }
        }
    }
    public string OptionalRowTablesInnnerJoinClauseList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["OptionalRowTablesInnnerJoinClauseList"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    //public string OptionalRowTablesWhereClause
    //{
    //    set
    //    {
    //        if (Convert.ToString(value).Trim().Length > 0)
    //        {
    //            ViewState["OptionalRowTablesWhereClause"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
    //        }
    //    }
    //}
    public string VisibleColumnsList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["VisibleColumnsList"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    public bool DeleteVisible
    {
        set
        {
            ViewState["DeleteVisible"] = value;
        }
    }
    public string DependentTablesList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["DependentTablesList"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    public string DependentTableKeysList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["DependentTableKeysList"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    public string DeleteTablesList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["DeleteTablesList"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    public string DeleteTablePKList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["DeleteTablePKList"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    public string LoadStatus
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["LoadStatus"] = Convert.ToString(value).Trim();
            }
        }
    }
    public string LastColumnLinksList
    {
        set
        {
            ViewState["LastColumnLinksList"] = (Convert.ToString(value).Trim().Length > 0) ? (Convert.ToString(value).Trim().Replace(",", ";").Split(';')) : null;
        }
    }
    public string LastColumnPagesList
    {
        set
        {
            ViewState["LastColumnPagesList"] = (Convert.ToString(value).Trim().Length > 0) ? (Convert.ToString(value).Trim().Replace(",", ";").Split(';')) : null;
        }
    }
    public bool LastLocationVisible
    {
        set
        {
            ViewState["LastLocationVisible"] = value;
        }
    }
    public string MultipleRowOrderByCluase
    {
        set
        {
            ViewState["MultipleRowOrderByCluase"] = Convert.ToString(value).Trim().Split(';');
        }
    }
    public string Role
    {
        set
        {
            ViewState["Role"] = Convert.ToString(value).Trim().Split(';');
        }
    }
    public string ShowStatus
    {
        get
        {
            if (Session["ShowStatusValueforUser"] == null)
            {
                Session["ShowStatusValueforUser"] = "Non LFD";
            }
            return Session["ShowStatusValueforUser"].ToString();
        }
        set
        {
            Session["ShowStatusValueforUser"] = value;
        }
    }
    private void FormatGrid()
    {
        dggrid.AutoGenerateColumns = false;
        dggrid.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["LoadGridPageSize"].ToString());
        dggrid.AllowCustomPaging = true;
        dggrid.AllowPaging = true;
        dggrid.AllowSorting = true;
        dggrid.PagerStyle.Mode = PagerMode.NumericPages;
        dggrid.PagerStyle.HorizontalAlign = HorizontalAlign.Center;
    }
    public void BindGrid(int iPageNumber)
    {
        if (Convert.ToString(ViewState["MainTableName"]).Trim().Length > 0)
        {
            #region Validation
            //if (ViewState["VisibleHeadersList"] != null && ViewState["VisibleColumnsList"] != null)
            //    if (((string[])ViewState["VisibleHeadersList"]).Length != ((string[])ViewState["VisibleColumnsList"]).Length)
            //        return;
            #endregion
            #region Getting Main DataSet
            object[] parms = null;
            string strProdName = "";
            if (Convert.ToString(ViewState["InnerJoinClauseWithOnlySingleRowTables"]).Trim().Length == 0)
            {
                parms = new object[] { Convert.ToString(ViewState["MainTableName"]), Convert.ToString(ViewState["MainTablePK"]), Convert.ToString(ViewState["SingleRowColumnsList"]), ConfigurationSettings.AppSettings["LoadGridPageSize"].ToString(), iPageNumber, Convert.ToString(ViewState["SingleRowColumnsWhereClause"]) };
                strProdName = "GetPagedData";
            }
            else
            {
                if (Convert.ToString(ViewState["SingleRowColumnsOrderByClause"]).Trim().Length == 0)
                {
                    parms = new object[] { Convert.ToString(ViewState["MainTableName"]), Convert.ToString(ViewState["MainTablePK"]), Convert.ToString(ViewState["SingleRowColumnsList"]), ConfigurationSettings.AppSettings["LoadGridPageSize"].ToString(), iPageNumber, Convert.ToString(ViewState["InnerJoinClauseWithOnlySingleRowTables"]), Convert.ToString(ViewState["SingleRowColumnsWhereClause"]) };
                    strProdName = "GetPagedDataWithJoins";
                }
                else
                {
                    parms = new object[] { Convert.ToString(ViewState["MainTableName"]), Convert.ToString(ViewState["MainTablePK"]), Convert.ToString(ViewState["SingleRowColumnsList"]), ConfigurationSettings.AppSettings["LoadGridPageSize"].ToString(), iPageNumber, Convert.ToString(ViewState["InnerJoinClauseWithOnlySingleRowTables"]), Convert.ToString(ViewState["SingleRowColumnsWhereClause"]), Convert.ToString(ViewState["SingleRowColumnsOrderByClause"]) };
                    strProdName = "GetPagedDataWithJoinsAndOrderBy";
                    if (Session["IsNewStatus"] != null)
                        strProdName = "GetDistinctPagedDataWithJoinsAndOrderBy";
                }
            }
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], strProdName, parms);
            parms = null;
            #endregion
            #region Clearing Grid
            dggrid.Controls.Clear();
            dggrid.Columns.Clear();
            #endregion
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    #region Formatting DataSet For Optional Row Columns
                    if (ViewState["OptionalRowTablesList"] != null && ViewState["OptionalRowTableColumnsList"] != null && ViewState["OptionalRowTableColumnsList"] != null)
                    {
                        string[] strOptionalRowTablesList = (string[])ViewState["OptionalRowTablesList"];
                        string[] strOptionalRowTableKeyList = (string[])ViewState["OptionalRowTableKeyList"];
                        string[] strOptionalRowTableColumnsList = (string[])ViewState["OptionalRowTableColumnsList"];
                        string[] strOptionalRowTablesInnnerJoinClauseList = null;
                        bool isOptionalInnerJoinExist = false;
                        if (ViewState["OptionalRowTablesInnnerJoinClauseList"] != null)
                        {
                            strOptionalRowTablesInnnerJoinClauseList = (string[])ViewState["OptionalRowTablesInnnerJoinClauseList"];
                            isOptionalInnerJoinExist = true;
                        }

                        if (strOptionalRowTablesList.Length == strOptionalRowTableKeyList.Length && strOptionalRowTableKeyList.Length == strOptionalRowTableColumnsList.Length)
                        {
                            string[] strOptID = null;
                            string strOptQuery = "";
                            DataTable dtOpt = null;
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                strOptID = Convert.ToString(ds.Tables[0].Rows[i][0]).Trim().Split('^');
                                for (int j = 0; j < strOptionalRowTablesList.Length; j++)
                                {
                                    string[] strTables = strOptionalRowTablesList[j].Split('^');
                                    string[] strKeys = strOptionalRowTableKeyList[j].Split('^');
                                    string[] strColumns = strOptionalRowTableColumnsList[j].Split('^');
                                    if (strTables.Length == strKeys.Length && strKeys.Length == strColumns.Length)
                                    {
                                        for (int k = 0; k < strTables.Length; k++)
                                        {
                                            if (isOptionalInnerJoinExist)
                                                strOptQuery = "select " + strColumns[k] + " from " + strTables[k] + " " + (strOptionalRowTablesInnnerJoinClauseList[j].Split('^'))[k] + " where " + strKeys[k] + "=" + strOptID[0];
                                            else
                                                strOptQuery = "select " + strColumns[k] + " from " + strTables[k] + " where " + strKeys[k] + "=" + strOptID[0];
                                            if (strTables[k].Trim() == "eTn_AssignDriver")
                                                strOptQuery += " order by eTn_AssignDriver.[date_DateAssigned] ASC";
                                            if (dtOpt != null) { dtOpt.Dispose(); dtOpt = null; }
                                            dtOpt = DBClass.returnDataTable(strOptQuery);
                                            if (dtOpt.Rows.Count > 0)
                                            {
                                                for (int colCount = 0; colCount < dtOpt.Columns.Count; colCount++)
                                                {
                                                    if (!ds.Tables[0].Columns.Contains(dtOpt.Columns[colCount].ColumnName))
                                                        ds.Tables[0].Columns.Add(dtOpt.Columns[colCount].ColumnName, typeof(string));
                                                    for (int rowCount = 0; rowCount < dtOpt.Rows.Count; rowCount++)
                                                    {
                                                        if (rowCount == 0)
                                                        {
                                                            ds.Tables[0].Rows[i][dtOpt.Columns[colCount].ColumnName] = Convert.ToString(dtOpt.Rows[rowCount][colCount]).Trim();
                                                        }
                                                        else
                                                        {
                                                            ds.Tables[0].Rows[i][dtOpt.Columns[colCount].ColumnName] = Convert.ToString(ds.Tables[0].Rows[i][dtOpt.Columns[colCount].ColumnName]) + "^^^^" + Convert.ToString(dtOpt.Rows[rowCount][colCount]).Trim();
                                                        }
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                    }
                                    strTables = null; strKeys = null; strColumns = null;
                                }
                                strOptID = null;
                            }
                            if (dtOpt != null) { dtOpt.Dispose(); dtOpt = null; }
                            strOptID = null;
                        }
                        else
                        {
                            strOptionalRowTablesList = null; strOptionalRowTableKeyList = null; strOptionalRowTableColumnsList = null; strOptionalRowTablesInnnerJoinClauseList = null;
                            return;
                        }
                        strOptionalRowTablesList = null; strOptionalRowTableKeyList = null; strOptionalRowTableColumnsList = null; strOptionalRowTablesInnnerJoinClauseList = null;
                    }
                    #endregion
                    #region Formatting DataSet For Multiple Row Columns
                    if (ViewState["MultipleRowTableNames"] != null && ViewState["MultipleRowTableKeyList"] != null && ViewState["MultipleRowTableColumnsList"] != null)
                    {
                        string[] strMultipleRowTableNames = (string[])ViewState["MultipleRowTableNames"];
                        string[] strMultipleRowTableKeyList = (string[])ViewState["MultipleRowTableKeyList"];
                        string[] strMultipleRowTableColumnsList = (string[])ViewState["MultipleRowTableColumnsList"];
                        string[] strMultipleRowTablesInnnerJoinClauseList = null;
                        string[] strMultipleRowOrderByClauseList = null;
                        //ViewState["MultipleRowOrderByCluase"]
                        bool isInnerJoinExist = false; bool IsOrderByExist = false;
                        if (ViewState["MultipleRowTablesInnnerJoinClause"] != null)
                        {
                            strMultipleRowTablesInnnerJoinClauseList = (string[])ViewState["MultipleRowTablesInnnerJoinClause"];
                            isInnerJoinExist = true;
                        }

                        if (ViewState["MultipleRowOrderByCluase"] != null)
                        {
                            strMultipleRowOrderByClauseList = (string[])ViewState["MultipleRowOrderByCluase"];
                            IsOrderByExist = true;
                        }

                        string[] strMultipleRowTablesAdditionalWhereClause = null;
                        bool isWhereClauseExist = false;
                        if (ViewState["MultipleRowTablesAdditionalWhereClause"] != null)
                        {
                            strMultipleRowTablesAdditionalWhereClause = (string[])ViewState["MultipleRowTablesAdditionalWhereClause"];
                            isWhereClauseExist = true;
                        }
                        //ViewState["MultipleRowTablesWhereClause"]
                        if (strMultipleRowTableNames.Length == strMultipleRowTableKeyList.Length && strMultipleRowTableKeyList.Length == strMultipleRowTableColumnsList.Length)
                        {
                            string[] strID = null;
                            string strQuery = "";
                            DataTable dt = null;
                            string tempIds = string.Empty;
                            for (int i = (ds.Tables[0].Rows.Count - 1); i >= 0; i--)
                            {
                                string[] tempArry = tempIds.Split('+');
                                bool isFind = false;
                                foreach (string str in tempArry)
                                {
                                    if (str.Trim() == (Convert.ToString(ds.Tables[0].Rows[i][0])).Trim())
                                    {
                                        isFind = true;
                                        break;
                                    }
                                }

                                if (isFind)
                                    ds.Tables[0].Rows.RemoveAt(i);
                                else
                                    tempIds = (Convert.ToString(ds.Tables[0].Rows[i][0])).Trim() + "+";
                            }

                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {

                                strID = Convert.ToString(ds.Tables[0].Rows[i][0]).Trim().Split('^');
                                for (int j = 0; j < strMultipleRowTableNames.Length; j++)
                                {
                                    strMultipleRowTableColumnsList[j] = strMultipleRowTableColumnsList[j].Trim().Replace("^", ",");
                                    if (isInnerJoinExist)
                                        strQuery = "select " + strMultipleRowTableColumnsList[j] + " from " + strMultipleRowTableNames[j] + " " + strMultipleRowTablesInnnerJoinClauseList[j] + " where " + strMultipleRowTableKeyList[j] + "=" + strID[0];
                                    else
                                        strQuery = "select " + strMultipleRowTableColumnsList[j] + " from " + strMultipleRowTableNames[j] + " where " + strMultipleRowTableKeyList[j] + "=" + strID[0];
                                    if (isWhereClauseExist)
                                    {
                                        if (strMultipleRowTablesAdditionalWhereClause[j].Trim().Length > 0)
                                            strQuery += " and " + strMultipleRowTablesAdditionalWhereClause[j];
                                    }


                                    //Gettting err here
                                    if (IsOrderByExist)
                                    {
                                        if (strMultipleRowOrderByClauseList.Length == 2)
                                        {
                                            if (strMultipleRowOrderByClauseList[j].Trim().Length > 0)
                                            {
                                                strQuery += strMultipleRowOrderByClauseList[j];
                                            }
                                        }
                                    }

                                    //Getting the Delivery SORT here
                                    if (dt != null) { dt.Dispose(); dt = null; }
                                    dt = DBClass.returnDataTable(strQuery);
                                    for (int colCount = 0; colCount < dt.Columns.Count; colCount++)
                                    {
                                        if (!ds.Tables[0].Columns.Contains(dt.Columns[colCount].ColumnName))
                                            ds.Tables[0].Columns.Add(dt.Columns[colCount].ColumnName, typeof(string));
                                        if (Convert.ToString(ds.Tables[0].Rows[i][dt.Columns[colCount].ColumnName]).Trim().Length == 0)
                                        {
                                            for (int rowCount = 0; rowCount < dt.Rows.Count; rowCount++)
                                            {
                                                if (rowCount == 0)
                                                {
                                                    ds.Tables[0].Rows[i][dt.Columns[colCount].ColumnName] = Convert.ToString(dt.Rows[rowCount][colCount]).Trim();
                                                }
                                                else
                                                {
                                                    ds.Tables[0].Rows[i][dt.Columns[colCount].ColumnName] = Convert.ToString(ds.Tables[0].Rows[i][dt.Columns[colCount].ColumnName]) + "^^^^" + Convert.ToString(dt.Rows[rowCount][colCount]).Trim();
                                                }
                                            }
                                        }
                                    }
                                }
                                strID = null;
                            }
                            if (dt != null) { dt.Dispose(); dt = null; }
                            strID = null;
                        }
                        else
                        {
                            strMultipleRowTableNames = null; strMultipleRowTableKeyList = null; strMultipleRowTableColumnsList = null; strMultipleRowTablesInnnerJoinClauseList = null; strMultipleRowOrderByClauseList = null;
                            return;
                        }
                        strMultipleRowTableNames = null; strMultipleRowTableKeyList = null; strMultipleRowTableColumnsList = null; strMultipleRowTablesInnnerJoinClauseList = null;
                    }
                    #endregion
                    #region Formatting DataSet For Last Location
                    //if (Convert.ToString(ViewState["LastLocationVisible"]).Trim().Length > 0)
                    //{
                    //    if (Convert.ToBoolean(ViewState["LastLocationVisible"]))
                    //    {
                    //        if (ds.Tables[0].Columns.Contains("Container1") || ds.Tables[0].Columns.Contains("Container2"))
                    //        {
                    //            string strQuer = "", strRes = "";
                    //            ds.Tables[0].Columns.Add("Last Location", typeof(string));
                    //            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    //            {
                    //                if (ds.Tables[0].Columns.Contains("Container1"))
                    //                {
                    //                    strQuer = "Select " + CommonFunctions.AddressQueryString("eTn_Equipment.nvar_City;eTn_Equipment.nvar_State", "Last Location") + " from eTn_Load " +
                    //                        "inner join eTn_Equipment on eTn_Equipment.nvar_EquipmentName = eTn_Load.nvar_Container where eTn_Load.bint_LoadId = " + (ds.Tables[0].Rows[i][0].ToString().Split('^'))[0];
                    //                    strRes = DBClass.executeScalar(strQuer);
                    //                    if (strRes.Length > 0)
                    //                    {
                    //                        ds.Tables[0].Rows[i]["Last Location"] = strRes;
                    //                    }
                    //                }
                    //                if (ds.Tables[0].Columns.Contains("Container2"))
                    //                {
                    //                    strQuer = "Select " + CommonFunctions.AddressQueryString("eTn_Equipment.nvar_City;eTn_Equipment.nvar_State", "Last Location") + " from eTn_Load " +
                    //                            "inner join eTn_Equipment on eTn_Equipment.nvar_EquipmentName = eTn_Load.nvar_Container1 where eTn_Load.bint_LoadId = " + (ds.Tables[0].Rows[i][0].ToString().Split('^'))[0];
                    //                    strRes = DBClass.executeScalar(strQuer);
                    //                    if (strRes.Length > 0)
                    //                    {
                    //                        if (Convert.ToString(ds.Tables[0].Rows[i]["Last Location"]).Trim().Length == 0)
                    //                            ds.Tables[0].Rows[i]["Last Location"] = strRes;
                    //                        else
                    //                            ds.Tables[0].Rows[i]["Last Location"] = Convert.ToString(ds.Tables[0].Rows[i]["Last Location"]) + "^^^^" + strRes;
                    //                    }
                    //                }
                    //            }
                    //            ds.Tables[0].AcceptChanges();
                    //        }
                    //    }
                    //}
                    #endregion
                    #region Sorting Dataset
                    //if (string.Compare(Convert.ToString(ViewState["LoadStatus"]), "Transit", true, System.Globalization.CultureInfo.CurrentCulture) != 0)
                    //{
                    //DataRow[] drc1 = ds.Tables[0].Select("[Last Free Date] is not null and len([Last Free Date]) > 0", "[Last Free Date] asc,[Load] asc");
                    //DataRow[] drc2 = ds.Tables[0].Select("[Last Free Date] is null or len([Last Free Date]) = 0", "[Load] asc");
                    //if (drc1.Length + drc2.Length <= Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]))
                    //{
                    //    DataTable dtTemp = ds.Tables[0].Clone();
                    //    DataRow dr = ds.Tables[0].NewRow();
                    //    for (int p = 0; p < drc1.Length; p++)
                    //    {
                    //        dtTemp.ImportRow(drc1[p]);
                    //        dtTemp.AcceptChanges();
                    //        dr = null;
                    //    }
                    //    dr = null;
                    //    for (int p = 0; p < drc2.Length; p++)
                    //    {
                    //        dtTemp.ImportRow(drc2[p]);
                    //        dtTemp.AcceptChanges();
                    //        dr = null;
                    //    }
                    //    dr = null;
                    //    ds.Tables[0].Rows.Clear();
                    //    for (int p = 0; p < dtTemp.Rows.Count; p++)
                    //    {
                    //        ds.Tables[0].ImportRow(dtTemp.Rows[p]);
                    //        ds.Tables[0].AcceptChanges();
                    //    }
                    //    if (dtTemp != null) { dtTemp.Dispose(); dtTemp = null; }
                    //}
                    //drc2 = null; drc1 = null;
                    //}
                    #endregion
                    FormatGrid();
                    #region Column Addition
                    string[] strVisibleColumnsList = (string[])ViewState["VisibleColumnsList"];
                    BoundColumn bCol = null;
                    for (int colCount = 0; colCount < ds.Tables[0].Columns.Count; colCount++)
                    {
                        if (!this.IsColumnVisible(ds.Tables[0].Columns[colCount].ColumnName, strVisibleColumnsList))
                        {
                            bCol = new BoundColumn();
                            bCol.DataField = bCol.HeaderText = ds.Tables[0].Columns[colCount].ColumnName;
                            bCol.Visible = false;
                            bCol.HeaderStyle.CssClass = "GridHeader";
                            bCol.ItemStyle.CssClass = "GridItem";
                            dggrid.Columns.Add(bCol);
                            bCol = null;
                        }
                    }
                    TemplateColumn tCol = null;
                    for (int colCount = 0; colCount < strVisibleColumnsList.Length; colCount++)
                    {
                        if (ds.Tables[0].Columns.Contains(strVisibleColumnsList[colCount]))
                        {
                            tCol = new TemplateColumn();
                            if (string.Compare(strVisibleColumnsList[colCount], "Type", true, System.Globalization.CultureInfo.CurrentCulture) == 0 && ViewState["DeleteVisible"] != null)
                                tCol.ItemTemplate = new LoadGridManagerTemplate(ListItemType.Item, strVisibleColumnsList[colCount], ds.Tables[0], Convert.ToBoolean(ViewState["DeleteVisible"]), Convert.ToString(ViewState["LoadStatus"]));
                            else
                                tCol.ItemTemplate = new LoadGridManagerTemplate(ListItemType.Item, strVisibleColumnsList[colCount], ds.Tables[0], Convert.ToString(ViewState["LoadStatus"]));
                            tCol.HeaderText = strVisibleColumnsList[colCount];
                            tCol.Visible = true;
                            tCol.HeaderStyle.CssClass = "GridHeader";
                            tCol.ItemStyle.CssClass = "GridItem";
                            dggrid.Columns.Add(tCol);
                            tCol = null;
                        }
                    }
                    strVisibleColumnsList = null; tCol = null;
                    bCol = null;
                    #region Last Column
                    if (ViewState["LastColumnLinksList"] != null && ViewState["LastColumnPagesList"] != null)
                    {
                        string[] strLastColumnPagesList = (string[])ViewState["LastColumnPagesList"];
                        string[] strLastColumnLinksList = (string[])ViewState["LastColumnLinksList"];
                        if (strLastColumnPagesList.Length == strLastColumnLinksList.Length)
                        {
                            TemplateColumn tLastCol = new TemplateColumn();
                            tLastCol.ItemTemplate = new LoadGridManagerTemplate(ListItemType.Item, "LastColumn", strLastColumnLinksList, strLastColumnPagesList, ds.Tables[0], Convert.ToString(ViewState["LoadStatus"]));
                            tLastCol.HeaderStyle.CssClass = "GridHeader";
                            tLastCol.ItemStyle.CssClass = "GridItem";
                            dggrid.Columns.Add(tLastCol);
                            tLastCol = null;
                        }
                        strLastColumnLinksList = null; strLastColumnLinksList = null;
                    }
                    #endregion
                    #endregion
                    #region Check Role

                    if (ViewState["Role"] != null)
                    {
                        if (((string[])ViewState["Role"])[0].Trim().ToLower() == "driver")
                        {
                            // if it is driver we show origin and destination as to from of the driver
                            if (ds.Tables[0].Columns.Contains("FromAddress") && ds.Tables[0].Columns.Contains("ToAddress") && ds.Tables[0].Columns.Contains("Origin") && ds.Tables[0].Columns.Contains("Destination"))
                            {
                                foreach (DataRow dr in ds.Tables[0].Rows)
                                {
                                    if (dr["FromAddress"] != null && !string.IsNullOrEmpty(dr["FromAddress"].ToString().Trim()) && dr["FromAddress"].ToString().Trim() != "<br/><br/>")
                                    {
                                        dr["Origin"] = dr["FromAddress"].ToString().Trim();
                                    }
                                    if (dr["ToAddress"] != null && !string.IsNullOrEmpty(dr["ToAddress"].ToString().Trim()) && dr["ToAddress"].ToString().Trim() != "<br/><br/>")
                                    {
                                        dr["Destination"] = dr["ToAddress"].ToString().Trim();
                                    }
                                }
                            }
                            //ds.Tables[0].Columns["Origin"].ColumnName = "Origin/From";
                            //ds.Tables[0].Columns["Destination"].ColumnName = "Destination/To";
                        }
                    }

                    #endregion
                    #region Binding DataSet to Grid
                    dggrid.AllowCustomPaging = true;
                    dggrid.VirtualItemCount = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                    dggrid.DataSource = ds.Tables[0];
                    dggrid.CurrentPageIndex = iPageNumber;
                    dggrid.DataBind();
                    //dggrid.ItemStyle.CssClass = "GridItem";
                    //dggrid.HeaderStyle.CssClass = "GridHeader";
                    //dggrid.AlternatingItemStyle.CssClass = "GridAltItem";
                    if (dggrid.Items.Count > 0)
                    {
                        switch (Convert.ToString(ViewState["LoadStatus"]).Trim().ToLower())
                        {
                            case "new"://must be modified
                                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                                {
                                    if (index % 2 == 0)
                                    {
                                        for (int k = 0; k < dggrid.Columns.Count; k++)
                                        {
                                            dggrid.Items[index].Cells[k].CssClass = "GridItem";
                                        }
                                    }
                                    else
                                    {
                                        for (int k = 0; k < dggrid.Columns.Count; k++)
                                        {
                                            dggrid.Items[index].Cells[k].CssClass = "GridAltItem";
                                        }
                                    }
                                    if (Convert.ToString(ds.Tables[0].Rows[index]["Last Free Date"]).Trim().Length > 0)
                                    {
                                        if (Convert.ToDateTime(ds.Tables[0].Rows[index]["Last Free Date"].ToString().Trim()).Date <= DateTime.Now.Date)
                                        {
                                            for (int k = 0; k < dggrid.Columns.Count; k++)
                                            {
                                                dggrid.Items[index].Cells[k].CssClass = "redrow";
                                            }
                                        }
                                        else if (Convert.ToDateTime(ds.Tables[0].Rows[index]["Last Free Date"].ToString().Trim()).Date == DateTime.Now.Date.AddDays(1))
                                        {
                                            for (int k = 0; k < dggrid.Columns.Count; k++)
                                            {
                                                dggrid.Items[index].Cells[k].CssClass = "yellowrow";
                                            }
                                        }
                                    }
                                }
                                break;
                            case "assigned":
                                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                                {
                                    if (Convert.ToString(ds.Tables[0].Rows[index]["Last Free Date"]).Trim().Length > 0)
                                    {
                                        if (Convert.ToDateTime(ds.Tables[0].Rows[index]["Last Free Date"].ToString().Trim()).Date <= DateTime.Now.Date)
                                        {
                                            for (int k = 0; k < dggrid.Columns.Count; k++)
                                            {
                                                dggrid.Items[index].Cells[k].CssClass = "redrow";
                                            }
                                        }
                                        else if (Convert.ToDateTime(ds.Tables[0].Rows[index]["Last Free Date"].ToString().Trim()).Date == DateTime.Now.Date.AddDays(1))
                                        {
                                            for (int k = 0; k < dggrid.Columns.Count; k++)
                                            {
                                                dggrid.Items[index].Cells[k].CssClass = "yellowrow";
                                            }
                                        }
                                        else
                                        {
                                            for (int k = 0; k < dggrid.Columns.Count; k++)
                                            {
                                                dggrid.Items[index].Cells[k].CssClass = "greenrow";
                                            }
                                        }
                                    }
                                }
                                //for (int k = 0; k < dggrid.Columns.Count; k++)
                                //{
                                //    dggrid.Columns[k].ItemStyle.CssClass = "greenrow";                                    
                                //}
                                break;
                            case "pickedup":
                                for (int k = 0; k < dggrid.Columns.Count; k++)
                                {
                                    dggrid.Columns[k].ItemStyle.CssClass = "skybluerow";
                                }
                                break;
                            case "loaded in yard":
                                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                                {
                                    if (index % 2 == 0)
                                    {
                                        for (int k = 0; k < dggrid.Columns.Count; k++)
                                        {
                                            dggrid.Items[index].Cells[k].CssClass = "GridItem";
                                        }
                                    }
                                    else
                                    {
                                        for (int k = 0; k < dggrid.Columns.Count; k++)
                                        {
                                            dggrid.Items[index].Cells[k].CssClass = "GridAltItem";
                                        }
                                    }
                                }
                                break;
                            case "en-route":
                                for (int k = 0; k < dggrid.Columns.Count; k++)
                                {
                                    dggrid.Columns[k].ItemStyle.CssClass = "yellowrow";
                                }
                                break;
                            case "drop in warehouse":
                                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                                {
                                    if (index % 2 == 0)
                                    {
                                        for (int k = 0; k < dggrid.Columns.Count; k++)
                                        {
                                            dggrid.Items[index].Cells[k].CssClass = "GridItem";
                                        }
                                    }
                                    else
                                    {
                                        for (int k = 0; k < dggrid.Columns.Count; k++)
                                        {
                                            dggrid.Items[index].Cells[k].CssClass = "GridAltItem";
                                        }
                                    }
                                }
                                break;
                            case "driver on waiting":
                                for (int k = 0; k < dggrid.Columns.Count; k++)
                                {
                                    dggrid.Columns[k].ItemStyle.CssClass = "orangerow";
                                }
                                //for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                                //{
                                //    if (index % 2 == 0)
                                //    {
                                //        for (int k = 0; k < dggrid.Columns.Count; k++)
                                //        {
                                //            dggrid.Items[index].Cells[k].CssClass = "GridItem";
                                //        }
                                //    }
                                //    else
                                //    {
                                //        for (int k = 0; k < dggrid.Columns.Count; k++)
                                //        {
                                //            dggrid.Items[index].Cells[k].CssClass = "GridAltItem";
                                //        }
                                //    }
                                //}
                                break;
                            case "delivered":
                                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                                {
                                    if (index % 2 == 0)
                                    {
                                        for (int k = 0; k < dggrid.Columns.Count; k++)
                                        {
                                            dggrid.Items[index].Cells[k].CssClass = "GridItem";
                                        }
                                    }
                                    else
                                    {
                                        for (int k = 0; k < dggrid.Columns.Count; k++)
                                        {
                                            dggrid.Items[index].Cells[k].CssClass = "GridAltItem";
                                        }
                                    }
                                }
                                break;
                            case "empty in yard":
                                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                                {
                                    if (index % 2 == 0)
                                    {
                                        for (int k = 0; k < dggrid.Columns.Count; k++)
                                        {
                                            dggrid.Items[index].Cells[k].CssClass = "GridItem";
                                        }
                                    }
                                    else
                                    {
                                        for (int k = 0; k < dggrid.Columns.Count; k++)
                                        {
                                            dggrid.Items[index].Cells[k].CssClass = "GridAltItem";
                                        }
                                    }
                                }
                                break;
                            case "terminated":
                                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                                {
                                    if (index % 2 == 0)
                                    {
                                        for (int k = 0; k < dggrid.Columns.Count; k++)
                                        {
                                            dggrid.Items[index].Cells[k].CssClass = "GridItem";
                                        }
                                    }
                                    else
                                    {
                                        for (int k = 0; k < dggrid.Columns.Count; k++)
                                        {
                                            dggrid.Items[index].Cells[k].CssClass = "GridAltItem";
                                        }
                                    }
                                }
                                break;
                            case "paperwork pending":
                                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                                {
                                    if (index % 2 == 0)
                                    {
                                        for (int k = 0; k < dggrid.Columns.Count; k++)
                                        {
                                            dggrid.Items[index].Cells[k].CssClass = "GridItem";
                                        }
                                    }
                                    else
                                    {
                                        for (int k = 0; k < dggrid.Columns.Count; k++)
                                        {
                                            dggrid.Items[index].Cells[k].CssClass = "GridAltItem";
                                        }
                                    }
                                }
                                break;
                            case "closed":
                                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                                {
                                    if (index % 2 == 0)
                                    {
                                        for (int k = 0; k < dggrid.Columns.Count; k++)
                                        {
                                            dggrid.Items[index].Cells[k].CssClass = "GridItem";
                                        }
                                    }
                                    else
                                    {
                                        for (int k = 0; k < dggrid.Columns.Count; k++)
                                        {
                                            dggrid.Items[index].Cells[k].CssClass = "GridAltItem";
                                        }
                                    }
                                }
                                break;
                            case "transit":
                                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                                {
                                    if (Convert.ToString(ds.Tables[0].Rows[index]["Status"]).Replace("^", "").Trim().Length > 0)
                                    {
                                        switch (Convert.ToString(ds.Tables[0].Rows[index]["Status"]).Trim().ToLower())
                                        {
                                            case "assigned":
                                                if (Convert.ToString(ds.Tables[0].Rows[index]["Last Free Date"]).Replace("^", "").Trim().Length > 0)
                                                {
                                                    if (Convert.ToDateTime(ds.Tables[0].Rows[index]["Last Free Date"].ToString().Replace("^", "").Trim()).Date <= DateTime.Now.Date)
                                                    {
                                                        for (int k = 0; k < dggrid.Columns.Count; k++)
                                                        {
                                                            dggrid.Items[index].Cells[k].CssClass = "redrow";
                                                        }
                                                        continue;
                                                    }
                                                    else if (Convert.ToDateTime(ds.Tables[0].Rows[index]["Last Free Date"].ToString().Replace("^", "").Trim()).Date == DateTime.Now.Date.AddDays(1))
                                                    {
                                                        for (int k = 0; k < dggrid.Columns.Count; k++)
                                                        {
                                                            dggrid.Items[index].Cells[k].CssClass = "greenrow";
                                                        }
                                                        continue;
                                                    }
                                                    else
                                                    {
                                                        if (Convert.ToString(ds.Tables[0].Rows[index]["Delivery Appt."]).Replace("^", "").Trim().Length > 0)
                                                        {
                                                            string dateString = ds.Tables[0].Rows[index]["Delivery Appt."].ToString().Split('^')[0];
                                                            if (Convert.ToDateTime(dateString.Trim()).Date <= DateTime.Now.Date)
                                                            {
                                                                for (int k = 0; k < dggrid.Columns.Count; k++)
                                                                {
                                                                    dggrid.Items[index].Cells[k].CssClass = "redrow";
                                                                }
                                                                continue;
                                                            }
                                                            else if (Convert.ToDateTime(dateString.Trim()).Date == DateTime.Now.Date.AddDays(1))
                                                            {
                                                                for (int k = 0; k < dggrid.Columns.Count; k++)
                                                                {
                                                                    dggrid.Items[index].Cells[k].CssClass = "greenrow";
                                                                }
                                                                continue;
                                                            }
                                                            else
                                                            {
                                                                for (int k = 0; k < dggrid.Columns.Count; k++)
                                                                {
                                                                    dggrid.Items[index].Cells[k].CssClass = "greenrow";
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            for (int k = 0; k < dggrid.Columns.Count; k++)
                                                            {
                                                                dggrid.Items[index].Cells[k].CssClass = "greenrow";
                                                            }
                                                        }
                                                    }
                                                }
                                                break;
                                            case "empty in yard":
                                                for (int k = 0; k < dggrid.Columns.Count; k++)
                                                {
                                                    dggrid.Items[index].Cells[k].CssClass = "redrow";
                                                }
                                                break;

                                            case "pickedup":
                                                for (int k = 0; k < dggrid.Columns.Count; k++)
                                                {
                                                    dggrid.Items[index].Cells[k].CssClass = "skybluerow";
                                                }
                                                break;
                                            case "en-route":
                                                for (int k = 0; k < dggrid.Columns.Count; k++)
                                                {
                                                    dggrid.Items[index].Cells[k].CssClass = "yellowrow";
                                                }
                                                break;
                                            case "driver on waiting":
                                                for (int k = 0; k < dggrid.Columns.Count; k++)
                                                {
                                                    dggrid.Items[index].Cells[k].CssClass = "orangerow";
                                                }
                                                break;

                                        }
                                    }
                                }
                                break;

                            case "containersgoingperdiem":
                                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                                {
                                    if (Convert.ToString(ds.Tables[0].Rows[index]["Empty Return Last Free Date"]).Trim().Length > 0)
                                    {
                                        //DateTime newdatetime = Convert.ToDateTime("1/1/1900 12:00:00 AM");
                                        //if (Convert.ToDateTime(ds.Tables[0].Rows[index]["Empty Return Last Free Date"].ToString().Trim()) == newdatetime)
                                        //{

                                        //    for (int k = 0; k < dggrid.Columns.Count; k++)
                                        //    {
                                        //        dggrid.Items[index].Cells[k].CssClass = "greenrow";
                                        //        if (k == 5)
                                        //        {
                                        //            dggrid.Items[index].Cells[k].Text = "";
                                        //        }
                                        //    }
                                        //}
                                        //else
                                        if (Convert.ToDateTime(ds.Tables[0].Rows[index]["Empty Return Last Free Date"].ToString().Trim()).Date <= DateTime.Now.Date)
                                        {
                                            for (int k = 0; k < dggrid.Columns.Count; k++)
                                            {
                                                dggrid.Items[index].Cells[k].CssClass = "redrow";
                                            }
                                        }
                                        else if (Convert.ToDateTime(ds.Tables[0].Rows[index]["Empty Return Last Free Date"].ToString().Trim()).Date == DateTime.Now.Date.AddDays(1))
                                        {
                                            for (int k = 0; k < dggrid.Columns.Count; k++)
                                            {
                                                dggrid.Items[index].Cells[k].CssClass = "yellowrow";
                                            }
                                        }
                                        else
                                        {
                                            for (int k = 0; k < dggrid.Columns.Count; k++)
                                            {
                                                dggrid.Items[index].Cells[k].CssClass = "greenrow";
                                            }
                                        }
                                    }
                                }

                                break;
                            default: break;
                        }

                        //HazmatLoad style

                        if (ds.Tables[0].Columns.Contains("HazmatLoad"))
                        {
                            for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                            {
                                if (Convert.ToBoolean(ds.Tables[0].Rows[index]["HazmatLoad"]))
                                {
                                    if (ds.Tables[0].Columns.Contains("LoadStatusID"))
                                    {
                                        if (Convert.ToInt32(ds.Tables[0].Rows[index]["LoadStatusID"]) != 10)
                                        {
                                            for (int k = 0; k < dggrid.Columns.Count; k++)
                                            {
                                                dggrid.Items[index].Cells[k].CssClass = "hazmatrow";
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //HazmatLoad style

                        //SLine 2016 Enhancements ---- HotShipment style

                        if (ds.Tables[0].Columns.Contains("HotShipment"))
                        {
                            for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                            {
                                if (Convert.ToBoolean(ds.Tables[0].Rows[index]["HotShipment"]))
                                {
                                    if (ds.Tables[0].Columns.Contains("LoadStatusID"))
                                    {
                                        if (Convert.ToInt32(ds.Tables[0].Rows[index]["LoadStatusID"]) != 10)
                                        {
                                            for (int k = 0; k < dggrid.Columns.Count; k++)
                                            {
                                                dggrid.Items[index].Cells[k].CssClass = "hazmatrow";
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //SLine 2016 Enhancements ---- HotShipment style

                    }
                    #region Column Width adjustment
                    if (dggrid.Columns.Count > 0)
                    {
                        for (int i = 0; i < dggrid.Columns.Count; i++)
                        {
                            if (string.Compare(dggrid.Columns[i].HeaderText, "Delivery Appt.", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
                                string.Compare(dggrid.Columns[i].HeaderText, "Pickup", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
                                string.Compare(dggrid.Columns[i].HeaderText, "Last Free Date", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
                                string.Compare(dggrid.Columns[i].HeaderText, "Date & Time", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
                                string.Compare(dggrid.Columns[i].HeaderText, "Appt. Date", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
                                string.Compare(dggrid.Columns[i].HeaderText, "Delivered Date", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
                                string.Compare(dggrid.Columns[i].HeaderText, "Terminated Date", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
                                string.Compare(dggrid.Columns[i].HeaderText, "Container Size/Location", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                            {
                                dggrid.Columns[i].ItemStyle.Width = 80;
                            }
                            if (string.Compare(dggrid.Columns[i].HeaderText, "Load", true, System.Globalization.CultureInfo.CurrentCulture) == 0 || string.Compare(dggrid.Columns[i].HeaderText, "Delivery Appt Date", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                                dggrid.Columns[i].ItemStyle.Width = 120;
                            if (string.Compare(dggrid.Columns[i].HeaderText, "Days In Transit", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
                                string.Compare(dggrid.Columns[i].HeaderText, "Days In Yard", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
                                string.Compare(dggrid.Columns[i].HeaderText, "St. Turn", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
                                string.Compare(dggrid.Columns[i].HeaderText, "No. of Days", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                                dggrid.Columns[i].ItemStyle.Width = 40;
                        }
                    }
                    #endregion
                    #endregion
                    //if (dtFinal != null) { dtFinal.Dispose(); dtFinal = null; }
                }
            }
            if (ds != null) { ds.Dispose(); ds = null; }
        }
    }
    protected bool IsColumnVisible(string strColName, string[] strVisibleColumnsList)
    {
        bool blVal = false;
        for (int i = 0; i < strVisibleColumnsList.Length; i++)
        {
            if (string.Compare(strVisibleColumnsList[i], strColName, true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                blVal = true;
                break;
            }
        }
        return blVal;
    }
    protected void dggrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        string[] strID = e.Item.Cells[0].Text.Trim().Split('^');
        switch (e.CommandName.ToLower())
        {
            //case "editload":
            //    Response.Redirect("NewLoad.aspx?" + strID[0].Trim()); break;
            //case "editcustomer":
            //    Response.Redirect("NewCustomer.aspx?" + strID[1].Trim()); break;
            //case "editdispatch":
            //    Response.Redirect("UpdateLoad.aspx?" + strID[0].Trim()); break;
            //case "editloadstatus":
            //    Response.Redirect("UpdateLoadStatus.aspx?" + strID[0].Trim()); break;
            case "delete":
                if (ViewState["DeleteTablesList"] != null)
                {
                    string[] DeleteTables = (string[])ViewState["DeleteTablesList"];
                    if (DeleteTables.Length > 0)
                    {
                        #region Check Dependencies
                        if (string.Compare(Convert.ToString(ViewState["LoadStatus"]), "New", true, System.Globalization.CultureInfo.CurrentCulture) != 0)
                        {
                            bool blCanWeDelete = true;
                            if (ViewState["DependentTablesList"] != null)
                            {
                                string[] DependentTables = (string[])ViewState["DependentTablesList"];
                                string[] DependentTableKeys = (string[])ViewState["DependentTableKeysList"];
                                if (DependentTables.Length == DependentTableKeys.Length)
                                {
                                    string strQuery = "";
                                    for (int i = 0; i < DependentTableKeys.Length; i++)
                                    {
                                        strQuery = "Select Count(" + DependentTableKeys[i].Trim() + ") from " + DependentTables[i].Trim() + " where " + DependentTableKeys[i].Trim() + "=" + strID[0];
                                        if (Convert.ToInt32(DBClass.executeScalar(strQuery)) > 0)
                                        {
                                            blCanWeDelete = false;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    blCanWeDelete = false;
                                }
                                DependentTables = null; DependentTableKeys = null;
                                if (!blCanWeDelete)
                                {
                                    Response.Write("<script>alert('Unable to delete due to dependencies.')</script>");
                                    return;
                                }
                            }
                        }
                        #endregion
                        #region Delete
                        string strWhereClause = "";
                        string[] strQueries = new string[DeleteTables.Length];
                        if (ViewState["DeleteTablePKList"] == null)
                        {
                            strWhereClause = " where " + Convert.ToString(ViewState["Primarykey"]).Trim() + " = " + strID[0];
                            for (int i = 0; i < DeleteTables.Length; i++)
                            {
                                strQueries[i] = "delete from " + DeleteTables[i] + strWhereClause;
                            }
                        }
                        else
                        {
                            string[] DeleteTablesPK = (string[])ViewState["DeleteTablePKList"];
                            for (int i = 0; i < DeleteTables.Length; i++)
                            {
                                strWhereClause = " where " + DeleteTablesPK[i] + " = " + strID[0];
                                strQueries[i] = "delete from " + DeleteTables[i] + strWhereClause;
                            }
                        }
                        if (DBClass.executeQueries(strQueries) > 0)
                        {
                            Session["LoadGridPageIndex"] = 0;
                            BindGrid(0);
                        }
                        strQueries = null;
                        #endregion
                    }
                    DeleteTables = null;
                    strID = null;
                }
                break;
            case "reopen":
                if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_ReopenLoad", new object[] { Convert.ToInt64(strID[0]), Convert.ToInt64(Session["UserLoginId"]) }) > 0)
                {
                    Response.Write("<script>alert('Load re-opened successfully.')</script>");
                    Session["LoadGridPageIndex"] = 0;
                    BindGrid(0);
                }
                break;
            case "cancel":
                if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_LoadCanceLation", new object[] { Convert.ToInt64(strID[0]), Convert.ToInt64(Session["UserLoginId"]) }) > 0)
                {
                    Response.Write("<script>alert('Load Cancelled successfully.')</script>");
                    Session["LoadGridPageIndex"] = 0;
                    BindGrid(0);
                }
                break;
            default: break;
        }
        strID = null;
    }
    protected void dggrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        Session["LoadGridPageIndex"] = e.NewPageIndex;
        BindGrid(e.NewPageIndex);
    }
}
public class LoadGridManagerTemplate : System.Web.UI.Page, ITemplate
{
    ListItemType templateType;
    bool blDelete = false;
    string strColName = "";
    string[] strLastColumnLinksList = null;
    string[] strLastColumnPagesList = null;
    DataTable dt = null;
    string strLoadStatus = "";
    int index = -1;
    public LoadGridManagerTemplate(ListItemType itype, string ColName, DataTable pDt, string strPLoadStatus)
    {
        templateType = itype;
        strColName = ColName;
        dt = pDt;
        Session["LoadManagerIndex"] = -1;
        strLoadStatus = strPLoadStatus;
    }
    public LoadGridManagerTemplate(ListItemType itype, string ColName, DataTable pDt, bool bDelete, string strPLoadStatus)
    {
        templateType = itype;
        strColName = ColName;
        blDelete = bDelete;
        dt = pDt; Session["LoadManagerIndex"] = -1;
        strLoadStatus = strPLoadStatus;
    }
    public LoadGridManagerTemplate(ListItemType itype, string ColName, string[] pLastColumnLinksList, string[] pLastColumnPagesList, DataTable pDt, string strPLoadStatus)
    {
        templateType = itype;
        strColName = ColName;
        strLastColumnLinksList = pLastColumnLinksList;
        strLastColumnPagesList = pLastColumnPagesList;
        dt = pDt;
        Session["LoadManagerIndex"] = -1;
        strLoadStatus = strPLoadStatus;
    }
    public void InstantiateIn(System.Web.UI.Control container)
    {
        if (Session["LoadManagerIndex"] != null)
            index = Convert.ToInt32(Session["LoadManagerIndex"]);
        Literal lc = new Literal();
        switch (templateType)
        {
            #region Header
            case ListItemType.Header:
                lc.Text = "<B>Edit</B>";
                LinkButton lb = new LinkButton();
                lb.Text = "Edit";
                lb.CommandName = "EditButton";
                container.Controls.Add(lb);
                container.Controls.Add(lc);
                lc = null; lb = null;
                break;
            #endregion
            case ListItemType.Item:
                switch (strColName.ToLower())
                {
                    case "load":
                        #region load                       
                        if (Session["Role"] != null)
                        {
                            if (string.Compare(Convert.ToString(Session["Role"]).Trim(), "Customer", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                            {
                                LinkButton lnkCust = new LinkButton();
                                lnkCust.ID = (Convert.ToString(dt.Rows[index + 1]["ID"]).Split('^'))[0];
                                lnkCust.PostBackUrl = "~/" + Convert.ToString(Session["Folder"]).Trim() + "/CustomerTrackLoad.aspx?" + (Convert.ToString(dt.Rows[index + 1]["ID"]).Split('^'))[0];
                                lnkCust.CommandName = "editload";
                                lnkCust.Text = Convert.ToString(dt.Rows[index + 1]["Load"]);
                                container.Controls.Add(lnkCust);
                                lnkCust = null;
                            }
                            else if (string.Compare(Convert.ToString(Session["Role"]).Trim(), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
                                     string.Compare(Convert.ToString(Session["Role"]).Trim(), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
                                     string.Compare(Convert.ToString(Session["Role"]).Trim(), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                            {
                                Session["ErrorLoad"] = Convert.ToString(dt.Rows[index + 1]["Load"]);
                                if (string.Compare(strLoadStatus, "Closed", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                                {
                                    container.Controls.Add(new LiteralControl("<font font-size=12; color=red>" + Convert.ToString(dt.Rows[index + 1]["Load"]) + "</font>"));
                                }
                                else
                                {
                                    LinkButton lnkload = new LinkButton();
                                    lnkload.ID = (Convert.ToString(dt.Rows[index + 1]["ID"]).Split('^'))[0];
                                    lnkload.PostBackUrl = "~/" + Convert.ToString(Session["Folder"]).Trim() + "/TrackLoad.aspx?" + (Convert.ToString(dt.Rows[index + 1]["ID"]).Split('^'))[0];
                                    lnkload.CommandName = "editload";
                                    lnkload.Text = Convert.ToString(dt.Rows[index + 1]["Load"]);
                                    if (Session["ShowStatusValueforUser"] != null)
                                    {
                                        if (Session["ShowStatusValueforUser"].ToString().Trim() == "LFD")
                                        {
                                            if (dt.Columns.Contains("LoadStatusID"))
                                            {
                                                if (Convert.ToInt32(dt.Rows[index + 1]["LoadStatusID"]) == 2)
                                                {
                                                    container.Controls.Add(new LiteralControl("<b> Assigned </b><br />"));
                                                }
                                                else if (Convert.ToInt32(dt.Rows[index + 1]["LoadStatusID"]) == 1)
                                                {
                                                    container.Controls.Add(new LiteralControl("<b> New </b><br />"));
                                                }
                                                else if (Convert.ToInt32(dt.Rows[index + 1]["LoadStatusID"]) == 15)
                                                {
                                                    container.Controls.Add(new LiteralControl("<b> Load Planner </b><br />"));
                                                }
                                                else if (Convert.ToInt32(dt.Rows[index + 1]["LoadStatusID"]) == 4)
                                                {
                                                    container.Controls.Add(new LiteralControl("<b> Loaded in Yard </b><br />"));
                                                }

                                            }
                                        }
                                    }
                                    container.Controls.Add(lnkload);
                                    lnkload = null;
                                }
                            }
                            else if (string.Compare(Convert.ToString(Session["Role"]).Trim(), "Driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                            {
                                LinkButton lnkDriver = new LinkButton();
                                lnkDriver.ID = (Convert.ToString(dt.Rows[index + 1]["ID"]).Split('^'))[0];
                                lnkDriver.PostBackUrl = "~/" + Convert.ToString(Session["Folder"]).Trim() + "/DriverTrackLoad.aspx?" + (Convert.ToString(dt.Rows[index + 1]["ID"]).Split('^'))[0];
                                lnkDriver.CommandName = "editload";
                                lnkDriver.Text = Convert.ToString(dt.Rows[index + 1]["Load"]);
                                container.Controls.Add(lnkDriver);
                                lnkDriver = null;
                            }
                            else if (string.Compare(Convert.ToString(Session["Role"]).Trim(), "Carrier", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                            {
                                LinkButton lnkCarr = new LinkButton();
                                lnkCarr.ID = (Convert.ToString(dt.Rows[index + 1]["ID"]).Split('^'))[0];
                                lnkCarr.PostBackUrl = "~/" + Convert.ToString(Session["Folder"]).Trim() + "/CarrierTrackLoad.aspx?" + (Convert.ToString(dt.Rows[index + 1]["ID"]).Split('^'))[0];
                                lnkCarr.CommandName = "editload";
                                lnkCarr.Text = Convert.ToString(dt.Rows[index + 1]["Load"]);
                                container.Controls.Add(lnkCarr);
                                lnkCarr = null;
                            }
                        }
                        //}
                        container.Controls.Add(new LiteralControl("<table width=90px><tr><td></td></tr></table>"));

                        //container.Controls.Add(new LiteralControl("<br/>"));
                        string chassis1 = string.Empty;
                        string chassis2 = string.Empty;
                        int isChassis1Available = 0;
                        int isChassis2Available = 0;
                        if (dt.Columns.Contains("Chasis1"))
                        {
                            chassis1 = Convert.ToString(dt.Rows[index + 1]["Chasis1"]).Trim();
                            if (chassis1.Length == 0)
                            {
                                chassis1 = "-NA-";
                                isChassis1Available = 0;
                            }
                            else
                            {
                                isChassis1Available = 1;
                            }
                        }
                        if (dt.Columns.Contains("Chasis2"))
                        {
                            chassis2 = Convert.ToString(dt.Rows[index + 1]["Chasis2"]).Trim();

                            if (chassis2.Length == 0)
                            {
                                chassis2 = "-NA-";
                                isChassis2Available = 0;
                            }
                            else
                            {
                                isChassis2Available = 1;
                            }
                        }
                        if (Convert.ToString(dt.Rows[index + 1]["Container1"]).Trim().Length == 0 && Convert.ToString(dt.Rows[index + 1]["Container2"]).Trim().Length == 0)
                        {
                            if (isChassis1Available > 0 && isChassis2Available > 0)
                            {
                                container.Controls.Add(new LiteralControl("<b><font color=blue>(DROP)<br/>" + chassis1 + "<br/>(PICK)<br/>" + chassis2 + "</font></b>"));
                            }
                            else if (isChassis1Available > 0)
                            {
                                container.Controls.Add(new LiteralControl("<b><font color=blue>" + chassis1 + "</font></b>"));
                            }
                            else if (isChassis2Available > 0)
                            {
                                container.Controls.Add(new LiteralControl("<b><font color=blue>" + chassis2 + "</font></b>"));
                            }
                        }
                        else if (Convert.ToString(dt.Rows[index + 1]["Container1"]).Trim().Length > 0 && Convert.ToString(dt.Rows[index + 1]["Container2"]).Trim().Length > 0)
                        {
                            if (isChassis1Available > 0 && isChassis2Available > 0)
                            {
                                container.Controls.Add(new LiteralControl("<b><font color=blue>(DROP)" + Convert.ToString(dt.Rows[index + 1]["Container1"]).Trim() + "<br/>" + chassis1 + "<br/>(PICK)" + Convert.ToString(dt.Rows[index + 1]["Container2"]).Trim() + "<br/>" + chassis2 + "</font></b>"));
                            }
                            else
                            {
                                if (isChassis1Available > 0)
                                {
                                    container.Controls.Add(new LiteralControl("<b><font color=blue>(DROP)" + Convert.ToString(dt.Rows[index + 1]["Container1"]).Trim() + "<br/>" + chassis1 + "<br/>(PICK)" + Convert.ToString(dt.Rows[index + 1]["Container2"]).Trim() + "</font></b>"));
                                }
                                else if (isChassis2Available > 0)
                                {
                                    container.Controls.Add(new LiteralControl("<b><font color=blue>(DROP)" + Convert.ToString(dt.Rows[index + 1]["Container1"]).Trim() + "<br/>(PICK)" + Convert.ToString(dt.Rows[index + 1]["Container2"]).Trim() + "<br/>" + chassis2 + "</font></b>"));
                                }
                                else
                                {
                                    container.Controls.Add(new LiteralControl("<b><font color=blue>(DROP)" + Convert.ToString(dt.Rows[index + 1]["Container1"]).Trim() + "<br/>(PICK)" + Convert.ToString(dt.Rows[index + 1]["Container2"]).Trim() + "<br/> -NA- </font></b>"));
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToString(dt.Rows[index + 1]["Container1"]).Trim().Length > 0)
                            {
                                if (chassis1.Contains("-NA-") && chassis2.Contains("-NA-"))
                                {
                                    container.Controls.Add(new LiteralControl("<b><font color=blue>" + Convert.ToString(dt.Rows[index + 1]["Container1"]).Trim() + "<br/>" + "-NA-" + "</font></b>"));
                                }
                                else if (isChassis1Available > 0 || isChassis2Available > 0)
                                {
                                    container.Controls.Add(new LiteralControl("<b><font color=blue>" + Convert.ToString(dt.Rows[index + 1]["Container1"]).Trim() + "<br/>" + chassis1 + "</font></b>")); //"<br/>" + chassis2 + 
                                }
                                else if (isChassis1Available > 0 && isChassis2Available > 0)
                                {
                                    container.Controls.Add(new LiteralControl("<b><font color=blue>" + Convert.ToString(dt.Rows[index + 1]["Container1"]).Trim() + "<br/>" + chassis1 + "</font></b>")); //"<br/>" + chassis2 + 
                                }
                                else
                                {
                                    container.Controls.Add(new LiteralControl("<b><font color=blue>" + Convert.ToString(dt.Rows[index + 1]["Container1"]).Trim() + "</font></b>"));
                                }
                            }
                            else
                            {
                                if (isChassis1Available > 0 || isChassis2Available > 0)
                                {
                                    container.Controls.Add(new LiteralControl("<b><font color=blue>" + chassis1 + "<br/>" + Convert.ToString(dt.Rows[index + 1]["Container2"]).Trim() + "<br/>" + chassis2 + "</font></b>"));
                                }
                                else if (isChassis1Available > 0 && isChassis2Available > 0)
                                {
                                    container.Controls.Add(new LiteralControl("<b><font color=blue>" + chassis1 + "<br/>" + Convert.ToString(dt.Rows[index + 1]["Container2"]).Trim() + "<br/>" + chassis2 + "</font></b>"));
                                }
                                else
                                {
                                    container.Controls.Add(new LiteralControl("<b><font color=blue>" + Convert.ToString(dt.Rows[index + 1]["Container2"]).Trim() + "</font></b>"));
                                }
                            }
                        }

                        try
                        {
                            #region TripType
                            //SLine 2016 Enhancements 
                            if (dt.Columns.Contains("TripType"))
                            {
                                string strTripType = Convert.ToString(dt.Rows[index + 1]["TripType"]);
                                if (strTripType != "")
                                {
                                    if (strTripType == "DL")
                                        container.Controls.Add(new LiteralControl("<br/><img src='../images/DL.png' alt='DL'>"));
                                    else if (strTripType == "DO")
                                        container.Controls.Add(new LiteralControl("<br/><img src='../images/DO.png' alt='DO'>"));
                                    else if (strTripType == "PU")
                                        container.Controls.Add(new LiteralControl("<br/><img src='../images/PU.png' alt='PU'>"));
                                    else if (strTripType == "ROUND")
                                        container.Controls.Add(new LiteralControl("<br/><b><font color=Black>Round</font></b>"));
                                }
                            }
                            //SLine 2016 Enhancements  
                            #endregion

                            #region Hazmath
                            if (dt.Columns.Contains("HazmatLoad"))
                            {
                                bool ishizmart = Convert.ToBoolean(dt.Rows[index + 1]["HazmatLoad"]);
                                if (dt.Columns.Contains("LoadStatusID"))
                                {
                                    if (Convert.ToInt32(dt.Rows[index + 1]["LoadStatusID"]) != 10)
                                    {
                                        if (ishizmart)
                                        {
                                            container.Controls.Add(new LiteralControl("<br /><blink><b><font color=orange> HAZMAT </font></b></blink>"));
                                        }
                                    }
                                    else if (ishizmart)
                                    {
                                        container.Controls.Add(new LiteralControl("<br /><b><font color=orange> HAZMAT </font></b>"));
                                    }
                                }

                            }
                            #endregion

                            #region BookingProblem
                            if (dt.Columns.Contains("BookingProblem"))
                            {
                                bool isBookingProblem = Convert.ToBoolean(dt.Rows[index + 1]["BookingProblem"]);
                                if (isBookingProblem)
                                {
                                    container.Controls.Add(new LiteralControl("<br /><blink><b><font color=yellow> BOOKING PROBLEM </font></b></blink>"));
                                }
                            }
                            #endregion

                            #region PutOnHold
                            if (dt.Columns.Contains("PutOnHold"))
                            {
                                bool isPutOnHold = Convert.ToBoolean(dt.Rows[index + 1]["PutOnHold"]);
                                if (isPutOnHold)
                                {
                                    container.Controls.Add(new LiteralControl("<br /><blink><b><font color=red> PUT ON HOLD </font></b></blink>"));
                                }
                            }
                            #endregion

                            #region HotShipment
                            //SLine 2016 Enhancements ---- HotShipment style Added this code block
                            if (dt.Columns.Contains("HotShipment"))
                            {
                                bool iHotShipment = Convert.ToBoolean(dt.Rows[index + 1]["HotShipment"]);
                                if (dt.Columns.Contains("LoadStatusID"))
                                {
                                    //Load Status = 10 is "Terminated" hence showing the Status to all except this State
                                    if (Convert.ToInt32(dt.Rows[index + 1]["LoadStatusID"]) != 10)
                                    {
                                        if (iHotShipment)
                                        {
                                            container.Controls.Add(new LiteralControl("<br /><blink><b><font color=red> Hot Shipment </font></b></blink>"));
                                        }
                                    }
                                    else if (iHotShipment)
                                    {
                                        container.Controls.Add(new LiteralControl("<br /><blink><b><font color=red> Hot Shipment </font></b></blink>"));
                                    }
                                }
                            }
                            #endregion

                            #region Type
                            if (dt.Columns.Contains("Type"))
                            {
                                string iRail = Convert.ToString(dt.Rows[index + 1]["Type"]);
                                //if (dt.Columns.Contains("LoadStatusID"))
                                //{
                                if (iRail.StartsWith("Rail-"))
                                    container.Controls.Add(new LiteralControl("<br /><blink><b><font color=red>RAIL</font></b></blink>"));

                                //}
                            }
                            #endregion
                            //SLine 2016 Enhancements                           
                        }
                        catch (Exception ex)
                        {
                        }

                        index = index + 1;
                        Session["LoadManagerIndex"] = index;
                        break;
                    #endregion
                    case "type":
                        #region type
                        string strTemp = Convert.ToString(dt.Rows[index]["Type"]).Trim();
                        if (strTemp.Length > 4)
                        {
                            int intLength = strTemp.Length;
                            if (strTemp.StartsWith("Rail-"))
                            {
                                strTemp = strTemp.Substring(0, intLength) + ".";
                                container.Controls.Add(new LiteralControl("<font font-size=12; color=red>" + strTemp + "</font>"));
                            }
                            else
                            {
                                strTemp = strTemp.Substring(0, intLength) + ".";
                                container.Controls.Add(new LiteralControl("<font font-size=12; color=black>" + strTemp + "</font>"));
                            }
                        }
                        //container.Controls.Add(new LiteralControl("<font font-size=12; color=black>" + strTemp + "</font>"));
                        if (blDelete)
                        {
                            container.Controls.Add(new LiteralControl("<br/><br/>"));
                            //LinkButton lnkDelete = new LinkButton();
                            //lnkDelete.PostBackUrl = "#";
                            //lnkDelete.CommandName = "delete";
                            //lnkDelete.Text = "Delete";
                            //container.Controls.Add(lnkDelete);  
                            ImageButton imgDel = new ImageButton();
                            imgDel.ID = strTemp + (Convert.ToString(dt.Rows[index]["ID"]).Split('^'))[0];
                            imgDel.ImageUrl = "~/images/delete_icon.gif";
                            imgDel.CommandName = "Delete";
                            imgDel.CausesValidation = false;
                            imgDel.Width = 13; imgDel.Height = 12;
                            imgDel.OnClientClick = "return confirm('Are you sure you want to delete this record?');";
                            container.Controls.Add(imgDel);
                            imgDel = null;
                        }
                        break;
                    #endregion
                    case "customer":
                        #region Customer
                        LinkButton lnkCustomer = new LinkButton();
                        lnkCustomer.ID = "Customer" + (Convert.ToString(dt.Rows[index]["ID"]).Split('^'))[1];
                        lnkCustomer.PostBackUrl = "~/" + Convert.ToString(Session["Folder"]).Trim() + "/NewCustomer.aspx?" + (Convert.ToString(dt.Rows[index]["ID"]).Split('^'))[1];
                        //lnkCustomer.CommandName = "editcustomer";
                        lnkCustomer.Text = Convert.ToString(dt.Rows[index]["CustomerName"]);
                        container.Controls.Add(lnkCustomer);
                        container.Controls.Add(new LiteralControl("<br/><font font-size=12; color=black>" + Convert.ToString(dt.Rows[index]["CustomerAddress"]).Trim() + "</font>"));
                        lnkCustomer = null;
                        break;
                    #endregion
                    case "origin":
                        #region origin
                        FormatMultipleRowColumns(Convert.ToString(dt.Rows[index]["Origin"]), "black", ref container);
                        break;
                    #endregion
                    case "pickup":
                        #region pickup 
                        FormatMultipleRowColumns(Convert.ToString(dt.Rows[index]["PickUp"]), "black", ref container);
                        break;
                    #endregion
                    case "destination":
                        #region destination
                        FormatMultipleRowColumns(Convert.ToString(dt.Rows[index]["Destination"]), "black", ref container);
                        break;
                    #endregion
                    case "origin/from":
                        #region origin/from
                        FormatMultipleRowColumns(Convert.ToString(dt.Rows[index]["Origin/From"]), "black", ref container);
                        break;
                    #endregion
                    case "destination/to":
                        #region destination/to
                        FormatMultipleRowColumns(Convert.ToString(dt.Rows[index]["Destination/To"]), "black", ref container);
                        break;
                    #endregion
                    case "delivery appt.":
                        #region delivery appt.
                        if (string.Compare(strLoadStatus, "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
                            string.Compare(strLoadStatus, "Assigned", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        {
                            FormatMultipleRowColumnDatesWithImages(Convert.ToString(dt.Rows[index]["Delivery Appt."]).Trim(), "green", strLoadStatus, ref container);
                        }
                        else if (string.Compare(strLoadStatus, "Transit", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        {
                            //if (string.Compare(Convert.ToString(dt.Rows[index]["Status"]).Trim(), "Assigned", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                            //{
                            //    FormatMultipleRowColumnDatesWithImages(Convert.ToString(dt.Rows[index]["Delivery Appt."]).Trim(), "green", "Assigned", ref container);
                            //}
                            if (Convert.ToString(dt.Rows[index]["Delivery Appt."]).Trim().Length > 0)
                            {
                                if (Convert.ToDateTime(dt.Rows[index]["Delivery Appt."].ToString().Replace("^", "").Trim()).Date == DateTime.Now.Date)
                                {
                                    FormatSingleRowColumnsWithImages(Convert.ToString(dt.Rows[index]["Delivery Appt."]), "../images/bulb-pink-icon.gif", ref container);
                                }
                                else if (Convert.ToDateTime(dt.Rows[index]["Delivery Appt."].ToString().Replace("^", "").Trim()).Date == DateTime.Now.Date.AddDays(1))
                                {
                                    FormatSingleRowColumnsWithImages(Convert.ToString(dt.Rows[index]["Delivery Appt."]), "../images/bulb-yellow-icon.gif", ref container);
                                }
                                else
                                {
                                    FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Delivery Appt."]), "green", ref container);
                                }
                            }
                        }
                        else
                        {
                            FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Delivery Appt."]), "green", ref container);
                        }

                        #region Old Code
                        //write conditions depend on loadstatus here     
                        //if (string.Compare(strLoadStatus, "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
                        //    string.Compare(strLoadStatus, "Assigned", true, System.Globalization.CultureInfo.CurrentCulture) == 0 )
                        //{
                        //     FormatMultipleRowColumnDatesWithImages(Convert.ToString(dt.Rows[index]["Delivery Appt."]).Trim(), "green",strLoadStatus, ref container);
                        //}
                        //else if (string.Compare(strLoadStatus, "Transit", true, System.Globalization.CultureInfo.CurrentCulture) == 0 )
                        //{
                        //    if (string.Compare(Convert.ToString(dt.Rows[index]["Status"]).Trim(), "Assigned", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        //    {
                        //        FormatMultipleRowColumnDatesWithImages(Convert.ToString(dt.Rows[index]["Delivery Appt."]).Trim(), "green", "Assigned", ref container);
                        //    }
                        //    else
                        //    {
                        //        FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Delivery Appt."]), "green", ref container);
                        //    }
                        //}
                        //else
                        //{
                        //     FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Delivery Appt."]), "green", ref container);
                        // }
                        #endregion

                        //if (Convert.ToString(dt.Rows[index]["Delivery Appt."]).Trim().Length > 0)
                        //{
                        //    if (Convert.ToDateTime(dt.Rows[index]["Delivery Appt."].ToString().Trim()).Date == DateTime.Now.Date)
                        //    {
                        //        if (string.Compare(strLoadStatus, "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        //            FormatMultipleRowColumnsWithImages(Convert.ToString(dt.Rows[index]["Delivery Appt."]), "green", "images/bulb-pink-icon.gif", ref container);
                        //        else
                        //            FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Delivery Appt."]), "green", ref container);
                        //    }
                        //    else if (Convert.ToDateTime(dt.Rows[index]["Delivery Appt."].ToString().Trim()).Date == DateTime.Now.Date.AddDays(1))
                        //    {
                        //        if (string.Compare(strLoadStatus, "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        //            FormatMultipleRowColumnsWithImages(Convert.ToString(dt.Rows[index]["Delivery Appt."]), "green", "images/bulb-icon.gif", ref container);
                        //        else
                        //            FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Delivery Appt."]), "green", ref container);
                        //    }
                        //    else
                        //    {
                        //        FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Delivery Appt."]), "green", ref container);
                        //    }
                        //}
                        break;
                    #endregion
                    case "delivery appt date":
                        #region delivery appt date.
                        if (string.Compare(strLoadStatus, "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
                           string.Compare(strLoadStatus, "Assigned", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        {
                            FormatMultipleRowColumnDatesWithImages(Convert.ToString(dt.Rows[index]["Delivery Appt."]).Trim(), "green", strLoadStatus, ref container, Convert.ToString(dt.Rows[index]["Delivery Appt Date"]).Trim());
                        }
                        else if (string.Compare(strLoadStatus, "Transit", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        {
                            int k = 0;
                            bool isBlink = false;
                            string[] strDates = Convert.ToString(dt.Rows[index]["Delivery Appt."]).Replace("^^^^", "^").Split('^');
                            for (int i = 0; i < strDates.Length; i++)
                            {
                                if (Convert.ToString(strDates[i]).Trim().Length == 0)
                                    continue;
                                if (Convert.ToDateTime(strDates[i]).Date <= DateTime.Now.Date || Convert.ToDateTime(strDates[i]).Date == DateTime.Now.Date.AddDays(1))
                                {
                                    if (!isBlink)
                                        if (Convert.ToDateTime(strDates[i]) < DateTime.Now && Convert.ToString(dt.Rows[index]["Status"]) != "Delivered" && Convert.ToString(dt.Rows[index]["Status"]) != "Reached Destination")
                                        {
                                            for (int j = i; j < strDates.Length; j++)
                                            {
                                                if (!isBlink && !string.IsNullOrEmpty(strDates[j].Trim()))
                                                {
                                                    string[] array = ((Convert.ToString(dt.Rows[index]["Delivery Appt Date"])).Replace("^^^^", "^").Split('^')[j].Trim().Replace("<br/>", " ")).Split(' ');
                                                    string time = array[array.Length - 1].Trim();
                                                    time = time.Insert(time.Length - 2, " ");
                                                    DateTime tempDT = Convert.ToDateTime(Convert.ToDateTime(strDates[j]).ToString().Split(' ')[0] + " " + time);
                                                    if ((tempDT.AddMinutes(10)) < DateTime.Now)
                                                        isBlink = true;
                                                }
                                            }
                                        }
                                        else if (Convert.ToString(dt.Rows[index]["Status"]) == "Reached Destination")
                                        {
                                            string tempDate = SqlHelper.ExecuteScalar(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadStatusUpdateDatetime", new object[] { Int64.Parse(Convert.ToString(dt.Rows[index]["Load"])), 14 }).ToString();
                                            if (((Convert.ToDateTime(tempDate)).AddHours(2)) < DateTime.Now)
                                                isBlink = true;

                                        }

                                    k = i;
                                    break;
                                }
                            }
                            if (k > 0)
                            {
                                if (Convert.ToString(strDates[k]).Replace("^", "").Trim().Length > 0)
                                {
                                    if (Convert.ToDateTime(strDates[k].ToString().Replace("^", "").Trim()).Date <= DateTime.Now.Date)
                                    {
                                        if (string.Compare(Convert.ToString(dt.Rows[index]["Status"]).Trim(), "Assigned", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                                            FormatMultipleRowColumnDatesWithImages(Convert.ToString(dt.Rows[index]["Delivery Appt."]), "../images/bulb-pink-icon.gif", ref container, Convert.ToString(dt.Rows[index]["Delivery Appt Date"]).Trim(), isBlink);
                                        else
                                            FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Delivery Appt."]), "green", ref container, Convert.ToString(dt.Rows[index]["Delivery Appt Date"]).Trim(), isBlink);
                                    }
                                    else if (Convert.ToDateTime(strDates[k].ToString().Replace("^", "").Trim()).Date == DateTime.Now.Date.AddDays(1))
                                    {
                                        if (string.Compare(Convert.ToString(dt.Rows[index]["Status"]).Trim(), "Assigned", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                                            FormatMultipleRowColumnDatesWithImages(Convert.ToString(dt.Rows[index]["Delivery Appt."]), "../images/bulb-yellow-icon.gif", ref container, Convert.ToString(dt.Rows[index]["Delivery Appt Date"]).Trim(), isBlink);
                                        else
                                            FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Delivery Appt."]), "green", ref container, Convert.ToString(dt.Rows[index]["Delivery Appt Date"]).Trim(), isBlink);
                                    }
                                    else
                                    {
                                        FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Delivery Appt."]), "green", ref container, Convert.ToString(dt.Rows[index]["Delivery Appt Date"]).Trim(), isBlink);
                                    }
                                }
                            }
                            else
                            {
                                FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Delivery Appt."]), "green", ref container, Convert.ToString(dt.Rows[index]["Delivery Appt Date"]).Trim(), isBlink);
                            }
                            strDates = null;
                        }
                        else
                        {
                            FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Delivery Appt."]), "green", ref container, Convert.ToString(dt.Rows[index]["Delivery Appt Date"]).Trim(), false);
                        }
                        break;
                    #endregion
                    case "appt. date":
                        #region appt. date
                        //FormatMultipleRowColumnDatesWithImages(Convert.ToString(dt.Rows[index]["Appt. Date"]).Trim(), "green", ref container);

                        FormatMultipleRowColumns(Convert.ToString(dt.Rows[index]["Appt. Date"]).Trim(), "black", ref container);

                        //write conditions depend on loadstatus here                        
                        //if (Convert.ToString(dt.Rows[index]["Appt. Date"]).Trim().Length > 0)
                        //{
                        //    if (Convert.ToDateTime(dt.Rows[index]["Appt. Date"].ToString().Trim()).Date == DateTime.Now.Date)
                        //    {
                        //        if(string.Compare(strLoadStatus,"New",true,System.Globalization.CultureInfo.CurrentCulture)==0)
                        //            FormatMultipleRowColumnsWithImages(Convert.ToString(dt.Rows[index]["Appt. Date"]), "green", "images/bulb-pink-icon.gif", ref container);
                        //        else
                        //            FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Appt. Date"]), "green", ref container);
                        //    }
                        //    else if (Convert.ToDateTime(dt.Rows[index]["Appt. Date"].ToString().Trim()).Date == DateTime.Now.Date.AddDays(1))
                        //    {
                        //        if (string.Compare(strLoadStatus, "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        //            FormatMultipleRowColumnsWithImages(Convert.ToString(dt.Rows[index]["Appt. Date"]), "green", "images/bulb-icon.gif", ref container);
                        //        else
                        //            FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Appt. Date"]), "green", ref container);
                        //    }
                        //    else
                        //    {
                        //        FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Appt. Date"]), "green", ref container);
                        //    }
                        //}
                        break;
                    #endregion
                    case "last free date":
                        #region last free date
                        if (Convert.ToString(dt.Rows[index]["Last Free Date"]).Replace("^", "").Trim().Length > 0)
                        {
                            #region Old Code for Displaying the bulds based on dates
                            if (Convert.ToDateTime(dt.Rows[index]["Last Free Date"].ToString().Trim()).Date <= DateTime.Now.Date)
                            {
                                if (string.Compare(strLoadStatus, "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                                    FormatSingleRowColumnsWithImages(Convert.ToString(dt.Rows[index]["Last Free Date"]), "../images/bulb-pink-icon.gif", ref container);
                                else if (string.Compare(strLoadStatus, "Assigned", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                                    FormatSingleRowColumnsWithImages(Convert.ToString(dt.Rows[index]["Last Free Date"]), "../images/bulb-yellow-icon.gif", ref container);
                                else if (string.Compare(strLoadStatus, "Transit", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                                {
                                    if (Convert.ToDateTime(dt.Rows[index]["Last Free Date"].ToString().Trim()).Date <= DateTime.Now.Date)
                                    {
                                        if (string.Compare(Convert.ToString(dt.Rows[index]["Status"]).Trim(), "Assigned", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                                            FormatSingleRowColumnsWithImages(Convert.ToString(dt.Rows[index]["Last Free Date"]), "../images/bulb-pink-icon.gif", ref container);
                                        else
                                            FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Last Free Date"]), "red", ref container);
                                    }
                                    //if (string.Compare(Convert.ToString(dt.Rows[index]["Status"]).Trim(), "Assigned", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                                    //{
                                    //    FormatSingleRowColumnsWithImages(Convert.ToString(dt.Rows[index]["Last Free Date"]), "../images/bulb-yellow-icon.gif", ref container);
                                    //}
                                    else
                                    {
                                        FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Last Free Date"]), "red", ref container);
                                    }
                                }
                                else
                                    FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Last Free Date"]), "red", ref container);
                            }
                            else if (Convert.ToDateTime(dt.Rows[index]["Last Free Date"].ToString().Trim()).Date == DateTime.Now.Date.AddDays(1))
                            {
                                if (string.Compare(strLoadStatus, "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                                    FormatSingleRowColumnsWithImages(Convert.ToString(dt.Rows[index]["Last Free Date"]), "../images/bulb-icon.gif", ref container);
                                else if (string.Compare(strLoadStatus, "Assigned", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                                    FormatSingleRowColumnsWithImages(Convert.ToString(dt.Rows[index]["Last Free Date"]), "../images/bulb-yellow-icon.gif", ref container);
                                else if (string.Compare(strLoadStatus, "Transit", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                                {
                                    //if (string.Compare(Convert.ToString(dt.Rows[index]["Status"]).Trim(), "Assigned", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                                    //{
                                    //    FormatSingleRowColumnsWithImages(Convert.ToString(dt.Rows[index]["Last Free Date"]), "../images/bulb-yellow-icon.gif", ref container);
                                    //}
                                    if (Convert.ToDateTime(dt.Rows[index]["Last Free Date"].ToString().Trim()).Date == DateTime.Now.Date.AddDays(1))
                                    {
                                        if (string.Compare(Convert.ToString(dt.Rows[index]["Status"]).Trim(), "Assigned", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                                            FormatSingleRowColumnsWithImages(Convert.ToString(dt.Rows[index]["Last Free Date"]), "../images/bulb-yellow-icon.gif", ref container);
                                        else
                                            FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Last Free Date"]), "red", ref container);
                                    }
                                    else
                                    {
                                        FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Last Free Date"]), "red", ref container);
                                    }
                                }
                                else
                                    FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Last Free Date"]), "red", ref container);
                            }
                            else
                            {
                                container.Controls.Add(new LiteralControl("<b><font color=red>" + Convert.ToString(dt.Rows[index]["Last Free Date"]).Trim() + "</font></b>"));
                            }
                            #endregion
                        }
                        break;
                    #endregion
                    case "lastcolumn":
                        #region Last Column
                        //container.Controls.Add(new LiteralControl("<table border=0 cellpadding=0 cellspacing=0>"));
                        for (int i = 0; i < strLastColumnLinksList.Length; i++)
                        {
                            if (strLastColumnLinksList[i].ToString() != "Update Load Status")
                            {
                                //container.Controls.Add(new LiteralControl("<tr><td align=left>"));
                                LinkButton lnkTemp = new LinkButton();
                                lnkTemp.ID = strLastColumnLinksList[i] + (Convert.ToString(dt.Rows[index]["ID"]).Split('^'))[0];
                                if (string.Compare(strLoadStatus, "Closed", true, System.Globalization.CultureInfo.CurrentCulture) != 0)
                                {
                                    lnkTemp.PostBackUrl = strLastColumnPagesList[i] + "?" + (Convert.ToString(dt.Rows[index]["ID"]).Split('^'))[0];
                                    if (strLastColumnLinksList[i].ToString() == "Cancel" && strLoadStatus == "New")
                                    {
                                        lnkTemp.OnClientClick = "return confirm('Are you sure you want to Cancel this Load?')";
                                        lnkTemp.CommandName = "Cancel";
                                        lnkTemp.CausesValidation = false;
                                    }
                                }
                                else
                                {
                                    lnkTemp.OnClientClick = "return confirm('Are you sure you want to re-open this Load?')";
                                    lnkTemp.CommandName = "Reopen";
                                    lnkTemp.CausesValidation = false;
                                }
                                lnkTemp.Text = strLastColumnLinksList[i];
                                container.Controls.Add(lnkTemp);
                                lnkTemp = null;
                                container.Controls.Add(new LiteralControl("<br/><br/>"));
                            }
                            else
                            {
                                container.Controls.Add(new LiteralControl("<A href=UpdateLoadStatus.aspx?" + (Convert.ToString(dt.Rows[index]["ID"]).Split('^'))[0] + " target=_self>" + strLastColumnLinksList[i].ToString() + "</A>"));
                            }
                            //container.Controls.Add(new LiteralControl("</td></tr>"));
                        }
                        //container.Controls.Add(new LiteralControl("</table>"));
                        break;
                    #endregion
                    case "booking#":
                        #region booking#
                        container.Controls.Add(new LiteralControl("<font font-size=12; color=black>" + Convert.ToString(dt.Rows[index]["Booking#"]) + "</font>"));
                        break;
                    #endregion
                    case "pickup#":
                        #region pickup#
                        container.Controls.Add(new LiteralControl("<font font-size=12; color=black>" + Convert.ToString(dt.Rows[index]["pickup#"]) + "</font>"));
                        break;
                    #endregion
                    case "driver/carrier":
                        #region driver/carrier
                        FormatMultipleRowColumns(Convert.ToString(dt.Rows[index]["Driver/Carrier"]), "black", ref container);
                        break;
                    #endregion
                    case "from":
                        #region from
                        FormatMultipleRowColumns(Convert.ToString(dt.Rows[index]["From"]), "black", ref container);
                        break;
                    #endregion
                    case "to":
                        #region to
                        FormatMultipleRowColumns(Convert.ToString(dt.Rows[index]["To"]), "black", ref container);
                        break;
                    #endregion
                    case "last location":
                        #region last location
                        FormatMultipleRowColumns(Convert.ToString(dt.Rows[index]["Last Location"]), "black", ref container);
                        break;
                    #endregion
                    case "date & time":
                        #region date & time
                        FormatMultipleRowColumns(Convert.ToString(dt.Rows[index]["Date & Time"]), "black", ref container);
                        break;
                    #endregion
                    case "comments":
                        #region comments
                        FormatMultipleRowColumns(Convert.ToString(dt.Rows[index]["Comments"]), "black", ref container);
                        break;
                    #endregion
                    case "delivered date":
                        #region delivered date
                        FormatMultipleRowColumns(Convert.ToString(dt.Rows[index]["Delivered Date"]), "black", ref container);
                        break;
                    #endregion
                    case "terminated date":
                        #region terminated date
                        FormatMultipleRowColumns(Convert.ToString(dt.Rows[index]["Terminated Date"]), "black", ref container);
                        break;
                    #endregion
                    case "days in transit":
                        #region days in transit
                        container.Controls.Add(new LiteralControl("<font font-size=12; color=black>" + Convert.ToString(dt.Rows[index]["Days In Transit"]).Trim() + "</font>"));
                        break;
                    #endregion
                    case "days in yard":
                        #region days in transit
                        container.Controls.Add(new LiteralControl("<font font-size=12; color=black>" + Convert.ToString(dt.Rows[index]["Days In Yard"]).Trim() + "</font>"));
                        break;
                    #endregion
                    case "st. turn":
                        #region days in transit
                        container.Controls.Add(new LiteralControl("<font font-size=12; color=black>" + Convert.ToString(dt.Rows[index]["St. Turn"]).Trim() + "</font>"));
                        break;
                    #endregion
                    case "status":
                        #region status   
                        if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[index]["Status"]).Trim()) && Convert.ToString(dt.Rows[index]["Status"]).ToLower() == "empty in yard")
                        {
                            container.Controls.Add(new LiteralControl("<b><font font-size=12; color=black>" + Convert.ToString(dt.Rows[index]["Status"]) + "</font><b><br/><img src=../images/bulb-pink-icon.gif width=28 height=26 align=middle/>"));
                        }
                        else
                        {
                            container.Controls.Add(new LiteralControl("<font font-size=12; color=black>" + Convert.ToString(dt.Rows[index]["Status"]) + "</font>"));
                        }

                        break;
                    #endregion
                    case "container size/location":
                        #region container size/Location
                        FormatMultipleRowColumns(Convert.ToString(dt.Rows[index]["Container Size/Location"]), "black", ref container);
                        break;
                    #endregion
                    case "no. of days":
                        #region Drop in Warehouse No of days
                        container.Controls.Add(new LiteralControl("<font font-size=12; color=black>" + Convert.ToString(dt.Rows[index]["No. of Days"]) + "</font>"));
                        #endregion
                        break;
                    case "driver reached @":
                        #region Drop in Warehouse date
                        container.Controls.Add(new LiteralControl("<font font-size=12; color=black>" + Convert.ToString(dt.Rows[index]["Driver Reached @"]) + "</font>"));
                        break;
                    #endregion
                    case "empty return last free date":
                        #region Empty Return Last Free Date
                        DateTime datenewvalue = Convert.ToDateTime("1/1/1900 12:00:00 AM");
                        if (string.IsNullOrEmpty(dt.Rows[index]["Empty Return Last Free Date"].ToString().Trim()))
                        {
                            FormatMultipleRowColumnsWithBold("", "green", ref container, "", false);
                        }
                        else if (Convert.ToDateTime(dt.Rows[index]["Empty Return Last Free Date"].ToString().Trim()) == datenewvalue)
                        {
                            FormatMultipleRowColumnsWithBold("", "green", ref container, "", false);
                        }
                        else if (Convert.ToDateTime(dt.Rows[index]["Empty Return Last Free Date"].ToString().Trim()).Date <= DateTime.Now.Date)
                        {
                            FormatSingleRowColumnsWithHandImages(Convert.ToString(dt.Rows[index]["Empty Return Last Free Date"]), "../images/hand-icon.gif", ref container);
                        }
                        else if (Convert.ToDateTime(dt.Rows[index]["Empty Return Last Free Date"].ToString().Trim()).Date == DateTime.Now.Date.AddDays(1))
                        {
                            FormatSingleRowColumnsWithHandImages(Convert.ToString(dt.Rows[index]["Empty Return Last Free Date"]), "../images/hand-icon.gif", ref container);
                        }
                        else
                        {
                            FormatMultipleRowColumnsWithBold(Convert.ToString(dt.Rows[index]["Empty Return Last Free Date"]), "green", ref container, Convert.ToString(dt.Rows[index]["Empty Return Last Free Date"]).Trim(), false);
                        }
                        break;
                    #endregion
                    case "container#":
                        #region Container#
                        container.Controls.Add(new LiteralControl("<font font-size=12; color=black>" + Convert.ToString(dt.Rows[index]["Container#"]) + "</font>"));
                        break;
                    #endregion
                    default: break;
                }
                break;
            #region EditItem and Footer
            case ListItemType.EditItem:
                TextBox tb = new TextBox();
                tb.Text = "";
                container.Controls.Add(tb);
                break;
            case ListItemType.Footer:
                lc.Text = "<I>Edit</I>";
                container.Controls.Add(lc);
                break;
                #endregion
        }
    }
    public void FormatMultipleRowColumns(string strColValue, string strColor, ref System.Web.UI.Control container)
    {
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0)
        {
            string strFormat = "<table border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;border-color:Black;>";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (i == 0)
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;>" + strMuls[i].Trim() + "</td></tr>";
                else
                {
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;>" + strMuls[i].Trim() + "</td></tr>";
                }
            }
            strFormat += "</table>";
            container.Controls.Add(new LiteralControl(strFormat));
        }
        strMuls = null;
    }
    public void FormatMultipleRowColumnsWithBold(string strColValue, string strColor, ref System.Web.UI.Control container)
    {
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0)
        {
            string strFormat = "<table border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;border-color:Black;>";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (i == 0)
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + strMuls[i].Trim() + "</b></td></tr>";
                else
                {
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + strMuls[i].Trim() + "</b></td></tr>";
                }
            }
            strFormat += "</table>";
            container.Controls.Add(new LiteralControl(strFormat));
        }
        strMuls = null;
    }
    public void FormatMultipleRowColumnsWithBold(string strColValue, string strColor, ref System.Web.UI.Control container, string Column, bool isBlink)
    {
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        string[] strDates = Column.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0 && strDates.Length > 0)
        {
            string strFormat = "<table border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;border-color:Black;>";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (i == 0)
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + BlinkTag(isBlink, true) + strDates[i] + BlinkTag(isBlink, false) + "</b></td></tr>";
                else
                {
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + BlinkTag(isBlink, true) + strDates[i] + BlinkTag(isBlink, false) + "</b></td></tr>";
                }
            }
            strFormat += "</table>";
            container.Controls.Add(new LiteralControl(strFormat));
        }
        strMuls = null;
    }
    public void FormatMultipleRowColumnsWithImages(string strColValue, string strColor, string strImagePath, ref System.Web.UI.Control container)
    {
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0)
        {
            string strFormat = "<table border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;>";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (i == 0)
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + strMuls[i].Trim() + "<b/><br/><img src=" + strImagePath + " width=28 height=26 align=middle/></td></tr>";
                else
                {
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + strMuls[i].Trim() + "<b/><br/><img src=" + strImagePath + " width=28 height=26 align=middle/></td></tr>";
                }
            }
            strFormat += "</table>";
            container.Controls.Add(new LiteralControl(strFormat));
        }
        strMuls = null;
    }
    public void FormatMultipleRowColumnDatesWithImages(string strColValue, string strColor, string strStatus, ref System.Web.UI.Control container)
    {
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0)
        {
            string strFormat = "<table border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;>";
            string strImagePath = "";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (Convert.ToString(strMuls[i]).Trim().Length > 0)
                {
                    if (string.Compare(strStatus, "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                    {
                        if (Convert.ToDateTime(strMuls[i].Trim()).Date <= DateTime.Now.Date)
                        {
                            strImagePath = "../images/bulb-pink-icon.gif";
                        }
                        else if (Convert.ToDateTime(strMuls[i].ToString().Trim()).Date == DateTime.Now.Date.AddDays(1))
                        {
                            strImagePath = "../images/bulb-icon.gif";
                        }
                        else
                        {
                            strImagePath = "";
                        }
                    }
                    else if (string.Compare(strStatus, "Assigned", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                    {
                        strImagePath = "../images/bulb-yellow-icon.gif";
                    }
                    else
                    {
                        strImagePath = "";
                    }
                    if (strImagePath.Trim().Length > 0)
                    {
                        if (i == 0)
                            strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + strMuls[i].Trim() + "<b/><br/><img src=" + strImagePath + " width=28 height=26 align=middle/></td></tr>";
                        else
                        {
                            strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                            strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + strMuls[i].Trim() + "<b/><br/><img src=" + strImagePath + " width=28 height=26 align=middle/></td></tr>";
                        }
                    }
                    else
                    {
                        if (i == 0)
                            strFormat += "<tr><td  width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + strMuls[i].Trim() + "<b/></td></tr>";
                        else
                        {
                            strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                            strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + strMuls[i].Trim() + "<b/></td></tr>";
                        }
                    }
                }
                else
                {
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><br/></td></tr>";
                }
            }
            strFormat += "</table>";
            container.Controls.Add(new LiteralControl(strFormat));
        }
        strMuls = null;
    }
    public void FormatMultipleRowColumnDatesWithImages(string strColValue, string strColor, string strStatus, ref System.Web.UI.Control container, string Column)
    {
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        string[] strDates = Column.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0 && strDates.Length > 0)
        {
            string strFormat = "<table border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;>";
            string strImagePath = "";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (Convert.ToString(strMuls[i]).Trim().Length > 0)
                {
                    if (string.Compare(strStatus, "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                    {
                        if (Convert.ToDateTime(strMuls[i].Trim()).Date <= DateTime.Now.Date)
                        {
                            strImagePath = "../images/bulb-pink-icon.gif";
                        }
                        else if (Convert.ToDateTime(strMuls[i].ToString().Trim()).Date == DateTime.Now.Date.AddDays(1))
                        {
                            strImagePath = "../images/bulb-yellow-icon.gif";
                        }
                        else
                        {
                            strImagePath = "";
                        }
                    }
                    else if (string.Compare(strStatus, "Assigned", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                    {
                        if (Convert.ToDateTime(strMuls[i].Trim()).Date <= DateTime.Now.Date)
                        {
                            strImagePath = "../images/bulb-pink-icon.gif";
                        }
                        else if (Convert.ToDateTime(strMuls[i].ToString().Trim()).Date == DateTime.Now.Date.AddDays(1))
                        {
                            strImagePath = "../images/bulb-yellow-icon.gif";
                        }
                        else
                        {
                            strImagePath = "";
                        }
                    }
                    else
                    {
                        strImagePath = "";
                    }
                    if (strImagePath.Trim().Length > 0)
                    {
                        if (i == 0)
                            strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + strDates[i] + "<b/><br/><img src=" + strImagePath + " width=28 height=26 align=middle/></td></tr>";
                        else
                        {
                            strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                            strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + strDates[i] + "<b/><br/><img src=" + strImagePath + " width=28 height=26 align=middle/></td></tr>";
                        }
                    }
                    else
                    {
                        if (i == 0)
                            strFormat += "<tr><td  width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + strDates[i] + "<b/></td></tr>";
                        else
                        {
                            strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                            strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + strDates[i] + "<b/></td></tr>";
                        }
                    }
                }
                else
                {
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><br/></td></tr>";
                }
            }
            strFormat += "</table>";
            container.Controls.Add(new LiteralControl(strFormat));
        }
        strMuls = null;
    }
    private string BlinkTag(bool isBlink, bool isStartTag)
    {
        return isBlink ? (isStartTag ? "<blink>" : "</blink>") : string.Empty;
    }
    public void FormatMultipleRowColumnDatesWithImages(string strColValue, string strImagePath, ref System.Web.UI.Control container, string Column, bool isBlink)
    {
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        string[] strDates = Column.Trim().Replace("^^^^", "^").Split('^');
        string strColor = "Red";
        if (strMuls.Length > 0 && strDates.Length > 0)
        {
            string strFormat = "<table border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;>";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (Convert.ToString(strMuls[i]).Trim().Length > 0)
                {
                    if (strImagePath.Trim().Length > 0)
                    {
                        if (i == 0)
                            strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + BlinkTag(isBlink, true) + strDates[i] + BlinkTag(isBlink, false) + "<b/><br/><img src=" + strImagePath + " width=28 height=26 align=middle/></td></tr>";
                        else
                        {
                            strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                            strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + BlinkTag(isBlink, true) + strDates[i] + BlinkTag(isBlink, false) + "<b/><br/><img src=" + strImagePath + " width=28 height=26 align=middle/></td></tr>";
                        }
                    }
                    else
                    {
                        if (i == 0)
                            strFormat += "<tr><td  width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + BlinkTag(isBlink, true) + strDates[i] + BlinkTag(isBlink, false) + "<b/></td></tr>";
                        else
                        {
                            strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                            strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + BlinkTag(isBlink, true) + strDates[i] + BlinkTag(isBlink, false) + "<b/></td></tr>";
                        }
                    }
                }
                else
                {
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><br/></td></tr>";
                }
            }
            strFormat += "</table>";
            container.Controls.Add(new LiteralControl(strFormat));
        }
        strMuls = null;
    }
    public void FormatSingleRowColumnsWithImages(string strColValue, string strImagePath, ref System.Web.UI.Control container)
    {
        container.Controls.Add(new LiteralControl("<b><font color=red>" + strColValue.Trim() + "</font><b><br/><img src=" + strImagePath + " width=28 height=26 align=middle/>"));
    }
    public void FormatSingleRowColumnsWithImages(string strColValue, string strImagePath, ref System.Web.UI.Control container, string Column)
    {
        container.Controls.Add(new LiteralControl("<b><font color=red>" + Column.Trim() + "</font><b><br/><img src=" + strImagePath + " width=28 height=26 align=middle/>"));
    }
    public void FormatSingleRowColumnsWithHandImages(string strColValue, string strImagePath, ref System.Web.UI.Control container)
    {
        container.Controls.Add(new LiteralControl("<img src=" + strImagePath + " /><b><font color=red >" + strColValue.Trim() + "</font><b>"));
    }

}