<%@ Page AutoEventWireup="true" CodeFile="UnAssignedDrivers.aspx.cs" Inherits="UnAssignedDrivers"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="../UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>                
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="height: 5px">
                            <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <uc2:GridBar ID="GridBar1" runat="server" HeaderText="UnAssigned Drivers" />
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="3" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:GridView ID="gridDrivers" runat="server" CssClass="Grid" PageSize="5" Width="100%" DataSourceID="SqlDataSource1">
                                <PagerStyle CssClass="GridAltItem" />
                                <RowStyle BorderColor="White" CssClass="GridItem" />
                                <HeaderStyle BorderColor="White" CssClass="GridHeader" ForeColor="Black" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>                            
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:eTransportString %>"
                                SelectCommand="Get_dispatcher_UnAssingedDriverDetails" SelectCommandType="StoredProcedure">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>                          
            </td>
        </tr>
    </table>
</asp:Content>

