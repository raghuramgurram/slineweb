<%@ Page AutoEventWireup="true" CodeFile="CustomerwiseLoads.aspx.cs" Inherits="CustomerwiseLoads"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>
    <%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc12" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>
                <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="middle">
                        <asp:Label ID="lbldeliveryapptdate" runat="server" Text="Delivery Appt. Date  : "></asp:Label>
                            <uc12:DatePicker ID="txtDate" runat="server" IsDefault="true" CountNextYears="1" CountPreviousYears="0" SetInitialDate="false" />
                            &nbsp;&nbsp;
                            <asp:Label ID="lblText" runat="server" Text="Customer Name : "></asp:Label>
                            <asp:DropDownList ID="ddlCustomers" runat="server"> 
                            </asp:DropDownList>
                            &nbsp;&nbsp;<asp:Label ID="lblStatus" runat="server" Text="Status : "></asp:Label>&nbsp;<asp:DropDownList
                                ID="dropStatus" runat="server">
                            </asp:DropDownList>
                            &nbsp;
                            <asp:Button ID="btnSearch" runat="server" CssClass="btnStyle" Text="Search" OnClick="btnSearch_Click" />
                        </td>
                    </tr>
                </table>                
            </td>
        </tr>        
    </table>    
</asp:Content>

