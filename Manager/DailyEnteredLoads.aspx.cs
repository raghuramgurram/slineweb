using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class DailyEnteredLoads : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Daily Entered Loads";
        if (!IsPostBack)
            FillCustomers();
    }

    private void FillCustomers()
    {
        //DataTable dt = DBClass.returnDataTable("SELECT [bint_CustomerId],[nvar_CustomerName] FROM [eTn_Customer] order by [nvar_CustomerName]");
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCustomerNames");
        if (ds.Tables.Count > 0)
        {
            ddlCustomer.DataSource = ds.Tables[0];
            ddlCustomer.DataTextField = "nvar_CustomerName";
            ddlCustomer.DataValueField = "bint_CustomerId";
            ddlCustomer.DataBind();
        }
        ddlCustomer.Items.Insert(0, new ListItem("All Customers", "0"));
        ddlCustomer.SelectedIndex = 0;
        if (ds != null) { ds.Dispose(); ds = null; }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Session["SerchCriteria"] = null;
            Session["ReportParameters"] = null;
            Session["ReportName"] = null;
            Session["ReportParameters"] = dtpFromDate.Date + "~~^^^^~~" + dtpToDate.Date + "~~^^^^~~" + ddlCustomer.SelectedItem.Value;
            Session["SerchCriteria"] = " From Date : " + dtpFromDate.Date + "&nbsp;&nbsp;&nbsp; To Date : " + dtpToDate.Date + "&nbsp;&nbsp;&nbsp;" + (ddlCustomer.SelectedIndex > 0 ? "&nbsp; Customer Name : " + ddlCustomer.SelectedItem.Text : " All Customers ");
            Session["ReportName"] = "GetDailyEnteredLoads";
            Response.Redirect("~/Manager/DisplayReport.aspx");
        }
    }
}
