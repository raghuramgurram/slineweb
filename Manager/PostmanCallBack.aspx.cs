﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_PostmanCallBack : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string queryString = HttpContext.Current.Request.Url.Query;
        if (!string.IsNullOrEmpty(queryString))
        {
            NameValueCollection queryStringCollection = HttpUtility.ParseQueryString(queryString);
            if (!string.IsNullOrEmpty(Convert.ToString(queryStringCollection.Get("LoadNumber"))))
            {
                Customer obj = new Customer();
                using (WebClient client = new WebClient())
                {
                    string requestUrl = ConfigurationManager.AppSettings["PostmanURL"] + "api/GetUpdatedLoad/" + ConfigurationManager.AppSettings["PartnerCode"] + "/" + ConfigurationManager.AppSettings["PartnerLocationCode"] +"/" + queryStringCollection.Get("LoadNumber");
                    string json = client.DownloadString(requestUrl);
                    LoadDetail objLoad = (new JavaScriptSerializer()).Deserialize<LoadDetail>(json);
                    string siteName = "Move to " + (ConfigurationManager.AppSettings["PartnerLocationCode"].ToString() == "SL" ? "Integra" : "Sline");
                    obj = CommonFunctions.ConvertEDILoadDetails(objLoad, siteName);

                    string AT5 = string.Empty;
                    if (obj.AT5 != null)
                        obj.AT5.ForEach(x => AT5 = AT5 + (!string.IsNullOrWhiteSpace(AT5) ? "~" : string.Empty) + x);
                    object[] objParams = new object[]{(objLoad.TransactionSetPurpose=="Change"? "U": "C"),obj.customerName,obj.orderNumber.Trim(),obj.remarks,string.IsNullOrWhiteSpace(AT5)?string.Empty:AT5,0,(obj.TripType=="ROUND"?2:1),string.Empty,string.IsNullOrWhiteSpace(obj.SealNumber)?string.Empty:obj.SealNumber,
                                                    string.Empty,0,obj.TripType,"40HC",string.Empty,string.Empty,0,Convert.ToInt64(ViewState["LoadID"]),0};
                    System.Data.SqlClient.SqlParameter[] objsqlparams = null;
                    ViewState["LoadID"] = "0";
                    objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationManager.AppSettings["conString"], "Add_Load_Details_New", objParams);

                    if (SqlHelper.ExecuteNonQuery(ConfigurationManager.AppSettings["conString"], CommandType.StoredProcedure, "Add_Load_Details_New", objsqlparams) > 0)
                    {
                        ViewState["LoadID"] = Convert.ToInt64(objsqlparams[objsqlparams.Length - 2].Value);
                        
                        if (objLoad.TransactionSetPurpose == "Change" && Convert.ToString(ViewState["LoadID"]).Trim().Length > 0 || Convert.ToString(ViewState["LoadID"]) != "0")
                        {
                            var locationId= SqlHelper.ExecuteScalar(ConfigurationSettings.AppSettings["conString"], "SP_Delete_Org_Des", new object[] { ViewState["LoadID"] });
                            if (obj.OriginDetails != null && obj.OriginDetails.Count > 0)
                            {
                                foreach (OriginDestination org in obj.OriginDetails)
                                {
                                    object[] objParamsorg = new object[]{org.CompanyName,"O", locationId,NullCheck(org.Address),NullCheck(org.Address1),NullCheck(org.City),NullCheck(org.StateorProvinceCode),
                                NullCheck(org.PostalCode), (string.IsNullOrWhiteSpace(org.Tel)?"0":org.Tel),NullCheck(string.IsNullOrWhiteSpace(org.Fax)?null:org.Fax),NullCheck(org.Email),ViewState["LoadID"].ToString(),NullCheck(org.FromDate),NullCheck(org.ContactPerson), NullCheck(org.ToDate),0};
                                    System.Data.SqlClient.SqlParameter[] objsqlparamsorg = null;
                                    objsqlparamsorg = SqlHelper.PrepareSqlParamsFromSP(ConfigurationManager.AppSettings["conString"], "SP_Insert_Org_Des", objParamsorg);
                                    SqlHelper.ExecuteNonQuery(ConfigurationManager.AppSettings["conString"], CommandType.StoredProcedure, "SP_Insert_Org_Des", objsqlparamsorg).ToString();
                                }

                            }
                            if (obj.DestinationDetails != null && obj.DestinationDetails.Count > 0)
                            {
                                foreach (OriginDestination org in obj.DestinationDetails)
                                {
                                    object[] objParamsorg = new object[]{org.CompanyName,"D", locationId,NullCheck(org.Address),NullCheck(org.Address1),NullCheck(org.City),NullCheck(org.StateorProvinceCode),
                                NullCheck(org.PostalCode), (string.IsNullOrWhiteSpace(org.Tel)?"0":org.Tel),NullCheck(string.IsNullOrWhiteSpace(org.Fax)?null:org.Fax),NullCheck(org.Email),ViewState["LoadID"].ToString(),NullCheck(org.FromDate),NullCheck(org.ContactPerson), NullCheck(org.ToDate),0};
                                    System.Data.SqlClient.SqlParameter[] objsqlparamsorg = null;
                                    objsqlparamsorg = SqlHelper.PrepareSqlParamsFromSP(ConfigurationManager.AppSettings["conString"], "SP_Insert_Org_Des", objParamsorg);
                                    SqlHelper.ExecuteNonQuery(ConfigurationManager.AppSettings["conString"], CommandType.StoredProcedure, "SP_Insert_Org_Des", objsqlparamsorg).ToString();
                                }

                            }
                            //SendInvoiceNumber(obj.customerName, obj.orderNumber, ConfigurationManager.AppSettings["InvoiceNumberPrefix"].ToString() + ViewState["LoadID"].ToString());
                            Response.Write("<script>alert('Load saved successully with load number " + ViewState["LoadID"].ToString() + ".')</script>");
                        }
                        objsqlparams = null;
                        objParams = null;
                    }
                }
            }
        }
    }
    private object NullCheck(object obj)
    {
        return obj != null ? obj : DBNull.Value;
    }
}
