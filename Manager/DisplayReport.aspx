<%@ Page AutoEventWireup="true" CodeFile="DisplayReport.aspx.cs" Inherits="DisplayReport"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" EnableEventValidation="false"%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
function goBack()
{
   var rptName = '<%= Session["ReportName"].ToString().Trim() %>';
   if(rptName == "GetStaffMistakes")
   {
     window.location.href = "../Manager/Mistakes.aspx";
     return window.location.href;
   }
   else if (rptName == "GetPaperWorkPending") {
     window.location.href = "../Manager/LoadswithPaperworkPending.aspx";
     return window.location.href;
   }
   else if (rptName == "GetPayableEntries") {
     window.location.href = "../Manager/PayableEntries.aspx";
     return window.location.href;
   }
   else
   {
     return window.history.go(-1);
   }
}
</script>
<div id="ContentTbl" class="auto-height">
<table width="100%" style=" position: absolute; top: 108px;" cellpadding="0" cellspacing="0">
<tr>
<td align="center" >
<input type="button" value="Back" style="height:21px;left: 685px; position: absolute;" class="btnstyle" onclick="goBack()"/>
<asp:Button ID="btnExport" runat="server" CssClass="btnstyle" Text="Export to Excel" OnClick="btnExport_Click" Height="21px" style="left: 735px; position: absolute;"/>
<asp:Button ID="btnSendMail" runat="server" CssClass="btnstyle" Text="Send Mail" Height="21px" style="left: 920px; position: absolute;" Visible="false" OnClick="btnSendMail_Click"/>
</td>
</tr>
</table>

<table id="tbldispaly" runat="server" cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr id="row">
        <td>
    <table  border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, sans-serif;
        color: #4D4B4C; font-size: 12px; height: 1px;" width="100%">
        <tr>
            <td valign="top">
                <asp:Label ID="lblHeader" runat="server" Visible="false" Font-Bold="True" ForeColor="Blue"
        Style="font-family: Arial, sans-serif; font-size: 12px;"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="height: 4px" valign="top">
                &nbsp;<asp:Label ID="lblSearchCriteria" runat="server" Font-Bold="True" ForeColor="Blue"
        Style="font-family: Arial, sans-serif; font-size: 12px;" Visible="false">
    </asp:Label>
    &nbsp;<asp:Label ID="lblTotal" runat="server" Font-Bold="True" ForeColor="red" Style="font-size: 12px;
        font-family: Arial, sans-serif" Visible="false">
    </asp:Label>
    <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="red" Style="font-size: 12px;
        font-family: Arial, sans-serif" Visible="false">
    </asp:Label>
    </td>            
        </tr>
        
    </table>
    </td>
    </tr>
    <tr>
    <td>
    <table  border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, sans-serif;
        color: #4D4B4C; font-size: 12px; height: 1px;" width="100%" id="Table1">
        <tr><td>
       
    <asp:Label ID="lblDisplay" runat="server" Text="" Width="100%"></asp:Label>&nbsp;
    <input id="btnPrintReport" type="button" value="Print"  class="btnstyle" runat="server" style="left: 891px; position: absolute; top: 108px;height: 21px;"/>
    <table id="tblResult" runat="server" width="100%" visible="false">
        <tr>
            <td colspan="2" align="right" Style="font-family: Arial, sans-serif; font-size: 12px;">
                <asp:Label ID="lblPaymentTotal" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2" Style="font-family: Arial, sans-serif; font-size: 12px;">
                <asp:Label ID="lblPaid" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2" Style="font-family: Arial, sans-serif; font-size: 12px;">
                <asp:Label ID="lblPending" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" Style="font-family: Arial, sans-serif; font-size: 14px;color:Red;font-weight:bold;">
                Total  deduction $ ______________________________
            </td>
        </tr>
        <tr>
            <td colspan="2" Style="font-family: Arial, sans-serif; font-size: 14px;color:Red;font-weight:bold;">
                Paid  check # __________________________________
            </td>
        </tr>
        <tr>
            <td colspan="2" Style="font-family: Arial, sans-serif; font-size: 14px;color:Red;font-weight:bold;">
               Total  Loads  on this sheet : <asp:Label ID="lblcount" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
</div>
</asp:Content>

