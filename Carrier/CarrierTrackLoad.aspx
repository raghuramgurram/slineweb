<%@ Page AutoEventWireup="true" CodeFile="~/Carrier/CarrierTrackLoad.aspx.cs" Inherits="CarrierTrackLoad"
    Language="C#" MasterPageFile="~/Carrier/CarrierMaster.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/Grid.ascx" TagName="Grid" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td style="width: 953px">
                <table id="tblbox1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                        </td>
                        <td align="right">
                            Track Load :
                            <asp:TextBox ID="txtContainer" runat="server">
                            </asp:TextBox>
                            <asp:DropDownList ID="ddlContainer" runat="server">
                                <asp:ListItem Selected="True" Value="1">Container#</asp:ListItem>
                                <asp:ListItem Value="2">Invoice#</asp:ListItem>
                                <asp:ListItem Value="3">Booking#</asp:ListItem>
                                <asp:ListItem Value="4">Chasis#</asp:ListItem>
                                <asp:ListItem Value="5">Load#</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Button ID="btnSearch" runat="server" CssClass="btnstyle"
                                Text="Search" OnClick="btnSearch_Click" />
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="height: 5px">
                            <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <%--<uc1:GridBar ID="barTrackLoad" runat="server" Visible="true" />--%>
                            <table id="GridBar" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblBarText" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:LinkButton ID="lnkLoadCharges" runat="server" Visible="false">Load Charges</asp:LinkButton>
                                        &nbsp;|&nbsp;
                                        <asp:LinkButton ID="lnkUpdateLoadStatus" runat="server" Visible="false">Update Load Status</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                         </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="height: 5px">
                            <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" style="width: 30%;" valign="top">
                            <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0"
                                width="100%">
                                <tr>
                                    <th colspan="2">
                                        Load Identification -&nbsp;<asp:Label ID="lblLoadId" runat="server" Text="LoadId" style="width:15%;"></asp:Label>
                                          <%--new coloumns add by madhav for Hazmat --%>
                                         <span id="spanishazmat" runat="server" style="color:Red;text-align:right;width:40%">HAZMAT</span>
                                           <%--end --%>
                                        </th>
                                </tr>
                                <tr id="row">
                                    <td align="right" width="40%" style="height: 20px">
                                        Invoice # : &nbsp;
                                    </td>
                                    <td width="60%" style="height: 20px">
                                        <asp:Label ID="lblInvoice" runat="server"></asp:Label>&nbsp;<br />
                                        <asp:LinkButton ID="lnkAdds" runat="server" Text="Add #(s)">
                                        </asp:LinkButton> |
                                        <asp:LinkButton ID="lnkLoadRequest" runat="server" Text="Load Request" OnClick="lnkLoadRequest_Click">
                                        </asp:LinkButton></td>                                        
                                        </tr>                               
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>
                            <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <th colspan="2">
                                        Order Status
                                    </th>
                                </tr>
                                <tr id="row">
                                    <td align="right" width="40%">
                                        Load Status :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblStatus" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        Container Size/Location :</td>
                                    <td>
                                        <asp:Label ID="lblRailBill" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="row">
                                    <td align="right">
                                        Order Bill Date :</td>
                                    <td>
                                        <asp:Label ID="lblBillDate" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        Person Bill :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPersonBill" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>
                            <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <th colspan="2">
                                        Routing</th>
                                </tr>
                                <tr id="row">
                                    <td colspan="2">                                      
                                        <asp:Label ID="lblRoute" runat="server" Text=""></asp:Label>&nbsp;
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" width="45%">
                                        Pier Termination / Empty Return:</td>
                                    <td>
                                        <asp:Label ID="lblPier" runat="server" Text=""></asp:Label>&nbsp;
                                    </td>
                                </tr>
                                <tr id="row" valign="top">
                                    <td align="right">
                                        Contact Person(s) :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblShippingContact" runat="server" Text=""></asp:Label>&nbsp;</td>
                                </tr>
                            </table>                          
                        </td>
                        <td align="left" style="width: 1%;" valign="top">
                            <img alt="" height="1" src="../images/pix.gif" width="5" /></td>
                        <td align="left" valign="top">
                            <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                   <td align="left" colspan="4">
                                     <table width="100%" border="0" cellpadding="0" cellspacing="0" id="goldBar">
                                          <tr>
                                            <td align="left">
                                                <strong>Order Identification</strong>
                                            </td>
                                          </tr>
                                        </table>
                                   </td>
                                </tr>
                                <%--<tr id="row">
                                    <td align="right" valign="top" width="15%">
                                        Customer :
                                    </td>
                                    <td colspan="3" valign="top">
                                        <asp:Label ID="lblCustomer" runat="server"></asp:Label>&nbsp;</td>
                                </tr>--%>
                                <tr id="altrow">
                                    <td align="right" valign="middle">
                                        Pickup# :
                                    </td>
                                    <td valign="middle" width="25%">
                                        <asp:Label ID="lblPickUp" runat="server"></asp:Label>&nbsp;</td>
                                    <td align="right" valign="middle">
                                        Commodity :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblCommodity" runat="server" Text=""></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="row">
                                    <td align="right" valign="middle">
                                        Container# :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblContainer" runat="server"></asp:Label>&nbsp;</td>
                                    <td align="right" valign="middle">
                                        Seal# :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblSeal" runat="server" Text=""></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" valign="middle">
                                        Chasis# :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblChasis" runat="server" Text=""></asp:Label>&nbsp;</td>
                                    <td align="right" valign="middle">
                                        Last free date:
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblLastfreedate" runat="server" Text=""></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="row">
                                    <td align="right" valign="middle">
                                        Pieces :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblPieces" runat="server"></asp:Label>&nbsp;
                                    </td>
                                    <td align="right" valign="middle">
                                        Weight :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblWeigth" runat="server"></asp:Label>&nbsp;</td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" valign="middle">
                                       Create Person :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblPerson" runat="server"></asp:Label>&nbsp;</td>
                                    <td align="right" valign="middle">
                                        Created :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblCreated" runat="server" Text=""></asp:Label>&nbsp;</td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="orangeBar">
                                          <tr>
                                            <td align="left">
                                                <strong>Origin(s)</strong>
                                            </td>
                                          </tr>
                                        </table> 
                                        <uc2:Grid ID="Grid1" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tealBar">
                                          <tr>
                                            <td align="left">
                                                <strong>Destination(s)</strong>
                                            </td>
                                          </tr>
                                        </table>
                                        <uc2:Grid ID="Grid2" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>
                           <%-- <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="right">
                                        <uc1:GridBar ID="GridBar2" runat="server" HeaderText="Load Status Information" />
                                    </td>
                                </tr>
                            </table>  --%>   
                                         <table width="100%" border="0" cellpadding="0" cellspacing="0" id="yellowBar">
                                <tr>
                                    <td align="left">
                                        Load Status Information
                                        <%--<uc1:GridBar ID="GridBar2" runat="server" HeaderText="Load Status Information"/>--%>
                                    </td>
                                </tr>
                            </table>          
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <uc2:Grid ID="Grid4" runat="server" PageSize="20"/>
                                    </td>
                                </tr>
                            </table>   
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="5" src="Images/pix.gif" width="1" /></td>
                                </tr>
                            </table>
                           <%-- <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="right">
                                        <uc1:GridBar ID="GridBar3" runat="server" HeaderText="Notes" />
                                    </td>
                                </tr>
                            </table>--%>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tealBar">
                                <tr>
                                    <td align="left">
                                        Notes
                                    </td>
                                    <td align="right">
                                        <asp:LinkButton ID="lnkNotes" runat="server">Add Note</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <uc2:Grid ID="Grid5" runat="server" />
                                    </td>
                                </tr>
                            </table>                                          
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

