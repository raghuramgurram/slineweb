using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

//SLine 2016 Enhancements
using System.Globalization;
using System.Xml;
using System.Xml.XPath;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.Rendering;
using System.Diagnostics;
using CreatePDFInvoice;
using System.IO;
using System.Net;


public partial class Invoice : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Invoice";
        if (!IsPostBack)
        {
            //lblCompanyAddress.Text = "<strong>" + Convert.ToString(ConfigurationSettings.AppSettings["CompanyName"]) + "</strong><br/>" + Convert.ToString(ConfigurationSettings.AppSettings["ComapanyAddress"]) + "<br/>" + Convert.ToString(ConfigurationSettings.AppSettings["ComapanyState"]) + "<br/>" + Convert.ToString(ConfigurationSettings.AppSettings["CompanyPhone"]) + "<br/>" + Convert.ToString(ConfigurationSettings.AppSettings["CompanyFax"]);
            lblCompanyAddress.Text = "<strong>" + GetFromXML.CompanyName + "</strong><br/>" + GetFromXML.ComapanyAddress + "<br/>" + GetFromXML.ComapanyState + "<br/>" + GetFromXML.CompanyPhone + "<br/>" + GetFromXML.CompanyFax;
            if (Request.QueryString.Count > 0)
            {
                ViewState["ID"] = Convert.ToInt64(Request.QueryString[0]);
                FillDetails(Convert.ToInt64(ViewState["ID"]));

                if (CheckInvoiceIsExists())
                {
                    hiddenInvoiceFlag.Value = "yes";
                }
                else
                {
                    hiddenInvoiceFlag.Value = "no";
                }
            }
        }
    }
    private void FillDetails(long ID)
    {
        lblInvoiceLoad.Text=lblLoad.Text = ID.ToString();

        DataSet dsInvoiceDetails = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetInvoiceDetails", new object[] { ID });

        if (dsInvoiceDetails.Tables.Count > 2)
        {
            lblOrigin.Text = CommonFunctions.FormatMultipleOrigins(Convert.ToString(dsInvoiceDetails.Tables[0].Rows[0][0]).Trim());
            lblDestination.Text = CommonFunctions.FormatMultipleOrigins(Convert.ToString(dsInvoiceDetails.Tables[0].Rows[0][1]).Trim());
            if (dsInvoiceDetails.Tables[1].Rows.Count > 0)
            {
                lblBillTo.Text = Convert.ToString(dsInvoiceDetails.Tables[1].Rows[0][0]) + "<br/>" + Convert.ToString(dsInvoiceDetails.Tables[1].Rows[0][1]) + "  " + Convert.ToString(dsInvoiceDetails.Tables[1].Rows[0][2]) + "<br/>";
                lblBillTo.Text += Convert.ToString(dsInvoiceDetails.Tables[1].Rows[0][3]) + ", " + Convert.ToString(dsInvoiceDetails.Tables[1].Rows[0][4]) + " " + Convert.ToString(dsInvoiceDetails.Tables[1].Rows[0][5]);
                lblBillingRef.Text = Convert.ToString(dsInvoiceDetails.Tables[1].Rows[0][6]);
                lblDate.Text = Convert.ToString(dsInvoiceDetails.Tables[1].Rows[0][7]);
                lblLoad.Text = Convert.ToString(ID);
                lblContainer.Text = Convert.ToString(dsInvoiceDetails.Tables[1].Rows[0][11]);
                lblChasis.Text = Convert.ToString(dsInvoiceDetails.Tables[1].Rows[0][12]);
                lblDescription.Text = Convert.ToString(dsInvoiceDetails.Tables[1].Rows[0][14]);
                lblTerms.Text = "Due on Receipt";
                //lblremarks.Text = Convert.ToString(ds.Tables[1].Rows[0][14]);
                //lblBooking.Text = Convert.ToString(ds.Tables[1].Rows[0][9]);


                ViewState["BillToLine1"] = Convert.ToString(dsInvoiceDetails.Tables[1].Rows[0][0]);
                ViewState["BillToLine2"] = Convert.ToString(dsInvoiceDetails.Tables[1].Rows[0][1]) + "  " + Convert.ToString(dsInvoiceDetails.Tables[1].Rows[0][2]);
                ViewState["BillToLine3"] = Convert.ToString(dsInvoiceDetails.Tables[1].Rows[0][3]) + ", " + Convert.ToString(dsInvoiceDetails.Tables[1].Rows[0][4]) + " " + Convert.ToString(dsInvoiceDetails.Tables[1].Rows[0][5]);
                ViewState["Origins"] = Convert.ToString(dsInvoiceDetails.Tables[0].Rows[0][0]).Trim();
                ViewState["Destination"] = Convert.ToString(dsInvoiceDetails.Tables[0].Rows[0][1]).Trim();
                ViewState["Address"] = GetFromXML.CompanyName + " " + GetFromXML.ComapanyAddress + " " + GetFromXML.ComapanyState + " " + "^" + GetFromXML.CompanyPhone + " " + GetFromXML.CompanyFax;
                ViewState["BillingRef"] = Convert.ToString(dsInvoiceDetails.Tables[1].Rows[0][6]);
                ViewState["Chasis"] = Convert.ToString(dsInvoiceDetails.Tables[1].Rows[0][12]);
                ViewState["Description"] = Convert.ToString(dsInvoiceDetails.Tables[1].Rows[0][14]);
                ViewState["Terms"] = "Due on Receipt";

                if (dsInvoiceDetails.Tables[3].Rows.Count > 0)
                {
                    ViewState["Amount"] = Convert.ToString(dsInvoiceDetails.Tables[3].Rows[0][0]);
                }
                else
                {
                    ViewState["Amount"] = "";
                }
            }
            if (dsInvoiceDetails.Tables[2].Rows.Count > 0)
            {
                lblSacript.Text = Convert.ToString(dsInvoiceDetails.Tables[2].Rows[0][0]);
            }
            #region Old Code
            //{
            //    lblAmount.Text = "$" + Convert.ToString(ds.Tables[2].Rows[0][0]).Trim();
            //    lblFuelCharges.Text = "$" + Convert.ToString(ds.Tables[2].Rows[0][2]).Trim();

            //    if (Convert.ToString(ds.Tables[2].Rows[0][3]).Trim().Length > 0 && Convert.ToDouble(ds.Tables[2].Rows[0][3]) > 0)
            //    {
            //        lblChargesSplit.Text = "$" + Convert.ToString(ds.Tables[2].Rows[0][3]).Trim();
            //    }
            //    else
            //    {
            //        lblChargesSplit.Visible = false;
            //        lblChasisSplit1.Visible = false;
            //    }
            //    if (Convert.ToString(ds.Tables[2].Rows[0][4]).Trim().Length > 0 && Convert.ToDouble(ds.Tables[2].Rows[0][4]) > 0)
            //    {
            //        lblChasisRent.Text = "$" + Convert.ToString(ds.Tables[2].Rows[0][4]).Trim();
            //    }
            //    else
            //    {
            //        lblChasisRent.Visible = false;
            //        lblChasisRent1.Visible = false;
            //    }
            //    if (Convert.ToString(ds.Tables[2].Rows[0][7]).Trim().Length > 0)
            //    {
            //        lblRemarks.Text = Convert.ToString(ds.Tables[2].Rows[0][7]).Trim() + "&nbsp;&nbsp;";
            //    }
            //    else
            //    {
            //        lblRemarks.Visible = false;
            //        lblOtherChargesReason1.Visible = false;
            //    }
            //    if (Convert.ToString(ds.Tables[2].Rows[0][5]).Trim().Length > 0 && Convert.ToDouble(ds.Tables[2].Rows[0][5]) > 0)
            //    {
            //        lblYardStorage.Text = "$" + Convert.ToString(ds.Tables[2].Rows[0][5]).Trim();
            //    }
            //    else
            //    {
            //        lblYardStorage.Visible = false;
            //        lblYardStorage1.Visible = false;
            //    }
            //    if (Convert.ToString(ds.Tables[2].Rows[0][6]).Trim().Length > 0 && Convert.ToDouble(ds.Tables[2].Rows[0][6]) > 0)
            //    {
            //        lblYardPull.Text = "$" + Convert.ToString(ds.Tables[2].Rows[0][6]).Trim() + "&nbsp;";
            //    }
            //    else
            //    {
            //        lblYardPull.Visible = false;
            //        lblYardPull1.Visible = false;
            //    }


            //    lblBalance.Text = "$" + Convert.ToString(Convert.ToInt64(ds.Tables[2].Rows[0][0]) + Convert.ToInt64(ds.Tables[2].Rows[0][1]) +
            //        Convert.ToInt64(ds.Tables[2].Rows[0][2]) + Convert.ToInt64(ds.Tables[2].Rows[0][3]) + Convert.ToInt64(ds.Tables[2].Rows[0][4]) +
            //        Convert.ToInt64(ds.Tables[2].Rows[0][5]) + Convert.ToInt64(ds.Tables[2].Rows[0][6]));
            //}
            //else
            //{
            //    lblAmount.Text = "$0";
            //    lblFuelCharges.Text = "$0";
            //    lblBalance.Text = "$0";
            //    lblChargesSplit.Visible = false;
            //    lblChasisSplit1.Visible = false;
            //    lblChasisRent.Visible = false;
            //    lblChasisRent1.Visible = false;
            //    lblRemarks.Visible = false;
            //    lblOtherChargesReason1.Visible = false;
            //    lblYardStorage.Visible = false;
            //    lblYardStorage1.Visible = false;
            //    lblYardPull.Visible = false;
            //    lblYardPull1.Visible = false;
            //}
            #endregion
        }
        if (dsInvoiceDetails != null) { dsInvoiceDetails.Dispose(); dsInvoiceDetails = null; }
    }
    //SLine 2016 Enhancements    
    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (ViewState["ID"] != null)
        {
            Response.Redirect("~/Manager/TrackLoad.aspx?" + Convert.ToInt64(ViewState["ID"]));
        }
    }
    protected void btnSaveAsPdf_Click(object sender, EventArgs e)
     {
        // Create a invoice form with the sample invoice data
        PDFInvoice invoice = new PDFInvoice();

        // Create a MigraDoc document
        Document document = invoice.CreateDocument(Convert.ToInt64(ViewState["ID"]), ViewState["BillToLine1"].ToString(),
                                                    ViewState["BillToLine2"].ToString(), ViewState["BillToLine3"].ToString(),
                                                    Convert.ToDateTime(lblDate.Text).ToShortDateString(), lblLoad.Text, lblContainer.Text,
                                                    ViewState["Origins"].ToString(), ViewState["Destination"].ToString(),
                                                    ViewState["Amount"].ToString(), ViewState["Address"].ToString(),
                                                    ViewState["BillingRef"].ToString(), ViewState["Chasis"].ToString(),
                                                    ViewState["Description"].ToString(),ViewState["Terms"].ToString());
        document.UseCmykColor = true;

        // Create a renderer for PDF that uses Unicode font encoding
        PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(true);

        // Set the MigraDoc document
        pdfRenderer.Document = document;

        // Create the PDF document
        pdfRenderer.RenderDocument();
        
        //Setting Folder and Invoice Name
        string directoryPath = Server.MapPath("~/Documents/" + Convert.ToInt64(ViewState["ID"]));
        string strInvoiceName = "";
        //Checking if Directory Exists
        if (Directory.Exists(directoryPath))
        {
            //Get the Invoice name here
            strInvoiceName = GetInvoiceName();

            //Check here whether Invoice exists
            //If the File exists in the Directory we will not make any changes in the DB for this Invoice, just
            //Rewrite the file taking confirmation from the User
            if (File.Exists(directoryPath + "\\" + strInvoiceName))
            {               
                    try
                    {
                        //here the file gets saved into the Documents folder
                        pdfRenderer.Save(directoryPath + "\\" + strInvoiceName);
                        //Update the flag, as the 
                        hiddenInvoiceFlag.Value = "yes";
                        Response.Write("<script>alert('Invoice overwrite successful.')</script>");
                    }
                    catch
                    {
                        Response.Write("<script>alert('Invoice overwrite failed. Please try again')</script>");
                    }   
            }

            //If the Directory Exists but Invoice is not, then create it here
            else
            {
                //First insert the Record-->Get the File name--> save the pdf with that name
                string strNewInvoiceName = CreateNewInvoiceDB();
                if (strNewInvoiceName != "")
                {
                    try
                    {
                        pdfRenderer.Save(directoryPath + "\\" + strNewInvoiceName);
                        hiddenInvoiceFlag.Value = "yes";
                        Response.Write("<script>alert('Invoice saved as pdf successfully.')</script>");
                    }
                    catch
                    {
                        Response.Write("<script>alert('Invoice not saved as pdf. Please try again')</script>");
                    }
                }
            }
        }

       //Else if the Directory is not Existing
        else
        {
            // Try to create the directory.
            DirectoryInfo di = Directory.CreateDirectory(directoryPath);
            //First insert the Record-->Get the File name--> save the pdf with that name
            string strNewInvoiceName = CreateNewInvoiceDB();
            if (strNewInvoiceName != "")
            {
                try
                {
                    pdfRenderer.Save(directoryPath + "\\" + strNewInvoiceName);
                    hiddenInvoiceFlag.Value = "yes";
                    Response.Write("<script>alert('Invoice saved as pdf successfully.')</script>");
                }
                catch
                {
                    Response.Write("<script>alert('Invoice not saved as pdf. Please try again')</script>");
                }
            }
        }
    }
    private string CreateNewInvoiceDB()
    {
        string strNewInvoiceName = "";
        //Here we insert a record in the DB
        //Here ^N means that a new record has to be generated and it will be Entered only once
        string strInvoiceName = Convert.ToInt64(ViewState["ID"]) + "_" + 11 + "_Invoice.pdf^N";
        if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_SaveUploadDocuments", new object[] { Convert.ToInt64(ViewState["ID"]), 11, strInvoiceName }) > 0)
        {
            //If file name saved Succefully in the DB get the Name
            //The existing SP to return and a seperat Inline Query get the name
            strNewInvoiceName = GetInvoiceName();
            return strNewInvoiceName;
        }
        return strNewInvoiceName = "";
    }

    private string GetInvoiceName()
    {
        string strDocumentName = "";
        try
        {
            strDocumentName = DBClass.executeScalar("select [nvar_DocumentName] as DocumentName from [eTn_UploadDocuments] where [bint_LoadId] = " + Convert.ToInt64(ViewState["ID"]) + " and [bint_DocumentId] = 11 and [bint_FileNo] = 1");
            string[] strName = strDocumentName.Split('^');
            return strDocumentName = strName[0];
        }
        catch
        {
        }
        return strDocumentName;
    }

    private bool CheckInvoiceIsExists()
    {
        string directoryPath = Server.MapPath("~/Documents/" + Convert.ToInt64(ViewState["ID"]));
        if (Directory.Exists(directoryPath))
        {
            string strInvoiceName = GetInvoiceName();
            if (File.Exists(directoryPath + "\\" + strInvoiceName))
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        else
        {
            return false;
        }
    }
}
