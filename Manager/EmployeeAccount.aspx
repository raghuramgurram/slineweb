<%@ Page AutoEventWireup="true" CodeFile="EmployeeAccount.aspx.cs" Inherits="EmployeeAccount"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/Grid.ascx" TagName="Grid" TagPrefix="uc3" %>

<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="ContentTbl">
<tr valign="top">
    <td>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblbox1">
        <tr style="background-color:#FFFFFF;">
            <td align="left" width="50%">
                &nbsp;<asp:Label ID="lblText" runat="server" Text="Employees Type : "></asp:Label>&nbsp;
                <asp:DropDownList ID="ddlList" runat="server">
                    <asp:ListItem Text="--Select Employees--" Value="0" Selected="true"></asp:ListItem>
                    <asp:ListItem Value="1">Active Employees</asp:ListItem>
                    <asp:ListItem Value="0">In Active Employees</asp:ListItem>
                </asp:DropDownList>&nbsp;
             <asp:Button ID="btnShow" runat="server" Text="Show" CssClass="btnstyle" OnClick="btnShow_Click" />
          </td>
          <td align="right">
            Driving License# (or) Plate# &nbsp;:&nbsp; <asp:TextBox ID="txtSearch" runat="Server"></asp:TextBox>&nbsp;
            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnstyle" OnClick="btnSearch_Click" />
          </td>
        </tr>
      </table>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
            </tr>
        </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
              <uc2:GridBar ID="barCurrent" runat="server" HeaderText="Active Employees" LinksList="Add New Employee" PagesList="AddEmployee.aspx"/></td>
        </tr>
      </table>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
            </tr>
        </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tbldisplay">
       <tr>
            <td>
                <uc3:Grid ID="gridCurrent" runat="server" />                
            </td>            
       </tr>
      </table>     
    </td>  
  </tr>
</table>
      
</asp:Content>

