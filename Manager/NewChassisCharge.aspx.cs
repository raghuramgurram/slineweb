using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Manager_NewChassisCharge : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "New / Edit Chassis Charge";
        if (!Page.IsPostBack)
        {
            if (Request.QueryString.Count > 0)
            {
                ViewState["ID"] = Request.QueryString[0].Trim();
                try
                {
                    ViewState["Mode"] = "Edit";
                    ViewState["ID"] = Convert.ToInt64(Request.QueryString[0]);
                    FillDetails();
                }
                catch
                {
                }
                Session["TopHeaderText"] = "Edit Chassis Charge";
            }
            else
            {
                if (ViewState["ID"] == null)
                {
                    ViewState["Mode"] = "New";
                    Session["TopHeaderText"] = "New Chassis Charge";
                }
                else
                {
                    try
                    {
                        ViewState["Mode"] = "Edit";
                        FillDetails();
                        Session["TopHeaderText"] = "Edit Chassis Charge";
                    }
                    catch
                    {
                    }
                }
            }
        }
    }

    private void FillDetails()
    {
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetChassisCharges", new object[] { Convert.ToInt64(ViewState["ID"]) });
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtChassisCharge.Text = Convert.ToString(ds.Tables[0].Rows[0]["num_ChassisCharges"]);
                txtEffectiveDate.Date = Convert.ToString(ds.Tables[0].Rows[0]["date_EffectiveDate"]);
                hdnEffectiveDate.Value = Convert.ToString(ds.Tables[0].Rows[0]["date_EffectiveDate"]);
            }
        }
        if (ds != null) { ds.Dispose(); ds = null; }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
       Response.Redirect("~/Manager/ChassisCharges.aspx");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            bool flag = false;
            string effDate = string.Empty;
            if (ViewState["Mode"] != null)
            {
                object[] objParams = new object[] { txtChassisCharge.Text.Trim(),CommonFunctions.CheckDateTimeNull(txtEffectiveDate.Date),Convert.ToInt64(Session["UserLoginId"]),Convert.ToInt64(ViewState["ID"])};
                System.Data.SqlClient.SqlParameter[] objsqlparams = null;
                if (Convert.ToString(ViewState["Mode"]).StartsWith("Edit", true, System.Globalization.CultureInfo.CurrentCulture))
                {
                    effDate = hdnEffectiveDate.Value;
                    objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Update_ChassisCharge", objParams);
                    if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Update_ChassisCharge", objsqlparams) > 0)
                    {
                        flag = true;
                        //Response.Redirect("~/Manager/ChassisCharges.aspx");
                    }
                }
                else if (Convert.ToString(ViewState["Mode"]).StartsWith("New", true, System.Globalization.CultureInfo.CurrentCulture))
                {
                    effDate = txtEffectiveDate.Date;
                    if (Convert.ToInt32(DBClass.executeScalar("select count(bint_ChassisChargesId) from eTn_ChassisCharges where date_EffectiveDate ='" + CommonFunctions.CheckDateTimeNull(txtEffectiveDate.Date) + "'")) > 0)
                    {
                        lblNameError.Visible = true;
                        return;
                    }
                    objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Add_ChassisCharge", objParams);
                    if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Add_ChassisCharge", objsqlparams) > 0)
                    {
                        flag = true;
                        //Response.Redirect("~/Manager/ChassisCharges.aspx");
                    }
                }
                if (flag)
                {
                    objParams = new object[] { txtChassisCharge.Text.Trim(), CommonFunctions.CheckDateTimeNull(effDate), Convert.ToInt64(Session["UserLoginId"]) };
                    objsqlparams = null;
                    objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Update_Load_ChassisCharge", objParams);
                    if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Update_Load_ChassisCharge", objsqlparams) > 0)
                    {
                        
                    }
                    Response.Redirect("~/Manager/ChassisCharges.aspx");
                }
                objParams = null;
                objsqlparams = null;
            }
        }
    }
}
