/*
 * dateTimePicker for jQuery Mobile
 * developed by: Harm Verbeek (harm@certeza.nl)
 * update: Aug 9 2013
 */ 
dateTimePicker = function (inputElement) {

    var self = this;

    this.forElement = '#' + this.id;
    this.oldValue = null;

	this.months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	this.month   = 0;
	this.day     = 0;
	this.year    = 0;
	this.hours   = 0;
	this.minutes = 0;
	this.seconds = 0;

    var $popUp = $("<div id='dtpPopup' />").popup({
        dismissible: false,
        theme: "b",
        transition: "pop"
    }).on("popupafterclose", function () {
        $(this).remove();
    }).css({
        'width': '235px',
        'padding': '5px'
    });

    var content = '<div data-role="navbar" data-grid="d">'
		    + '<ul>'
		        + '<li><a href="#" class="DTP_btn" data_dtp_func="incr_month">+</a></li>'
		        + '<li><a href="#" class="DTP_btn" data_dtp_func="incr_day">+</a></li>'
		        + '<li><a href="#" class="DTP_btn" data_dtp_func="incr_year">+</a></li>'
		        + '<li><a href="#" class="DTP_btn" data_dtp_func="incr_hours">+</a></li>'
		        + '<li><a href="#" class="DTP_btn" data_dtp_func="incr_minutes">+</a></li>'
		    + '</ul>'
		+ '</div>'
		+ '<div class="ui-grid-d">'
			+ '<div class="DTP_elm ui-block-a" id="DTP_month" style="border-left:1px solid #666">Aug</div>'
			+ '<div class="DTP_elm ui-block-b" id="DTP_day">15</div>'
			+ '<div class="DTP_elm ui-block-c" id="DTP_year">2013</div>'
			+ '<div class="DTP_elm ui-block-d" id="DTP_hours" style="border-left:1px solid #666">00</div>'
			+ '<div class="DTP_elm ui-block-e" id="DTP_minutes"style="border-right:1px solid #666">25</div>'
		+ '</div>'
		+ '<div data-role="navbar" data-grid="d">'
		    + '<ul>'
		        + '<li><a href="#" class="DTP_btn" data_dtp_func="decr_month">-</a></li>'
		        + '<li><a href="#" class="DTP_btn" data_dtp_func="decr_day">-</a></li>'
		        + '<li><a href="#" class="DTP_btn" data_dtp_func="decr_year">-</a></li>'
		        + '<li><a href="#" class="DTP_btn" data_dtp_func="decr_hours">-</a></li>'
		        + '<li><a href="#" class="DTP_btn" data_dtp_func="decr_minutes">-</a></li>'
		    + '</ul>'
		+ '</div>'
		+ '<a href="" class="DTP_cmd" data_dtp_func="now" data-role="button" data-inline="true" data-theme="b">Now</a>'
		+ '<a href="" class="DTP_cmd" data_dtp_func="set" data-role="button" data-inline="true" data-theme="b">Set</a>'
		+ '<a href="" class="DTP_cmd" data_dtp_func="done" data-role="button" data-inline="true" data-theme="b">Cancel</a>';

	$(content).appendTo($popUp);	

	$('.DTP_elm').css({'background-color':'#fff','text-align':'center','height':'40px','font-size':'10pt','line-height':'45px'});
	$('.DTP_btn > .ui-btn-inner').css({'font-size':'18pt','height':'2px','line-height':'1px'});

	$(".DTP_btn").click( function(event) {
	   var func = $(this).attr('data_dtp_func').split('_');

	   if (func[0] === 'incr') {
	   		self.incr(func[1]);
	   } else if ( func[0] === 'decr' ) {
	   		self.decr(func[1]);
	   }

	   $(this).removeClass($.mobile.activeBtnClass);
	});

	$('.DTP_cmd').click( function() {
	   self.cmd( $(this).attr('data_dtp_func') );
	});

    this.isValidDate = function(dateString) {

       if (dateString==='')
       	  return false;

       // convert to date string
	   var d1 = (new Date(dateString)+'').substr(3,18);
	   // convert to ms
	   var d2 = new Date(dateString).getTime();
	   // create datestring from ms
	   var d3 = (new Date(d2) + '').substr(3,18);

	   // compare
	   return (d3 === d1);
    }

	this.now = function() {
		self.init();
		self.set();
	}

	this.set = function() {
		var ds = this.months[this.month]+ ' ';
		ds += (this.day<10 ? '0'+this.day : this.day) + ' ' + this.year + ' ';
		ds += (this.hours<10 ? '0'+this.hours : this.hours) + ':';
		ds += this.minutes<10 ? '0'+this.minutes : this.minutes;

		$(this.forElement).val(ds);
	}

	this.reset = function() {
	   $(this.forElement).val(this.oldValue);
	}

	this.cmd = function(c) {
	  if (c === 'now') 
	     this.now();
	  else if (c === 'set')
	     this.set();
	  else if (c === 'done')
	  	 this.reset();
	  $popUp.popup('close');
	}

	this.incr = function(dt_elem) {
	   switch (dt_elem) {
	       case 'day':
	          this.day++;
	          if (this.day>31)
	             this.day = 1;
	          $('#DTP_day').text(this.day<10 ? '0'+this.day : this.day);   	
	          break; 
	       case 'month':
	       	  this.month++;
	          if (this.month>11)
	             this.month = 0;
	          $('#DTP_month').text(this.months[this.month]); 
	          break; 
	       case 'year':
	          this.year++;
	          if (this.year>2100)
	             this.year = 1970;
	          $('#DTP_year').text(this.year); 
	          break; 
	       case 'hours':
	          this.hours++;
	          if (this.hours>23)
	             this.hours = 0;
	          $('#DTP_hours').text(this.hours<10 ? '0'+this.hours : this.hours); 
	          break; 
	       case 'minutes':
	          this.minutes++;
	          if (this.minutes>59)
	             this.minutes = 0;
	          $('#DTP_minutes').text(this.minutes<10 ? '0'+this.minutes : this.minutes); 
	          break; 
	   }
	}

	this.decr = function(dt_elem) {
	   switch (dt_elem) {
	       case 'day':
	          this.day--;
	          if (this.day<1)
	             this.day = 31;
	          $('#DTP_day').text(this.day<10 ? '0'+this.day : this.day);   	
	          break; 
	       case 'month':
	       	  this.month--;
	          if (this.month<0)
	             this.month = 11;
	          $('#DTP_month').text(this.months[this.month]); 
	          break; 
	       case 'year':
	          this.year--;
	          if (this.year<0)
	             this.year = 0;
	          $('#DTP_year').text(this.year); 
	          break; 
	       case 'hours':
	          this.hours--;
	          if (this.hours<0)
	             this.hours = 0;
	          $('#DTP_hours').text(this.hours<10 ? '0'+this.hours : this.hours); 
	          break; 
	       case 'minutes':
	          this.minutes--;
	          if (this.minutes<0)
	             this.minutes = 59;
	          $('#DTP_minutes').text(this.minutes<10 ? '0'+this.minutes : this.minutes); 
	          break; 
	   }
	}

	this.init = function() {
	    this.forElement = '#' + this.id;
		this.oldValue = $(this.forElement).val();

		var today;
		if (self.isValidDate(this.oldValue)) {
		   today = new Date(this.oldValue);
		} else {
		   today = new Date();
		}

		this.day     = today.getDate();
		this.month   = today.getMonth(); //January is 0!
		this.year    = today.getFullYear();
		this.hours   = today.getHours();
		this.minutes = today.getMinutes();
		this.seconds = today.getSeconds();

		$('#DTP_day').text(this.day<10 ? '0'+this.day : this.day);
		$('#DTP_month').text(this.months[this.month]);
		$('#DTP_year').text(this.year); 
		$('#DTP_hours').text(this.hours<10 ? '0'+this.hours : this.hours);
		$('#DTP_minutes').text(this.minutes<10 ? '0'+this.minutes : this.minutes);
    }

    self.init();
	var offset = $(self.forElement).offset();
    $popUp.popup('open', {x: offset.left + 120, y:offset.top + 190}).trigger("create");
}
