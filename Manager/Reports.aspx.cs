using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Reports : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Reports";

        //SLine 2016 Enhancements
        if (Convert.ToString(Session["Role"]) == "Manager")
        {           
           panel_PaymentReports.Visible = true;
        }
        else
        {
           panel_PaymentReports.Visible = false;
        }

        //SLine 2016 Enhancements

    }
    protected void lnkDailyDispatchedLoads_Click(object sender, EventArgs e)
    {
        Session["SerchCriteria"] = null;
        Session["ReportParameters"] = null;
        Session["ReportName"] = null;       
        Session["ReportName"] = "GetDailyDispatchedLoads";
        Response.Redirect("~/Manager/DisplayReport.aspx");
    }
    protected void lnkReadyToDispatchLoads_Click(object sender, EventArgs e)
    {
        Session["SerchCriteria"] = null;
        Session["ReportParameters"] = null;
        Session["ReportName"] = null;
        Session["ReportName"] = "GetReadyToDispatchedLoads";
        Response.Redirect("~/Manager/DisplayReport.aspx");
    }
    protected void lnkAllOpenLoads_Click(object sender, EventArgs e)
    {
        Session["SerchCriteria"] = null;
        Session["ReportParameters"] = null;
        Session["ReportName"] = null;
        Session["ReportName"] = "GetAllOpenLoads";
        Response.Redirect("~/Manager/DisplayReport.aspx");
    }
    protected void lnkLoadsWithLastFreeDay_Click(object sender, EventArgs e)
    {
        Session["SerchCriteria"] = null;
        Session["ReportParameters"] = null;
        Session["ReportName"] = null;
        Session["ReportName"] = "GetLoadsWithLastFreeDay";
        Response.Redirect("~/Manager/DisplayReport.aspx");
    }
    protected void lnkLoadsWithNoLastFreeDay_Click(object sender, EventArgs e)
    {
        Session["SerchCriteria"] = null;
        Session["ReportParameters"] = null;
        Session["ReportName"] = null;
        Session["ReportName"] = "GetAllLoadsWithNoLastFreeDay";
        Response.Redirect("~/Manager/DisplayReport.aspx");
    }
    protected void lnkLoadedInYard_Click(object sender, EventArgs e)
    {
        Session["SerchCriteria"] = null;
        Session["ReportParameters"] = null;
        Session["ReportName"] = null;
        Session["ReportName"] = "GetLoadedInYardLoads";
        Response.Redirect("~/Manager/DisplayReport.aspx");
    }
    protected void lnlEmptiesInYard_Click(object sender, EventArgs e)
    {
        Session["SerchCriteria"] = null;
        Session["ReportParameters"] = null;
        Session["ReportName"] = null;
        Session["ReportName"] = "GetEmptiesInYardLoads";
        Response.Redirect("~/Manager/DisplayReport.aspx");
    }
    protected void lnkPaperWorkPending_Click(object sender, EventArgs e)
    {
        Session["SerchCriteria"] = null;
        Session["ReportParameters"] = null;
        Session["ReportName"] = null;
        Session["ReportName"] = "GetPaperWorkPending";
        Response.Redirect("~/Manager/LoadswithPaperworkPending.aspx");
    }

    protected void lnkPaperWorkPendingOverdue_Click(object sender, EventArgs e)
    {
        Session["SerchCriteria"] = null;
        Session["ReportParameters"] = null;
        Session["ReportName"] = null;
        Session["ReportName"] = "GetPaperworkOverdueLoads";
        Response.Redirect("~/Manager/PaperworkOverdueLoads.aspx");
    }

    protected void lnkInvoiceable_Click(object sender, EventArgs e)
    {
        Session["SerchCriteria"] = null;
        Session["ReportParameters"] = null;
        Session["ReportName"] = null;
        Session["ReportName"] = "GetInvoiceableLoads";
        Response.Redirect("~/Manager/InvoiceableLoads.aspx?Type=1");
    }

    protected void lnkLoadswithnoapptDate_Click(object sender, EventArgs e)
    {
        Session["SerchCriteria"] = null;
        Session["ReportParameters"] = null;
        Session["ReportName"] = null;
        Session["ReportName"] = "GetLoadsWithNoApptDate";
        Response.Redirect("~/Manager/DisplayReport.aspx");
    }
    protected void lnkLoadswithnoPickup_Click(object sender, EventArgs e)
    {
        Session["SerchCriteria"] = null;
        Session["ReportParameters"] = null;
        Session["ReportName"] = null;
        Session["ReportName"] = "GetLoadsWithNoPickupDate";
        Response.Redirect("~/Manager/DisplayReport.aspx");
    }
    protected void lnkLoadwithAppDate_Click(object sender, EventArgs e)
    {
        Session["SerchCriteria"] = null;
        Session["ReportParameters"] = null;
        Session["ReportName"] = null;    
        Session["ReportName"] = "LoadswithApptDate";
        Response.Redirect("~/Manager/DisplayReport.aspx");
    }
    protected void lnkLoadwithNoTag_Click(object sender, EventArgs e)
    {
        Session["SerchCriteria"] = null;
        Session["ReportParameters"] = null;
        Session["ReportName"] = null;    
        Session["ReportName"] = "LoadswithNoTag";
        Response.Redirect("~/Manager/DisplayReport.aspx");
    }
}
