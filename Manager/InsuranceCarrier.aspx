<%@ Page AutoEventWireup="true" CodeFile="InsuranceCarrier.aspx.cs" Inherits="InsuranceCarrier"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc3" %>

<%@ Register Src="~/UserControls/Phone.ascx" TagName="Phone" TagPrefix="uc2" %>

<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td>
             <uc1:GridBar ID="GridBar1" runat="server" HeaderText="Carriers Name 01" LinksList="" PagesList=""/>
          </td>
          </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../images/pix.gif" alt="" width="1" height="3" /></td>
        </tr>

      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblform">
        <tr>
          <th style="width: 43px">&nbsp;</th>
          <th style="width: 173px">Liability Insurance </th>
          <th style="width: 166px">Cargo Insurance </th>
          <th style="width: 167px">General Liability </th>
          <th style="width: 173px">Workman Comp. </th>
          <th style="width: 187px">Physical Insurance </th>
          </tr>
        <tr id="row">
          <td style="width: 43px"><strong><asp:Label ID="lblCompany" runat="server" Text="Company"/></strong></td>
          <td style="width: 173px"><asp:TextBox ID="txtCompanyLInsure" runat="Server" MaxLength="150"/></td>
          <td style="width: 166px"><asp:TextBox ID="txtCompanyCInsure" runat="Server" MaxLength="150"/></td>
          <td style="width: 167px"><asp:TextBox ID="txtCompanyGInsure" runat="Server" MaxLength="150"/></td>
          <td style="width: 173px"><asp:TextBox ID="txtCompanyWCInsure" runat="Server" MaxLength="150"/></td>
          <td style="width: 187px"><asp:TextBox ID="txtCompanyPInsure" runat="Server" MaxLength="150"/></td>
          </tr>
        <tr id="altrow">
          <td style="height: 20px; width: 43px;"><strong><asp:Label ID="lblLimit" runat="server" Text="Limit($)"/></strong></td>
          <td style="height: 20px; width: 173px;"><asp:TextBox ID="txtLimitLInsure" runat="Server" MaxLength="18"/>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtLimitLInsure"
                  Display="Dynamic" ErrorMessage="Please enter a valid Liability Limit." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
          <td style="width: 166px; height: 20px"><asp:TextBox ID="txtLimitCInsure" runat="Server" MaxLength="18"/>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtLimitCInsure"
                  Display="Dynamic" ErrorMessage="Please enter a valid  Cargo Insurance Limit."
                  ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
          <td style="height: 20px; width: 167px;"><asp:TextBox ID="txtLimitGInsure" runat="Server" MaxLength="18"/>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtLimitGInsure"
                  Display="Dynamic" ErrorMessage="Please enter a valid Genaral Liability Limit."
                  ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
          <td style="height: 20px; width: 173px;"><asp:TextBox ID="txtLimitWCInsure" runat="Server" MaxLength="18"/>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtLimitWCInsure"
                  Display="Dynamic" ErrorMessage="Please enter a valid Work Company Limit." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
          <td style="height: 20px; width: 187px;"><asp:TextBox ID="txtLimitPInsure" runat="Server" MaxLength="18"/>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                  ControlToValidate="txtLimitPInsure" Display="Dynamic" ErrorMessage="Please enter a valid Physical Insurance Limit."
                  ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
          </tr>
        <tr id="row">
          <td style="width: 43px; height: 20px;"><strong><asp:Label ID="lblPhone" runat="server" Text="Phone"/></strong></td>
          <td style="width: 173px; height: 20px;"><%--<asp:TextBox ID="txtPhoneLInsure" runat="Server" MaxLength="18"/>--%>
              <uc2:Phone ID="txtPhoneLInsure" runat="server" />
          </td>
          <td style="width: 166px; height: 20px;"><%--<asp:TextBox ID="txtPhoneCInsure" runat="Server" MaxLength="18"/>--%>
              <uc2:Phone ID="txtPhoneCInsure" runat="server" />
          </td>
          <td style="width: 167px; height: 20px;"><%--<asp:TextBox ID="txtPhoneGInsure" runat="Server" MaxLength="18"/>--%>
              <uc2:Phone ID="txtPhoneGInsure" runat="server" />
          </td>
          <td style="width: 173px; height: 20px;"><%--<asp:TextBox ID="txtPhoneWCInsure" runat="Server" MaxLength="18"/>--%>
              <uc2:Phone ID="txtPhoneWCInsure" runat="server" />
          </td>
          <td style="width: 187px; height: 20px;"><%--<asp:TextBox ID="txtPhonePInsure" runat="Server" MaxLength="18"/>--%>
          <uc2:Phone ID="txtPhonePInsure" runat="server" />
          </td>
        </tr>
        <tr id="altrow">
          <td style="width: 43px"><strong><asp:Label ID="lblExpDate" runat="server" Text="Expiration Date"/> </strong></td>
          <td style="width: 173px">
              <uc3:DatePicker ID="txtExpDateLInsure" runat="server" IsDefault="true" IsRequired="false" CountNextYears="2" CountPreviousYears="0" />
          </td>
          <td style="width: 166px">
              <uc3:DatePicker ID="txtExpDateCInsure" runat="server" IsDefault="true" IsRequired="false" CountNextYears="2" CountPreviousYears="0" />
          </td>
          <td style="width: 167px">
              <uc3:DatePicker ID="txtExpDateGInsure" runat="server" IsDefault="true" IsRequired="false" CountNextYears="2" CountPreviousYears="0" />
          </td>
          <td style="width: 173px">
              <uc3:DatePicker ID="txtExpDateWCInsure" runat="server" CountNextYears="2" CountPreviousYears="0"
                  IsDefault="true" IsRequired="false" />
          </td>
          <td style="width: 187px">
              <uc3:DatePicker ID="txtExpDatePInsure" runat="server" IsDefault="true" IsRequired="false" CountNextYears="2" CountPreviousYears="0" />
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../images/pix.gif" alt="" width="1" height="5" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
        <tr>
          <td align="right">
              &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btnstyle" OnClick="btnSave_Click"/>&nbsp;<asp:Button ID="btnCancle"
                  runat="server" CausesValidation="False" Text="Cancle" CssClass="btnstyle"
                  UseSubmitBehavior="False" OnClick="btnCancel_Click" />
          </td>        
        </tr>
      </table> 
    </td>
  </tr>
</table>

</asp:Content>

