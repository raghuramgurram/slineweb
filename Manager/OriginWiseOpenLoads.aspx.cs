using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class OriginWiseOpenLoads : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Origin Wise Open Loads";
        if (!IsPostBack)
            FillLists();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
            Session["SerchCriteria"] = null;
            Session["ReportParameters"] = null;
            Session["ReportName"] = null;
            Session["ReportParameters"] = ddlcompany.SelectedValue;
            Session["SerchCriteria"] = null;
            Session["ReportName"] = "GetOriginWiseOpenLoads";
            Response.Redirect("~/Manager/DisplayReport.aspx");
    }

    private void FillLists()
    {
        ddlcompany.Items.Clear();
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;        
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCompanyNames", new object[] { "O", officeLocationId });
        //SLine 2017 Enhancement for Office Location Ends
        if(ds.Tables.Count > 0)
        {
            ddlcompany.DataSource = ds.Tables[0];
            ddlcompany.DataTextField = "nvar_CompanyName";
            ddlcompany.DataValueField = "bint_CompanyId";
            ddlcompany.DataBind();
            ddlcompany.Items.Insert(0,(new ListItem("All Locations","0")));
        }        
        if(ds != null) { ds.Dispose(); ds = null; }
    }
    
}
