using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

public partial class DashBoard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CommonFunctions.InsertIntoInfoLog("DashBoard.aspx :: Page_Load :: Start");
        try
        {
            Session["TopHeaderText"] = Convert.ToString(Session["Role"]) + " Dashboard";
            Session["BackPage3"] = null;
            Session["BackPage4"] = null;
            Session["BackPage2"] = "~/Manager/DashBoard.aspx";
            Session["ViewLog"] = true;
            Response.AppendHeader("Refresh", "300");
            if (!Page.IsPostBack)
            {
                AssignListValues();
                if (dropStatus.Items.Count > 0)
                {
                    dropStatus.SelectedIndex = 0;
                    if (Session["LoadGridPageIndex"] == null)
                        Session["LoadGridPageIndex"] = 0;
                    GridFill();
                }
                Session["ShowStatusValueforUser"] = null;
            }
            FillLoadswithStatus();
            FillExportLoadswithStatus();
            FillCurrentWeekLoadsCount();

        }
        catch (Exception ex)
        {
            //CommonFunctions.SendEmail(ConfigurationSettings.AppSettings["SmtpUserName"], "tbnsridhar@savitr.com", "shiva994u@gmail.com", "",
            //"Error Details",
            //"error in Dash board for the role :" + Convert.ToString(Session["Role"]) + "While" + ex.Message, null);
        }
        CommonFunctions.InsertIntoInfoLog("DashBoard.aspx :: Page_Load :: END");
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        CommonFunctions.InsertIntoInfoLog("DashBoard.aspx :: Page_PreRender :: Start");
        if (dropStatus.Items.Count > 0)
        {
            if (Session["LoadStatus"] != null)
            {
                if (Convert.ToString(ViewState["LoadStatus"]) != Convert.ToString(Session["LoadStatus"]))
                {
                    dropStatus.SelectedIndex = dropStatus.Items.IndexOf(dropStatus.Items.FindByText(Convert.ToString(Session["LoadStatus"])));
                    GridFill();
                }
            }
            // dropStatus.Attributes.Add("onchange", "javascript:VisibleNewStatus('" + ddlNewStatus.ClientID + "','" + dropStatus.ClientID + "');");
            //btnShow.Attributes.Add("onClick", "javascript:VisibleNewStatus('" + ddlNewStatus.ClientID + "','" + dropStatus.ClientID + "');");
        }
        CommonFunctions.InsertIntoInfoLog("DashBoard.aspx :: Page_PreRender :: END");
    }

    private void FillCurrentWeekLoadsCount()
    {
        //SLine 2017 Enhancement for Office Location
        long officeLocationId = GetOfficeLocationId();
        //SLine 2017 Enhancement for Office Location added parameter to ExecuteDataset
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCurrentWeekAppointmentLoads", new object[] { officeLocationId });
        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count == 7)
        {
            lnkDay0.Text = ds.Tables[0].Rows[0][2] + " (" + ds.Tables[0].Rows[0][0] + ")" + "<br/>" + "DL (" + ds.Tables[0].Rows[0]["DL"].ToString() + ") " + "PU (" + ds.Tables[0].Rows[0]["PU"].ToString() + ") " + "DO (" + ds.Tables[0].Rows[0]["DO"].ToString() + ") " + "<br/>" + "ROUND (" + ds.Tables[0].Rows[0]["ROUND"].ToString() + ")";
            lnkDay1.Text = ds.Tables[0].Rows[1][2] + " (" + ds.Tables[0].Rows[1][0] + ")" + "<br/>" + "DL (" + ds.Tables[0].Rows[1]["DL"].ToString() + ") " + "PU (" + ds.Tables[0].Rows[1]["PU"].ToString() + ") " + "DO (" + ds.Tables[0].Rows[1]["DO"].ToString() + ") " + "<br/>" + "ROUND (" + ds.Tables[0].Rows[1]["ROUND"].ToString() + ")";
            lnkDay2.Text = ds.Tables[0].Rows[2][2] + " (" + ds.Tables[0].Rows[2][0] + ")" + "<br/>" + "DL (" + ds.Tables[0].Rows[2]["DL"].ToString() + ") " + "PU (" + ds.Tables[0].Rows[2]["PU"].ToString() + ") " + "DO (" + ds.Tables[0].Rows[2]["DO"].ToString() + ") " + "<br/>" + "ROUND (" + ds.Tables[0].Rows[2]["ROUND"].ToString() + ")";
            lnkDay3.Text = ds.Tables[0].Rows[3][2] + " (" + ds.Tables[0].Rows[3][0] + ")" + "<br/>" + "DL (" + ds.Tables[0].Rows[3]["DL"].ToString() + ") " + "PU (" + ds.Tables[0].Rows[3]["PU"].ToString() + ") " + "DO (" + ds.Tables[0].Rows[3]["DO"].ToString() + ") " + "<br/>" + "ROUND (" + ds.Tables[0].Rows[3]["ROUND"].ToString() + ")";
            lnkDay4.Text = ds.Tables[0].Rows[4][2] + " (" + ds.Tables[0].Rows[4][0] + ")" + "<br/>" + "DL (" + ds.Tables[0].Rows[4]["DL"].ToString() + ") " + "PU (" + ds.Tables[0].Rows[4]["PU"].ToString() + ") " + "DO (" + ds.Tables[0].Rows[4]["DO"].ToString() + ") " + "<br/>" + "ROUND (" + ds.Tables[0].Rows[4]["ROUND"].ToString() + ")";
            lnkDay5.Text = ds.Tables[0].Rows[5][2] + " (" + ds.Tables[0].Rows[5][0] + ")" + "<br/>" + "DL (" + ds.Tables[0].Rows[5]["DL"].ToString() + ") " + "PU (" + ds.Tables[0].Rows[5]["PU"].ToString() + ") " + "DO (" + ds.Tables[0].Rows[5]["DO"].ToString() + ") " + "<br/>" + "ROUND (" + ds.Tables[0].Rows[5]["ROUND"].ToString() + ")";
            lnkDay6.Text = ds.Tables[0].Rows[6][2] + " (" + ds.Tables[0].Rows[6][0] + ")" + "<br/>" + "DL (" + ds.Tables[0].Rows[6]["DL"].ToString() + ") " + "PU (" + ds.Tables[0].Rows[6]["PU"].ToString() + ") " + "DO (" + ds.Tables[0].Rows[6]["DO"].ToString() + ") " + "<br/>" + "ROUND (" + ds.Tables[0].Rows[6]["ROUND"].ToString() + ")";
            lnkDay0.CommandArgument = ds.Tables[0].Rows[0][2].ToString();
            lnkDay1.CommandArgument = ds.Tables[0].Rows[1][2].ToString();
            lnkDay2.CommandArgument = ds.Tables[0].Rows[2][2].ToString();
            lnkDay3.CommandArgument = ds.Tables[0].Rows[3][2].ToString();
            lnkDay4.CommandArgument = ds.Tables[0].Rows[4][2].ToString();
            lnkDay5.CommandArgument = ds.Tables[0].Rows[5][2].ToString();
            lnkDay6.CommandArgument = ds.Tables[0].Rows[6][2].ToString();
            lnkDay0.Enabled = ds.Tables[0].Rows[0][0].ToString() == "0" ? false : true;
            lnkDay1.Enabled = ds.Tables[0].Rows[1][0].ToString() == "0" ? false : true;
            lnkDay2.Enabled = ds.Tables[0].Rows[2][0].ToString() == "0" ? false : true;
            lnkDay3.Enabled = ds.Tables[0].Rows[3][0].ToString() == "0" ? false : true;
            lnkDay4.Enabled = ds.Tables[0].Rows[4][0].ToString() == "0" ? false : true;
            lnkDay5.Enabled = ds.Tables[0].Rows[5][0].ToString() == "0" ? false : true;
            lnkDay6.Enabled = ds.Tables[0].Rows[6][0].ToString() == "0" ? false : true;
        }
    }

    private void FillExportLoadswithStatus()
    {
        long officeLocationId = GetOfficeLocationId();
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_Get_ExportLoadCount", new object[] { officeLocationId });


        if (ds != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {

                if (Convert.ToInt32(ds.Tables[0].Rows[0]["ExportLoadCount"]) == 0)
                {
                    lnkIsExportLoads.Text = "Export Loads (" + ds.Tables[0].Rows[0]["ExportLoadCount"].ToString() + ")";
                    lnkIsExportLoads.ForeColor = System.Drawing.Color.Black;
                    lnkIsExportLoads.Font.Bold = true;
                    lnkIsExportLoads.Attributes.Add("style", "text-decoration:none;");
                    lnkIsExportLoads.Enabled = false;
                }
                else
                {
                    lnkIsExportLoads.Text = "<blink> Export Loads (" + ds.Tables[0].Rows[0]["ExportLoadCount"].ToString() + ")</blink>";
                    lnkIsExportLoads.ForeColor = System.Drawing.Color.Red;
                    lnkIsExportLoads.Font.Bold = true;
                    lnkIsExportLoads.Attributes.Add("style", "text-decoration:none;");
                }
            }
        }
    }

    private void FillLoadswithStatus()
    {
        ReloadAllStatus();
        //SLine 2017 Enhancement for Office Location
        long officeLocationId = GetOfficeLocationId();
        //SLine 2017 Enhancement for Office Location added parameter to ExecuteDataset
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "Get_LoadStatusWithCount", new object[] { officeLocationId });
        Dictionary<string, string> tempdic = new Dictionary<string, string>();

        // new sp to get the count for the today , tomorrow , Accessorial and TodayLFDLoads (Madhav)
        //SLine 2017 Enhancement for Office Location added parameter to ExecuteDataset
        DataSet todayandtomorrowcount = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "Get_LoadCountForTodayandTomorrow", new object[] { officeLocationId });
        // new sp to get the count for the today , tomorrow , Accessorial and TodayLFDLoads (Madhav)

        //today , tomorrow , Accessorial and TodayLFDLoads in the the links (Madhav) 
        if (todayandtomorrowcount != null)
        {
            if (todayandtomorrowcount.Tables.Count > 0 && todayandtomorrowcount.Tables[0].Rows.Count > 0)
            {

                //Today count in the the links (Madhav)               
                lnkSameDayLoads.Text = "Same Day Loads (" + todayandtomorrowcount.Tables[0].Rows[0]["TodayCount"].ToString() + ")";
                if (Convert.ToInt32(todayandtomorrowcount.Tables[0].Rows[0]["TodayCount"]) == 0)
                {
                    lnkSameDayLoads.Enabled = false;
                    lnkSameDayLoads.ForeColor = System.Drawing.Color.Black;
                    lnkSameDayLoads.Attributes.Add("style", "text-decoration:none;");
                }
                else
                {
                    lnkSameDayLoads.Attributes.Add("style", "text-decoration:none;");
                }
                //Today count in the the links (Madhav)

                //Tomorrow count in the the links (Madhav)
                lnkNextDayLoads.Text = "Next Day Loads (" + todayandtomorrowcount.Tables[0].Rows[0]["TomorrowCount"].ToString() + ")";
                if (Convert.ToInt32(todayandtomorrowcount.Tables[0].Rows[0]["TomorrowCount"]) == 0)
                {
                    lnkNextDayLoads.Enabled = false;
                    lnkNextDayLoads.ForeColor = System.Drawing.Color.Black;
                    lnkNextDayLoads.Attributes.Add("style", "text-decoration:none;");
                }
                else
                {
                    lnkNextDayLoads.Attributes.Add("style", "text-decoration:none;");
                }
                //Tomorrow count in the the links (Madhav)

                // Accessorial count in the links (Madhav)
                if (Convert.ToInt32(todayandtomorrowcount.Tables[0].Rows[0]["Accessorial"]) == 0)
                {
                    lnkAccessorialLoads.Text = "Accessorial charges loads (" + todayandtomorrowcount.Tables[0].Rows[0]["Accessorial"].ToString() + ")";
                    lnkAccessorialLoads.ForeColor = System.Drawing.Color.Black;
                    lnkAccessorialLoads.Font.Bold = true;
                    lnkAccessorialLoads.Attributes.Add("style", "text-decoration:none;");
                    lnkAccessorialLoads.Enabled = false;
                }
                else
                {
                    lnkAccessorialLoads.Text = "<blink> Accessorial Charges Loads (" + todayandtomorrowcount.Tables[0].Rows[0]["Accessorial"].ToString() + ")</blink>";
                    lnkAccessorialLoads.ForeColor = System.Drawing.Color.Red;
                    lnkAccessorialLoads.Font.Bold = true;
                    lnkAccessorialLoads.Attributes.Add("style", "text-decoration:none;");
                }
                // TodayLFDLoads count in the links (Madhav)

                // TodayLFDLoads count in the Links (Madhav)
                if (Convert.ToInt32(todayandtomorrowcount.Tables[0].Rows[0]["TodayLFDLoads"]) == 0)
                {
                    lnkltodayLFDLoads.ForeColor = System.Drawing.Color.Black;
                    lnkltodayLFDLoads.Enabled = false;
                    lnkltodayLFDLoads.Attributes.Add("style", "text-decoration:none;");
                    lnkltodayLFDLoads.Text = "Today�s LFD Loads (" + todayandtomorrowcount.Tables[0].Rows[0]["TodayLFDLoads"].ToString() + ")";
                }
                else
                {
                    lnkltodayLFDLoads.ForeColor = System.Drawing.Color.Coral;
                    lnkltodayLFDLoads.Text = "Today�s LFD Loads (" + todayandtomorrowcount.Tables[0].Rows[0]["TodayLFDLoads"].ToString() + ")";
                    lnkltodayLFDLoads.Enabled = true;
                    lnkltodayLFDLoads.Attributes.Add("style", "text-decoration:none;");
                }
                // TodayLFDLoads count in the Links (Madhav)
                if (Convert.ToInt32(todayandtomorrowcount.Tables[0].Rows[0]["LFDExpiredloads"]) == 0)
                {
                    lnkLFDExpired.ForeColor = System.Drawing.Color.Black;
                    lnkLFDExpired.Enabled = false;
                    lnkLFDExpired.Attributes.Add("style", "text-decoration:none;");
                    lnkLFDExpired.Text = "LFD Expired Loads (" + todayandtomorrowcount.Tables[0].Rows[0]["LFDExpiredloads"].ToString() + ")";
                }
                else
                {
                    lnkLFDExpired.ForeColor = System.Drawing.Color.Coral;
                    lnkLFDExpired.Text = "LFD Expired Loads (" + todayandtomorrowcount.Tables[0].Rows[0]["LFDExpiredloads"].ToString() + ")";
                    lnkLFDExpired.Enabled = true;
                    lnkLFDExpired.Attributes.Add("style", "text-decoration:none;");
                }

            }
        }
        //today , tomorrow , Accessorial and TodayLFDLoads count in the the links (Madhav)

        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                LoadStatus(ds.Tables[0].Rows[i][0].ToString(), ds.Tables[0].Rows[i][1].ToString());
            }
        }


    }

    //SLine 2017 Enhancement for Office Location Starts
    private long GetOfficeLocationId()
    {
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        else
        {
            if (Session["LocationDS"] != null)
            {
                DataSet locDs = Session["LocationDS"] as DataSet;
                officeLocationId = Convert.ToInt64(locDs.Tables[1].Rows[0][0]);
            }
        }
        return officeLocationId;
    }
    //SLine 2017 Enhancement for Office Location Ends

    private void LoadStatus(string status, string count)
    {
        if (status.ToLower().Trim() == "new")
        {
            lblNew.Text = "New (" + count + ")";
        }
        else if (status.ToLower().Trim() == "assigned")
        {
            lblAssigned.Text = "Assigned (" + count + ")";
            if (count != "0")
                lblAssigned.ForeColor = System.Drawing.Color.Green;
            else
                lblAssigned.ForeColor = System.Drawing.Color.Black;
        }
        else if (status.ToLower().Trim() == "pickedup")
        {
            lblPickedup.Text = "Pickedup (" + count + ")";
            if (count != "0")
                lblPickedup.ForeColor = System.Drawing.Color.Blue;
            else
                lblPickedup.ForeColor = System.Drawing.Color.Black;
        }
        else if (status.ToLower().Trim() == "loaded in yard")
        {
            lblLoadedinYard.Text = "Loaded in Yard (" + count + ")";
            if (count != "0")
                lblLoadedinYard.ForeColor = System.Drawing.Color.DeepPink;
            else
                lblLoadedinYard.ForeColor = System.Drawing.Color.Black;
        }
        else if (status.ToLower().Trim() == "en-route")
        {
            lblEnRoute.Text = "En-route (" + count + ")";
            if (count != "0")
                lblEnRoute.ForeColor = System.Drawing.Color.Teal;
            else
                lblEnRoute.ForeColor = System.Drawing.Color.Black;
        }
        else if (status.ToLower().Trim() == "drop in warehouse")
        {
            lblDropinWarehouse.Text = "Drop in Warehouse (" + count + ")";
            if (count != "0")
                lblDropinWarehouse.ForeColor = System.Drawing.Color.Brown;
            else
                lblDropinWarehouse.ForeColor = System.Drawing.Color.Black;
        }
        else if (status.ToLower().Trim() == "driver on waiting")
        {
            lblDriveronWaiting.Text = "Driver on Waiting (" + count + ")";
            if (count != "0")
            {
                lblDriveronWaiting.ForeColor = System.Drawing.Color.Orange;
                lblDriveronWaiting.Text = lblDriveronWaiting.Text.Insert(0, "<blink>");
                lblDriveronWaiting.Text = lblDriveronWaiting.Text.Insert(lblDriveronWaiting.Text.Length, "</blink>");
            }
            else
                lblDriveronWaiting.ForeColor = System.Drawing.Color.Black;
        }
        else if (status.ToLower().Trim() == "delivered")
        {
            lblDelivered.Text = "Delivered (" + count + ")";
            if (count != "0")
                lblDelivered.ForeColor = System.Drawing.Color.Lime;
            else
                lblDelivered.ForeColor = System.Drawing.Color.Black;
        }
        else if (status.ToLower().Trim() == "empty in yard")
        {
            lblEmptyinYard.Text = "Empty in Yard (" + count + ")";
            if (count != "0")
                lblEmptyinYard.ForeColor = System.Drawing.Color.Red;
            else
                lblEmptyinYard.ForeColor = System.Drawing.Color.Black;
        }
        else if (status.ToLower().Trim() == "terminated")
        {
            lblTerminated.Text = "Terminated (" + count + ")";
        }
        else if (status.ToLower().Trim() == "paperwork pending")
        {
            lblPaperworkpending.Text = "Paperwork Pending (" + count + ")";
        }
        else if (status.ToLower().Trim() == "closed")
        {
            //lblClosed.Text = "&nbsp;";
        }
        else if (status.ToLower().Trim() == "cancel")
        {
            // lblCancel.Text = "&nbsp;";
        }
        else if (status.ToLower().Trim() == "reached destination")
        {
            lblReachedDestination.Text = "Reached Destination (" + count + ")";
            if (count != "0")
                lblReachedDestination.ForeColor = System.Drawing.Color.Orange;
            else
                lblReachedDestination.ForeColor = System.Drawing.Color.Black;
        }
        else if (status.ToLower().Trim() == "load planner")
        {
            lblLoadPlanner.Text = "Load Planner (" + count + ")";
            if (count != "0")
            {
                lblLoadPlanner.ForeColor = System.Drawing.Color.Red;
                lblLoadPlanner.Text = lblLoadPlanner.Text.Insert(0, "<blink>");
                lblLoadPlanner.Text = lblLoadPlanner.Text.Insert(lblLoadPlanner.Text.Length, "</blink>");
            }
            else
                lblLoadPlanner.ForeColor = System.Drawing.Color.Black;
        }
        else if (status.ToLower().Trim() == "invoiceable")
        {
            lblInvoiceable.Text = "Invoiceable (" + count + ")";
            lblInvoiceable.Font.Bold = true;
            lblInvoiceable.Attributes.Add("style", "text-decoration:none;");
            if (count != "0")
            {
                lblInvoiceable.ForeColor = System.Drawing.Color.Red;
                lblInvoiceable.Enabled = true;
            }
            else
            {
                lblInvoiceable.ForeColor = System.Drawing.Color.Black;
                lblInvoiceable.Enabled = false;

            }
        }
        else if (status.ToLower().Trim() == "street turn")
        {
            lblStreetTurn.Text = "Street Turn (" + count + ")";
            lblStreetTurn.Font.Bold = true;
            lblStreetTurn.Attributes.Add("style", "text-decoration:none;");
            lblStreetTurn.ForeColor = System.Drawing.Color.DarkOrange;
            lblStreetTurn.Enabled = true;
        }
        else
        { }
    }

    private void ReloadAllStatus()
    {
        LoadStatus("new", "0");
        LoadStatus("assigned", "0");
        LoadStatus("pickedup", "0");
        LoadStatus("loaded in yard", "0");
        LoadStatus("en-route", "0");

        LoadStatus("drop in warehouse", "0");
        LoadStatus("driver on waiting", "0");
        LoadStatus("delivered", "0");
        LoadStatus("empty in yard", "0");
        LoadStatus("terminated", "0");

        LoadStatus("paperwork pending", "0");
        LoadStatus("closed", "0");
        LoadStatus("cancel", "0");
        LoadStatus("reached destination", "0");
        LoadStatus("load planner", "0");
        LoadStatus("invoiceable", "0");
        LoadStatus("street turn", "0");
    }

    private void AssignListValues()
    {
        if (string.Compare(Convert.ToString(Session["Role"]).Trim(), "manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
            string.Compare(Convert.ToString(Session["Role"]).Trim(), "staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            barManager.LinksList = "Add NewLoad";
            barManager.PagesList = "NewLoad.aspx";
        }
        else if (string.Compare(Convert.ToString(Session["Role"]).Trim(), "dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            barManager.LinksList = "";
            barManager.PagesList = "";
        }
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadStatusDetails");
        if (ds.Tables.Count > 0)
        {
            dropStatus.Items.Clear();
            dropStatus.DataSource = ds.Tables[0];
            dropStatus.DataTextField = "nvar_LoadStatusDesc";
            dropStatus.DataValueField = "bint_LoadStatusId";
            dropStatus.DataBind();
            if (dropStatus.Items.Count > 0)
            {
                dropStatus.Items.Insert(2, new ListItem("All Transit Loads", dropStatus.Items.Count + 1.ToString()));
                ListItem Item = dropStatus.Items.FindByText("Reached Destination");
                dropStatus.Items.Remove(dropStatus.Items.FindByText("Closed"));
                dropStatus.Items.Remove(dropStatus.Items.FindByText("Cancel"));
                dropStatus.Items.Remove(dropStatus.Items.FindByText("Invoiceable"));
                dropStatus.Items.Remove(Item);
                dropStatus.Items.Insert(7, Item);
                ListItem Item1 = dropStatus.Items.FindByText("Load Planner");
                dropStatus.Items.Remove(dropStatus.Items.FindByText("Load Planner"));
                dropStatus.Items.Insert(1, Item1);
                ListItem Item2 = dropStatus.Items.FindByText("Street Turn");
                dropStatus.Items.Remove(dropStatus.Items.FindByText("Street Turn"));
                dropStatus.Items.Insert(12, Item2);
                Item = null;
            }
            if (Session["LoadStatus"] == null)
                Session["LoadStatus"] = dropStatus.SelectedItem.Text;
        }
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    private void GridFill()
    {
        Session["IsNewStatus"] = null;
        //SLine 2017 Enhancement for Office Location
        long officeLocationId = GetOfficeLocationId();
        // Added a column in select for bit_IsHazmatLoad display in the loads
        switch (dropStatus.SelectedItem.Text.Trim())
        {
            case "New":
                #region NEW
                if (ddlNewStatus.SelectedItem == null)
                    ddlNewStatus.SelectedIndex = 0;
                //if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) != 0)
                //{
                switch (ddlNewStatus.SelectedItem.Text)
                {
                    case "LFD & Appt.Date":
                        #region New Loads With Lastfree date and delivery appt date.
                        grdManager.MainTableName = "eTn_Load";
                        grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                        //SLine 2016 Enhancements Added the Rail Container, Hot Shipments, Trip type column to the Query.
                        grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                        "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis1 as 'Chasis2', (Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end) + eTn_LoadType.nvar_LoadTypeDesc as 'Type'," +
                        "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                        "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Pickup as 'Pickup#', etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold, eTn_Load.bint_LoadStatusId as LoadStatusID,eTn_Load.nvar_TripType as TripType ";
                        //SLine 2016 Enhancements Added the Rail Container, Hot Shipments, Trip type column to the Query.

                        grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                            " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId";
                        if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        {
                            grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                                " and eTn_Load.date_LastFreeDate is not null and" +
                                "  eTn_Load.bint_LoadId in (select bint_LoadId from eTn_Destination where bint_LoadId =eTn_Load.bint_LoadId and eTn_Destination.date_DeliveryAppointmentDate is not null)" +
                                //SLine 2017 Enhancement for Office Location
                                " and eTn_Load.bint_OfficeLocationId=" + officeLocationId;
                            grdManager.DeleteVisible = true;
                            grdManager.LastColumnLinksList = "Load Planner;Cancel";
                            grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/DashBoard.aspx";
                        }
                        else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        {
                            grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                               " and eTn_Load.date_LastFreeDate is not null and" +
                               "  eTn_Load.bint_LoadId in (select bint_LoadId from eTn_Destination where bint_LoadId =eTn_Load.bint_LoadId and eTn_Destination.date_DeliveryAppointmentDate is not null)" +
                               //SLine 2017 Enhancement for Office Location
                               " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                            grdManager.DeleteVisible = false;
                            //SLine 2016
                            //OLD conditions commented below
                            //grdManager.LastColumnLinksList = "Dispatch";
                            //grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx";

                            //Sline 2016 --- Here adding the Spl view permission for the Dispatcher
                            if (Convert.ToBoolean(Session["LoadPlannerViewPermission"].ToString()))
                            {
                                grdManager.LastColumnLinksList = "Load Planner;";
                                grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/DashBoard.aspx";
                            }

                            //Else if the Dispatcher is not given the permission, it will be set to default view here.
                            else
                            {
                                grdManager.LastColumnLinksList = "Dispatch";
                                grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx";
                            }
                            //Sline 2016 --- Here adding the Spl view permission for the Dispatcher

                        }
                        else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        {
                            grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                                " and eTn_Load.date_LastFreeDate is not null and " +
                                "  eTn_Load.bint_LoadId in (select bint_LoadId from eTn_Destination where bint_LoadId =eTn_Load.bint_LoadId and eTn_Destination.date_DeliveryAppointmentDate is not null)" +
                                //SLine 2017 Enhancement for Office Location
                                " and eTn_Load.bint_OfficeLocationId=" + officeLocationId;
                            grdManager.DeleteVisible = false;
                            grdManager.LastColumnLinksList = "";
                        }

                        grdManager.SingleRowColumnsOrderByClause = " order by eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                        grdManager.VisibleColumnsList = "Load;Type;Customer;Origin;Pickup#;Pickup;Destination;Delivery Appt Date;Last Free Date";
                        grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination";
                        grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId";
                        grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                            "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+'<br/> To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                            "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.',case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' <br/>To<br/> '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Delivery Appt Date'";
                        grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                            "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId";
                        grdManager.DeleteTablesList = "eTn_Notes;eTn_UploadDocuments;eTn_TrackLoad;eTn_Receivables;eTn_Payables;eTn_AssignCarrier;eTn_AssignDriver;eTn_Origin;eTn_Destination;eTn_Load";
                        grdManager.DeleteTablePKList = "bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId";

                        //SLine 2016 Enhancements Added Order BY Clause
                        //grdManager.MultipleRowOrderByCluase = " ; Order By eTn_Destination.date_DeliveryAppointmentDate asc";
                        //SLine 2016 Enhancements Added Order BY Clause

                        //grdManager.DependentTablesList = "eTn_AssignCarrier;eTn_AssignDriver;eTn_Destination;eTn_Notes;eTn_Payables;eTn_Receivables;eTn_TrackLoad;eTn_UploadDocuments";
                        //grdManager.DependentTableKeysList = "bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId";                                
                        grdManager.LastLocationVisible = false;
                        grdManager.LoadStatus = "New";
                        barManager.HeaderText = "New Loads";
                        if (Session["LoadGridPageIndex"] == null)
                            Session["LoadGridPageIndex"] = 0;
                        grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                        break;
                    #endregion

                    case "LFD & No Appt.Date":
                        #region New Loads With Lastfree date and No delivery appt date.
                        grdManager.MainTableName = "eTn_Load";
                        grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                        //SLine 2016 Enhancements Added the Rail Container and Hot Shipment, Trip Type column to the Query.
                        grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                        "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis1 as 'Chasis2', (Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end) +  eTn_LoadType.nvar_LoadTypeDesc as 'Type'," +
                        "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                        "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Pickup as 'Pickup#',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as 'HazmatLoad', eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID, eTn_Load.nvar_TripType as TripType";
                        //SLine 2016 Enhancements Added the Rail Container and Hot Shipment, Trip Type column to the Query.

                        grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                            " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId";
                        if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        {
                            grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                                " and eTn_Load.date_LastFreeDate is not null and " +
                                "  eTn_Load.bint_LoadId not in (select bint_LoadId from eTn_Destination where bint_LoadId =eTn_Load.bint_LoadId and eTn_Destination.date_DeliveryAppointmentDate is not null)" +
                                //SLine 2017 Enhancement for Office Location
                                " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                            grdManager.DeleteVisible = true;
                            grdManager.LastColumnLinksList = "Load Planner;Cancel";
                            grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/DashBoard.aspx";
                        }
                        else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        {
                            grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                                " and eTn_Load.date_LastFreeDate is not null and " +
                                "  eTn_Load.bint_LoadId not in (select bint_LoadId from eTn_Destination where bint_LoadId =eTn_Load.bint_LoadId and eTn_Destination.date_DeliveryAppointmentDate is not null)" +
                                //SLine 2017 Enhancement for Office Location
                                " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                            grdManager.DeleteVisible = false;

                            //SLine 2016
                            //OLD conditions commented below
                            //grdManager.LastColumnLinksList = "Dispatch";
                            //grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx";

                            //Sline 2016 --- Here adding the Spl view permission for the Dispatcher
                            if (Convert.ToBoolean(Session["LoadPlannerViewPermission"].ToString()))
                            {
                                grdManager.LastColumnLinksList = "Load Planner;";
                                grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/DashBoard.aspx";
                            }

                            //Else if the Dispatcher is not given the permission, it will be set to default view here.
                            else
                            {
                                grdManager.LastColumnLinksList = "Dispatch";
                                grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx";
                            }
                            //Sline 2016 --- Here adding the Spl view permission for the Dispatcher
                        }
                        else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        {
                            grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                                " and eTn_Load.date_LastFreeDate is not null and " +
                                "  eTn_Load.bint_LoadId not in (select bint_LoadId from eTn_Destination where bint_LoadId =eTn_Load.bint_LoadId and eTn_Destination.date_DeliveryAppointmentDate is not null)" +
                                //SLine 2017 Enhancement for Office Location
                                " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                            grdManager.DeleteVisible = false;
                            grdManager.LastColumnLinksList = "";
                        }

                        grdManager.SingleRowColumnsOrderByClause = " order by eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                        grdManager.VisibleColumnsList = "Load;Type;Customer;Origin;Pickup#;Pickup;Destination;Delivery Appt Date;Last Free Date";
                        grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination";
                        grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId";
                        grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                            "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+'<br/> To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                            "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.',case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' <br/>To<br/> '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Delivery Appt Date'";
                        grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                            "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId";
                        grdManager.DeleteTablesList = "eTn_Notes;eTn_UploadDocuments;eTn_TrackLoad;eTn_Receivables;eTn_Payables;eTn_AssignCarrier;eTn_AssignDriver;eTn_Origin;eTn_Destination;eTn_Load";
                        grdManager.DeleteTablePKList = "bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId";

                        //SLine 2016 Enhancements Added Order BY Clause
                        //grdManager.MultipleRowOrderByCluase = " ; Order By eTn_Destination.date_DeliveryAppointmentDate";
                        //SLine 2016 Enhancements Added Order BY Clause

                        //grdManager.DependentTablesList = "eTn_AssignCarrier;eTn_AssignDriver;eTn_Destination;eTn_Notes;eTn_Payables;eTn_Receivables;eTn_TrackLoad;eTn_UploadDocuments";
                        //grdManager.DependentTableKeysList = "bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId";                                
                        grdManager.LastLocationVisible = false;
                        grdManager.LoadStatus = "New";
                        barManager.HeaderText = "New Loads";
                        if (Session["LoadGridPageIndex"] == null)
                            Session["LoadGridPageIndex"] = 0;
                        grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                        break;
                    #endregion

                    case "No LFD & Appt.Date":
                        Session["IsNewStatus"] = true;
                        #region New Loads With No Lastfree date and delivery appt date.
                        grdManager.MainTableName = "eTn_Load";
                        grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                        //SLine 2016 Enhancements Added the Rail Container And Hot Shipment, Trip type column to the Query.
                        grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                        "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis1 as 'Chasis2', (Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end) + eTn_LoadType.nvar_LoadTypeDesc as 'Type'," +
                        "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                        "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Pickup as 'Pickup#',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad ,eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID, eTn_Load.nvar_TripType as TripType";
                        //SLine 2016 Enhancements Added the Rail Container and Hot Shipment, Trip Type column to the Query.

                        grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                            " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId";
                        if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        {
                            grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                                " and eTn_Load.date_LastFreeDate is null and " +
                               "  eTn_Load.bint_LoadId in (select bint_LoadId from eTn_Destination where bint_LoadId =eTn_Load.bint_LoadId and eTn_Destination.date_DeliveryAppointmentDate is not null)" +
                                //SLine 2017 Enhancement for Office Location
                                " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                            grdManager.DeleteVisible = true;
                            grdManager.LastColumnLinksList = "Load Planner;Cancel";
                            grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/DashBoard.aspx";
                        }
                        else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        {
                            grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                                " and eTn_Load.date_LastFreeDate is null and " +
                               "  eTn_Load.bint_LoadId in (select bint_LoadId from eTn_Destination where bint_LoadId =eTn_Load.bint_LoadId and eTn_Destination.date_DeliveryAppointmentDate is not null)" +
                                //SLine 2017 Enhancement for Office Location
                                " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                            grdManager.DeleteVisible = false;

                            //SLine 2016
                            //OLD conditions commented below
                            //grdManager.LastColumnLinksList = "Dispatch";
                            //grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx";

                            //Sline 2016 --- Here adding the Spl view permission for the Dispatcher
                            if (Convert.ToBoolean(Session["LoadPlannerViewPermission"].ToString()))
                            {
                                grdManager.LastColumnLinksList = "Load Planner;";
                                grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/DashBoard.aspx";
                            }

                            //Else if the Dispatcher is not given the permission, it will be set to default view here.
                            else
                            {
                                grdManager.LastColumnLinksList = "Dispatch";
                                grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx";
                            }
                            //Sline 2016 --- Here adding the Spl view permission for the Dispatcher
                        }
                        else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        {
                            grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                                " and eTn_Load.date_LastFreeDate is null and " +
                                "  eTn_Load.bint_LoadId in (select bint_LoadId from eTn_Destination where bint_LoadId =eTn_Load.bint_LoadId and eTn_Destination.date_DeliveryAppointmentDate is not null)" +
                                //SLine 2017 Enhancement for Office Location
                                " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                            grdManager.DeleteVisible = false;
                            grdManager.LastColumnLinksList = "";
                        }

                        grdManager.SingleRowColumnsOrderByClause = " order by eTn_Load.date_LastFreeDate";
                        grdManager.VisibleColumnsList = "Load;Type;Customer;Origin;Pickup#;Pickup;Destination;Delivery Appt Date;Last Free Date";
                        grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination";
                        grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId";
                        grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                            "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+'<br/> To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                            "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.',case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' <br/>To<br/> '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Delivery Appt Date'";
                        grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                            "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId";
                        grdManager.DeleteTablesList = "eTn_Notes;eTn_UploadDocuments;eTn_TrackLoad;eTn_Receivables;eTn_Payables;eTn_AssignCarrier;eTn_AssignDriver;eTn_Origin;eTn_Destination;eTn_Load";
                        grdManager.DeleteTablePKList = "bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId";

                        //SLine 2016 Enhancements Added Order BY Clause
                        //grdManager.MultipleRowOrderByCluase = " ; Order By eTn_Destination.date_DeliveryAppointmentDate";
                        //SLine 2016 Enhancements Added Order BY Clause

                        //grdManager.DependentTablesList = "eTn_AssignCarrier;eTn_AssignDriver;eTn_Destination;eTn_Notes;eTn_Payables;eTn_Receivables;eTn_TrackLoad;eTn_UploadDocuments";
                        //grdManager.DependentTableKeysList = "bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId";                                
                        grdManager.LastLocationVisible = false;
                        grdManager.LoadStatus = "New";
                        barManager.HeaderText = "New Loads";
                        if (Session["LoadGridPageIndex"] == null)
                            Session["LoadGridPageIndex"] = 0;
                        grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                        break;
                    #endregion

                    case "No LFD & No Appt.Date":
                        #region New Loads With No Lastfree date and No delivery appt date.
                        grdManager.MainTableName = "eTn_Load";
                        grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                        //SLine 2016 Enhancements Added the Rail Container and Hot Shipment, Trip Type column to the Query.
                        grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                        "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis1 as 'Chasis2', (Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end)+eTn_LoadType.nvar_LoadTypeDesc as 'Type'," +
                        "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                        "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Pickup as 'Pickup#', etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID, eTn_Load.nvar_TripType as TripType";
                        //SLine 2016 Enhancements Added the Rail Container and Hot Shipment, Trip Type to the Query.

                        grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                            " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId";
                        if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        {
                            grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                                " and eTn_Load.date_LastFreeDate is null and " +
                                "  eTn_Load.bint_LoadId not in (select bint_LoadId from eTn_Destination where bint_LoadId =eTn_Load.bint_LoadId and eTn_Destination.date_DeliveryAppointmentDate is not null)" +
                                //SLine 2017 Enhancement for Office Location
                                " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                            grdManager.DeleteVisible = true;
                            grdManager.LastColumnLinksList = "Load Planner;Cancel";
                            grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/DashBoard.aspx";
                        }
                        else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        {
                            grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                                " and eTn_Load.date_LastFreeDate is null and " +
                                "  eTn_Load.bint_LoadId not in (select bint_LoadId from eTn_Destination where bint_LoadId =eTn_Load.bint_LoadId and eTn_Destination.date_DeliveryAppointmentDate is not null)" +
                                //SLine 2017 Enhancement for Office Location
                                " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                            grdManager.DeleteVisible = false;

                            //SLine 2016
                            //OLD conditions commented below
                            //grdManager.LastColumnLinksList = "Dispatch";
                            //grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx";


                            //Sline 2016 --- Here adding the Spl view permission for the Dispatcher
                            if (Convert.ToBoolean(Session["LoadPlannerViewPermission"].ToString()))
                            {
                                grdManager.LastColumnLinksList = "Load Planner;";
                                grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/DashBoard.aspx";
                            }

                            //Else if the Dispatcher is not given the permission, it will be set to default view here.
                            else
                            {
                                grdManager.LastColumnLinksList = "Dispatch";
                                grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx";
                            }
                            //Sline 2016 --- Here adding the Spl view permission for the Dispatcher
                        }
                        else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                        {
                            grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                                " and eTn_Load.date_LastFreeDate is null and " +
                                "  eTn_Load.bint_LoadId not in (select bint_LoadId from eTn_Destination where bint_LoadId =eTn_Load.bint_LoadId and eTn_Destination.date_DeliveryAppointmentDate is not null)" +
                                //SLine 2017 Enhancement for Office Location
                                " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                            grdManager.DeleteVisible = false;
                            grdManager.LastColumnLinksList = "";
                        }

                        grdManager.SingleRowColumnsOrderByClause = " order by eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                        grdManager.VisibleColumnsList = "Load;Type;Customer;Origin;Pickup#;Pickup;Destination;Delivery Appt Date;Last Free Date";
                        grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination";
                        grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId";
                        grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                            "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+'<br/> To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                            "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.',case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' <br/>To<br/> '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Delivery Appt Date'";
                        grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                            "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId";
                        grdManager.DeleteTablesList = "eTn_Notes;eTn_UploadDocuments;eTn_TrackLoad;eTn_Receivables;eTn_Payables;eTn_AssignCarrier;eTn_AssignDriver;eTn_Origin;eTn_Destination;eTn_Load";
                        grdManager.DeleteTablePKList = "bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId";

                        //SLine 2016 Enhancements Added Order BY Clause
                        //grdManager.MultipleRowOrderByCluase = " ; Order By eTn_Destination.date_DeliveryAppointmentDate";
                        //SLine 2016 Enhancements Added Order BY Clause

                        //grdManager.DependentTablesList = "eTn_AssignCarrier;eTn_AssignDriver;eTn_Destination;eTn_Notes;eTn_Payables;eTn_Receivables;eTn_TrackLoad;eTn_UploadDocuments";
                        //grdManager.DependentTableKeysList = "bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId";                                
                        grdManager.LastLocationVisible = false;
                        grdManager.LoadStatus = "New";
                        barManager.HeaderText = "New Loads";
                        if (Session["LoadGridPageIndex"] == null)
                            Session["LoadGridPageIndex"] = 0;
                        grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                        #endregion
                        break;
                }
                #region Commented by Raghu for Dispatcher
                //}
                //else
                //{
                //    dropStatus.AutoPostBack = false;
                //    ddlNewStatus.Visible = false;
                //    lblNewStatus.Visible = false;
                //    #region New Loads With Lastfree date and delivery appt date.
                //    grdManager.MainTableName = "eTn_Load";
                //    grdManager.MainTablePK = "eTn_Load.bint_LoadId";
                //    grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                //    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_LoadType.nvar_LoadTypeDesc as 'Type'," +
                //    "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                //    "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Pickup as 'Pickup#',eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bint_LoadStatusId as LoadStatusID"; 
                //    grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                //        " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId";
                //    if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                //    {
                //        grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                //            " and eTn_Load.date_LastFreeDate is not null and" +
                //            "  eTn_Load.bint_LoadId in (select bint_LoadId from eTn_Destination where bint_LoadId =eTn_Load.bint_LoadId and eTn_Destination.date_DeliveryAppointmentDate is not null)";
                //        grdManager.DeleteVisible = true;
                //        grdManager.LastColumnLinksList = "Load Planner;Cancel";
                //        grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/DashBoard.aspx";
                //    }
                //    else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                //    {
                //        grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                //            " and eTn_Load.date_LastFreeDate is not null and " +
                //            "  eTn_Load.bint_LoadId in (select bint_LoadId from eTn_Destination where bint_LoadId =eTn_Load.bint_LoadId and eTn_Destination.date_DeliveryAppointmentDate is not null)";
                //        grdManager.DeleteVisible = false;
                //        grdManager.LastColumnLinksList = "Dispatch";
                //        grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx";
                //    }
                //    else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                //    {
                //        grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                //            " and eTn_Load.date_LastFreeDate is not null and " +
                //            "  eTn_Load.bint_LoadId in (select bint_LoadId from eTn_Destination where bint_LoadId =eTn_Load.bint_LoadId and eTn_Destination.date_DeliveryAppointmentDate is not null)";
                //        grdManager.DeleteVisible = false;
                //        grdManager.LastColumnLinksList = "";
                //    }

                //    grdManager.SingleRowColumnsOrderByClause = " order by eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                //    grdManager.VisibleColumnsList = "Load;Type;Customer;Origin;Pickup#;Pickup;Destination;Delivery Appt Date;Last Free Date";
                //    grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination";
                //    grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId";
                //    grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                //        "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+'<br/> To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                //        "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.',case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' <br/>To<br/> '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Delivery Appt Date'";
                //    grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                //        "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId";
                //    grdManager.DeleteTablesList = "eTn_Notes;eTn_UploadDocuments;eTn_TrackLoad;eTn_Receivables;eTn_Payables;eTn_AssignCarrier;eTn_AssignDriver;eTn_Origin;eTn_Destination;eTn_Load";
                //    grdManager.DeleteTablePKList = "bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId";
                //    //grdManager.MultipleRowOrderByCluase = " ; Order By eTn_Destination.date_DeliveryAppointmentDate";
                //    //grdManager.DependentTablesList = "eTn_AssignCarrier;eTn_AssignDriver;eTn_Destination;eTn_Notes;eTn_Payables;eTn_Receivables;eTn_TrackLoad;eTn_UploadDocuments";
                //    //grdManager.DependentTableKeysList = "bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId";                                
                //    grdManager.LastLocationVisible = false;
                //    grdManager.LoadStatus = "New";
                //    barManager.HeaderText = "New Loads";
                //    if (Session["LoadGridPageIndex"] == null)
                //        Session["LoadGridPageIndex"] = 0;
                //    grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                //    break;
                //    #endregion
                //}
                #endregion
                #endregion
                break;

            case "Cancel":
                ddlNewStatus.Visible = false;
                lblNewStatus.Visible = false;
                #region Cancel Load Details
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added Hot Shipments, and Rail column to the Query.
                //eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2'
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', (Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end) + eTn_LoadType.nvar_LoadTypeDesc as 'Type' ," +
                "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Pickup as 'Pickup#', etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold, eTn_Load.bint_LoadStatusId as LoadStatusID ";
                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                    " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId ";
                //SLine 2016 Enhancements Added the Hot Shipments and Rail column to the Query.


                if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                        //SLine 2017 Enhancement for Office Location
                        " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                    grdManager.DeleteVisible = true;
                    grdManager.LastColumnLinksList = "";
                    grdManager.LastColumnPagesList = "";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                        //SLine 2017 Enhancement for Office Location
                        " and eTn_Load.bint_OfficeLocationId = " + officeLocationId + " and " +
                        " len(eTn_Load.date_LastFreeDate)>0 and " +
                        "((select Count(date_DeliveryAppointmentDate) from eTn_Destination where bint_LoadId =eTn_Load.bint_LoadId and len(date_DeliveryAppointmentDate)>0)>0)";
                    grdManager.DeleteVisible = false;
                    grdManager.LastColumnLinksList = "";
                    grdManager.LastColumnPagesList = "";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                        //SLine 2017 Enhancement for Office Location
                        " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                    grdManager.DeleteVisible = false;
                }

                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Type;Customer;Origin;Pickup#;Pickup;Destination;Delivery Appt Date;Last Free Date";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId";
                grdManager.DeleteTablesList = "eTn_Notes;eTn_UploadDocuments;eTn_TrackLoad;eTn_Receivables;eTn_Payables;eTn_AssignCarrier;eTn_AssignDriver;eTn_Origin;eTn_Destination;eTn_Load";
                grdManager.DeleteTablePKList = "bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId";
                //grdManager.DependentTablesList = "eTn_AssignCarrier;eTn_AssignDriver;eTn_Destination;eTn_Notes;eTn_Payables;eTn_Receivables;eTn_TrackLoad;eTn_UploadDocuments";
                //grdManager.DependentTableKeysList = "bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId";                                
                grdManager.LastLocationVisible = false;
                grdManager.LoadStatus = "New";
                barManager.HeaderText = "New Loads";
                if (Session["LoadGridPageIndex"] == null)
                    Session["LoadGridPageIndex"] = 0;
                grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                break;
            #endregion

            case "Load Planner":
                ddlNewStatus.Visible = false;
                lblNewStatus.Visible = false;
                #region Load Planner
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Hot Shipments, and Rail column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end as 'Type'," +
                    "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                    "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Booking as 'Booking#';nvar_Pickup as 'Pickup#' , etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID,eTn_Load.nvar_TripType as TripType ";
                //SLine 2016 Enhancements Added the Hot Shipments and Rail column to the Query.

                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                    //SLine 2017 Enhancement for Office Location
                    " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Customer;Driver/Carrier;Origin;Pickup#;Pickup;Destination;Delivery Appt.;Last Free Date;Booking#";
                grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
                grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";

                grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                    "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                    CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");

                grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver') ";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To<br/> '+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.';eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Dispatch;Cancel";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/DashBoard.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Dispatch";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "";
                }
                grdManager.DeleteVisible = false;
                grdManager.LastLocationVisible = true;
                grdManager.LoadStatus = "Load Planner";
                barManager.HeaderText = "Load Planner";
                if (Session["LoadGridPageIndex"] == null)
                    Session["LoadGridPageIndex"] = 0;
                grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                break;
            #endregion

            case "Assigned":
                ddlNewStatus.Visible = false;
                lblNewStatus.Visible = false;
                #region Assigned
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Hot Shipments, and Rail column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end 'Type'," +
                    "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                    "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Booking as 'Booking#';nvar_Pickup as 'Pickup#',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID,eTn_Load.nvar_TripType as TripType ";
                //SLine 2016 Enhancements Added the Hot Shipments and Rail column to the Query.

                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                    //SLine 2017 Enhancement for Office Location
                    " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Customer;Driver/Carrier;Origin;Pickup#;Pickup;Destination;Delivery Appt Date;Last Free Date;Booking#;Last Location";
                grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
                grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";
                grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                    "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                    CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");
                grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                    "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver') ";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To<br/> '+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.';eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;
                grdManager.LastLocationVisible = true;
                grdManager.LoadStatus = "Assigned";
                barManager.HeaderText = "Assigned Loads";
                if (Session["LoadGridPageIndex"] == null)
                    Session["LoadGridPageIndex"] = 0;
                grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                break;
            #endregion

            case "Pickedup":
                ddlNewStatus.Visible = false;
                lblNewStatus.Visible = false;
                #region Pickedup
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Hot Shipments and Rail column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end as 'Type'," +
                    "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                    "'' as 'Customer' ,etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID,eTn_Load.nvar_TripType as TripType," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date");
                //SLine 2016 Enhancements Added the Hot Shipments and Rail column to the Query.

                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                    //SLine 2017 Enhancement for Office Location
                    " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Customer;Driver/Carrier;Origin;Destination;Last Location;Date & Time;Comments";
                grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
                grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";
                grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                    "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                    CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");
                grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                    "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver') ";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To<br/> '+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.';eTn_TrackLoad.date_CreateDate as 'Date & Time'^eTn_TrackLoad.nvar_Notes as 'Comments'^eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;
                grdManager.LastLocationVisible = true;
                grdManager.LoadStatus = "Pickedup";
                barManager.HeaderText = "Pickedup Loads";
                if (Session["LoadGridPageIndex"] == null)
                    Session["LoadGridPageIndex"] = 0;
                grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                break;
            #endregion

            case "Loaded in Yard":
                ddlNewStatus.Visible = false;
                lblNewStatus.Visible = false;
                #region Loaded in Yard
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Hot Shipments column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end as 'Type'," +
                    "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                    "'' as 'Customer', etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID, eTn_Load.nvar_TripType as TripType," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date");
                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId ";
                //SLine 2016 Enhancements Added the Hot Shipments column to the Query.

                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                    //SLine 2017 Enhancement for Office Location
                    " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Customer;Appt. Date;Driver/Carrier;Origin;Destination;Days In Yard;Last Location;Date & Time;Comments";
                grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
                grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";
                grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                    "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                    CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");
                grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                    "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver')";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To<br/> '+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Appt. Date';eTn_TrackLoad.date_CreateDate as 'Date & Time'^eTn_TrackLoad.nvar_Notes as 'Comments'^DateDiff(dd,isnull(eTn_TrackLoad.date_CreateDate,getdate()),getdate()) as 'Days In Yard'^eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Load Planner";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    //Sline 2016
                    //OLD conditions commented below
                    //grdManager.LastColumnLinksList = "";
                    //grdManager.LastColumnPagesList = "";

                    //Sline 2016 --- Here adding the Spl view permission for the Dispatcher
                    if (Convert.ToBoolean(Session["LoadPlannerViewPermission"].ToString()))
                    {
                        grdManager.LastColumnLinksList = "Load Planner";
                        grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx";
                    }

                    //Else if the Dispatcher is not given the permission, it will be set to default view here.
                    else
                    {
                        grdManager.LastColumnLinksList = "";
                        grdManager.LastColumnPagesList = "";
                    }
                    //Sline 2016 --- Here adding the Spl view permission for the Dispatcher
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "";
                    grdManager.LastColumnPagesList = "";
                }
                grdManager.DeleteVisible = false;
                grdManager.LastLocationVisible = true;
                grdManager.LoadStatus = "Loaded in Yard";
                barManager.HeaderText = "Loaded in Yard Loads";
                if (Session["LoadGridPageIndex"] == null)
                    Session["LoadGridPageIndex"] = 0;
                grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                break;
            #endregion

            case "En-route":
                ddlNewStatus.Visible = false;
                lblNewStatus.Visible = false;
                #region En-route
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Hot Shipments and Rail column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end as 'Type'," +
                    "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                    "'' as 'Customer',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID, eTn_Load.nvar_TripType as TripType, " + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date");
                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId ";
                //SLine 2016 Enhancements Added the Hot Shipments and Rail Column column to the Query.

                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                    //SLine 2017 Enhancement for Office Location
                    " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Customer;Origin;Destination;Appt. Date;Last Location;Date & Time;Comments";
                //grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
                //grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";
                //grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                //    "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                //    CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");
                //grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                //    "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId ";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To<br/> '+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Appt. Date';eTn_TrackLoad.date_CreateDate as 'Date & Time'^eTn_TrackLoad.nvar_Notes as 'Comments'^eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;
                grdManager.LastLocationVisible = true;
                grdManager.LoadStatus = "En-route";
                barManager.HeaderText = "En-route Loads";
                if (Session["LoadGridPageIndex"] == null)
                    Session["LoadGridPageIndex"] = 0;
                grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                break;
            #endregion

            case "Drop in Warehouse":
                ddlNewStatus.Visible = false;
                lblNewStatus.Visible = false;
                #region Drop in Warehouse
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Hot Shipments and Rail column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end as 'Type'," +
                    "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                    "'' as 'Customer',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID, eTn_Load.nvar_TripType as TripType, " + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date");
                //SLine 2016 Enhancements Added the Hot Shipments and Rail column to the Query.

                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                    //SLine 2017 Enhancement for Office Location
                    " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Customer;Driver/Carrier;Origin;Destination;No. of Days;Last Location;Date & Time;Comments";

                grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
                grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";
                grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                    "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                    CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");
                grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                    "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver') ";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To<br/> '+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Appt. Date';isnull(datediff(dd,isnull((select top 1 t.date_CreateDate from eTn_TrackLoad t where t.bint_LoadStatusId = 6 and t.bint_LoadId = eTn_TrackLoad.bint_LoadId order by t.date_CreateDate desc),getdate()),getdate()),0) as 'No. of Days'^eTn_TrackLoad.date_CreateDate as 'Date & Time'^eTn_TrackLoad.nvar_Notes as 'Comments'^eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;
                grdManager.LastLocationVisible = true;
                grdManager.LoadStatus = "Drop in Warehouse";
                barManager.HeaderText = "Drop in Warehouse Loads";
                if (Session["LoadGridPageIndex"] == null)
                    Session["LoadGridPageIndex"] = 0;
                grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                break;
            #endregion

            case "Driver on Waiting":
                ddlNewStatus.Visible = false;
                lblNewStatus.Visible = false;
                #region Driver on Waiting
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Hot Shipments and Rail column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end as 'Type'," +
                    "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                    "'' as 'Customer',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID, eTn_Load.nvar_TripType as TripType, " + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date");
                //SLine 2016 Enhancements Added the Hot Shipments and Rail column to the Query.

                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                    //SLine 2017 Enhancement for Office Location
                    " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Customer;Driver/Carrier;Origin;Destination;Last Location;Date & Time;Comments";
                grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
                grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";
                grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                    "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                    CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");
                grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                    "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver') ";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To<br/> '+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Appt. Date';eTn_TrackLoad.date_CreateDate as 'Date & Time'^eTn_TrackLoad.nvar_Notes as 'Comments'^eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;
                grdManager.LastLocationVisible = true;
                grdManager.LoadStatus = "Driver on Waiting";
                barManager.HeaderText = "Driver on Waiting Loads";
                if (Session["LoadGridPageIndex"] == null)
                    Session["LoadGridPageIndex"] = 0;
                grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                break;
            #endregion

            case "Delivered":
                ddlNewStatus.Visible = false;
                lblNewStatus.Visible = false;
                #region Delivered
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Hot Shipments and Rail column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end as 'Type'," +
                    "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                    "'' as 'Customer',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID, eTn_Load.nvar_TripType as TripType, " + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date");
                //SLine 2016 Enhancements Added the Hot Shipments and Rail column to the Query.

                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                    //SLine 2017 Enhancement for Office Location
                    " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Customer;Driver/Carrier;Origin;Destination;Delivered Date;Last Location;Date & Time;Comments";
                grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
                grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";

                grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                    "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                    CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");

                grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                    "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver') ";

                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Appt. Date';eTn_TrackLoad.date_CreateDate as 'Delivered Date'^eTn_TrackLoad.date_CreateDate as 'Date & Time'^eTn_TrackLoad.nvar_Notes as 'Comments'^eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;
                grdManager.LastLocationVisible = true;
                grdManager.LoadStatus = "Delivered";
                barManager.HeaderText = "Delivered Loads";
                if (Session["LoadGridPageIndex"] == null)
                    Session["LoadGridPageIndex"] = 0;
                grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                break;
            #endregion

            case "Empty in Yard":
                ddlNewStatus.Visible = false;
                lblNewStatus.Visible = false;
                #region Empty in Yard
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Hot Shipments and Rail column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end as 'Type'," +
                    "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                    "'' as 'Customer',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID, eTn_Load.nvar_TripType as TripType, " + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date");
                //SLine 2016 Enhancements Added the Hot Shipments and Rail column to the Query.

                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                    //SLine 2017 Enhancement for Office Location
                    " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Customer;Driver/Carrier;Origin;Destination;Days In Yard;Last Location;Date & Time;Comments";
                grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
                grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";
                grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                    "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                    CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");
                grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                    "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver') ";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Appt. Date';eTn_TrackLoad.date_CreateDate as 'Delivered Date'^eTn_TrackLoad.date_CreateDate as 'Date & Time'^eTn_TrackLoad.nvar_Notes as 'Comments'^DateDiff(dd,isnull(eTn_TrackLoad.date_CreateDate,getdate()),getdate()) as 'Days In Yard'^eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;
                grdManager.LastLocationVisible = true;
                grdManager.LoadStatus = "Empty In Yard";
                barManager.HeaderText = "Empty In Yard Loads";
                if (Session["LoadGridPageIndex"] == null)
                    Session["LoadGridPageIndex"] = 0;
                grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                break;
            #endregion

            case "Terminated":
                ddlNewStatus.Visible = false;
                lblNewStatus.Visible = false;
                #region Terminated
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Hot Shipments and Rail column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end as 'Type'," +
                    "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                    "'' as 'Customer',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID, eTn_Load.nvar_TripType as TripType, " + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date");
                //SLine 2016 Enhancements Added the Hot Shipments and Rail column to the Query.

                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                    //SLine 2017 Enhancement for Office Location
                    " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Customer;Origin;Destination;Last Location;Date & Time;Comments";
                //grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
                //grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";
                //grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                //    "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                //    CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");
                //grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                //    "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId ";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Appt. Date';eTn_TrackLoad.date_CreateDate as 'Delivered Date'^eTn_TrackLoad.date_CreateDate as 'Date & Time'^eTn_TrackLoad.nvar_Notes as 'Comments'^eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;
                grdManager.LastLocationVisible = true;
                grdManager.LoadStatus = "Terminated";
                barManager.HeaderText = "Terminated Loads";
                if (Session["LoadGridPageIndex"] == null)
                    Session["LoadGridPageIndex"] = 0;
                grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                break;
            #endregion

            case "Paperwork pending":
                ddlNewStatus.Visible = false;
                lblNewStatus.Visible = false;
                #region Paperwork Pending
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Hot Shipments and Rail column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end as 'Type'," +
                    "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                    "'' as 'Customer',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID, eTn_Load.nvar_TripType as TripType, " + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date");
                //SLine 2016 Enhancements Added the Hot Shipments and Rail column to the Query.

                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                    //SLine 2017 Enhancement for Office Location
                    " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Customer;Driver/Carrier;Origin;Destination;Last Location;Date & Time;Comments";
                grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
                grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";

                grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                    "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                    CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");

                grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                    "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver') ";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To<br/> '+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Appt. Date';eTn_TrackLoad.date_CreateDate as 'Delivered Date'^eTn_TrackLoad.date_CreateDate as 'Date & Time'^eTn_TrackLoad.nvar_Notes as 'Comments'^eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;
                grdManager.LastLocationVisible = true;
                grdManager.LoadStatus = "Paperwork Pending";
                barManager.HeaderText = "Paperwork Pending Loads";
                if (Session["LoadGridPageIndex"] == null)
                    Session["LoadGridPageIndex"] = 0;
                grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                break;
            #endregion

            case "Closed":
                lblNewStatus.Visible = false;
                ddlNewStatus.Visible = false;
                #region Closed
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Hot Shipments and Rail column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end as 'Type'," +
                    "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                    "'' as 'Customer',etn_Load.bit_HotShipment as 'HotShipment',eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date");
                //SLine 2016 Enhancements Added the Hot Shipments and Rail column to the Query.

                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                    //SLine 2017 Enhancement for Office Location
                    " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.SingleRowColumnsOrderByClause = " order by eTn_Load.bint_LoadId DESC";
                grdManager.VisibleColumnsList = "Load;Customer;Driver/Carrier;Origin;Destination;Delivered Date;Terminated Date;Days In Transit;Last Location;Date & Time;Comments";
                grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
                grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";
                grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                    "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                    CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");
                grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                    "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver') ";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Appt. Date',case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' <br/>To<br/> '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Delivery Appt Date';eTn_TrackLoad.date_CreateDate as 'Terminated Date'^eTn_TrackLoad.date_CreateDate as 'Date & Time'^eTn_TrackLoad.nvar_Notes as 'Comments'^isnull(DateDiff(dd,(select top 1 t.date_CreateDate from eTn_TrackLoad t where t.bint_LoadStatusId = 3 and t.bint_LoadId = eTn_TrackLoad.bint_LoadId order by t.date_CreateDate desc),isnull(eTn_TrackLoad.date_CreateDate,getdate())),0) as 'Days In Transit';" +
                    "eTn_TrackLoad.date_CreateDate as 'Delivered Date'^eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim() + ";eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Re-Open";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Re-Open";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "";
                    grdManager.LastColumnPagesList = "";
                }
                grdManager.DeleteVisible = false;
                grdManager.LastLocationVisible = true;
                grdManager.LoadStatus = "Closed";
                barManager.HeaderText = "Closed Loads";
                if (Session["LoadGridPageIndex"] == null)
                    Session["LoadGridPageIndex"] = 0;
                grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                break;
            #endregion

            case "All Transit Loads":
                ddlNewStatus.Visible = false;
                lblNewStatus.Visible = false;
                #region All Transit Loads
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Hot Shipments column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end as 'Type'," +
                    "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                    "'' as 'Customer',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ",eTn_LoadStatus.nvar_LoadStatusDesc as 'Status' ;nvar_Pickup as 'Pickup#',eTn_Load.nvar_TripType as TripType";
                //SLine 2016 Enhancements Added the Hot Shipments column to the Query.

                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                    " inner join  eTn_LoadStatus on eTn_LoadStatus.bint_LoadStatusId = eTn_Load.bint_LoadStatusId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId in (select eTn_LoadStatus.bint_LoadStatusId from eTn_LoadStatus where lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'new' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'load planner' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'drop in warehouse' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'delivered' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'empty in yard' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'terminated' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'paperwork pending' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'loaded in yard' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'closed' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'cancel' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'invoiceable' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'street turn')" +
                    //SLine 2017 Enhancement for Office Location
                    " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                //lower(eTn_LoadStatus.nvar_LoadStatusDesc) = 'assigned' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) = 'pickedup' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) = 'en-route' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) = 'driver on waiting')";
                grdManager.SingleRowColumnsOrderByClause = " order by eTn_LoadStatus.bint_LoadStatusId,len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Customer;Driver/Carrier;Origin;Pickup#;Pickup;Destination;Delivery Appt Date;Last Free Date;Status";
                grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
                grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";
                grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                    "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                    CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");
                grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                    "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver') ";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+'<br/> To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.',case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' <br/>To<br/> '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Delivery Appt Date'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId";
                if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;
                grdManager.LastLocationVisible = true;
                grdManager.LoadStatus = "Transit";
                barManager.HeaderText = "Transit Loads";
                if (Session["LoadGridPageIndex"] == null)
                    Session["LoadGridPageIndex"] = 0;
                grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                #endregion
                break;

            case "Reached Destination":
                ddlNewStatus.Visible = false;
                lblNewStatus.Visible = false;
                #region Reached Destination Status Loads
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Hot Shipments And Rail column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end as 'Type'," +
                    "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                    "'' as 'Customer',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID, eTn_Load.nvar_TripType as TripType, " + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ",eTn_LoadStatus.nvar_LoadStatusDesc as 'Status',(select [date_CreateDate] from eTn_TrackLoad where bint_LoadId=eTn_Load.bint_LoadId and bint_LoadStatusId=(select bint_LoadStatusId from eTn_LoadStatus where lower([nvar_LoadStatusDesc])='reached destination')) as 'Driver Reached @'";
                //SLine 2016 Enhancements Added the Hot Shipments And Rail column to the Query.

                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                    " inner join  eTn_LoadStatus on eTn_LoadStatus.bint_LoadStatusId = eTn_Load.bint_LoadStatusId ";

                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                    //SLine 2017 Enhancement for Office Location
                    " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.SingleRowColumnsOrderByClause = " order by eTn_LoadStatus.bint_LoadStatusId,len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Customer;Driver/Carrier;Origin;Pickup#;Pickup;Destination;Driver Reached @;Delivery Appt Date";
                grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
                grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";
                grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                    "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                    CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");
                grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                    "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver') ";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To<br/> '+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.',case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' <br/>To<br/> '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Delivery Appt Date'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId";
                if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/UpdateLoadStatus.aspx";
                }

                grdManager.DeleteVisible = false;
                grdManager.LastLocationVisible = true;
                grdManager.LoadStatus = "Reached Destination";
                barManager.HeaderText = "Reached Destination";
                if (Session["LoadGridPageIndex"] == null)
                    Session["LoadGridPageIndex"] = 0;
                grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                #endregion
                break;


            //SLine 2016 ---- New Case for "Street Turn" Condition"
            case "Street Turn":
                ddlNewStatus.Visible = false;
                lblNewStatus.Visible = false;
                #region Empty in Yard
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end as 'Type'," +
                    "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                    "'' as 'Customer',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID, eTn_Load.nvar_TripType as TripType, " + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date");

                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() +
                    //SLine 2017 Enhancement for Office Location
                    " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Customer;Driver/Carrier;Origin;Destination;St. Turn;Last Location;Date & Time;Comments";
                grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
                grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";
                grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                    "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                    CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");
                grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                    "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver') ";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Appt. Date';eTn_TrackLoad.date_CreateDate as 'Delivered Date'^eTn_TrackLoad.date_CreateDate as 'Date & Time'^eTn_TrackLoad.nvar_Notes as 'Comments'^DateDiff(dd,isnull([eTn_Load].date_StreetTurnDate,getdate()),getdate()) as 'St. Turn'^eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Edit Dispatch;Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/UpdateLoadStatus.aspx";
                }
                else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Manager/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;
                grdManager.LastLocationVisible = true;
                grdManager.LoadStatus = "Street Turn";
                barManager.HeaderText = "Street Turn Loads";
                if (Session["LoadGridPageIndex"] == null)
                    Session["LoadGridPageIndex"] = 0;
                grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                break;
            #endregion





            default: break;
        }
    }
    protected void btnShow_Click(object sender, EventArgs e)
    {
        Session["LoadGridPageIndex"] = 0;
        Session["LoadStatus"] = ViewState["LoadStatus"] = dropStatus.SelectedItem.Text;
        GridFill();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (txtContainer.Text.Trim().Length > 0)
        {
            //SLine 2017 Enhancement for Office Location
            long officeLocationId = GetOfficeLocationId();
            switch (ddlContainer.SelectedIndex)
            {
                case 0:
                    //SLine 2017 Enhancement for Office Location
                    Session["TrackWhereClause"] = "(eTn_Load.[nvar_Container] like '" + txtContainer.Text.Trim() + "' or eTn_Load.[nvar_Container1] like '" + txtContainer.Text.Trim() + "') and eTn_Load.bint_OfficeLocationId=" + officeLocationId;
                    break;
                case 1:
                    //SLine 2017 Enhancement for Office Location
                    Session["TrackWhereClause"] = "eTn_Load.[nvar_Ref] like '" + txtContainer.Text.Trim() + "%' and eTn_Load.bint_OfficeLocationId=" + officeLocationId;
                    break;
                case 2:
                    //SLine 2017 Enhancement for Office Location
                    Session["TrackWhereClause"] = "eTn_Load.[nvar_Booking] like '" + txtContainer.Text.Trim() + "%' and eTn_Load.bint_OfficeLocationId=" + officeLocationId;
                    break;
                case 3:
                    //SLine 2017 Enhancement for Office Location
                    Session["TrackWhereClause"] = "(eTn_Load.[nvar_Chasis] like '" + txtContainer.Text.Trim() + "%' or eTn_Load.[nvar_Chasis1] like '" + txtContainer.Text.Trim() + "%') and eTn_Load.bint_OfficeLocationId=" + officeLocationId;
                    break;
                case 4:
                    try
                    {
                        //SLine 2017 Enhancement for Office Location
                        Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=" + Convert.ToInt64(txtContainer.Text.Trim()) + " and eTn_Load.bint_OfficeLocationId=" + officeLocationId;
                    }
                    catch
                    {
                        Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=0";
                    }
                    break;
            }
        }
        else
        {
            Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=0";
        }
        bool blVal = false;
        try
        {
            if (Convert.ToInt32(DBClass.executeScalar("select count(bint_LoadId) from eTn_Load where " + Convert.ToString(Session["TrackWhereClause"]))) == 1)
            {
                blVal = true;
            }
        }
        catch
        {
        }
        if (blVal)
        {
            Response.Redirect("~/Manager/TrackLoad.aspx?" + DBClass.executeScalar("select bint_LoadId from eTn_Load where " + Convert.ToString(Session["TrackWhereClause"])));
        }
        Response.Redirect("~/Manager/SearchResults.aspx");
    }
    protected void dropStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["IsDropDown"] = true;
        ViewState["LoadStatus"] = Session["LoadStatus"] = dropStatus.SelectedItem.Text;
        if (dropStatus.SelectedItem.Text != "New")
        {
            ddlNewStatus.Visible = false;
            lblNewStatus.Visible = false;
        }
        else
        {
            ddlNewStatus.Visible = true;
            lblNewStatus.Visible = true;
        }
    }

    // today , tomorrow , LFD and Accessorial links click  (madhav)
    #region  today , tomorrow and Accessorial count click

    protected void lnkDay_click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        Response.Redirect(ResolveClientUrl("~/Manager/SameandNextDayLoads.aspx?type=3&name=" + lnk.CommandArgument + "&day=" + lnk.ID.Substring(lnk.ID.Length - 1, 1) + ""));
    }

    protected void lnkNextDayLoads_click(object sender, EventArgs e)
    {
        Response.Redirect(ResolveClientUrl("~/Manager/SameandNextDayLoads.aspx?type=2"));
    }
    protected void lnkSameDayLoads_click(object sender, EventArgs e)
    {
        Response.Redirect(ResolveClientUrl("~/Manager/SameandNextDayLoads.aspx?type=1"));
    }
    protected void lnkIsExportLoads_click(object sender, EventArgs e)
    {
        Response.Redirect(ResolveClientUrl("~/Manager/SameandNextDayLoads.aspx?type=6"));
    }
    protected void lnkAccessorialLoads_click(object sender, EventArgs e)
    {
        Response.Redirect(ResolveClientUrl("~/Manager/EmptyReturnFreeDays.aspx?type=3"));
    }
    protected void lnkltodayLFDLoads_click(object sender, EventArgs e)
    {
        Response.Redirect(ResolveClientUrl("~/Manager/SameandNextDayLoads.aspx?type=4"));
    }
    protected void lnkDriversDispatch_click(object sender, EventArgs e)
    {
        Response.Redirect(ResolveClientUrl("~/Manager/DriversDispatch.aspx"));
    }
    protected void lnkInvoiceableLoads_click(object sender, EventArgs e)
    {
        Response.Redirect(ResolveClientUrl("~/Manager/InvoiceableLoads.aspx?type=1"));
    }
    protected void lnkLFDExpired_click(object sender, EventArgs e)
    {
        Response.Redirect(ResolveClientUrl("~/Manager/SameandNextDayLoads.aspx?type=5"));
    }

    #endregion
}
