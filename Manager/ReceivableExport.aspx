<%@ Page Language="C#" MasterPageFile="~/Manager/MasterPage.master" AutoEventWireup="true" CodeFile="ReceivableExport.aspx.cs" Inherits="Manager_Default" Title="Untitled Page" %>
<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td style="height:10px;" width="50%">
                &nbsp; &nbsp; From:
                <uc1:DatePicker ID="dtpFromDate" runat="server" IsDefault="false" IsRequired="false" SetInitialDate="true" CountNextYears="2" CountPreviousYears="0" />
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;To:
                <uc1:DatePicker ID="dtpToDate" runat="server" IsDefault="false" IsRequired="false" SetInitialDate="true" CountNextYears="2" CountPreviousYears="0" />
                <%--&nbsp; &nbsp;Customer Name: <asp:DropDownList ID="ddlCustomer" runat="server">
                </asp:DropDownList>
                &nbsp; &nbsp;--%>
                <asp:Button ID="btnSearch" runat="server" CssClass="btnstyle" OnClick="btnSearch_Click"
                    Text="Search" />
            </td>           
            <td align="right">
                Note :&nbsp;<asp:TextBox ID="txtNote" runat="Server" Width="240px"></asp:TextBox> 
                &nbsp;<asp:Button ID="btnExport" runat="server" CssClass="btnstyle" Text="Export" OnClick="btnExport_Click"/>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height:5px;" align="left" valign="middle" visible="false">
                <asp:Panel ID="pnlCreateFile" runat="server" Width="100%" Visible="False">
                    The selected loads are exported to a file.You can <asp:LinkButton ID="lnkDownload" runat="Server" OnClick="lnkDownload_Click" Visible="False">Download Here.</asp:LinkButton>
                </asp:Panel>
            </td>
        </tr>
        <tr valign="top" align="left">
            <td  width="100%" colspan="2">
                <asp:Label ID="lblError" runat="server" Text="no records exist for the selected dates." Visible="false" ForeColor="red"></asp:Label>
                 <asp:GridView ID="gvReceiable" runat="server" CssClass="Grid" OnRowCommand="GridView1_RowCommand" AutoGenerateColumns="false">
                    <PagerStyle CssClass="GridPage" Font-Size="Smaller" HorizontalAlign="Center" Width="100%"/>
                    <RowStyle BorderColor="White" CssClass="GridItem" />
                    <HeaderStyle BorderColor="White" CssClass="GridHeader" ForeColor="Black" />
                    <AlternatingRowStyle CssClass="GridAltItem" />
                    <Columns>                        
                        <asp:BoundField DataField="LoadId" HeaderText="Load Number" HeaderStyle-Width="10%"/>
                        <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" />
                        <%--<asp:TemplateField HeaderText="Customer" HeaderStyle-Width="30%">
                            <ItemTemplate>
                                <asp:Label ID="lblCustomer" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CustomerName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:BoundField DataField="Charges" HeaderText="Charges" HeaderStyle-Width="10%"/>
                        <asp:BoundField DataField="FuelSurcharges" HeaderText="Fuel Surcharges" HeaderStyle-Width="15%"/>
                        <asp:BoundField DataField="ExtraCharges" HeaderText="Extra Charges" HeaderStyle-Width="10%"/>
                        <asp:BoundField DataField="NetAmount" HeaderText="Net Amount" HeaderStyle-Width="8%"/>
<%--                        <asp:BoundField DataField="QBStatus" HeaderText="QBStatus" HeaderStyle-Width="10%"/>
--%>                        <asp:TemplateField HeaderStyle-Width="60px">
                            <HeaderTemplate>
                               <asp:LinkButton ID="lnkSelectAll" runat="Server">Select</asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>                                
                                <asp:CheckBox ID="ChkLoadID" runat="Server"/>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
         </tr>                  
</table>        
</asp:Content>

