<%@ Page AutoEventWireup="true" CodeFile="SearchCustomer.aspx.cs" Inherits="SearchCustomer"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/Grid.ascx" TagName="Grid" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc2" %>
<%--<%@ Register Src="~/UserControls/SearchControl.ascx" TagName="SearchControl" TagPrefix="uc1" %>
--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="ContentTbl">
<tr valign="top">
    <td>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblbox1">
        <tr style="background-color:#FFFFFF;">
             <td align="left" valign="middle">
                <asp:Label ID="lblText" runat="server" Text="Customer Name : "></asp:Label>
                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                <asp:DropDownList ID="ddlList" runat="server">
                    <asp:ListItem Selected="True" Value="1">Active</asp:ListItem>
                    <asp:ListItem Value="0">In Active</asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnStyle" OnClick="btnSearch_Click"/>
                 <asp:Label ID="lblShowError" runat="server" Font-Bold="True" ForeColor="Red" Text="*Invalid Customer name."
                     Visible="False"></asp:Label></td>
           </tr>
        </table> 
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
          </tr>
        </table> 
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="left" width="100%">
                  <uc2:GridBar ID="barComstomer" runat="server" HeaderText="Customers" LinksList="Add New Customer" PagesList="NewCustomer.aspx" Visible="true"/>                
              </td>
            </tr>
          </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
          </tr>
        </table> 
      <table cellpadding="0" cellspacing="0" width="100%">
          <tr>
          <td>
              <uc3:Grid ID="gridCustomer" runat="server" />
          </td>
        </tr>
      </table>
     </td>
  </tr>
</table>
</asp:Content>

