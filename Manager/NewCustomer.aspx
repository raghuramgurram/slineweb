<%@ Page Language="C#" MasterPageFile="~/Manager/MasterPage.master" AutoEventWireup="true" CodeFile="NewCustomer.aspx.cs" Inherits="NewCustomer" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/Url.ascx" TagName="Url" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/USState.ascx" TagName="USState" TagPrefix="uc5" %>

<%@ Register Src="~/UserControls/Phone.ascx" TagName="Phone" TagPrefix="uc2" %>

<%@ Register Src="~/UserControls/LinksBar.ascx" TagName="LinksBar" TagPrefix="uc4" %>
<%@ Register Src="~/UserControls/Grid.ascx" TagName="Grid" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td style="width: 952px">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="tblform1">
            <tr>
                <td>
                    <asp:ValidationSummary ID="ReqCustomer" runat="server" Width="526px" />
                </td>
            </tr>
        </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblform">
        <tr>
          <th colspan="2" valign="middle">Address Details</th>
        </tr>
        <tr id="row">
          <td width="50%" valign="top" style="height: 20px"><asp:Label ID="lblCustomer" runat="server" Text="Customer : "></asp:Label> <br />
              <asp:TextBox ID="txtCustomer" runat="Server" MaxLength="150"/>
              <asp:RequiredFieldValidator ID="rfvCustomer" runat="server" ControlToValidate="txtCustomer"
                  Display="Dynamic" ErrorMessage="Please Enter Customer.">*</asp:RequiredFieldValidator>
            <asp:CheckBox ID="chkActive" runat="Server" Text="Active " TextAlign="Left" Checked="True"/>
              <asp:Label ID="lblNameError" runat="server" Font-Bold="False" ForeColor="Red" Text="Customer name already exists."
                  Visible="False"></asp:Label></td>
          <td style="height: 20px"><asp:CheckBox ID="chkCreditLimit" runat="Server" Text="Credit Limit Exceeded " TextAlign="Left"/></td>                      
        </tr>
        <tr id="altrow">
          <td style="height: 20px"><asp:Label ID="lblStreet" runat="server" Text="Street : "/><br />
              <asp:TextBox ID="txtStreet" runat="Server" MaxLength="200"/>
              <asp:RequiredFieldValidator ID="rfvStreet" runat="server" ControlToValidate="txtStreet"
                  Display="Dynamic" ErrorMessage="Please Enter Street.">*</asp:RequiredFieldValidator></td>
          <td style="height: 20px"><asp:Label ID="lblPhone" runat="server" Text="Phone : "/><br />
           <%-- <asp:TextBox ID="txtPhone" runat="Server" MaxLength="18"/>
              <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ControlToValidate="txtPhone"
                  Display="Dynamic" ErrorMessage="Please Enter Phone Number.">*</asp:RequiredFieldValidator>
              <asp:RegularExpressionValidator ID="revPhone" runat="server" ControlToValidate="txtPhone"
                  Display="Dynamic" ErrorMessage="Please enter a valid Phone number in the format (123) 123-1234 or 123-123-1234."
                  ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">*</asp:RegularExpressionValidator>--%>
              <uc2:Phone ID="txtPhone" runat="server" IsRequiredField="false" />
          </td>
        </tr>
        <tr id="row">
          <td><asp:Label ID="lblSuite" runat="server" Text="Suite# : "/><br />
              <asp:TextBox ID="txtSuite" runat="Server" MaxLength="100"/></td>
          <td><asp:Label ID="lblFax" runat="server" Text="Fax : "/><br />
              <%--<asp:TextBox ID="txtFax" runat="Server" MaxLength="18"/>
              <asp:RegularExpressionValidator ID="revFax" runat="server" ControlToValidate="txtFax"
                  Display="Dynamic" ErrorMessage="Please enter a valid Fax number in the format (123) 123-1234 or 123-123-1234."
                  ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">*</asp:RegularExpressionValidator>--%>
              <uc2:Phone ID="txtFax" runat="server" IsRequiredField="false" />
          </td> 
        </tr>
        <tr id="altrow">
          <td valign="top"><asp:Label ID="lblCity" runat="server" Text="City : "/><br />
              <asp:TextBox ID="txtCity" runat="Server" MaxLength="100"/></td>
          <td><asp:Label ID="lblWebSiteUrl" runat="server" Text="Website URL : "/><br />
              <uc3:Url ID="txtWebSiteUrl" runat="server" IsRequired="false" />
          </td> 
        </tr>
          <tr>
              <td valign="top">
                  &nbsp;&nbsp;
                  <asp:Label ID="lblZip" runat="server" Text="Zip : "/><br />
                  &nbsp;&nbsp;
                  <asp:TextBox ID="txtZip" runat="server" MaxLength="20"/></td>
              <td>
                  &nbsp; &nbsp;<asp:Label ID="lblState" runat="server" Text="State :"/><br />
                  &nbsp;&nbsp;
                  <uc5:USState ID="ddlState" runat="server" IsRequired="false" />
              </td>
          </tr>
          <%--new coloumns add by raghu--%>
           <tr>
              <td valign="top">
                  &nbsp;&nbsp;
                  <asp:Label ID="lblAccessorialChargesEmailAddress" runat="server" Text="Accessorial Charges Email : "/><br />
                  &nbsp;&nbsp;
                  <asp:TextBox ID="txtAccessorialChargesEmailAddress" runat="server" MaxLength="100" Width="200px"/>
                  <%--<asp:RequiredFieldValidator ID="rfvAccessorialChargesEmailAddress" runat="server" ErrorMessage="Please enter Accessorial Charges Email Address." ControlToValidate="txtAccessorialChargesEmailAddress" Display="Dynamic">*</asp:RequiredFieldValidator>--%>
                  <asp:RegularExpressionValidator ID="revAccessorialChargesEmailAddress" runat="server" ErrorMessage="Please enter a valid Accessorial Charges Email Address." ControlToValidate="txtAccessorialChargesEmailAddress" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                  </td>
              <td>
                  &nbsp; &nbsp;<asp:Label ID="lblPODEmailAddress" runat="server" Text="POD Email : "/><br />
                  &nbsp;&nbsp;
                   <asp:TextBox ID="txtPODEmailAddress" runat="server" MaxLength="100" Width="200px"/>
                   <%--<asp:RequiredFieldValidator ID="rfvPODEmailAddress" runat="server" ErrorMessage="Please enter POD Email Address." ControlToValidate="txtPODEmailAddress" Display="Dynamic">*</asp:RequiredFieldValidator>--%>
                   <asp:RegularExpressionValidator ID="revPODEmailAddress" runat="server" ErrorMessage="Please enter a valid POD Email Address." ControlToValidate="txtPODEmailAddress" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
              </td>
          </tr>
          
           <tr>
              <td valign="top">
                  &nbsp;&nbsp;
                  <asp:Label ID="lblDeliveryEmailAddress" runat="server" Text="Delivery Email : "/><br />
                  &nbsp;&nbsp;
                  <asp:TextBox ID="txtDeliveryEmailAddress" runat="server" MaxLength="100" Width="200px"/>
                  <%--<asp:RequiredFieldValidator ID="rfvDeliveryEmailAddress" runat="server" ErrorMessage="Please enter Delivery Email Address." ControlToValidate="txtDeliveryEmailAddress" Display="Dynamic">*</asp:RequiredFieldValidator>--%>
                  <asp:RegularExpressionValidator ID="revDeliveryEmailAddress" runat="server" ErrorMessage="Please enter a valid Delivery Email Address." ControlToValidate="txtDeliveryEmailAddress" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                  </td>
              <td><asp:CheckBox ID="chkSendMails" runat="server" Text="Send Status Update Emails   " TextAlign="Left" />             
              </td>
          </tr>
           <%--End--%>
        <tr id="row">
          <td valign="top"><asp:Label ID="lblDirection" runat="server" Text="Directions : "/>
              <asp:TextBox ID="txtDirection" runat="Server" TextMode="MultiLine" Width="336px" Height="67px" MaxLength="1000"/></td> 


               <%--SLine 2016 Enhancements Added Automatic Invoice--%>
          <td>


              <table style="text-align: left;">
                  <tr>
                      <td align="left">
                          Send Invoice via. Email:
                      </td>
                      <td align="left">
                          <asp:CheckBox ID="chkSendInvoiceViaEmail" runat="server" AutoPostBack="true" 
                              oncheckedchanged="chkSendInvoiceViaEmail_CheckedChanged" Checked="false"/>
                      </td>
                  </tr>
                  <tr id="trBillingEmail" runat="server" visible="false">
                      <td>
                          Billing Email Address:
                      </td>
                      <td>
                          <asp:TextBox ID="txtBillingEmail" runat="server" MaxLength="100" Width="200px" />
                          <asp:RequiredFieldValidator ID="rfvBillingEmail" runat="server" ErrorMessage="Please enter Billing Email Address." ControlToValidate="txtBillingEmail" Display="Dynamic">*</asp:RequiredFieldValidator>
                          <asp:RegularExpressionValidator
                              ID="revSendInvoiceViaEmail" runat="server" ErrorMessage="Please enter a valid Email Address."
                              ControlToValidate="txtBillingEmail" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                      </td>
                  </tr>
              </table>


          </td>
          <%--SLine 2016 Enhancements Added Automatic Invoice--%>

        </tr>
          <tr>
              <td valign="top" height="10px">
              </td>
              <td valign="top">
              </td>
          </tr>
          <tr>
              <td valign="top" colspan="2">
                  &nbsp;</td>              
          </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../images/pix.gif" alt="" width="1" height="3" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
        <tr>
          <td align="right">
              <asp:Button ID="btnSave" runat="server" Height="22px" Text="Save" OnClick="btnSave_Click" CssClass="btnstyle"/>
              &nbsp;<asp:Button ID="btnCancel" runat="server" CausesValidation="False" OnClick="btnCancel_Click"
                  Text="Cancel" UseSubmitBehavior="False" Height="22px" CssClass="btnstyle"/>&nbsp;
          </td>        
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../images/pix.gif" alt="" width="1" height="3" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td>
            <uc4:LinksBar ID="lnkBar" runat="server" HeaderText="Contact Persons" LinksList="Add Contact" />
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tbldisplay">
        <tr>
           <td>
               <uc1:Grid ID="Grid1" runat="server" Visible="true" />
            </td>
        </tr>
        <tr>
        <td>
            &nbsp;</td>
        </tr>
      </table>
    </table>
</asp:Content>

