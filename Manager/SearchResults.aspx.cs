using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class SearchResults : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Track Load";
        if (!IsPostBack)
        {
            DisplayDetails();
        }
    }

    private void DisplayDetails()
    {
        //string strQuery = "select [bint_LoadStatusId] as 'Id',[nvar_LoadStatusDesc] as 'Name' from [eTn_LoadStatus]";
        //DataTable dt = DBClass.returnDataTable(strQuery);
        //if (dt.Rows.Count > 0)
        //{
        //    ddlLoads.DataSource = dt;
        //    ddlLoads.DataTextField = "Name";
        //    ddlLoads.DataValueField = "Id";
        //    ddlLoads.DataBind();
        //}
        //if (dt != null) { dt.Dispose(); dt = null; }

        GridSearch.Visible = true;
        GridSearch.DeleteVisible = false;
        GridSearch.EditVisible = false;
        GridSearch.TableName = "eTn_Load";
        GridSearch.Primarykey = "eTn_Load.bint_LoadId";
        GridSearch.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        GridSearch.ColumnsList = "eTn_Load.bint_LoadId;" + CommonFunctions.LoadColumnQueryString("convert(varchar(20),eTn_Load.bint_LoadId);eTn_Load.nvar_Container;eTn_Load.nvar_Container1", "Load","TrackLoad.aspx") + ";eTn_LoadType.nvar_LoadTypeDesc as 'Type';eTn_LoadStatus.nvar_LoadStatusDesc as 'Status';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_CustomerName;eTn_Customer.nvar_Street;eTn_Customer.nvar_Suite;eTn_Customer.nvar_City;eTn_Customer.nvar_State;eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "Customer");
        GridSearch.VisibleColumnsList = "Load;Type;Status;Customer";
        GridSearch.VisibleHeadersList = "Load;Type;Status;Customer";
        GridSearch.InnerJoinClause = " inner join eTn_Customer on eTn_Load.bint_CustomerId = eTn_Customer.bint_CustomerId " +
               " inner join eTn_LoadStatus on eTn_Load.bint_LoadStatusId = eTn_LoadStatus.bint_LoadStatusId " +
               "inner join eTn_LoadType on eTn_Load.bint_LoadTypeId = eTn_LoadType.bint_LoadTypeId ";
        if (Session["TrackWhereClause"] != null)
            GridSearch.WhereClause = Convert.ToString(Session["TrackWhereClause"]);
        GridSearch.BindGrid(0);
        //GridSearch.EditPage = "TrackLoad.aspx";
        //if (ViewState["txtContainer"] != null)
        //    txtContainer.Text = Convert.ToString(ViewState["txtContainer"]);
        //if (ViewState["SelectedIndex"] != null)
        //    ddlContainer.SelectedIndex = Convert.ToInt32(ViewState["SelectedIndex"]);
        //else
        //    ddlContainer.SelectedIndex = 0;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //ViewState["txtContainer"] = Convert.ToString(txtContainer.Text.Trim());
        //ViewState["SelectedIndex"] = Convert.ToInt32(ddlContainer.SelectedIndex);

        if (txtContainer.Text.Trim().Length > 0)
        {
            //SLine 2017 Enhancement for Office Location Starts
            long officeLocationId = 0;
            if (Session["OfficeLocationID"] != null)
            {
                officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
            }
            else
            {
                if (Session["LocationDS"] != null)
                {
                    DataSet locDs = Session["LocationDS"] as DataSet;
                    officeLocationId = Convert.ToInt64(locDs.Tables[1].Rows[0][0]);
                }
            }
            //SLine 2017 Enhancement for Office Location Ends
            switch (ddlContainer.SelectedIndex)
            {
                case 0:
                    //SLine 2017 Enhancement for Office Location
                    Session["TrackWhereClause"] = "(eTn_Load.[nvar_Container] like '" + txtContainer.Text.Trim() + "' or eTn_Load.[nvar_Container1] like '" + txtContainer.Text.Trim() + "') and eTn_Load.bint_OfficeLocationId=" + officeLocationId;
                    break;
                case 1:
                    //SLine 2017 Enhancement for Office Location
                    Session["TrackWhereClause"] = "eTn_Load.[nvar_Ref] like '" + txtContainer.Text.Trim() + "%' and eTn_Load.bint_OfficeLocationId=" + officeLocationId;
                    break;
                case 2:
                    //SLine 2017 Enhancement for Office Location
                    Session["TrackWhereClause"] = "eTn_Load.[nvar_Booking] like '" + txtContainer.Text.Trim() + "%' and eTn_Load.bint_OfficeLocationId=" + officeLocationId;
                    break;
                case 3:
                    //SLine 2017 Enhancement for Office Location
                    Session["TrackWhereClause"] = "(eTn_Load.[nvar_Chasis] like '" + txtContainer.Text.Trim() + "%' or eTn_Load.[nvar_Chasis1] like '" + txtContainer.Text.Trim() + "%') and eTn_Load.bint_OfficeLocationId=" + officeLocationId;
                    break;
                case 4:
                    try
                    {
                        //SLine 2017 Enhancement for Office Location
                        Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=" + Convert.ToInt64(txtContainer.Text.Trim()) + " and eTn_Load.bint_OfficeLocationId=" + officeLocationId;
                    }
                    catch
                    {
                        Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=0";
                    }
                    break;
            }
        }
        else
        {
            Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=0";
        }
        bool blVal = false;
        try
        {
            if (Convert.ToInt32(DBClass.executeScalar("select count(bint_LoadId) from eTn_Load where " + Convert.ToString(Session["TrackWhereClause"]))) == 1)
            {
                blVal = true;
            }
        }
        catch
        {
        }
        if(blVal)
        {
            Response.Redirect("TrackLoad.aspx?" + DBClass.executeScalar("select bint_LoadId from eTn_Load where " + Convert.ToString(Session["TrackWhereClause"])));
        }
        //Response.Redirect("SearchResults.aspx");
        DisplayDetails();
    }
}
