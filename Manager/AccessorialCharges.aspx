<%@ Page AutoEventWireup="true" CodeFile="AccessorialCharges.aspx.cs" Inherits="AccessorialCharges"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" %>
<%@ Register Src="../UserControls/LoadReceivables.ascx" TagName="LoadReceivables"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%" border="0" cellpadding="3" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center" class="head1">ACCEPTANCE FORM </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img alt="" height="10" src="../Images/pix.gif" width="1" /></td>
        </tr>
      </table>
      <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td style="background-color: #CCCCCC; left: 10px;">
          <img src="../Images/pix.gif" width="1" height="1" /></td>
        </tr>
      </table>
      <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td><img src="../Images/pix.gif" alt="" width="1" height="10" /></td>
        </tr>
      </table>
      
        <table border="0" cellpadding="0" cellspacing="0" style="padding-right: 5px; padding-left: 15px;
            height: 20px; padding-top: 1px; padding-bottom: 1px;" width="100%">
          <tr>
            <td width="50%" valign="top">
            <table border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, sans-serif;
                color: #4D4B4C; font-size: 12px;" width="100%">
              <tr>
                <td width="30%" valign="top"><p><strong>Bill To:</strong><br />
                  <asp:Label ID="lblBillTo" runat="server" Text=" " />
                  <br /><strong>Billing Ref.# </strong>:<asp:Label ID="lblBillingRef" runat="server" Text=" "></asp:Label>
                  </p>
                  </td>
                <td valign="top"><strong>Load Number : 
                    <asp:Label ID="lblLoad" runat="server" Text=" "></asp:Label></strong><br />
                  Type: 
                    <asp:Label ID="lblType" runat="server" Text=" "></asp:Label><br />
                  Commodity : 
                    <asp:Label ID="lblCommodity" runat="server" Text=" "></asp:Label><br />
                  Booking No: 
                    <asp:Label ID="lblBooking" runat="server" Text=" "></asp:Label><br />
                  Container ID:<asp:Label ID="lblContainer" runat="server" Text=" "></asp:Label><br />
                  Empty/ Chassie#:<asp:Label ID="lblChassie" runat="server" Text=" "></asp:Label><br />
                  Remarks:<asp:Label ID="lblremarks" runat="server" Text=" "></asp:Label><br />
                  Description:<asp:Label ID="lblDesc" runat="server" Text=" "></asp:Label>
                  </td>
              </tr>
            </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial, sans-serif;
                color: #4D4B4C; font-size: 12px;">
                <tr>
                  <td><img src="../Images/pix.gif" alt="" width="1" height="10" /></td>
                </tr>
                <tr id="row">
                  <td align="left">
                    <strong><asp:Label ID="lblCompanyName" runat="server"></asp:Label></strong>
                  </td>
                  <td align="right">
                   <strong><asp:Label ID="lblPhone" runat="server"></asp:Label></strong>
                  </td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0" id="TABLE1" onclick="return TABLE1_onclick()">
                <tr>
                  <td bgcolor="#CCCCCC"><img src="../Images/pix.gif" alt="" width="1" height="1" /></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><img src="../Images/pix.gif" alt="" width="1" height="10" /></td>
                </tr>
              </table>
              <table border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, sans-serif;
                  color: #4D4B4C; font-size: 12px;" width="100%">
                <tr>
                  <td valign="top">Dear Customer,<br />
                  Our company request authorization for the additional charges. please sign below and fax (510 625 0635/510 444 2899) it back to us asap.</td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><img src="../Images/pix.gif" alt="" width="1" height="10" /></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td bgcolor="#CCCCCC"><img src="../Images/pix.gif" alt="" width="1" height="1" /></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><img src="../Images/pix.gif" alt="" width="1" height="10" /></td>
                </tr>
              </table>
              <%--<table id="tblform" border="0" cellpadding="0" cellspacing="0" onclick="return tblform_onclick()"
                  width="100%">
              <tr>
                <th colspan="2" style="height: 22px">Accessorial Charges  ($) </th>
              </tr>
              <tr id="row">
                <td align="right" style="height: 20px">Dry Run  :</td>
                <td style="height: 20px">
                    <asp:Label ID="lblDryRun" runat="server" Text=" "></asp:Label></td>
              </tr>
              <tr id="altrow">
                <td align="right">Extra Stops :</td>
                <td>
                    <asp:Label ID="lblExtraStops" runat="server" Text=" "></asp:Label></td>
              </tr>
              <tr id="row">
                <td width="20%" align="right">Lumper Charges :</td>
                <td >
                    <asp:Label ID="lblLumCharges" runat="server" Text=" "></asp:Label></td>
                </tr>
              <tr id="altrow">
                <td align="right">Detention Charges :</td>
                <td>
                    <asp:Label ID="lblDetention" runat="server" Text=" "></asp:Label></td>
                </tr>
              <tr id="row">
                <td align="right">Chassis Split :</td>
                <td>
                    <asp:Label ID="lblChassisSplit" runat="server" Text=" "></asp:Label></td>
                </tr>
              <tr id="altrow">
                <td align="right">Chassis Rent : </td>
                <td>
                    <asp:Label ID="lblChassisRent" runat="server" Text=" "></asp:Label></td>
                </tr>
              <tr id="row">              
                <td align="right">Pallet Charges :</td>
                <td>
                    <asp:Label ID="lblPallet" runat="server" Text=" "></asp:Label></td>
               </tr>
              <tr id="altrow">
                <td align="right">Container Wash Out :</td>
                <td>
                    <asp:Label ID="lblContainerWash" runat="server" Text=" "></asp:Label></td>
                </tr>
              <tr id="row">
                <td align="right">Yard Storage :</td>
                <td>
                    <asp:Label ID="lblYardStorage" runat="server" Text=" "></asp:Label></td>
              </tr>
              <tr id="altrow">
                <td align="right">Ramp pull Charges :</td>
                <td>
                    <asp:Label ID="lblRampPull" runat="server" Text=" "></asp:Label></td>
              </tr>
              <tr id="row">
                <td align="right">Triaxle Charges :</td>
                <td>
                    <asp:Label ID="lblTriaxle" runat="server" Text=" "></asp:Label></td>
              </tr>
              <tr id="altrow">
                <td align="right">Trans Load charges :</td>
                <td>
                    <asp:Label ID="lblTransLoad" runat="server" Text=" "></asp:Label></td>
              </tr>
              <tr id="row">
                <td align="right">Waiting Time Charges :</td>
                <td>
                    <asp:Label ID="lblWaitingTime" runat="server" Text=" "></asp:Label></td>
              </tr>
              <tr id="altrow">
                <td align="right" style="height: 15px">Heavy & Light Scale  Charges :</td>
                <td style="height: 15px">
                    <asp:Label ID="lblScaleCharges" runat="server" Text=" "></asp:Label></td>
              </tr>
              <tr id="row">
                <td align="right">Anyt Other Charges :</td>
                <td>
                    <asp:Label ID="lblOtherCharges" runat="server" Text=" "></asp:Label></td>
                </tr>
              <tr id="altrow">
                <td align="right">Explanation :</td>
                <td>
                    <asp:Label ID="lblExplanation" runat="server" Text=" "></asp:Label></td>
                </tr>
              <tr id="row">
                <td align="right" style="height: 20px"><strong>Total Additional Charges :</strong></td>
                <td style="height: 20px"><strong>
                    <asp:Label ID="lblTotal" runat="server" Text=" "></asp:Label></strong></td>
              </tr>
                  <tr id="Tr1">
                      <td align="right" style="height: 20px">
                          <strong>Total Charges :&nbsp;</strong></td>
                      <td style="height: 20px">
                          <asp:Label ID="lblTotalCharges" runat="server" Text=" "></asp:Label></td>
                  </tr>
            </table>--%>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <uc1:LoadReceivables ID="LoadReceivables1" runat="server" IsAccessorialCharges="true" />
                        </td>
                    </tr>
                </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><img src="../Images/pix.gif" alt="" width="1" height="10" /></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td bgcolor="#CCCCCC"><img src="../Images/pix.gif" alt="" width="1" height="1" /></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><img src="../Images/pix.gif" alt="" width="1" height="25" /></td>
                </tr>
              </table>
              <table border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, sans-serif;font-size: 12px;" width="100%">
                <tr>
                  <td width="50%" valign="top"><p>Authorized by (Signature) : ________________________ </p></td>
                  <td valign="top">Date : ________________________ </td>
                </tr>
                <tr>
                  <td valign="top"></td>
                  <td valign="top"></td>
                </tr>
                  <tr>
                      <td>
                          <img alt="" height="25" src="../Images/pix.gif" width="1" /></td>
                  </tr>
                <tr>
                  <td valign="top">Name : ________________________ </td>
                  <td valign="top"></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><img src="../Images/pix.gif" alt="" width="1" height="20" /></td>
                  </tr>
              </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="right" valign="middle">
                           <asp:Button ID="btnSendMail" runat="server" CssClass="btnstyle" 
                                Text="Send Mail" OnClick="btnSendMail_Click" />
                                 <input type="button" value="Close" onclick="window.close();" class="btnstyle"/>
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="10" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
            </td>
          </tr>
        </table>
     </td>
  </tr>
</table>
</asp:Content>

