using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class LoadPayments : System.Web.UI.UserControl
{
    public string LoadId
    {
        set { ViewState["LoadId"]=value;}
    }
    public string PlayerId
    {
        set { ViewState["PlayerId"] = value; }
    }
    public string RedirectBackURL
    {
        set { ViewState["RedirectBackURL"] = value; }
    }

    public bool ViewClose
    {
        set 
        { 
            btnBackPage.Visible = !value;
            btnClose.Visible = value;
        }
    }

    public bool BackButtonVisible
    {
        get { return pnlButton.Visible; }
        set 
        {
            if (value != null)
                pnlButton.Visible = value;
            else
                pnlButton.Visible = true;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            ShowLoadChargeDetails();
        }
    }
    private void ShowLoadChargeDetails()
    {        
        if (ViewState["LoadId"] != null && ViewState["PlayerId"] != null)
        {
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetPayableDetails", new object[] { Convert.ToInt64(ViewState["LoadId"]), Convert.ToInt64(ViewState["PlayerId"]) });
            if (ds.Tables.Count > 0)
            {               
                   DesginPaybleDetails(ds.Tables[0]);   
            }           
            if (ds != null) { ds.Dispose(); ds = null; }
        }
    }

    private void AssignNonZeroColumn(ref Label lblName,ref Label lblText,string ColumnName,string ColumnValue)
    {
        if (ColumnValue.Trim().Length > 0 && Convert.ToDouble(ColumnValue) != 0)
        {
            lblName.Visible = true;
            lblText.Visible = true;
            lblName.Text = ColumnName;
            lblText.Text = ColumnValue;
        }
    }
    protected void btnBackPage_Click(object sender, EventArgs e)
    {
        if(ViewState["RedirectBackURL"]!=null)
            Response.Redirect(Convert.ToString(ViewState["RedirectBackURL"]));
    }
    public void DesginPaybleDetails(DataTable dtPayables)
    {
        System.Text.StringBuilder strCode = new System.Text.StringBuilder();
        strCode.Append("");
        if (dtPayables.Rows.Count > 0)
        {
            bool blVal = false;
            for (int i = 0, j = 0; i < dtPayables.Columns.Count; i++)
            {
                #region Validations
                blVal = false;
                if (Convert.IsDBNull(dtPayables.Rows[0][i]))
                    dtPayables.Rows[0][i] = 0;
                if (Convert.ToString(dtPayables.Rows[0][i]).Length == 0)
                    dtPayables.Rows[0][i] = 0;
                try
                {
                    if (Convert.ToDouble(dtPayables.Rows[0][i]) > 0)
                    {
                        blVal = true;
                    }
                }
                catch
                {
                    if (Convert.ToString(dtPayables.Rows[0][i]).Trim().Length > 0)
                    {
                        blVal = true;
                    }
                }
                #endregion
                if (blVal)
                {
                    strCode.Append("<tr id=" + (((j % 2) == 0) ? "row" : "altrow") + " >");
                    if (i < (dtPayables.Columns.Count - 1))
                        strCode.Append("<td align=right width=30%>" + dtPayables.Columns[i].ColumnName + "</td>");
                    else
                        strCode.Append("<td align=right width=30%><b>" + dtPayables.Columns[i].ColumnName + "</b></td>");
                    strCode.Append("<td align=left>" + Convert.ToString(dtPayables.Rows[0][i]).Trim() + "</td>");
                    strCode.Append("</tr>");
                    j = j + 1;
                }
            }
            if (strCode.ToString().Length > 0)
            {
                strCode.Insert(0, "<table width=100% border=0 cellspacing=0 cellpadding=0 id=tblform> <tr><th colspan=2>Load Charges($)</th></tr>");
                strCode.Append("</table>");
            }
            else
            {
                strCode.Append("<table width=100% border=0 cellspacing=0 cellpadding=0 id=tblform> <tr><th colspan=2>Load Charges($)</th></tr>");
                strCode.Append("<tr id=row>");
                strCode.Append("<td align=center width=30%><b>No Charges Available for Load : " + Convert.ToString(ViewState["LoadId"]) + " </b></td>");
                strCode.Append("</tr>");
                strCode.Append("</table>");
            }
            lblDisplay.Text = strCode.ToString();
            strCode = null;
        }
        else
        {
            strCode.Append("<table width=100% border=0 cellspacing=0 cellpadding=0 id=tblform> <tr><th colspan=2>Load Charges($)</th></tr>");
            strCode.Append("<tr id=row>");
            strCode.Append("<td align=center width=30%><b>No Charges Available for Load : " + Convert.ToString(ViewState["LoadId"]) + " </b></td>");
            strCode.Append("</tr>");
            strCode.Append("</table>");
            lblDisplay.Text = strCode.ToString();
            strCode = null;            
        }
    }
}
