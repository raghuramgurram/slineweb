﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage.master" AutoEventWireup="true" CodeFile="InvoiceProcessing.aspx.cs" Inherits="InvoiceProcessing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>
                <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="middle">
                        <asp:DropDownList ID="ddlIsVerified" runat="server" EnableViewState="true">
                                <asp:ListItem Value="0" Selected="True">All</asp:ListItem>
                                <asp:ListItem Value="1">Verified</asp:ListItem>
                                <asp:ListItem Value="2">Not Verified</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;&nbsp;&nbsp;
                        <asp:Label ID="lblMonth" runat="server" Text="Month : "></asp:Label>
                            <asp:DropDownList ID="ddlMonth" runat="server">
                                 <asp:ListItem Text="Jan" Value="1"/>
                                 <asp:ListItem Text="Feb" Value="2"/>
                                 <asp:ListItem Text="Mar" Value="3"/>
                                 <asp:ListItem Text="Apr" Value="4"/>
                                 <asp:ListItem Text="May" Value="5"/>
                                 <asp:ListItem Text="Jun" Value="6"/>
                                 <asp:ListItem Text="July" Value="7"/>
                                 <asp:ListItem Text="Aug" Value="8"/>
                                 <asp:ListItem Text="Sep" Value="9"/>
                                 <asp:ListItem Text="Oct" Value="10"/>
                                 <asp:ListItem Text="Nov" Value="11"/>
                                 <asp:ListItem Text="Dec" Value="12"/>
                            </asp:DropDownList>&nbsp; &nbsp;
                        <asp:Label ID="lblYear" runat="server" Text="Year : "></asp:Label>
                            <asp:DropDownList ID="ddlYear" runat="server">
                            </asp:DropDownList>
                        <asp:Button ID="btnSearch" runat="server" CssClass="btnStyle" Text="Search" onClick="btnSearch_Click"/>
                        </td>
                    </tr>
                </table>                
            </td>
        </tr>
    </table>


</asp:Content>

