using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class UnAssignedDrivers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Drivers";
        if(!IsPostBack)
        {
            FillDetails();
        }
    }

    private void FillDetails()
    {
        gridDrivers.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"].ToString());          
        GridBar1.LinksList = "Assigned Drivers ";
        GridBar1.PagesList = "SearchDriver.aspx";
    }
}
