﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SlineCards.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>T Card Details</title>
    <link href="../Styles/cardstyle.css" rel="Stylesheet" type="text/css" />
    <link href="../Styles/cardstyle1.css" rel="Stylesheet" type="text/css" />
</head>
<body>
    <script type="text/javascript" language="javascript">
        function Openfrontpage()
        {
            window.open('SmallTCard.aspx?LoadId='+document.getElementById('<%= HiddenField1.ClientID %>').value);
        }
        function Openbackpage()
        {
            window.open('SmallTCardBack.aspx?LoadId='+document.getElementById('<%= HiddenField1.ClientID %>').value);
        }
        function Openslinecard()
        {
            window.open('LargeTCard.aspx?LoadId='+document.getElementById('<%= HiddenField1.ClientID %>').value);
        }
    </script>
    <form id="form1" runat="server">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td align="center">
                </td>
                <td align="right">
                    <input type="button" value="close" onclick="self.close();" />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <input type="button" value="Small Card front" onclick="Openfrontpage()" />
                </td>
                <td align="center">
                    <input type="button" value="Small Card back" onclick="Openbackpage()" />
                </td>
            </tr>
            <tr>
                <td style="width: 349px; height: 467px; text-align: center;vertical-align:top;">
      <table width="349" border="0" cellpadding="0" cellspacing="0" style="height:455px;border:1px solid #000000;">
      <tr>
        <td height="42" colspan="4" align="left" valign="top">
            <table width="349" border="0" cellpadding="0" cellspacing="0" style="height:42px" class="tblbg">
              <tr>
                <td width="217" height="18" class="text">TRAILER/CONTAINER</td>
                <td width="62" height="18" class="text">TIME</td>
                <td width="70" height="18" class="text">DATE</td>
              </tr>
              <tr>
                <td width="217" height="24" align="left" valign="top" class="container"><asp:Label ID="lblContainer" runat="server" Width="217"></asp:Label></td>
                <td width="62"  height="24" align="left" valign="middle" class="text1"><asp:Label ID="lblTime" runat="server"></asp:Label></td>
                <td width="70"  height="24" class="text1"><asp:Label ID="lblApptDate" runat="server"></asp:Label></td>
              </tr>
            </table>        </td>
        </tr>
      <tr>
        <td colspan="4" align="left" valign="top">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" style="height:29px" class="tblbg">
          <tr>
            <td width="193" height="13" align="left" valign="top" class="text">CHASSIS NO</td>
            <td width="110" height="13" align="left" valign="top" class="text">PHONE#</td>
          </tr>
          <tr>
            <td width="193" height="16" align="left" valign="top" class="text1"><asp:Label ID="lblChassis" runat="server"></asp:Label></td>
            <td width="110" height="16" align="left" valign="top" class="text1"><asp:Label ID="lblPhone" runat="server"></asp:Label></td>
          </tr>
        </table></td>
        </tr>
      <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:26px">
          <tr>
            <td width="192" height="10" align="left" valign="top" class="text">SHIPPER/CONSIGNEE</td>
            <td width="111" height="10" align="left" valign="top" class="text">NAME</td>
          </tr>
          <tr>
            <td width="192" height="16" align="left" valign="middle" class="text1"><asp:Label ID="lblshipper" runat="server"></asp:Label></td>
            <td width="111" height="16" align="left" valign="middle" class="text1"><asp:Label ID="lblName" runat="server"></asp:Label></td>
          </tr>
        </table></td>
        </tr>
        
        <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:26px">
          <tr>
            <td width="195" height="10" align="left" valign="top" class="text">ADDRESS</td>
            <td width="60" height="10" align="left" valign="top" class="text">STAY WITH</td>
            <td width="48" height="10" align="left" valign="top" class="text">DROP</td>
          </tr>
          <tr>
            <td width="195" height="16" align="left" valign="middle" class="text1"><asp:Label ID="lblAddress" runat="server"></asp:Label></td>
            <td width="60" height="16" align="left" valign="middle" class="text1"></td>
            <td width="48" height="16" align="left" valign="middle" class="text1"></td>
          </tr>
        </table></td>
        </tr>
        
        <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:26px">
          <tr>
            <td width="193" height="10" align="left" valign="top" class="text">CITY</td>
            <td width="110" height="10" align="left" valign="top" class="text">SHIP</td>
          </tr>
          <tr>
            <td width="193" height="16" align="left" valign="middle" class="text1"><asp:Label ID="lblCity" runat="server"></asp:Label></td>
            <td width="110" height="16" align="left" valign="middle" class="text1"><asp:Label ID="lblship" runat="server"></asp:Label></td>
          </tr>
        </table></td>
        </tr>
        
        <tr>
        <td colspan="4">
            <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:26px">
              <tr>
                <td width="62" height="10" align="left" valign="top" class="text">AGENT</td>
                <td width="62" height="10" align="left" valign="top" class="text">BOOKING</td>
                <td width="67" height="10" align="left" valign="top" class="text">PU#</td>
                <td width="112" height="10" align="left" valign="top" class="text">DEST</td>
              </tr>
              <tr>
                <td width="62" height="16" align="left" valign="middle" class="text1"><asp:Label ID="lblAgent" runat="server"></asp:Label></td>
                 <td width="62" height="16" align="left" valign="middle" class="text1"><asp:Label ID="lblbooking" runat="server"></asp:Label></td>
                  <td width="67" height="16" align="left" valign="middle" class="text1"><asp:Label ID="lblPickup" runat="server"></asp:Label></td>
                <td width="112" height="16" align="left" valign="middle" class="text1"><asp:Label ID="lblDest" runat="server"></asp:Label></td>
              </tr>
            </table>        </td>
        </tr>
        
      <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" style="height:26px" class="tblbg">
          <tr>
            <td height="10" align="left" valign="top" class="text">CONSIGNEE/SHIPPER</td>
          </tr>
          <tr>
            <td height="16" align="left" valign="middle" class="text1"><asp:Label ID="lblConsignee" runat="server"></asp:Label></td>
          </tr>
        </table></td>
        </tr>
         <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" style="height:26px" class="tblbg">
          <tr>
            <td height="10" align="left" valign="top" class="text">LOCATION</td>
          </tr>
          <tr>
            <td height="16" align="left" valign="middle" class="text1"><asp:Label ID="lblLocation" runat="server"></asp:Label></td>
          </tr>
        </table></td>
        </tr>
       <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" style="height:26px" class="tblbg">
          <tr>
            <td height="10" align="left" valign="top" class="text">RAMP/PIER</td>
          </tr>
          <tr>
            <td height="16" align="left" valign="middle" class="text1"><asp:Label ID="lblRamp" runat="server"></asp:Label></td>
          </tr>
        </table></td>
        </tr>
      <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" height="26px">
          <tr>
            <td width="82" height="10" align="left" valign="top" class="text">PCS</td>
            <td width="82" height="10" align="left" valign="top" class="text">WT</td>
            <td width="139" height="10" align="left" valign="top" class="text">SEAL</td>
          </tr>
          <tr>
            <td width="82" height="16" align="left" valign="middle" class="text1"><asp:Label ID="lblPCS" runat="server"></asp:Label></td>
            <td width="82" height="16" align="left" valign="middle" class="text1"><asp:Label ID="lblWT" runat="server"></asp:Label></td>
            <td width="139" height="16" align="left" valign="middle" class="text1"><asp:Label ID="lblSeal" runat="server"></asp:Label></td>
          </tr>
          
        </table></td>
        </tr>
      
        <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:26px">
          <tr>
            <td width="303" height="26" align="left" valign="middle">&nbsp;</td>
          </tr>
        </table></td>
        </tr>
        <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:26px">
        <tr>
        <td width="58" height="26" align="center" valign="bottom" class="text">RFR</td>
        <td width="55" height="26" align="left" valign="bottom" class="text">TEMP</td>
        <td width="80" height="26" align="left" valign="bottom" class="text">&nbsp;</td>
        <td width="110" height="26" align="left" valign="bottom" class="text">&nbsp;</td>
        </tr>
          </table></td>
        </tr>
        <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:26px">
        <td width="57" height="26" align="center" valign="top" class="text1"><asp:Label ID="lblLoad" runat="server"></asp:Label></td>
        <td width="246" height="26" align="left" valign="bottom" class="text1">&nbsp;</td>
        </table></td>
        </tr>
        <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:29px">
        <tr>
        <td width="58" height="13" align="center" valign="bottom" class="text">&nbsp;</td>
        <td width="157" height="13" align="left" valign="bottom" class="text">&nbsp;</td>
        <td width="88" height="13" align="left" valign="top" class="text">STAY WITH</td>
        </tr>
        <tr>
        <td width="58" height="16" align="center" valign="middle" class="text1">&nbsp;</td>
        <td width="157" height="16" align="left" valign="middle" class="text1">&nbsp;</td>
        <td width="88" height="16" align="left" valign="middle" class="text1"></td>
        </tr>
        </table>
        </td>
        </tr>
        <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:26px">
        <tr>
        <td width="63" height="10" align="left" valign="top" class="text">TIME IN</td>
        <td width="70" height="10" align="left" valign="top" class="text">TIME OUT</td>
        <td width="81" height="10" align="left" valign="bottom" class="text">&nbsp;</td>
        <td width="89" height="10" align="left" valign="top" class="text">STAY WITH</td>
        </tr>
        <tr>
        <td width="63" height="16" align="left" valign="middle" class="text1"></td>
       <td width="70" height="16" align="left" valign="middle" class="text1"></td>
        <td width="81" height="16" align="left" valign="middle" class="text1">&nbsp;</td>
        <td width="89" height="16" align="left" valign="middle" class="text1"></td>
        </tr>
        </table>
        </td>
        </tr>
        <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:26px">
        <tr>
        <td width="215" height="10" align="left" valign="top" class="text">TERMINATE TO</td>
        <td width="88" height="10" align="left" valign="top" class="text">&nbsp;</td>
        </tr>
        <tr>
        <td width="215" height="16" align="left" valign="middle" class="text1"><asp:Label ID="lblTerminate" runat="server"></asp:Label></td>
       <td width="88" height="16" align="left" valign="middle" class="text1">&nbsp;</td>
        </tr>
        </table>
        </td>
        </tr>
        <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:29px">
        <tr>
        <td height="10" width="148" align="left" valign="top" class="text">&nbsp;</td>
        <td height="10" width="155" align="left" valign="top" class="text">DRIVER RATE</td>
        </tr>
        <tr>
        <td height="19" width="148" align="left" valign="bottom" class="text">BW-3842</td>
        <td height="19" width="155" align="left" valign="top" class="text"></td>

        </tr>
        </table>
        </td>
        </tr>
</table>
                </td>
                <td style="width: 349px; height: 455px; text-align: center;vertical-align:top;">
                <table width="349" border="0" cellpadding="0" cellspacing="0" style="height:467px;border:1px solid #000000;">
    <tr>
        <td colspan="4">
        <table width="349" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:42px">
        <tr>
        <td width="349" height="13" align="left" valign="top" class="text">&nbsp;</td>
       </tr>
        <tr>
        <td width="349" height="26" align="left" valign="middle" class="text1">&nbsp;</td>
        </tr>
        </table>
        </td>
        </tr>
       <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:29px">
        <tr>
        <td height="10" align="left" valign="top" class="text">&nbsp;</td>
        </tr>
        <tr>
       <td height="19" align="left" valign="middle" class="text1">&nbsp;</td>
        </tr>
        </table>
        </td>
        </tr>
        <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:29px">
        <tr>
        <td height="10" align="left" valign="top" class="text">&nbsp;</td>
        </tr>
        <tr>
       <td height="19" align="left" valign="middle" class="text1">&nbsp;</td>
        </tr>
        </table>
        </td>
        </tr>
        <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:29px">
         <tr>
        <td height="10" align="left" valign="top" class="text">&nbsp;</td>
        </tr>
        <tr>
       <td height="19" align="left" valign="middle" class="text1">&nbsp;</td>
        </tr>
        </table>
        </td>
        </tr>
        <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:26px">
         <tr>
        <td height="10" align="left" valign="top" class="text">&nbsp;</td>
        </tr>
        <tr>
       <td height="16" align="left" valign="middle" class="text1">&nbsp;</td>
        </tr>
        </table>
        </td>
        </tr>
        <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:26px">
         <tr>
        <td height="10" align="left" valign="top" class="text">&nbsp;</td>
        </tr>
        <tr>
       <td height="16" align="left" valign="middle" class="text1">&nbsp;</td>
        </tr>
        </table>
        </td>
        </tr>
        <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:26px">
        <tr>
        <td height="10" align="left" valign="top" class="text">&nbsp;</td>
        </tr>
        <tr>
       <td height="16" align="left" valign="middle" class="text1">&nbsp;</td>
        </tr>
        </table>
        </td>
        </tr>
        <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:26px">
        <tr>
        <td height="10" align="left" valign="top" class="text">&nbsp;</td>
        </tr>
        <tr>
       <td height="16" align="left" valign="middle" class="text1">&nbsp;</td>
        </tr>
        </table>
        </td>
        </tr>
        <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:26px">
         <tr>
        <td height="10" align="left" valign="top" class="text">&nbsp;</td>
        </tr>
        <tr>
       <td height="16" align="left" valign="middle" class="text1">&nbsp;</td>
        </tr>
        </table>
        </td>
        </tr>
        <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:26px">
         <tr>
        <td height="10" align="left" valign="top" class="text">&nbsp;</td>
        </tr>
        <tr>
       <td height="16" align="left" valign="middle" class="text1">&nbsp;</td>
        </tr>
        </table>
        </td>
        </tr>
   <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:26px">
        <tr>
        <td width="169" height="10" align="left" valign="top" class="text">BILL TO</td>
        <td width="134" height="10" align="left" valign="top" class="text">RATE</td>
        </tr>
        <tr>
        <td width="169" height="16" align="left" valign="middle" class="text1"><asp:Label ID="lblBillTo" runat="server"></asp:Label></td>
       <td width="134" height="16" align="left" valign="middle" class="text1"><asp:Label ID="lblRate" runat="server"></asp:Label></td>
        </tr>
        </table>
        </td>
        </tr>
      
      <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:29px">
        <tr>
        <td width="169" height="10" align="left" valign="top" class="text">AGENT</td>
        <td width="134" height="10" align="left" valign="top" class="text">EXTRA CHARGE</td>
        </tr>
        <tr>
        <td width="169" height="19" align="left" valign="middle" class="text1"></td>
       <td width="134" height="19" align="left" valign="middle" class="text1"></td>
        </tr>
        </table>
        </td>
        </tr>
    <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:29px">
        <tr>
        <td width="169" height="10" align="left" valign="top" class="text">&nbsp;</td>
        <td width="134" height="10" align="left" valign="top" class="text">EXTRA CHARGE</td>
        </tr>
        <tr>
        <td width="169" height="19" align="left" valign="middle" class="text1"></td>
       <td width="134" height="19" align="left" valign="middle" class="text1"></td>
        </tr>
        </table>
        </td>
        </tr>
       <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:29px">
        <tr>
        <td width="169" height="10" align="left" valign="top" class="text">&nbsp;</td>
        <td width="134" height="10" align="left" valign="top" class="text">EXTRA CHARGE</td>
        </tr>
        <tr>
        <td width="169" height="19" align="left" valign="middle" class="text1"></td>
       <td width="134" height="19" align="left" valign="middle" class="text1"></td>
        </tr>
        </table>
        </td>
        </tr>
        <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:29px">
        <tr>
        <td width="169" height="10" align="left" valign="top" class="text">&nbsp;</td>
        <td width="134" height="10" align="left" valign="top" class="text">EXTRA CHARGE</td>
        </tr>
        <tr>
        <td width="169" height="19" align="left" valign="middle" class="text1"></td>
       <td width="134" height="19" align="left" valign="middle" class="text1"></td>
        </tr>
        </table>
        </td>
        </tr>
        <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:29px">
        <tr>
        <td width="169" height="10" align="left" valign="top" class="text">REF#</td>
        <td width="134" height="10" align="left" valign="top" class="text">TOTEL</td>
        </tr>
        <tr>
        <td width="169" height="19" align="left" valign="middle" class="text1"><asp:Label ID="lblRef" runat="server"></asp:Label></td>
       <td width="134" height="19" align="left" valign="middle" class="text1"><asp:Label ID="lbltotal" runat="server"></asp:Label></td>
        </tr>
        </table>
        </td>
        </tr>
        <tr>
        <td colspan="4">
        <table width="303" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg" style="height:11px">
        <tr>
        <td height="11" width="303" align="left" valign="bottom" class="text">BW-3842</td>
        </tr>
        </table>
        </td>
        </tr>
</table>
                </td>
            </tr>
            <tr> 
                <td style="height:5px;" width="100%" colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" style="height: 5px" width="100%" align="center">
                    <input onclick="Openslinecard()" type="button" value="Large Card" /></td>
            </tr>
            <tr>
                <td align="left"  colspan="2">
                    <table width="481" border="0" cellpadding="0" cellspacing="0" style="height:695px;border:1px solid #000000;" ALIGN="center">
                          <tr>
                            <td height="40" colspan="4" align="left" valign="top">
                                <table width="481" border="0" cellpadding="0" cellspacing="0" style="height:40px" class="tblbg1">
                                  <tr>
                                    <td width="311" height="15" class="text3">TRAILER/CONTAINER</td>
                                    <td width="56" height="15" class="text3">TIME</td>
                                    <td width="114" height="15" class="text3">DATE</td>
                                  </tr>
                                  <tr>
                                    <td  width="311" height="25" align="left" valign="middle" class="text2"><asp:Label ID="lblContainer1" runat="server"></asp:Label></td>
                                    <td   width="56" height="25" align="left" valign="middle" class="text2"><asp:Label ID="lblTime1" runat="server"></asp:Label></td>
                                    <td   width="114" height="25" class="text2"><asp:Label ID="lblApptDate1" runat="server"></asp:Label></td>
                                  </tr>
                                </table>
                                </td>
                            </tr>
                          <tr>
                            <td colspan="4" align="left" valign="top">
                            <table width="481" border="0" align="center" cellpadding="0" cellspacing="0" style="height:37px" class="tblbg1">
                              <tr>
                                <td width="307" height="16" align="left" valign="top" class="text">CHASSIS NO</td>
                                <td width="174" height="16" align="left" valign="top" class="text">PHONE#</td>
                              </tr>
                              <tr>
                                <td width="307" height="21" align="left" valign="top" class="text2"><asp:Label ID="lblChassis1" runat="server"></asp:Label></td>
                                <td width="174" height="21" align="left" valign="top" class="text2"><asp:Label ID="lblPhone1" runat="server"></asp:Label></td>
                              </tr>
                            </table></td>
                            </tr>
                          <tr>
                            <td colspan="4">
                            <table width="428" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg1" style="height:36px">
                              <tr>
                                <td width="280" height="16" align="left" valign="top" class="text">SHIPPER/CONSIGNEE</td>
                                <td width="148" height="16" align="left" valign="top" class="text">NAME</td>
                              </tr>
                              <tr>
                                <td width="280" height="20" align="left" valign="middle" class="text2"><asp:Label ID="lblShipper1" runat="server"></asp:Label></td>
                                <td width="148" height="20" align="left" valign="middle" class="text2"><asp:Label ID="lblName1" runat="server"></asp:Label></td>
                              </tr>
                            </table></td>
                            </tr>
                            
                            <tr>
                            <td colspan="4">
                            <table width="428" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg1" style="height:36px">
                              <tr>
                                <td width="288" height="16" align="left" valign="top" class="text3">ADDRESS</td>
                                <td width="75" height="16" align="left" valign="top" class="text3">STAY WITH</td>
                                <td width="65" height="16" align="left" valign="top" class="text3">DROP</td>
                              </tr>
                              <tr>
                                <td width="288" height="20" align="left" valign="middle" class="text2"><asp:Label ID="lblAddress1" runat="server"></asp:Label></td>
                                <td width="75" height="20" align="left" valign="middle" class="text2"></td>
                                <td width="65" height="20" align="left" valign="middle" class="text2"></td>
                              </tr>
                            </table></td>
                            </tr>
                            
                            <tr>
                            <td colspan="4">
                            <table width="428" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg1" style="height:36px">
                              <tr>
                                <td width="284" height="16" align="left" valign="top" class="text3">CITY</td>
                                <td width="144" height="16" align="left" valign="top" class="text3">SHIP</td>
                              </tr>
                              <tr>
                                <td width="283" height="20" align="left" valign="middle" class="text2"><asp:Label ID="lblCity1" runat="server"></asp:Label></td>
                                <td width="145" height="20" align="left" valign="middle" class="text2"><asp:Label ID="lblShip1" runat="server"></asp:Label></td>
                              </tr>
                            </table></td>
                            </tr>
                            
                            <tr>
                            <td colspan="4">
                                <table width="428" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg1" style="height:36px">
                                  <tr>
                                    <td width="134" height="16" align="left" valign="top" class="text3">AGENT</td>
                                    <td width="145" height="16" align="left" valign="top" class="text3">BOOKING/PU#</td>
                                    <td width="149" height="16" align="left" valign="top" class="text3">DEST</td>
                                  </tr>
                                  <tr>
                                    <td width="134" height="20" align="left" valign="middle" class="text2"></td>
                                     <td width="145" height="20" align="left" valign="middle" class="text2"><asp:Label ID="lblBooking1" runat="server"></asp:Label>/<asp:Label ID="lblPickup1" runat="server"></asp:Label></td>
                                      <td width="149" height="20" align="left" valign="middle" class="text2"><asp:Label ID="lblDest1" runat="server"></asp:Label></td>
                                  </tr>
                                </table>        </td>
                            </tr>
                            
                          <tr>
                            <td colspan="4">
                            <table width="428" border="0" align="center" cellpadding="0" cellspacing="0" style="height:36px" class="tblbg1">
                              <tr>
                                <td  width="428" height="16" align="left" valign="top" class="text3">CONSIGNEE/SHIPPER</td>
                              </tr>
                              <tr>
                                <td width="428" height="20" align="left" valign="middle" class="text2"><asp:Label ID="lblConsignee1" runat="server"></asp:Label></td>
                              </tr>
                            </table></td>
                            </tr>
                             <tr>
                            <td colspan="4">
                            <table width="428" border="0" align="center" cellpadding="0" cellspacing="0" style="height:36px" class="tblbg1">
                              <tr>
                                <td width="428" height="16" align="left" valign="top" class="text3">LOCATION</td>
                              </tr>
                              <tr>
                                <td width="428" height="20" align="left" valign="middle" class="text2"><asp:Label ID="lblLocation1" runat="server"></asp:Label></td>
                              </tr>
                            </table></td>
                            </tr>
                           <tr>
                            <td colspan="4">
                            <table width="428" border="0" align="center" cellpadding="0" cellspacing="0" style="height:36px" class="tblbg1">
                              <tr>
                                <td width="428" height="16" align="left" valign="top" class="text3">RAMP/PIER</td>
                              </tr>
                              <tr>
                                <td width="428" height="20" align="left" valign="middle" class="text2"><asp:Label ID="lblRamp1" runat="server"></asp:Label></td>
                              </tr>
                            </table></td>
                            </tr>
                          <tr>
                            <td colspan="4">
                            <table width="428" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg1" height="36px">
                              <tr>
                                <td width="164" height="16" align="left" valign="top" class="text3">PCS</td>
                                <td width="129" height="16" align="left" valign="top" class="text3">WT</td>
                                <td width="135" height="16" align="left" valign="top" class="text3">SEAL</td>
                              </tr>
                              <tr>
                                <td width="164" height="20" align="left" valign="middle" class="text2"><asp:Label ID="lblPCS1" runat="server"></asp:Label></td>
                                <td width="129" height="20" align="left" valign="middle" class="text2"><asp:Label ID="lblWT1" runat="server"></asp:Label></td>
                                <td width="135" height="20" align="left" valign="middle" class="text2"><asp:Label ID="lblSeal1" runat="server"></asp:Label></td>
                              </tr>
                              
                            </table></td>
                            </tr>
                          
                            <tr>
                            <td colspan="4">
                            <table width="428" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg1" style="height:36px">
                              <tr>
                                <td width="428" height="36" align="left" valign="middle">&nbsp;</td>
                              </tr>
                            </table></td>
                            </tr>
                            <tr>
                            <td colspan="4">
                            <table width="428" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg1" style="height:36px">
                             <td width="428" height="36" align="left" valign="middle" class="text2"><asp:Label ID="lblLoadId1" runat="server"></asp:Label></td>
                            </table></td>
                            </tr>
                            <tr>
                            <td colspan="4">
                            <table width="428" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg1" style="height:36px">
                             <td width="428" height="36" align="left" valign="middle">&nbsp;</td>
                            </table></td>
                            </tr>        
                            <tr>
                            <td colspan="4">
                            <table width="428" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg1" style="height:36px">
                            <tr>
                            <td width="67" height="16" align="center" valign="bottom" class="text3">&nbsp;</td>
                            <td width="294" height="16" align="left" valign="bottom" class="text3">&nbsp;</td>
                            <td width="67" height="16" align="left" valign="top" class="text3">STAY WITH</td>
                            </tr>
                            <tr>
                            <td width="67" height="20" align="center" valign="middle" class="text2">&nbsp;</td>
                            <td width="294" height="20" align="left" valign="middle" class="text2">&nbsp;</td>
                            <td width="67" height="20" align="left" valign="middle" class="text2"></td>
                            </tr>
                            </table>
                            </td>
                            </tr>
                            <tr>
                            <td colspan="4">
                            <table width="428" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg1" style="height:36px">
                            <tr>
                            <td width="94" height="16" align="left" valign="top" class="text3">TIME IN</td>
                            <td width="94" height="16" align="left" valign="top" class="text3">TIME OUT</td>
                            <td width="172" height="16" align="left" valign="bottom" class="text3">&nbsp;</td>
                            <td width="68" height="16" align="left" valign="top" class="text3">STAY WITH</td>
                            </tr>
                            <tr>
                            <td width="94" height="20" align="left" valign="middle" class="text2"></td>
                           <td width="94" height="20" align="left" valign="middle" class="text2"></td>
                            <td width="172" height="20" align="left" valign="middle" class="text2">&nbsp;</td>
                            <td width="68" height="20" align="left" valign="middle" class="text2"></td>
                            </tr>
                            </table>
                            </td>
                            </tr>
                            <tr>
                            <td colspan="4">
                            <table width="428" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg1" style="height:36px">
                            <tr>
                            <td width="160" height="16" align="left" valign="top" class="text3">TERMINATE TO</td>
                            <td width="197" height="16" align="left" valign="top" class="text3">&nbsp;</td>
                            <td width="71" height="16" align="left" valign="top" class="text3">&nbsp;</td>
                            </tr>
                            <tr>
                            <td width="160" height="20" align="left" valign="middle" class="text2"><asp:Label ID="lblTerminated1" runat="server"></asp:Label></td>
                           <td width="197" height="20" align="left" valign="middle" class="text2">&nbsp;</td>
                           <td width="71" height="20" align="left" valign="top" class="text3">&nbsp;</td>
                            </tr>
                            </table>
                            </td>
                            </tr>
                            <tr>
                            <td colspan="4">
                            <table width="428" border="0" align="center" cellpadding="0" cellspacing="0" class="tblbg1" style="height:114px">
                            <tr>
                            <td width="219" height="114" align="left" valign="top" class="text3">
                            <table width="219" border="0" cellpadding="0" cellspacing="0" height="114">
                              <tr>
          	                    <td width="219" height="50" align="left" valign="top">
                                <table width="219" border="0" cellpadding="0" cellspacing="0" 	height="50">
                                  <tr>
                                    <td width="124" height="20" align="left" valign="top" class="text3">BILL TO</td>
                                    <td width="95" height="20" align="left" valign="top" class="text3">AGENT</td>
                                  </tr>
                                  <tr>
                                    <td width="124" height="30" align="left" valign="middle" class="text2"><asp:Label ID="lblBillTo1" runat="server"></asp:Label></td>
                                    <td width="95" height="30" align="left" valign="middle" class="text2">&nbsp;</td>
                                  </tr>
                                </table>
                                </td>            
                              </tr>
                              <tr>
          	                    <td width="219" height="64" align="left" valign="middle">
                                <table width="219" border="0" cellpadding="0" cellspacing="0" 	height="64">
            	                    <tr>
                	                    <td width="219" height="24" align="left" valign="middle" class="text3">
                    	                    REF #
                                        </td>
                                    </tr>
                                    <tr>
                	                    <td width="219" height="40" align="left" valign="middle" class="text2">
                	                    <asp:Label ID="lblRef1" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>            		
                                </td>
                                </tr>
                            </table></td>
                           <td width="209" height="85" align="left" valign="top" class="text3">
                           <table width="209" border="0" cellpadding="0" cellspacing="0" height="114">
                             <tr>
                               <td align="left" valign="top">
                               <table width="209" height="50" border="0" cellpadding="0" cellspacing="0">
                               <tr>
           		                    <td width="63" height="20" align="left" valign="top" class="text3">RATE</td>
                                    <td width="75" height="20" align="left" valign="top" class="text3">EXTRA CHRG</td>
                                    <td width="71" height="20" align="left" valign="top" class="text3">TOTEL</td>
                               </tr>
                                <tr>
           		                    <td width="63" height="30" align="left" valign="middle" class="text2"><asp:Label ID="lblRate1" runat="server"></asp:Label></td>
                                    <td width="75" height="30" align="left" valign="middle" class="text2"></td>
                                    <td width="71" height="30" align="left" valign="middle" class="text2"><asp:Label ID="lblTotal1" runat="server"></asp:Label></td>
                               </tr>
                               </table>
                               </td>
                             </tr>
                             <tr>
                                <td align="left" valign="top">
                                <table width="209" height="64" border="0" cellpadding="0" cellspacing="0">
            	                    <tr>
                	                    <td width="209" height="24" align="left" valign="top" class="text3">
                                        DRIVER RATE
                                        </td>
                                    </tr>
                                    <tr>
                	                    <td width="209" height="40" align="left" valign="middle" class="text2">
                                        </td>
                                    </tr>
                                </table>
                                </td>
                             </tr>
                           </table></td>
                            </tr>
                            </table>
                            </td>
                            </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="HiddenField1" runat="server" />
    </form>    
</body>
</html>
