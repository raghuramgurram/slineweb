using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class ForgotPassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblReq.Visible = false;
            lblErrMsg.Visible = false;
            lblEmailId.Visible = false;
        }
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Login.aspx");
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        CheckUserDetails();
    }

    private void CheckUserDetails()
    {
        if (Page.IsValid)
        {
            lblReq.Visible = false;
            lblErrMsg.Visible = false;
            lblEmailId.Visible = false;
            string UserId="";
            if (txtEmailId.Text.Trim().Length > 0 || txtUserId.Text.Trim().Length > 0)
            {
                DataSet ds = new DataSet();
                if (txtUserId.Text.Trim().Length>0)
                {
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_CheckUserId", new object[] { txtUserId.Text.Trim() });
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (Convert.ToString(ds.Tables[0].Rows[0][0]).Trim().Length > 0)
                            {
                                GenaratePassword(Convert.ToString(ds.Tables[0].Rows[0][0]).Trim(), Convert.ToString(ds.Tables[0].Rows[0][1]).Trim(), Convert.ToString(ds.Tables[0].Rows[0][2]).Trim());
                                return;
                            }
                            else
                            {
                                lblErrMsg.Visible = true;
                                lblEmailId.Visible = false;
                                txtUserId.Text = "";
                            }
                        }
                        else
                        {
                            lblErrMsg.Visible = true;
                            lblEmailId.Visible = false;
                            txtUserId.Text = "";
                        }
                    }
                }

                if (txtEmailId.Text.Trim().Length > 0)
                {
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_CheckEmailId", new object[] { txtEmailId.Text.Trim() });
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (Convert.ToString(ds.Tables[0].Rows[0][0]).Trim().Length > 0)
                            {
                                GenaratePassword(Convert.ToString(ds.Tables[0].Rows[0][0]).Trim(), Convert.ToString(ds.Tables[0].Rows[0][1]).Trim(), Convert.ToString(ds.Tables[0].Rows[0][2]).Trim());
                                return;
                            }
                            else
                            {
                                lblEmailId.Visible = true;
                                lblErrMsg.Visible = false;
                                txtEmailId.Text = "";
                            }
                        }
                        else
                        {
                            lblEmailId.Visible = true;
                            lblErrMsg.Visible = false;
                            txtEmailId.Text = "";
                        }
                    }
                }
                if (ds != null)
                { ds.Dispose(); ds = null; }
            }
            else
            {
                lblReq.Visible = true;
                return;
            }
        }
    }

    private void GenaratePassword(string UserId, string Password,string EmailId)
    {
        if (Password != null)
        {
            CommonFunctions.SendEmail(ConfigurationSettings.AppSettings["SmtpUserName"].ToString(), EmailId, "", "",
            "Password Details",
            "Dear " + UserId + ",<br/><p> Your Login Details are as below <br/>" + 
            "User Id : "+UserId+"<br/>Password : "+Password, null,null);

            //Response.Redirect("~/Login.aspx");            
            pnlSendMail.Visible = true;
        }
    }
}
