<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GridBar.ascx.cs" Inherits="UserControls_GridBar" %>
<link type="text/css" href="../Styles/OwnStyle.css" rel="stylesheet" />
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="GridBar">
<tr>
    <td>
        <asp:Label ID="lblText" runat="server"></asp:Label>
    </td>
    <td align="right">
        <asp:PlaceHolder ID="plhLinks" runat="server"></asp:PlaceHolder>
        <asp:Label ID="lblLine" Visible="false" runat="server"> &nbsp;|&nbsp;</asp:Label><asp:LinkButton ID="lnkReopen" runat="server" Text="Re Open" Visible="false" OnClientClick="return confirm('Are you sure you want to re-open this Load?');" OnClick="lnkReopen_Click"></asp:LinkButton>
        <asp:Label ID="lblRightText" runat="server" Visible="false"></asp:Label>
        <asp:PlaceHolder ID="plhLogLink" runat="server">
        
        </asp:PlaceHolder>
    </td>    
</tr>
</table>
