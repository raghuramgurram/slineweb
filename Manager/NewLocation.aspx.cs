using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class NewLocation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblShowError.Visible = false;
        //Session["TopHeaderText"] = "New / Edit Location ";
        if (!IsPostBack)
        {
            if (Request.QueryString.Count > 0)
            {
                if (Convert.ToString(Request.QueryString[0]).Trim().IndexOf('^') > 0)
                {
                    string[] strCompInfo = Request.QueryString[0].Trim().Replace("%5e", "^").Split('^');
                    if (strCompInfo.Length == 2)
                    {
                        lblCompanyName.Visible = true;
                        lblCompanyName.Text = strCompInfo[1];
                        txtCompanyName.Text = strCompInfo[1];
                        txtCompanyName.Visible = false;
                        ViewState["CompanyID"] = strCompInfo[0];
                        this.Page.SetFocus(txtStreet.ClientID);
                        Session["TopHeaderText"] = "New Location ";                        
                    }
                    ViewState["Mode"] = "Edit";
                }
                else
                {
                    ViewState["Mode"] = "Edit";
                    ViewState["LocationID"] = Convert.ToInt64(Request.QueryString[0]);
                    FillDetails();
                    Session["TopHeaderText"] = "Edit Location ";   
                }
            }
            else
            {
                ViewState["Mode"] = "New";
                txtCompanyName.Visible = true;
                lblCompanyName.Visible = false;
                this.Page.SetFocus(txtCompanyName.ClientID);
                Session["TopHeaderText"] = "New Company & Location ";
            }
        }        
    }

    private void FillDetails()
    {
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLocationDetails", new object[] { Convert.ToInt64(ViewState["LocationID"]) });
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblCompanyName.Visible = true;
                txtCompanyName.Text = Convert.ToString(ds.Tables[0].Rows[0][0]);
                lblCompanyName.Text = Convert.ToString(ds.Tables[0].Rows[0][0]);
                txtStreet.Text = Convert.ToString(ds.Tables[0].Rows[0][1]);
                txtSuite.Text = Convert.ToString(ds.Tables[0].Rows[0][2]);
                txtCity.Text = Convert.ToString(ds.Tables[0].Rows[0][3]);
                ddlState.Text = Convert.ToString(ds.Tables[0].Rows[0][4]);
                txtZip.Text = Convert.ToString(ds.Tables[0].Rows[0][5]);
                txtPhone.Text = Convert.ToString(ds.Tables[0].Rows[0][6]);
                txtFax.Text = Convert.ToString(ds.Tables[0].Rows[0][7]);
                txtWebsiteURL.Text = Convert.ToString(ds.Tables[0].Rows[0][8]);
                txtDirections.Text = Convert.ToString(ds.Tables[0].Rows[0][9]);
                ViewState["CompId"] = Convert.ToString(ds.Tables[0].Rows[0][10]);
                radLocationType.SelectedIndex = radLocationType.Items.IndexOf(radLocationType.Items.FindByValue(Convert.ToString(ds.Tables[0].Rows[0][11])));
            }
        }
        if (ds != null) { ds.Dispose(); ds = null; }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        CheckBackPage();
        if (Convert.ToString(ViewState["CompanyID"]).Trim().Length > 0)
        {
            Response.Redirect("~/Manager/SearchLocation.aspx?" + Convert.ToString(ViewState["CompanyID"]));
        }
        else if (ViewState["CompId"] != null)
        {
            Response.Redirect("~/Manager/SearchLocation.aspx?" + Convert.ToString(ViewState["CompId"]));

        }
        else
            Response.Redirect("~/Manager/SearchLocation.aspx");
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (txtCompanyName.Visible)
            {
                if (Convert.ToInt32(DBClass.executeScalar("select count([bint_CompanyId]) from eTn_Company where nvar_CompanyName='" + txtCompanyName.Text.Trim() + "'")) == 0)
                {
                    //SLine 2017 Enhancement for Office Location Starts
                    long officeLocationId = 0;
                    if (Session["OfficeLocationID"] != null)
                    {
                        officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
                    }
                    object[] parms = new object[] { Convert.ToString(txtCompanyName.Text.Trim()), Convert.ToString(txtStreet.Text.Trim()), Convert.ToString(txtSuite.Text.Trim()), Convert.ToString(txtCity.Text.Trim()),
                 Convert.ToString(ddlState.Text.Trim()),Convert.ToString(txtZip.Text.Trim()),txtPhone.Text,txtFax.Text,Convert.ToString(txtWebsiteURL.Text.Trim()), Convert.ToString(txtDirections.Text.Trim()),radLocationType.SelectedItem.Value,officeLocationId};
                    //SLine 2017 Enhancement for Office Location Ends
                    if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_CreateNewCompanyAndLocation", parms) > 0)
                    {
                        CheckBackPage();
                        Response.Redirect("~/Manager/SearchLocation.aspx?" + Convert.ToString(ViewState["CompanyID"]));
                    }
                    parms = null;
                }
                else
                {
                    lblShowError.Visible = true;
                }
            }
            else if(lblCompanyName.Visible)
            {
                if (ViewState["CompanyID"] != null)
                {
                    object[] parms = new object[] { Convert.ToInt32(ViewState["CompanyID"]), Convert.ToString(txtStreet.Text.Trim()), Convert.ToString(txtSuite.Text.Trim()), Convert.ToString(txtCity.Text.Trim()),
                        Convert.ToString(ddlState.Text.Trim()),Convert.ToString(txtZip.Text.Trim()),CommonFunctions.FormatPhone(txtPhone.Text),CommonFunctions.FormatPhone(txtFax.Text),Convert.ToString(txtWebsiteURL.Text.Trim()), Convert.ToString(txtDirections.Text.Trim()),radLocationType.SelectedItem.Value};
                    if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_AddLocation", parms) > 0)
                    {
                        CheckBackPage();
                        Response.Redirect("~/Manager/SearchLocation.aspx?" + Convert.ToString(ViewState["CompanyID"]));
                    }
                    parms = null;
                }
                else if (ViewState["LocationID"] != null && ViewState["CompId"]!=null)
   			   	{
                    object[] parms = new object[] { Convert.ToInt32(ViewState["LocationID"]), Convert.ToString(txtStreet.Text.Trim()), Convert.ToString(txtSuite.Text.Trim()), Convert.ToString(txtCity.Text.Trim()),
                        Convert.ToString(ddlState.Text.Trim()),Convert.ToString(txtZip.Text.Trim()),CommonFunctions.FormatPhone(txtPhone.Text),CommonFunctions.FormatPhone(txtFax.Text),Convert.ToString(txtWebsiteURL.Text.Trim()), Convert.ToString(txtDirections.Text.Trim()),radLocationType.SelectedItem.Value};
                    if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_EditLocation", parms) > 0)
                    {
                        CheckBackPage();
                        Response.Redirect("~/Manager/SearchLocation.aspx?" + Convert.ToString(ViewState["CompId"]));
                    }
                    parms = null;
                }		   			   		
            }
        }
    }
    private void CheckBackPage()
    {
        if (Session["BackPage"] != null)
        {
            if (Convert.ToString(Session["BackPage"]).Trim().Length > 0)
            {
                string strTemp = Convert.ToString(Session["BackPage"]).Trim();
                Session["BackPage"] = null;
                Response.Redirect(strTemp);
            }
        }
    }

}
