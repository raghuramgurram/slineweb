using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class AccessorialCharges : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Accessorial Charges";
        this.Page.Title = lblCompanyName.Text = GetFromXML.CompanyName;
       // this.Page.Title = lblCompanyName.Text = Convert.ToString(ConfigurationSettings.AppSettings["CompanyName"]);
        lblPhone.Text = GetFromXML.CompanyPhone + " " + GetFromXML.CompanyFax;
        if (!IsPostBack)
        {
            if (Request.QueryString.Count > 0)
            {
                ViewState["ID"] = Convert.ToInt64(Request.QueryString[0]);
                FillDetails();
            }
        }
    }

    private void FillDetails()
    {
        if (ViewState["ID"] != null)
        {
            lblLoad.Text = Convert.ToString(ViewState["ID"]);
            string strQuery = "SELECT C.[nvar_CustomerName],C.[nvar_Street],C.[nvar_Suite],C.[nvar_City],C.[nvar_State],C.[nvar_Zip],L.[nvar_Ref],T.[nvar_LoadTypeDesc],CT.[nvar_CommodityTypeDesc],L.[nvar_Booking]," +
                               "L.[nvar_Container],L.[nvar_Container1],L.[nvar_Chasis],L.[nvar_Chasis1],L.[nvar_Remarks],L.[nvar_Description]," +
                               "case when len(convert(nvarchar(20),C.num_Phone))=10 then '<br/>Tel: '+substring(convert(nvarchar(10),C.num_Phone),1,3)+'-'+substring(convert(nvarchar(10),C.num_Phone),4,3)+'-'+substring(convert(nvarchar(10),C.num_Phone),7,4) else '' end ,"+
	                           " case when len(convert(nvarchar(20),C.[num_Fax]))=10 then '<br/>Fax: '+substring(convert(nvarchar(10),C.[num_Fax]),1,3)+'-'+substring(convert(nvarchar(10),C.[num_Fax]),4,3)+'-'+substring(convert(nvarchar(10),C.[num_Fax]),7,4) else '' end "+
                               "FROM [eTn_Load] L inner join [eTn_Customer] C on L.[bint_CustomerId]=C.[bint_CustomerId] inner join [eTn_LoadType] T on L.[bint_LoadTypeId]=T.[bint_LoadTypeId]  " +
                               "inner join [eTn_CommodityType] CT on L.[bint_CommodityTypeId]=CT.[bint_CommodityTypeId] where L.[bint_LoadId]=" + Convert.ToString(ViewState["ID"]);
            DataTable dt = DBClass.returnDataTable(strQuery);
            if (dt.Rows.Count > 0)
            {
                lblBillTo.Text = "<strong>" + Convert.ToString(dt.Rows[0][0]) + "</strong><br/>" + Convert.ToString(dt.Rows[0][1]) + "  " + Convert.ToString(dt.Rows[0][2]) + "<br/>";
                lblBillTo.Text += Convert.ToString(dt.Rows[0][3]) + ", " + Convert.ToString(dt.Rows[0][4]) + " " + Convert.ToString(dt.Rows[0][5]) + Convert.ToString(dt.Rows[0][16]) +  Convert.ToString(dt.Rows[0][17])+"<br/>";
                lblBillingRef.Text = Convert.ToString(dt.Rows[0][6]);
                lblType.Text = Convert.ToString(dt.Rows[0][7]);
                lblCommodity.Text = Convert.ToString(dt.Rows[0][8]);
                lblBooking.Text = Convert.ToString(dt.Rows[0][9]);
                lblContainer.Text = Convert.ToString(dt.Rows[0][10]);
                lblChassie.Text = Convert.ToString(dt.Rows[0][12]);
                lblremarks.Text = Convert.ToString(dt.Rows[0][14]);
                lblDesc.Text = Convert.ToString(dt.Rows[0][15]);
            }
            LoadReceivables1.LoadId = Convert.ToString(ViewState["ID"]);
            LoadReceivables1.BackButtonVisible = false;
            LoadReceivables1.RedirectBackURL = "~/Manager/AccessorialCharges.aspx?" + Convert.ToString(ViewState["ID"]);
        }
    }

    //private void FillDetails(long ID)
    //{
    //    lblLoad.Text = ID.ToString();
    //    //string strQuery = "SELECT C.[nvar_CustomerName],C.[nvar_Street],C.[nvar_Suite],C.[nvar_City],C.[nvar_State],C.[nvar_Zip],L.[nvar_Ref],T.[nvar_LoadTypeDesc],CT.[nvar_CommodityTypeDesc],L.[nvar_Booking]," +
    //    //                   "L.[nvar_Container],L.[nvar_Container1],L.[nvar_Chasis],L.[nvar_Chasis1],L.[nvar_Remarks],L.[nvar_Description]" +
    //    //                   "FROM [eTn_Load] L inner join [eTn_Customer] C on L.[bint_CustomerId]=C.[bint_CustomerId] inner join [eTn_LoadType] T on L.[bint_LoadTypeId]=T.[bint_LoadTypeId]  " +
    //    //                   "inner join [eTn_CommodityType] CT on L.[bint_CommodityTypeId]=CT.[bint_CommodityTypeId] where L.[bint_LoadId]="+ID;

    //    DataSet ds=SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetAccessorialChargesDetails", new object[] { ID });
    //    //DataTable dt = DBClass.returnDataTable(strQuery);
    //    if (ds.Tables.Count > 0)
    //    {
    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            lblBillTo.Text = Convert.ToString(ds.Tables[0].Rows[0][0]) + "<br/>" + Convert.ToString(ds.Tables[0].Rows[0][1]) + "  " + Convert.ToString(ds.Tables[0].Rows[0][2]) + "<br/>";
    //            lblBillTo.Text += Convert.ToString(ds.Tables[0].Rows[0][3]) + ", " + Convert.ToString(ds.Tables[0].Rows[0][4]) + " " + Convert.ToString(ds.Tables[0].Rows[0][5]);
    //            lblBillingRef.Text = Convert.ToString(ds.Tables[0].Rows[0][6]);
    //            lblType.Text = Convert.ToString(ds.Tables[0].Rows[0][7]);
    //            lblCommodity.Text = Convert.ToString(ds.Tables[0].Rows[0][8]);
    //            lblBooking.Text = Convert.ToString(ds.Tables[0].Rows[0][9]);
    //            lblContainer.Text = Convert.ToString(ds.Tables[0].Rows[0][10]);
    //            lblChassie.Text = Convert.ToString(ds.Tables[0].Rows[0][12]);
    //            lblremarks.Text = Convert.ToString(ds.Tables[0].Rows[0][14]);
    //            lblDesc.Text = Convert.ToString(ds.Tables[0].Rows[0][15]);
    //        }
    //        if (ds.Tables[1].Rows.Count > 0)
    //        {
    //            lblDryRun.Text = Convert.ToString(ds.Tables[1].Rows[0][0]).Trim();
    //            lblExtraStops.Text = Convert.ToString(ds.Tables[1].Rows[0][1]).Trim();
    //            lblLumCharges.Text = Convert.ToString(ds.Tables[1].Rows[0][2]).Trim();
    //            lblDetention.Text = Convert.ToString(ds.Tables[1].Rows[0][3]).Trim();
    //            lblChassisSplit.Text = Convert.ToString(ds.Tables[1].Rows[0][4]).Trim();
    //            lblChassisRent.Text = Convert.ToString(ds.Tables[1].Rows[0][5]).Trim();
    //            lblPallet.Text = Convert.ToString(ds.Tables[1].Rows[0][6]).Trim();
    //            lblContainerWash.Text = Convert.ToString(ds.Tables[1].Rows[0][7]).Trim();
    //            lblYardStorage.Text = Convert.ToString(ds.Tables[1].Rows[0][8]).Trim();
    //            lblRampPull.Text = Convert.ToString(ds.Tables[1].Rows[0][9]).Trim();
    //            lblTriaxle.Text = Convert.ToString(ds.Tables[1].Rows[0][10]).Trim();
    //            lblTransLoad.Text = Convert.ToString(ds.Tables[1].Rows[0][11]).Trim();
    //            lblWaitingTime.Text = Convert.ToString(ds.Tables[1].Rows[0][12]).Trim();
    //            lblScaleCharges.Text = Convert.ToString(ds.Tables[1].Rows[0][13]).Trim();
    //            lblOtherCharges.Text = Convert.ToString(ds.Tables[1].Rows[0][14]).Trim();
    //            lblExplanation.Text = Convert.ToString(ds.Tables[1].Rows[0][15]).Trim();
    //            lblTotal.Text = Convert.ToString(ds.Tables[1].Rows[0][16]).Trim();
    //            lblTotalCharges.Text = Convert.ToString(ds.Tables[1].Rows[0][17]).Trim();
    //        }
    //    }
    //    if (ds != null) { ds.Dispose(); ds = null; }
    //}
    protected void btnBack_Click(object sender, EventArgs e)
    {
        CheckBackPage();
        if (ViewState["ID"] != null)
        {
            Response.Redirect("~/Manager/ShowReceivables.aspx?" + Convert.ToString(ViewState["ID"]) + "^");
        }
    }

    protected void btnSendMail_Click(object sender, EventArgs e)
    {
        string[] filePaths;
        DataTable dt = DBClass.returnDataTable("select [nvar_DocumentName],isnull([bint_FileNo],0),[date_ReceivedDate] from [eTn_UploadDocuments] where [bint_LoadId]=" + Convert.ToInt64(ViewState["ID"]) + " and [bint_DocumentId] in (4,7) and [nvar_DocumentName] is not null");
        if (dt != null && dt.Rows.Count > 0)
        {
            filePaths = new string[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                filePaths[i] = Server.MapPath("../Documents" + "\\" + Convert.ToString(ViewState["ID"]).Trim() + "\\" + Convert.ToString(dt.Rows[i][0]));
            }
        }
        else
        {
            filePaths = new string[0];
        }
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriveronWaitingDetailsWithOutStstusId", new object[] { Convert.ToInt64(ViewState["ID"]) });
        DataSet ds1 = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadEmailsByLoadId", new object[] { Convert.ToInt64(ViewState["ID"]) });
        if (ds.Tables.Count == 5 && ds.Tables[4].Rows.Count > 0 && Convert.ToString(ds.Tables[4].Rows[0][0]).Trim() != "0.00" && Convert.ToString(ds.Tables[4].Rows[0][0]).Trim() != "0")
        {
            string toAddress = string.Empty;
            if (ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
            {
                toAddress = !string.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0][0])) ? Convert.ToString(ds1.Tables[0].Rows[0][0]) : !string.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0][1])) ? Convert.ToString(ds1.Tables[0].Rows[0][1]) : Convert.ToString(ds.Tables[1].Rows[0][0]);
            }
            else
            {
                toAddress = Convert.ToString(ds.Tables[1].Rows[0][0]);
            }
            try
            {
                CommonFunctions.SendEmail(GetFromXML.FromAccessrial, toAddress, GetFromXML.CCAccessorial, "", GetAccerialSubject(Convert.ToString(ViewState["ID"]), Convert.ToString(ds.Tables[2].Rows[0][10]), Convert.ToString(ds.Tables[2].Rows[0][6])), GetAccerialChargesBody(ds), null, filePaths);
            }
            catch (Exception ex)
            { }

        }
    }

    private void CheckBackPage()
    {
        if (Session["BackPage3"] != null)
        {
            if (Convert.ToString(Session["BackPage3"]).Trim().Length > 0)
            {
                string strTemp = Convert.ToString(Session["BackPage3"]).Trim();
                Session["BackPage3"] = null;
                Response.Redirect(strTemp);
            }
        }
        else if (Session["BackPage4"] != null)
        {
            if (Convert.ToString(Session["BackPage4"]).Trim().Length > 0)
            {
                string strTemp = Convert.ToString(Session["BackPage4"]).Trim();
                Session["BackPage4"] = null;
                Response.Redirect(strTemp);
            }
        }
    }

    private string GetAccerialChargesBody(DataSet ds)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();
        strBody.Append("<%@ Page AutoEventWireup=true CodeFile='AccessorialCharges.aspx.cs' Inherits='AccessorialCharges' Language='C#' MasterPageFile='~/Manager/MasterPage.master' %>");
        strBody.Append("<asp:Content ID='Content1' ContentPlaceHolderID='ContentPlaceHolder1' Runat='Server'>");
        strBody.Append("<table width='100%' border='0' cellpadding='3' cellspacing='0' id='ContentTbl'><tr valign='top'><td><table width='100%' border='0' cellspacing='0' cellpadding='0'>");
        strBody.Append("<tr><td align='center' class='head1'>ACCEPTANCE FORM <br/> Accessorial Charges </td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img alt='' height='10' src='../Images/pix.gif' width='1'/></td></tr></table>");
        strBody.Append("<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td style='background-color: #CCCCCC; left: 10px;'><img src='../Images/pix.gif' width='1' height='1' /></td></tr></table>");
        strBody.Append("<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td><img src='../Images/pix.gif' alt=' width='1' height='10' /></td></tr></table>");
        strBody.Append("<table border='0' cellpadding='0' cellspacing='0' style='padding-right: 5px; padding-left: 15px;height: 20px; padding-top: 1px; padding-bottom: 1px;' width='100%'><tr><td width='50%' valign='top'>");
        strBody.Append("<table border='0' cellpadding='0' cellspacing='0' style='font-family: Arial, sans-serif;color: #4D4B4C; font-size: 12px;' width='100%'><tr><td width='30%' valign='top'><p><strong>Bill To:</strong><br />");
        strBody.Append("<strong>" + Convert.ToString(ds.Tables[2].Rows[0][0]) + "</strong><br/>" + Convert.ToString(ds.Tables[2].Rows[0][1]) + "  " + Convert.ToString(ds.Tables[2].Rows[0][2]) + "<br/>" + Convert.ToString(ds.Tables[2].Rows[0][3]) + ", " + Convert.ToString(ds.Tables[2].Rows[0][4]) + " " + Convert.ToString(ds.Tables[2].Rows[0][5]) + Convert.ToString(ds.Tables[2].Rows[0][16]) + Convert.ToString(ds.Tables[2].Rows[0][17]));
        strBody.Append("<br /><br/><strong>Billing Ref.# </strong>:" + Convert.ToString(ds.Tables[2].Rows[0][6]) + "</p></td>");
        strBody.Append("<td valign='top'><strong>Load Number&nbsp;:&nbsp;" + Convert.ToString(ViewState["ID"]) + "</strong><br />");
        strBody.Append("Type&nbsp;: " + Convert.ToString(ds.Tables[2].Rows[0][7]) + "<br />");
        strBody.Append("Commodity&nbsp;: " + Convert.ToString(ds.Tables[2].Rows[0][8]) + "<br />");
        strBody.Append("Booking#&nbsp;: " + Convert.ToString(ds.Tables[2].Rows[0][9]) + "<br />");
        strBody.Append("Container#&nbsp;:&nbsp;" + Convert.ToString(ds.Tables[2].Rows[0][10]) + "<br />");
        strBody.Append("Chassis#&nbsp;:&nbsp;" + Convert.ToString(ds.Tables[2].Rows[0][12]) + "<br />");
        strBody.Append("Remarks&nbsp;:&nbsp;" + Convert.ToString(ds.Tables[2].Rows[0][14]) + "<br />");
        strBody.Append("Description&nbsp;:&nbsp;" + Convert.ToString(ds.Tables[2].Rows[0][15]) + "</td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0' style='font-family: Arial, sans-serif;color: #4D4B4C; font-size: 12px;'><tr><td><img src='../Images/pix.gif' alt='' width='1' height='10' /></td></tr><tr id='row'><td align=left><strong>" + GetFromXML.CompanyName + "<br/>" + GetFromXML.CompanyPhone + "</strong></td>");
        //strBody.Append("<td align='right'><strong>" + ConfigurationSettings.AppSettings["CompanyPhone"] + "&nbsp;&nbsp;&nbsp;" + ConfigurationSettings.AppSettings["CompanyFax"] + "</strong></td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0' id='TABLE1'><tr><td bgcolor='#CCCCCC'><img src='../Images/pix.gif' alt='' width='1' height='1' /></td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img src='../Images/pix.gif' alt='' width='1' height='10'/></td></tr></table>");
        strBody.Append("<table border='0' cellpadding='0' cellspacing='0' style='font-family: Arial, sans-serif;color: #4D4B4C; font-size: 12px;' width='100%'>");
        strBody.Append("<tr><td valign='top'>Dear Customer,<br />Our company request authorization for the additional charges. please sign below and " + GetFromXML.CompanyFax + " with in 24 hrs after receiving this form.</td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img src='../Images/pix.gif' alt=' width='1' height='10' /></td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td bgcolor='#CCCCCC'><img src='../Images/pix.gif' alt='' width='1' height='1' /></td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img src='../Images/pix.gif' alt='' width='1' height='10'/></td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td>" + DesginPaybleDetails(ds.Tables[3]) + "</td></tr></table>");
        //need to write code for charges
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img src='../Images/pix.gif' alt=' width='1' height=10'/></td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td bgcolor='#CCCCCC'><img src='../Images/pix.gif' alt='' width='1' height='1'/></td></tr></table>");
        strBody.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td><img src='../Images/pix.gif' alt='' width='1' height='25' /></td></tr></table>");
        strBody.Append("<table border='0' cellpadding='0' cellspacing='0' style='font-family: Arial, sans-serif;font-size: 12px;' width='100%'><tr><td width='50%' valign='top'><p>Authorized by (Signature) : ________________________ </p></td><td valign='top'>Date : ________________________ </td></tr><tr><td valign='top'></td><td valign='top'></td></tr><tr><td><img alt='' height='25' src='../Images/pix.gif' width='1' /></td></tr>");
        strBody.Append("<tr><td valign='top'>Name : ________________________ </td><td valign='top'></td></tr></table></td></tr></table></td></tr></table></asp:Content>");
        return strBody.ToString();
    }
    private string GetAccerialSubject(string LoadID, string Container, string RefNo)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();

        strBody.Append("Accessorial Charges for Load# " + LoadID);
        if (RefNo.Length > 0)
            strBody.Append(" ,  Ref #  " + RefNo);
        if (Container.Length > 0)
            strBody.Append(" , Container# " + Container);

        return strBody.ToString();
    }
    public string DesginPaybleDetails(DataTable dtPayables)
    {
        System.Text.StringBuilder strCode = new System.Text.StringBuilder();
        strCode.Append("");
        if (dtPayables.Rows.Count > 0)
        {
            bool blVal = false;
            for (int i = 0, j = 0; i < dtPayables.Columns.Count; i++)
            {
                #region Validations
                blVal = false;
                if (Convert.IsDBNull(dtPayables.Rows[0][i]))
                    dtPayables.Rows[0][i] = 0;
                if (Convert.ToString(dtPayables.Rows[0][i]).Length == 0)
                    dtPayables.Rows[0][i] = 0;
                try
                {
                    if (Convert.ToDouble(dtPayables.Rows[0][i]) > 0)
                    {
                        blVal = true;
                    }
                }
                catch
                {
                    if (Convert.ToString(dtPayables.Rows[0][i]).Trim().Length > 0)
                    {
                        blVal = true;
                    }
                }
                #endregion
                if (blVal)
                {
                    strCode.Append("<tr id=" + (((j % 2) == 0) ? "row" : "altrow") + " >");
                    if (i < (dtPayables.Columns.Count - 1))
                        strCode.Append("<td align=right width=30%>" + dtPayables.Columns[i].ColumnName + "</td>");
                    else
                        strCode.Append("<td align=right width=30%><b>" + dtPayables.Columns[i].ColumnName + "</b></td>");
                    strCode.Append("<td align=left>" + Convert.ToString(dtPayables.Rows[0][i]).Trim() + "</td>");
                    strCode.Append("</tr>");
                    j = j + 1;
                }
            }
            if (strCode.ToString().Length > 0)
            {
                strCode.Insert(0, "<table width=100% border=0 cellspacing=0 cellpadding=0 id=tblform> <tr><th colspan=2>Accessorial Charges($)</th></tr>");
                strCode.Append("</table>");
            }
            else
            {
                strCode.Append("<table width=100% border=0 cellspacing=0 cellpadding=0 id=tblform> <tr><th colspan=2>Accessorial Charges($)</th></tr>");
                strCode.Append("<tr id=row>");
                strCode.Append("<td align=center width=30%><b>No Charges Available for Load : " + Convert.ToString(ViewState["ID"]) + " </b></td>");
                strCode.Append("</tr>");
                strCode.Append("</table>");
            }
        }
        else
        {
            strCode.Append("<table width=100% border=0 cellspacing=0 cellpadding=0 id=tblform> <tr><th colspan=2>Accessorial Charges($)</th></tr>");
            strCode.Append("<tr id=row>");
            strCode.Append("<td align=center width=30%><b>No Charges Available for Load : " + Convert.ToString(ViewState["ID"]) + " </b></td>");
            strCode.Append("</tr>");
            strCode.Append("</table>");
        }
        return strCode.ToString();
    }
}
