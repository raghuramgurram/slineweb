<%@ Page Language="C#" MasterPageFile="~/Manager/MasterPage.master" AutoEventWireup="true" CodeFile="PayableExport.aspx.cs" Inherits="Manager_Default" Title="Untitled Page" %>

<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <style>

        #ContentTbl {
            height: auto !important;
        }

        .text-right {
            text-align: right;
        }
    </style>
       <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="updpage" runat="server"  UpdateMode="Conditional" ChildrenAsTriggers="false">
            <ContentTemplate>
                <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td valign="top" align="left">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #4B4B4B;">
                                <tr valign="top">
                                    <td style="height: 5px;">&nbsp;From:
                            <uc1:DatePicker ID="dtpFromDate" runat="server" CountNextYears="2" CountPreviousYears="0"
                                IsDefault="false" IsRequired="false" SetInitialDate="true"></uc1:DatePicker>
                                        &nbsp; &nbsp; &nbsp; To:
                            <uc1:DatePicker ID="dtpToDate" runat="server" CountNextYears="2" CountPreviousYears="0"
                                IsDefault="false" IsRequired="false" SetInitialDate="true"></uc1:DatePicker>
                                        &nbsp; &nbsp;
                            <asp:Button ID="btnDateSearch" runat="server" CssClass="btnstyle" OnClick="btnDateSearch_Click"
                                Text="Search" />
                                    </td>
                                    <%--<td align="right" style="padding-right: 20px;">Note :&nbsp;<asp:TextBox ID="txtNote" runat="Server" Width="240px"></asp:TextBox>
                                        &nbsp;<asp:Button ID="btnExport" runat="server" CssClass="btnstyle" Text="Export" OnClick="btnExport_Click" />
                                    </td>--%>
                                </tr>
                                <tr>
                                    <td style="height: 30px;" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton ID="RadDriver"
                                        runat="server" GroupName="driverorcarrier"
                                        Text="  Drivers " />
                                        &nbsp; &nbsp;<asp:RadioButton ID="RadCarrier" runat="server"
                                            GroupName="driverorcarrier" Text="  Carriers " />
                                        &nbsp; &nbsp;<asp:DropDownList ID="ddlDriverorCarrier" runat="server" Visible="False">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 10px;" align="center" valign="middle" visible="false" colspan="2">
                                        <asp:Panel ID="pnlCreateFile" runat="server" Width="100%" Visible="False">
                                            The selected loads are exported to a file.You can
                                <asp:LinkButton ID="lnkDownload" runat="Server" OnClick="lnkDownload_Click">Download Here.</asp:LinkButton>
                                        </asp:Panel>
                                        <br />
                                    </td>
                                </tr>
                                <tr valign="top" align="left">
                                    <td width="100%">
                                        <asp:Label ID="lblError" style="padding-bottom:10px;"  runat="server" Text="No records exist for the selected dates." Visible="false" ForeColor="red"></asp:Label>
                                      
                                        <asp:GridView ID="gvPayables" runat="server" AutoGenerateColumns="false" CssClass="Grid"
                                            OnRowCommand="GridView1_RowCommand" OnRowDataBound="OnRowDataBound" DataKeyNames="EmployeeOrCarrierId" >
                                            <PagerStyle CssClass="GridPage" Font-Size="Smaller" HorizontalAlign="Center" Width="100%" />
                                            <RowStyle BorderColor="White" CssClass="GridItem" />
                                            <HeaderStyle BorderColor="White" CssClass="GridHeader" ForeColor="Black" />
                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                            <Columns>
                                                <asp:TemplateField ItemStyle-Width="30" >
                                                    <ItemTemplate>                                                      
                                                        <asp:LinkButton ID="lnkDetails" runat="server" CommandArgument ="Hide" alt="" style="cursor: pointer">
                                                            <asp:Image ID="imgToggle" runat="server" ImageUrl="../Images/plus.png" style="border-width: 0px;" />
                                                        </asp:LinkButton>                                                       
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Width="600" ItemStyle-Font-Bold="true" ItemStyle-Font-Size="Medium">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblPlayerName" runat="Server" Text="Driver/Carrier Name"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <%#DataBinder.Eval(Container.DataItem,"PlayerName") %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="NetAmount" HeaderText="Net Amount" HeaderStyle-Width="20%" ItemStyle-Font-Bold="true" ItemStyle-Font-Size="Medium" />
                                                <asp:TemplateField HeaderStyle-Width="60">
                                                    <HeaderTemplate>
                                                        <asp:LinkButton ID="lnkSelectAll" runat="Server">Select</asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="ChkLoadID" runat="Server" OnCheckedChanged="chkAll_CheckedChanged" AutoPostBack="true" src="../Images/plus.png" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Width="60" ItemStyle-HorizontalAlign="Center">
                                                   
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnExport" runat="server" CssClass="btnstyle" Text="Pay and Email" OnClick="btnPay_Click"  CommandArgument='<%#DataBinder.Eval(Container.DataItem,"EmployeeOrCarrierId") %>'/>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                     <ItemTemplate>
                                                   
                                                         <asp:Panel ID="pnlOrders" runat="server" Visible="false">
                                                              </tr><tr><td></td><td colspan = '5'>                                                            
                                                            <asp:GridView ID="gvOrders" runat="server" AutoGenerateColumns="false" CssClass="ChildGrid" Width="100%">
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:HiddenField ID="hdnLoadId" runat ="server" Value='><%# Eval("Load")%>' />
                                                                            <table  width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl" >
                                                                                <tr valign="top">
                                                                                    <td >
                                                                                        <table border="1" cellpading="0" cellspacing="0" width="100%" align="center" valign="top" style="border-color: black; border-left-width: 0px; border-bottom-width: 0px;">
                                                                                            <tr>
                                                                                                <th align="left" valign="middle" style="font-size: 14px; border-top-width: 0px; border-right-width: 0px; border-color: black; background-color: lightblue; padding-left: 4px; padding-right: 4px; font-family: Arial,Helvetica,sans-serif; width: 60px;" align="left">Load</th>
                                                                                                <th align="left" valign="middle" style="font-size: 14px; border-top-width: 0px; border-right-width: 0px; border-color: black; background-color: lightblue; padding-left: 4px; padding-right: 4px; font-family: Arial,Helvetica,sans-serif; width: 100px;" align="left">Container</th>
                                                                                                <th align="left" valign="middle" style="font-size: 14px; border-top-width: 0px; border-right-width: 0px; border-color: black; background-color: lightblue; padding-left: 4px; padding-right: 4px; font-family: Arial,Helvetica,sans-serif; width: 60px;" align="left">Invoice/Tag</th>
                                                                                                <th align="left" valign="middle" style="font-size: 14px; border-top-width: 0px; border-right-width: 0px; border-color: black; background-color: lightblue; padding-left: 4px; padding-right: 4px; font-family: Arial,Helvetica,sans-serif; width: 130px;" align="left">Driver/Carrier Name</th>
                                                                                                <th align="left" valign="middle" style="font-size: 14px; border-top-width: 0px; border-right-width: 0px; border-color: black; background-color: lightblue; padding-left: 4px; padding-right: 4px; font-family: Arial,Helvetica,sans-serif; width: 50px;" align="left">Total</th>
                                                                                                <th align="left" valign="middle" style="font-size: 14px; border-top-width: 0px; border-right-width: 0px; border-color: black; background-color: lightblue; padding-left: 4px; padding-right: 4px; font-family: Arial,Helvetica,sans-serif; width: 130px;" align="left">Total Report</th>
                                                                                                <th align="left" valign="middle" style="font-size: 14px; border-top-width: 0px; border-right-width: 0px; border-color: black; background-color: lightblue; padding-left: 4px; padding-right: 4px; font-family: Arial,Helvetica,sans-serif; width: 130px;" align="left">From</th>
                                                                                                <th align="left" valign="middle" style="font-size: 14px; border-top-width: 0px; border-right-width: 0px; border-color: black; background-color: lightblue; padding-left: 4px; padding-right: 4px; font-family: Arial,Helvetica,sans-serif; width: 130px;" align="left">To</th>
                                                                                                <th align="left" valign="middle" style="font-size: 14px; border-top-width: 0px; border-right-width: 0px; border-color: black; background-color: lightblue; padding-left: 4px; padding-right: 4px; font-family: Arial,Helvetica,sans-serif; width: 60px;" align="left">Select</th>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left" valign="middle" style="font-size: 12px; border-top-width: 0px; border-right-width: 0px; border-color: black; padding-left: 3px; padding-right: 3px; background-color: #F3F8FC; font-family: Arial,Helvetica,sans-serif;"><span style='color: red;'><%# Eval("Load")%></span></td>
                                                                                                <td align="left" valign="middle" style="font-size: 12px; border-top-width: 0px; border-right-width: 0px; border-color: black; padding-left: 3px; padding-right: 3px; background-color: #F3F8FC; font-family: Arial,Helvetica,sans-serif;"><%# Eval("Container")%></td>
                                                                                                <td align="left" valign="middle" style="font-size: 12px; border-top-width: 0px; border-right-width: 0px; border-color: black; padding-left: 3px; padding-right: 3px; background-color: #F3F8FC; font-family: Arial,Helvetica,sans-serif;">
                                                                                                    <%# Eval("Column1")%>
                                                                                                </td>
                                                                                                <td align="left" valign="middle" style="font-size: 12px; border-top-width: 0px; border-right-width: 0px; border-color: black; padding-left: 3px; padding-right: 3px; background-color: #F3F8FC; font-family: Arial,Helvetica,sans-serif;">
                                                                                                     <%# Eval("DriverOrCarrierName")%>
                                                                                                </td>
                                                                                                <td align="left" valign="middle"  font-size: 12px; border-top-width: 0px; border-right-width: 0px; border-color: black; padding-left: 3px; padding-right: 3px; background-color: #F3F8FC; font-family: Arial,Helvetica,sans-serif;"><span style='color: red;'>
                                                                                                    <%# Eval("Total")%>
                                                                                                </td>
                                                                                                <td align="left" valign="middle" style="font-size: 12px; border-top-width: 0px; border-right-width: 0px; border-color: black; padding-left: 3px; padding-right: 3px; background-color: #F3F8FC; font-family: Arial,Helvetica,sans-serif;"><%# Eval("Payables")%></td>
                                                                                                <td align="left" valign="middle" style="font-size: 12px; border-top-width: 0px; border-right-width: 0px; border-color: black; padding-left: 3px; padding-right: 3px; background-color: #F3F8FC; font-family: Arial,Helvetica,sans-serif;">
                                                                                                    <%# Eval("Origin")%>
                                                                                                </td>
                                                                                                <td align="left" valign="middle" style="font-size: 12px; border-top-width: 0px; border-right-width: 0px; border-color: black; padding-left: 3px; padding-right: 3px; background-color: #F3F8FC; font-family: Arial,Helvetica,sans-serif;">
                                                                                                    <%# Eval("Destination")%>
                                                                                                </td>
                                                                                                <td align="middle">
                                                                                                   
                                                                                                    <asp:CheckBox ID="ChkDriverID" runat="server" OnCheckedChanged="Payments_OnCheckedChanged" AutoPostBack="true" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                               
                                                            </asp:GridView>                                                          
                                                                   </td></tr>
                                                        </asp:Panel>

                                                       
                                                     </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                      
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
          </ContentTemplate>
            </asp:UpdatePanel>
</asp:Content>

