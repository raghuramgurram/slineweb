using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;

public partial class DisplayReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            btnPrintReport.Attributes.Add("onclick", "javascript:window.open('printreport.aspx','print','width=1000,height=650,scrollbars=yes,menubar=yes,resizable=yes');");
            DataSet ds = new DataSet();
            string[] strColumnsList = null;
            string[] colWidths = null;
            object[] objParams = null;
            lblTotal.Visible = false;
            //SLine 2017 Enhancement for Office Location Starts
            long officeLocationId = 0;
            if (Session["OfficeLocationID"] != null)
            {
                officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
            }
            //SLine 2017 Enhancement for Office Location Ends
            if (Convert.ToString(Session["SerchCriteria"]).Trim().Length > 0)
            {
                lblSearchCriteria.Visible = true;
                lblSearchCriteria.Text = "<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + Convert.ToString(Session["SerchCriteria"]);
            }
            else
            {
                lblSearchCriteria.Visible = false;
            }
            string reportParam = string.Empty;
            switch (Convert.ToString(Session["ReportName"]))
            {
                case "GetDailyEnteredLoads":
                    Session["TopHeaderText"] = "Daily Entered Loads";
                    //SLine 2017 Enhancement for Office Location Starts
                    reportParam = Convert.ToString(Session["ReportParameters"]) + "^" + officeLocationId.ToString();
                    objParams = reportParam.Trim().Replace("~~^^^^~~", "^").Split('^');
                    //objParams = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
                    //SLine 2017 Enhancement for Office Location Ends
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDailyEnteredLoads", objParams);
                    if (Convert.ToString(Session["SerchCriteria"]).Trim().Length > 0)
                    {
                        lblSearchCriteria.Visible = true;
                        lblSearchCriteria.Text = "<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + Convert.ToString(Session["SerchCriteria"]) + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + (ds.Tables[0].Rows.Count > 0 ? "No of Loads :" + ds.Tables[0].Rows.Count.ToString() : "No Loads are available.");
                    }

                   
                    strColumnsList = new string[] { "Load", "Container", "Customer", "Origin", "Destination"};
                    colWidths = new string[] { "10%", "15%", "25%", "25%", "25%" };
                    break;
                case "GetDailyDispatchedLoads":
                    Session["TopHeaderText"] = "Daily Dispatched Loads";
                    //SLine 2017 Enhancement for Office Location
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDailyDispatchedloads", new object[] { officeLocationId });

                    //SLine 2016 Enhancements ---- Added Rail Container to the Table below
                    strColumnsList = new string[] { "Load", "Container", "Customer", "Driver/Carrier", "Origin", "Destination", "App. Date", "Last Free Date", "Status"};
                    colWidths = new string[] { "8%", "10%", "16%", "9%", "17%", "17%", "8%", "8%","8%"};
                    lblSearchCriteria.Text = "<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + (ds.Tables[0].Rows.Count > 0 ? "No of Loads :" + ds.Tables[0].Rows.Count.ToString() : "No Loads are available.");
                    lblSearchCriteria.Visible = true;
                    break;
                case "GetReadyToDispatchedLoads":
                    Session["TopHeaderText"] = "Ready To Dispatched Loads";
                    //SLine 2017 Enhancement for Office Location
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetReadytoDispatchLoads", new object[] { officeLocationId });
                    strColumnsList = new string[] { "Load", "Type", "Customer", "Origin","Pickup", "Destination", "Delivery App.", "Last Free Date" };
                    colWidths = new string[] { "120px", "60px", "150px", "150px", "80px", "150px", "80px", "80px" };
                    break;
                case "GetAllOpenLoads":
                    Session["TopHeaderText"] = "All Open Loads";
                    //SLine 2017 Enhancement for Office Location
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetAllOpenloads", new object[] { officeLocationId });
                    strColumnsList = new string[] { "Load", "Container", "Customer", "Origin", "Destination", "Driver/Carrier", "Status" };
                    colWidths = new string[] { "80px", "120px", "150px", "150px", "150px", "150px", "60px" };
                    break;
                case "GetLoadsWithLastFreeDay":
                    Session["TopHeaderText"] = "Loads With Last Free Day";
                    //SLine 2017 Enhancement for Office Location
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadsWithLastFreeDate", new object[] { officeLocationId });
                    strColumnsList = new string[] { "Load", "Type", "Customer", "Origin", "Pickup", "Destination", "Delivery App.", "Last Free Date" };
                    colWidths = new string[] { "120px", "60px", "150px", "150px", "80px", "150px", "80px", "80px" };
                    break;
                case "GetAllLoadsWithNoLastFreeDay":
                    Session["TopHeaderText"] = "Loads With No Last Free Day";
                    //SLine 2017 Enhancement for Office Location
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadsWithNoLastFreeDate", new object[] { officeLocationId });
                    strColumnsList = new string[] { "Load", "Type", "Customer", "Origin", "Pickup", "Destination", "Delivery App."};
                    colWidths = new string[] { "120px", "80px", "160px", "160px", "90px", "160px", "90px"};
                    break;
                case "GetCustomerwiseLoads":
                    Session["TopHeaderText"] = "Customer Wise Loads";
                    //SLine 2017 Enhancement for Office Location Starts
                    reportParam = Convert.ToString(Session["ReportParameters"]) + "^" + officeLocationId.ToString();
                    objParams = reportParam.Trim().Replace("~~^^^^~~", "^").Split('^');
                    //objParams = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
                    //SLine 2017 Enhancement for Office Location Ends
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCustomerwiseLoads", objParams);
                    #region Parameter Adjustment
                    //strColumnsList = new string[] { "Name", "Phone", "Fax", "Created" };
                    //colWidths = new string[] { "50%", "20%", "20%", "10%" };
                    if (Convert.ToString(objParams[0]) == "0")
                    {
                        if (Convert.ToString(objParams[1]).ToLower() == "all transit loads")
                        {
                            strColumnsList = new string[] { "Load", "Container", "Type", "Customer", "Origin", "Pickup", "Destination", "Delivery App.", "Last Free Date","Status" };
                            colWidths = new string[] { "100px","120px", "60px", "150px", "150px", "80px", "150px", "80px", "80px","50px" };
                        }
                        else if (Convert.ToString(objParams[1]).ToLower() == "loaded in yard")
                        {
                            strColumnsList = new string[] { "Load", "Container", "Type", "Customer", "Origin", "Pickup", "Destination", "Delivery App.", "Loaded in Yard Date" };
                            colWidths = new string[] { "100px", "120px", "60px", "150px", "150px", "80px", "150px", "80px", "80px" };
                        }
                        else
                        {
                            strColumnsList = new string[] { "Load", "Container", "Type", "Customer", "Origin", "Pickup", "Destination", "Delivery App.", "Last Free Date" };
                            colWidths = new string[] { "100px", "120px", "60px", "150px", "150px", "80px", "150px", "80px", "80px" };
                        }
                    }
                    else
                    {
                        if (Convert.ToString(objParams[1]).ToLower() == "all transit loads")
                        {
                            strColumnsList = new string[] { "Load", "Container", "Type", "Origin", "Pickup", "Destination", "Delivery App.", "Last Free Date", "Status" };
                            colWidths = new string[] { "100px", "120px", "60px", "200px", "80px", "200px", "80px", "80px", "50px" };
                        }
                        else if (Convert.ToString(objParams[1]).ToLower() == "loaded in yard")
                        {
                            strColumnsList = new string[] { "Load", "Container", "Type", "Origin", "Pickup", "Destination", "Delivery App.", "Loaded in Yard Date" };
                            colWidths = new string[] { "100px", "120px", "60px", "150px", "150px", "80px", "150px", "80px", "80px" };
                        }
                        else
                        {
                            strColumnsList = new string[] { "Load", "Container", "Type", "Origin", "Pickup", "Destination", "Delivery App.", "Last Free Date" };
                            colWidths = new string[] { "100px", "120px", "60px", "200px", "80px", "200px", "80px", "80px" };
                        }
                    }
                    #endregion

                    Session["SerchCriteria"] = lblSearchCriteria.Text + " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No. Of Loads :" + (ds.Tables[0].Rows.Count > 0 ? ds.Tables[0].Rows.Count.ToString() : "None");
                    lblSearchCriteria.Text = Convert.ToString(Session["SerchCriteria"]);
                    break;
                case "GetCarrierwiseLoads":
                    Session["TopHeaderText"] = "Carrier Wise Loads";
                    //SLine 2017 Enhancement for Office Location Starts
                    reportParam = Convert.ToString(Session["ReportParameters"]) + "^" + officeLocationId.ToString();
                    objParams = reportParam.Trim().Replace("~~^^^^~~", "^").Split('^');
                    //objParams = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
                    //SLine 2017 Enhancement for Office Location Ends
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCarrierwiseLoads", objParams);
                    #region Parameter Adjustment
                    //strColumnsList = new string[] { "Name", "Phone", "Fax", "Created" };
                    //colWidths = new string[] { "50%", "20%", "20%", "10%" };
                    if (Convert.ToString(objParams[0]) == "0")
                    {
                        if (Convert.ToString(objParams[1]).ToLower() == "all transit loads")
                        {
                            strColumnsList = new string[] { "Load", "Type", "Customer", "Carrier", "Origin", "Pickup", "Destination", "Delivery App.", "Last Free Date", "Status" };
                            colWidths = new string[] { "120px", "60px", "150px", "150px","150px", "80px", "150px", "80px", "80px", "50px" };
                        }
                        else
                        {
                            strColumnsList = new string[] { "Load", "Type", "Customer", "Carrier", "Origin", "Pickup", "Destination", "Delivery App.", "Last Free Date" };
                            colWidths = new string[] { "120px", "60px", "150px", "150px","150px", "80px", "150px", "80px", "80px" };
                        }
                    }
                    else
                    {
                        if (Convert.ToString(objParams[1]).ToLower() == "all transit loads")
                        {
                            strColumnsList = new string[] { "Load", "Type", "Customer", "Origin", "Pickup", "Destination", "Delivery App.", "Last Free Date", "Status" };
                            colWidths = new string[] { "120px", "60px", "150px", "150px", "80px", "150px", "80px", "80px", "50px" };
                        }
                        else
                        {
                            strColumnsList = new string[] { "Load", "Type","Customer", "Origin", "Pickup", "Destination", "Delivery App.", "Last Free Date" };
                            colWidths = new string[] { "120px", "60px", "150px", "150px", "80px", "150px", "80px", "80px" };
                        }
                    }
                    #endregion
                    //strColumnsList = new string[] { "Name", "Phone", "Fax", "Created" };
                    //colWidths = new string[] { "50%", "20%", "20%", "10%" };
                    break;
                case "GetDriverwiseLoads":
                    Session["TopHeaderText"] = "Driver Wise Loads";
                    //SLine 2017 Enhancement for Office Location Starts
                    reportParam = Convert.ToString(Session["ReportParameters"]) + "^" + officeLocationId.ToString();
                    objParams = reportParam.Trim().Replace("~~^^^^~~", "^").Split('^');
                    //objParams = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
                    //SLine 2017 Enhancement for Office Location Ends
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriverwiseLoads", objParams);
                    #region Parameter Adjustment
                    //strColumnsList = new string[] { "Name", "Phone", "Fax", "Created" };
                    //colWidths = new string[] { "50%", "20%", "20%", "10%" };
                    if (Convert.ToString(objParams[0]) == "0")
                    {
                        if (Convert.ToString(objParams[1]).ToLower() == "all transit loads" || Convert.ToString(objParams[1]).ToLower() == "all status")
                        {
                            strColumnsList = new string[] { "Load", "Type", "Customer", "Driver", "From", "Pickup", "To", "Delivery App.", "Last Free Date", "Status" };
                            colWidths = new string[] { "120px", "60px", "150px", "150px", "150px", "80px", "150px", "80px", "80px", "50px" };
                        }
                        else
                        {
                            strColumnsList = new string[] { "Load", "Type", "Customer", "Driver", "From", "Pickup", "To", "Delivery App.", "Last Free Date" };
                            colWidths = new string[] { "120px", "60px", "150px", "150px", "150px", "80px", "150px", "80px", "80px" };
                        }
                    }
                    else
                    {
                        if (Convert.ToString(objParams[1]).ToLower() == "all transit loads" || Convert.ToString(objParams[1]).ToLower() == "all status")
                        {
                            strColumnsList = new string[] { "Load", "Type", "Customer", "From", "Pickup", "To", "Delivery App.", "Last Free Date", "Status" };
                            colWidths = new string[] { "120px", "60px", "150px", "150px", "80px", "150px", "80px", "80px", "50px" };
                        }
                        else
                        {
                            strColumnsList = new string[] { "Load", "Type", "Customer", "From", "Pickup", "To", "Delivery App.", "Last Free Date" };
                            colWidths = new string[] { "120px", "60px", "150px", "150px", "80px", "150px", "80px", "80px" };
                        }
                    }
                    #endregion
                    //strColumnsList = new string[] { "Name", "User ID", "Password", "Date Created" };
                    //colWidths = new string[] { "40%", "20%", "20%", "20%" };
                    break;
                case "GetLoadedInYardLoads":
                    Session["TopHeaderText"] = "Loaded In Yard Loads";
                    //SLine 2017 Enhancement for Office Location
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadedInYardLoads", new object[] { officeLocationId });
                    strColumnsList = new string[] { "Load", "Container", "Customer", "Origin", "Destination", "Pickup", "Days In Yard" };
                    colWidths = new string[] { "80px", "120px", "150px", "150px", "150px", "90px","60px"};
                    break;
                case "GetEmptiesInYardLoads":
                    Session["TopHeaderText"] = "Empties In Yard Loads";                   
                    //ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetEmptiesInYardLoads");
                    //strColumnsList = new string[] { "Load", "Container", "Customer", "Origin", "Destination", "Pickup", "Days In Yard" };
                    //colWidths = new string[] { "80px", "120px", "150px", "150px", "150px", "90px", "60px" };
                    //SLine 2017 Enhancement for Office Location
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetEmptiesInYardLoads_New", new object[] { officeLocationId });
                    strColumnsList = new string[] { "Driver", "Container", "Shipping line", "Return to", "Days", "Size", "Pickup" };
                    colWidths = new string[] { "150px", "120px", "150px", "150px", "80px", "60px", "90px" };
                    break;
                case "GetPaperWorkPending":
                    Session["TopHeaderText"] = "Loads With Paper Work Pending";

                    //serch for paper work Pending(Madhav)
                    //SLine 2017 Enhancement for Office Location Starts
                    reportParam = Convert.ToString(Session["ReportParameters"]) + "^" + officeLocationId.ToString();
                    objParams = reportParam.Trim().Replace("~~^^^^~~", "^").Split('^');
                    //objParams = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
                    //SLine 2017 Enhancement for Office Location Ends
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadsWithPaperWorkPending", objParams);
                    //serch for paper work Pending(Madhav)

                    strColumnsList = new string[] { "Load", "Container", "Customer", "Driver/Carrier", "From", "To", "Delivered Date", "No.of Days" };
                    colWidths = new string[] { "80px", "120px", "150px", "120px", "120px", "120px", "90px", "60px" };
                    break;
                case "GetReceivableEntries":
                    Session["TopHeaderText"] = "Receivable Entries";
                    //SLine 2017 Enhancement for Office Location Starts
                    reportParam = officeLocationId.ToString() + "~~^^^^~~" + Convert.ToString(Session["ReportParameters"]);
                    objParams = reportParam.Trim().Replace("~~^^^^~~", "^").Split('^');
                    //objParams = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
                    //SLine 2017 Enhancement for Office Location Ends
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetReceivableEntries", objParams);
                    strColumnsList = new string[] { "Load", "Container","Customer", "Origin", "Destination", "Charges", "Fuel Sur- charges", "Extra Charges", "Total", "QB Status" };
                    colWidths = new string[] { "80px", "120px", "150px","150px", "150px", "60px", "60px", "60px", "70px","70px" };
                    double Amount = 0.00;
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Amount += Convert.ToDouble(ds.Tables[0].Rows[i]["Total"]);
                    }
//                    lblTotal.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total = " + Convert.ToString(ds.Tables[1].Rows[0][0]);
                    lblTotal.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total = " + Convert.ToString(Amount);
                    lblTotal.Visible = true;
                    Session["Total"] = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total = " + Convert.ToString(Amount);
                    break;
                case "GetPayableEntries":
                    Session["TopHeaderText"] = "Payable Entries";
                    //SLine 2017 Enhancement for Office Location Starts
                    reportParam = Convert.ToString(Session["ReportParameters"]) + "^" + officeLocationId.ToString();
                    objParams = reportParam.Trim().Replace(":", " like ").Replace("~~^^^^~~", "^").Split('^');
                    //objParams = Convert.ToString(Session["ReportParameters"]).Trim().Replace(":"," like ").Replace("~~^^^^~~", "^").Split('^');
                    //SLine 2017 Enhancement for Office Location Ends
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], Convert.ToString(Session["PayableSPName"]), objParams);
                    if (Convert.ToString(objParams[objParams.Length - 2]) == "*")
                    {
                        if (Convert.ToString(Session["SerchCriteria"]).Contains("All Drivers"))
                        {
                            strColumnsList = new string[] { "Load", "Container", "Invoice/Tag", "Driver/Carrier Name", "Total", "From", "To", "QB Status" };
                            colWidths = new string[] { "80px", "120px", "80px", "150px", "60px", "150px", "150px", "60px" };
                            if (Convert.ToString(Session["SerchCriteria"]).Contains("All Drivers and Carriers"))
                            {
                                ViewState["HasMultipleTables"] = false;
                            }
                            else
                            {
                                ViewState["HasMultipleTables"] = true;
                            }
                        }
                        else
                        {
                            strColumnsList = new string[] { "Load", "Container", "Invoice/Tag", "Driver/Carrier Name", "Total", "Origin", "Destination", "QB Status" };
                            colWidths = new string[] { "60px", "100px", "60px", "130px", "50px", "130px", "130px", "130px", "60px" };
                        }
                        lblTotal.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";                      
                        lblTotal.Visible = true;                        
                        Label1.Text = Convert.ToString(ds.Tables[1].Rows[0][0]);                        
                        Label1.Visible = true;
                    }
                    else
                    {
                        //&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        if (objParams.Length != 3)
                        {
                            strColumnsList = new string[] { "Load", "Container", "Invoice/Tag", "Driver/Carrier Name", "Total", "Total Report", "From", "To", "QB Status" };
                            colWidths = new string[] { "60px", "100px", "60px", "130px", "50px", "130px", "130px", "130px", "60px" };

                            Label1.Visible = false;
                            lblTotal.Visible = false;
                            Session["Total"] = null;
                            string[] strPayments = Convert.ToString(ds.Tables[1].Rows[0][0]).Replace("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "^").Split('^');
                            if (strPayments.Length == 3)
                            {
                                lblPaymentTotal.Text = strPayments[0];
                                lblPaid.Text = strPayments[1];
                                lblPending.Text = strPayments[2];
                                tblResult.Visible = true;
                                lblcount.Text = ds.Tables[0].Rows.Count.ToString();
                                Session["DriverResult"] = Convert.ToString(ds.Tables[1].Rows[0][0]) + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + ds.Tables[0].Rows.Count.ToString();
                            }
                            lblHeader.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Payment  settlement Sheet<br/>";
                            lblHeader.Visible = true;
                           // lblSearchCriteria.Text = "Payment  settlement Sheet<br/>";
                           // lblSearchCriteria.Visible = true;
                        }
                        else
                        {
                            strColumnsList = new string[] { "Load", "Container", "Invoice/Tag", "Driver/Carrier Name", "Total", "Origin", "Destination", "QB Status" };
                            colWidths = new string[] { "60px", "100px", "60px", "120px", "50px", "160px", "120px", "120px", "60px" };
                            lblTotal.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                            lblTotal.Visible = true;
                            Label1.Text = Convert.ToString(ds.Tables[1].Rows[0][0]);
                            Label1.Visible = true;
                            Session["Total"] = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " + Convert.ToString(ds.Tables[1].Rows[0][0]);
                        }                                            
                    }                    
                    break;
                case "GetLoadsWithNoApptDate":
                    Session["TopHeaderText"] = "Loads With No Appointment Date";
                    //SLine 2017 Enhancement for Office Location
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadsWithNoApptDate", new object[] { officeLocationId });
                    strColumnsList = new string[] { "Load", "Type", "Customer", "Origin", "Pickup", "Destination", "Delivery App.", "Last Free Date" };
                    colWidths = new string[] { "120px", "60px", "150px", "150px", "80px", "150px", "80px", "80px" };
                    break;
                case "GetLoadsWithNoPickupDate":
                    Session["TopHeaderText"] = "Loads with no Pickup#";
                    //SLine 2017 Enhancement for Office Location
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadsWithNoPickupDate", new object[] { officeLocationId });
                    strColumnsList = new string[] { "Load", "Type", "Customer", "Origin", "Pickup", "Destination", "Delivery App.", "Last Free Date" };
                    colWidths = new string[] { "120px", "60px", "150px", "150px", "80px", "150px", "80px", "80px" };
                    break;
                case "GetUserLogs":
                    Session["TopHeaderText"] = "User Logs";
                    objParams = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetUserLogs", objParams);
                    strColumnsList = new string[] { "User Id", "User Name", "Role", "Logged-in Date", "Logged-out Date"};
                    colWidths = new string[] { "150px", "250px", "120px", "200px", "200px" };
                    break;
                case "CancelledLoads":
                    Session["TopHeaderText"] = "Cancelled Loads";
                    //SLine 2017 Enhancement for Office Location Starts
                    reportParam = Convert.ToString(Session["ReportParameters"]) + "^" + officeLocationId.ToString();
                    objParams = reportParam.Trim().Replace("~~^^^^~~", "^").Split('^');
                    //SLine 2017 Enhancement for Office Location Ends
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCancelLoads", objParams);
                    strColumnsList = new string[] { "Load", "Customer", "Driver/Carrier", "Origin", "Destination", "Delivered Date", "Terminated Date", "Last Location", "Comments" };
                    colWidths = new string[] { "12%", "12%", "12%", "12%", "12%", "10%", "10%", "10%", "10%" };
                    break;
                case "ClosedLoads":
                    Session["TopHeaderText"] = "Closed Loads";
                    //Load;Customer;Driver/Carrier;Origin;Destination;Delivered Date;Terminated Date;Days In Transit;Last Location;Date & Time;Comments
                    //SLine 2017 Enhancement for Office Location Starts
                    reportParam = Convert.ToString(Session["ReportParameters"]) + "^" + officeLocationId.ToString();
                    objParams = reportParam.Trim().Replace("~~^^^^~~", "^").Split('^');                    
                    //objParams = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
                    //SLine 2017 Enhancement for Office Location Ends
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetClosedLoads", objParams);
                    strColumnsList = new string[] { "Load", "Customer", "Driver/Carrier", "Origin", "Destination", "Delivered Date","Terminated Date","Last Location","Comments" };
                    colWidths = new string[] { "12%", "12%", "12%", "12%", "12%", "10%", "10%", "10%", "10%" };
                    break;
                case "LoadswithNoTag":
                    Session["TopHeaderText"] = "Loads with No Tag";
                    //SLine 2017 Enhancement for Office Location
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadswithNoTags", new object[] { officeLocationId });
                    //Load;Customer;Driver/Carrier;Origin;Pickup;Destination;Delivery Appt Date;Last Free Date;Status
                    strColumnsList = new string[] { "Load", "Customer", "Driver/Carrier", "Origin", "Pickup", "Destination", "Delivery App.", "Last Free Date", "Status" };
                    colWidths = new string[] { "12%", "12%", "12%", "12%", "10%", "12%", "10%", "10%","10%" };
                    break;
                case "LoadswithApptDate":
                    Session["TopHeaderText"] = "Loads with Appt.Date";
                    //SLine 2017 Enhancement for Office Location
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadsWithApptDate", new object[] { officeLocationId });
                    strColumnsList = new string[] { "Load", "Type", "Customer", "Origin", "Pickup", "Destination", "Delivery App.", "Last Free Date" };
                    colWidths = new string[] { "120px", "60px", "150px", "150px", "80px", "150px", "80px", "80px" };
                    break;
                case "GetShipingLineWiseOpenLoads":
                    Session["TopHeaderText"] = "Shipping Line Wise Open Loads";                    
                    objParams = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
                    lblSearchCriteria.Text = "From Date : " + objParams[1].ToString() + " To Date : " + objParams[2].ToString() + "  Shipping Line Name : " + Convert.ToString(Session["ShipingLineName"]).Trim();
                    lblSearchCriteria.Visible = true;
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetAllOpenLoadDetailsBy_ShippingLine", objParams);
                    strColumnsList = new string[] { "Container#" };
                    colWidths = new string[] {  "200px" };
                    break;
                case "GetOriginWiseOpenLoads":
                    Session["TopHeaderText"] = "Origin Wise OpenLoads";
                    //SLine 2017 Enhancement for Office Location Starts
                    reportParam = Convert.ToString(Session["ReportParameters"]) + "^" + officeLocationId.ToString();
                    objParams = reportParam.Trim().Replace("~~^^^^~~", "^").Split('^');
                    //objParams = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
                    //SLine 2017 Enhancement for Office Location Ends
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetAllOpenLoadDetailsBy_Origin", objParams);
                    strColumnsList = new string[] { "Location", "Container#" };
                    colWidths = new string[] { "120px", "200px" };
                    break;
                case "GetStaffMistakes":
                    Session["TopHeaderText"] = "Staff Mistakes";
                    objParams = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetStaffMistakes", objParams);
                    strColumnsList = new string[] { "Name", "Mistakes Count" };
                    colWidths = new string[] { "200px", "120px" };
                    break;
                case "GetProfitandLoss":
                    Session["TopHeaderText"] = "Profit and Loss";
                    objParams = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
                    lblSearchCriteria.Text = "Month : " + objParams[2].ToString() + " Year : " + objParams[1].ToString();
                    lblSearchCriteria.Visible = true;
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetProfitLoss", objParams);
                    strColumnsList = new string[] { "Customer", "Load#", "Container#", "Origin", "Destination", "Delivered on", "Driver One", "Driver Two", "Driver Three", "Driver Four", "Receivable", "Pay Driver1", "Pay Driver2", "Pay Driver3", "Pay Driver4", "Chassis Days", "Chassis Charge", "Miscellaneous", "Margin", "Invoiced", "Load Status", "Notes" };
                    colWidths = new string[] { "60px", "100px", "60px", "100px", "50px", "100px", "100px", "130px", "60px", "60px", "100px", "60px", "100px", "50px", "100px", "100px", "130px", "60px", "100px", "100px", "100px", "60px" };
                    break;
                case "GetProfitandLossMonthWise":
                    Session["TopHeaderText"] = "Profit and Loss Month Wise";
                    //SLine 2017 Enhancement for Office Location Starts
                    reportParam = Convert.ToString(Session["ReportParameters"]) + "^" + officeLocationId.ToString();
                    objParams = reportParam.Trim().Replace("~~^^^^~~", "^").Split('^');                                      
                    //objParams = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
                    //SLine 2017 Enhancement for Office Location Ends
                    lblSearchCriteria.Text = " Year : " + objParams[1].ToString();
                    lblSearchCriteria.Visible = true;
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetProfitLossGraphData", objParams);
                    strColumnsList = new string[] { "Month", "No.of Loads", "Receivable", "Payable", "Margin" };  
                    colWidths = new string[] { "100px", "100px", "100px", "100px", "100px" };
                    break;
                case "GetShipingLineWiseLoads":
                    Session["TopHeaderText"] = "Shipping Line Wise Loads";
                    objParams = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
                    //lblSearchCriteria.Text = "From Date : " + objParams[1].ToString() + " To Date : " + objParams[2].ToString() + "  Shipping Line Name : " + Convert.ToString(Session["ShipingLineName"]).Trim();
                    lblSearchCriteria.Text = "<table border='0' Style='font-family: Arial, sans-serif; font-size: 12px; color:Blue'><tr><td><b>" + Convert.ToString(Session["ShipingLineName"]).Trim() + "</b></td></tr></table>";
                    lblSearchCriteria.Visible = true;
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetShippingLineWiseLoads", objParams);
                    strColumnsList = new string[] { "Container Number", "Chassis Number", "Outgate Date", "Ingate Date", "Use Days", "Outgate Loc", "Ingate Loc", "Shipment #" };
                    colWidths = new string[] { "80px","80px", "80px", "80px", "50px", "100px", "80px","120px" };
                    break;
                case "GetAccessorialLoads":
                    btnSendMail.Visible = true;
                    Session["TopHeaderText"] = "Accessorial loads - Customer wise";
                    //SLine 2017 Enhancement for Office Location
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetAccessorialLoads", new object[] { Convert.ToInt32(Session["ReportParameters"]), officeLocationId});
                    strColumnsList = new string[] { "Ref.#", "Container", "Accessorial", "Yard Pull", "Yard Storage", "Chassis Rent", "Lumper", "Re-Delivery Charges", "Detention", "Flip Charges", "Overweight", "Other Charges", "Total" };
                    colWidths = new string[] { "60px", "70px", "40px", "70px", "70px", "70px", "70px", "90px", "60px", "60px", "60px", "60px", "70px" };
                    if (ds.Tables[0].Rows.Count == 0 || Convert.ToInt32(Session["ReportParameters"]) == 0)
                      btnSendMail.Visible = false;
                    break;

                //SLine 2016 ---- Added new Report
                case "GetReportInvoiceProcessing":
                    Session["TopHeaderText"] = "Invoice Processing";
                    objParams = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
                    lblSearchCriteria.Text = "Month : " + objParams[2].ToString() + " Year : " + objParams[1].ToString();
                    lblSearchCriteria.Visible = true;
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetReportInvoiceProcessing", objParams);
                    strColumnsList = new string[] { "Customer", "Load#", "Container#", "Origin", "Destination", "Delivered on", "Driver One", "Driver Two", "Driver Three", "Driver Four", "Receivable", "Pay Driver1", "Pay Driver2", "Pay Driver3", "Pay Driver4", "Chassis Days", "Chassis Charge", "Miscellaneous", "Margin", "Invoiced", "Load Status", "Notes" };
                    colWidths = new string[] { "60px", "100px", "60px", "100px", "50px", "100px", "100px", "130px", "60px", "60px", "100px", "60px", "100px", "50px", "100px", "100px", "130px", "60px", "100px", "100px", "100px", "60px" };
                    break;
                
                case "GetDailyDeliveredLoads":
                    Session["TopHeaderText"] = "Daily Delivered Loads";
                    string[] strDates = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
                    //SLine 2017 Enhancement for Office Location
                    object[] objParamsTemp = new object[] { strDates[0], strDates[1] , strDates[2], "", "", "*", officeLocationId };
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDailyDeliveredLoads", objParamsTemp);
                    if (ds.Tables.Count > 0)
                    {
                        if (Convert.ToString(Session["SerchCriteria"]).Trim().Length > 0)
                        {
                            lblSearchCriteria.Visible = true;
                            lblSearchCriteria.Text = "<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + Convert.ToString(Session["SerchCriteria"]) + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + (ds.Tables[0].Rows.Count > 0 ? "No of Loads :" + ds.Tables[0].Rows.Count.ToString() : "No Loads are available.");
                        }
                    }
                    strColumnsList = new string[] { "Load", "Customer", "Driver/Carrier", "Origin", "Destination", "Delivered Date & Time", "Comments","POD" };
                    colWidths = new string[] { "10%", "15%", "15%", "10%", "25%", "10%", "15%", "15%" };
                    break;

                case "GetPaperworkOverdueLoads":
                    Session["TopHeaderText"] = "Paperwork Overdue Loads";
                    string[] strDatesp = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
                    //SLine 2017 Enhancement for Office Location
                    object[] objParamsTempp = new object[] { strDatesp[0], strDatesp[1], strDatesp[2], officeLocationId };
                    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadsWithPaperWorkPendingOverdue", objParamsTempp);
                    if (ds.Tables.Count > 0)
                    {
                        if (Convert.ToString(Session["SerchCriteria"]).Trim().Length > 0)
                        {
                            lblSearchCriteria.Visible = true;
                            lblSearchCriteria.Text = "<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + Convert.ToString(Session["SerchCriteria"]) + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + (ds.Tables[0].Rows.Count > 0 ? "No of Loads :" + ds.Tables[0].Rows.Count.ToString() : "No Loads are available.");
                        }
                    }
                    strColumnsList = new string[] { "Load", "Container", "Customer", "Driver/Carrier", "From", "To", "Delivered Date", "No.of Days" };
                    colWidths = new string[] { "80px", "120px", "150px", "120px", "120px", "120px", "90px", "60px" };
                    break;
                //SLine 2016 ---- Added new Report For Daily Delivered Reports
            }
            objParams = null;
            if (ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                if (Convert.ToString(Session["ReportName"]) == "GetShipingLineWiseLoads")
                {
                    lblSearchCriteria.Text = "<table border='0' Style='font-family: Arial, sans-serif; font-size: 12px; color:Blue'><tr><td><b>" + Convert.ToString(Session["ShipingLineName"]).Trim() + " (" + dt.Rows.Count + ")</b></td></tr></table>";
                }
                FormatDataTable(ref dt);
                if (Convert.ToString(Session["ReportName"]) == "GetAccessorialLoads" && Convert.ToInt32(Session["ReportParameters"]) == 0)
                  ReportDisplayAccessorialLoadsCustomerWise(dt, strColumnsList, colWidths);
                //SLine 2016 Added a new Report
                //else if (Convert.ToString(Session["ReportName"]) == "SP_GetReportInvoiceProcessing" && Convert.ToInt32(Session["ReportParameters"]) == 0)
                   // ReportDisplayAccessorialLoadsCustomerWise(dt, strColumnsList, colWidths);
                //SLine 2016 Added a new Report
                else
                  ReportDisplay(dt, strColumnsList, colWidths);
                if (dt != null) { dt.Dispose(); dt = null; }
            }
            else
            {
                lblDisplay.Text = "<table width=100% border=0 cellpadding=0 cellspacing=0 id=ContentTbl><tr valign=top><td>" +
                    "<table border=0 width=100% height=200px border=0 cellpadding=0 cellspacing=0 align=center valign=middle><tr><td width=100% align=center valign=middle>" +
                    "<b><font color=blue>No Records To Display.<font></b></td></tr></table><br/><br/><br/></td></tr></table>";
            }
            if (ds != null) { ds.Dispose(); ds = null; }
            strColumnsList = null; colWidths = null;
        }
        
    }

    protected void FormatDataTable(ref DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    if (Convert.ToString(dt.Rows[i][j]).Contains("^^^^"))
                    {
                        //if (Convert.ToString(Session["ReportName"]) == "GetPayableEntries")
                        //{
                        //    if (Convert.ToInt32(dt.Rows[i]["LoadStatus"]) != 12)
                        //    {
                        //        if (j == 4)
                        //        {
                        //            dt.Rows[i][j] = FormatMultipleRowColumnsred(Convert.ToString(dt.Rows[i][j]).Trim());
                        //        }
                        //        else
                        //        {
                        //            dt.Rows[i][j] = FormatMultipleRowColumns(Convert.ToString(dt.Rows[i][j]).Trim());
                        //        }
                        //    }
                        //    else
                        //    {
                        //        dt.Rows[i][j] = FormatMultipleRowColumns(Convert.ToString(dt.Rows[i][j]).Trim());
                        //    }
                        //}
                        //else
                        {
                            dt.Rows[i][j] = FormatMultipleRowColumns(Convert.ToString(dt.Rows[i][j]).Trim());
                        }
                    }
                    else if (Convert.ToString(Session["ReportName"]) == "GetProfitandLoss")
                    {
                        if (j == 18)
                        {
                            if (Convert.ToString(dt.Rows[i]["Margin"]) == "0.00")
                            {
                                dt.Rows[i][j] = FormatMultipleRowColumns("<Font color='red'>" + Convert.ToString(dt.Rows[i][j]).Trim() + "</Font>");
                            }
                            else if (Convert.ToString(dt.Rows[i]["Margin"]).Contains("-"))
                            {
                                dt.Rows[i][j] = FormatMultipleRowColumns("<Font color='red'>" + Convert.ToString(dt.Rows[i][j]).Trim() + "</Font>");
                            }
                        }
                    }
                }
            }
        }
    }
    public string FormatMultipleRowColumns(string strColValue)
    {
        string strFormat = "";
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0)
        {
            strFormat = "<table cellpadding=0 cellspacing=0 border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;>";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (i == 0)
                    strFormat += "<tr ><td  width=100% align=left valign=middle style=color:black;font-size:12;height:90px;>" + strMuls[i].Trim() + "</td></tr>";
                else
                {
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:black;font-size:12;height:90px;>" + strMuls[i].Trim() + "</td></tr>";
                }
            }
            strFormat += "</table>";
        }
        strMuls = null;
        return strFormat;
    }
    public string FormatMultipleRowColumnsred(string strColValue)
    {
        string strFormat = "";
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0)
        {
            strFormat = "<table cellpadding=0 cellspacing=0 border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;>";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (i == 0)
                    strFormat += "<tr ><td  width=100% align=left valign=middle style=color:black;font-size:12;height:90px;><span style='color:red;'>" + strMuls[i].Trim() + "</span></td></tr>";
                else
                {
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:black;font-size:12;height:90px;><span style='color:red;'>" + strMuls[i].Trim() + "</span></td></tr>";
                }
            }
            strFormat += "</table>";
        }
        strMuls = null;
        return strFormat;
    }

    protected void ReportDisplay(DataTable dt, string[] strColumnsList,string[] colWidths)
    {
        if (dt.Rows.Count > 0)
        {
            if (Convert.ToString(Session["ReportName"]) == "GetProfitandLoss" && dt.Rows.Count == 1)
            {
                lblDisplay.Text = "<table width=100% border=0 cellpadding=0 cellspacing=0 id=ContentTbl><tr valign=top><td>" +
                "<table border=0 width=100% height=200px border=0 cellpadding=0 cellspacing=0 align=center valign=middle><tr><td width=100% align=center valign=middle>" +
                "<b><font color=blue>No Records To Display.<font></b></td></tr></table><br/><br/><br/></td></tr></table>";
            }
            else
            {
                string strColProperties = " align=left valign=middle style=font-size:12px;border-top-width:0px;border-right-width:0px;border-color:black;padding-left:3px;padding-right:3px;background-color:#F3F8FC;font-family:Arial,Helvetica,sans-serif;";                
                string strHeadProperties = " align=left valign=middle style=font-size:14px;border-top-width:0px;border-right-width:0px;border-color:black;background-color:lightblue;padding-left:4px;padding-right:4px;font-family:Arial,Helvetica,sans-serif;";
                string strTableProperties = " border=1 cellpading=0 cellspacing=0 width=100% align=center valign=top style=border-color:black;border-left-width:0px;border-bottom-width:0px;";//padding-left:2px;padding-right:2px;";
                string strDisplay = "<table width=100% border=0 cellpadding=0 cellspacing=0 id=ContentTbl><tr valign=top><td>" +
                    "<table " + strTableProperties + ">";
                for (int i = 0; i < strColumnsList.Length; i++)
                {
                    if (i == 0)
                        strDisplay += "<tr>";
                    strDisplay += "<th " + strHeadProperties + "width:" + colWidths[i] + "; align=left >" + strColumnsList[i] + "</th>";
                    if (i == strColumnsList.Length - 1)
                        strDisplay += "</tr>";
                }
                if (Convert.ToString(Session["ReportName"]) == "GetPayableEntries")
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {                       
                        strDisplay += "<tr >";
                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            //if (Convert.ToInt32(dt.Rows[i]["LoadStatus"]) != 12)
                            //{
                            //    int x = 8;
                            //    if (dt.Columns.Count > 9)
                            //    {
                            //        x = 9;
                            //    }
                            //    if (j != x)
                            //    {
                            //        if (j == 0 || j == 4)
                            //        {
                            //            strDisplay += "<td" + strColProperties + ">";
                            //            strDisplay += "<span style='color:red;'>" + Convert.ToString(dt.Rows[i][j]) + "</span>";
                            //            strDisplay += "</td>";
                            //        }
                            //        else
                            //        {
                            //            strDisplay += "<td" + strColProperties + ">";
                            //            strDisplay += Convert.ToString(dt.Rows[i][j]);
                            //            strDisplay += "</td>";
                            //        }
                            //    }
                            //}
                            //else
                            {
                                int x = 8;
                                if (dt.Columns.Count > 9)
                                {
                                    x = 9;
                                }
                                if (j != x)
                                {
                                    strDisplay += "<td" + strColProperties + ">";
                                    strDisplay += Convert.ToString(dt.Rows[i][j]);
                                    strDisplay += "</td>";
                                }
                            }
                        }
                        strDisplay += "</tr>";
                    }
                }
                else if (Convert.ToString(Session["ReportName"]) == "GetProfitandLossMonthWise")  
                {
                    int Loads = 0;
                    double Receivable = 0.00;
                    double Payable = 0.00;
                    double Margin = 0.00;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Loads += Convert.ToInt32(dt.Rows[i]["LoadId"]);
                        Receivable += Convert.ToDouble(dt.Rows[i]["Receivable"]);
                        Payable += Convert.ToDouble(dt.Rows[i]["Payable"]);
                        Margin += Convert.ToDouble(dt.Rows[i]["Margin"]);
                    }
                    DataRow row = dt.NewRow();
                    row[0] = "Total";
                    row[1] = Loads;
                    row[2] = Receivable;
                    row[3] = Payable;
                    row[4] = Margin;
                    dt.Rows.Add(row);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        bool IsLastRow = ((dt.Rows.Count - 1 - i) == 0 ? true : false);
                        strDisplay += "<tr >";
                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            if (IsLastRow)
                            {
                                if (j == 0)
                                strDisplay += "<td " + strColProperties + "><b>";
                                else
                                strDisplay += "<td align='right'" + strColProperties + "><b>";
                                if (j == 4)
                                {
                                    if (Convert.ToString(dt.Rows[i]["Margin"]) == "0.00")
                                    {
                                        strDisplay += "<Font color='red'>"+string.Format("{0:N}",(dt.Rows[i][j]))+"</Font>";
                                    }
                                    else if (Convert.ToString(dt.Rows[i]["Margin"]).Contains("-"))
                                    {
                                        strDisplay += "<Font color='red'>" + string.Format("{0:N}",dt.Rows[i][j]) + "</Font>";
                                    }
                                    else {
                                        strDisplay += string.Format("{0:N}",(dt.Rows[i][j]));
                                    }
                                }
                                else if (j == 1)
                                {
                                    strDisplay += Convert.ToString(dt.Rows[i][j]);
                                }
                                else
                                {
                                    strDisplay += string.Format("{0:N}", dt.Rows[i][j]);
                                }
                                strDisplay += "</b></td>";
                            }
                            else
                            {
                                if (j == 0)
                                strDisplay += "<td " + strColProperties + ">";
                                else
                                strDisplay += "<td align='right'" + strColProperties + ">";
                                if (j == 4)
                                {
                                    if (Convert.ToString(dt.Rows[i]["Margin"]) == "0.00")
                                    {
                                        strDisplay += "<Font color='red'>" + string.Format("{0:N}",(dt.Rows[i][j])) + "</Font>";
                                    }
                                    else if (Convert.ToString(dt.Rows[i]["Margin"]).Contains("-"))
                                    {
                                        strDisplay += "<Font color='red'>" + string.Format("{0:N}", dt.Rows[i][j]) + "</Font>";
                                    }
                                    else
                                    {
                                        strDisplay += string.Format("{0:N}",(dt.Rows[i][j]));
                                    }
                                }
                                else if (j == 1)
                                {
                                    strDisplay += Convert.ToString(dt.Rows[i][j]);
                                }
                                else
                                {
                                    strDisplay += string.Format("{0:N}", dt.Rows[i][j]);
                                }
                                strDisplay += "</td>";
                            }
                        }
                        strDisplay += "</tr>";
                    }
                }
                else
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        bool IsLastRow = ((dt.Rows.Count - 1 - i) == 0 ? true : false);
                        strDisplay += "<tr >";
                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            if (Convert.ToString(Session["ReportName"]) == "GetProfitandLoss" && IsLastRow)
                            {
                                strDisplay += "<td" + strColProperties + "><b>";
                                strDisplay += Convert.ToString(dt.Rows[i][j]);
                                strDisplay += "</b></td>";
                            }
                            else if(Convert.ToString(Session["ReportName"]) == "GetDailyDeliveredLoads")
                            {
                                strDisplay += "<td" + (strColProperties).Replace("F3F8FC", (Convert.ToString(dt.Rows[i][6]).Contains("Verified") ? "F3F8FC" : "FFA8A8")).Replace("align=left", (dt.Columns[j].ColumnName == "POD" ? "align=center" : "align=left")) + ">";
                                strDisplay += Convert.ToString(dt.Rows[i][j]);
                                strDisplay += "</td>";
                            }
                            else
                            {
                                strDisplay += "<td" + strColProperties + ">";
                                strDisplay += Convert.ToString(dt.Rows[i][j]);
                                strDisplay += "</td>";
                            }
                        }
                        strDisplay += "</tr>";
                    }
                }
                strDisplay += "</table>" +
                    "</td></tr></table>";
                lblDisplay.Text = strDisplay;
            }
        }
        else
        {
            lblDisplay.Text = "<table width=100% border=0 cellpadding=0 cellspacing=0 id=ContentTbl><tr valign=top><td>" +
                "<table border=0 width=100% height=200px border=0 cellpadding=0 cellspacing=0 align=center valign=middle><tr><td width=100% align=center valign=middle>" +
                "<b><font color=blue>No Records To Display.<font></b></td></tr></table><br/><br/><br/></td></tr></table>";
        }
        Session["PrintDetails"] = lblDisplay.Text.Trim();
        //Page.Controls.Add(new LiteralControl(strDisplay));       
    }

    protected void ReportDisplayAccessorialLoadsCustomerWise(DataTable dt, string[] strColumnsList, string[] colWidths)
    {
        lblSearchCriteria.Visible = false;
        if (dt.Rows.Count > 0)
        {
            string strColProperties = " align=left valign=middle style=font-size:12px;border-top-width:0px;border-right-width:0px;border-color:black;padding-left:3px;padding-right:3px;background-color:#F3F8FC;font-family:Arial,Helvetica,sans-serif;";
            string strHeadProperties = " align=left valign=middle style=font-size:14px;border-top-width:0px;border-right-width:0px;border-color:black;background-color:lightblue;padding-left:4px;padding-right:4px;font-family:Arial,Helvetica,sans-serif;";
            string strTableProperties = " border=1 cellpadding=0 cellspacing=0 width=100% align=center valign=top style=border-color:black;border-left-width:0px;border-bottom-width:0px;margin-top:10px;margin-bottom:20px;";//padding-left:2px;padding-right:2px;";
            string strDisplay = "<table width=100% border=0 cellpadding=0 cellspacing=0 id=ContentTbl><tr valign=top><td>";
            string customerId = string.Empty;
            for (int k = 0; k < dt.Rows.Count; k++)
            {
                    if (Convert.ToString(dt.Rows[k][0]).Trim() != customerId)
                    {
                        customerId = Convert.ToString(dt.Rows[k][0]).Trim();
                        strDisplay +=
                         "<span style='color:Blue;font-weight:bold;font-family: Arial, sans-serif; font-size: 12px;margin-bottom:5px'>Customer Name : " + Convert.ToString(dt.Rows[k][1]).Trim() + "</span><table " + strTableProperties + ">";
                        for (int i = 0; i < strColumnsList.Length; i++)
                        {
                            if (i == 0)
                                strDisplay += "<tr>";
                            strDisplay += "<th " + strHeadProperties + "width:" + colWidths[i] + "; align=left >" + strColumnsList[i] + "</th>";
                            if (i == strColumnsList.Length - 1)
                                strDisplay += "</tr>";
                        }

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (Convert.ToString(dt.Rows[k][0]).Trim() == Convert.ToString(dt.Rows[i][0]).Trim())
                            {
                                strDisplay += "<tr >";
                                for (int j = 2; j < dt.Columns.Count; j++)
                                {
                                    strDisplay += "<td" + strColProperties + ">";
                                    strDisplay += Convert.ToString(dt.Rows[i][j]);
                                    strDisplay += "</td>";
                                }
                                strDisplay += "</tr>";
                            }
                        }

                        strDisplay += "</table>";
                        
                    }
            }
            strDisplay += "</td></tr></table>";
            lblDisplay.Text = strDisplay;
        }
        else
        {
            lblDisplay.Text = "<table width=100% border=0 cellpadding=0 cellspacing=0 id=ContentTbl><tr valign=top><td>" +
                "<table border=0 width=100% height=200px border=0 cellpadding=0 cellspacing=0 align=center valign=middle><tr><td width=100% align=center valign=middle>" +
                "<b><font color=blue>No Records To Display.<font></b></td></tr></table><br/><br/><br/></td></tr></table>";
        }
        Session["SerchCriteria"] = "";
        Session["PrintDetails"] = lblDisplay.Text.Trim();
        //Page.Controls.Add(new LiteralControl(strDisplay));       
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            ExportToExcel();
        }
        catch
        { }
    }

    private void ExportToExcel()
    {
        string fileName=string.Empty;
        if (Master.FindControl("Top1").FindControl("lblHeadText") != null)
        {
            fileName = ((Label)Master.FindControl("Top1").FindControl("lblHeadText")).Text;
        }
        if(string.IsNullOrEmpty(fileName))
        {
            fileName = "ExcelFile";
        }
        fileName=fileName.Replace(" ", "");
        fileName = fileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
        Response.ContentType = "application/x-msexcel";
        Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + ".xls"); Response.ContentEncoding = Encoding.UTF8;
        StringWriter tw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(tw);
        //serch for paper work Pending(Madhav)        
            tbldispaly.RenderControl(hw);
            Response.Write(tw.ToString());
            Response.End();
        //serch for paper work Pending(Madhav)
        
    }

    protected void btnSendMail_Click(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCustomerDetails", Convert.ToInt32(Session["ReportParameters"]));
        if (ds.Tables[0].Rows.Count > 0)
        {
            if (Convert.ToString(ds.Tables[0].Rows[0]["bit_SendEmails"]) == "True" && !String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["nvar_AccessorialChargesEmail"])))
            {
                string fileName = string.Empty;
                string[] filePaths;
                filePaths = new string[1];
                fileName = "AccessorialLoads" + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".xls";
                StringWriter tw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(tw);

                // Render grid view control.
                tbldispaly.RenderControl(hw);

                // Write the rendered content to a file.
                string renderedGridView = tw.ToString();
                File.WriteAllText(Server.MapPath("~/Documents/" + fileName), renderedGridView);
                filePaths[0] = Server.MapPath("../Documents" + "\\" + fileName);
                CommonFunctions.SendEmail(GetFromXML.FromAccessrial, Convert.ToString(ds.Tables[0].Rows[0]["nvar_AccessorialChargesEmail"]), GetFromXML.CCAccessorial, "", "AccessorialLoads", GetBodyAccessorialDetails(), null, filePaths);
                File.Delete(Server.MapPath("~/Documents/" + fileName));
                Response.Write("<script>alert('Mail sent successfully.')</script>");
            }
            else
            {
                Response.Write("<script>alert('Mail not sent successfully.')</script>");
            }
        }
    }

    private string GetBodyAccessorialDetails()
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();
        strBody.Append("Dear Valued Customer, <br/>");
        strBody.Append("<p>Please see attached accessorial loads.</p>");
        strBody.Append("<p></p>Regards<br/>" + GetFromXML.CompanyName + "<br/>" + GetFromXML.CompanyPhone + "<br/>" + GetFromXML.CompanyFax);
        return strBody.ToString();
    }
   
}
