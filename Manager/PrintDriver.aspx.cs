using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class PrintDriver : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Dispatched Driver Print";
        if (!IsPostBack)
        {
            if (Request.QueryString.Count > 0)
            {
                try
                {
                    string[] strParam = Convert.ToString(Request.QueryString[0]).Trim().Split('?');
                    //ViewState["LoadId"] = Convert.ToInt64(strParam[0]);
                    //ViewState["DriverId"] = Convert.ToInt64(strParam[1]);
                    //barCurrent.HeaderText = "Load Id : " + Convert.ToString(strParam[0]);
                    ViewState["AssignedID"] = Convert.ToInt64(strParam[1]);
                    FillDriversDeatils(Convert.ToInt64(strParam[1]));
                }
                catch { }
            }
        }
    }

    private void FillDriversDeatils(long AssignedId)
    {
        //lblDriver.Text = DBClass.executeScalar("select [nvar_NickName] from [eTn_Employee] where [bint_EmployeeId]="+EmployeeId);
        DataTable dt = DBClass.returnDataTable("SELECT [eTn_AssignDriver].[nvar_From],[eTn_AssignDriver].[nvar_FromCity],[eTn_AssignDriver].[nvar_FromState],[eTn_AssignDriver].[nvar_To],[eTn_AssignDriver].[nvar_Tocity],[eTn_AssignDriver].[nvar_Tostate],[eTn_AssignDriver].[nvar_Tag],[eTn_AssignDriver].[date_DateAssigned],eTn_Employee.[nvar_NickName],[eTn_AssignDriver].[bint_LoadId],[eTn_AssignDriver].bint_EmployeeId" +
                                                " FROM [eTn_AssignDriver]	inner join [eTn_Employee] on eTn_Employee.[bint_EmployeeId]=[eTn_AssignDriver].bint_EmployeeId Where [eTn_AssignDriver].[bint_AssignedId]=" + AssignedId);
        // DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetAssignedDriverDetails", new object[] { LoadId,EmployeeId });
        if (dt.Rows.Count > 0)
        {
            lblFrom.Text = Convert.ToString(dt.Rows[0][0]);
            lblFromCity.Text = Convert.ToString(dt.Rows[0][1]);
            lblFromState.Text = Convert.ToString(dt.Rows[0][2]);
            lblTO.Text = Convert.ToString(dt.Rows[0][3]);
            lblToCity.Text = Convert.ToString(dt.Rows[0][4]);
            lblToState.Text = Convert.ToString(dt.Rows[0][5]);
            lblTag.Text = Convert.ToString(dt.Rows[0][6]);
            lblDriver.Text = Convert.ToString(dt.Rows[0][8]);
            ViewState["LoadId"] = Convert.ToString(dt.Rows[0][9]);
            barCurrent.HeaderText = "Load Id : " + Convert.ToString(dt.Rows[0][9]);
            ViewState["DriverId"] = Convert.ToString(dt.Rows[0][10]);
        }
        if (dt != null) { dt.Dispose(); dt = null; }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (ViewState["LoadId"] != null)
        {
            Response.Redirect("~/Manager/Dispatch.aspx?" + Convert.ToString(ViewState["LoadId"]));
        }
    }
}

