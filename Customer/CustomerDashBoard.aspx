<%@ Page AutoEventWireup="true" CodeFile="CustomerDashBoard.aspx.cs" Inherits="CustomerDashBoard"
    Language="C#" MasterPageFile="~/Customer/CustomerMaster.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/LoadGridManager.ascx" TagName="LoadGridManager" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">
    <table id="ContentTbl1" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr >
            <td align="left" style="width: 100%">
                <table id="tblbox1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            Get Loads:
                            <asp:DropDownList ID="dropStatus" runat="server">
                            </asp:DropDownList>
                            <asp:Button ID="btnShow" runat="server" CssClass="btnstyle" OnClick="btnShow_Click"
                                Text="Show" /></td>
                        <td align="right">
                            Track Load:
                            <asp:TextBox ID="txtContainer" runat="server">
                            </asp:TextBox>
                            <asp:DropDownList ID="ddlContainer" runat="server">
                                <asp:ListItem Selected="True" Value="1">Container#</asp:ListItem>
                                <asp:ListItem Value="2">Ref#</asp:ListItem>
                                <asp:ListItem Value="3">Booking#</asp:ListItem>
                                <asp:ListItem Value="4">Chasis#</asp:ListItem>
                                <asp:ListItem Value="5">Load#</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Button ID="btnSearch" runat="server" CssClass="btnstyle" OnClick="btnSearch_Click"
                                Text="Search" />
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <uc2:GridBar ID="barManager" runat="server" HeaderText="Loads" 
                                 />
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="5" src="../images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <uc1:LoadGridManager ID="grdManager" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
