﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LoadDetails
/// </summary>
public class LoadDetails
{
    public LoadDetails()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
[Serializable]
public class Customer
{
    public string TransactionId { set; get; }
    public string SiteName { set; get; }
    public string name { get; set; }
    public string orderNumber { get; set; }
    public DateTime? ExpireDate { set; get; }
    public string ExpireDateString { set; get; }
    public string PartnerCode { get; set; }    
    public string customerName { get; set; }
    public string contactName { get; set; }
    public string remarks { get; set; }
    public string shippingLine { get; set; }
    public string Equipment { get; set; }
    public string EquipmentProvider { get; set; }
    public List<OriginDestination> OriginDetails { get; set; }
    public List<OriginDestination> DestinationDetails { get; set; }
    public string PickupDates { get; set; }
    public string DeliveryDates { get; set; }
    public string Origins { get; set; }
    public string Destinations { get; set; }
    public Charges Charges { get; set; }
    public string TrailerNumber { get; set; }
    public string SealNumber { get; set; }
    public string EquipmentLength { get; set; }
    public string RailPickupNum { get; set; }
    public string TripType { get; set; }
    public long OfficeLocId { get; set; }
    public long ShipingLineId { get; set; }
    public List<string> AT5 { get; set; }
    public string ContainerNumber { get; set; }
    //public string SerialNumber { get; set; }


    //public string originloaction { get; set; }
    //public string pfromdatetime { get; set; }
    //public string ptodatetime { get; set; }
    //public string destinationlocation { get; set; }
    //public string dfromdatetime { get; set; }
    //public string dtodatetime { get; set; }
    //public string EquipmentDescriptionCode { set; get; }

}
[Serializable]
public class OriginDestination
{
    public string sequenceNumber { get; set; }
    public string reasonCode { get; set; }
    public DateTime? FromDate { get; set; }
    public DateTime? ToDate { get; set; }
    public string CompanyName { get; set; }
    public string Address { get; set; }
    public string Address1 { get; set; }
    public string City { get; set; }
    public string StateorProvinceCode { get; set; }
    public string PostalCode { get; set; }
    public string Pieces { get; set; }
    public string Weight { get; set; }
    public string AppNum { set; get; }
    public string Tel { get; set; }
    public string Fax { get; set; }
    public string Email { get; set; }
    public string ContactPerson { get; set; }


    //public List<DescriptionMarksList> DescriptionMarksList { get; set; }

}

//public class DescriptionMarksList
//{
//    public string LadingLineItemNumber { set; get; }
//    public string LadingDescription { set; get; }
//    public string CommodityCode { set; get; }
//    public string CommodityCodeQualifier { set; get; }
//    public string PackagingCode { set; get; }
//    public string MarksandNumbers { set; get; }
//    public string MarksandNumbersQualifier { set; get; }
//    public string CommodityCodeQualifier1 { set; get; }
//    public string CommodityCode1 { set; get; }
//    public string CompartmentIDCode { set; get; }

//}


//public class DatetimeDetailsList
//{
//    public string pDateQualifier { get; set; }
//    public string pDate { get; set; }
//    public string pTimeQualifier { get; set; }
//    public string pTime { get; set; }
//    public string ptoDateQualifier { get; set; }
//    public string ptoDate { get; set; }
//    public string ptoTimeQualifier { get; set; }
//    public string ptoTime { get; set; }
//    public string dDateQualifier { get; set; }
//    public string dDate { get; set; }
//    public string dTimeQualifier { get; set; }
//    public string dTime { get; set; }
//    public string dtoDateQualifier { get; set; }
//    public string dtoDate { get; set; }
//    public string dtoTimeQualifier { get; set; }
//    public string dtoTime { get; set; }
//}
[Serializable]
public class Charges
{
    public string Weight { get; set; }
    public string WeightQualifier { get; set; }
    public string FreightRate { get; set; }
    public string RateValueQualifier { get; set; }
    public string Charge { get; set; }
    public string Advances { get; set; }

}

public class LoadDetail
{
    public LoadDetail()
    { }
    public string TransactionId { set; get; }
    public string AggregatorCode { set; get; }
    public string PartnerCode { set; get; }
    public string OrderNumber { set; get; }
    public string TransactionSetPurpose { set; get; }
    public string ReferenceIdentification { set; get; }
    public string ReferenceIdentificationQualifier { set; get; }
    //(22 Special Charge or Allowance Code
    //BN Booking Number
    //EQ Equipment Number
    //KK Delivery Reference
    //LU Location Number
    //MB Master Bill of Lading
    //OC Ocean Container Number
    //SI Shipper's Identifying Number for Shipment (SID)
    //ABS Vessel Name
    //RSN Reservation Number)
    public string InterlineInformationAlphaCode { set; get; }
    public string InterlineInformationRoutingSequenceCode { set; get; }
    public string InterlineInformationCityName { set; get; }
    public string InterlineInformationTransportationMethodTypeCode { set; get; }
    public string InterlineInformationStateorProvinceCode { set; get; }
    public string RailPickup { set; get; }
    //            (J Motor   X Intermodal(Piggyback))
    public int? Offset { set; get; }
    public DateTime? ExpireDate { set; get; }
    public List<string> Notes { set; get; }
    public List<OrganizationDetails> OrganizationDetails { set; get; }
    public List<EquipmentDetails> EquipmentDetails { set; get; }
    public List<Stop> Stops { set; get; }
    public WeightAndCharges WeightAndCharges { set; get; }
    public List<HandlingAndService> HandlingAndServices { set; get; }
    public List<string> AT5 { get; set; }

}

public class OrganizationDetails
{
    public string EntityIdentifierCode { set; get; }
    //            (N101-->BT Bill-to-Party,
    //CA Carrier,
    //CB Customs Broker,
    //CN Consignee,
    //RD Destination Intermodal Ramp,
    //RO Original Intermodal Ramp,
    //SF Ship From,
    //SH Shipper)
    public string Name { set; get; }
    public string IdentificationCodeQualifier { set; get; }
    //            (N103-->2 Standard Carrier Alpha Code(SCAC),
    //56 Division,
    //FA Facility Identification)
    public string IdentificationCode { set; get; }
    public string Address1 { set; get; }
    public string Address2 { set; get; }
    public string CityName { set; get; }
    public string StateorProvinceCode { set; get; }
    public string PostalCode { set; get; }
    public string ContactName { set; get; }
    public string TeleNumber { set; get; }
    public string Fax { set; get; }
    public string Email { set; get; }
}

public class EquipmentDetails
{
    public string EquipmentInitial { set; get; }
    public string EquipmentNumber { set; get; }
    public string Weight { set; get; }
    public string WeightQualifier { set; get; }
    public string WeightAllowance { set; get; }
    public string TareWeight { set; get; }
    public string Dunnage { set; get; }
    public string Volume { set; get; }
    public string VolumeUnitQualifier { set; get; }
    public string OwnershipCode { set; get; }
    public string EquipmentDescriptionCode { set; get; } // (CN Container, TL Trailer(not otherwise specified),TV Truck or Van)           
    public string StandardCarrierAlphaCode1 { set; get; }
    public string TemperatureControl { set; get; }
    public string Position { set; get; }
    public string EquipmentLength { set; get; }
    public string TareQualifierCode { set; get; }
    public string WeightUnitCode { set; get; }
    public string EquipmentNumberCheckDigit { set; get; }
    public string TypeofServiceCode { set; get; }
    public string Height { set; get; }
    public string Width { set; get; }
    public string EquipmentType { set; get; }
    public string StandardCarrierAlphaCode2 { set; get; }
    public string CarTypeCode { set; get; }
    public string SealNumber { set; get; }
}

public class Stop
{
    public string SequenceNumber { set; get; }
    public string ReasonCode { set; get; }
    public string ReferenceIdentification { set; get; }
    public string ReferenceIdentificationQualifier { set; get; }
    public string Weight { set; get; }
    public string Pices { set; get; }
    public string AppNum { set; get; }
    public List<DatetimeDetails> DatetimeDetails { set; get; }
    public OrganizationDetails OrganizationDetails { set; get; }
    public List<DescriptionMarks> DescriptionMarks { set; get; }

}

public class DatetimeDetails
{
    public string DateQualifier { set; get; }
    public string Date { set; get; }
    public string TimeQualifier { set; get; }
    public string Time { set; get; }
    public string TimeCode { set; get; }
}

public class DescriptionMarks
{
    public string LadingLineItemNumber { set; get; }
    public string LadingDescription { set; get; }
    public string CommodityCode { set; get; }
    public string CommodityCodeQualifier { set; get; }
    public string PackagingCode { set; get; }
    public string MarksandNumbers { set; get; }
    public string MarksandNumbersQualifier { set; get; }
    public string CommodityCodeQualifier1 { set; get; }
    public string CommodityCode1 { set; get; }
    public string CompartmentIDCode { set; get; }
}

public class WeightAndCharges
{
    public string Weight { set; get; }

    public string WeightQualifier { set; get; }

    public string FreightRate { set; get; }

    public string RateValueQualifier { set; get; }
    public string Charge { set; get; }

    public string Advances { set; get; }

    public string PrepaidAmount { set; get; }

    public string SpecialChargeorAllowanceCode { set; get; }

    public string Volume { set; get; }

    public string VolumeUnitQualifier { set; get; }

    public string LadingQuantity { set; get; }

    public string WeightUnitCode { set; get; }

    public string TariffNumber { set; get; }

    public string DeclaredValue { set; get; }

    public string RateValueQualifier2 { set; get; }
}

public class HandlingAndService
{
    public string SpecialHandlingCode { set; get; }
    public string SpecialServicesCode { set; get; }
    public string SpecialHandlingDescription { set; get; }
}

//public class CustEquipmentDetails
//{
//public string EquipmentDescriptionCode { set; get; } // (CN Container, TL Trailer(not otherwise specified),TV Truck or Van)
//}