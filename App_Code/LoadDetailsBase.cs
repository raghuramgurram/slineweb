﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;


    public class LoadDetailsBase
    {
        public LoadDetailsBase()
        {
        }

        public DataTable loadloaddetails(int Loadid)
        {
            try
            {
                parametereforloadDetails parameter = new parametereforloadDetails();
                parameter.bint_LoadId = Loadid;
                string[] str = { "bint_LoadId" };
                BaseClass bis = new BaseClass();
                DataSet ds = bis.Loaddata(parameter, "SP_GetLoadDetailsByLoadID", str);
                DataTable dt = ds.Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable loadfromaddresslist(int Loadid)
        {
            try
            {
                parametereforloadDetails parameter = new parametereforloadDetails();
                parameter.bint_LoadId = Loadid;
                string[] str = { "bint_LoadId" };
                BaseClass bis = new BaseClass();
                DataSet ds = bis.Loaddata(parameter, "SP_GetLoadfromByLoadID", str);
                DataTable dt = ds.Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable loadtoaddresslist(int Loadid)
        {
            try
            {
                parametereforloadDetails parameter = new parametereforloadDetails();
                parameter.bint_LoadId = Loadid;
                string[] str = { "bint_LoadId" };
                BaseClass bis = new BaseClass();
                DataSet ds = bis.Loaddata(parameter, "SP_GetLoadtoaddressByLoadID", str);
                DataTable dt = ds.Tables[0];
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public DataTable loaddatesforapp(int Loadid)
        {
            try
            {
                parametereforloadDetails parameter = new parametereforloadDetails();
                parameter.bint_LoadId = Loadid;
                string[] str = { "bint_LoadId" };
                BaseClass bis = new BaseClass();
                DataSet ds = bis.Loaddata(parameter, "SP_GetLoadDeliveryAppointmentDateByLoadID", str);
                DataTable dt = ds.Tables[0];
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool SavechassisNumber(string chaisses, int loadid)
        {
            bool returnvalue = false;
            try
            {
                int i = -1;
                parametereforloadDetails parameter = new parametereforloadDetails();
                parameter.bint_Chasis = chaisses;
                parameter.bint_LoadId = loadid;
                string[] str = { "bint_Chasis", "bint_LoadId" };
                BaseClass bis = new BaseClass();
                i = bis.savedata(parameter, "sp_Update_chasis", str);
                if (i == 1)
                    returnvalue = true;
                else
                    returnvalue = false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return returnvalue;
        }

    }

public class parametereforloadDetails : NewBaseClass
    {
        public parametereforloadDetails()
        {
        }
        
    }