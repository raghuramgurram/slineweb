using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CustomerChargeDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Load Charges";
        if (!IsPostBack)
        {
            string BackPage = "";
            if (Request.QueryString.Count > 0 && Session["CustomerId"] != null)
            {
                try
                {
                    ViewState["LoadId"] = Convert.ToInt64(Request.QueryString[0]);
                    LoadReceivables1.LoadId = Convert.ToString(ViewState["LoadId"]);
                    LoadReceivables1.CustomerId = Convert.ToString(Session["CustomerId"]);
                    lblBarText.Text = "Load Id : " + Convert.ToString(ViewState["LoadId"]);
                    if (Session["RedirectBackURL"] != null && ViewState["LoadId"] != null)
                    {
                        BackPage = Convert.ToString(Session["RedirectBackURL"]);
                        Session["RedirectBackURL"] = null;
                        LoadReceivables1.RedirectBackURL = BackPage + Convert.ToInt64(ViewState["LoadId"]);
                    }
                    else
                        LoadReceivables1.RedirectBackURL = Convert.ToString(("~/Customer/CustomerPayments.aspx?" + Convert.ToInt64(ViewState["LoadId"])));
                }
                catch
                {
                }
            }
        }
    }
}
