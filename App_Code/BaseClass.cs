﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Reflection;
using System.Configuration;


    public class BaseClass
    {
        public BaseClass()
        {
        }
        public DataSet Loaddata(object obj, string spname, string[] propertyNames)
        {
            DataSet ResultData = new DataSet();
            string cstring = ConfigurationSettings.AppSettings["myConnectionStringformsettings"].ToString();
            SqlConnection sqlcon = new SqlConnection(cstring);
            try
            {
                sqlcon.Open();
                SqlCommand cmd;
                SqlDataAdapter da = new SqlDataAdapter();
                cmd = new SqlCommand(spname, sqlcon);
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (string parname in propertyNames)
                {
                    PropertyInfo pro = obj.GetType().GetProperty(parname);
                    cmd.Parameters.Add(new SqlParameter(parname, pro.GetType())).Value = pro.GetValue(obj, null);
                }
                da.SelectCommand = cmd;
                da.Fill(ResultData);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                sqlcon.Close();
            }
            return ResultData;

        }
        public int savedata(object obj, string spname, string[] propertyNames)
        {
            int returnval = -1;
            string cstring = ConfigurationSettings.AppSettings["myConnectionStringformsettings"].ToString();
            SqlConnection sqlcon = new SqlConnection(cstring);
            try
            {
                sqlcon.Open();
                SqlCommand cmd;
                cmd = new SqlCommand(spname, sqlcon);
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (string parname in propertyNames)
                {
                    PropertyInfo pro = obj.GetType().GetProperty(parname);
                    cmd.Parameters.Add(new SqlParameter(parname, pro.GetType())).Value = pro.GetValue(obj, null);
                }
                returnval = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                sqlcon.Close();
            }
            return returnval;
        }
    }