using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CustomerPayments : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Payments";
        if (!IsPostBack)
        {
            if (Session["CustomerId"] != null)
            {
                GridFill(Convert.ToString(Session["CustomerId"]).Trim(), "Pending");
                //FindTotal();
            }
        }
    }
    protected void FindTotal(string strWhereClause)
    {
        object[] objParams = new object[] { strWhereClause, "" };
        System.Data.SqlClient.SqlParameter[] objsqlparams = null;
        objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_GetPendingTotalOfCustomerByCriteria", objParams);
        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_GetPendingTotalOfCustomerByCriteria", objsqlparams);
        lblTotalDisplay.Text = "Total : " + objsqlparams[objsqlparams.Length - 1].Value;
        objsqlparams = null;
        objParams = null;
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (ViewState["FromDate"] != null)
        {
            datFromDate.SetInitialDate = false;
            datToDate.SetInitialDate = false;
            datFromDate.SetDate(Convert.ToString(ViewState["FromDate"]));
            datToDate.SetDate(Convert.ToString(ViewState["ToDate"]));
            //ddlPaymentStatus.SelectedIndex = ddlPaymentStatus.Items.IndexOf(ddlPaymentStatus.Items.FindByText(Convert.ToString(ViewState["PendingStatus"])));
        }
    }
    private void GridFill(string strCustomerId,string strPending)
    {
        grdCurrent.MainTableName = "eTn_Load";
        grdCurrent.MainTablePK = "eTn_Load.bint_LoadId";
        grdCurrent.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Load.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
            "eTn_LoadStatus.nvar_LoadStatusDesc as 'LoadStatus','$' + Convert(varchar(20),isnull(eTn_Receivables.num_GrossAmount,0)) as 'Gross'," +
            "'$' + Convert(varchar(20),isnull(eTn_Receivables.num_LumperCharges,0)) as 'Lumper','$' + Convert(varchar(20),isnull(eTn_Receivables.num_DetentionCharges,0)) as 'Dtn.'," +
            "'$' + Convert(varchar(22),(isnull(eTn_Receivables.num_OtherCharges1,0) + isnull(eTn_Receivables.num_OtherCharges2,0) + " +
            " isnull(eTn_Receivables.num_OtherCharges3,0) + isnull(eTn_Receivables.num_OtherCharges4,0))) as 'Other'," +
            "'$' + Convert(varchar(20),isnull(eTn_Receivables.num_AdvanceReceived,0)) as 'Adv.'," +
            "'<a href=CustomerChargeDetails.aspx?'+Convert(varchar(20),eTn_Load.bint_LoadId)+'>'+'$' + Convert(varchar(25),(isnull([eTn_Receivables].[num_GrossAmount],0)+isnull([eTn_Receivables].[num_AdvanceReceived],0)+isnull([eTn_Receivables].[num_Dryrun],0)+isnull([eTn_Receivables].[num_FuelSurchargeAmount],0)+isnull([eTn_Receivables].[num_SurchargeAmount],0)+isnull([eTn_Receivables].[num_PierTermination],0)+isnull([eTn_Receivables].[bint_ExtraStops],0)+isnull([eTn_Receivables].[num_LumperCharges],0)+" +
            "isnull([eTn_Receivables].[num_DetentionCharges],0)+isnull([eTn_Receivables].[num_ChassisSplit],0)+isnull([eTn_Receivables].[num_ChassisRent],0)+isnull([eTn_Receivables].[num_Pallets],0)+isnull([eTn_Receivables].[num_ContainerWashout],0)+isnull([eTn_Receivables].[num_YardStorage],0)+isnull([eTn_Receivables].[num_RampPullCharges],0)+isnull([eTn_Receivables].[num_TriaxleCharge],0)+isnull([eTn_Receivables].[num_TransLoadCharges],0)+" +
            "isnull([eTn_Receivables].[num_HeavyLightScaleCharges],0)+isnull([eTn_Receivables].[num_ScaleTicketCharges],0)+isnull([eTn_Receivables].[num_OtherCharges1],0)+isnull([eTn_Receivables].[num_OtherCharges2],0)+isnull([eTn_Receivables].[num_OtherCharges3],0)+isnull([eTn_Receivables].[num_OtherCharges4],0)))+'</a>' AS 'Total'";
        grdCurrent.InnerJoinClauseWithOnlySingleRowTables = " left outer join eTn_Receivables on eTn_Receivables.bint_LoadId = eTn_Load.bint_LoadId " +
            "  inner join eTn_LoadStatus on eTn_LoadStatus.bint_LoadStatusId = eTn_Load.bint_LoadStatusId";
        if (string.Compare(strPending, "Pending", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            strPending = " and (lower(isnull(eTn_Receivables.nvar_ReceivableStatus,'pending'))='pending' or len(eTn_Receivables.nvar_ReceivableStatus)=0)"+
            " and (eTn_Load.date_DateOfCreation >= '" + datFromDate.Date + "' and eTn_Load.date_DateOfCreation <= DateAdd(dd,1,'" + datToDate.Date + "'))";
        }
        else if (string.Compare(strPending, "Paid", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            strPending = " and (lower(eTn_Receivables.nvar_ReceivableStatus)='paid')" +
            " and (eTn_Load.date_DateOfCreation >= '" + datFromDate.Date + "' and eTn_Load.date_DateOfCreation <= DateAdd(dd,1,'" + datToDate.Date + "'))";
        }       
        grdCurrent.SingleRowColumnsWhereClause = "eTn_Load.bint_CustomerId = " + strCustomerId +
        strPending;
        FindTotal("eTn_Load.bint_CustomerId = " + strCustomerId + strPending);
        grdCurrent.SingleRowColumnsOrderByClause = " order by eTn_Load.bint_LoadId desc";
        grdCurrent.VisibleColumnsList = "Load;Origin;Destination;Delivered;Gross;Lumper;Dtn.;Other;Adv.;Total";
        //grdCurrent.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver";
        //grdCurrent.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver.bint_LoadId";
        //grdCurrent.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
        //    "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_WorkPhone") + " as 'Driver/Carrier'," +
        //    CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_ToCity;eTn_AssignDriver.nvar_Tostate", "To");
        //grdCurrent.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
        //    "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver') ";
        grdCurrent.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
        grdCurrent.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
        grdCurrent.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + ";" + 
            CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + ";" +
            "eTn_TrackLoad.date_CreateDate as 'Delivered'";
        grdCurrent.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
            "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
            "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
        grdCurrent.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=(select eTn_LoadStatus.bint_LoadStatusId from eTn_LoadStatus where lower(eTn_LoadStatus.nvar_LoadStatusDesc) = 'delivered')";
        grdCurrent.LastColumnLinksList = "Invoice";
        grdCurrent.LastColumnPagesList = "~/Customer/CustomerInvoice.aspx";
        grdCurrent.DeleteVisible = false;
        grdCurrent.LastLocationVisible = false;
        grdCurrent.LoadStatus = "All";
        Session["RedirectBackURL"] = "~/Customer/CustomerPayments.aspx?";
        grdCurrent.BindGrid(0);                     
    }
    protected void btnShow_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            GridFill(Convert.ToString(Session["CustomerId"]).Trim(), "Pending");
            ViewState["FromDate"] = datFromDate.Date;
            ViewState["ToDate"] = datToDate.Date;
            ViewState["PendingStatus"] = "Pending";
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string whereClause = "";
        if (Session["CustomerId"] != null)
        {
            if (txtSearchText.Text.Trim().Length > 0)
            {
                switch (ddlTypes.SelectedIndex)
                {
                    case 1:
                        whereClause = " and eTn_Load.[nvar_Ref] like '" + txtSearchText.Text.Trim() + "%'";
                        break;
                    case 0:
                        try
                        {
                            whereClause = " and eTn_Load.[bint_LoadId]=" + Convert.ToInt64(txtSearchText.Text.Trim());
                        }
                        catch
                        {
                            whereClause = " and eTn_Load.[bint_LoadId]=0";
                        }
                        break;
                }
            }
            else
            {
                whereClause = "";
            }
        }
        GridFill(Convert.ToString(Session["CustomerId"]).Trim(), whereClause);
    }
}
