<%@ Page AutoEventWireup="true" CodeFile="InvoiceableLoads.aspx.cs" Inherits="InvoiceableLoads"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>
                <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="middle">
                            <asp:Label ID="lblText" runat="server" Text="Customer Name : "></asp:Label>
                            <asp:DropDownList ID="ddlCustomers" runat="server"> 
                            </asp:DropDownList>                            
                            &nbsp;
                            <asp:Button ID="btnSearch" runat="server" CssClass="btnStyle" Text="Search" OnClick="btnSearch_Click" />
                        </td>
                    </tr>
                </table>                
            </td>
        </tr>        
    </table>    
</asp:Content>
