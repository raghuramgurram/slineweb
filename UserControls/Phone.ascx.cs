using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Phone : System.Web.UI.UserControl
{
    public bool IsRequiredField
    {
        set
        {
            Req1.Enabled = value; Req2.Enabled = value; Req3.Enabled = value;
        }
    }
    //to add validation Group if requried(madhav)
    public string ValidationGroupforUser
    {        
        set
        {
            Req1.ValidationGroup = value; Req2.ValidationGroup = value; Req3.ValidationGroup = value;
        }
    }
    public string Text
    {
        get
        {
            if (txtPhone1.Text.Trim().Length == 3 && txtPhone2.Text.Trim().Length == 3 && txtPhone3.Text.Trim().Length == 4)
                return txtPhone1.Text.Trim() + txtPhone2.Text.Trim() + txtPhone3.Text.Trim();
            else
                return "0";
        }
        set
        {
            if (Convert.ToString(value).Trim().Length == 10)
            {
                txtPhone1.Text = Convert.ToString(value).Trim().Substring(0, 3);
                txtPhone2.Text = Convert.ToString(value).Trim().Substring(3, 3);
                txtPhone3.Text = Convert.ToString(value).Trim().Substring(6, 4);
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    //Clearing the text for the Controle (Madhav)
    public void ClearText()
    {
        txtPhone1.Text = "";
        txtPhone2.Text = "";
        txtPhone3.Text = "";
    }
}
