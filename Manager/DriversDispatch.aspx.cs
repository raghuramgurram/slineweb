using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

public partial class Manager_DriversDispatch : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
         Session["TopHeaderText"] = "Drivers Dispatch Details";
         if (!IsPostBack)
         {
             long officeLocationId = 0;
             if (Session["OfficeLocationID"] != null)
             {
                 officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
             }
             DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriverwithDispatchNamesForaLocation", new object[] { officeLocationId });
             //DataSet allDriverNamesds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetActiveDrivers");
             DataSet allDriverNamesds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetActiveDriversForaLocation", new object[] { officeLocationId });
             if (ds.Tables[0].Rows.Count > 0)
             {                
                 for (int k = 0; k < ds.Tables[0].Rows.Count; k++)
                 {
                     if (k != ds.Tables[0].Rows.Count - 1)
                     {
                         if (Convert.ToString(ds.Tables[0].Rows[k][0]).Trim() == Convert.ToString(ds.Tables[0].Rows[k + 1][0]).Trim())
                         {
                                 ds.Tables[0].Rows[k + 1][2] = int.Parse(ds.Tables[0].Rows[k + 1][2].ToString()) + int.Parse(ds.Tables[0].Rows[k][2].ToString());

                                 ds.Tables[0].Rows[k + 1][3] = int.Parse(ds.Tables[0].Rows[k + 1][3].ToString()) + int.Parse(ds.Tables[0].Rows[k][3].ToString());
                             
                             ds.Tables[0].Rows[k].Delete();
                            
                         }                         
                     }
                 }
                 
                 //ds.Tables[0].Columns.Remove(ds.Tables[0].Columns[0]);
                 ds.AcceptChanges();
                 for (int k = 0; k < ds.Tables[0].Rows.Count; k++)
                 {
                     ds.Tables[0].Rows[k][2] = " (" + ds.Tables[0].Rows[k][2] + ")";
                     ds.Tables[0].Rows[k][3] = " (" + ds.Tables[0].Rows[k][3] + ")";
                 }
                 ds.AcceptChanges();
                 DriverwithDispatchGrid.DataSource = ds.Tables[0];
                 DriverwithDispatchGrid.DataBind();
             }
             

             if (allDriverNamesds.Tables[0].Rows.Count > 0)
             {
                 for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                 {
                     for (int j = 0; j < allDriverNamesds.Tables[0].Rows.Count; j++)
                     {
                         if (Convert.ToString(ds.Tables[0].Rows[i][0]).Trim() == Convert.ToString(allDriverNamesds.Tables[0].Rows[j][0]).Trim())
                         {
                             allDriverNamesds.Tables[0].Rows[j].Delete();
                         }
                     }
                     allDriverNamesds.AcceptChanges();
                 }
                 allDriverNamesds.Tables[0].Columns.Remove(allDriverNamesds.Tables[0].Columns[0]);
                 allDriverNamesds.AcceptChanges();
                 DriverwithoutDispatchGrid.DataSource = allDriverNamesds.Tables[0];
                 DriverwithoutDispatchGrid.DataBind();
             }
             if (ds.Tables[0].Rows.Count > 0)
             {
                 ds.Tables[0].Columns.Remove(ds.Tables[0].Columns[0]);
                 ds.AcceptChanges();
                 DriverwithDispatchGrid.DataSource = ds.Tables[0];
                 DriverwithDispatchGrid.DataBind();
             }
             barManager.HeaderText = "Drivers with Dispatch (" + ds.Tables[0].Rows.Count + ")";
             barManager1.HeaderText = "Drivers without Dispatch (" + allDriverNamesds.Tables[0].Rows.Count + ")";
         }
    }

    protected void GVDriversDispatch_RowDataBound(object sender, GridViewRowEventArgs e)
    {
      if (e.Row.RowType == DataControlRowType.Header)
      {
         e.Row.Cells[1].Width = new Unit("80px");
         e.Row.Cells[2].Width = new Unit("80px");
      }
    }
}
