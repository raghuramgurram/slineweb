﻿using System;
using System.Data;
using System.Xml;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Data;

/// <summary>
/// Summary description for SqlHelper
/// </summary>
public sealed class  SqlHelper
{
	public SqlHelper()
	{
		//
		// TODO: Add constructor logic here
		//
    }

    #region For Out Values
    public static SqlParameter[] PrepareSqlParamsFromSP(string connectionString, string spName, object[] parameterValues)
    {
        if (connectionString == null || connectionString.Length == 0) throw new ArgumentNullException("connectionString");
        if (spName == null || spName.Length == 0) throw new ArgumentNullException("spName");
        SqlParameter[] commandParameters = null;
        Microsoft.Practices.EnterpriseLibrary.Data.Database db = null;
        System.Data.Common.DbCommand dbCommand = null;
        try
        {
            db = DatabaseFactory.CreateDatabase("eTransportString");
            dbCommand = db.GetStoredProcCommand(spName);
            db.DiscoverParameters(dbCommand);
            int iCount = 0;
            foreach (System.Data.Common.DbParameter dbParam in dbCommand.Parameters)
            {
                if (dbParam.Direction == ParameterDirection.Input || dbParam.Direction == ParameterDirection.Output || dbParam.Direction==ParameterDirection.InputOutput )
                {
                    dbParam.Value = parameterValues[iCount];
                    iCount = iCount + 1;
                }
            }
            if (iCount == parameterValues.Length)
             {
                commandParameters = new SqlParameter[iCount];
                iCount = 0;
                foreach (System.Data.Common.DbParameter dbParam in dbCommand.Parameters)
                {
                    if (dbParam.Direction == ParameterDirection.Input || dbParam.Direction == ParameterDirection.Output || dbParam.Direction == ParameterDirection.InputOutput)
                    {
                        commandParameters[iCount] = new SqlParameter();
                        commandParameters[iCount].ParameterName = dbParam.ParameterName;
                        commandParameters[iCount].DbType = dbParam.DbType;
                        commandParameters[iCount].Size = dbParam.Size;
                        commandParameters[iCount].Value = dbParam.Value;
                        iCount = iCount + 1;
                    }
                }
            }
            else
            {
                throw new ArgumentException("parameterValues");
            }
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (db != null) 
            { 
                db = null;
            }
            if (dbCommand != null) 
            { 
                dbCommand.Dispose(); dbCommand = null; 
            }
        }
        return commandParameters;
    }
    #endregion

    #region ExecuteDataset
    /// <summary>
    /// Execute a SqlCommand (that returns a resultset and takes no parameters) against the database specified in 
    /// the connection string. 
    /// </summary>
    /// <remarks>
    /// e.g.:  
    ///  DataSet ds = ExecuteDataset(connString, CommandType.StoredProcedure, "GetOrders");
    /// </remarks>
    /// <param name="connectionString">A valid connection string for a SqlConnection</param>
    /// <param name="commandText">The stored procedure name or T-SQL command</param>
    /// <returns>A dataset containing the resultset generated by the command</returns>
    public static DataSet ExecuteDataset(string connectionString, string commandText)
    {
        return ExecuteDataset(connectionString, commandText, (object[])null);
    }

    /// <summary>
    /// Execute a SqlCommand (that returns a resultset and takes no parameters) against the database specified in 
    /// the connection string. 
    /// </summary>
    /// <remarks>
    /// e.g.:  
    ///  DataSet ds = ExecuteDataset(connString, CommandType.StoredProcedure, "GetOrders");
    /// </remarks>
    /// <param name="connectionString">A valid connection string for a SqlConnection</param>
    /// <param name="commandType">The CommandType (stored procedure, text, etc.)</param>
    /// <param name="commandText">The stored procedure name or T-SQL command</param>
    /// <returns>A dataset containing the resultset generated by the command</returns>
    public static DataSet ExecuteDataset(string connectionString, CommandType commandType, string commandText)
    {
        return ExecuteDataset(connectionString, commandType, commandText, (object[])null);
    }
    /// <summary>
    /// Execute a stored procedure via a SqlCommand (that returns a resultset) against the database specified in 
    /// the connection string using the provided parameter values.  This method will query the database to discover the parameters for the 
    /// stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
    /// </summary>
    /// <remarks>
    /// This method provides no access to output parameters or the stored procedure's return value parameter.
    /// 
    /// e.g.:  
    ///  DataSet ds = ExecuteDataset(connString, "GetOrders", 24, 36);
    /// </remarks>
    /// <param name="connectionString">A valid connection string for a SqlConnection</param>
    /// <param name="commandType">The CommandType (stored procedure, text, etc.)</param>
    /// <param name="commandText">The stored procedure name or T-SQL command</param>
    /// <param name="parameterValues">An array of objects to be assigned as the input values of the stored procedure</param>
    /// <returns>A dataset containing the resultset generated by the command</returns>
    public static DataSet ExecuteDataset(string connectionString, CommandType commandType, string spName, params object[] parameterValues)
    {
        return ExecuteDataset(connectionString, spName, parameterValues);
    }
    /// <summary>
    /// Execute a SqlCommand (that returns a resultset) against the database specified in the connection string 
    /// using the provided parameters.
    /// </summary>
    /// <remarks>
    /// e.g.:  
    ///  DataSet ds = ExecuteDataset(connString, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24));
    /// </remarks>
    /// <param name="connectionString">A valid connection string for a SqlConnection</param>
    /// <param name="commandType">The CommandType (stored procedure, text, etc.)</param>
    /// <param name="commandText">The stored procedure name or T-SQL command</param>
    /// <param name="commandParameters">An array of SqlParamters used to execute the command</param>
    /// <returns>A dataset containing the resultset generated by the command</returns>
    public static DataSet ExecuteDataset(string connectionString, CommandType commandType, string spName, SqlParameter[] parameterValues)
    {
        return ExecuteDataset(connectionString, spName, parameterValues);
    }
    /// <summary>
    /// Execute a stored procedure via a SqlCommand (that returns a resultset) against the database specified in 
    /// the connection string using the provided parameter values.  This method will query the database to discover the parameters for the 
    /// stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
    /// </summary>
    /// <remarks>
    /// This method provides no access to output parameters or the stored procedure's return value parameter.
    /// 
    /// e.g.:  
    ///  DataSet ds = ExecuteDataset(connString, "GetOrders", 24, 36);
    /// </remarks>
    /// <param name="connectionString">A valid connection string for a SqlConnection</param>
    /// <param name="spName">The name of the stored procedure</param>
    /// <param name="parameterValues">An array of objects to be assigned as the input values of the stored procedure</param>
    /// <returns>A dataset containing the resultset generated by the command</returns>
    public static DataSet ExecuteDataset(string connectionString, string spName, params object[] parameterValues)//main
    {
        CommonFunctions.InsertIntoInfoLog("SqlHelper.cs :: ExecuteDataset-"+ spName + " :: Start");
        if (connectionString == null || connectionString.Length == 0) throw new ArgumentNullException("connectionString");
        if (spName == null || spName.Length == 0) throw new ArgumentNullException("spName");

        Microsoft.Practices.EnterpriseLibrary.Data.Database db = null;
        System.Data.Common.DbCommand dbCommand = null;
        DataSet ds = new DataSet();
        try
        {            
            db = DatabaseFactory.CreateDatabase("eTransportString");
            dbCommand = db.GetStoredProcCommand(spName);
            if (parameterValues != null)
            {
                if (parameterValues.Length > 0)
                {
                    db.DiscoverParameters(dbCommand);
                    int iCount = 0;
                    foreach (System.Data.Common.DbParameter dbParam in dbCommand.Parameters)
                    {
                        if (dbParam.Direction == ParameterDirection.Input || dbParam.Direction == ParameterDirection.Output || dbParam.Direction == ParameterDirection.InputOutput)
                        {
                            dbParam.Value = parameterValues[iCount];
                            iCount = iCount + 1;
                        }
                    }
                }
            }
            dbCommand.CommandTimeout = int.Parse(ConfigurationSettings.AppSettings["CommandTimeout"]);
            ds = db.ExecuteDataSet(dbCommand);
        }
        catch(Exception ex)
        {
        }
        finally
        {
            if (db != null) { db = null; }
            if (dbCommand != null) { dbCommand.Dispose(); dbCommand = null; }
        }
        CommonFunctions.InsertIntoInfoLog("SqlHelper.cs :: ExecuteDataset-" + spName + " :: END");
        return ds;
    }
    /// <summary>
    /// Execute a SqlCommand (that returns a resultset) against the database specified in the connection string 
    /// using the provided parameters.
    /// </summary>
    /// <remarks>
    /// e.g.:  
    ///  DataSet ds = ExecuteDataset(connString, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24));
    /// </remarks>
    /// <param name="connectionString">A valid connection string for a SqlConnection</param>    
    /// <param name="spName">The stored procedure name or T-SQL command</param>
    /// <param name="commandParameters">An array of SqlParamters used to execute the command</param>
    /// <returns>A dataset containing the resultset generated by the command</returns>
    public static DataSet ExecuteDataset(string connectionString, string spName, params SqlParameter[] parameterValues)//main
    {
        CommonFunctions.InsertIntoInfoLog("SqlHelper.cs :: ExecuteDataset-" + spName + " :: Start");
        if (connectionString == null || connectionString.Length == 0) throw new ArgumentNullException("connectionString");
        if (spName == null || spName.Length == 0) throw new ArgumentNullException("spName");

        Microsoft.Practices.EnterpriseLibrary.Data.Database db = null;
        System.Data.Common.DbCommand dbCommand = null;
        DataSet ds = new DataSet();
        try
        {
            db = DatabaseFactory.CreateDatabase("eTransportString");
            dbCommand = db.GetStoredProcCommand(spName);
            if (parameterValues != null)
            {
                if (parameterValues.Length > 0)
                {
                    db.DiscoverParameters(dbCommand);
                    int iCount = 0;
                    foreach (System.Data.Common.DbParameter dbParam in dbCommand.Parameters)
                    {
                        if (dbParam.Direction == ParameterDirection.Input || dbParam.Direction == ParameterDirection.Output || dbParam.Direction == ParameterDirection.InputOutput)
                        {
                            dbParam.Value = parameterValues[iCount].Value;
                            iCount = iCount + 1;
                        }
                    }
                }
            }
            ds = db.ExecuteDataSet(dbCommand);
        }
        catch
        {
        }
        finally
        {
            if (db != null) { db = null; }
            if (dbCommand != null) { dbCommand.Dispose(); dbCommand = null; }
        }
        CommonFunctions.InsertIntoInfoLog("SqlHelper.cs :: ExecuteDataset-" + spName + " :: END");
        return ds;
    }    
    #endregion

    #region ExecuteNonQuery
    /// <summary>
    /// Execute a SqlCommand (that returns no resultset and takes no parameters) against the database specified in 
    /// the connection string
    /// </summary>
    /// <remarks>
    /// e.g.:  
    ///  int result = ExecuteNonQuery(connString, CommandType.StoredProcedure, "PublishOrders");
    /// </remarks>
    /// <param name="connectionString">A valid connection string for a SqlConnection</param>
    /// <param name="commandType">The CommandType (stored procedure, text, etc.)</param>
    /// <param name="commandText">The stored procedure name or T-SQL command</param>
    /// <returns>An int representing the number of rows affected by the command</returns>
    public static int ExecuteNonQuery(string connectionString, CommandType commandType, string commandText)
    {        
        return ExecuteNonQuery(connectionString, commandText, (object[])null);
    }
    /// <summary>
    /// Execute a SqlCommand (that returns no resultset) against the database specified in the connection string 
    /// using the provided parameters
    /// </summary>
    /// <remarks>
    /// e.g.:  
    ///  int result = ExecuteNonQuery(connString, CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24));
    /// </remarks>
    /// <param name="connectionString">A valid connection string for a SqlConnection</param>
    /// <param name="commandType">The CommandType (stored procedure, text, etc.)</param>
    /// <param name="commandText">The stored procedure name or T-SQL command</param>
    /// <param name="commandParameters">An array of SqlParamters used to execute the command</param>
    /// <returns>An int representing the number of rows affected by the command</returns>
    public static int ExecuteNonQuery(string connectionString, CommandType commandType, string commandText, params object[] commandParameters)
    {
        return ExecuteNonQuery(connectionString, commandText, commandParameters);
    }
    /// <summary>
    /// Execute a SqlCommand (that returns no resultset) against the database specified in the connection string 
    /// using the provided parameters
    /// </summary>
    /// <remarks>
    /// e.g.:  
    ///  int result = ExecuteNonQuery(connString, CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24));
    /// </remarks>
    /// <param name="connectionString">A valid connection string for a SqlConnection</param>
    /// <param name="commandType">The CommandType (stored procedure, text, etc.)</param>
    /// <param name="commandText">The stored procedure name or T-SQL command</param>
    /// <param name="parameterValues">An array of objects to be assigned as the input values of the stored procedure</param>
    /// <returns>An int representing the number of rows affected by the command</returns>
    public static int ExecuteNonQuery(string connectionString, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
    {
        return ExecuteNonQuery(connectionString, commandText, commandParameters);
    }
    /// <summary>
    /// Execute a stored procedure via a SqlCommand (that returns no resultset) against the database specified in 
    /// the connection string using the provided parameter values.  This method will query the database to discover the parameters for the 
    /// stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
    /// </summary>
    /// <remarks>
    /// This method provides no access to output parameters or the stored procedure's return value parameter.
    /// 
    /// e.g.:  
    ///  int result = ExecuteNonQuery(connString, "PublishOrders", 24, 36);
    /// </remarks>
    /// <param name="connectionString">A valid connection string for a SqlConnection</param>
    /// <param name="spName">The name of the stored prcedure</param>
    /// <param name="parameterValues">An array of objects to be assigned as the input values of the stored procedure</param>
    /// <returns>An int representing the number of rows affected by the command</returns>
    public static int ExecuteNonQuery(string connectionString, string spName, params object[] parameterValues)
    {
        CommonFunctions.InsertIntoInfoLog("SqlHelper.cs :: ExecuteNonQuery-" + spName + " :: Start");
        if (connectionString == null || connectionString.Length == 0)
            throw new ArgumentNullException("connectionString");
        if (spName == null || spName.Length == 0)
            throw new ArgumentNullException("spName");

        Microsoft.Practices.EnterpriseLibrary.Data.Database db = null;
        System.Data.Common.DbCommand dbCommand = null;
        int iRes = 0;
        try
        {
            db = DatabaseFactory.CreateDatabase("eTransportString");
            dbCommand = db.GetStoredProcCommand(spName);
            if (parameterValues != null)
            {
                if (parameterValues.Length > 0)
                {
                    db.DiscoverParameters(dbCommand);
                    int iCount = 0;
                    foreach (System.Data.Common.DbParameter dbParam in dbCommand.Parameters)
                    {
                        if (dbParam.Direction == ParameterDirection.Input || dbParam.Direction == ParameterDirection.Output || dbParam.Direction == ParameterDirection.InputOutput)
                        {
                            dbParam.Value = parameterValues[iCount];
                            iCount = iCount + 1;
                        }
                    }
                }
            }
            iRes = db.ExecuteNonQuery(dbCommand);
        }
        catch(Exception ex)
        {
        }
        finally
        {
            if (db != null) { db = null; }
            if (dbCommand != null) { dbCommand.Dispose(); dbCommand = null; }
        }
        CommonFunctions.InsertIntoInfoLog("SqlHelper.cs :: ExecuteNonQuery-" + spName + " :: END");
        return iRes;
        
    }
    /// <summary>
    /// Execute a stored procedure via a SqlCommand (that returns no resultset) against the database specified in 
    /// the connection string using the provided parameter values.  This method will query the database to discover the parameters for the 
    /// stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
    /// </summary>
    /// <remarks>
    /// This method provides no access to output parameters or the stored procedure's return value parameter.
    /// 
    /// e.g.:  
    ///  int result = ExecuteNonQuery(connString, "PublishOrders", 24, 36);
    /// </remarks>
    /// <param name="connectionString">A valid connection string for a SqlConnection</param>
    /// <param name="spName">The name of the stored prcedure</param>
    /// <param name="commandParameters">An array of SqlParamters used to execute the command</param>
    /// <returns>An int representing the number of rows affected by the command</returns>
    public static int ExecuteNonQuery(string connectionString, string spName, params SqlParameter[] parameterValues)
    {
        CommonFunctions.InsertIntoInfoLog("SqlHelper.cs :: ExecuteNonQuery-" + spName + " :: Start");
        if (connectionString == null || connectionString.Length == 0) throw new ArgumentNullException("connectionString");
        if (spName == null || spName.Length == 0) throw new ArgumentNullException("spName");

        Microsoft.Practices.EnterpriseLibrary.Data.Database db = null;
        System.Data.Common.DbCommand dbCommand = null;
        int iRes = 0;
        try
        {
            db = DatabaseFactory.CreateDatabase("eTransportString");
            dbCommand = db.GetStoredProcCommand(spName);
            int iCount = 0;
            if (parameterValues != null)
            {
                if (parameterValues.Length > 0)
                {                    
                    db.DiscoverParameters(dbCommand);                    
                    foreach (System.Data.Common.DbParameter dbParam in dbCommand.Parameters)
                    {
                        if (dbParam.Direction == ParameterDirection.Input || dbParam.Direction == ParameterDirection.Output || dbParam.Direction == ParameterDirection.InputOutput)
                        {
                            dbParam.Value = parameterValues[iCount].Value;
                            iCount = iCount + 1;
                        }
                    }
                }
            }
            iRes = db.ExecuteNonQuery(dbCommand);
            iCount = 0;
            foreach (System.Data.Common.DbParameter dbparams in dbCommand.Parameters)
            {
                if (dbparams.Direction == ParameterDirection.ReturnValue)
                    continue;
                if (dbparams.Direction == ParameterDirection.Output || dbparams.Direction == ParameterDirection.InputOutput)
                {
                    parameterValues[iCount].Value = dbparams.Value;                    
                }
                iCount = iCount + 1;
            }
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (db != null) { db = null; }
            if (dbCommand != null) { dbCommand.Dispose(); dbCommand = null; }
        }
        CommonFunctions.InsertIntoInfoLog("SqlHelper.cs :: ExecuteNonQuery-" + spName + " :: END");
        return iRes;

    }
    #endregion ExecuteNonQuery

    #region ExecuteScalar

    /// <summary>
    /// Execute a SqlCommand (that returns a 1x1 resultset and takes no parameters) against the database specified in 
    /// the connection string. 
    /// </summary>
    /// <remarks>
    /// e.g.:  
    ///  int orderCount = (int)ExecuteScalar(connString, CommandType.StoredProcedure, "GetOrderCount");
    /// </remarks>
    /// <param name="connectionString">A valid connection string for a SqlConnection</param>
    /// <param name="commandType">The CommandType (stored procedure, text, etc.)</param>
    /// <param name="commandText">The stored procedure name or T-SQL command</param>
    /// <returns>An object containing the value in the 1x1 resultset generated by the command</returns>
    public static object ExecuteScalar(string connectionString, CommandType commandType, string commandText)
    {
        return ExecuteScalar(connectionString, commandText, (object[])null);
    }

    /// <summary>
    /// Execute a SqlCommand (that returns a 1x1 resultset) against the database specified in the connection string 
    /// using the provided parameters.
    /// </summary>
    /// <remarks>
    /// e.g.:  
    ///  int orderCount = (int)ExecuteScalar(connString, CommandType.StoredProcedure, "GetOrderCount", new SqlParameter("@prodid", 24));
    /// </remarks>
    /// <param name="connectionString">A valid connection string for a SqlConnection</param>
    /// <param name="commandType">The CommandType (stored procedure, text, etc.)</param>
    /// <param name="commandText">The stored procedure name or T-SQL command</param>
    /// <param name="parameterValues">An array of objects to be assigned as the input values of the stored procedure</param>
    /// <returns>An object containing the value in the 1x1 resultset generated by the command</returns>
    public static object ExecuteScalar(string connectionString, CommandType commandType, string commandText, params object[] commandParameters)
    {
        return ExecuteScalar(connectionString, commandText, commandParameters);
    }

    /// <summary>
    /// Execute a SqlCommand (that returns a 1x1 resultset) against the database specified in the connection string 
    /// using the provided parameters.
    /// </summary>
    /// <remarks>
    /// e.g.:  
    ///  int orderCount = (int)ExecuteScalar(connString, CommandType.StoredProcedure, "GetOrderCount", new SqlParameter("@prodid", 24));
    /// </remarks>
    /// <param name="connectionString">A valid connection string for a SqlConnection</param>
    /// <param name="commandType">The CommandType (stored procedure, text, etc.)</param>
    /// <param name="commandText">The stored procedure name or T-SQL command</param>
    /// <param name="commandParameters">An array of SqlParamters used to execute the command</param>
    /// <returns>An object containing the value in the 1x1 resultset generated by the command</returns>
    public static object ExecuteScalar(string connectionString, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
    {
        return ExecuteScalar(connectionString, commandText, commandParameters);
    }

    /// <summary>
    /// Execute a stored procedure via a SqlCommand (that returns a 1x1 resultset) against the database specified in 
    /// the connection string using the provided parameter values.  This method will query the database to discover the parameters for the 
    /// stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
    /// </summary>
    /// <remarks>
    /// This method provides no access to output parameters or the stored procedure's return value parameter.
    /// 
    /// e.g.:  
    ///  int orderCount = (int)ExecuteScalar(connString, "GetOrderCount", 24, 36);
    /// </remarks>
    /// <param name="connectionString">A valid connection string for a SqlConnection</param>
    /// <param name="spName">The name of the stored procedure</param>
    /// <param name="parameterValues">An array of objects to be assigned as the input values of the stored procedure</param>
    /// <returns>An object containing the value in the 1x1 resultset generated by the command</returns>
    public static object ExecuteScalar(string connectionString, string spName, params object[] parameterValues)
    {
        CommonFunctions.InsertIntoInfoLog("SqlHelper.cs :: ExecuteScalar-" + spName + " :: Start");
        if (connectionString == null || connectionString.Length == 0) throw new ArgumentNullException("connectionString");
        if (spName == null || spName.Length == 0) throw new ArgumentNullException("spName");

        Microsoft.Practices.EnterpriseLibrary.Data.Database db = null;
        System.Data.Common.DbCommand dbCommand = null;
        object objRes = null;
        try
        {
            db = DatabaseFactory.CreateDatabase("eTransportString");
            dbCommand = db.GetStoredProcCommand(spName);
            if (parameterValues != null)
            {
                if (parameterValues.Length > 0)
                {
                    db.DiscoverParameters(dbCommand);
                    int iCount = 0;
                    foreach (System.Data.Common.DbParameter dbParam in dbCommand.Parameters)
                    {
                        if (dbParam.Direction == ParameterDirection.Input || dbParam.Direction == ParameterDirection.Output || dbParam.Direction == ParameterDirection.InputOutput)
                        {
                            dbParam.Value = parameterValues[iCount];
                            iCount = iCount + 1;
                        }
                    }
                }
            }
            objRes = db.ExecuteScalar(dbCommand);
        }
        catch
        {
        }
        finally
        {
            if (db != null) { db = null; }
            if (dbCommand != null) { dbCommand.Dispose(); dbCommand = null; }
        }
        CommonFunctions.InsertIntoInfoLog("SqlHelper.cs :: ExecuteScalar-" + spName + " :: END");
        return objRes;
    }

    /// <summary>
    /// Execute a stored procedure via a SqlCommand (that returns a 1x1 resultset) against the database specified in 
    /// the connection string using the provided parameter values.  This method will query the database to discover the parameters for the 
    /// stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
    /// </summary>
    /// <remarks>
    /// This method provides no access to output parameters or the stored procedure's return value parameter.
    /// 
    /// e.g.:  
    ///  int orderCount = (int)ExecuteScalar(connString, "GetOrderCount", 24, 36);
    /// </remarks>
    /// <param name="connectionString">A valid connection string for a SqlConnection</param>
    /// <param name="spName">The name of the stored procedure</param>
    /// <param name="commandParameters">An array of SqlParamters used to execute the command</param>
    /// <returns>An object containing the value in the 1x1 resultset generated by the command</returns>
    public static object ExecuteScalar(string connectionString, string spName, params SqlParameter[] parameterValues)
    {
        CommonFunctions.InsertIntoInfoLog("SqlHelper.cs :: ExecuteScalar-" + spName + " :: Start");
        if (connectionString == null || connectionString.Length == 0) throw new ArgumentNullException("connectionString");
        if (spName == null || spName.Length == 0) throw new ArgumentNullException("spName");

        Microsoft.Practices.EnterpriseLibrary.Data.Database db = null;
        System.Data.Common.DbCommand dbCommand = null;
        object objRes = null;
        try
        {
            string constring = System.Configuration.ConfigurationSettings.AppSettings["conString"];
            db = DatabaseFactory.CreateDatabase(constring);
            dbCommand = db.GetStoredProcCommand(spName);
            if (parameterValues != null)
            {
                if (parameterValues.Length > 0)
                {
                    db.DiscoverParameters(dbCommand);
                    int iCount = 0;
                    foreach (System.Data.Common.DbParameter dbParam in dbCommand.Parameters)
                    {
                        if (dbParam.Direction == ParameterDirection.Input || dbParam.Direction == ParameterDirection.Output || dbParam.Direction == ParameterDirection.InputOutput)
                        {
                            dbParam.Value = parameterValues[iCount].Value;
                            iCount = iCount + 1;
                        }
                    }
                }
            }
            objRes = db.ExecuteScalar(dbCommand);
        }
        catch
        {
        }
        finally
        {
            if (db != null) { db = null; }
            if (dbCommand != null) { dbCommand.Dispose(); dbCommand = null; }
        }
        CommonFunctions.InsertIntoInfoLog("SqlHelper.cs :: ExecuteScalar-" + spName + " :: END");
        return objRes;
    }
    #endregion 
}

