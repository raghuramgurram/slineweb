 using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class newload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Edit / New Load";
        txtlastfree.DisplayError = false;
        txtOrderBillDate.DisplayError = false;
        if (!Page.IsPostBack)
        {
            //ddlTripType.Visible = false;
            //txtcontainer1.Visible = false;
            //txtchasis.Visible = false;
            TripVisibilityChange();
            FillDetails();            
            //FillContainerType();
            if (Request.QueryString.Count > 0)
            {
                ViewState["LoadID"] = Request.QueryString[0].Trim();
                try
                {
                    ViewState["Mode"] = "Edit";
                    FillGrids(Convert.ToInt64(ViewState["LoadID"]));
                    FillEditDetails(Convert.ToInt64(ViewState["LoadID"]));
                    Session["TopHeaderText"] = "Edit Load";
                    Session["BackPage4"] = "NewLoad.aspx?" + Convert.ToString(ViewState["LoadID"]);
                    //string Script = "<script type='text/javascript' language='javascript'>window.onload=function ValidateRoundTrip('" + ddltrip.ClientID + "','" + txtcontainer1.ClientID + "','" + txtchasis.ClientID + "');}</script>";
                    //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "onload", Script);
                   // string Script = "<script type='text/javascript' language='javascript'>window.onload=function ValidateRoundTrip(DropDownId,txtcontainer1,txtchasis, ddlTripType)" +
                   // "{" +
                   //    " var ddlStatus=document.getElementById('"+ddltrip.ClientID+"');" +
                   //    " if(ddlStatus.options.selectedIndex>-1)" +
                   //   "  {  " +
                   //     "    if(ddlStatus.options[ddlStatus.options.selectedIndex].text=='Round')" +
                   //      "   {" +
                   //          "   document.getElementById('"+txtcontainer1.ClientID+"').style.display='inline';  " +
                   //           "   document.getElementById('" + ddlTripType.ClientID + "').style.display='none';" +
                   //          "   document.getElementById('" + txtchasis.ClientID + "').style.display='inline';      " +
                   //         "}     " +
                   //        " else" +
                   //        " {" +
                   //          "   document.getElementById('" + txtcontainer1.ClientID + "').style.display='none';" +
                   //           "   document.getElementById('" + ddlTripType.ClientID + "').style.display='inline';" +
                   //           "  document.getElementById('" + txtchasis.ClientID + "').style.display='none';" +
                   //        " }" +
                   //    " }" +
                   //" }</script>";
                   // Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "onload", Script);
                }
                catch
                {
                }
            }
            else
            {
                if (ViewState["LoadID"] == null)
                {
                    ViewState["Mode"] = "New";
                    Grid1.Visible = false;
                    Grid2.Visible = false;
                    lnkBarOrigin.EnableLinksList = "false";
                    lnkBarDest.EnableLinksList = "false";
                    lnkBarUpload.EnableLinksList = "false;false";
                    Session["TopHeaderText"] = "New Load";                    
                }
                else
                {
                    try
                    {
                        Session["TopHeaderText"] = "Edit Load";
                        ViewState["Mode"] = "Edit";
                        FillGrids(Convert.ToInt64(ViewState["LoadID"]));
                        FillEditDetails(Convert.ToInt64(ViewState["LoadID"]));
                        Session["BackPage4"] = "NewLoad.aspx?" + Convert.ToString(ViewState["LoadID"]);
                    }
                    catch
                    {
                    }
                }
            }            
            //ddlTripType.Visible = false;
            //lblTripType.Visible = false;           
        }
    }
   
    protected void Page_PreRender(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
        //    if (ddltrip.Items.Count > 0)
        //    {
        //        ddltrip.Attributes.Add("onchange", "javascript:ValidateRoundTrip('" + ddltrip.ClientID + "','" + txtcontainer1.ClientID + "','" + txtchasis.ClientID + "','" + ddlTripType.ClientID + "')");
        //    }
        //}
    }

    private void FillEditDetails(long ID)
    {
        try
        {
            //string strQuery = "SELECT [bint_CustomerId],[nvar_Description],[nvar_Ref],[nvar_Remarks],[date_LastFreeDate],[bint_LoadTypeId],[bint_CommodityTypeId]," +
            //                "[nvar_Booking],[bint_ShippingId],[bint_TripTypeId],[nvar_Container],[nvar_Container1],[nvar_Chasis],[nvar_Chasis1],[nvar_Pickup],[nvar_Seal],[nvar_RailBill]," +
            //                "[date_OrderBillDate],[nvar_PersonalBill],[date_DateOfCreation],[bint_UserLoginId],[nvar_Terms],[bint_LoadStatusId]" +
            //                "FROM [eTn_Load] where [bint_LoadId]=" + ID;
            //DataTable dt = DBClass.returnDataTable(strQuery);

            // added collumn for hazmat charges
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetEditLoadDetails", new object[] { ID });
            if (ds.Tables.Count > 0)
            {
                ddlcustomer.SelectedIndex = ddlcustomer.Items.IndexOf(ddlcustomer.Items.FindByValue(Convert.ToString(ds.Tables[0].Rows[0][0])));
                txtdescription.Text = Convert.ToString(ds.Tables[0].Rows[0][1]);
                txtref.Text = Convert.ToString(ds.Tables[0].Rows[0][2]);
                txtremarks.Text = Convert.ToString(ds.Tables[0].Rows[0][3]);
                txtlastfree.Date = Convert.ToString(ds.Tables[0].Rows[0][4]);
                ddlloadtype.SelectedIndex = ddlloadtype.Items.IndexOf(ddlloadtype.Items.FindByValue(Convert.ToString(ds.Tables[0].Rows[0][5])));
                ddlcommodity.SelectedIndex = ddlcommodity.Items.IndexOf(ddlcommodity.Items.FindByValue(Convert.ToString(ds.Tables[0].Rows[0][6])));
                txtbooking.Text = Convert.ToString(ds.Tables[0].Rows[0][7]);
                ddlshipping.SelectedIndex = ddlshipping.Items.IndexOf(ddlshipping.Items.FindByValue(Convert.ToString(ds.Tables[0].Rows[0][8])));
                ddltrip.SelectedIndex = ddltrip.Items.IndexOf(ddltrip.Items.FindByValue(Convert.ToString(ds.Tables[0].Rows[0][9])));


                if (ddltrip.SelectedItem.Text.ToUpper() == "SINGLE")
                {
                    ddlTripType.Visible = true; 
                }
                
                //SLine 2016 Enhancements
                txtcontainer.Text = Convert.ToString(ds.Tables[0].Rows[0][10]);
                if (Convert.ToString(ds.Tables[0].Rows[0][10]) != "")
                {
                    txtcontainer1.Visible = true;
                    txtcontainer1.Text = Convert.ToString(ds.Tables[0].Rows[0][11]);
                }
                else
                {
                    txtcontainer1.Visible = false;
                }

                txtempty.Text = Convert.ToString(ds.Tables[0].Rows[0][12]);
                if (Convert.ToString(ds.Tables[0].Rows[0][11]) != "")
                {
                    txtchasis.Visible = true;
                    txtchasis.Text = Convert.ToString(ds.Tables[0].Rows[0][13]);
                }
                else
                {
                    txtchasis.Visible = false;
                }
                //SLine 2016 Enhancements                
                
                txtpickup.Text = Convert.ToString(ds.Tables[0].Rows[0][14]);
                txtseal.Text = Convert.ToString(ds.Tables[0].Rows[0][15]);
                txtRailBill.Text = Convert.ToString(ds.Tables[0].Rows[0][16]);
                txtOrderBillDate.Date = Convert.ToString(ds.Tables[0].Rows[0][17]);
                txtPersonBill.Text = Convert.ToString(ds.Tables[0].Rows[0][18]);
                txtAccessorialChargesEmailAddress.Text = Convert.ToString(ds.Tables[0].Rows[0][23]);
                txtPODEmailAddress.Text = Convert.ToString(ds.Tables[0].Rows[0][24]);
                txtDeliveryEmailAddress.Text = Convert.ToString(ds.Tables[0].Rows[0][25]);
                // added collumn for hazmat charges
                bool isHazmat = Convert.ToBoolean(ds.Tables[0].Rows[0][26]);
                if (isHazmat)
                {
                    chkIsHazmatLoad.Checked = true;
                }
                else
                {
                    chkIsHazmatLoad.Checked = false;
                }
                bool chassisRent = Convert.ToBoolean(ds.Tables[0].Rows[0][27]);
                if (chassisRent)
                {
                    chkChassisRent.Checked = true;
                }
                else
                {
                    chkChassisRent.Checked = false;
                }

                //SLine 2016 Enhancements 22 april
                if (Convert.ToBoolean(ds.Tables[0].Rows[0][28]))
                { 
                    chkRailContainer.Checked = true; 
                }
                else 
                { 
                    chkRailContainer.Checked = false; 
                }
                //SLine 2021 Enhancements 22 auguest
                if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsExportLoad"]))
                {
                    chkIsExport.Checked = true;
                }
                else
                {
                    chkIsExport.Checked = false;
                }

                txtBillingEMail.Text = Convert.ToString(ds.Tables[0].Rows[0]["nvar_BillingEmail"]);
                ddlTripType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["nvar_TripType"]);

                ddlContainerType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["nvar_ContainerType"]);
                //SLine 2016 Enhancements 22 april
            }
            if (ds != null) { ds.Dispose(); ds = null; }
        }
        catch
        {
        }
    }
    private void FillDetails()
    {
        if (Request.QueryString.Count > 0)
            ViewState["Mode"] = "Edit";
        else
            ViewState["Mode"] = "Add";
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCommonLoadDetails", new object[] { ViewState["Mode"], officeLocationId });
        //SLine 2017 Enhancement for Office Location Ends
         if (ds.Tables.Count > 0)
         {
             //DataTable dt = DBClass.returnDataTable("select [bint_CustomerId],[nvar_CustomerName] FROM [eTn_Customer] order by [nvar_CustomerName]");

             ddlcustomer.DataSource = ds.Tables[0];
             ddlcustomer.DataTextField = "nvar_CustomerName";
             ddlcustomer.DataValueField = "bint_CustomerId";
             ddlcustomer.DataBind();
             ddlcustomer.Items.Insert(0,"Select Customer");

             //dt = DBClass.returnDataTable("SELECT [bint_LoadTypeId],[nvar_LoadTypeDesc] FROM [eTn_LoadType] order by [nvar_LoadTypeDesc]");
             ddlloadtype.DataSource = ds.Tables[1];
             ddlloadtype.DataTextField = "nvar_LoadTypeDesc";
             ddlloadtype.DataValueField = "bint_LoadTypeId";
             ddlloadtype.DataBind();
             ddlloadtype.Items.Insert(0,"Select");

             //dt = DBClass.returnDataTable("SELECT [bint_CommodityTypeId],[nvar_CommodityTypeDesc] FROM [eTn_CommodityType] order by [nvar_CommodityTypeDesc]");
             ddlcommodity.DataSource = ds.Tables[2];
             ddlcommodity.DataTextField = "nvar_CommodityTypeDesc";
             ddlcommodity.DataValueField = "bint_CommodityTypeId";
             ddlcommodity.DataBind();
             ddlcommodity.Items.Insert(0, "Select");

             //dt = DBClass.returnDataTable("SELECT [bint_TripTypeId],[nvar_TripTypeDesc] FROM [eTn_TripType] order by [nvar_TripTypeDesc]");
             ddltrip.DataSource = ds.Tables[3];
             ddltrip.DataTextField = "nvar_TripTypeDesc";
             ddltrip.DataValueField = "bint_TripTypeId";
             ddltrip.DataBind();
             ddltrip.Items.Insert(0, "Select");
             ddltrip.SelectedIndex = 0;

             //dt = DBClass.returnDataTable("SELECT [bint_ShippingId],[nvar_ShippingLineName] FROM [eTn_Shipping] order by [nvar_ShippingLineName]");
             ddlshipping.DataSource = ds.Tables[4];
             ddlshipping.DataTextField = "nvar_ShippingLineName";
             ddlshipping.DataValueField = "bint_ShippingId";
             ddlshipping.DataBind();
             ddlshipping.Items.Insert(0, "Select");

             //Sline 2016 ---- Filling the Container Type Dropdown
             DataTable dt = DBClass.returnDataTable("SELECT nvar_ContainerType FROM eTn_ContainerType");                          
             ddlContainerType.DataSource = dt;
             ddlContainerType.DataTextField = "nvar_ContainerType";
             ddlContainerType.DataValueField = "nvar_ContainerType";
             ddlContainerType.DataBind();
             ListItem lt = new ListItem();
             lt.Text = "Select";
             lt.Value = "";
             ddlContainerType.Items.Insert(0, lt);
         }
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    private void FillGrids(long ID)
    {
        //Origin Grid
        Grid1.Visible = true;
        Grid1.DeleteVisible = true;
        Grid1.EditVisible = true;
        Grid1.FunctionName = "DeleteOrigin";
        Grid1.TableName = "eTn_Origin";
        Grid1.Primarykey = "bint_OriginId";
        Grid1.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        Grid1.ColumnsList = "Convert(varchar(20),eTn_Origin.bint_OriginId)+'^'+Convert(varchar(20),eTn_Origin.bint_LoadId) as 'Id';eTn_Origin.nvar_PO as 'PO';eTn_Origin.bint_Pieces as 'Pieces';eTn_Origin.num_Weight as 'Weight';eTn_Origin.date_PickupDateTime as 'Date';eTn_Origin.nvar_ApptGivenby as 'ApptGivenBy';" +
            "eTn_Company.nvar_CompanyName + '<br/>' + " + CommonFunctions.AddressQueryStringWithPhone("eTn_Location.nvar_Street;eTn_Location.nvar_Suite;eTn_Location.nvar_City;eTn_Location.nvar_State;eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Name");
        Grid1.VisibleColumnsList = "PO;Pieces;Weight;Date;ApptGivenBy;Name";
        Grid1.VisibleHeadersList = "P.O.#;Pieces;Weight;Pickup Date /Time;Appt. Given by ;Company";
        Grid1.InnerJoinClause = " inner join eTn_Location on eTn_Origin.bint_LocationId = eTn_Location.bint_LocationId " +
               " inner join eTn_Company on eTn_Location.bint_CompanyId = eTn_Company.bint_CompanyId ";
        Grid1.DeleteTablesList = "eTn_Origin";
        Grid1.DeleteTablePKList = "bint_OriginId";
        Grid1.WhereClause = "[bint_LoadId]=" + ID;
        Grid1.EditPage = "AddNewLoadOrigin.aspx";
        Grid1.IsPagerVisible = false;
        Grid1.BindGrid(0);
        

        //Destination Grid
        Grid2.Visible = true;
        Grid2.DeleteVisible = true;
        Grid2.EditVisible = true;
        Grid2.FunctionName = "DeleteDestination";
        Grid2.TableName = "eTn_Destination";
        Grid2.Primarykey = "bint_DestinationId";
        Grid2.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        Grid2.ColumnsList = "Convert(varchar(20),eTn_Destination.bint_DestinationId)+'^'+Convert(varchar(20),eTn_Destination.bint_LoadId) as 'Id';eTn_Destination.nvar_PO as 'PO';eTn_Destination.bint_Pieces as 'Pieces';eTn_Destination.num_Weight as 'Weight';eTn_Destination.date_DeliveryAppointmentDate as 'Date';eTn_Destination.nvar_ApptGivenby as 'ApptGivenBy';" +
                        "eTn_Company.nvar_CompanyName + '<br/>' + " + CommonFunctions.AddressQueryStringWithPhone("eTn_Location.nvar_Street;eTn_Location.nvar_Suite;eTn_Location.nvar_City;eTn_Location.nvar_State;eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Name");
        Grid2.VisibleColumnsList = "PO;Pieces;Weight;Date;ApptGivenBy;Name";
        Grid2.VisibleHeadersList = "P.O.#;Pieces;Weight;Delivery Date /Time;Appt. Given by ;Company";
        Grid2.InnerJoinClause = " inner join eTn_Location on eTn_Destination.bint_LocationId = eTn_Location.bint_LocationId " +
               " inner join eTn_Company on eTn_Location.bint_CompanyId = eTn_Company.bint_CompanyId ";
        Grid2.DeleteTablesList = "eTn_Destination";
        Grid2.DeleteTablePKList = "bint_DestinationId";
        Grid2.WhereClause = "[bint_LoadId]=" + ID;
        Grid2.EditPage = "AddNewLoadDestination.aspx";
        Grid2.IsPagerVisible = false;
        Grid2.BindGrid(0);

        lnkBarUpload.HeaderText = "Edit Load ( " + ID.ToString() + " ) ";
        lnkBarUpload.EnableLinksList = "true;true";
        lnkBarUpload.PagesList = "UploadLoadOrder.aspx?" + Convert.ToString(ViewState["LoadID"]) + "^New;ShowReceivables.aspx?" + Convert.ToString(ViewState["LoadID"]) + "^New";
        Session["BackPage4"] = "~/Manager/NewLoad.aspx?" + Convert.ToString(ViewState["LoadID"]);

        lnkBarOrigin.PagesList = "AddNewLoadOrigin.aspx?" + Convert.ToString(ViewState["LoadID"]) + "^New";
        lnkBarOrigin.EnableLinksList = "true";

        lnkBarDest.PagesList = "AddNewLoadDestination.aspx?" + Convert.ToString(ViewState["LoadID"]) + "^New";
        lnkBarDest.EnableLinksList = "true";
    }

    protected void ddlcustomer_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlcustomer.SelectedIndex  == 0)
        {
            txtAccessorialChargesEmailAddress.Text = string.Empty;
            txtPODEmailAddress.Text = string.Empty;
            txtDeliveryEmailAddress.Text = string.Empty;
        }
        else
        {
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCustomerEmailsByCustomerId", new object[] { Int64.Parse(ddlcustomer.SelectedValue) });
            if (ds.Tables.Count > 0)
            {
                txtAccessorialChargesEmailAddress.Text = Convert.ToString(ds.Tables[0].Rows[0][1]);
                txtPODEmailAddress.Text = Convert.ToString(ds.Tables[0].Rows[0][2]);
                txtDeliveryEmailAddress.Text = Convert.ToString(ds.Tables[0].Rows[0][3]);

                //SLine 2016 ---- Adding Billing EMail to the Customer
                if (ds.Tables[0].Rows[0]["BillingEmail"].ToString() != "0")
                {
                    txtBillingEMail.Text = ds.Tables[0].Rows[0]["BillingEmail"].ToString();
                }
                else
                {
                    txtBillingEMail.Text = string.Empty;
                }

            }
            else
            {
                txtAccessorialChargesEmailAddress.Text = string.Empty;
                txtPODEmailAddress.Text = string.Empty;
                txtDeliveryEmailAddress.Text = string.Empty;
                txtBillingEMail.Text = string.Empty;
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        int iRes = 0;
        try
        {
            if (ViewState["Mode"] != null)
            {
                if (string.Compare(ddlloadtype.SelectedItem.Text.Trim(), "container", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    if (txtcontainer.Text.Trim().Length == 0 && txtcontainer1.Text.Trim().Length == 0)
                    {
                        Response.Write("<script>alert('Please Enter Container#')</script>");
                        return;
                    }
                }
                if (!txtlastfree.IsValidDate)
                    return;
                if (!txtOrderBillDate.IsValidDate)
                    return;

                if (txtref.Text.Trim().Length > 0)
                {                   
                    if (txtcontainer.Text.Trim().Length > 0)
                    {
                        if (Convert.ToString(ViewState["LoadID"]).Length == 0)
                            ViewState["LoadID"] = 0;
                        string strQuery = "";
                        string strWhereClause = "";
                        strQuery = "select count([bint_LoadId]) from [eTn_Load]";
                        strWhereClause = " where [bint_LoadId]<>" + Convert.ToString(ViewState["LoadID"]) + " and [nvar_Ref]='" + txtref.Text.Trim() + "' and [nvar_Container]='" + txtcontainer.Text.Trim() + "'";                       
                        if (Convert.ToInt32(DBClass.executeScalar(strQuery + strWhereClause)) > 0)
                        {
                            Response.Write("<script>alert('Reference number with same container# already exists.')</script>");
                            return;
                        }
                    }
                    if (txtcontainer1.Text.Trim().Length > 0)
                    {
                        if (Convert.ToString(ViewState["LoadID"]).Length == 0)
                            ViewState["LoadID"] = 0;
                        string strQuery = "";
                        string strWhereClause = "";
                        strQuery = "select count([bint_LoadId]) from [eTn_Load]";
                        strWhereClause = " where [bint_LoadId]<>" + Convert.ToString(ViewState["LoadID"]) + " and [nvar_Ref]='" + txtref.Text.Trim() + "' and [nvar_Container1]='" + txtcontainer1.Text.Trim() + "'";
                        if (Convert.ToInt32(DBClass.executeScalar(strQuery + strWhereClause)) > 0)
                        {
                            Response.Write("<script>alert('Reference number with same RETURN container# already exists.')</script>");
                            return;
                        }
                    }
                }
                string strTripType ="";
                if (ddltrip.SelectedItem.Text.ToUpper() == "ROUND")
                {
                    strTripType = "ROUND";
                }
                else
                    strTripType = ddlTripType.SelectedValue;

                //SLine 2017 Enhancement for Office Location Starts
                long officeLocationId = 0;
                if (Session["OfficeLocationID"] != null)
                {
                    officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
                }
                //SLine 2017 Enhancement for Office Location Ends
                // IMPORTANT NOTE:
                // Whenever a new control value or anything is added to the Stored procedure or
                //below params List, Parameter values, the Logic is to increase the Value Substracting from IT to get 
                //Correct "LoadID"
                
                // added collumn for hazmat charges
                //SLine 2016 Enhancements. Added chkRailContainer checkbox to the objParams 
                //SLine 2017 Enhancement for Office Location added officeLocation to objParams
                object[] objParams = new object[]{  Convert.ToInt64(ddlcustomer.SelectedValue),txtref.Text.Trim(),txtdescription.Text.Trim(),txtremarks.Text.Trim(),
                                                    CommonFunctions.CheckDateTimeNull(txtlastfree.Date),Convert.ToInt64(ddlloadtype.SelectedValue),
                                                    Convert.ToInt64(ddlcommodity.SelectedValue),txtbooking.Text.Trim(),Convert.ToInt64(ddlshipping.SelectedValue),
                                                    Convert.ToInt64(ddltrip.SelectedValue),txtcontainer.Text.Trim(),txtempty.Text.Trim(),txtpickup.Text.Trim(),
                                                    txtseal.Text.Trim(),txtRailBill.Text.Trim(),CommonFunctions.CheckDateTimeNull(txtOrderBillDate.Date),txtPersonBill.Text.Trim(),
                                                    ((txtcontainer1.Visible)?txtcontainer1.Text.Trim():""),((txtchasis.Visible)?txtchasis.Text.Trim():""),
                                                    txtAccessorialChargesEmailAddress.Text.Trim(),txtPODEmailAddress.Text.Trim(),txtDeliveryEmailAddress.Text.Trim(),
                                                    Convert.ToInt64(ViewState["LoadID"]),Convert.ToString(Session["UserLoginId"]),(chkIsHazmatLoad.Checked ? 1 : 0),
                                                    (chkChassisRent.Checked ? 1 : 0), (chkRailContainer.Checked ? 1 : 0), txtBillingEMail.Text, strTripType, ddlContainerType.SelectedValue, officeLocationId,(chkIsExport.Checked ? 1 : 0)};


                System.Data.SqlClient.SqlParameter[] objsqlparams = null;
                if (ViewState["LoadID"] != null && Convert.ToString(ViewState["LoadID"]) != "0")
                {
                    objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "Update_Load_Details", objParams);

                    if (string.Compare(Convert.ToString(ViewState["Mode"]), "Edit", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                    {
                        if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "Update_Load_Details", objsqlparams) > 0)
                        {
                            iRes = 1;                            
                        }
                    }
                }
                else if (string.Compare(Convert.ToString(ViewState["Mode"]), "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    ViewState["LoadID"] = "0";
                    objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "Add_Load_Details", objParams);

                    if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "Add_Load_Details", objsqlparams) > 0)
                    {
                        //SLine Enhancements 2016
                        //Getting the Load Id from the Parameter values, the Logic is to increase the Value Substracting from the 
                        //Params Length
                        ViewState["LoadID"] = Convert.ToInt64(objsqlparams[objsqlparams.Length - 10].Value);
                        string[] Values = Convert.ToString(DBClass.executeScalar("select Case when Len([eTn_Load].[nvar_Container])>0 and Len([eTn_Load].[nvar_Container1])>0 then [eTn_Load].[nvar_Container] when Len([eTn_Load].[nvar_Container])>0 and Len([eTn_Load].[nvar_Container1])=0 then [eTn_Load].[nvar_Container] when Len([eTn_Load].[nvar_Container1])>0 then [eTn_Load].[nvar_Container1] else '' end+'^'+Case when Len([eTn_Load].[nvar_Chasis])>0 and Len([eTn_Load].[nvar_Chasis1])>0 then [eTn_Load].[nvar_Chasis] when Len([eTn_Load].[nvar_Chasis])>0 and Len([eTn_Load].[nvar_Chasis1])=0 then [eTn_Load].[nvar_Chasis] when Len([eTn_Load].[nvar_Chasis1])>0 then [eTn_Load].[nvar_Chasis1] else '' end+'^'+(isnull((select top 1 case when Len(nvar_Email)>0 then nvar_Email else '' end from eTn_ShippingContacts where bint_ShippingId=[eTn_Load].bint_ShippingId),(isnull((select top 1 case when Len(nvar_Email)>0 then nvar_Email else '' end from eTn_Shipping where bint_ShippingId=[eTn_Load].bint_ShippingId),'')))) from [eTn_Load] where [eTn_Load].[bint_LoadId]=" + Convert.ToInt64(ViewState["LoadID"]))).Trim().Split('^');
                        if (Values != null)
                        {
                            //CommonFunctions.SendEmail(GetFromXML.AdminEmail, (Values.Length == 3 ? Values[2] : ""), GetFromXML.AdminEmail, "", "Pre Authorization for empty UNIT # " + Values[0] + " ( OffHire ) ", GetPickedUpBodyDetails(Values[0], Values[1]), new string[] { "<font color=red>This email was not sent to the shipping line,because Contact email address is not available for this shipping line.</font><br/> " }, null);
                            CommonFunctions.SendEmail(GetFromXML.AdminEmail, (Values.Length == 3 ? Values[2] : ""), "", "", "Pre Authorization for empty UNIT # " + Values[0] + " ( OffHire ) ", GetPickedUpBodyDetails(Values[0], Values[1]), new string[] { "<font color=red>This email was not sent to the shipping line,because Contact email address is not available for this shipping line.</font><br/> " }, null);
                        }
                        Values = null;
                    }
                    if (Convert.ToString(ViewState["LoadID"]).Trim().Length > 0 || Convert.ToString(ViewState["LoadID"]) != "0")
                    {
                        Response.Write("<script>alert('Load saved successully.')</script>");
                        ViewState["Mode"] = "Edit";
                        lnkBarUpload.HeaderText = "Edit Load ( " + Convert.ToString(ViewState["LoadID"]) + " ) ";
                        lnkBarUpload.EnableLinksList = "true;true";
                        lnkBarUpload.PagesList = "UploadLoadOrder.aspx?" + Convert.ToString(ViewState["LoadID"]) + "^New;ShowReceivables.aspx?" + Convert.ToString(ViewState["LoadID"]) + "^New";

                        lnkBarOrigin.PagesList = "AddNewLoadOrigin.aspx?" + Convert.ToString(ViewState["LoadID"]) + "^New";
                        lnkBarOrigin.EnableLinksList = "true";

                        lnkBarDest.PagesList = "AddNewLoadDestination.aspx?" + Convert.ToString(ViewState["LoadID"]) + "^New";
                        lnkBarDest.EnableLinksList = "true";                        
                    }
                }
                objsqlparams = null;
                objParams = null;
            }
        }
        catch (Exception ex)
        { }
        finally
        {
            if (iRes == 1)
            {
                CheckBackPage();
                Response.Redirect("~/Manager/DashBoard.aspx");
            }
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        CheckBackPage();
        Response.Redirect("~/Manager/DashBoard.aspx");
    }
    protected void ddltrip_SelectedIndexChanged(object sender, EventArgs e)
    {
        TripVisibilityChange();
    }
    private void TripVisibilityChange()
    {
        if (ddltrip.Items.Count > 0)
        {
            if (string.Compare(ddltrip.SelectedItem.Text.Trim(), "Single", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                txtcontainer1.Visible = false;
                txtchasis.Visible = false;
                ddlTripType.Visible = true;
                lblTripType.Visible = true;
                ddlTripType.SelectedValue = "0";
            }
            else if (string.Compare(ddltrip.SelectedItem.Text.Trim(), "Round", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                txtcontainer1.Visible = true;
                txtchasis.Visible = true;
                lblTripType.Visible = false;
                ddlTripType.Visible = false;
                ddlTripType.SelectedValue = "0";
            }
            else if (string.Compare(ddltrip.SelectedItem.Text.Trim(), "Select", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                txtcontainer1.Visible = false;
                txtchasis.Visible = false;
                lblTripType.Visible = false;
                ddlTripType.Visible = false;
                ddlTripType.SelectedValue = "0";
            }
        }     
                  
    }

    protected void lblCustomer_Click(object sender, EventArgs e)
    {
        if (string.Compare(Convert.ToString(ViewState["Mode"]), "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            Session["BackPage4"] = "~/Manager/NewLoad.aspx";
        }
        else if(string.Compare(Convert.ToString(ViewState["Mode"]), "Edit", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        {
            Session["BackPage4"] = "~/Manager/NewLoad.aspx?" + Convert.ToString(ViewState["LoadID"]);
        }
        Response.Redirect("~/Manager/NewCustomer.aspx");
    }
    private void CheckBackPage()
    {
        if (Session["BackPage3"] != null)
        {
            if (Convert.ToString(Session["BackPage3"]).Trim().Length > 0)
            {
                string strTemp = Convert.ToString(Session["BackPage3"]).Trim();
                Session["BackPage3"] = null;
                Response.Redirect(strTemp);
            }
        }
    }
    private string GetPickedUpBodyDetails(string Container, string Chasis)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();
        strBody.Append("Date :" + DateTime.Today.ToShortDateString() + "<br/><p></p>");
        strBody.Append("Dear Sir/Madam, <br/><p></p>");
        strBody.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Regarding ");
        if (Container.Length > 0)
            strBody.Append("&nbsp;Container# " + Container + ",");
        if (Chasis.Length > 0 && Chasis.Length > 0)
            strBody.Append("&nbsp;&nbsp;  CHASSIS #&nbsp; " + Chasis + ",");
        strBody.Append("&nbsp;Please  put this container in Terminal system for offhire. Container is arriving Oakland California soon. Also please advise us at which port we have to terminate this container when empty.");
        strBody.Append("<p>Please reply   this  e mail  asap. </p>");
        strBody.Append("</br>Container # " + Container + " </br>Chassis # " + Chasis + "</br></br><p>Your help is greatly appreciated.</p>");
        strBody.Append("<p></p>Regards<br/>" + GetFromXML.CompanyName + "<br/>Dispatch Dept.<br/>" + GetFromXML.CompanyPhone + "<br/>" + GetFromXML.CompanyFax + "<br/>E Mail:<a href='mailto:" + GetFromXML.AdminEmail + "'> " + GetFromXML.AdminEmail + "</a>");
        return strBody.ToString();
    }
}
