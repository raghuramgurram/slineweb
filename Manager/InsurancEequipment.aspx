<%@ Page AutoEventWireup="true" CodeFile="InsurancEequipment.aspx.cs" Inherits="insuranceequipment"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc3" %>

<%@ Register Src="~/UserControls/Phone.ascx" TagName="Phone" TagPrefix="uc2" %>

<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td>
     <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblform">
        <tr>
          <td>
              <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="307px" />
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td>
              <uc1:GridBar ID="GridBar1" runat="server" />
          </td>
          </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../Images/pix.gif" alt="" width="1" height="3" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="3" cellspacing="0" id="tblform">
        <tr>
          <th></th>
          <th>Liability Insurance </th>
          <th>Cargo Insurance </th>
          <th>Physical Insurance </th>
          </tr>
        <tr>
          <td style="height: 26px"><strong>Company</strong></td>
          <td style="height: 26px">
              <asp:TextBox ID="txtcliability" runat="server" MaxLength="150"></asp:TextBox></td>
          <td style="height: 26px">
              <asp:TextBox ID="txtccargo" runat="server" MaxLength="150"></asp:TextBox></td>
          <td style="height: 26px">
              <asp:TextBox ID="txtcphysical" runat="server" MaxLength="150"></asp:TextBox></td>
          </tr>
        <tr>
          <td><strong>Policy#</strong></td>
          <td>
              <asp:TextBox ID="txtpliability" runat="server" MaxLength="100"></asp:TextBox></td>
          <td>
              <asp:TextBox ID="txtpcargo" runat="server" MaxLength="100"></asp:TextBox></td>
          <td>
              <asp:TextBox ID="txtpphysical" runat="server" MaxLength="100"></asp:TextBox></td>
          </tr>
        <tr>
          <td><strong>Limit($)</strong></td>
          <td>
              <asp:TextBox ID="txtlliability" runat="server" MaxLength="18"></asp:TextBox>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtlliability"
                  Display="Dynamic" ErrorMessage="Please enter a valid amount." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
          <td>
              <asp:TextBox ID="txtlcargo" runat="server" MaxLength="18"></asp:TextBox>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtlcargo"
                  Display="Dynamic" ErrorMessage="Please enter a valid amount." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
          <td>
              <asp:TextBox ID="txtlphysical" runat="server" MaxLength="18"></asp:TextBox>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtlphysical"
                  Display="Dynamic" ErrorMessage="Please enter a valid amount." ValidationExpression="^\d*\.?\d*$">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr>
          <td><strong>Broker</strong></td>
          <td>
              <asp:TextBox ID="txtbliability" runat="server" MaxLength="100"></asp:TextBox></td>
          <td>
              <asp:TextBox ID="txtbcargo" runat="server" MaxLength="100"></asp:TextBox></td>
          <td>
              <asp:TextBox ID="txtbphysical" runat="server" MaxLength="100"></asp:TextBox></td>
        </tr>
        <tr>
          <td><strong>Phone</strong></td>
          <td>
              <%--<asp:TextBox ID="txtphliabity" runat="server" MaxLength="18"></asp:TextBox>--%>
              <uc2:Phone ID="txtphliabity" runat="server" />
          </td>
          <td>
              <%--<asp:TextBox ID="txtphcargo" runat="server" MaxLength="18"></asp:TextBox>--%>
              <uc2:Phone ID="txtphcargo" runat="server" />
          </td>
          <td>
              <%--<asp:TextBox ID="txtphphysical" runat="server" MaxLength="18"></asp:TextBox>--%>
              <uc2:Phone ID="txtphphysical" runat="server" />
          </td>
        </tr>
        <tr>
          <td><strong>Fax</strong></td>
          <td>
              <%--<asp:TextBox ID="txtfliability" runat="server" MaxLength="18"></asp:TextBox>--%>
              <uc2:Phone ID="txtfliability" runat="server" />
          </td>
          <td>
              <%--<asp:TextBox ID="txtfcargo" runat="server" MaxLength="18"></asp:TextBox>--%>
              <uc2:Phone ID="txtfcargo" runat="server" />
          </td>
          <td>
              <%--<asp:TextBox ID="txtfphysical" runat="server" MaxLength="18"></asp:TextBox>--%>
              <uc2:Phone ID="txtfphysical" runat="server" />
          </td>
        </tr>
        <tr>
          <td><strong>Email</strong></td>
          <td>
              <asp:TextBox ID="txtemailliabi" runat="server" MaxLength="100"></asp:TextBox>
              <asp:RegularExpressionValidator ID="reqemailliabi" runat="server" ControlToValidate="txtemailliabi"
                  ErrorMessage="Please enter valid emial id" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
          <td>
              <asp:TextBox ID="txtemailcargo" runat="server" MaxLength="100"></asp:TextBox>
              <asp:RegularExpressionValidator ID="reqemailcargo" runat="server" ControlToValidate="txtemailcargo"
                  ErrorMessage="Please enter valid emial id" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
          <td>
              <asp:TextBox ID="txtemailphy" runat="server" MaxLength="100"></asp:TextBox>
              <asp:RegularExpressionValidator ID="reqemailphy" runat="server" ControlToValidate="txtemailphy"
                  ErrorMessage="Please enter valid emial id" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr>
          <td style="height: 26px"><strong>Expiry Date </strong></td>
          <td style="height: 26px">
              <uc3:DatePicker ID="txteliaility" runat="server" IsDefault="true" IsRequired="false" CountNextYears="2" CountPreviousYears="0" />
          </td>
          <td style="height: 26px">
              <uc3:DatePicker ID="txtecargo" runat="server" IsDefault="true" IsRequired="false" CountNextYears="2" CountPreviousYears="0" />
          </td>
          <td style="height: 26px">
              <uc3:DatePicker ID="txtephysical" runat="server" IsDefault="true" IsRequired="false" CountNextYears="2" CountPreviousYears="0" />
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar" language="javascript" onclick="return tblBottomBar_onclick()">
        <tr>
          <td align="right">
              <asp:Button ID="btnsave" runat="server" Height="22px" Text="Save" Width="52px" CssClass="btnstyle" OnClick="btnsave_Click"/>&nbsp;
              <asp:Button ID="btncancel" runat="server" Text="Cancel" Height="22px" CssClass="btnstyle" OnClick="btncancel_Click"/></td>
        </tr>
      </table>
   
    </td>
  </tr>
</table>
</asp:Content>

