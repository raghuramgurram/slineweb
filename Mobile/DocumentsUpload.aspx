<%@ Page Language="C#" MasterPageFile="~/Mobile/eTransportMobile.master" AutoEventWireup="true" CodeFile="DocumentsUpload.aspx.cs" Inherits="Mobile_DocumentsUpload" Title="Untitled Page" %>
<%@ Register Src="MobileMenu.ascx" TagName="Menu"
    TagPrefix="mu" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <style type="text/css">
    .OverLay {position: absolute; z-index: 3;  top: 0; bottom: 0; left: 0; right: 0; width: 100%; height: 100%; background: rgba(0,0,0,.5);}
    .OverLay .GifLoader{ background:url(images/ajax-loader.gif) no-repeat center; width: 100%; height: 100%; opacity: 0.4;}
    .OverLay .CloseBtnTop{ position:absolute; right:0; top:18%;}
    </style>        
<script type="text/javascript">
var myVar;    
    function Browseclick() {
        myVar = true;
         document.getElementById('linkcloseoverlay').style.display = '';   
        document.getElementById('<%= lnkCapture.ClientID %>').Disabled = true;
        document.getElementById('<%= lnkaddImagetoTemp.ClientID %>').Disabled = true;
        document.getElementById('<%= lnkCreatePDFWithImags.ClientID %>').Disabled = true;
        document.getElementById('<%= lnkgoback.ClientID %>').Disabled = true;
        document.getElementById('<%= Divoverlay.ClientID %>').style.display = '';
        document.getElementById('<%= ImageFileUpload.ClientID %>').click();               
        return false;
    }
    
        
    
    function Fileuploaded() {
       // document.getElementById('<%= Divoverlay.ClientID %>').style.display = 'none';
        myVar = false;
        document.getElementById('linkcloseoverlay').style.display = 'none';        
        document.getElementById('<%= lnkaddImagetoTemp.ClientID %>').click();
    }
    function CloseOverLay()
    {   
        if(myVar == true)
        {
            document.getElementById('<%= Divoverlay.ClientID %>').style.display = 'none';
        }
    }
   
</script>
<mu:Menu ID="MuPageMenu" runat="server" />

<div id="Divoverlay" runat="server"  class="OverLay" style="display:none;">
<div class="GifLoader"></div>
<div class="CloseBtnTop">
<a data-role="button" data-icon="delete" data-theme="a" data-iconpos="notext" onclick="javascript:CloseOverLay();" id="linkcloseoverlay" >Delete</a> 
</div>
</div>
        <h2 id="spanTitle" runat="server">
           Bill Of Landing
        </h2>
        <asp:LinkButton ID="lnkCapture" runat="server" data-role="button" data-theme="b"  OnClientClick="javascript:return Browseclick()">Capture</asp:LinkButton>
 <asp:LinkButton ID="lnkaddImagetoTemp" OnClick="lnkaddImagetoTemp_click"  style="display:none;" runat="server" data-role="button" data-inline="true" data-theme="b">Add</asp:LinkButton>        

 <asp:Repeater ID="rptFileNames" runat="server">
      <HeaderTemplate>
      </HeaderTemplate>
      <ItemTemplate>
     <h5> <asp:Literal ID="ltlFileName" Text='<%# Eval("FileName").ToString()%>' runat="server"></asp:Literal> <asp:LinkButton ID="lnkDeletImage" runat="server" CommandArgument='<%# Eval("fileid").ToString()%>' OnClick="lnkDeletImage_click">X</asp:LinkButton></h5>
      </ItemTemplate>
      <FooterTemplate>
      </FooterTemplate>
      </asp:Repeater>
        <input type="file" runat="server" id="ImageFileUpload" onchange="javascript:Fileuploaded();" accept="image/*" capture="camera" style="display:none;"  />
  <asp:LinkButton ID="lnkCreatePDFWithImags" runat="server" OnClick="lnkCreatePDFWithImags_click" data-role="button" data-theme="b"> Upload </asp:LinkButton>
        
    
   <asp:LinkButton ID="lnkgoback" runat="server" data-role="button" data-inline="true" data-theme="b" 
        data-icon="arrow-l" data-iconpos="left" class="backbtn fl-right" rel="external" OnClick="lnkgoback_click"> Back </asp:LinkButton>
        
   
</asp:Content>

