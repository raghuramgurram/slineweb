<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Grid.ascx.cs" Inherits="Grid"%>
<link type="text/css" href="../Styles/OwnStyle.css" rel="stylesheet" />

<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
    <td>
<%--<div id="div" style="left:15px;right:15px;border:1;z-index:101;height:auto;" runat="server">--%>
    <asp:DataGrid ID="dggrid" Width="100%" runat="server" OnItemCommand="dggrid_ItemCommand" OnPageIndexChanged="dggrid_PageIndexChanged"  CssClass="Grid" OnItemDataBound="DataGrid1_ItemDataBound">          
        <PagerStyle Mode="NumericPages" />
        <ItemStyle CssClass="GridItem" BorderColor="White" />
        <HeaderStyle CssClass="GridHeader" BorderColor="White" />
        <AlternatingItemStyle CssClass="GridAltItem" />
    </asp:DataGrid>
<%--</div>--%>
</td>
</tr>    
</table>




