using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class NewCustomer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblNameError.Visible = false;
        //Session["TopHeaderText"] = "New / Edit Customer";
        Session["ContactType"] = "Customer";
        if (!Page.IsPostBack)
        {
            if (Request.QueryString.Count > 0)
            {
                ViewState["ID"] = Request.QueryString[0].Trim();
                try
                {
                    ViewState["Mode"] = "Edit";
                    FillDetails(Convert.ToInt64(ViewState["ID"]));
                    Session["TopHeaderText"] = "Edit Customer";
                }
                catch
                {
                }
            }
            else
            {
                if (ViewState["ID"] == null)
                {
                    ViewState["Mode"] = "New";
                    Grid1.Visible = false;
                    Session["TopHeaderText"] = "New Customer";
                    lnkBar.EnableLinksList = "false";  
                }
                else
                {
                    try
                    {
                        ViewState["Mode"] = "Edit";
                        FillDetails(Convert.ToInt64(ViewState["ID"]));
                        Session["TopHeaderText"] = "Edit Customer";
                    }
                    catch
                    {
                    }
                }
            }
        }
    }
    private void FillDetails(long ID)
    {
        //string strQuery = "SELECT [nvar_CustomerName],[bit_Status],[bit_CreditLimitExceed],[nvar_Street],[nvar_Suite]," +
        //"[nvar_City],[nvar_State],[nvar_Zip],[num_Phone],[num_Fax],[nvar_WebsiteURL],[nvar_Directions] " +
        //" FROM [eTn_Customer] where [bint_CustomerId] = " + ID;
        //DataTable dt = DBClass.returnDataTable(strQuery);
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCustomerDetails", new object[] { ID });
        if (ds.Tables.Count > 0)
        {
            ViewState["Name"] = Convert.ToString(ds.Tables[0].Rows[0][0]).Trim();
            txtCustomer.Text = Convert.ToString(ds.Tables[0].Rows[0][0]).Trim();
            chkActive.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0][1]);
            chkCreditLimit.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0][2]);
            txtStreet.Text = Convert.ToString(ds.Tables[0].Rows[0][3]).Trim();
            txtSuite.Text = Convert.ToString(ds.Tables[0].Rows[0][4]).Trim();
            txtCity.Text = Convert.ToString(ds.Tables[0].Rows[0][5]).Trim();
            ddlState.Text = Convert.ToString(ds.Tables[0].Rows[0][6]).Trim();
            txtZip.Text = Convert.ToString(ds.Tables[0].Rows[0][7]).Trim();
            txtPhone.Text = Convert.ToString(ds.Tables[0].Rows[0][8]).Trim();
            txtFax.Text = Convert.ToString(ds.Tables[0].Rows[0][9]).Trim();
            txtWebSiteUrl.Text = Convert.ToString(ds.Tables[0].Rows[0][10]).Trim();
            txtDirection.Text = Convert.ToString(ds.Tables[0].Rows[0][11]).Trim();
            txtAccessorialChargesEmailAddress.Text = Convert.ToString(ds.Tables[0].Rows[0][12]).Trim();
            txtPODEmailAddress.Text = Convert.ToString(ds.Tables[0].Rows[0][13]).Trim();
            txtDeliveryEmailAddress.Text = Convert.ToString(ds.Tables[0].Rows[0][14]).Trim();
            chkSendMails.Checked = bool.Parse(ds.Tables[0].Rows[0][15].ToString());
            Grid1.Visible = true;
            Grid1.DeleteVisible = true;
            Grid1.EditVisible = true;
            Grid1.TableName = "eTn_CustomerContacts";
            Grid1.Primarykey = "bint_ContactId";
            Grid1.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
            Grid1.ColumnsList = "bint_ContactId;nvar_Name;" +  CommonFunctions.PhoneQueryString("num_Mobile", "Mobile") + ";" +
                CommonFunctions.PhoneQueryString("num_WorkPhone", "WorkPhone") + ";" + CommonFunctions.PhoneQueryString("num_Fax", "Fax") + ";nvar_Email";
            Grid1.VisibleColumnsList = "nvar_Name;Mobile;WorkPhone;Fax;nvar_Email";
            Grid1.VisibleHeadersList = "Name;Mobile;Work Phone;Fax;Email";
            Grid1.DeleteTablesList = "eTn_CustomerContacts";
            Grid1.WhereClause = "[bint_CustomerId]=" + ID;
            Grid1.EditPage = "~/Manager/AddContact.aspx";
            Grid1.BindGrid(0);
            lnkBar.PagesList = "AddContact.aspx?" + ID + "New";
            lnkBar.EnableLinksList = "true";

            //SLine 2016 Enhancements Added, Send Invoice via. Email, column name nvar_BillingEmail
            //DBNull.Value.Equals(ds.Tables[0].Rows[0][16])

            if (String.IsNullOrEmpty(ds.Tables[0].Rows[0][16].ToString()))
            {
                chkSendInvoiceViaEmail.Checked = false;
                trBillingEmail.Visible = false;
            }
            else
            {
                chkSendInvoiceViaEmail.Checked = true;
                trBillingEmail.Visible = true;
                txtBillingEmail.Text = ds.Tables[0].Rows[0][16].ToString();
            }
            //SLine 2016 Enhancements Added, Send Invoice via. Email, column name nvar_BillingEmail


        }
        if (ds != null) { ds.Dispose(); ds = null; }
        
    }    
    protected void btnSave_Click(object sender, EventArgs e)
    {
       
        if (Page.IsValid)
        {
            string strQuery = "";
            if (string.Compare(Convert.ToString(ViewState["Name"]).Trim(), txtCustomer.Text.Trim(), true, System.Globalization.CultureInfo.CurrentCulture) != 0)
            {
                if (Convert.ToInt32(DBClass.executeScalar("Select Count(bint_CustomerId) from eTn_Customer Where nvar_CustomerName ='" + txtCustomer.Text.Trim() + "'")) > 0)
                {
                    lblNameError.Visible = true;
                    return;
                }
            }

            if (ViewState["Mode"] != null)
            {
                    object[] objParams = new object[] {txtCustomer.Text.Trim(), chkActive.Checked?1:0,chkCreditLimit.Checked?1:0,txtStreet.Text.Trim(),txtSuite.Text.Trim(), txtCity.Text.Trim(),ddlState.Text,txtZip.Text.Trim(),txtPhone.Text, txtFax.Text,
                                                       txtWebSiteUrl.Text.Trim(),txtDirection.Text.Trim(),txtAccessorialChargesEmailAddress.Text.Trim(),txtPODEmailAddress.Text.Trim(),txtDeliveryEmailAddress.Text.Trim(),chkSendMails.Checked?1:0,Convert.ToInt64(ViewState["ID"]), txtBillingEmail.Text.Trim(), chkSendInvoiceViaEmail.Checked?1:0 };
                    System.Data.SqlClient.SqlParameter[] objsqlparams = null;

                    if (Convert.ToString(ViewState["Mode"]).StartsWith("Edit", true, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Update_Customers", objParams);
                        if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Update_Customers", objsqlparams) > 0)
                        {
                            CheckBackPage();
                            Response.Redirect("~/Manager/SearchCustomer.aspx");
                        }
                    }

                    else if (Convert.ToString(ViewState["Mode"]).StartsWith("New", true, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Add_Customers", objParams);
                        if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Add_Customers", objsqlparams) > 0)
                        {
                            ViewState["ID"] = Convert.ToInt64(objsqlparams[objsqlparams.Length - 1].Value);
                            //Response.Write("<script>alert('Customer saved successfully.')</script>");
                            ViewState["Mode"] = "Edit";
                            lnkBar.EnableLinksList = "true";
                            lnkBar.PagesList = "AddContact.aspx?" + Convert.ToString(ViewState["ID"]) + "New";
                            ViewState["Name"] = txtCustomer.Text.Trim();
                            Response.Write("<script>alert('Customer saved successully.')</script>");
                            //No need for below line    
                            CheckBackPage();
                            //Response.Redirect("~/SearchCustomer.aspx");                        
                        }
                    }
            }
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        CheckBackPage();
        Response.Redirect("SearchCustomer.aspx");
    }
    private void CheckBackPage()
    {
        if (Session["BackPage4"] != null)
        {
            if (Convert.ToString(Session["BackPage4"]).Trim().Length > 0)
            {
                string strTemp = Convert.ToString(Session["BackPage4"]).Trim();
                Session["BackPage4"] = null;
                Response.Redirect(strTemp);
            }
        }
        else if (Session["BackPage2"] != null)
        {
            if (Convert.ToString(Session["BackPage2"]).Trim().Length > 0)
            {
                string strTemp = Convert.ToString(Session["BackPage2"]).Trim();
                Session["BackPage2"] = null;
                Response.Redirect(strTemp);
            }
        }        
    }

    //SLine 2016 Enhancements Added Automatic Invoice/Billing Address
    protected void chkSendInvoiceViaEmail_CheckedChanged(object sender, EventArgs e)
    {
        if (Convert.ToBoolean(chkSendInvoiceViaEmail.Checked))
        {
            trBillingEmail.Visible = true;
        }

        else
        {
            trBillingEmail.Visible = false;
            txtBillingEmail.Text = "";
        }
    }

    //SLine 2016 Enhancements Added Automatic Invoice/Billing Address

}