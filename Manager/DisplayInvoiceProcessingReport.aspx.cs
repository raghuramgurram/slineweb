﻿
/* SLine 2016 Updates ------------ Added New page for showing new report for Invoice Processing */
/* Mohammed Akhtar */




using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;

public partial class Manager_DisplayInvoiceProcessingReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Text = string.Empty;
        if (!IsPostBack)
        {
            if (Session["ReportName"].ToString().Equals("SP_GetReportInvoiceProcessing"))
            {
                ReportDisplay();
            }

            else
            {
                Response.Redirect("Login.aspx", false);
            }
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            ExportToExcel();
        }
        catch
        { }
    }

    protected void btnMakeInvoiceable_Click(object sender, EventArgs e)
    {
        try
        {
            int intchkstatus = 0;
            bool returnvalue = false;
            int intCounter = 0;
            foreach (DataGridItem item in dggridInvoiceProcessing.Items)
            {
                if (intCounter != dggridInvoiceProcessing.Items.Count)
                {
                    if (((CheckBox)item.FindControl("chkInvoiceable")).Checked)
                    {
                        returnvalue = (MakeLoadInvoiceable(int.Parse(item.Cells[1].Text)));
                        intchkstatus++;
                    }
                }
            }
            //if the counter goes atleast once, Atleast One chk box is checked else print this
            if (intchkstatus == 0)
            {
                msg.Text = "Please select atleast one checkbox";
            }
            else
            {
                ReportDisplay();
            }          

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void lnkInvoiceable_Click(object sender, EventArgs e)
    {
        if (MakeLoadInvoiceable(int.Parse(((LinkButton)sender).CommandArgument)))
            ReportDisplay();        
    }

    private bool MakeLoadInvoiceable(Int64 longLoadID)
    {
        bool boolResult;
        boolResult = Convert.ToBoolean(SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"],
            "sp_Insert_Update_TrackLoad",
            new object[] 
            { 
                longLoadID, 16, "", System.DateTime.Now.ToString(), "", Convert.ToInt64(Session["UserLoginId"]), "I"
            }));
        if (boolResult)
        {
            ChangeLoadStatus cls = new ChangeLoadStatus();
            cls.UpdateLoadStatusToPostman(longLoadID, "Invoiceable", DateTime.Now);
        }
        return boolResult;
    }   

    private void ReportDisplay()
    {
        DataSet ds = new DataSet();
        //object[] objParams = null;
        string[] objParams = null;
        if (Convert.ToString(Session["SerchCriteria"]).Trim().Length > 0)
        {
            lblSearchCriteria.Visible = true;
            lblSearchCriteria.Text = "<br/><br/>" + Convert.ToString(Session["SerchCriteria"]);
        }
        else
        {
            lblSearchCriteria.Visible = false;
        }
        Session["TopHeaderText"] = "Invoice Processing";
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        string reportParams = Convert.ToString(Session["ReportParameters"]) + "^" + officeLocationId.ToString();
        objParams = reportParams.Trim().Replace("~~^^^^~~", "^").Split('^');
        //objParams = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
        //SLine 2017 Enhancement for Office Location Ends
        ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetReportInvoiceProcessing", objParams);
        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            DataTable dt = ds.Tables[0];
            FormatDataTable(ref dt);
            dggridInvoiceProcessing.Visible = true;
            dggridInvoiceProcessing.DataSource = dt;
            dggridInvoiceProcessing.DataBind();

            //Checking for "Not Verified Loads" ---- Disabling and Enabling
            CheckNotVerifiedLoads();

            if (dt != null) { dt.Dispose(); dt = null; }
        }
        else
        {
            dggridInvoiceProcessing.Visible = false;
            lblDisplay.Text = "<table width=100% border=0 cellpadding=0 cellspacing=0 id=ContentTbl><tr valign=top><td>" +
                "<table border=0 width=100% height=200px border=0 cellpadding=0 cellspacing=0 align=center valign=middle><tr><td width=100% align=center valign=middle>" +
                "<b><font color=blue>No Records To Display.<font></b></td></tr></table><br/><br/><br/></td></tr></table>";
        }
        if (ds != null) { ds.Dispose(); ds = null; }

        Session["PrintDetails"] = lblDisplay.Text.Trim();
    }

    private void CheckNotVerifiedLoads()
    {
        //int intCounter = 1;
        foreach (DataGridItem dgi in dggridInvoiceProcessing.Items)
        {
            CheckBox chkBx = (CheckBox)dgi.FindControl("chkInvoiceable");
            LinkButton lnkBtn = (LinkButton)dgi.FindControl("lnkInvoiceable");           

            //if (intCounter != dggridInvoiceProcessing.Items.Count)
            //{
                //Here the logic will Hide and Show the Controls as per the Condition == Is Load Verified
                if (Convert.ToBoolean(DBClass.executeScalar("Select eTn_Load.bit_Is_Verified from eTn_Load where (bint_LoadId =" + dgi.Cells[1].Text + ")")))
                {
                    chkBx.Visible = true;
                    lnkBtn.Visible = true;
                    //chkBx.Checked = true;
                }
                else
                {
                    //if (Session["SerchCriteria"].ToString() == "Not Verified Loads")
                    //{
                        chkBx.Visible = false;
                        lnkBtn.Visible = false;
                        //chkBx.Checked = false;
                    //}   
                }
            //}
            //else
            //{
            //    chkBx.Visible = false;
            //    lnkBtn.Visible = false;
            //    //chkBx.Checked = false;
            //}

            //The Last ROW having Total value is Explicitly Hidden
            //if (dggridInvoiceProcessing.Items.Count == 1)
            //{
            //    chkBx.Visible = false;
            //    lnkBtn.Visible = false;
            //    //chkBx.Checked = false;
            //}
            //intCounter++;
        }
    }

    protected void FormatDataTable(ref DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    if (Convert.ToString(dt.Rows[i][j]).Contains("^^^^"))
                    {
                        if (Convert.ToString(Session["ReportName"]) == "GetPayableEntries")
                        {
                            if (Convert.ToInt32(dt.Rows[i]["LoadStatus"]) != 12)
                            {
                                if (j == 4)
                                {
                                    dt.Rows[i][j] = FormatMultipleRowColumnsred(Convert.ToString(dt.Rows[i][j]).Trim());
                                }
                                else
                                {
                                    dt.Rows[i][j] = FormatMultipleRowColumns(Convert.ToString(dt.Rows[i][j]).Trim());
                                }
                            }
                            else
                            {
                                dt.Rows[i][j] = FormatMultipleRowColumns(Convert.ToString(dt.Rows[i][j]).Trim());
                            }

                        }
                        else
                        {
                            dt.Rows[i][j] = FormatMultipleRowColumns(Convert.ToString(dt.Rows[i][j]).Trim());
                        }
                    }
                }
            }
        }
    }

    public string FormatMultipleRowColumnsred(string strColValue)
    {
        string strFormat = "";
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0)
        {
            strFormat = "<table cellpadding=0 cellspacing=0 border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;>";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (i == 0)
                    strFormat += "<tr ><td  width=100% align=left valign=middle style=color:black;font-size:12;height:90px;><span style='color:red;'>" + strMuls[i].Trim() + "</span></td></tr>";
                else
                {
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:black;font-size:12;height:90px;><span style='color:red;'>" + strMuls[i].Trim() + "</span></td></tr>";
                }
            }
            strFormat += "</table>";
        }
        strMuls = null;
        return strFormat;
    }

    public string FormatMultipleRowColumns(string strColValue)
    {
        string strFormat = "";
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0)
        {
            strFormat = "<table cellpadding=0 cellspacing=0 border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;>";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (i == 0)
                    strFormat += "<tr ><td  width=100% align=left valign=middle style=color:black;font-size:12;height:90px;>" + strMuls[i].Trim() + "</td></tr>";
                else
                {
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:black;font-size:12;height:90px;>" + strMuls[i].Trim() + "</td></tr>";
                }
            }
            strFormat += "</table>";
        }
        strMuls = null;
        return strFormat;
    }

    private void ExportToExcel()
    {
        string fileName = string.Empty;
        if (Master.FindControl("Top1").FindControl("lblHeadText") != null)
        {
            fileName = ((Label)Master.FindControl("Top1").FindControl("lblHeadText")).Text;
        }
        if (string.IsNullOrEmpty(fileName))
        {
            fileName = "ExcelFile";
        }
        fileName = fileName.Replace(" ", "");
        fileName = fileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
        Response.ContentType = "application/x-msexcel";
        Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + ".xls");
        Response.ContentEncoding = Encoding.UTF8;
        StringWriter tw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(tw);
        tbldispaly.RenderControl(hw);
        Response.Write(tw.ToString());
        Response.End();
    }
}