using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class DriverDashBoard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Driver Dashboard";
        Session["ViewLog"] = true;
        if (!Page.IsPostBack)
        {
            AssignListValues();
            if (dropStatus.Items.Count > 0)
            {
                dropStatus.SelectedIndex = 0;
                Session["LoadGridPageIndex"] = 0;
                GridFill();
            }
        }
    }
    private void AssignListValues()
    {
        //dropStatus.DataSource = DBClass.returnDataTable("Select bint_LoadStatusId,nvar_LoadStatusDesc from eTn_LoadStatus");
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadStatusDetails");
        if (ds.Tables.Count > 0)
        {
            dropStatus.DataSource = ds.Tables[0];
            dropStatus.DataValueField = "bint_LoadStatusId";
            dropStatus.DataTextField = "nvar_LoadStatusDesc";
            dropStatus.DataBind();
        }                
        if (dropStatus.Items.Count > 0)
        {
            dropStatus.Items.Remove(dropStatus.Items.FindByText("New"));
            dropStatus.Items.Remove(dropStatus.Items.FindByText("Closed"));
            dropStatus.Items.Remove(dropStatus.Items.FindByText("Cancel"));
            dropStatus.Items.Remove(dropStatus.Items.FindByText("Loaded in Yard"));
            dropStatus.Items.Remove(dropStatus.Items.FindByText("Terminated"));
            dropStatus.Items.Remove(dropStatus.Items.FindByText("Load Planner"));
            dropStatus.Items.Remove(dropStatus.Items.FindByText("Drop in Warehouse"));
            dropStatus.Items.Remove(dropStatus.Items.FindByText("Invoiceable"));
            ListItem Item = dropStatus.Items.FindByText("Reached Destination");
            dropStatus.Items.Remove(Item);
            dropStatus.Items.Insert(5, Item);
            dropStatus.Items.Insert(0, new ListItem("All Transit Loads", dropStatus.Items.Count + 1.ToString()));
        }
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    private void GridFill()
    {
        if (Session["EmployeeId"] == null)
            return;
        string strEmployeeId = Convert.ToString(Session["EmployeeId"]).Trim();
        switch (dropStatus.SelectedItem.Text.Trim())
        {//added a collumn for hazmat(madhav)
            case "Assigned":
                #region Assigned
                grdManager.MainTableName = "eTn_Load_View";
                grdManager.MainTablePK = "eTn_Load_View.bint_LoadId";

                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.
                grdManager.SingleRowColumnsList = " Convert(varchar(20),eTn_Load_View.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load_View.bint_LoadId as 'Load'," +
                    "eTn_Load_View.nvar_Container as 'Container1',eTn_Load_View.nvar_Container1 as 'Container2', (Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end) + eTn_LoadType.nvar_LoadTypeDesc as 'Type',eTn_Load_View.nvar_RailBill as 'Container Size/Location', eTn_Load.nvar_TripType as TripType," +
                    CommonFunctions.DateQueryString("eTn_Load_View.date_LastFreeDate", "Last Free Date") + ";nvar_Booking as 'Booking#';nvar_Pickup as 'Pickup#',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID ," +
                    "(dbo.eTn_AssignDriver_View.nvar_From+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromState) as 'FromAddress'," +
                    "(dbo.eTn_AssignDriver_View.nvar_To+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToState) as 'ToAddress';nvar_Booking as 'Booking#';nvar_Pickup as 'Pickup#'";
                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.

                grdManager.Role = "Driver";
                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load_View.bint_CustomerId " +
                    " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load_View.bint_LoadTypeId " +
                    " inner join eTn_AssignDriver_View on eTn_AssignDriver_View.bint_LoadId = eTn_Load_View.bint_LoadId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load_View.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() + " and Lower(eTn_Load_View.nvar_AssignTo) = 'driver' and eTn_AssignDriver_View.bint_EmployeeId = " + strEmployeeId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load_View.date_DeliveryAppointmentDate) desc,eTn_Load_View.date_DeliveryAppointmentDate,eTn_Load_View.bint_LoadId";
                //grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load_View.date_LastFreeDate) desc,eTn_Load_View.date_LastFreeDate,eTn_Load_View.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Type;Origin;Pickup#;Pickup;Destination;Delivery Appt Date;Booking#;Last Free Date;Container Size/Location";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+'<br/> To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.',case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' <br/>To<br/> '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Delivery Appt Date';eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load_View.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Driver/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;
                grdManager.LoadStatus = "Assigned";
                barManager.HeaderText = "Assigned Loads";
                grdManager.BindGrid(0);
                break;
                #endregion

            case "Pickedup":
                #region Pickedup
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', (Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end) + eTn_LoadType.nvar_LoadTypeDesc as 'Type', eTn_Load.nvar_TripType as TripType, " +
                    CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Booking as 'Booking#';nvar_Booking as 'Pickup#',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad, eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID ," +
                    "(dbo.eTn_AssignDriver_View.nvar_From+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromState) as 'FromAddress'," +
                    "(dbo.eTn_AssignDriver_View.nvar_To+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToState) as 'ToAddress' ;nvar_Pickup as 'Pickup#'";
                grdManager.Role = "Driver";
                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.

                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                    " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId " +
                    " inner join eTn_AssignDriver_View on eTn_AssignDriver_View.bint_LoadId = eTn_Load.bint_LoadId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() + " and Lower(eTn_Load.nvar_AssignTo) = 'driver' and eTn_AssignDriver_View.bint_EmployeeId = " + strEmployeeId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Type;Origin;Pickup#;Pickup;Destination;Delivery Appt Date;Last Free Date";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+'<br/> To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.',case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' <br/>To<br/> '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Delivery Appt Date';eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Driver/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;
                grdManager.LoadStatus = "Pickedup";
                barManager.HeaderText = "Pickedup Loads";
                grdManager.BindGrid(0);
                break;
                #endregion

            case "Loaded in Yard":
                #region Loaded in Yard
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', (Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end) + eTn_LoadType.nvar_LoadTypeDesc as 'Type'," +
                    CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Booking as 'Booking#';nvar_Booking as 'Pickup#',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad, eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID ," +
                    "(dbo.eTn_AssignDriver_View.nvar_From+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromState) as 'FromAddress'," +
                    "(dbo.eTn_AssignDriver_View.nvar_To+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToState) as 'ToAddress' ;nvar_Pickup as 'Pickup#'";
                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.

                grdManager.Role = "Driver";
                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                    " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId " +
                    " inner join eTn_AssignDriver_View on eTn_AssignDriver_View.bint_LoadId = eTn_Load.bint_LoadId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() + " and Lower(eTn_Load.nvar_AssignTo) = 'driver' and eTn_AssignDriver_View.bint_EmployeeId = " + strEmployeeId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Type;Origin;Pickup#;Pickup;Destination;Delivery Appt Date;Last Free Date";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+'<br/> To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.',case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' <br/>To<br/> '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Delivery Appt Date';eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Driver/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;
                grdManager.LoadStatus = "Loaded in Yard";
                barManager.HeaderText = "Loaded in Yard Loads";
                grdManager.BindGrid(0);
                break;
                #endregion

            case "En-route":
                #region En-route
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', (Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end) + eTn_LoadType.nvar_LoadTypeDesc as 'Type', eTn_Load.nvar_TripType as TripType," +
                    CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Booking as 'Booking#';nvar_Booking as 'Pickup#',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad, eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID ," +
                    "(dbo.eTn_AssignDriver_View.nvar_From+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromState) as 'FromAddress'," +
                    "(dbo.eTn_AssignDriver_View.nvar_To+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToState) as 'ToAddress' ;nvar_Pickup as 'Pickup#'";
                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.

                grdManager.Role = "Driver";
                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                    " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId " +
                    " inner join eTn_AssignDriver_View on eTn_AssignDriver_View.bint_LoadId = eTn_Load.bint_LoadId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() + " and Lower(eTn_Load.nvar_AssignTo) = 'driver' and eTn_AssignDriver_View.bint_EmployeeId = " + strEmployeeId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Type;Origin;Pickup#;Pickup;Destination;Delivery Appt Date;Last Free Date";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+'<br/> To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.',case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' <br/>To<br/> '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Delivery Appt Date';eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Driver/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;
                grdManager.LoadStatus = "En-route";
                barManager.HeaderText = "En-route Loads";
                grdManager.BindGrid(0);
                break;
                #endregion

            case "Drop in Warehouse":
                #region Drop in Warehouse
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', (Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end) + eTn_LoadType.nvar_LoadTypeDesc as 'Type'," +
                    CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Booking as 'Booking#';nvar_Booking as 'Pickup#',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad, eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID ," +
                    "(dbo.eTn_AssignDriver_View.nvar_From+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromState) as 'FromAddress'," +
                    "(dbo.eTn_AssignDriver_View.nvar_To+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToState) as 'ToAddress' ;nvar_Pickup as 'Pickup#'";
                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.

                grdManager.Role = "Driver";
                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                    " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId " +
                    " inner join eTn_AssignDriver_View on eTn_AssignDriver_View.bint_LoadId = eTn_Load.bint_LoadId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() + " and Lower(eTn_Load.nvar_AssignTo) = 'driver' and eTn_AssignDriver_View.bint_EmployeeId = " + strEmployeeId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Type;Origin;Pickup#;Pickup;Destination;Delivery Appt Date;Last Free Date";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+'<br/> To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.',case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' <br/>To<br/> '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Delivery Appt Date';eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Driver/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;
                grdManager.LoadStatus = "Drop in Warehouse";
                barManager.HeaderText = "Drop in Warehouse Loads";
                grdManager.BindGrid(0);
                break;
                #endregion

            case "Driver on Waiting":
                #region Driver on Waiting
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', (Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end) + eTn_LoadType.nvar_LoadTypeDesc as 'Type', eTn_Load.nvar_TripType as TripType, " +
                    CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Booking as 'Booking#';nvar_Booking as 'Pickup#',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad, eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID ," +
                    "(dbo.eTn_AssignDriver_View.nvar_From+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromState) as 'FromAddress'," +
                    "(dbo.eTn_AssignDriver_View.nvar_To+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToState) as 'ToAddress' ;nvar_Pickup as 'Pickup#'";
                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.

                grdManager.Role = "Driver";
                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                    " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId " +
                    " inner join eTn_AssignDriver_View on eTn_AssignDriver_View.bint_LoadId = eTn_Load.bint_LoadId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() + " and Lower(eTn_Load.nvar_AssignTo) = 'driver' and eTn_AssignDriver_View.bint_EmployeeId = " + strEmployeeId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Type;Origin;Pickup#;Pickup;Destination;Delivery Appt Date;Last Free Date";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+'<br/> To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.',case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' <br/>To<br/> '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Delivery Appt Date';eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Driver/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;
                grdManager.LoadStatus = "Driver on Waiting";
                barManager.HeaderText = "Driver on Waiting Loads";
                grdManager.BindGrid(0);
                break;
                #endregion

            case "Delivered":
                #region Delivered
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', (Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end) + eTn_LoadType.nvar_LoadTypeDesc as 'Type', eTn_Load.nvar_TripType as TripType," +
                    CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Booking as 'Booking#';nvar_Booking as 'Pickup#', etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad, eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID ," +
                    "(dbo.eTn_AssignDriver_View.nvar_From+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromState) as 'FromAddress'," +
                    "(dbo.eTn_AssignDriver_View.nvar_To+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToState) as 'ToAddress' ;nvar_Pickup as 'Pickup#'";
                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.

                grdManager.Role = "Driver";
                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                    " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId " +
                    " inner join eTn_AssignDriver_View on eTn_AssignDriver_View.bint_LoadId = eTn_Load.bint_LoadId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() + " and Lower(eTn_Load.nvar_AssignTo) = 'driver' and eTn_AssignDriver_View.bint_EmployeeId = " + strEmployeeId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Type;Origin;Pickup#;Pickup;Destination;Delivery Appt Date;Last Free Date";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+'<br/> To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.',case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' <br/>To<br/> '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Delivery Appt Date';eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Driver/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;
                grdManager.LoadStatus = "Delivered";
                barManager.HeaderText = "Delivered Loads";
                grdManager.BindGrid(0);
                break;
                #endregion

            case "Empty in Yard":
                #region Empty in Yard
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', (Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end) + eTn_LoadType.nvar_LoadTypeDesc as 'Type', eTn_Load.nvar_TripType as TripType," +
                    CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Booking as 'Booking#';nvar_Booking as 'Pickup#',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID ," +
                    "(dbo.eTn_AssignDriver_View.nvar_From+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromState) as 'FromAddress'," +
                    "(dbo.eTn_AssignDriver_View.nvar_To+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToState) as 'ToAddress'";
                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.

                grdManager.Role = "Driver";
                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                    " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId " +
                    " inner join eTn_AssignDriver_View on eTn_AssignDriver_View.bint_LoadId = eTn_Load.bint_LoadId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() + " and Lower(eTn_Load.nvar_AssignTo) = 'driver' and eTn_AssignDriver_View.bint_EmployeeId = " + strEmployeeId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Type;Origin;Destination;Days In Yard;Last Location;Date & Time;Comments";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Appt. Date';eTn_TrackLoad.date_CreateDate as 'Delivered Date'^eTn_TrackLoad.date_CreateDate as 'Date & Time'^eTn_TrackLoad.nvar_Notes as 'Comments'^DateDiff(dd,isnull(eTn_TrackLoad.date_CreateDate,getdate()),getdate()) as 'Days In Yard'^eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Driver/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;
                grdManager.LoadStatus = "Empty In Yard";
                barManager.HeaderText = "Empty In Yard Loads";
                grdManager.BindGrid(0);
                break;
                #endregion

            case "Terminated":
                #region Terminated
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', (Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end) + eTn_LoadType.nvar_LoadTypeDesc as 'Type'," +
                    CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Booking as 'Booking#';nvar_Booking as 'Pickup#',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID ," +
                    "(dbo.eTn_AssignDriver_View.nvar_From+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromState) as 'FromAddress'," +
                    "(dbo.eTn_AssignDriver_View.nvar_To+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToState) as 'ToAddress' ;nvar_Pickup as 'Pickup#'";
                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.

                grdManager.Role = "Driver";
                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                    " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId " +
                    " inner join eTn_AssignDriver_View on eTn_AssignDriver_View.bint_LoadId = eTn_Load.bint_LoadId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() + " and Lower(eTn_Load.nvar_AssignTo) = 'driver' and eTn_AssignDriver_View.bint_EmployeeId = " + strEmployeeId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Type;Origin;Pickup#;Pickup;Destination;Delivery Appt Date;Last Free Date";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+'<br/> To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.',case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' <br/>To<br/> '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Delivery Appt Date';eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Driver/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;
                grdManager.LoadStatus = "Terminated";
                barManager.HeaderText = "Terminated Loads";
                grdManager.BindGrid(0);
                break;
                #endregion

            case "Paperwork pending":
                #region Paperwork Pending
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', (Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end) + eTn_LoadType.nvar_LoadTypeDesc as 'Type', eTn_Load.nvar_TripType as TripType," +
                    CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Booking as 'Booking#';nvar_Booking as 'Pickup#',etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID ," +
                    "(dbo.eTn_AssignDriver_View.nvar_From+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromState) as 'FromAddress'," +
                    "(dbo.eTn_AssignDriver_View.nvar_To+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToState) as 'ToAddress' ;nvar_Pickup as 'Pickup#'";
                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.

                grdManager.Role = "Driver";
                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                    " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId " +
                    " inner join eTn_AssignDriver_View on eTn_AssignDriver_View.bint_LoadId = eTn_Load.bint_LoadId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() + " and Lower(eTn_Load.nvar_AssignTo) = 'driver' and eTn_AssignDriver_View.bint_EmployeeId = " + strEmployeeId;
                grdManager.SingleRowColumnsOrderByClause = " order by len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Type;Origin;Pickup#;Pickup;Destination;Delivery Appt Date;Last Free Date";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+'<br/> To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.',case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' <br/>To<br/> '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Delivery Appt Date';eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                //if (string.Compare(Convert.ToString(Session["Role"]), "Driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                //{
                //    grdManager.LastColumnLinksList = "Update Load Status";
                //    grdManager.LastColumnPagesList = "~/Driver/UpdateLoadStatus.aspx";
                //}
                grdManager.LastColumnLinksList = string.Empty;
                grdManager.DeleteVisible = false;
                grdManager.LoadStatus = "Paperwork Pending";
                barManager.HeaderText = "Paperwork Pending Loads";
                grdManager.BindGrid(0);
                break;
                #endregion            

            case "All Transit Loads":
                #region All Transit Loads
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.
                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2', (Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end) + eTn_LoadType.nvar_LoadTypeDesc as 'Type',eTn_Load.nvar_TripType as TripType, etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID ," +
                    CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Booking as 'Booking#';nvar_Booking as 'Pickup#';eTn_LoadStatus.nvar_LoadStatusDesc as 'Status'," +
                    "(dbo.eTn_AssignDriver_View.nvar_From+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromState) as 'FromAddress'," +
                    "(dbo.eTn_AssignDriver_View.nvar_To+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToState) as 'ToAddress' ;nvar_Pickup as 'Pickup#'";
                //SLine 2016 Enhancements Added the Rail Container, Hot Shipments column to the Query.

                grdManager.Role = "Driver";
                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                    " inner join  eTn_LoadStatus on eTn_LoadStatus.bint_LoadStatusId = eTn_Load.bint_LoadStatusId " +
                    " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId " +
                    " inner join eTn_AssignDriver_View on eTn_AssignDriver_View.bint_LoadId = eTn_Load.bint_LoadId ";
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId in (select eTn_LoadStatus.bint_LoadStatusId from eTn_LoadStatus where lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'new' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'load planner' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'drop in warehouse' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'terminated' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'paperwork pending' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'loaded in yard' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'closed' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'cancel' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'assigned' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'invoiceable' and lower(eTn_LoadStatus.nvar_LoadStatusDesc) <> 'street turn') " + " and Lower(eTn_Load.nvar_AssignTo) = 'driver' and eTn_AssignDriver_View.bint_EmployeeId = " + strEmployeeId;
                grdManager.SingleRowColumnsOrderByClause = " order by eTn_LoadStatus.tint_SortOrder,len(eTn_Load.date_LastFreeDate) desc,eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Type;Origin;Pickup#;Pickup;Destination;Delivery Appt Date;Last Free Date;Status";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination;eTn_TrackLoad";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId;eTn_TrackLoad.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+'<br/> To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.',case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' <br/>To<br/> '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Delivery Appt Date';eTn_TrackLoad.nvar_Location as 'Last Location'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId ";
                grdManager.MultipleRowTablesAdditionalWhereClause = ";;eTn_TrackLoad.bint_LoadStatusId=" + dropStatus.SelectedItem.Value.Trim();
                if (string.Compare(Convert.ToString(Session["Role"]), "Driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Driver/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;                
                grdManager.LoadStatus = "Transit";
                barManager.HeaderText = "Transit Loads";
                grdManager.BindGrid(0);
                #endregion
                break;

            case "Reached Destination":
                #region Reached Destination Status Loads
                grdManager.MainTableName = "eTn_Load";
                grdManager.MainTablePK = "eTn_Load.bint_LoadId";

                //SLine 2016 Enhancements Added the Hot Shipments column to the Query.

                //In the Old Code the Query was having a blank field before the IsHazmat field(Showed by _____). May be was a typo-error. Have been removed in the new code in the next line.
                /*
                 *  grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2'," +
                    "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                    "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ",eTn_LoadStatus.nvar_LoadStatusDesc as 'Status',(select [date_CreateDate] from eTn_TrackLoad where bint_LoadId=eTn_Load.bint_LoadId and bint_LoadStatusId=(select bint_LoadStatusId from eTn_LoadStatus where lower([nvar_LoadStatusDesc])='reached destination')) as 'Driver Reached @'," +
                    "(dbo.eTn_AssignDriver_View.nvar_From+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromState) as 'FromAddress',_________, eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID ," +
                    "(dbo.eTn_AssignDriver_View.nvar_To+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToState) as 'ToAddress'";
                 * 
                 */


                grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
                    "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2', eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end as 'Type', eTn_Load.nvar_TripType as TripType," +
                    "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
                    "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ",eTn_LoadStatus.nvar_LoadStatusDesc as 'Status',(select [date_CreateDate] from eTn_TrackLoad where bint_LoadId=eTn_Load.bint_LoadId and bint_LoadStatusId=(select bint_LoadStatusId from eTn_LoadStatus where lower([nvar_LoadStatusDesc])='reached destination')) as 'Driver Reached @'," +
                    "(dbo.eTn_AssignDriver_View.nvar_From+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_FromState) as 'FromAddress', etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold , eTn_Load.bint_LoadStatusId as LoadStatusID ," +
                    "(dbo.eTn_AssignDriver_View.nvar_To+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToCity+'<br/>'+ dbo.eTn_AssignDriver_View.nvar_ToState) as 'ToAddress'";
                //SLine 2016 Enhancements Added the Hot Shipments column to the Query.

                grdManager.Role = "Driver";

                grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                    " inner join  eTn_LoadStatus on eTn_LoadStatus.bint_LoadStatusId = eTn_Load.bint_LoadStatusId "+
                  " inner join eTn_AssignDriver_View on eTn_AssignDriver_View.bint_LoadId = eTn_Load.bint_LoadId ";

                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId = " + dropStatus.SelectedItem.Value.Trim() + " and Lower(eTn_Load.nvar_AssignTo) = 'driver' and eTn_AssignDriver_View.bint_EmployeeId = " + strEmployeeId;
                grdManager.SingleRowColumnsOrderByClause = " order by eTn_Load.bint_LoadId";
                grdManager.VisibleColumnsList = "Load;Customer;Driver/Carrier;Origin;Pickup#;Pickup;Destination;Driver Reached @;Delivery Appt Date";
                grdManager.OptionalRowTablesList = "eTn_AssignCarrier^eTn_AssignDriver_View";
                grdManager.OptionalRowTableKeyList = "eTn_AssignCarrier.bint_LoadId^eTn_AssignDriver_View.bint_LoadId";
                grdManager.OptionalRowTableColumnsList = "eTn_Carrier.nvar_CarrierName + ' ' + " + CommonFunctions.PhoneQueryString("eTn_Carrier.num_Phone") + " as 'Driver/Carrier'" +
                    "^eTn_UserLogin.nvar_FirstName + ' ' + eTn_UserLogin.nvar_LastName + ' ' +  eTn_UserLogin.nvar_MiddleName +  ' ' + " + CommonFunctions.PhoneQueryString("eTn_Employee.num_Mobile") + " as 'Driver/Carrier'," +
                    CommonFunctions.AddressQueryString("eTn_AssignDriver_View.nvar_From;eTn_AssignDriver_View.nvar_FromCity;eTn_AssignDriver_View.nvar_Fromstate", "From") + "," + CommonFunctions.AddressQueryString("eTn_AssignDriver_View.nvar_To;eTn_AssignDriver_View.nvar_ToCity;eTn_AssignDriver_View.nvar_Tostate", "To");
                grdManager.OptionalRowTablesInnnerJoinClauseList = "inner join eTn_Carrier on eTn_Carrier.bint_CarrierId = eTn_AssignCarrier.bint_CarrierId" +
                    "^inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver_View.bint_EmployeeId inner join eTn_UserLogin on eTn_UserLogin.bint_RolePlayerId = eTn_AssignDriver_View.bint_EmployeeId and eTn_UserLogin.bint_RoleId = (select bint_RoleId from eTn_Role where lower(nvar_RoleName) = 'driver') ";
                grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination";
                grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId";
                grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
                    "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' <br/>To<br/> '+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
                    "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.',case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' <br/>To<br/> '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Delivery Appt Date'";
                grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
                    "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId";
                if (string.Compare(Convert.ToString(Session["Role"]), "Driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    grdManager.LastColumnLinksList = "Update Load Status";
                    grdManager.LastColumnPagesList = "~/Driver/UpdateLoadStatus.aspx";
                }
                grdManager.DeleteVisible = false;
                grdManager.LastLocationVisible = true;
                grdManager.LoadStatus = "Reached Destination";
                barManager.HeaderText = "Reached Destination";
                if (Session["LoadGridPageIndex"] == null)
                    Session["LoadGridPageIndex"] = 0;
                grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
                #endregion
                break;
            default: break;
        }
    }
    protected void btnShow_Click(object sender, EventArgs e)
    {
        Session["LoadGridPageIndex"] = 0;
        GridFill();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (Session["EmployeeId"] != null)
        {
            string strInnerJoinClause = " inner join eTn_AssignDriver_View on eTn_AssignDriver_View.bint_LoadId = eTn_Load.bint_LoadId ";
            string strExtraWhereClause = " and Lower(eTn_Load.[nvar_AssignTo]) = 'driver' and (eTn_Load.bint_LoadStatusId=6 or eTn_AssignDriver_View.bint_EmployeeId = " + Convert.ToString(Session["EmployeeId"]).Trim() +")";
            if (txtContainer.Text.Trim().Length > 0)
            {
                switch (ddlContainer.SelectedIndex)
                {
                    case 0:
                        Session["TrackWhereClause"] = "(eTn_Load.[nvar_Container] like '" + txtContainer.Text.Trim() + "' or eTn_Load.[nvar_Container1] like '" + txtContainer.Text.Trim() + "')";
                        break;
                    case 1:
                        Session["TrackWhereClause"] = "eTn_Load.[nvar_Ref] like '" + txtContainer.Text.Trim() + "%'";
                        break;
                    case 2:
                        Session["TrackWhereClause"] = "[eTn_AssignDriver_View].[nvar_Tag] like '" + txtContainer.Text.Trim() + "%'";
                        break;
                    case 3:
                        Session["TrackWhereClause"] = "(eTn_Load.[nvar_Chasis] like '" + txtContainer.Text.Trim() + "%' or eTn_Load.[nvar_Chasis1] like '" + txtContainer.Text.Trim() + "%')";
                        break;
                    case 4:
                        try
                        {
                            Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=" + Convert.ToInt64(txtContainer.Text.Trim());
                        }
                        catch
                        {
                            Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=0";
                        }
                        break;
                }
            }
            else
            {
                Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=0";
            }
            bool blVal = false;
            try
            {
                if (Convert.ToInt32(DBClass.executeScalar("select count(eTn_Load.bint_LoadId) from eTn_Load " + strInnerJoinClause + " where (" + Convert.ToString(Session["TrackWhereClause"]) + ")" + strExtraWhereClause)) == 1)
                {
                    blVal = true;
                }
            }
            catch
            {
            }
            if (blVal)
            {
                Response.Redirect("DriverTrackLoad.aspx?" + DBClass.executeScalar("select eTn_Load.bint_LoadId from eTn_Load " + strInnerJoinClause + " where (" + Convert.ToString(Session["TrackWhereClause"]) + ")" + strExtraWhereClause));
            }
            Response.Redirect("DriverTrackLoadResults.aspx");
        }        
    }

}
