using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class LinksBar : System.Web.UI.UserControl
{
    public string HeaderText
    {
        set { lblText.Text = Convert.ToString(value).Trim(); }
    }
    public string LinksList
    {
        set { ViewState["LinksList"] = Convert.ToString(value).Trim().Replace(',', ';'); }
    }
    public string PagesList
    {
        set { ViewState["PagesList"] = Convert.ToString(value).Trim().Replace(',', ';'); }
    }
    public string EnableLinksList
    {
        set { ViewState["EnableLinksList"] = Convert.ToString(value).Trim().Replace(',', ';'); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        AssignPagesToLinks();
    }
    public void AssignPagesToLinks()
    {
        if (ViewState["LinksList"] != null && ViewState["EnableLinksList"] != null)
        {
            string[] LinksList = Convert.ToString(ViewState["LinksList"]).Trim().Split(';');
            string[] PagesList = Convert.ToString(ViewState["PagesList"]).Trim().Split(';');
            string[] EnableLinksList = Convert.ToString(ViewState["EnableLinksList"]).Trim().Split(';');

            if (LinksList.Length == EnableLinksList.Length)
            {
                plhLinks.Controls.Clear();
                for (int i = 0; i < LinksList.Length; i++)
                {
                    if (!Convert.ToBoolean(EnableLinksList[i].Trim().ToLower()))
                        plhLinks.Controls.Add(new LiteralControl(LinksList[i].Replace("&", "&amp;")));
                    else                   
                        plhLinks.Controls.Add(new LiteralControl("<a href=" + PagesList[i] + ">" + LinksList[i].Replace("&", "&amp;") + "</a>"));
                    if (i != LinksList.Length - 1)
                        plhLinks.Controls.Add(new LiteralControl(" | "));
                }
            }
            LinksList = null; PagesList = null; EnableLinksList = null;
        }
    }
}
