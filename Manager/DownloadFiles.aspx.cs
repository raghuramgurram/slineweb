using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class Manager_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.Request.QueryString.Count > 0)
            {
                if (Request.QueryString["Id"].ToString() == "P")
                {
                    BindFilesList("P");
                    Session["TopHeaderText"] = "Download Payable Files";
                }
                else
                {
                    BindFilesList("R");
                    Session["TopHeaderText"] = "Download Receivable Files";
                }
            }
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (gvFiles.Rows.Count == 0)
            lblError.Visible = true;
        else
            lblError.Visible = false;

        for (int index = 0; index < gvFiles.Rows.Count; index++)
        {
            if (index % 2 == 0)
            {
                for (int k = 0; k < gvFiles.Columns.Count; k++)
                {
                    gvFiles.HeaderRow.Cells[k].CssClass = "GridHeader";
                    gvFiles.Rows[index].Cells[k].CssClass = "GridItem";
                }
            }
            else
            {
                for (int k = 0; k < gvFiles.Columns.Count; k++)
                {
                    gvFiles.Rows[index].Cells[k].CssClass = "GridAltItem";
                }
            }
        }
    }

    private void BindFilesList(string type)
    {
        try
        {
            ViewState["Type"] = type;
            gvFiles.DataSource = DBClass.returnDataTable("SELECT bint_ExportFileId,nvar_FileName,nvar_Notes,convert(varchar(10),[date_CreateDate],103) as 'CreatedDate' from [eTn_ExportFiles] where [nvar_type]='" + type + "'");
            gvFiles.DataBind();
        }
        catch
        {
 
        }
    }
    protected void OndownloadClick(object sender, EventArgs e)
    {
        LinkButton lnkDownload = (LinkButton)sender;
        GridViewRow gridrow = gvFiles.Rows[((GridViewRow)lnkDownload.Parent.Parent).RowIndex];
        if (gridrow != null)
        {
            string filename = ((Label)gridrow.Cells[0].FindControl("lblFileName")).Text;
            if (filename.Trim().Length > 0)
            {
                Downoloadfile(Server.MapPath("~/ExportFiles\\") + filename, filename);
            }
        }
        gridrow = null;       
        lnkDownload = null;
    }
    private void Downoloadfile(string filename, string name)
    {
        try
        {
            FileInfo fileinfo = new FileInfo(filename);
            if (fileinfo.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileinfo.Name);
                Response.AddHeader("Content-Length", fileinfo.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(fileinfo.FullName);
                Response.End();
            }
            else
            {
                Response.Write("<script>alert('file doesnot exist.');<script>");
            }
            fileinfo = null;
        }
        catch
        { }
    }
    //private void BindFilesList(string type)
    //{
    //    DirectoryInfo Dirinfo = new DirectoryInfo(Server.MapPath("~/ExportFiles"));
    //    if (Dirinfo.Exists)
    //    {
    //        DataTable dtFiles = ((DataTable)ViewState["files"]).Clone();
    //        foreach (FileInfo fileinfo in Dirinfo.GetFiles())
    //        {
    //            string[] filename = fileinfo.Name.Trim().Split('_');
    //            if (Convert.ToString(Session["UserId"]).Trim().Contains(filename[0]) && filename[1].Contains(type))
    //            {
    //                DataRow drow = dtFiles.NewRow();
    //                drow["FileName"] = fileinfo.Name;
    //                drow["FilePath"] = fileinfo.FullName;
    //                dtFiles.Rows.Add(drow);
    //                drow = null;
    //            }
    //        }
    //        if (dtFiles.Rows.Count > 0)
    //        {
    //            gvFiles.DataSource = dtFiles;
    //            gvFiles.DataBind();
    //            gvFiles.Visible = true;
    //        }
    //        else
    //        {
    //            lblError.Visible = true;
    //            gvFiles.Visible = false;
    //        }
    //    }
    //    Dirinfo = null;
    //}
    protected void OnDeleted(object sender, EventArgs e)
    {
        LinkButton lnkDownload = (LinkButton)sender;
        GridViewRow gridrow = gvFiles.Rows[((GridViewRow)lnkDownload.Parent.Parent).RowIndex];
        if (gridrow != null)
        {
            long FileId = Convert.ToInt64(((Label)gridrow.Cells[0].FindControl("lblFileId")).Text);
            string filename = ((Label)gridrow.Cells[0].FindControl("lblFileName")).Text;
            if (FileId > 0)
            {
                FileInfo fileinfo = new FileInfo(Server.MapPath("~/ExportFiles\\")+filename);
                if (fileinfo.Exists)
                {
                    fileinfo.Delete();
                    SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_DeleteExportFiles", new object[] { FileId });
                    BindFilesList(Convert.ToString(ViewState["Type"]));
                }
               
            }
        }
        gridrow = null;
        lnkDownload = null;
    }    
}
