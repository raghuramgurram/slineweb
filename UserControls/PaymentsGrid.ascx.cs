using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;


public partial class PaymentsGrid : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["PaymentGridPageIndex"] == null)
            BindGrid(0);
        else
        {
            BindGrid(Convert.ToInt32(Session["PaymentGridPageIndex"]));
        }
    }
    public string MainTableName
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["MainTableName"] = Convert.ToString(value).Trim();
            }
        }
    }
    public string MainTablePK
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["MainTablePK"] = Convert.ToString(value).Trim();
            }
        }
    }
    public string SingleRowColumnsList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["SingleRowColumnsList"] = Convert.ToString(value).Trim().Replace(";", ",");
            }
        }
    }
    public string InnerJoinClauseWithOnlySingleRowTables
    {
        set
        {
            ViewState["InnerJoinClauseWithOnlySingleRowTables"] = Convert.ToString(value).Trim();
        }
    }
    public string SingleRowColumnsWhereClause
    {
        set
        {
            ViewState["SingleRowColumnsWhereClause"] = Convert.ToString(value).Trim();
        }
    }
    public string SingleRowColumnsOrderByClause
    {
        set
        {
            ViewState["SingleRowColumnsOrderByClause"] = Convert.ToString(value).Trim();
        }
    }
    public string MultipleRowTablesList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["MultipleRowTableNames"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    public string MultipleRowTableKeyList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["MultipleRowTableKeyList"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    public string MultipleRowTableColumnsList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["MultipleRowTableColumnsList"] = Convert.ToString(value).Trim().Split(';');
            }
        }
    }
    public string MultipleRowTablesInnnerJoinClause
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["MultipleRowTablesInnnerJoinClause"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    public string MultipleRowTablesAdditionalWhereClause
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["MultipleRowTablesAdditionalWhereClause"] = Convert.ToString(value).Trim().Split(';');
            }
        }
    }
    public string OptionalRowTablesList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["OptionalRowTablesList"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    public string OptionalRowTableKeyList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["OptionalRowTableKeyList"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    public string OptionalRowTableColumnsList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["OptionalRowTableColumnsList"] = Convert.ToString(value).Trim().Split(';');
            }
        }
    }
    public string OptionalRowTablesInnnerJoinClauseList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["OptionalRowTablesInnnerJoinClauseList"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    //public string OptionalRowTablesWhereClause
    //{
    //    set
    //    {
    //        if (Convert.ToString(value).Trim().Length > 0)
    //        {
    //            ViewState["OptionalRowTablesWhereClause"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
    //        }
    //    }
    //}
    public string VisibleColumnsList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["VisibleColumnsList"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }            
        }
    }
    public bool DeleteVisible
    {
        set
        {
            ViewState["DeleteVisible"] = value;
        }
    }
    public string DependentTablesList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["DependentTablesList"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    public string DependentTableKeysList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["DependentTableKeysList"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    public string DeleteTablesList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["DeleteTablesList"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';'); 
            }
        }
    }
    public string DeleteTablePKList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["DeleteTablePKList"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    public string LoadStatus
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["LoadStatus"] = Convert.ToString(value).Trim();
            }
        }
    }
    public string LastColumnLinksList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["LastColumnLinksList"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    public string LastColumnPagesList
    {
        set
        {
            if (Convert.ToString(value).Trim().Length > 0)
            {
                ViewState["LastColumnPagesList"] = Convert.ToString(value).Trim().Replace(",", ";").Split(';');
            }
        }
    }
    public bool LastLocationVisible
    {
        set
        {
            ViewState["LastLocationVisible"] = value;
        }
    }
   
    private void FormatGrid()
    {        
        dggrid.AutoGenerateColumns = false;
        dggrid.PageSize =Convert.ToInt32(ConfigurationSettings.AppSettings["LoadGridPageSize"].ToString());
        dggrid.AllowCustomPaging = true;
        dggrid.AllowPaging = true;
        dggrid.AllowSorting = true;
        dggrid.PagerStyle.Mode = PagerMode.NumericPages;
        dggrid.PagerStyle.HorizontalAlign = HorizontalAlign.Center;
    }
    public void BindGrid(int iPageNumber)
    {
        if (Convert.ToString(ViewState["MainTableName"]).Trim().Length > 0)
        {
            #region Validation
            //if (ViewState["VisibleHeadersList"] != null && ViewState["VisibleColumnsList"] != null)
            //    if (((string[])ViewState["VisibleHeadersList"]).Length != ((string[])ViewState["VisibleColumnsList"]).Length)
            //        return;
            #endregion
            #region Getting Main DataSet
            object[] parms = null;
            string strProdName = "";
            if (Convert.ToString(ViewState["InnerJoinClauseWithOnlySingleRowTables"]).Trim().Length == 0)
            {
                parms = new object[] { Convert.ToString(ViewState["MainTableName"]), Convert.ToString(ViewState["MainTablePK"]), Convert.ToString(ViewState["SingleRowColumnsList"]), ConfigurationSettings.AppSettings["LoadGridPageSize"].ToString(), iPageNumber, Convert.ToString(ViewState["SingleRowColumnsWhereClause"]) };
                strProdName = "GetPagedData";
            }
            else
            {
                if (Convert.ToString(ViewState["SingleRowColumnsOrderByClause"]).Trim().Length == 0)
                {
                    parms = new object[] { Convert.ToString(ViewState["MainTableName"]), Convert.ToString(ViewState["MainTablePK"]), Convert.ToString(ViewState["SingleRowColumnsList"]), ConfigurationSettings.AppSettings["LoadGridPageSize"].ToString(), iPageNumber, Convert.ToString(ViewState["InnerJoinClauseWithOnlySingleRowTables"]), Convert.ToString(ViewState["SingleRowColumnsWhereClause"]) };
                    strProdName = "GetPagedDataWithJoins";
                }
                else
                {
                    parms = new object[] { Convert.ToString(ViewState["MainTableName"]), Convert.ToString(ViewState["MainTablePK"]), Convert.ToString(ViewState["SingleRowColumnsList"]), ConfigurationSettings.AppSettings["LoadGridPageSize"].ToString(), iPageNumber, Convert.ToString(ViewState["InnerJoinClauseWithOnlySingleRowTables"]), Convert.ToString(ViewState["SingleRowColumnsWhereClause"]), Convert.ToString(ViewState["SingleRowColumnsOrderByClause"]) };
                    strProdName = "GetPagedDataWithJoinsAndOrderBy";
                }
            }
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], strProdName, parms);
            parms = null;
            #endregion
            #region Clearing Grid
            dggrid.Controls.Clear();
            dggrid.Columns.Clear();
            #endregion
            if (ds.Tables[0].Rows.Count > 0)
            {
                #region Formatting DataSet For Optional Row Columns
                if (ViewState["OptionalRowTablesList"] != null && ViewState["OptionalRowTableColumnsList"] != null && ViewState["OptionalRowTableColumnsList"] != null)
                {                   
                    string[] strOptionalRowTablesList = (string[])ViewState["OptionalRowTablesList"];
                    string[] strOptionalRowTableKeyList = (string[])ViewState["OptionalRowTableKeyList"];
                    string[] strOptionalRowTableColumnsList = (string[])ViewState["OptionalRowTableColumnsList"];
                    string[] strOptionalRowTablesInnnerJoinClauseList = null;
                    bool isOptionalInnerJoinExist = false;
                    if (ViewState["OptionalRowTablesInnnerJoinClauseList"] != null)
                    {
                        strOptionalRowTablesInnnerJoinClauseList = (string[])ViewState["OptionalRowTablesInnnerJoinClauseList"];
                        isOptionalInnerJoinExist = true;
                    }
                    if (strOptionalRowTablesList.Length == strOptionalRowTableKeyList.Length && strOptionalRowTableKeyList.Length == strOptionalRowTableColumnsList.Length)
                    {
                        string[] strOptID = null;
                        string strOptQuery = "";
                        DataTable dtOpt = null;
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            strOptID = Convert.ToString(ds.Tables[0].Rows[i][0]).Trim().Split('^');
                            for (int j = 0; j < strOptionalRowTablesList.Length; j++)
                            {
                                string[] strTables = strOptionalRowTablesList[j].Split('^');
                                string[] strKeys = strOptionalRowTableKeyList[j].Split('^');
                                string[] strColumns = strOptionalRowTableColumnsList[j].Split('^');
                                if (strTables.Length == strKeys.Length && strKeys.Length == strColumns.Length)
                                {
                                    for (int k = 0; k < strTables.Length; k++)
                                    {
                                        if (isOptionalInnerJoinExist)
                                            strOptQuery = "select " + strColumns[k] + " from " + strTables[k] + " " + (strOptionalRowTablesInnnerJoinClauseList[j].Split('^'))[k] + " where " + strKeys[k] + "=" + strOptID[0];
                                        else
                                            strOptQuery = "select " + strColumns[k] + " from " + strTables[k] + " where " + strKeys[k] + "=" + strOptID[0];
                                        if (dtOpt != null) { dtOpt.Dispose(); dtOpt = null; }
                                        dtOpt = DBClass.returnDataTable(strOptQuery);
                                        if (dtOpt.Rows.Count > 0)
                                        {
                                            for (int colCount = 0; colCount < dtOpt.Columns.Count; colCount++)
                                            {
                                                if (!ds.Tables[0].Columns.Contains(dtOpt.Columns[colCount].ColumnName))
                                                    ds.Tables[0].Columns.Add(dtOpt.Columns[colCount].ColumnName, typeof(string));
                                                for (int rowCount = 0; rowCount < dtOpt.Rows.Count; rowCount++)
                                                {
                                                    if (rowCount == 0)
                                                    {
                                                        ds.Tables[0].Rows[i][dtOpt.Columns[colCount].ColumnName] = Convert.ToString(dtOpt.Rows[rowCount][colCount]).Trim();
                                                    }
                                                    else
                                                    {
                                                        ds.Tables[0].Rows[i][dtOpt.Columns[colCount].ColumnName] = Convert.ToString(ds.Tables[0].Rows[i][dtOpt.Columns[colCount].ColumnName]) + "^^^^" + Convert.ToString(dtOpt.Rows[rowCount][colCount]).Trim();
                                                    }
                                                }
                                            }
                                            break;
                                        }
                                    }
                                }
                                strTables = null; strKeys = null; strColumns = null;
                            }
                            strOptID = null;
                        }
                        if (dtOpt != null) { dtOpt.Dispose(); dtOpt = null; }
                        strOptID = null;
                    }
                    else
                    {
                        strOptionalRowTablesList = null; strOptionalRowTableKeyList = null; strOptionalRowTableColumnsList = null; strOptionalRowTablesInnnerJoinClauseList = null;
                        return;
                    }
                    strOptionalRowTablesList = null; strOptionalRowTableKeyList = null; strOptionalRowTableColumnsList = null; strOptionalRowTablesInnnerJoinClauseList = null;
                }
                #endregion
                #region Formatting DataSet For Multiple Row Columns
                if (ViewState["MultipleRowTableNames"] != null && ViewState["MultipleRowTableKeyList"] != null && ViewState["MultipleRowTableColumnsList"] != null)
                {
                    string[] strMultipleRowTableNames = (string[])ViewState["MultipleRowTableNames"];
                    string[] strMultipleRowTableKeyList = (string[])ViewState["MultipleRowTableKeyList"];
                    string[] strMultipleRowTableColumnsList = (string[])ViewState["MultipleRowTableColumnsList"];
                    string[] strMultipleRowTablesInnnerJoinClauseList = null;
                    bool isInnerJoinExist = false;
                    if (ViewState["MultipleRowTablesInnnerJoinClause"] != null)
                    {
                        strMultipleRowTablesInnnerJoinClauseList = (string[])ViewState["MultipleRowTablesInnnerJoinClause"];
                        isInnerJoinExist = true;
                    }
                    string[] strMultipleRowTablesAdditionalWhereClause =null;
                    bool isWhereClauseExist = false;
                    if (ViewState["MultipleRowTablesAdditionalWhereClause"] != null)
                    {
                        strMultipleRowTablesAdditionalWhereClause = (string[])ViewState["MultipleRowTablesAdditionalWhereClause"];
                        isWhereClauseExist = true;
                    }
                    //ViewState["MultipleRowTablesWhereClause"]
                    if (strMultipleRowTableNames.Length == strMultipleRowTableKeyList.Length && strMultipleRowTableKeyList.Length == strMultipleRowTableColumnsList.Length)
                    {
                        string[] strID = null;
                        string strQuery = "";
                        DataTable dt = null;
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            strID = Convert.ToString(ds.Tables[0].Rows[i][0]).Trim().Split('^');
                            for (int j = 0; j < strMultipleRowTableNames.Length; j++)
                            {
                                strMultipleRowTableColumnsList[j] = strMultipleRowTableColumnsList[j].Trim().Replace("^", ",");
                                if(isInnerJoinExist)
                                    strQuery = "select " + strMultipleRowTableColumnsList[j] + " from " + strMultipleRowTableNames[j] + " " +strMultipleRowTablesInnnerJoinClauseList[j] +  " where " + strMultipleRowTableKeyList[j] + "=" + strID[0];
                                else
                                    strQuery = "select " + strMultipleRowTableColumnsList[j] + " from " + strMultipleRowTableNames[j] + " where " + strMultipleRowTableKeyList[j] + "=" + strID[0];
                                if (isWhereClauseExist)
                                {
                                    if(strMultipleRowTablesAdditionalWhereClause[j].Trim().Length>0)
                                        strQuery += " and " + strMultipleRowTablesAdditionalWhereClause[j];
                                }
                                if (dt != null) { dt.Dispose(); dt = null; }
                                dt = DBClass.returnDataTable(strQuery);
                                for (int colCount = 0; colCount < dt.Columns.Count; colCount++)
                                {
                                    if(!ds.Tables[0].Columns.Contains(dt.Columns[colCount].ColumnName))
                                        ds.Tables[0].Columns.Add(dt.Columns[colCount].ColumnName, typeof(string));
                                    if (Convert.ToString(ds.Tables[0].Rows[i][dt.Columns[colCount].ColumnName]).Trim().Length == 0)
                                    {
                                        for (int rowCount = 0; rowCount < dt.Rows.Count; rowCount++)
                                        {
                                            if (rowCount == 0)
                                            {
                                                ds.Tables[0].Rows[i][dt.Columns[colCount].ColumnName] = Convert.ToString(dt.Rows[rowCount][colCount]).Trim();
                                            }
                                            else
                                            {
                                                ds.Tables[0].Rows[i][dt.Columns[colCount].ColumnName] = Convert.ToString(ds.Tables[0].Rows[i][dt.Columns[colCount].ColumnName]) + "^^^^" + Convert.ToString(dt.Rows[rowCount][colCount]).Trim();
                                            }
                                        }
                                    }
                                }
                            }
                            strID = null;
                        }
                        if (dt != null) { dt.Dispose(); dt = null; }
                        strID = null;
                    }
                    else
                    {
                        strMultipleRowTableNames = null; strMultipleRowTableKeyList = null; strMultipleRowTableColumnsList = null; strMultipleRowTablesInnnerJoinClauseList = null;
                        return;
                    }
                    strMultipleRowTableNames = null; strMultipleRowTableKeyList = null; strMultipleRowTableColumnsList = null; strMultipleRowTablesInnnerJoinClauseList = null;
                }
                #endregion
                #region Formatting DataSet For Last Location
                //if (Convert.ToString(ViewState["LastLocationVisible"]).Trim().Length > 0)
                //{
                //    if (Convert.ToBoolean(ViewState["LastLocationVisible"]))
                //    {
                //        if (ds.Tables[0].Columns.Contains("Container1") || ds.Tables[0].Columns.Contains("Container2"))
                //        {
                //            string strQuer = "", strRes = "";
                //            ds.Tables[0].Columns.Add("Last Location", typeof(string));
                //            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                //            {
                //                if (ds.Tables[0].Columns.Contains("Container1"))
                //                {
                //                    strQuer = "Select " + CommonFunctions.AddressQueryString("eTn_Equipment.nvar_City;eTn_Equipment.nvar_State", "Last Location") + " from eTn_Load " +
                //                        "inner join eTn_Equipment on eTn_Equipment.nvar_EquipmentName = eTn_Load.nvar_Container where eTn_Load.bint_LoadId = " + (ds.Tables[0].Rows[i][0].ToString().Split('^'))[0];
                //                    strRes = DBClass.executeScalar(strQuer);
                //                    if (strRes.Length > 0)
                //                    {
                //                        ds.Tables[0].Rows[i]["Last Location"] = strRes;
                //                    }
                //                }
                //                if (ds.Tables[0].Columns.Contains("Container2"))
                //                {
                //                    strQuer = "Select " + CommonFunctions.AddressQueryString("eTn_Equipment.nvar_City;eTn_Equipment.nvar_State", "Last Location") + " from eTn_Load " +
                //                            "inner join eTn_Equipment on eTn_Equipment.nvar_EquipmentName = eTn_Load.nvar_Container1 where eTn_Load.bint_LoadId = " + (ds.Tables[0].Rows[i][0].ToString().Split('^'))[0];
                //                    strRes = DBClass.executeScalar(strQuer);
                //                    if (strRes.Length > 0)
                //                    {
                //                        if (Convert.ToString(ds.Tables[0].Rows[i]["Last Location"]).Trim().Length == 0)
                //                            ds.Tables[0].Rows[i]["Last Location"] = strRes;
                //                        else
                //                            ds.Tables[0].Rows[i]["Last Location"] = Convert.ToString(ds.Tables[0].Rows[i]["Last Location"]) + "^^^^" + strRes;
                //                    }
                //                }
                //            }
                //            ds.Tables[0].AcceptChanges();
                //        }
                //    }
                //}
                #endregion
                #region Sorting Dataset
                //DataRow[] drc1 = ds.Tables[0].Select("[Last Free Date] is not null and len([Last Free Date]) > 0", "[Last Free Date] asc,[Load] asc");
                //DataRow[] drc2 = ds.Tables[0].Select("[Last Free Date] is null or len([Last Free Date]) = 0", "[Load] asc");
                //if (drc1.Length + drc2.Length <= Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]))
                //{
                //    DataTable dtTemp = ds.Tables[0].Clone();
                //    DataRow dr = ds.Tables[0].NewRow();
                //    for (int p = 0; p < drc1.Length; p++)
                //    {
                //        dtTemp.ImportRow(drc1[p]);
                //        dtTemp.AcceptChanges();
                //        dr = null;
                //    }
                //    dr = null;
                //    for (int p = 0; p < drc2.Length; p++)
                //    {
                //        dtTemp.ImportRow(drc2[p]);
                //        dtTemp.AcceptChanges();
                //        dr = null;
                //    }
                //    dr = null;
                //    ds.Tables[0].Rows.Clear();
                //    for (int p = 0; p < dtTemp.Rows.Count; p++)
                //    {
                //        ds.Tables[0].ImportRow(dtTemp.Rows[p]);
                //        ds.Tables[0].AcceptChanges();
                //    }
                //    if (dtTemp != null) { dtTemp.Dispose(); dtTemp = null; }
                //}
                //drc2 = null; drc1 = null;               
                #endregion
                FormatGrid();
                #region Column Addition
                string[] strVisibleColumnsList = (string[])ViewState["VisibleColumnsList"];
                BoundColumn bCol = null;
                for (int colCount = 0; colCount < ds.Tables[0].Columns.Count; colCount++)
                {
                    if (!this.IsColumnVisible(ds.Tables[0].Columns[colCount].ColumnName, strVisibleColumnsList))
                    {
                        bCol = new BoundColumn();
                        bCol.DataField = bCol.HeaderText = ds.Tables[0].Columns[colCount].ColumnName;
                        bCol.Visible = false;
                        bCol.HeaderStyle.CssClass = "GridHeader";
                        bCol.ItemStyle.CssClass = "GridItem";  
                        dggrid.Columns.Add(bCol);                      
                        bCol = null;
                    }
                }
                TemplateColumn tCol = null;
                for (int colCount = 0; colCount < strVisibleColumnsList.Length; colCount++)
                {
                    if (ds.Tables[0].Columns.Contains(strVisibleColumnsList[colCount]))
                    {
                        tCol = new TemplateColumn();
                        if (string.Compare(strVisibleColumnsList[colCount], "Type", true, System.Globalization.CultureInfo.CurrentCulture) == 0 && ViewState["DeleteVisible"] != null)
                            tCol.ItemTemplate = new PaymentGridTemplate(ListItemType.Item, strVisibleColumnsList[colCount], ds.Tables[0], Convert.ToBoolean(ViewState["DeleteVisible"]), Convert.ToString(ViewState["LoadStatus"]));
                        else
                            tCol.ItemTemplate = new PaymentGridTemplate(ListItemType.Item, strVisibleColumnsList[colCount], ds.Tables[0], Convert.ToString(ViewState["LoadStatus"]));
                        tCol.HeaderText = strVisibleColumnsList[colCount];
                        tCol.Visible = true;
                        tCol.HeaderStyle.CssClass = "GridHeader";
                        tCol.ItemStyle.CssClass = "GridItem";                        
                        dggrid.Columns.Add(tCol);
                        tCol = null;
                    }
                }
                strVisibleColumnsList = null; tCol = null;
                bCol = null;
                #region Last Column
                if (ViewState["LastColumnLinksList"] != null && ViewState["LastColumnPagesList"] != null)
                {
                    string[] strLastColumnPagesList = (string[])ViewState["LastColumnPagesList"];
                    string[] strLastColumnLinksList = (string[])ViewState["LastColumnLinksList"];
                    if (strLastColumnPagesList.Length == strLastColumnLinksList.Length)
                    {
                        TemplateColumn tLastCol = new TemplateColumn();
                        tLastCol.ItemTemplate = new PaymentGridTemplate(ListItemType.Item, "LastColumn", strLastColumnLinksList, strLastColumnPagesList,ds.Tables[0],Convert.ToString(ViewState["LoadStatus"]));
                        tLastCol.HeaderStyle.CssClass = "GridHeader";
                        tLastCol.ItemStyle.CssClass = "GridItem";                        
                        dggrid.Columns.Add(tLastCol);
                        tLastCol = null;
                    }
                    strLastColumnLinksList = null; strLastColumnLinksList = null;
                }
                #endregion
                #endregion
                #region Binding DataSet to Grid 
                dggrid.AllowCustomPaging = true;
                dggrid.VirtualItemCount = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                dggrid.DataSource = ds.Tables[0];
                dggrid.CurrentPageIndex = iPageNumber;                
                dggrid.DataBind();
                #region Old
                //if (dggrid.Items.Count > 0)
                //{
                //    switch (Convert.ToString(ViewState["LoadStatus"]).Trim().ToLower())
                //    {
                //        case "new"://must be modified
                //            for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                //            {
                //                if (index % 2 == 0)
                //                {
                //                    for (int k = 0; k < dggrid.Columns.Count; k++)
                //                    {
                //                        dggrid.Items[index].Cells[k].CssClass = "GridItem";
                //                    }
                //                }
                //                else
                //                {
                //                    for (int k = 0; k < dggrid.Columns.Count; k++)
                //                    {
                //                        dggrid.Items[index].Cells[k].CssClass = "GridAltItem";
                //                    }
                //                }
                //                if (Convert.ToString(ds.Tables[0].Rows[index]["Last Free Date"]).Trim().Length > 0)
                //                {
                //                    if (Convert.ToDateTime(ds.Tables[0].Rows[index]["Last Free Date"].ToString().Trim()).Date <= DateTime.Now.Date)
                //                    {
                //                        for (int k = 0; k < dggrid.Columns.Count; k++)
                //                        {
                //                            dggrid.Items[index].Cells[k].CssClass = "redrow";
                //                        }
                //                    }
                //                    else if (Convert.ToDateTime(ds.Tables[0].Rows[index]["Last Free Date"].ToString().Trim()).Date == DateTime.Now.Date.AddDays(1))
                //                    {
                //                        for (int k = 0; k < dggrid.Columns.Count; k++)
                //                        {
                //                            dggrid.Items[index].Cells[k].CssClass = "yellowrow";
                //                        }
                //                    }                                   
                //                }
                //            }
                //            break;
                //        case "assigned":
                //            for (int k = 0; k < dggrid.Columns.Count; k++)
                //            {
                //                dggrid.Columns[k].ItemStyle.CssClass = "greenrow";
                //            }
                //            break;
                //        case "pickedup":
                //            for (int k = 0; k < dggrid.Columns.Count; k++)
                //            {
                //                dggrid.Columns[k].ItemStyle.CssClass = "skybluerow";
                //            }
                //            break;
                //        case "loaded in yard":
                //            for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                //            {
                //                if (index % 2 == 0)
                //                {
                //                    for (int k = 0; k < dggrid.Columns.Count; k++)
                //                    {
                //                        dggrid.Items[index].Cells[k].CssClass = "GridItem";
                //                    }
                //                }
                //                else
                //                {
                //                    for (int k = 0; k < dggrid.Columns.Count; k++)
                //                    {
                //                        dggrid.Items[index].Cells[k].CssClass = "GridAltItem";
                //                    }
                //                }
                //            }
                //            break;
                //        case "en-route":
                //            for (int k = 0; k < dggrid.Columns.Count; k++)
                //            {
                //                dggrid.Columns[k].ItemStyle.CssClass = "yellowrow";
                //            }
                //            break;                            
                //        case "drop in warehouse":
                //            for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                //            {
                //                if (index % 2 == 0)
                //                {
                //                    for (int k = 0; k < dggrid.Columns.Count; k++)
                //                    {
                //                        dggrid.Items[index].Cells[k].CssClass = "GridItem";
                //                    }
                //                }
                //                else
                //                {
                //                    for (int k = 0; k < dggrid.Columns.Count; k++)
                //                    {
                //                        dggrid.Items[index].Cells[k].CssClass = "GridAltItem";
                //                    }
                //                }
                //            }
                //            break;
                //        case "driver on waiting":
                //            for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                //            {
                //                if (index % 2 == 0)
                //                {
                //                    for (int k = 0; k < dggrid.Columns.Count; k++)
                //                    {
                //                        dggrid.Items[index].Cells[k].CssClass = "GridItem";
                //                    }
                //                }
                //                else
                //                {
                //                    for (int k = 0; k < dggrid.Columns.Count; k++)
                //                    {
                //                        dggrid.Items[index].Cells[k].CssClass = "GridAltItem";
                //                    }
                //                }
                //            }
                //            break;
                //        case "delivered":
                //            for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                //            {
                //                if (index % 2 == 0)
                //                {
                //                    for (int k = 0; k < dggrid.Columns.Count; k++)
                //                    {
                //                        dggrid.Items[index].Cells[k].CssClass = "GridItem";
                //                    }
                //                }
                //                else
                //                {
                //                    for (int k = 0; k < dggrid.Columns.Count; k++)
                //                    {
                //                        dggrid.Items[index].Cells[k].CssClass = "GridAltItem";
                //                    }
                //                }
                //            }
                //            break;
                //        case "empty in yard":
                //            for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                //            {
                //                if (index % 2 == 0)
                //                {
                //                    for (int k = 0; k < dggrid.Columns.Count; k++)
                //                    {
                //                        dggrid.Items[index].Cells[k].CssClass = "GridItem";
                //                    }
                //                }
                //                else
                //                {
                //                    for (int k = 0; k < dggrid.Columns.Count; k++)
                //                    {
                //                        dggrid.Items[index].Cells[k].CssClass = "GridAltItem";
                //                    }
                //                }
                //            }
                //            break;
                //        case "terminated":
                //            for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                //            {
                //                if (index % 2 == 0)
                //                {
                //                    for (int k = 0; k < dggrid.Columns.Count; k++)
                //                    {
                //                        dggrid.Items[index].Cells[k].CssClass = "GridItem";
                //                    }
                //                }
                //                else
                //                {
                //                    for (int k = 0; k < dggrid.Columns.Count; k++)
                //                    {
                //                        dggrid.Items[index].Cells[k].CssClass = "GridAltItem";
                //                    }
                //                }
                //            }
                //            break;
                //        case "paperwork pending":
                //            for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                //            {
                //                if (index % 2 == 0)
                //                {
                //                    for (int k = 0; k < dggrid.Columns.Count; k++)
                //                    {
                //                        dggrid.Items[index].Cells[k].CssClass = "GridItem";
                //                    }
                //                }
                //                else
                //                {
                //                    for (int k = 0; k < dggrid.Columns.Count; k++)
                //                    {
                //                        dggrid.Items[index].Cells[k].CssClass = "GridAltItem";
                //                    }
                //                }
                //            }
                //            break;
                //        case "closed":
                //            for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                //            {
                //                if (index % 2 == 0)
                //                {
                //                    for (int k = 0; k < dggrid.Columns.Count; k++)
                //                    {
                //                        dggrid.Items[index].Cells[k].CssClass = "GridItem";
                //                    }
                //                }
                //                else
                //                {
                //                    for (int k = 0; k < dggrid.Columns.Count; k++)
                //                    {
                //                        dggrid.Items[index].Cells[k].CssClass = "GridAltItem";
                //                    }
                //                }
                //            }
                //            break;
                //        case "transit":
                //            for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                //            {
                //                if (index % 2 == 0)
                //                {
                //                    for (int k = 0; k < dggrid.Columns.Count; k++)
                //                    {
                //                        dggrid.Items[index].Cells[k].CssClass = "GridItem";
                //                    }
                //                }
                //                else
                //                {
                //                    for (int k = 0; k < dggrid.Columns.Count; k++)
                //                    {
                //                        dggrid.Items[index].Cells[k].CssClass = "GridAltItem";
                //                    }
                //                }
                //                if (Convert.ToString(ds.Tables[0].Rows[index]["Status"]).Trim().Length > 0)
                //                {
                //                    switch (Convert.ToString(ds.Tables[0].Rows[index]["Status"]).Trim().ToLower())
                //                    {
                //                        case "assigned":
                //                            for (int k = 0; k < dggrid.Columns.Count; k++)
                //                            {
                //                                dggrid.Items[index].Cells[k].CssClass = "greenrow";
                //                            }
                //                            break;
                //                        case "pickedup":
                //                            for (int k = 0; k < dggrid.Columns.Count; k++)
                //                            {
                //                                dggrid.Items[index].Cells[k].CssClass = "skybluerow";
                //                            }
                //                            break;                                        
                //                        case "en-route":
                //                            for (int k = 0; k < dggrid.Columns.Count; k++)
                //                            {
                //                                dggrid.Items[index].Cells[k].CssClass = "yellowrow";
                //                            }
                //                            break;                                                                                                                                                                                                                                 
                //                    }
                //                }
                //            }
                //            break;
                //        default: break;
                //    }
                //}
                #endregion
                #region Column Width adjustment
                //if (dggrid.Columns.Count > 0)
                //{
                //    for (int i = 0; i < dggrid.Columns.Count; i++)
                //    {
                //        if (string.Compare(dggrid.Columns[i].HeaderText, "Delivery Appt.", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
                //            string.Compare(dggrid.Columns[i].HeaderText, "Pickup", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
                //            string.Compare(dggrid.Columns[i].HeaderText, "Last Free Date", true, System.Globalization.CultureInfo.CurrentCulture) == 0 || 
                //            string.Compare(dggrid.Columns[i].HeaderText, "Date & Time", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
                //            string.Compare(dggrid.Columns[i].HeaderText, "Appt. Date", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
                //            string.Compare(dggrid.Columns[i].HeaderText, "Delivered Date", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
                //            string.Compare(dggrid.Columns[i].HeaderText, "Terminated Date", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                //        {
                //            dggrid.Columns[i].ItemStyle.Width = 80;
                //        }
                //        if (string.Compare(dggrid.Columns[i].HeaderText, "Load", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                //            dggrid.Columns[i].ItemStyle.Width = 120;
                //        if (string.Compare(dggrid.Columns[i].HeaderText, "Days In Transit", true, System.Globalization.CultureInfo.CurrentCulture) == 0 ||
                //            string.Compare(dggrid.Columns[i].HeaderText, "Days In Yard", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                //            dggrid.Columns[i].ItemStyle.Width = 40;
                //    }
                //}
                #endregion
                #endregion
                //if (dtFinal != null) { dtFinal.Dispose(); dtFinal = null; }
            }
            if (ds != null) { ds.Dispose(); ds = null; }
        }
    }
    protected bool IsColumnVisible(string strColName,string[] strVisibleColumnsList)
    {        
        bool blVal = false;
        for (int i = 0; i < strVisibleColumnsList.Length; i++)
        {
            if (string.Compare(strVisibleColumnsList[i], strColName, true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                blVal = true;
                break;
            }
        }        
        return blVal;      
    }   
    protected void dggrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        string[] strID = e.Item.Cells[0].Text.Trim().Split('^');
        switch (e.CommandName.ToLower())
        {            
            case "delete":               
                if (ViewState["DeleteTablesList"] != null)
                {
                    string[] DeleteTables = (string[])ViewState["DeleteTablesList"];
                    if (DeleteTables.Length > 0)
                    {
                        #region Check Dependencies
                        if (string.Compare(Convert.ToString(ViewState["LoadStatus"]), "New", true, System.Globalization.CultureInfo.CurrentCulture) != 0)
                        {
                            bool blCanWeDelete = true;
                            if (ViewState["DependentTablesList"] != null)
                            {
                                string[] DependentTables = (string[])ViewState["DependentTablesList"];
                                string[] DependentTableKeys = (string[])ViewState["DependentTableKeysList"];
                                if (DependentTables.Length == DependentTableKeys.Length)
                                {
                                    string strQuery = "";
                                    for (int i = 0; i < DependentTableKeys.Length; i++)
                                    {
                                        strQuery = "Select Count(" + DependentTableKeys[i].Trim() + ") from " + DependentTables[i].Trim() + " where " + DependentTableKeys[i].Trim() + "=" + strID[0];
                                        if (Convert.ToInt32(DBClass.executeScalar(strQuery)) > 0)
                                        {
                                            blCanWeDelete = false;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    blCanWeDelete = false;
                                }
                                DependentTables = null; DependentTableKeys = null;
                                if (!blCanWeDelete)
                                {
                                    Response.Write("<script>alert('Unable to delete due to dependencies.')</script>");
                                    return;
                                }
                            }
                        }
                        #endregion
                        #region Delete
                        string strWhereClause = "";
                        string[] strQueries = new string[DeleteTables.Length];
                        if (ViewState["DeleteTablePKList"] == null)
                        {
                            strWhereClause = " where " + Convert.ToString(ViewState["Primarykey"]).Trim() + " = " + strID[0];
                            for (int i = 0; i < DeleteTables.Length; i++)
                            {
                                strQueries[i] = "delete from " + DeleteTables[i] + strWhereClause;
                            }
                        }
                        else
                        {
                            string[] DeleteTablesPK = (string[])ViewState["DeleteTablePKList"];
                            for (int i = 0; i < DeleteTables.Length; i++)
                            {
                                strWhereClause = " where " + DeleteTablesPK[i] + " = " + strID[0];
                                strQueries[i] = "delete from " + DeleteTables[i] + strWhereClause;
                            }
                        }
                        if (DBClass.executeQueries(strQueries) > 0)
                        {
                            Session["PaymentGridPageIndex"] = 0;
                            BindGrid(0);
                        }
                        strQueries = null;
                        #endregion
                    }
                    DeleteTables = null;
                    strID = null;
                }                
                break;
            default: break;                
        }
        strID = null;
    }
    protected void dggrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        Session["PaymentGridPageIndex"] = e.NewPageIndex;
        BindGrid(e.NewPageIndex);
    }
}
public class PaymentGridTemplate : System.Web.UI.Page, ITemplate
{
    ListItemType templateType;
    bool blDelete = false;
    string strColName = "";
    string[] strLastColumnLinksList = null;
    string[] strLastColumnPagesList = null;
    DataTable dt = null;
    string strLoadStatus = "";
    int index = -1;
    public PaymentGridTemplate(ListItemType itype, string ColName, DataTable pDt, string strPLoadStatus)
    {
        templateType = itype;
        strColName = ColName;
        dt = pDt;
        Session["LoadManagerIndex"] = -1;
        strLoadStatus = strPLoadStatus;
    }
    public PaymentGridTemplate(ListItemType itype, string ColName, DataTable pDt, bool bDelete, string strPLoadStatus)
    {
        templateType = itype;
        strColName = ColName;
        blDelete = bDelete;
        dt = pDt; Session["LoadManagerIndex"] = -1;
        strLoadStatus = strPLoadStatus;
    }
    public PaymentGridTemplate(ListItemType itype, string ColName, string[] pLastColumnLinksList, string[] pLastColumnPagesList, DataTable pDt, string strPLoadStatus)
    {
        templateType = itype;
        strColName = ColName;
        strLastColumnLinksList = pLastColumnLinksList;
        strLastColumnPagesList = pLastColumnPagesList;
        dt = pDt;
        Session["LoadManagerIndex"] = -1;
        strLoadStatus = strPLoadStatus;
    }
    public void InstantiateIn(System.Web.UI.Control container)
    {
        if(Session["LoadManagerIndex"] != null)
            index = Convert.ToInt32(Session["LoadManagerIndex"]);
        Literal lc = new Literal();
        switch (templateType)
        {
            #region Header
            case ListItemType.Header:                
                lc.Text = "<B>Edit</B>";
                LinkButton lb = new LinkButton();
                lb.Text = "Edit";
                lb.CommandName = "EditButton";
                container.Controls.Add(lb);
                container.Controls.Add(lc);
                lc = null; lb = null;
                break;
            #endregion
            case ListItemType.Item:
                switch (strColName.ToLower())
                {
                    case "load":
                        #region load 
                        LinkButton lnkload = new LinkButton();
                        if (Session["Role"] != null)
                        {
                            if(string.Compare(Convert.ToString(Session["Role"]),"Customer",true,System.Globalization.CultureInfo.CurrentCulture)==0)
                                lnkload.PostBackUrl = "~/Customer/CustomerTrackLoad.aspx?" + Convert.ToString(dt.Rows[index + 1]["Load"]);
                            else if (string.Compare(Convert.ToString(Session["Role"]), "Driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                            {
                                lnkload.PostBackUrl = "~/Driver/DriverTrackLoad.aspx?" + Convert.ToString(dt.Rows[index + 1]["Load"]);
                            }
                            else if (string.Compare(Convert.ToString(Session["Role"]), "Carrier", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                            {
                                lnkload.PostBackUrl = "~/Carrier/CarrierTrackLoad.aspx?" + Convert.ToString(dt.Rows[index + 1]["Load"]);
                            }
                        }                        
                        lnkload.Text = Convert.ToString(dt.Rows[index + 1]["Load"]);
                        lnkload.ForeColor = System.Drawing.Color.Red;
                        container.Controls.Add(lnkload);
                        lnkload = null;
                        container.Controls.Add(new LiteralControl("<table width=90px><tr><td></td></tr></table>"));
                        container.Controls.Add(new LiteralControl("<font color=black font-size=12>" + Convert.ToString(dt.Rows[index + 1]["LoadStatus"]).Trim() + "</font>"));                        
                        index = index + 1;
                        Session["LoadManagerIndex"] = index;
                        break;
                        #endregion
                    case "lastcolumn":
                        #region Last Column
                        for (int i = 0; i < strLastColumnLinksList.Length; i++)
                        {
                            LinkButton lnkTemp = new LinkButton();
                            lnkTemp.PostBackUrl = strLastColumnPagesList[i] + "?" + Convert.ToString(dt.Rows[index]["ID"]).Trim();
                            lnkTemp.Text = strLastColumnLinksList[i];
                            lnkTemp.ForeColor = System.Drawing.Color.Red;
                            container.Controls.Add(lnkTemp);
                            lnkTemp = null;
                            if (strLastColumnLinksList.Length > 0)
                                container.Controls.Add(new LiteralControl("<br/><br/>"));
                        }
                        break;
                        #endregion
                    case "origin":
                        #region origin
                        FormatMultipleRowColumns(Convert.ToString(dt.Rows[index]["Origin"]), "black", ref container);
                        break;
                        #endregion
                    case "destination":
                        #region destination
                        FormatMultipleRowColumns(Convert.ToString(dt.Rows[index]["Destination"]), "black", ref container);
                        break;
                        #endregion
                    case "delivered":
                        #region delivered
                        FormatMultipleRowColumns(Convert.ToString(dt.Rows[index]["Delivered"]), "black", ref container);
                        break;
                        #endregion    
                    case "gross":
                        #region gross
                        container.Controls.Add(new LiteralControl("<font font-size=12; color=black>" + Convert.ToString(dt.Rows[index]["gross"]) + "</font>"));
                        break;
                        #endregion
                    case "charges":
                        #region gross
                        container.Controls.Add(new LiteralControl("<font font-size=12; color=black>" + Convert.ToString(dt.Rows[index]["charges"]) + "</font>"));
                        break;
                        #endregion
                    case "lumper":
                        #region lumper
                        container.Controls.Add(new LiteralControl("<font font-size=12; color=black>" + Convert.ToString(dt.Rows[index]["lumper"]) + "</font>"));
                        break;
                        #endregion
                    case "dtn.":
                        #region dtn.
                        container.Controls.Add(new LiteralControl("<font font-size=12; color=black>" + Convert.ToString(dt.Rows[index]["dtn."]) + "</font>"));
                        break;
                        #endregion
                    case "other":
                        #region other
                        container.Controls.Add(new LiteralControl("<font font-size=12; color=black>" + Convert.ToString(dt.Rows[index]["other"]) + "</font>"));
                        break;
                        #endregion
                    case "adv.":
                        #region adv.
                        container.Controls.Add(new LiteralControl("<font font-size=12; color=black>" + Convert.ToString(dt.Rows[index]["adv."]) + "</font>"));
                        break;
                        #endregion
                    case "total":
                        #region total
                        container.Controls.Add(new LiteralControl("<font font-size=12; color=black>" + Convert.ToString(dt.Rows[index]["total"]) + "</font>"));
                        break;
                        #endregion                                                       
                    case "invoice no#":
                        #region Invoice No#
                        container.Controls.Add(new LiteralControl("<font font-size=12; color=black>" + Convert.ToString(dt.Rows[index]["InvoiceNo"]) + "</font>"));
                        container.Controls.Add(new LiteralControl("<br/>"));
                        //container.Controls.Add(new LiteralControl("<a href='javascript:window.open('ViewDocuments.aspx?ID=" + Convert.ToString(dt.Rows[index]["Load"]) + "','width:500')'> P.O.D </a>"));
                        container.Controls.Add(new LiteralControl("<a href='ViewDocuments.aspx?ID=" + Convert.ToString(dt.Rows[index]["Load"]) + "' target='_blank'> P.O.D </a>"));
                        //LinkButton lnkInv = new LinkButton();
                        //lnkInv.PostBackUrl = "~/Carrier/ViewDocuments.aspx?ID=" + Convert.ToString(dt.Rows[index]["Load"]);
                        //lnkInv.Text = "P.O.D";
                        //lnkInv.ForeColor = System.Drawing.Color.Red;
                        //container.Controls.Add(lnkInv);
                        //lnkInv = null;                       
                        break;
                        #endregion
                    case "tag no#":
                        #region Tag No#
                        container.Controls.Add(new LiteralControl("<font font-size=12; color=black>" + Convert.ToString(dt.Rows[index]["TagNo"]) + "</font>"));
                        container.Controls.Add(new LiteralControl("<br/>"));
                        container.Controls.Add(new LiteralControl("<a href='ViewDocuments.aspx?ID=" + Convert.ToString(dt.Rows[index]["Load"]) + "' target='_blank'> P.O.D </a>"));
                        //LinkButton lnkTag = new LinkButton();
                        //lnkTag.PostBackUrl = "";
                        //lnkTag.Text = "P.O.D";
                        //lnkTag.ForeColor = System.Drawing.Color.Red;
                        //container.Controls.Add(lnkTag);
                        //lnkTag = null;
                        break;
                        #endregion
                   default: break;                        
                }                
                break;
            #region EditItem and Footer
            case ListItemType.EditItem:
                TextBox tb = new TextBox();
                tb.Text = "";
                container.Controls.Add(tb);
                break;
            case ListItemType.Footer:
                lc.Text = "<I>Edit</I>";
                container.Controls.Add(lc);
                break;
            #endregion
        }
    }
    public void FormatMultipleRowColumns(string strColValue,string strColor,ref System.Web.UI.Control container)
    {
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0)
        {
            string strFormat = "<table border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length -1))+ "px;border-color:Black;>";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (i == 0)
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;>" + strMuls[i].Trim() + "</td></tr>";
                else
                {
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;>" + strMuls[i].Trim() + "</td></tr>";
                }
            }
            strFormat += "</table>";
            container.Controls.Add(new LiteralControl(strFormat));
        }
        strMuls = null;
    }
    public void FormatMultipleRowColumnsWithBold(string strColValue, string strColor, ref System.Web.UI.Control container)
    {
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0)
        {
            string strFormat = "<table border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;border-color:Black;>";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (i == 0)
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + strMuls[i].Trim() + "</b></td></tr>";
                else
                {
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + strMuls[i].Trim() + "</b></td></tr>";
                }
            }
            strFormat += "</table>";
            container.Controls.Add(new LiteralControl(strFormat));
        }
        strMuls = null;
    }
    public void FormatMultipleRowColumnsWithImages(string strColValue,string strColor,string strImagePath, ref System.Web.UI.Control container)
    {
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0)
        {
            string strFormat = "<table border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;>";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (i == 0)
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + strMuls[i].Trim() + "<b/><br/><img src=" + strImagePath + " width=28 height=26 align=middle/></td></tr>";
                else
                {
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + strMuls[i].Trim() + "<b/><br/><img src=" + strImagePath + " width=28 height=26 align=middle/></td></tr>";
                }
            }
            strFormat += "</table>";
            container.Controls.Add(new LiteralControl(strFormat));
        }
        strMuls = null;
    }
    public void FormatMultipleRowColumnDatesWithImages(string strColValue, string strColor,string strStatus, ref System.Web.UI.Control container)
    {
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0)
        {
            string strFormat = "<table border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;>";
            string strImagePath = "";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (Convert.ToString(strMuls[i]).Trim().Length > 0)
                {
                    if (string.Compare(strStatus, "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                    {
                        if (Convert.ToDateTime(strMuls[i].Trim()).Date <= DateTime.Now.Date)
                        {
                            strImagePath = "../images/bulb-pink-icon.gif";
                        }
                        else if (Convert.ToDateTime(strMuls[i].ToString().Trim()).Date == DateTime.Now.Date.AddDays(1))
                        {
                            strImagePath = "../images/bulb-icon.gif";
                        }
                        else
                        {
                            strImagePath = "";
                        }
                    }
                    else if (string.Compare(strStatus, "Assigned", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                    {
                        strImagePath = "../images/bulb-yellow-icon.gif";
                    }
                    else
                    {
                        strImagePath = "";
                    }
                    if (strImagePath.Trim().Length > 0)
                    {
                        if (i == 0)
                            strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + strMuls[i].Trim() + "<b/><br/><img src=" + strImagePath + " width=28 height=26 align=middle/></td></tr>";
                        else
                        {
                            strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                            strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + strMuls[i].Trim() + "<b/><br/><img src=" + strImagePath + " width=28 height=26 align=middle/></td></tr>";
                        }
                    }
                    else
                    {
                        if (i == 0)
                            strFormat += "<tr><td  width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + strMuls[i].Trim() + "<b/></td></tr>";
                        else
                        {
                            strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                            strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><b>" + strMuls[i].Trim() + "<b/></td></tr>";
                        }
                    }
                }
                else
                {
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:" + strColor + ";font-size=12;height:90px;><br/></td></tr>";
                }                
            }
            strFormat += "</table>";
            container.Controls.Add(new LiteralControl(strFormat));
        }
        strMuls = null;
    }
    public void FormatSingleRowColumnsWithImages(string strColValue, string strImagePath, ref System.Web.UI.Control container)
    {
        container.Controls.Add(new LiteralControl("<b><font color=red>" + strColValue.Trim() + "</font><b><br/><img src=" + strImagePath + " width=28 height=26 align=middle/>"));
    }
}