using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString.Count > 0 && Request.QueryString["LoadId"] != null)
            {
                HiddenField1.Value = Request.QueryString["LoadId"];
                GetLoadDetails(Convert.ToInt64(Request.QueryString["LoadId"]));
            }
        }
    }

    private void GetLoadDetails(long LoadId)
    {
        DataTable dt = SqlHelper.ExecuteDataset(System.Configuration.ConfigurationManager.ConnectionStrings["eTransportString"].ConnectionString, "SP_GetTCardLoadDetails", new object[] { LoadId }).Tables[0];
        if (dt != null && dt.Rows.Count > 0)
        {
            lblLoadId1.Text = lblLoad.Text = Convert.ToString(dt.Rows[0][0]);
            lblContainer1.Text = lblContainer.Text = Convert.ToString(dt.Rows[0][1]);
            try
            {
                DateTime dtTime = Convert.ToDateTime(Convert.ToString(dt.Rows[0][2]));
                lblApptDate1.Text = lblApptDate.Text = dtTime.ToShortDateString();
                lblTime1.Text = lblTime.Text = dtTime.ToShortTimeString();
            }
            catch
            {
            }
            lblChassis1.Text = lblChassis.Text = Convert.ToString(dt.Rows[0][3]);
            lblPhone1.Text = lblPhone.Text = Convert.ToString(dt.Rows[0][4]);
            lblName1.Text = lblName.Text = Convert.ToString(dt.Rows[0][5]);
            lblShipper1.Text = lblshipper.Text = Convert.ToString(dt.Rows[0][6]);
            lblAddress1.Text = lblAddress.Text = Convert.ToString(dt.Rows[0][7]);
            lblCity1.Text = lblCity.Text = Convert.ToString(dt.Rows[0][8]);
            lblBooking1.Text = lblbooking.Text = Convert.ToString(dt.Rows[0][10]);
            lblPickup1.Text = lblPickup.Text = Convert.ToString(dt.Rows[0][11]);
            lblDest1.Text = lblDest.Text = Convert.ToString(dt.Rows[0][12]);
            lblConsignee1.Text = lblConsignee.Text = Convert.ToString(dt.Rows[0][13]);
            lblLocation1.Text = lblLocation.Text = Convert.ToString(dt.Rows[0][14]);
            lblRamp1.Text = lblRamp.Text = Convert.ToString(dt.Rows[0][15]);
            lblPCS1.Text = lblPCS.Text = Convert.ToString(dt.Rows[0][16]);
            lblWT1.Text = lblWT.Text = Convert.ToString(dt.Rows[0][17]);
            lblSeal1.Text = lblSeal.Text = Convert.ToString(dt.Rows[0][18]);
            lblTerminated1.Text = lblTerminate.Text = Convert.ToString(dt.Rows[0][19]);
            lblBillTo1.Text = lblBillTo.Text = Convert.ToString(dt.Rows[0][20]);
            lblRate1.Text = lblRate.Text = Convert.ToString(dt.Rows[0][21]);
            lblTotal1.Text = lbltotal.Text = Convert.ToString(dt.Rows[0][22]);
            lblRef1.Text = lblRef.Text = Convert.ToString(dt.Rows[0][23]); 
        }
        if (dt != null) { dt.Dispose(); dt = null; }
    }
}

