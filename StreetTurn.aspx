﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StreetTurn.aspx.cs" Inherits="StreetTurn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta charset="utf-8">
<title>S Line Transportation Inc.</title>
<style>
*{ margin:0; padding:0;}
body { width: 100%;}
*{ margin:0; padding:0;}
body{ font-family:Arial, Helvetica, sans-serif; font-size:12px;}
.header{ background:url(images/bodybgr.gif) top repeat-x; width:100%; height:105px;}
.header a img{ display:inline-block; border:none; text-decoration:none;}
.header span{ vertical-align:top; display:inline-block; margin-top:34px; margin-left:10px; font-size:18px; color:#800511;}
.container:before, .container:after{ content:''; clear:both;}
.container{ clear:both; text-align: center; padding-bottom:40px;}
.container .top{ background:#5B97C8; padding:30px 20px 20px; border-bottom:1px solid #999; font-size:16px; color:#fff;}
.container .top  h3, .container .top p{ margin:0 0 10px 0;}
.container .top  h3{ font-size:16px;}
.container .top  h3 a{color:#fff;}
.container .top  h3 a:hover{ color:#000;}
.container .top p{ font-size:14px;}
.mar-t-20{ margin-top:20px; }
.pad-20{padding:10px 20px;}
.street-reg{ margin-top:20px; font-size:14px; font-weight:600;}
.street-reg p{ margin-bottom:3px;}
.footer{ padding:10px 20px ; background:#F2F2F2; font-size:11px; position:fixed; bottom:0; left:0; right:0; border-top:1px solid #eee;}
table { margin: 0 auto;}
table th{background: #5B97C8; color:#fff;}
table th, table td{ padding:10px;}
table tr td:first-child{ background:#eee;}

</style>
</head>
<body>
<div class="header">
<a href="#"><img src="images/logo.gif"></a>
<span><strong>Street containers available at S LINE TRANSPORTATION INC.</strong>  </span>
</div>
<div class="container">
<div class="top">
<h3>Email us the list of containers you want to street turn from our company to <a href="mailto:streetturn@slinetransport.com">streetturn@slinetransport.com</a></h3>
<p class="mar-t-20">Below list will be updated every 1 hour from our system. Please keep  checking below list. This will help our company as well as your company to save our drivers' time. </p>
</div>
<div class="pad-20">
    <form id="form1" runat="server">

    <div><asp:GridView ID="gridviewContainers" runat="server" AllowPaging="false">

    </asp:GridView></div>
    <div>
        <asp:Label ID="lblDisplay" runat="server" Text="Label"></asp:Label></div>
    </form>
<div class="street-reg">
<br />
<p>All containers are in our yard located: </p>
<br/><br />
<p>1 Market Street </p>
<p>Oakland CA 94607</p>
<p>Tel: 510-3425066</p>
</div>


</div>
</div>
<div class="footer">
<p>&copy; Copyright 2016 S Line Transportation Inc. All rights reserved.</p>
</div>
</body>
</html>
