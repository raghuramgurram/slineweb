using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CarrierLoadCharges : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Load Charges";
        if (!IsPostBack)
        {
            string BackPage = "";
            if (Request.QueryString.Count > 0 && Session["CarrierId"] != null)
            {
                try
                {
                    ViewState["LoadId"] = Convert.ToInt64(Request.QueryString[0]);
                    LoadPayments1.LoadId = Convert.ToString(ViewState["LoadId"]);
                    LoadPayments1.PlayerId = Convert.ToString(Session["CarrierId"]);
                    lblBarText.Text = "Load Id : " + Convert.ToString(ViewState["LoadId"]);
                    if (Session["RedirectBackURL"] != null && ViewState["LoadId"] != null)
                    {
                        BackPage = Convert.ToString(Session["RedirectBackURL"]);
                        Session["RedirectBackURL"] = null;
                        LoadPayments1.RedirectBackURL = BackPage + Convert.ToInt64(ViewState["LoadId"]);
                    }
                    else
                        LoadPayments1.RedirectBackURL = Convert.ToString(("~/Carrier/CarrierTrackLoad.aspx?" + Convert.ToInt64(ViewState["LoadId"])));
                }
                catch
                {
                }
            }
        }
    }
}
