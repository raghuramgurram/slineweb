<%@ Page Language="C#" MasterPageFile="~/Mobile/eTransportMobile.master" AutoEventWireup="true" CodeFile="MyLoads.aspx.cs" Inherits="Mobile_MyLoads" Title="Untitled Page" %>
<%@ Register Src="MobileMenu.ascx" TagName="Menu"
    TagPrefix="mu" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<mu:Menu ID="MuPageMenu" runat="server" />
   <div class="main-wraper">
        
        <asp:Repeater ID="rptLoads" runat="server" OnItemDataBound="rptLoads_databind">
        <HeaderTemplate>
        </HeaderTemplate>
        <ItemTemplate>
         <div class="main-content bor">
          <div class="left-conte">
            <ul>
              <li> <span class="load-number"><%# Eval("bint_LoadId").ToString()%></span> <br>
                <span class="load-id"><%# Eval("nvar_Container").ToString()%></span> <br>
                <asp:HiddenField ID="hfealdloadid" runat="server" Value='<%# Eval("bint_LoadId").ToString()%>' />
                <asp:Repeater ID="rpeordate" runat="server" OnItemDataBound="rpeordate_databind" >
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemTemplate>
                <span class="load-apt">APT: <asp:Literal ID="ltlAPTdatetime" runat="server"></asp:Literal>              
                <asp:HiddenField ID="hfealdforaptdatetime" runat="server" Value='<%# Eval("date_DeliveryAppointmentDate").ToString()%>' />
                <asp:HiddenField ID="hfealdfortoaptdatetime" runat="server" Value='<%# Eval("date_DeliveryAppointmentToDate").ToString()%>' />
                </span> <br>
                </ItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
                </asp:Repeater>
                

                <span class="load-lfd">LFD: <asp:Literal ID="ltlLFDdatetime" runat="server" Text='<%# Eval("date_LastFreeDate").ToString()%> '></asp:Literal>  
                </span> <br>
                <span class="status">Status:
                 <asp:HiddenField ID="hfldstatusid" runat="server" Value='<%# Eval("bint_LoadStatusId").ToString()%>' />
                <asp:Literal ID="ltlvalueforstatus" runat="server"></asp:Literal>
               
                </span> <br>
              </li>
            </ul>

             <asp:LinkButton ID="lnkloadetails" data-role="button" data-theme="a" data-icon="arrow-r" data-iconpos="right"
          data-transition="flip" rel="external" runat="server" OnClick="lnkloadetails_click" CommandArgument='<%# Eval("bint_LoadId").ToString()%>'>
        Load Details</asp:LinkButton>
        <asp:LinkButton ID="lnkupdateloadestatus" runat='server' data-role="button" data-theme="e" data-icon="arrow-r"
        data-iconpos="right"  data-transition="flip" rel="external" OnClick="lnkupdateloadestatus_click" CommandArgument='<%# Eval("bint_LoadId").ToString()%>' > Update Load Status </asp:LinkButton>
        <asp:LinkButton ID="lnkuploaddocuments" runat="server" data-role="button" data-theme="b" data-icon="arrow-r"
        data-iconpos="right"  data-transition="flip" rel="external" OnClick="lnkuploaddocuments_click" CommandArgument='<%# Eval("bint_LoadId").ToString()%>'> Upload Documents </asp:LinkButton>
     </div>
        </div>
        </ItemTemplate>
        <FooterTemplate>
        </FooterTemplate>
        </asp:Repeater>
      </div>
</asp:Content>


