<%@ Page AutoEventWireup="true" CodeFile="NewShipping.aspx.cs" Inherits="NewShippingLine"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/USState.ascx" TagName="USState" TagPrefix="uc5" %>
<%@ Register Src="~/UserControls/LinksBar.ascx" TagName="LinksBar" TagPrefix="uc6" %>

<%@ Register Src="~/UserControls/Url.ascx" TagName="Url" TagPrefix="uc4" %>

<%@ Register Src="~/UserControls/Phone.ascx" TagName="Phone" TagPrefix="uc2" %>

<%@ Register Src="~/UserControls/Grid.ascx" TagName="Grid" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/MsgDispBar.ascx" TagName="GridBar" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
  <tr valign="top">
    <td width="100%">
        <table id="tblform1" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <asp:ValidationSummary ID="ReqCarrier" runat="server" Width="526px" />
                <asp:Label ID="lblNameError" runat="server" Font-Bold="False" ForeColor="Red" Text="Shipping line name already exists." Visible="False"/></td>
            </tr>
        </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="50%" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr valign="top">
                <td width="49%"><table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblform">
                  <tr>
                    <th colspan="2">Address Details</th>
                  </tr>
                  <tr id="row">
                    <td width="50%"><asp:Label ID="lblShippingLine" runat="server" Text="Shipping Line:"/>
                      <br /><asp:TextBox ID="txtShippingLine" runat="Server" MaxLength="150"/></td>
                    <td>
                      <asp:CheckBox ID="chkActive" runat="server" Text=" Active" TextAlign="Right" Width="65px" Checked="True"/>
                    </td>
                  </tr>                  
                  <tr id="altrow">
                    <td valign="top"><asp:Label ID="lblPier" runat="server" Text="Pier Termination / Empty Return:"/><br />
                    <asp:TextBox ID="txtPier" runat="Server" MaxLength="100"/></td>
                    <td><asp:Label ID="lblPhone" runat="server" Text="Phone :" /> <br />
                      <%--<asp:TextBox ID="txtPhone" runat="Server" MaxLength="18"/>
                        <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ControlToValidate="txtPhone"
                            Display="Dynamic" ErrorMessage="Please Enter Phone Number.">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revPhone" runat="server" ControlToValidate="txtPhone"
                            Display="Dynamic" ErrorMessage="Please enter a valid Phone number in the format (123) 123-1234 or 123-123-1234."
                            ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">*</asp:RegularExpressionValidator>--%>
                        <uc2:Phone ID="txtPhone" runat="server" IsRequiredField="true" />
                    </td>
                  </tr>
                  
                  <tr id="row">
                    <td>
                        <asp:RequiredFieldValidator ID="rfvStreet" runat="server" ControlToValidate="txtStreet"
                            Display="Dynamic" ErrorMessage="Please Enter Street.">*</asp:RequiredFieldValidator>
                        <asp:Label ID="lblStreet" runat="server" Text="Street :" /><br />
                     <asp:TextBox ID="txtStreet" runat="Server" MaxLength="200"/></td>
                    <td><asp:Label ID="lblFax" runat="server" Text="Fax :" /><br /><%--
                     <asp:TextBox ID="txtFax" runat="Server" MaxLength="18"/>
                        <asp:RequiredFieldValidator ID="rfvFax" runat="server" ControlToValidate="txtFax"
                            Display="Dynamic" ErrorMessage="Please Enter Fax.">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revFax" runat="server" ControlToValidate="txtFax"
                            Display="Dynamic" ErrorMessage="Please enter a valid Fax number in the format (123) 123-1234 or 123-123-1234."
                            ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">*</asp:RegularExpressionValidator>--%>
                             <uc2:Phone ID="txtFax" runat="server" IsRequiredField="false" />
                      </td>
                  </tr>
                  
                  <tr id="altrow">
                    <td><asp:Label ID="lblSuite" runat="server" Text="Suite# :" /><br />
                     <asp:TextBox ID="txtSuite" runat="Server" MaxLength="100"/></td>
                    <td><asp:Label ID="lblEmail" runat="server" Text="Email :" /><br/>
                        <asp:TextBox ID="txtEmail" runat="Server" MaxLength="100"/>
                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                            Display="Dynamic" ErrorMessage="Please Enter Email.">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                            Display="Dynamic" ErrorMessage="Please Enter A Valid Email." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
                  </tr>
                  
                  <tr id="row">
                    <td valign="top"><asp:Label ID="lblCity" runat="server" Text="City :" /><br />
                     <asp:TextBox ID="txtCity" runat="Server" MaxLength="100"/></td>
                    <td valign="top"><asp:Label ID="lblURL" runat="server" Text="WebSite URL :" /><br />
                   <%--  <asp:TextBox ID="txtURL" runat="Server" MaxLength="100"/>&nbsp;
                        <asp:RegularExpressionValidator ID="REVWebsiteURL" runat="server" ControlToValidate="txtURL"
                            Display="Dynamic" ErrorMessage="Please Enter A Valid URL." ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?">*</asp:RegularExpressionValidator>--%>
                        <uc4:Url ID="txtURL" runat="server" />
                    </td>
                  </tr>

                  <tr id="altrow">
                    <td valign="top"><asp:Label ID="lblState" runat="server" Text="State :" /><br />
                        &nbsp;<uc5:USState ID="ddlState" runat="server" />
                    </td>
                    <td valign="top" style="height: 20px"><asp:Label ID="lblEmptyReturnFreeDays" runat="server" Text="Empty Return Free Days :" /><br />
                     <asp:TextBox ID="txtEmptyReturnFreeDays" runat="Server" MaxLength="5"/>
                     <asp:RegularExpressionValidator ID="revEmptyReturnFreeDays" runat="server" ControlToValidate="txtEmptyReturnFreeDays"
                            Display="Dynamic" ErrorMessage="Please enter only numbers in Empty Return Free Days"
                            ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                     </td>
                  </tr>
                  <tr id="row">
                    <td valign="top" ><asp:Label ID="lblZip" runat="server" Text="Zip :" /><br />
                     <asp:TextBox ID="txtZip" runat="Server" MaxLength="20"/></td>
                    <td valign="top"><asp:Label ID="lblDirections" runat="server" Text="Directions :" /><br />
                     <asp:TextBox ID="txtDirections" runat="Server" Height="74px" TextMode="MultiLine" Width="222px" MaxLength="1000"/></td>
                  </tr>
                </table>
               </td>
                <td width="1%"><img height="1" src="../Images/pix.gif" width="5" /></td>
                <td>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td align="left" valign="top">
                            <uc6:LinksBar ID="lnkBar" runat="server" HeaderText="Contact Persons" LinksList="Add Contacts" />
                        </td>
                      </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                        </tr>
                    </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td><uc3:Grid ID="Grid1" runat="server" />                        
                      </td>
                    </tr>
                  </table>
                 </td>
              </tr>
            </table>
            
          </td>
         </tr>          
        </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
        </tr>
      </table>
     <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
        <tr>
          <td align="right">
              &nbsp;<asp:Button ID="btnSave" runat="server" Height="22px" Text="Save" CssClass="btnstyle" OnClick="btnSave_Click" />
              <asp:Button ID="btnCancle" runat="server" CausesValidation="False" CssClass="btnstyle" Height="22px"
                  Text="Cancel" OnClick="btnCancel_Click" UseSubmitBehavior="False" />
          </td>        
        </tr>
        <tr>
        <td>
            &nbsp;</td>
        </tr>
      </table> 
     </td>
     </tr>
     </table>
      
</asp:Content>

