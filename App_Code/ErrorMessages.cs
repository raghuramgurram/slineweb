﻿using System;
using System.Collections.Generic;
using System.Web;


    public class ErrorMessages
    {
        public static string LOGIN_IS_EMPTY = "Please enter user name and password.";
        public static string LOGIN_IS_NOT_VALID = "Please enter a valid user name and password.";
        public static string LOGIN_IS_NOT_DRIVER = "Only drivers are allowed to login.";

        public static string DETAILS_CHASSIS_OK = "Chassis updated successfully.";
        public static string DETAILS_CHASSIS_ERROR = "Chassis update has failed.";


        public static string CHANGE_STATUS_VALID_DATE = "Select a valid date.";
        public static string CHANGE_STATUS_VALID_TIME = "Select a valid time.";
        public static string CHANGE_STATUS_NO_OPTION = "Please select at least one status option.";
        public static string CHANGE_STATUS_DRIVER_ON_WAITING = "Driver On Waiting status can only be updated when Reached @ Destination status is entered.";
        public static string CHANGE_STATUS_DATE_ERROR = "Date & Time entered must be greater than previous status date & time.";
        public static string CHANGE_STATUS_UPDATE_OK = "Status update successfully.";
        public static string CHANGE_STATUS_UPDATE_ERROR = "Status update failed.";



        public static string FILE_UPLOAD_FILE_TYPE_ERROR = "Select a valid Image.";
        public static string FILE_UPLOAD_NO_FILES_TO_UPLOAD = "There are no images to Upload.";
        public static string FILE_UPLOAD_OK = "File uploaded successfully.";
        public static string FILE_UPLOAD_ERROR = "Error uploading.";


    }