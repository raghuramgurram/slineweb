using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Mobile_details : System.Web.UI.Page
{
    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session[Keys.LOGIN_ID_KEY] == null || Session[Keys.EMPLOY_ID] == null)
        {
            Response.Redirect(Keys.PAGE_LOAGIN);
        }

        if (!IsPostBack)
        {

            if (Session[Keys.LOAD_ID] != null)
            {
                try
                {
                    LoadDetailsBase load = new LoadDetailsBase();
                    DataTable loaddetails = load.loadloaddetails(Convert.ToInt32(Session[Keys.LOAD_ID]));
                    if (loaddetails != null)
                    {
                        SedLoadDetails(loaddetails);
                        MuPageMenu.SubTitle = ConfigurationManager.AppSettings["CompanyName"].ToString() + " - Load Details For Load -" + Session[Keys.LOAD_ID].ToString() + ", container #:" + loaddetails.Rows[0]["nvar_Container"].ToString();
                        MuPageMenu.LoadMenu();
                    }
                }
                catch (Exception ex)
                {

                }

            }
            else
            {
                Response.Redirect(Keys.PAGE_LOAD_HOME);
            }
        }
    }

    protected void SedLoadDetails(DataTable loaddata)
    {
        if (loaddata != null && loaddata.Rows.Count > 0)
        {
            if (loaddata.Rows[0]["nvar_Container"] != null) ltlContainer.Text = loaddata.Rows[0]["nvar_Container"].ToString(); else ltlContainer.Text = "&nbsp;";
            if (loaddata.Rows[0]["nvar_Booking"] != null) ltlBooking.Text = loaddata.Rows[0]["nvar_Booking"].ToString(); else ltlBooking.Text = "&nbsp;";
            if (loaddata.Rows[0]["nvar_Pickup"] != null) ltlPickup.Text = loaddata.Rows[0]["nvar_Pickup"].ToString(); else ltlPickup.Text = "&nbsp;";
            if (loaddata.Rows[0]["nvar_RailBill"] != null) ltlcontainerLocation.Text = loaddata.Rows[0]["nvar_RailBill"].ToString(); else ltlcontainerLocation.Text = "&nbsp;";
            int statue = 0;
            if (loaddata.Rows[0]["bint_LoadStatusId"] != null) statue = Convert.ToInt32(loaddata.Rows[0]["bint_LoadStatusId"].ToString()); else statue = 0;

            loadStatus value = ((loadStatus)statue);

            if (value == loadStatus.Delivered)
            {
                ltlLoadstatus.Text = " <label class='green'> " + value.ToString() + "</label>";
            }
            else
            {

                if (value == loadStatus.En_route)
                    ltlLoadstatus.Text = " <label class='red'> " + value.ToString().Replace("_", "-") + "</label>";
                else
                    ltlLoadstatus.Text = " <label class='red'> " + value.ToString().Replace("_", " ") + "</label>";
            }

            if (loaddata.Rows[0]["nvar_Chasis"] != null)
            {
                if (!string.IsNullOrEmpty(loaddata.Rows[0]["nvar_Chasis"].ToString()))
                {
                    ltlChassiss.Text = loaddata.Rows[0]["nvar_Chasis"].ToString();
                    txtchassisnumber.Text = loaddata.Rows[0]["nvar_Chasis"].ToString();
                    txtchassisnumber.Style.Clear();
                    txtchassisnumber.Style.Add("display", "none");
                    txtchassisnumber.Style.Add("width", "20%");
                    lnkCancelupdatechassisnumber.Style.Clear();
                    lnkCancelupdatechassisnumber.Style.Add("display", "none");
                    spnchassissupprater.Style.Clear();
                    spnchassissupprater.Style.Add("display", "none");
                    lnkaddorUpdatechassisnumber.Text = "Edit";
                    hfilChissisnumber.Value = loaddata.Rows[0]["nvar_Chasis"].ToString();
                }
                else
                {
                    ltlChassiss.Text = "&nbsp;";
                    txtchassisnumber.Text = string.Empty;
                    hfilChissisnumber.Value = string.Empty;
                    txtchassisnumber.Style.Clear();
                    txtchassisnumber.Style.Add("display", "inline");
                    txtchassisnumber.Style.Add("width", "20%");
                    lnkaddorUpdatechassisnumber.Text = "Add";
                    lnkCancelupdatechassisnumber.Style.Clear();
                    lnkCancelupdatechassisnumber.Style.Add("display", "inline");
                    spnchassissupprater.Style.Clear();
                    spnchassissupprater.Style.Add("display", "inline");
                }

            }
            else
            {
                ltlChassiss.Text = "&nbsp;";
                txtchassisnumber.Text = string.Empty;
                hfilChissisnumber.Value = string.Empty;
                txtchassisnumber.Style.Clear();
                txtchassisnumber.Style.Add("display", "inline");
                txtchassisnumber.Style.Add("width", "20%");
                lnkaddorUpdatechassisnumber.Text = "Add";
                lnkCancelupdatechassisnumber.Style.Clear();
                lnkCancelupdatechassisnumber.Style.Add("display", "inline");
                spnchassissupprater.Style.Clear();
                spnchassissupprater.Style.Add("display", "inline");
            }
            string orignaddr = string.Empty, destaddr = string.Empty;

            if (loaddata.Rows[0]["nvar_From"] != null)
                if (!string.IsNullOrEmpty(loaddata.Rows[0]["nvar_From"].ToString()))
                    orignaddr = loaddata.Rows[0]["nvar_From"].ToString();
                else
                    orignaddr = string.Empty;
            else
                orignaddr = string.Empty;

            if (loaddata.Rows[0]["nvar_FromCity"] != null)
                if (!string.IsNullOrEmpty(loaddata.Rows[0]["nvar_FromCity"].ToString()))
                    if (!string.IsNullOrEmpty(orignaddr)) orignaddr += "," + loaddata.Rows[0]["nvar_FromCity"].ToString(); else orignaddr += loaddata.Rows[0]["nvar_FromCity"].ToString();
                else
                    orignaddr += string.Empty;
            else
                orignaddr += string.Empty;


            if (loaddata.Rows[0]["nvar_FromState"] != null)
                if (!string.IsNullOrEmpty(loaddata.Rows[0]["nvar_FromState"].ToString()))
                    if (!string.IsNullOrEmpty(orignaddr)) orignaddr += "," + loaddata.Rows[0]["nvar_FromState"].ToString(); else orignaddr += loaddata.Rows[0]["nvar_FromState"].ToString();
                else
                    orignaddr += string.Empty;
            else
                orignaddr += string.Empty;
            //ltlorignaddress.Text = orignaddr;



            if (loaddata.Rows[0]["nvar_To"] != null)
                if (!string.IsNullOrEmpty(loaddata.Rows[0]["nvar_To"].ToString()))
                    destaddr = loaddata.Rows[0]["nvar_To"].ToString();
                else
                    destaddr = string.Empty;
            else
                destaddr = string.Empty;

            if (loaddata.Rows[0]["nvar_Tocity"] != null)
                if (!string.IsNullOrEmpty(loaddata.Rows[0]["nvar_Tocity"].ToString()))
                    if (!string.IsNullOrEmpty(destaddr)) destaddr += "," + loaddata.Rows[0]["nvar_Tocity"].ToString(); else destaddr += loaddata.Rows[0]["nvar_Tocity"].ToString();
                else
                    destaddr += string.Empty;
            else
                destaddr += string.Empty;


            if (loaddata.Rows[0]["nvar_Tostate"] != null)
                if (!string.IsNullOrEmpty(loaddata.Rows[0]["nvar_Tostate"].ToString()))
                    if (!string.IsNullOrEmpty(destaddr)) destaddr += "," + loaddata.Rows[0]["nvar_Tostate"].ToString(); else destaddr += loaddata.Rows[0]["nvar_Tostate"].ToString();
                else
                    destaddr += string.Empty;
            else
                destaddr += string.Empty;


            if (!string.IsNullOrEmpty(orignaddr))
            {
                rpeaddressfromdetails.DataSource = loaddata;
                rpeaddressfromdetails.DataBind();
            }
            else
            {
                LoadDetailsBase load = new LoadDetailsBase();
                DataTable fromlist = load.loadfromaddresslist(Convert.ToInt32(Session[Keys.LOAD_ID]));
                if (fromlist != null && fromlist.Rows.Count > 0)
                {
                    rpeaddressfromdetails.DataSource = fromlist;
                    rpeaddressfromdetails.DataBind();
                }
                else
                {
                    rpeaddressfromdetails.DataSource = loaddata;
                    rpeaddressfromdetails.DataBind();
                }
            }

            if (!string.IsNullOrEmpty(destaddr))
            {
                rpeaddresstodetails.DataSource = loaddata;
                rpeaddresstodetails.DataBind();
            }
            else
            {
                LoadDetailsBase load = new LoadDetailsBase();
                DataTable tolist = load.loadtoaddresslist(Convert.ToInt32(Session[Keys.LOAD_ID]));
                if (tolist != null && tolist.Rows.Count > 0)
                {
                    rpeaddresstodetails.DataSource = tolist;
                    rpeaddresstodetails.DataBind();
                }
                else
                {
                    rpeaddresstodetails.DataSource = loaddata;
                    rpeaddresstodetails.DataBind();
                }
            }

            //ltltoaddress.Text = destaddr;

            //if (loaddata.Rows[0]["nvar_FromPO"] != null) ltlorignphone.Text = loaddata.Rows[0]["nvar_FromPO"].ToString(); else ltlorignphone.Text = string.Empty;
            //if (loaddata.Rows[0]["nvar_FromApp"] != null) ltlorignappt.Text = loaddata.Rows[0]["nvar_FromApp"].ToString(); else ltlorignappt.Text = string.Empty;
            //if (loaddata.Rows[0]["nvar_FromGivenby"] != null) ltlorigngivenby.Text = loaddata.Rows[0]["nvar_FromGivenby"].ToString(); else ltlorigngivenby.Text = string.Empty;


            //if (loaddata.Rows[0]["nvar_toPO"] != null) ltlorignphone.Text = loaddata.Rows[0]["nvar_toPO"].ToString(); else ltlorignphone.Text = string.Empty;
            //if (loaddata.Rows[0]["nvar_ToApp"] != null) ltlorignappt.Text = loaddata.Rows[0]["nvar_ToApp"].ToString(); else ltlorignappt.Text = string.Empty;
            //if (loaddata.Rows[0]["nvar_ToGivenby"] != null) ltlorigngivenby.Text = loaddata.Rows[0]["nvar_ToGivenby"].ToString(); else ltlorigngivenby.Text = string.Empty;


            //if (loaddata.Rows[0]["date_PickupDateTime"] != null)
            //{
            //    DateTime datefrom = Convert.ToDateTime(loaddata.Rows[0]["date_PickupDateTime"]);
            //    ltlorigndatetime.Text = datefrom.ToString("MMM dd yyyy");
            //}
            //else
            //    ltlorigndatetime.Text = string.Empty;



            //if (loaddata.Rows[0]["date_DeliveryAppointmentDate"] != null)
            //{
            //    DateTime datefrom = Convert.ToDateTime(loaddata.Rows[0]["date_DeliveryAppointmentDate"]);
            //    ltltodatetime.Text = datefrom.ToString("MMM dd yyyy");
            //}
            //else
            //    ltltodatetime.Text = string.Empty;






        }
        else
        {
            PageType page = (PageType)Session[Keys.TAB_SELECTED];
            if (page == PageType.Loads)
            {
                Response.Redirect(Keys.PAGE_LOAD_HOME);
            }
            else
            {
                Response.Redirect(Keys.PAGE_PAPER_WORK_PENDING);
            }
        }
    }
    #endregion
    #region repiterdatabind
    protected void rpeaddresstodetails_databound(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
        {
            HiddenField street = e.Item.FindControl("hfieldaddress") as HiddenField;
            HiddenField city = e.Item.FindControl("hfieldcity") as HiddenField;
            HiddenField state = e.Item.FindControl("hfieldstate") as HiddenField;
            Literal ltLFD = e.Item.FindControl("ltltoaddress") as Literal;
            Literal todate = e.Item.FindControl("ltltodatetime") as Literal;
            Literal given = e.Item.FindControl("ltltoappt") as Literal;
            Literal givenby = e.Item.FindControl("ltltogivenby") as Literal;
            Literal Pno = e.Item.FindControl("ltltophone") as Literal;
            string destaddr = string.Empty;

            if (given != null)
            {
                if (string.IsNullOrEmpty(given.Text))
                {
                    given.Text = "&nbsp;";
                }
            }

            if (givenby != null)
            {
                if (string.IsNullOrEmpty(givenby.Text))
                {
                    givenby.Text = "&nbsp;";
                }
            }

            if (Pno != null)
            {
                if (string.IsNullOrEmpty(Pno.Text))
                {
                    Pno.Text = "&nbsp;";
                }
            }

            if (street != null)
                if (!string.IsNullOrEmpty(street.Value.ToString()))
                    destaddr = street.Value.ToString();
                else
                    destaddr = string.Empty;
            else
                destaddr = string.Empty;

            if (city != null)
                if (!string.IsNullOrEmpty(city.Value.ToString()))
                    if (!string.IsNullOrEmpty(destaddr)) destaddr += "," + city.Value.ToString(); else destaddr += city.Value.ToString();
                else
                    destaddr += string.Empty;
            else
                destaddr += string.Empty;


            if (state != null)
                if (!string.IsNullOrEmpty(state.Value.ToString()))
                    if (!string.IsNullOrEmpty(destaddr)) destaddr += "," + state.Value.ToString(); else destaddr += state.Value.ToString();
                else
                    destaddr += string.Empty;
            else
                destaddr += string.Empty;
            if (ltLFD != null)
                if (!string.IsNullOrEmpty(destaddr)) ltLFD.Text = destaddr; else ltLFD.Text = "&nbsp;";

            if (todate != null)
            {
                if (!string.IsNullOrEmpty(todate.Text.ToString()))
                {
                    DateTime date = Convert.ToDateTime(todate.Text.ToString());
                    todate.Text = date.ToString("MMM dd yyyy hh:mm tt");
                }
                else
                {
                    todate.Text = "&nbsp;";
                }
            }

        }
    }
    protected void rpeaddressfromdetails_databound(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
        {
            HiddenField street = e.Item.FindControl("hfieldaddress") as HiddenField;
            HiddenField city = e.Item.FindControl("hfieldcity") as HiddenField;
            HiddenField state = e.Item.FindControl("hfieldstate") as HiddenField;
            Literal ltLFD = e.Item.FindControl("ltlorignaddress") as Literal;
            Literal todate = e.Item.FindControl("ltlorigndatetime") as Literal;
            Literal given = e.Item.FindControl("ltlorignappt") as Literal;
            Literal givenby = e.Item.FindControl("ltlorigngivenby") as Literal;
            Literal Pno = e.Item.FindControl("ltlorignphone") as Literal;
            string destaddr = string.Empty;

            if (given != null)
            {
                if (string.IsNullOrEmpty(given.Text))
                {
                    given.Text = "&nbsp;";
                }
            }

            if (givenby != null)
            {
                if (string.IsNullOrEmpty(givenby.Text))
                {
                    givenby.Text = "&nbsp;";
                }
            }

            if (Pno != null)
            {
                if (string.IsNullOrEmpty(Pno.Text))
                {
                    Pno.Text = "&nbsp;";
                }
            }


            if (street != null)
                if (!string.IsNullOrEmpty(street.Value.ToString()))
                    destaddr = street.Value.ToString();
                else
                    destaddr = string.Empty;
            else
                destaddr = string.Empty;

            if (city != null)
                if (!string.IsNullOrEmpty(city.Value.ToString()))
                    if (!string.IsNullOrEmpty(destaddr)) destaddr += "," + city.Value.ToString(); else destaddr += city.Value.ToString();
                else
                    destaddr += string.Empty;
            else
                destaddr += string.Empty;


            if (state != null)
                if (!string.IsNullOrEmpty(state.Value.ToString()))
                    if (!string.IsNullOrEmpty(destaddr)) destaddr += "," + state.Value.ToString(); else destaddr += state.Value.ToString();
                else
                    destaddr += string.Empty;
            else
                destaddr += string.Empty;
            if (ltLFD != null)
                ltLFD.Text = destaddr;

            if (todate != null)
            {
                if (!string.IsNullOrEmpty(todate.Text.ToString()))
                {
                    DateTime date = Convert.ToDateTime(todate.Text.ToString());
                    todate.Text = date.ToString("MMM dd yyyy hh:mm tt");
                }
            }

        }
    }
    #endregion
    #region button clicks
    public void lnkgoback_click(object sender, EventArgs e)
    {
        try
        {
            PageType page = (PageType)Session[Keys.TAB_SELECTED];
            if (page == PageType.Loads)
            {
                Response.Redirect(Keys.PAGE_LOAD_HOME);
            }
            else
            {
                Response.Redirect(Keys.PAGE_PAPER_WORK_PENDING);
            }
        }
        catch (Exception ex)
        {
            // Response.Redirect(Keys.PAGE_LOAD_HOME);
        }
    }
    public void lnkaddorUpdatechassisnumber_click(object sender, EventArgs e)
    {
        try
        {
            if (lnkaddorUpdatechassisnumber.Text == "Edit")
            {
                LoadDetailsBase load = new LoadDetailsBase();
                DataTable loaddetails = load.loadloaddetails(Convert.ToInt32(Session[Keys.LOAD_ID]));
                if (loaddetails != null)
                {
                    SedLoadDetails(loaddetails);
                    lnkaddorUpdatechassisnumber.Text = "Update";
                    ltlChassiss.Text = "";
                    txtchassisnumber.Style.Clear();
                    txtchassisnumber.Style.Add("display", "inline");
                    txtchassisnumber.Style.Add("width", "20%");
                    lnkCancelupdatechassisnumber.Style.Clear();
                    lnkCancelupdatechassisnumber.Style.Add("display", "inline");
                    spnchassissupprater.Style.Clear();
                    spnchassissupprater.Style.Add("display", "inline");
                    MuPageMenu.SubTitle = ConfigurationManager.AppSettings["CompanyName"].ToString() + " - Load Details For Load -" + Session[Keys.LOAD_ID].ToString() + ", container #:" + loaddetails.Rows[0]["nvar_Container"].ToString();
                    MuPageMenu.LoadMenu();
                }
            }
            else
            {
                LoadDetailsBase load = new LoadDetailsBase();
                if (!string.IsNullOrEmpty(txtchassisnumber.Text))
                {
                    if (!load.SavechassisNumber(txtchassisnumber.Text.ToString(), Convert.ToInt32(Session[Keys.LOAD_ID])))
                    {
                    }
                    DataTable loaddetails = load.loadloaddetails(Convert.ToInt32(Session[Keys.LOAD_ID]));
                    if (loaddetails != null)
                    {
                        SedLoadDetails(loaddetails);
                        MuPageMenu.SubTitle = ConfigurationManager.AppSettings["CompanyName"].ToString() + " - Load Details For Load -" + Session[Keys.LOAD_ID].ToString() + ", container #:" + loaddetails.Rows[0]["nvar_Container"].ToString();
                        MuPageMenu.LoadMenu();
                    }

                    string message = " <p>" + ErrorMessages.DETAILS_CHASSIS_OK + " </p>";
                    Mobile_eTransportMobile MasterPage = (Mobile_eTransportMobile)Page.Master;
                    MasterPage.ShowMessage(message);
                }
                else
                {
                    DataTable loaddetails = load.loadloaddetails(Convert.ToInt32(Session[Keys.LOAD_ID]));
                    if (loaddetails != null)
                    {
                        SedLoadDetails(loaddetails);
                        MuPageMenu.SubTitle = ConfigurationManager.AppSettings["CompanyName"].ToString() + " - Load Details For Load -" + Session[Keys.LOAD_ID].ToString() + ", container #:" + loaddetails.Rows[0]["nvar_Container"].ToString();
                        MuPageMenu.LoadMenu();
                    }
                }

            }
        }
        catch (Exception ex)
        {
            string errors = " <p>" + ErrorMessages.DETAILS_CHASSIS_ERROR + " </p>";
            Mobile_eTransportMobile MasterPage = (Mobile_eTransportMobile)Page.Master;
            MasterPage.ShowErrors(errors);
        }
    }
    public void lnkCancelupdatechassisnumber_click(object sender, EventArgs e)
    {
        lnkCancelupdatechassisnumber.Style.Clear();
        lnkCancelupdatechassisnumber.Style.Add("display", "none");
        spnchassissupprater.Style.Clear();
        spnchassissupprater.Style.Add("display", "none");
        txtchassisnumber.Style.Clear();
        txtchassisnumber.Style.Add("display", "none");
        lnkaddorUpdatechassisnumber.Text = "Edit";
        ltlChassiss.Text = hfilChissisnumber.Value;
    }
    #endregion
}
