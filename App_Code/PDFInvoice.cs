﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using System.Globalization;
using System.Xml;
using System.Xml.XPath;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.Rendering;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace CreatePDFInvoice
{
    public class PDFInvoice
    {
        public PDFInvoice()
        {
        }

        /// <summary>
        /// The MigraDoc document that represents the invoice.
        /// </summary>
        Document document;

        /// <summary>
        /// The text frame of the MigraDoc document that contains the address.
        /// </summary>
        TextFrame addressFrame;

        /// <summary>
        /// The table of the MigraDoc document that contains the invoice items.
        /// </summary>
        Table table;

        /// <summary>
        /// Creates the invoice document.
        /// </summary>
        public Document CreateDocument(Int64 longInvoiceLoad,
                        string strBillto1,
                        string strBillto2,
                        string strBillto3,
                        string strDate,
                        string strLoad,
                        string strContainer,
                        string strOrigin,
                        string strDestination,
                        string strAmount,
                        string strAddress,
                        string strBillingRef,
                        string strChasis,
                        string strDescription,
                        string strTerms)
        {
            // Create a new MigraDoc document
            this.document = new Document();
            //this.document.Info.Title = "SLine Invoice";
            //this.document.Info.Subject = "Load ID";
            //this.document.Info.Author = "SLine";

            CreatePage(longInvoiceLoad, strBillto1, strBillto2, strBillto3, strDate, strLoad, strContainer, strOrigin, strDestination, strAmount, strAddress, strBillingRef, strChasis, strDescription, strTerms);
            return this.document;
        }

        /// <summary>
        /// Creates the static parts of the invoice.
        /// </summary>
        void CreatePage(Int64 longInvoiceLoad,
                        string strBillto1,
                        string strBillto2,
                        string strBillto3,
                        string strDate,
                        string strLoad,
                        string strContainer,
                        string strOrigin,
                        string strDestination,
                        string strAmount,
                        string strAddress,
                        string strBillingRef,
                        string strChasis,
                        string strDescription,
                        string strTerms)
        {

            Section section = this.document.AddSection();
            this.addressFrame = section.AddTextFrame();
            this.addressFrame.Height = "2.0cm";
            this.addressFrame.Width = "8.0cm";
            this.addressFrame.Left = ShapePosition.Center;
            this.addressFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            this.addressFrame.Top = "2.0cm";
            this.addressFrame.RelativeVertical = RelativeVertical.Page;

            // Create the item table
            this.table = section.AddTable();
            this.table.Borders.Color = Colors.White;
            this.table.Borders.Width = 0.25;
            this.table.Borders.Left.Width = 0.5;
            this.table.Borders.Right.Width = 0.5;
            this.table.Rows.LeftIndent = 0;

            Paragraph paragraph = section.AddParagraph();

            Column column = this.table.AddColumn("3.0cm");
            column = this.table.AddColumn("5.5cm");
            column = this.table.AddColumn("8.5cm");
            
                        
            #region ROW 1 Cell 1/2/3
            Row row1 = table.AddRow();
            row1.Shading.Color = TableBlue;
            row1.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row1.Cells[0].Shading.Color = TableBlue;
            

            paragraph = row1.Cells[0].AddParagraph();
            paragraph.AddImage(HttpContext.Current.Server.MapPath("../Images/inv_logo.png"));
            paragraph.AddLineBreak();

            paragraph = row1.Cells[1].AddParagraph();
            paragraph.Format.Font.Name = "Arial";
            paragraph.Format.Font.Size = 10;
            paragraph.Format.Font.Bold = false;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            paragraph.AddText(GetFromXML.CompanyName);
            paragraph.AddLineBreak();
            paragraph.AddText(GetFromXML.ComapanyAddress);
            paragraph.AddLineBreak();
            paragraph.AddText(GetFromXML.ComapanyState);
            paragraph.AddLineBreak();
            paragraph.AddLineBreak();

            string strPhoneNo = GetFromXML.CompanyPhone;
            string[] s1 = strPhoneNo.Split('-');
            string[] s2 = s1[0].Split(':');
            strPhoneNo = "(" + s2[1].TrimStart(' ') + ")" + s1[1] + "-" + s1[2];
            paragraph.AddText("Phone: "+strPhoneNo);
            paragraph.AddLineBreak();
            string strFaxNo = GetFromXML.CompanyFax;
            string[] s11 = strFaxNo.Split('-');
            string[] s12 = s11[0].Split(':');
            strFaxNo = "(" + s12[1].TrimStart(' ') + ")" + s11[1] + "-" + s11[2];
            paragraph.AddText("Fax: " + strFaxNo);
            paragraph.AddLineBreak();
            paragraph.AddText(GetFromXML.FromInvoiceEmailAddress);

            //#9498d4
            //#8376C6
            paragraph = row1.Cells[2].AddParagraph();
            //paragraph.Format.Font.Color = LogoName;
            paragraph.Format.Font.Color = Colors.DeepSkyBlue;
            paragraph.Format.Font.Name = "Impact";
            paragraph.Format.Font.Size = "19";
            paragraph.Format.Font.Bold = true;
            paragraph.Format.Alignment = ParagraphAlignment.Right;
            paragraph.AddText(GetFromXML.CompanyName.ToUpper());
            paragraph.AddLineBreak();

            paragraph = row1.Cells[2].AddParagraph();
            //paragraph.Format.Font.Color = LogoName;
            paragraph.Format.Font.Color = Colors.DeepSkyBlue; 
            paragraph.Format.Font.Name = "Impact";
            paragraph.Format.Font.Size = "14";
            paragraph.Format.Font.Bold = true;
            paragraph.Format.Alignment = ParagraphAlignment.Right;
            paragraph.AddText("Invoice");
            
            #endregion


            #region ROW 2 CELL 1&2


            #region BillToAddress
            this.table = section.AddTable();
            this.table.Borders.Color = Colors.Black;
            this.table.LeftPadding = 2;
            this.table.RightPadding = 0;
            this.table.Rows.LeftIndent = 0;
            //Paragraph paragraph2 = section.AddParagraph();

            Column column2 = this.table.AddColumn("8.5cm");
            column2 = this.table.AddColumn("8.5cm");

            //column2 = this.table.AddColumn("2.5cm");
            //column2 = this.table.AddColumn("2.5cm");
            


            Row row2 = table.AddRow();
            row2.Shading.Color = TableBlue;
            row2.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            row2.Cells[0].VerticalAlignment = VerticalAlignment.Top;
            row2.Cells[0].Shading.Color = TableBlue;

            paragraph = row2.Cells[0].AddParagraph();
            paragraph.Format.Font.Name = "Arial";
            paragraph.Format.Font.Size = 10;
            paragraph.Format.Font.Bold = true;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            //paragraph.Format.Shading.Color = BGColorForHeader;
            paragraph.AddText("Bill To: ");
            paragraph.AddLineBreak();

            paragraph = row2.Cells[0].AddParagraph();
            paragraph.Format.Font.Name = "Arial";
            paragraph.Format.Font.Size = 10;
            paragraph.Format.Font.Bold = false;
            paragraph.AddText(strBillto1);
            paragraph.AddLineBreak();
            paragraph.AddText(strBillto2);
            paragraph.AddLineBreak();
            paragraph.AddText(strBillto3); 
            #endregion


            #region DateAndall

            paragraph = row2.Cells[1].AddParagraph();
            paragraph.Format.Font.Name = "Arial";
            paragraph.Format.Font.Size = 10;
            paragraph.Format.Font.Bold = true;
            //paragraph.Format.Shading.Color = BGColorForHeader;
            paragraph.AddText("Date: ");            
            paragraph.Format.Font.Bold = false;
            paragraph.AddText(strDate);

            paragraph.AddLineBreak();
            paragraph.Format.Font.Bold = true;
            //paragraph.Format.Shading.Color = BGColorForHeader;
            paragraph.AddText("Invoice #: ");
            paragraph.Format.Font.Bold = false;
            paragraph.AddText(longInvoiceLoad.ToString());
            
            paragraph.AddLineBreak();
            paragraph.AddText("Description: " + strDescription);

            paragraph.AddLineBreak();
            paragraph = row2.Cells[1].AddParagraph();
            paragraph.Format.Font.Name = "Arial";
            paragraph.Format.Font.Size = 10;
            paragraph.Format.Font.Bold = false;
            //paragraph.Format.Shading.Color = BGColorForHeader;
            paragraph.AddText("Terms: Due on Receipt");
            paragraph.Format.SpaceAfter = 4;
            #endregion

            
            #endregion

            //Ref and Container
            #region ROW 3 CELL 1&2
            Row RefCont = table.AddRow();
            RefCont.Cells[1].Borders.Visible = false;
            //RefCont.Cells[0].MergeRight = 1;
            paragraph = RefCont.Cells[1].AddParagraph();
            //paragraph.Format.Borders.Left.Visible = false;      //Width = 0;  //Clear(); //Border BorderStyle.None; //false; //BorderStyle.None;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            paragraph.Format.Font.Name = "Arial";
            paragraph.Format.Font.Size = 10;
            paragraph.Format.Font.Bold = true;
            //paragraph.Format.Shading.Color = BGColorForHeader;
            paragraph.AddText("Reference #: ");
            paragraph.Format.Font.Bold = false;
            paragraph.AddText(strBillingRef);
            paragraph.AddLineBreak();
            paragraph.Format.Font.Bold = true;
            paragraph.AddText("Container #: ");
            paragraph.Format.Font.Bold = false;
            paragraph.AddText(strContainer); 
            #endregion

            //Dummy ROw for Description
            //ROW 7 Cell 1
            #region ROW 2 Cell 1 & 2
            Row rowDesc = table.AddRow();
            rowDesc.Shading.Color = BGColorForHeader;
            rowDesc.Cells[0].Format.Font.Bold = true;
            rowDesc.Cells[0].MergeRight = 1;
            paragraph = rowDesc.Cells[0].AddParagraph();
            paragraph.Format.Font.Name = "Arial";
            paragraph.Format.Font.Size = 11;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            paragraph.AddText("Description");
            #endregion


            //ROW Origin and Dest
            #region ROW 3 Cell 1
            Row row3 = table.AddRow();
            row3.Shading.Color = TableBlue;
            row3.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            row3.Cells[0].VerticalAlignment = VerticalAlignment.Top;

            paragraph = row3.Cells[0].AddParagraph();
            paragraph.Format.Font.Name = "Arial";
            paragraph.Format.Font.Size = 10;
            paragraph.Format.Font.Bold = true;
            paragraph.AddText("Origin: ");
            paragraph.AddLineBreak();

            paragraph = row3.Cells[0].AddParagraph();
            paragraph.Format.Font.Name = "Arial";
            paragraph.Format.Font.Size = 10;
            paragraph.Format.Font.Bold = false;
            strOrigin = strOrigin.Replace("<br/>", ", ");
            paragraph.AddText(strOrigin);
            #endregion

            //ROW 3 Cell 2
            #region ROW 2 Cell 2
            paragraph = row3.Cells[1].AddParagraph();
            paragraph.Format.Font.Name = "Arial";
            paragraph.Format.Font.Size = 10;
            paragraph.Format.Font.Bold = true;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            paragraph.AddText("Destination: ");
            paragraph.AddLineBreak();

            paragraph = row3.Cells[1].AddParagraph();
            paragraph.Format.Font.Name = "Arial";
            paragraph.Format.Font.Size = 10;
            paragraph.Format.Font.Bold = false;
            strDestination = strDestination.Replace("<br/>", ", ");
            paragraph.AddText(strDestination);
            #endregion

            //Amount work      

            #region AmountWork

            #region ROW 4 for AMOUNT heading
            //ROW 4 Cell 1&2
            Row row4 = table.AddRow();
            row4.Shading.Color = BGColorForHeader;
            row4.Cells[0].MergeRight = 1;
            row4.Format.Alignment = ParagraphAlignment.Left;
            paragraph = row4.Cells[0].AddParagraph();
            paragraph.Format.Font.Name = "Arial";
            paragraph.Format.Font.Size = 11;
            paragraph.Format.Font.Bold = true;
            //paragraph.Format.Alignment = ParagraphAlignment.Left;
            paragraph.AddText("Amount");
            #endregion

            //ROW 5 Cell 1 & Cell 2
            Row row5 = table.AddRow();
            row5.Shading.Color = TableBlue;

            //Printing the Values only if Amounts are entered
            if (strAmount != "")
            {
                string[] lines = Regex.Split(strAmount, "!");
                int intTotalLength = 0;
                //foreach (string line in lines)
                for (int intCounter = 0; intCounter < lines.Length - 2; intCounter++)
                {
                    string line = lines[intCounter];
                    string[] strLeftRightpart = line.Split(':');

                    if (strLeftRightpart[1].Split('^')[0] != "" && strLeftRightpart[1].Split('^')[0] != "$")
                    {
                        row5.Cells[0].Borders.Right.Width = 0;
                        row5.Format.Font.Bold = false;
                        paragraph = row5.Cells[0].AddParagraph();
                        paragraph.Format.Font.Name = "Arial";
                        paragraph.Format.Font.Size = 10;
                        paragraph.Format.Alignment = ParagraphAlignment.Right;
                        paragraph.AddText(strLeftRightpart[0] + ":  ");
                        paragraph.AddLineBreak();

                        row5.Cells[1].Borders.Left.Width = 0;
                        row5.Format.Font.Bold = false;
                        paragraph = row5.Cells[1].AddParagraph();
                        paragraph.Format.Font.Name = "Arial";
                        paragraph.Format.Font.Size = 10;
                        paragraph.Format.Alignment = ParagraphAlignment.Left;
                        paragraph.AddText(" " + strLeftRightpart[1]);
                        paragraph.AddLineBreak();
                    }
                    intTotalLength++;
                }
                //Start printing for the final two values in the Invoice i.e., In BOLD
                //ROW 5 Cell 1 & Cell 2
                Row rowAmount = table.AddRow();
                rowAmount.Shading.Color = TableBlue;

                for (int intNewCounter = intTotalLength; intNewCounter <= intTotalLength + 1; intNewCounter++)
                {
                    string line = lines[intNewCounter];
                    string[] strLeftRightpart = line.Split(':');

                    if (strLeftRightpart.Contains("Advance Received(-)") && strLeftRightpart.Contains("$", null))
                    {
                        //if true do nothing...
                    }

                    else
                    {
                        rowAmount.Cells[0].Borders.Right.Width = 0;
                        rowAmount.Format.Font.Bold = true;
                        paragraph = rowAmount.Cells[0].AddParagraph();
                        paragraph.Format.Font.Name = "Arial";
                        paragraph.Format.Font.Size = 10;
                        paragraph.Format.Alignment = ParagraphAlignment.Right;
                        paragraph.AddText(strLeftRightpart[0] + ": ");
                        paragraph.AddLineBreak();

                        rowAmount.Cells[1].Borders.Left.Width = 0;
                        rowAmount.Format.Font.Bold = true;
                        paragraph = rowAmount.Cells[1].AddParagraph();
                        paragraph.Format.Font.Name = "Arial";
                        paragraph.Format.Font.Size = 10;
                        paragraph.Format.Alignment = ParagraphAlignment.Left;
                        paragraph.AddText(" " + strLeftRightpart[1]);
                        paragraph.AddLineBreak();
                    }
                }
            }

            #endregion

            //Amount work


            //Check payable to...
            #region ROW 6 Cell 1
            Row row6 = table.AddRow();
            row6.Format.Alignment = ParagraphAlignment.Left;
            row6.Shading.Color = TableBlue;
            row6.Cells[0].MergeRight = 1;
            row6.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            row6.Cells[0].VerticalAlignment = VerticalAlignment.Top;

            paragraph = row6.Cells[0].AddParagraph();
            paragraph.Format.Font.Name = "Arial";
            paragraph.Format.Font.Size = 10;
            paragraph.Format.Font.Bold = true;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            paragraph.AddText("Please make check payable to :  ");
            paragraph.AddLineBreak();
            paragraph = row6.Cells[0].AddParagraph();
            paragraph.Format.Font.Name = "Arial";
            paragraph.Format.Font.Size = 10;
            paragraph.Format.Font.Bold = false;
            string[] Add = strAddress.Split('^');
            paragraph.AddText(Add[0]);
            paragraph.AddLineBreak();
            paragraph.AddText(Add[1]);
            #endregion

            //Please Note...
            #region ROW 7 Cell 1
            Row row7 = table.AddRow();
            row7.Shading.Color = TableBlue;
            row7.Cells[0].Format.Font.Bold = true;
            row7.Cells[0].MergeRight = 1;
            row7.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            row7.Cells[0].VerticalAlignment = VerticalAlignment.Top;
            paragraph = row7.Cells[0].AddParagraph();
            paragraph.Format.Font.Name = "Arial";
            paragraph.Format.Font.Size = 10;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            paragraph.AddText("Thank you for your business.");
            paragraph.AddLineBreak();
            paragraph.AddText("Please note that late charges may apply after 30 days.");
            #endregion            

            this.table.SetEdge(0, 0, this.table.Columns.Count, this.table.Rows.Count, Edge.Box, BorderStyle.Single, 0.75, Color.Empty);
        }


        //pre-defined colors
#if true
        // RGB colors
        //black for border
        readonly static Color TableBorder = new Color(0, 0, 0);
        //white for background
        readonly static Color TableBlue = new Color(255, 255, 255);
        readonly static Color BGColorForHeader = new Color(171, 180, 184);
        readonly static Color TableGray = new Color(242, 242, 242);
        readonly static Color LogoName = new Color(168, 102, 226); //R: 134 G: 128 B: 179
#else
            // CMYK colors
            readonly static Color tableBorder = Color.FromCmyk(100, 50, 0, 30);
            readonly static Color tableBlue = Color.FromCmyk(0, 80, 50, 30);
            readonly static Color tableGray = Color.FromCmyk(30, 0, 0, 0, 100);
#endif
    }
}