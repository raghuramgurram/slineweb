using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class showpayables : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Load Charges";
        if (!Page.IsPostBack)
        {
            if (Request.QueryString.Count > 0)
            {
                ViewState["ID"] = Convert.ToInt64(Request.QueryString[0]);
                //ViewState["AssginType"] = DBClass.executeScalar("Select [nvar_AssignTo] from [eTn_Load] where [bint_LoadId]=" + Convert.ToInt64(ViewState["ID"]));
                FillDetails(Convert.ToInt64(ViewState["ID"]));
                //BarPayables.HeaderText = "Load Number : " + Convert.ToString(ViewState["ID"]);
                //BarPayables.LinksList = "Show Payables;Accessorial Charges;Upload Accepted Accessorial Charges";
                //BarPayables.PagesList = "ShowPayables.aspx?" + Convert.ToInt64(ViewState["ID"]) + ";AccessorialCharges.aspx?" + Convert.ToInt64(ViewState["ID"]) + ";UpLoadAccessorialDocs.aspx?" + Convert.ToInt64(ViewState["ID"]);
            }
        }
    }
    private void FillDetails(long ID)
    {
        GridBar1.HeaderText = "Load Number : "+ID;
        GridBar1.LinksList = "Show Receivables ";
        GridBar1.PagesList = "ShowReceivables.aspx?"+ID;
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetAssignLoadDetails", new object[] { ID});
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddldriver.DataSource = ds.Tables[0];
                ddldriver.DataTextField = "Name";
                ddldriver.DataValueField = "Id";
                ddldriver.DataBind();
                ddldriver.Items.Insert(0,new ListItem("Select","0"));
                ddldriver.SelectedIndex = 0;
                DisplayData(Convert.ToInt64(ddldriver.SelectedValue));
                lblAssigned.Text = Convert.ToString(ds.Tables[1].Rows[0][0]);
            }
            if (string.Compare(Convert.ToString(ds.Tables[1].Rows[0][0]), "Driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriverPayableDetails", new object[] { ID });
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        DesginDriverDetails(ds.Tables[0],Convert.ToString(ds.Tables[1].Rows[0][0]));
                    }
                }
            }
        }
        if (ds != null) { ds.Dispose(); ds = null; }
        //if (string.Compare(strAssigned, "Driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
        //{
        //    lblAssigned.Text = "Driver";
        //    dt = DBClass.returnDataTable("select E.bint_EmployeeId as 'Id',E.nvar_NickName as 'Name' from [eTn_AssignDriver] D inner join eTn_Employee E on E.[bint_EmployeeId]=D.[bint_EmployeeId] where D.bint_LoadId=" + ID + " order by E.[nvar_NickName]");
        //}
        //else
        //{
        //    lblAssigned.Text = "Carrier";
        //    dt = DBClass.returnDataTable("select C.bint_CarrierId as 'Id',C.nvar_CarrierName as 'Name' from [eTn_AssignCarrier] AC inner join eTn_Carrier C on AC.[bint_CarrierId]=C.[bint_CarrierId] where AC.bint_LoadId=" + ID + " order by C.nvar_CarrierName");
        //}
        //if (dt.Rows.Count > 0)
        //{
        //    ddldriver.DataSource = dt;
        //    ddldriver.DataTextField = "Name";
        //    ddldriver.DataValueField = "Id";
        //    ddldriver.DataBind();
        //    ddldriver.SelectedIndex = 0;
        //    DisplayData(Convert.ToInt64(ddldriver.SelectedValue));
        //}          
    }
    private void DisplayData(long PlayerId)
    {
        lblTotalText.Visible = false;
        //string strQuery = "SELECT [num_Charges],[num_AdvancedPaid],[num_Dryrun],[num_FuelSurcharge],[num_FuelSurchargeAmounts],[num_PierTermination],[bint_ExtraStops],[num_LumperCharges]," +
        //                "[num_DetentionCharges],[num_ChassisSplit],[num_ChassisRent],[num_Pallets],[num_ContainerWashout],[num_YardStorage],[num_RampPullCharges],[num_TriaxleCharge],[num_TransLoadCharges]," +
        //                "[num_HLScaleCharges],[num_ScaleTicketCharges],[num_OtherCharges1],[num_OtherCharges2],[num_OtherCharges3],[num_OtherCharges4],[nvar_OtherChargesReason]," +
        //                "isnull(eTn_Payables.[num_Charges],0)+isnull(eTn_Payables.[num_AdvancedPaid],0)+isnull(eTn_Payables.[num_Dryrun],0)+isnull(eTn_Payables.[num_FuelSurcharge],0)+isnull(eTn_Payables.[num_FuelSurchargeAmounts],0)+"+
        //                "isnull(eTn_Payables.[num_PierTermination],0)+isnull(eTn_Payables.[bint_ExtraStops],0)+"+
        //                "isnull(eTn_Payables.[num_LumperCharges],0) +isnull(eTn_Payables.[num_DetentionCharges],0)+isnull(eTn_Payables.[num_ChassisSplit],0)+"+
        //                "isnull(eTn_Payables.[num_ChassisRent],0)+isnull(eTn_Payables.[num_Pallets],0)+isnull(eTn_Payables.[num_ContainerWashout],0)+" +
        //                "isnull(eTn_Payables.[num_YardStorage],0)+isnull(eTn_Payables.[num_RampPullCharges],0)+isnull(eTn_Payables.[num_TriaxleCharge],0)+"+
        //                "isnull(eTn_Payables.[num_TransLoadCharges],0)+isnull(eTn_Payables.[num_HLScaleCharges],0)+"+
        //                "isnull(eTn_Payables.[num_ScaleTicketCharges],0)+isnull(eTn_Payables.[num_OtherCharges1],0)+"+
        //                "isnull(eTn_Payables.[num_OtherCharges2],0)+isnull(eTn_Payables.[num_OtherCharges3],0)+isnull(eTn_Payables.[num_OtherCharges4],0) AS 'Total'" +
        //                " FROM [eTn_Payables] where [bint_LoadId]=" + Convert.ToInt64(ViewState["ID"]) + " and [bint_PayablePlayerId]=" + PlayerId;
        //DataTable dt = DBClass.returnDataTable(strQuery);
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetPayableDetails", new object[] { Convert.ToInt64(ViewState["ID"]), PlayerId });
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblTotalText.Visible = true;
                txtcharges.Text = Convert.ToString(ds.Tables[0].Rows[0][0]);
                txtadvancepaid.Text = Convert.ToString(ds.Tables[0].Rows[0][1]);
                txtdryrun.Text = Convert.ToString(ds.Tables[0].Rows[0][2]);
                txtfuel.Text = "";
                txtamount.Text = Convert.ToString(ds.Tables[0].Rows[0][4]);
                txtpier.Text = Convert.ToString(ds.Tables[0].Rows[0][5]);
                txtextrastops.Text = Convert.ToString(ds.Tables[0].Rows[0][6]);
                txtlumper.Text = Convert.ToString(ds.Tables[0].Rows[0][7]);
                txtdetention.Text = Convert.ToString(ds.Tables[0].Rows[0][8]);
                txtchassis.Text = Convert.ToString(ds.Tables[0].Rows[0][9]);
                txtchassisrent.Text = Convert.ToString(ds.Tables[0].Rows[0][10]);
                txtpallets.Text = Convert.ToString(ds.Tables[0].Rows[0][11]);
                txtcontainer.Text = Convert.ToString(ds.Tables[0].Rows[0][12]);
                txtyardstorage.Text = Convert.ToString(ds.Tables[0].Rows[0][13]);
                txtramp.Text = Convert.ToString(ds.Tables[0].Rows[0][14]);
                txttriaxle.Text = Convert.ToString(ds.Tables[0].Rows[0][15]);
                txttrans.Text = Convert.ToString(ds.Tables[0].Rows[0][16]);
                txtheavy.Text = Convert.ToString(ds.Tables[0].Rows[0][17]);
                txtscaleticket.Text = Convert.ToString(ds.Tables[0].Rows[0][18]);

                txtPaidAnotherDriver.Text = Convert.ToString(ds.Tables[0].Rows[0][19]);
                txtTollCharges.Text = Convert.ToString(ds.Tables[0].Rows[0][20]);
                txtSlipCharges.Text = Convert.ToString(ds.Tables[0].Rows[0][21]);
                txtRepairCharges.Text = Convert.ToString(ds.Tables[0].Rows[0][22]);

                txtothercharges.Text = Convert.ToString(ds.Tables[0].Rows[0][23]);
                txtother.Text = Convert.ToString(ds.Tables[0].Rows[0][24]);
                txtcharges3.Text = Convert.ToString(ds.Tables[0].Rows[0][25]);
                txtcharges4.Text = Convert.ToString(ds.Tables[0].Rows[0][26]);
                if (Convert.ToString(ds.Tables[0].Rows[0][27]) != "0")
                    txtreason.Text = Convert.ToString(ds.Tables[0].Rows[0][27]);
                else
                    txtreason.Text = "";
                lblTotal.Text = Convert.ToString(ds.Tables[0].Rows[0][28]);

                if (Convert.ToString(ds.Tables[0].Rows[0][29]) != "0")
                    txtCheckNumber.Text = Convert.ToString(ds.Tables[0].Rows[0][29]);
                else
                    txtCheckNumber.Text = "";

                txtChargesPerDiem.Text = Convert.ToString(ds.Tables[0].Rows[0][30]);

                ViewState["Mode"] = "Edit";
            }
            else
            {
                ClearControls();
                lblTotalText.Visible = false;
                ViewState["Mode"] = "New";
            }
        }
        else
        {
            ClearControls();
            lblTotalText.Visible = false;
            ViewState["Mode"] = "New";
        }
        if (ds != null) { ds.Dispose(); ds = null; }
      
    }
    protected void btnCancle_Click(object sender, EventArgs e)
    {
        CheckBackPage();
        if(ViewState["ID"]!=null)
            Response.Redirect("~/Manager/ShowReceivables.aspx?" + Convert.ToString(ViewState["ID"]));
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (txtfuel.Text.Trim().Length > 0 && txtamount.Text.Trim().Length > 0)
            {
                Response.Write("<script>alert('You can not enter both Fuel surcharge percentage and Fuel surcharge amount.Enter only one.')</script>");
                return;
            }
            if (txtfuel.Text.Trim().Length > 0 && txtcharges.Text.Trim().Length == 0)
            {
                Response.Write("<script>alert('Enter Charges to calculate Fuel surcharge percentage.')</script>");
                return;
            }
            if (txtfuel.Text.Trim().Length > 0 && txtcharges.Text.Trim().Length > 0)
            {
                txtamount.Text = Convert.ToString((Convert.ToDouble(txtcharges.Text.Trim()) / 100) * Convert.ToDouble(txtfuel.Text.Trim()));
                txtfuel.Text = "0";
            }
            if (ddldriver.SelectedIndex == 0)
            {
                return;
            }
            if (ViewState["Mode"] != null)
            {
                if (ViewState["ID"] != null)
                {
                    //SLine 2016 Enhancements ---- Added the txtChargesPerDiem to the Pbject parameters
                    object[] objParams = new object[] {Convert.ToInt64(ViewState["ID"]),Convert.ToInt64(ddldriver.SelectedValue),txtcharges.Text.Trim(),txtadvancepaid.Text.Trim(),txtdryrun.Text.Trim(),txtfuel.Text.Trim(),txtamount.Text.Trim(),txtpier.Text.Trim(),txtextrastops.Text.Trim(),txtlumper.Text.Trim(),
                                            txtdetention.Text.Trim(),txtchassis.Text.Trim(),txtchassisrent.Text.Trim(),txtpallets.Text.Trim(),txtcontainer.Text.Trim(),txtyardstorage.Text.Trim(),txtramp.Text.Trim(),txttriaxle.Text.Trim(),
                                            txttrans.Text.Trim(),txtheavy.Text.Trim(),txtscaleticket.Text.Trim(),txtothercharges.Text.Trim(),txtother.Text.Trim(),txtcharges3.Text.Trim(),txtcharges4.Text.Trim(),txtreason.Text.Trim(),txtPaidAnotherDriver.Text.Trim(),txtTollCharges.Text.Trim(),txtSlipCharges.Text.Trim(),txtRepairCharges.Text.Trim(),txtCheckNumber.Text.Trim(),lblAssigned.Text.Trim(), txtChargesPerDiem.Text };
                    CheckNull(ref objParams);

                    if (string.Compare(Convert.ToString(ViewState["Mode"]), "Edit", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                    {
                        if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Update_Payables", SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Update_Payables", objParams)) > 0)
                        {
                            ViewState["Mode"] = "Edit";
                            //if (string.Compare(Convert.ToString(ViewState["AssginType"]), "driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                            //    DisplayData(Convert.ToInt64(ddldriver.SelectedValue));
                            //else
                            //{
                            CheckBackPage();
                            if (ViewState["ID"] != null)
                                Response.Redirect("~/Manager/ShowReceivables.aspx?" + Convert.ToString(ViewState["ID"]));
                            //}
                        }
                    }
                    else if (string.Compare(Convert.ToString(ViewState["Mode"]), "New", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                    {
                        if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Add_Payables", SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Add_Payables", objParams)) > 0)
                        {
                            ViewState["Mode"] = "Edit";
                            //if (string.Compare(Convert.ToString(ViewState["AssginType"]), "driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                            //    DisplayData(Convert.ToInt64(ddldriver.SelectedValue));
                            //else
                            //{
                               CheckBackPage();
                                 Response.Redirect("~/Manager/ShowReceivables.aspx?" + Convert.ToString(ViewState["ID"]));
                                //}
                        }
                    }
                    objParams = null;
                }
            }
        }
    }

    private void CheckNull(ref object[] objParams)
    {
        for (int i = 0; i < objParams.Length; i++)
        {
            if (i == objParams.Length - 2)
            {
                if (Convert.ToString(objParams[i]).Trim().Length == 0)
                {
                    objParams[i] = "";
                }
            }
            else if (Convert.ToString(objParams[i]).Trim().Length == 0)
            {
                objParams[i] = "0";
            }            
        }
    }
    protected void ddldriver_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ViewState["ID"] != null && ddldriver.SelectedIndex>0)
        {
            DisplayData(Convert.ToInt64(ddldriver.SelectedValue));
        }
        else
        {
            ClearControls();
        }
    }

    private void CheckBackPage()
    {
        if (Session["BackPage5"] != null)
        {
            if (Convert.ToString(Session["BackPage5"]).Trim().Length > 0)
            {
                string strTemp = Convert.ToString(Session["BackPage5"]).Trim();
                Session["BackPage5"] = null;
                Response.Redirect(strTemp);
            }
        }
        else if (Session["BackPage4"] != null)
        {
            if (Convert.ToString(Session["BackPage4"]).Trim().Length > 0)
            {
                string strTemp = Convert.ToString(Session["BackPage4"]).Trim();
                Session["BackPage4"] = null;
                Response.Redirect(strTemp);
            }
        }
    }    
    public void DesginDriverDetails(DataTable dtDriver,string Totals)
    {
        System.Text.StringBuilder strCode =new System.Text.StringBuilder();
        strCode.Append("<table width=100% border=0 cellspacing=0 cellpadding=0 id=tbldisplay>");
        DesignTable(dtDriver.Columns, dtDriver.Rows, ref strCode);
        strCode.Append("<tr id=" + (((dtDriver.Rows.Count - 1) % 2 == 0) ? "row" : "altrow") + " ><td  align=center colspan=" + Convert.ToString(dtDriver.Rows.Count + 1) + "><span  class=mandatory> Total Payable : " + Totals + "</span></td></tr>");
        strCode.Append("</table>");
        lblDriver.Text = strCode.ToString();
        strCode = null;
    }

    private void DesignTable(DataColumnCollection dcDriver,DataRowCollection drDriver,ref System.Text.StringBuilder strCode)
    {        
        strCode.Append("<tr><th>"+dcDriver[0].ColumnName+"</th>");
        for (int k = 0; k < drDriver.Count; k++)
        {
            strCode.Append("<th>" + drDriver[k].ItemArray[0] + "</th>");
        }
        strCode.Append("</tr>");
        for (int i = 1; i < dcDriver.Count; i++)
        {
            if(((i+1)%2)==0)
                strCode.Append("<tr id=row>");
            else
                strCode.Append("<tr id=altrow>");
            strCode.Append("<td width=30% align=right>" + ((i == dcDriver.Count-1) ? "<strong>" + dcDriver[i].ColumnName + "</strong>" : dcDriver[i].ColumnName) + "</td>");
            for(int j=0;j<drDriver.Count;j++)
            {
                strCode.Append("<td>" + ((i == dcDriver.Count - 1) ? "<strong>" + Convert.ToString(drDriver[j].ItemArray[i]).Trim() + "</strong>" : Convert.ToString(drDriver[j].ItemArray[i]).Trim()) + "</td>");
            }
            strCode.Append("</tr>");
        }
    }
    private void ClearControls()
    {
        txtcharges.Text = "";
        txtadvancepaid.Text = "";
        txtdryrun.Text = "";
        txtfuel.Text = "";
        txtamount.Text = "";
        txtpier.Text = "";
        txtextrastops.Text = "";
        txtlumper.Text = "";
        txtdetention.Text = "";
        txtchassis.Text = "";
        txtchassisrent.Text = "";
        txtpallets.Text = "";
        txtcontainer.Text = "";
        txtyardstorage.Text = "";
        txtramp.Text = "";
        txttriaxle.Text = "";
        txttrans.Text = "";
        txtheavy.Text = "";
        txtscaleticket.Text = "";
        txtothercharges.Text = ""; ;
        txtother.Text = "";
        txtcharges3.Text = "";
        txtcharges4.Text = "";
        txtreason.Text = "";
        lblTotal.Text = "";
        txtCheckNumber.Text = "";
        txtPaidAnotherDriver.Text = "";
        txtTollCharges.Text = "";
        txtSlipCharges.Text = "";
        txtRepairCharges.Text = "";
    }
}
