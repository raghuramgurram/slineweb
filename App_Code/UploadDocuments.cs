﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;



    public class UploadDocuments
    {
        public UploadDocuments() { }
        public int GetDocumentCount(int loadid, int DocumentType)
        {
            int dateupdate = 0;
            try
            {
                parametereforUploadDocuments parameter = new parametereforUploadDocuments();
                parameter.bint_LoadId = loadid;
                parameter.bint_DocumentId = DocumentType;
                string[] str = { "bint_LoadId", "bint_DocumentId" };
                BaseClass bis = new BaseClass();
                DataSet ds = bis.Loaddata(parameter, "sp_Get_UploadDocuments_Count", str);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                if (Convert.ToInt32(dt.Rows[0]["Document_count"].ToString()) > 0)
                                {
                                    dateupdate = Convert.ToInt32(dt.Rows[0]["Document_count"].ToString());
                                }
                                else
                                {
                                    dateupdate = 0;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                dateupdate = 0;
            }


            return dateupdate;
            //Get_Load_tracking_LastDate
        }
        public bool InsertDocument(int loadid, int DocumentType, string DocumentName, int documentcount)
        {
            bool returnvalue = false;
            try
            {
                int i = -1;
                parametereforUploadDocuments parameter = new parametereforUploadDocuments();
                parameter.bint_LoadId = loadid;
                parameter.bint_FileNo = documentcount;
                parameter.nvar_DocumentName = DocumentName;
                parameter.bint_DocumentId = DocumentType;
                string[] str = { "bint_LoadId", "bint_FileNo", "nvar_DocumentName", "bint_DocumentId" };
                BaseClass bis = new BaseClass();
                i = bis.savedata(parameter, "sp_Insert_UploadDocuments", str);
                if (i >= 1)
                    returnvalue = true;
                else
                    returnvalue = false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return returnvalue;
        }

    }
public class parametereforUploadDocuments : NewBaseClass
    {
        public parametereforUploadDocuments()
        {
        }
        
    }