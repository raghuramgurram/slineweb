using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CustomerTrack : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (Session["CustomerId"] != null)
        {
            if (txtContainer.Text.Trim().Length > 0)
            {
                switch (ddlContainer.SelectedIndex)
                {
                    case 0:
                        Session["TrackWhereClause"] = "(eTn_Load.[nvar_Container] like '" + txtContainer.Text.Trim() + "' or eTn_Load.[nvar_Container1] like '" + txtContainer.Text.Trim() + "') and eTn_Load.[bint_CustomerId]=" + Convert.ToInt64(Session["CustomerId"]) + "";
                        break;
                    case 1:
                        Session["TrackWhereClause"] = "eTn_Load.[nvar_Ref] like '" + txtContainer.Text.Trim() + "%'  and eTn_Load.[bint_CustomerId]=" + Convert.ToInt64(Session["CustomerId"]) + "";
                        break;
                    case 2:
                        Session["TrackWhereClause"] = "eTn_Load.[nvar_Booking] like '" + txtContainer.Text.Trim() + "%'  and eTn_Load.[bint_CustomerId]=" + Convert.ToInt64(Session["CustomerId"]) + "";
                        break;
                    case 3:
                        try
                        {
                            Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=" + Convert.ToInt64(txtContainer.Text.Trim()) + "  and eTn_Load.[bint_CustomerId]=" + Convert.ToInt64(Session["CustomerId"]);
                        }
                        catch
                        {
                            Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=0";
                        }
                        break;
                }
            }
            else
            {
                Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=0";
            }
            bool blVal = false;
            try
            {
                if (Convert.ToInt32(DBClass.executeScalar("select count(bint_LoadId) from eTn_Load where " + Convert.ToString(Session["TrackWhereClause"]))) == 1)
                {
                    blVal = true; 
                }
            }
            catch
            {
            }
            if (blVal)
            {
                Response.Redirect("CustomerTrackLoad.aspx?" + DBClass.executeScalar("select bint_LoadId from eTn_Load where " + Convert.ToString(Session["TrackWhereClause"])));
            }
           
            Response.Redirect("CustomerTrackLoadResults.aspx");
        }
    }
}
