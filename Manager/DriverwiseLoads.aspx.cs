using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class DriverwiseLoads : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Driver Wise Loads";
        if (!IsPostBack)
            FillLists();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if(Page.IsValid)
        {
            Session["SerchCriteria"] = null;
            Session["ReportParameters"] = null;
            Session["ReportName"] = null;
            Session["ReportParameters"] = ddlDrivers.SelectedValue + "~~^^^^~~" + dropStatus.SelectedItem.Text;
            Session["SerchCriteria"] = ((Convert.ToInt64(ddlDrivers.SelectedValue) > 0) ? "Driver nick name : " + ddlDrivers.SelectedItem.Text.Trim() : "All Drivers ") + "&nbsp;&nbsp;&nbsp; Status : " + dropStatus.SelectedItem.Text;
            Session["ReportName"] = "GetDriverwiseLoads";
            Response.Redirect("~/Manager/DisplayReport.aspx");
        }
    }

    private void FillLists()
    {
        ddlDrivers.Items.Clear();
        //ddlDrivers.DataSource = DBClass.returnDataTable("select bint_EmployeeId as 'Id',nvar_NickName as 'Name' from eTn_Employee where bint_RoleId=(select R.bint_RoleId from eTn_Role R where lower(nvar_RoleName)='driver') order by [nvar_NickName]");
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriverNamesForaLocation", new object[] { officeLocationId });
        //SLine 2017 Enhancement for Office Location Ends
        if(ds.Tables.Count > 0)
        {
            ddlDrivers.DataSource = ds.Tables[0];
            ddlDrivers.DataTextField = "Name";
            ddlDrivers.DataValueField = "Id";
            ddlDrivers.DataBind();
        }
        ddlDrivers.Items.Insert(0, new ListItem("All Drivers", "0"));
        ds.Tables.Clear();
        //dropStatus.DataSource = DBClass.returnDataTable("Select bint_LoadStatusId,nvar_LoadStatusDesc from eTn_LoadStatus");
        ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadStatusDetails");
        if(ds.Tables.Count > 0)
        {
            dropStatus.DataSource = ds.Tables[0];
            dropStatus.DataValueField = "bint_LoadStatusId";
            dropStatus.DataTextField = "nvar_LoadStatusDesc";
            dropStatus.DataBind();
        }
        if(dropStatus.Items.Count > 0)
        {
            dropStatus.Items.Remove(dropStatus.Items.FindByText("Cancel"));
            dropStatus.Items.Remove(dropStatus.Items.FindByText("Closed"));
            dropStatus.Items.Remove(dropStatus.Items.FindByText("New"));
            dropStatus.Items.Insert(1, new ListItem("All Transit Loads", dropStatus.Items.Count + 1.ToString()));
            dropStatus.Items.Insert(0, new ListItem("All Status", Convert.ToString(dropStatus.Items.Count + 3)));
        }
        if(ds != null) { ds.Dispose(); ds = null; }
    }

    //private void DisplayResport()
    //{
    //    DataSet ds;
    //    object[] objParams = new object[] { txtName.Text.Trim(), ddlList.SelectedItem.Value };
    //    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriverwiseLoads", objParams);
    //    string[] strColumnsList = new string[] { "Name", "User ID", "Password", "Date Created" };
    //    string[] colWidths = new string[] { "40%", "20%", "20%", "20%" };

    //    objParams = null;
    //    if (ds.Tables.Count > 0)
    //    {
    //        DataTable dt = ds.Tables[0];
    //        CommonFunctions.FormatDataTable(ref dt);
    //        lblDisplay.Text = CommonFunctions.ReportDisplay(dt, strColumnsList, colWidths);
    //        if (dt != null) { dt.Dispose(); dt = null; }
    //    }
    //    else
    //    {
    //        lblDisplay.Text = "<table width=100% border=0 cellpadding=0 cellspacing=0 id=ContentTbl><tr valign=top><td>" +
    //            "<table border=0 width=100% height=200px border=0 cellpadding=0 cellspacing=0 align=center valign=middle><tr><td width=100% align=center valign=middle>" +
    //            "<b><font color=blue>No Records To Display.<font></b></td></tr></table><br/><br/><br/></td></tr></table>";
    //    }
    //    if (ds != null) { ds.Dispose(); ds = null; }
    //    strColumnsList = null; colWidths = null;
    //}    
}
