<%@ Page Language="C#" MasterPageFile="~/Manager/MasterPage.master" AutoEventWireup="true" CodeFile="DownloadFiles.aspx.cs" Inherits="Manager_Default" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td valign="top" align="left" Width="100%">     
          <asp:Label ID="lblError" runat="server" Text="no files exist." Visible="false" ForeColor="red"></asp:Label>
            <asp:GridView ID="gvFiles" runat="server" CssClass="Grid" AutoGenerateColumns="false">
                <PagerStyle CssClass="GridPage" Font-Size="Smaller" HorizontalAlign="Center" Width="100%"/>
                <RowStyle BorderColor="White" CssClass="GridItem" />
                <HeaderStyle BorderColor="White" CssClass="GridHeader" ForeColor="Black" />
                <AlternatingRowStyle CssClass="GridAltItem" />
                <Columns>
                <asp:TemplateField HeaderText="File Name" HeaderStyle-Width="25%">
                    <ItemTemplate>
                       <asp:Label ID="lblFileId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"bint_ExportFileId") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lblFileName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"nvar_FileName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>     
                <asp:TemplateField HeaderText="Note" HeaderStyle-Width="30%">
                    <ItemTemplate>
                        <asp:Label ID="lblNote" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"nvar_Notes") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>     
                <asp:BoundField HeaderText="Created Date" DataField="CreatedDate" HeaderStyle-Width="15%"/>  
                 <asp:TemplateField HeaderStyle-Width="15%">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkDownLoad" runat="server" Text='Download' ToolTip="Click the link to download." OnClick="OndownloadClick"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>      
                <asp:TemplateField HeaderStyle-Width="15%">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" ToolTip="Click the link to Delete the file perminetly." OnClick="OnDeleted"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <br />
        </td>
     </tr>          
 </table>
</asp:Content>

