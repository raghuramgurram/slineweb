<%@ Page AutoEventWireup="true" CodeFile="ForgotPassword.aspx.cs" Inherits="ForgotPassword"
    Language="C#" MasterPageFile="~/LoginMaster.master" Title="S Line Transport Inc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="100%">
        <tr>
            <td class="pagehead" colspan="2">
                Receive a new password
            </td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td bgcolor="#ffffff" height="252" valign="top" width="318">
                <img alt="Login" height="252" src="images/laptop.jpg" width="318" /></td>
            <td bgcolor="#ffffff" height="252" valign="top">
                <table border="0" cellpadding="25" cellspacing="0" width="100%">
                    <tr>
                        <td valign="top">
                            <table id="loginbox" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" width="80%">
                                            <tr id="tbldisplay">
                                                <td colspan="2" align="center">
                                                    <asp:Label ID="lblReq" runat="server" ForeColor="Red" Text="Please enter User Id/E-mail.">
                                                    </asp:Label>
                                                 </td>
                                            </tr>
                                            <tr id="tbldisplay">
                                                <td align="right" style="width: 40%; height: 20px;">
                                                    User Id :
                                                </td>
                                                <td align="left">
                                                    &nbsp;<asp:TextBox ID="txtUserId" runat="server">
                                                    </asp:TextBox>
                                                    <asp:Label ID="lblErrMsg" runat="server" ForeColor="Red" Text="User Id is invalid."
                                                        Visible="false"></asp:Label></td>
                                            </tr>
                                            <tr id="tbldisplay">
                                                <td align="right" style="width: 40%; height: 20px;"> &nbsp;                                                   
                                                </td>
                                                <td align="left">
                                                  &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;( Or )
                                                </td>
                                            </tr>
                                            <tr id="tbldisplay">
                                                <td align="right" style="width: 40%; height: 20px;">
                                                    E-Mail &nbsp;:
                                                </td>
                                                <td align="left">
                                                    &nbsp;<asp:TextBox ID="txtEmailId" runat="server">
                                                    </asp:TextBox>&nbsp;
                                                    <asp:Label ID="lblEmailId" runat="server" ForeColor="Red" Text="E-mail Id is invalid."
                                                        Visible="false"></asp:Label></td>
                                            </tr>
                                            <tr id="Tr3">
                                                <td align="right" style="width: 40%; height: 45px;">
                                                    <asp:Button ID="btnsubmit" runat="server" CssClass="btnstyle" OnClick="btnsubmit_Click"
                                                        Text="Continue" />
                                                </td>
                                                <td align="left">
                                                    &nbsp;<asp:Button ID="btncancel" runat="server" CausesValidation="False" CssClass="btnstyle"
                                                        OnClick="btncancel_Click" Text="Cancel" UseSubmitBehavior="False" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="labeltext" colspan="2">
                                                    <asp:Panel ID="pnlSendMail" runat="Server" Visible="false">
                                                        <asp:Label ID="lblTest" runat="Server">
                                                           Password is mailed to your mail address.Please check your mail and <a href="Login.aspx">login</a> here
                                                        </asp:Label>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table> 
                        </td>
                    </tr>
                </table>
            </td>            
        </tr>        
    </table>
</asp:Content>

