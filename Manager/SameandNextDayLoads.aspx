<%@ Page Language="C#" MasterPageFile="~/Manager/MasterPage.master" AutoEventWireup="true" CodeFile="SameandNextDayLoads.aspx.cs" Inherits="Manager_SameandNextDayLoads" Title="Untitled Page" %>
<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/LoadGridManager.ascx" TagName="LoadGridManager" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <table id="ContentTbl1" border="0" cellpadding="2" cellspacing="0" width="100%">  
     <tr >
            <td align="left" style="width: 100%">                
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="5" src="../images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <uc2:GridBar ID="barManager" runat="server" HeaderText="Loads"/>
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="5" src="../images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table id="tbldisplay1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <uc1:LoadGridManager ID="grdManager" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
 </table>
</asp:Content>

