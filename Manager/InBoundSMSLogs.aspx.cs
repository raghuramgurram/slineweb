using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class InBoundSMSLogs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "InBound SMS Log";
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            ViewState["FromDate"] = dtpFromDate.Date;
            ViewState["ToDate"] = dtpToDate.Date;
            LoadGrid(1);
        }
    }

    protected void dggrid_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        LoadGrid(e.NewPageIndex + 1);
    }

    private void LoadGrid(int currentPageNumber)
    {
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "sp_GetInboundSMSLogs_RowNumber", new object[] { currentPageNumber, dggrid.PageSize, Convert.ToString(ViewState["FromDate"]), Convert.ToString(ViewState["ToDate"]), officeLocationId });
        //SLine 2017 Enhancement for Office Location Ends
        if(ds.Tables.Count==2)
        {
            dggrid.CurrentPageIndex = currentPageNumber - 1;
            dggrid.VirtualItemCount = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            dggrid.DataSource = ds.Tables[0];
            dggrid.DataBind();
            
        }
    }
}
