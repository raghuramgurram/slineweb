<%@ Page AutoEventWireup="true" CodeFile="PrintDriver.aspx.cs" Inherits="PrintDriver"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc2" %>
<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">
    <table id="ContentTbl" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <uc2:GridBar ID="barCurrent" runat="server" />
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="3" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table id="tblform" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <th colspan="2">
                            Driver Edit
                        </th>
                    </tr>
                    <tr id="row">
                        <td align="right" style="width: 35%;" valign="middle">
                            Driver Name:
                        </td>
                        <td style="width: 65%;">
                            <strong>
                                <asp:Label ID="lblDriver" runat="server"></asp:Label></strong>
                        </td>
                    </tr>
                    <tr id="altrow">
                        <td align="right">
                            From :
                        </td>
                        <td>
                            <asp:Label ID="lblFrom" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr id="row">
                        <td align="right">
                            City :
                        </td>
                        <td>
                            <asp:Label ID="lblFromCity" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr id="altrow">
                        <td align="right">
                            State :</td>
                        <td>
                            <asp:Label ID="lblFromState" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr id="row">
                        <td align="right">
                            To :
                        </td>
                        <td>
                            <asp:Label ID="lblTO" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr id="altrow">
                        <td align="right">
                            City :
                        </td>
                        <td>
                            <asp:Label ID="lblToCity" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr id="row">
                        <td align="right">
                            State :</td>
                        <td>
                            <asp:Label ID="lblToState" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr id="altrow">
                        <td align="right">
                            Tag# :</td>
                        <td>
                            <asp:Label ID="lblTag" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="5" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table id="tblBottomBar" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="right">
                            &nbsp;<asp:Button ID="btnCancel" runat="server" CssClass="btnstyle" OnClick="btnCancel_Click"
                                Text="Back" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
