<%@ Page AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="login" Language="C#"
    MasterPageFile="~/LoginMaster.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table width="100%">
  <tr>
    <td colspan="2" class="pagehead">Login Area </td>
  </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" >
        <tr>
        <td bgcolor="#ffffff" height="252" valign="top" width="318">
            <img alt="Login" height="252" src="images/laptop.jpg" width="318" /></td>
        <td bgcolor="#ffffff" height="252" valign="top">
            <table border="0" cellpadding="25" cellspacing="0" width="100%">
                 <tr>
        <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" id="loginbox">
          <tr>
            <td valign="top" >
              <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr id="tbldisplay">
                  <td><strong>User Name</strong><asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                          runat="server" ErrorMessage="User name can not be empty." ControlToValidate="txtusername" Display="Dynamic">*</asp:RequiredFieldValidator><br />
                      <asp:TextBox ID="txtusername" runat="server"></asp:TextBox></td>
                </tr>
                <tr id="tbldisplay">
                  <td><img src="images/pix.gif" alt="" width="1" height="3" /></td>
                </tr>
                <tr id="tbldisplay">
                  <td style="height: 38px"><strong>Password
                  </strong> 
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Password can not be empty." ControlToValidate="txtpassword" Display="Dynamic">*</asp:RequiredFieldValidator>
                      <br />
                      <asp:TextBox ID="txtpassword" runat="server" TextMode="Password"></asp:TextBox></td>
                </tr>
                <tr id="tbldisplay">
                  <td><img src="images/pix.gif" alt="" width="1" height="3" /></td>
                </tr>
               
                
                <tr id="tbldisplay">
                  <td>
                      <asp:Button ID="btnsubmit" CssClass="btnstyle" runat="server" Text="Submit" 
                          OnClick="btnsubmit_Click" Width="50px" />
                      &nbsp;
                      <asp:Button ID="btncancel" runat="server" CssClass="btnstyle" Text="Cancel" 
                          CausesValidation="False" UseSubmitBehavior="False" Width="52px" /></td>
                </tr>
                  <tr id="tbldisplay">
                      <td>
                          <asp:ValidationSummary ID="ValidationSummary1" runat="server" Font-Size="12px" />                         
                          <asp:Label ID="lblErrMsg" runat="server" Text="User name/Password is invalid." ForeColor="Red" Visible="False"></asp:Label></td></tr>
                           <tr>
                <td>
                <asp:CheckBox ID="chkRememberMe" runat="server" Text="Remember Me" CssClass="bltext" />                
                
                </td>
                </tr>
                <tr id="tbldisplay">
                  <td><asp:LinkButton ID="lknforgot" runat="server" PostBackUrl="~/ForgotPassword.aspx" CausesValidation="False">Forgot Password ?</asp:LinkButton></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
            </td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<%--<iframe id="test" onload="javascript:DontGoback()">
</iframe>

<script language="javascript" type="text/javascript">
function DontGoback()
{
alert('<%= Session["UserId"] %>');
    var ses = '<%= Session["UserId"] %>'
    if(ses == null || ses=='')
        history.go(-1);
}
</script>--%>
</asp:Content>

