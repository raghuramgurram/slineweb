using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using Ionic.Zip;
using System.Web.Services;
using System.Net;

public partial class DisplayInvoiceableLoadsReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Text = string.Empty;
        if (!IsPostBack)
        {                     
            ReportDisplay();
            ViewState["strList_LoadsWithNOdocs"] = "";
        }
    }

    protected void lnkClose_Click(object sender, EventArgs e)
    {
        UpdateLoadStatus(int.Parse(((LinkButton)sender).CommandArgument));
    }

    public bool UpdateLoadStatus(int loadid)
    {
        bool returnvalue = false;
        ViewState["strList_LoadsWithNOdocs"] = "";
        try
        {
            //to check if the this cust has opted for sendin the Invoice by email
            if (CheckIsSendInvoice(loadid))
            {
                if (GetInvoiceName(loadid))
                {
                    returnvalue = (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_Insert_Update_TrackLoad", new object[] { loadid, 12,
                                string.Empty,DateTime.Now,string.Empty,Convert.ToInt64(Session["UserLoginId"]),"I"}) > 0);
                    if (returnvalue)
                        ReportDisplay();

                    //SLine 2016 Enhancements
                    //For Sending the Invoice when the Load is Closed
                    SendInvoiceMails(loadid);
                    ShowAlert("success");
                }
                else
                {
                    ViewState["strList_LoadsWithNOdocs"] = loadid.ToString();
                    ShowAlert("NoInvoice");
                }
            }

            else
            {
                returnvalue = (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_Insert_Update_TrackLoad", new object[] { loadid, 12,
                                string.Empty,DateTime.Now,string.Empty,Convert.ToInt64(Session["UserLoginId"]),"I"}) > 0);
                if (returnvalue)
                    ReportDisplay();
                ShowAlert("NoSendInvoice");
            }
            
        }
        catch (Exception ex)
        {
            CommonFunctions.LogError(ex);
            throw new Exception(ex.Message);
        }

        return returnvalue;
    }

    private void ReportDisplay()
    {
        DataSet ds = new DataSet();
        object[] objParams = null;
        if (Convert.ToString(Session["SerchCriteria"]).Trim().Length > 0)
        {
            lblSearchCriteria.Visible = true;
            lblSearchCriteria.Text = "<br/><br/>" + Convert.ToString(Session["SerchCriteria"]);
        }
        else
        {
            lblSearchCriteria.Visible = false;
        }
        Session["TopHeaderText"] = "Loads With Invoiceable";

        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        string strParams = Convert.ToString(Session["ReportParameters"]) + "^" + officeLocationId.ToString();
        //SLine 2017 Enhancement for Office Location Ends
        //serch for paper work Pending(Madhav)
        objParams = strParams.Trim().Replace("~~^^^^~~", "^").Split('^');
        ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetLoadsWithInvoiceable", objParams);
        //serch for paper work Pending(Madhav)


        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {            
            DataTable dt = ds.Tables[0];
            FormatDataTable(ref dt);
            dggrid.DataSource = dt;
            dggrid.DataBind();
            //ReportDisplay(dt, strColumnsList, colWidths);
            if (dt != null) { dt.Dispose(); dt = null; }
        }
        else
        {
            dggrid.Visible = false;
            lblDisplay.Text = "<table width=100% border=0 cellpadding=0 cellspacing=0 id=ContentTbl><tr valign=top><td>" +
                "<table border=0 width=100% height=200px border=0 cellpadding=0 cellspacing=0 align=center valign=middle><tr><td width=100% align=center valign=middle>" +
                "<b><font color=blue>No Records To Display.<font></b></td></tr></table><br/><br/><br/></td></tr></table>";
        }
        if (ds != null) { ds.Dispose(); ds = null; }

        Session["PrintDetails"] = lblDisplay.Text.Trim();
        //Page.Controls.Add(new LiteralControl(strDisplay));       
    }

    protected void btnZipandDownload_Click(object sender, EventArgs e)
    {
        try
        {
            bool chkStatus = false;
            string loadID= string.Empty;
            msg.Text = string.Empty; 
            foreach(DataGridItem item in dggrid.Items)
            {
                if(((CheckBox) item.FindControl("chkPrint")).Checked)
                {
                    loadID = loadID +(string.IsNullOrEmpty(loadID)?string.Empty:",")+ item.Cells[0].Text;
                }
            }
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "Get_All_Documents", new object[] { loadID });

            using (ZipFile zip = new ZipFile())
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count;i++ )
                    {
                        if (File.Exists(Server.MapPath("~/Documents/" + ds.Tables[0].Rows[i][0] + "/" + ds.Tables[0].Rows[i][2])))
                        {
                            zip.AddFile(Server.MapPath("~/Documents/" + ds.Tables[0].Rows[i][0] + "/" + ds.Tables[0].Rows[i][2]), "");
                            chkStatus = true;
                        }
                    }
                }
                
                if (chkStatus)
                {
                    Response.Clear();
                    Response.BufferOutput = false;
                    String zipName = "Invoiceable_" + DateTime.Now.ToString("yyyy_MMM_dd_HHmmss") + ".zip";
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                    zip.Save(Response.OutputStream);
                    //Response.End();
                }
                else
                {
                    msg.Text = "No files to download";
                }

                if (loadID == string.Empty)
                {
                    msg.Text = "Please select atleast one checkbox";
                }
                   
            }
        }
        catch(Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void btnCloseLoads_Click(object sender, EventArgs e)
    {
        try
        {
            int chkstatus = 0;
            int counter = 0;
            int counterNoInvoice = 0;
            bool returnvalue = false;
            ViewState["strList_LoadsWithNOdocs"] = "";
            foreach (DataGridItem item in dggrid.Items)
            {
                if (((CheckBox)item.FindControl("chkPrint")).Checked)
                {
                    chkstatus++;

                    //to check if the this cust has opted for sendin the Invoice by email
                    if (CheckIsSendInvoice(int.Parse(item.Cells[0].Text)))
                    {
                        if (GetInvoiceName(int.Parse(item.Cells[0].Text)))
                        {
                            returnvalue = (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_Insert_Update_TrackLoad", new object[] { int.Parse(item.Cells[0].Text), 12,
                                   string.Empty,DateTime.Now,string.Empty,Convert.ToInt64(Session["UserLoginId"]),"I"}) > 0);
                            counter++;

                            //SLine 2016 Enhancements
                            //For Sending the Invoice when the Load is Closed
                            SendInvoiceMails(int.Parse(item.Cells[0].Text));
                        }

                        else
                        {
                            ViewState["strList_LoadsWithNOdocs"] = ViewState["strList_LoadsWithNOdocs"] + ", " + int.Parse(item.Cells[0].Text);
                            //Response.Write("<script>alert('Invoice not generated or saved as pdf. For Load#: " + int.Parse(item.Cells[0].Text) + "')</script>");
                        }
                    }

                    else
                    {
                        returnvalue = (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_Insert_Update_TrackLoad", new object[] { int.Parse(item.Cells[0].Text), 12,
                                   string.Empty,DateTime.Now,string.Empty,Convert.ToInt64(Session["UserLoginId"]),"I"}) > 0);
                        counter++;
                        counterNoInvoice++;
                    }
                }
            }

            if (chkstatus == 0)
            {
                msg.Text = "Please select atleast one checkbox";
            }
            else if (ViewState["strList_LoadsWithNOdocs"].ToString() == "" && counter != 0)
            {
                ShowAlert("success");
            }
            else if (ViewState["strList_LoadsWithNOdocs"].ToString() != "" && counter != 0)
            {
                ShowAlert("NoAllDocuments");
            }
            else if (ViewState["strList_LoadsWithNOdocs"].ToString() != "" && counter == 0)
            {
                ShowAlert("NoInvoice");
            }
            else if (ViewState["strList_LoadsWithNOdocs"].ToString() == "" && counterNoInvoice != 0)
            {
                ShowAlert("NoSendInvoice");
            }
            else if (ViewState["strList_LoadsWithNOdocs"].ToString() == "" && counterNoInvoice != 0 && counter != 0)
            {
                ShowAlert("NoSendInvoice");
            }
            ReportDisplay();
        }
        catch (Exception ex)
        {
            CommonFunctions.LogError(ex);
            throw new Exception(ex.Message);            
        }
    }

    protected void FormatDataTable(ref DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    if (Convert.ToString(dt.Rows[i][j]).Contains("^^^^"))
                    {
                        if (Convert.ToString(Session["ReportName"]) == "GetPayableEntries")
                        {
                            if (Convert.ToInt32(dt.Rows[i]["LoadStatus"]) != 12)
                            {
                                if (j == 4)
                                {
                                    dt.Rows[i][j] = FormatMultipleRowColumnsred(Convert.ToString(dt.Rows[i][j]).Trim());
                                }
                                else
                                {
                                    dt.Rows[i][j] = FormatMultipleRowColumns(Convert.ToString(dt.Rows[i][j]).Trim());
                                }
                            }
                            else
                            {
                                dt.Rows[i][j] = FormatMultipleRowColumns(Convert.ToString(dt.Rows[i][j]).Trim());
                            }

                        }
                        else
                        {
                            dt.Rows[i][j] = FormatMultipleRowColumns(Convert.ToString(dt.Rows[i][j]).Trim());
                        }
                    }
                }
            }
        }
    }
    public string FormatMultipleRowColumns(string strColValue)
    {
        string strFormat = "";
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0)
        {
            strFormat = "<table cellpadding=0 cellspacing=0 border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;>";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (i == 0)
                    strFormat += "<tr ><td  width=100% align=left valign=middle style=color:black;font-size:12;height:90px;>" + strMuls[i].Trim() + "</td></tr>";
                else
                {
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:black;font-size:12;height:90px;>" + strMuls[i].Trim() + "</td></tr>";
                }
            }
            strFormat += "</table>";
        }
        strMuls = null;
        return strFormat;
    }
    public string FormatMultipleRowColumnsred(string strColValue)
    {
        string strFormat = "";
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0)
        {
            strFormat = "<table cellpadding=0 cellspacing=0 border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;>";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (i == 0)
                    strFormat += "<tr ><td  width=100% align=left valign=middle style=color:black;font-size:12;height:90px;><span style='color:red;'>" + strMuls[i].Trim() + "</span></td></tr>";
                else
                {
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:black;font-size:12;height:90px;><span style='color:red;'>" + strMuls[i].Trim() + "</span></td></tr>";
                }
            }
            strFormat += "</table>";
        }
        strMuls = null;
        return strFormat;
    }


    //SLine 2016
    #region Sline 2016 Enhancements

    private void SendInvoiceMails(Int64 longLoadId)
    {
        //string strCmd = "SELECT eTn_Customer.nvar_BillingEmail FROM eTn_Customer inner join eTn_Load ON eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId WHERE (eTn_Load.bint_LoadId =" + longLoadId + " and eTn_Customer.bit_SendInvoice=1)";
        string strCmd = "SELECT eTn_Load.nvar_BillingEmail FROM eTn_Load inner join eTn_Customer ON eTn_Load.bint_CustomerId = eTn_Customer.bint_CustomerId WHERE (eTn_Load.bint_LoadId =" + longLoadId + " and eTn_Customer.bit_SendInvoice=1)";
        string strBillingEmail = DBClass.executeScalar(strCmd);        
        if (strBillingEmail != "")
        {
            //Getting the Document names for that LoadId
            DataSet ds_GetDocumentNames = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDocumentNames", new object[] { longLoadId });

            if (ds_GetDocumentNames.Tables.Count > 0 && ds_GetDocumentNames.Tables[0].Rows.Count > 0)
            {
                //Merging or Making a Single Pdf for all the 3-4 Documents and getting the name of the Invoice for that Customer/LoadId
                string[] str_InvoicePath = new string[1];

                str_InvoicePath[0] = CommonFunctions.MergeAllPdf(ds_GetDocumentNames, Server.MapPath("../Documents" + "/" + longLoadId), Server.MapPath("../Documents/temp/"), longLoadId);
                
                string[] DocumentsList = new string[ds_GetDocumentNames.Tables[0].Rows.Count];
                int i = 0;
                int[] arrDocumentId = { 11, 7, 4, 3, 1 };                
                foreach(DataRow dr in ds_GetDocumentNames.Tables[0].Rows)
                {
                    switch (int.Parse(ds_GetDocumentNames.Tables[0].Rows[i]["DocumentID"].ToString()))
                        {
                            case 11:
                                DocumentsList[i] = "Invoice";
                                break;
                            case 7:
                                DocumentsList[i] = "Proof of Delivery";
                                break;
                            case 4:
                                DocumentsList[i] = "Accessorial Charges";
                                break;
                            case 3:
                                DocumentsList[i] = "Bill of Lading";
                                break;
                            case 1:
                                DocumentsList[i] = "Load Order";
                                break;
                        }
                        i++;
                }
                
                //Sending the Mail
                CommonFunctions.SendEmail(GetFromXML.FromInvoiceEmailAddress, strBillingEmail, GetFromXML.CCInvoiceEmailAddress, "", GetInvoiceSubject(Convert.ToString(longLoadId), "", ""), GetInvoiceBodyDetails(DocumentsList), null, str_InvoicePath);
                DeleteFile(str_InvoicePath);
            }

            else
            {
                ViewState["strList_LoadsWithNOdocs"] = ViewState["strList_LoadsWithNOdocs"] + ", " +longLoadId.ToString();
            }        

        }
        else
        {
            Response.Write("<script>alert('Email not sent, Billing EMail id not available for Load# '" + longLoadId + ")</script>");
        }
    }

    private void DeleteFile(string[] filepaths)
    {
        if (File.Exists(filepaths[0]))
        {
            try
            {
                File.Delete(filepaths[0]);
            }
            catch
            {
            }
        }
    }

    //This method checks the Invoce name and whether the Invoice Exists in Documents    
    private bool GetInvoiceName(Int64 longLoadID)
    {
        string strDocumentName = "";
        try
        {
            strDocumentName = DBClass.executeScalar("select [nvar_DocumentName] as DocumentName from [eTn_UploadDocuments] where [bint_LoadId] = " + longLoadID + " and [bint_DocumentId] = 11 and [bint_FileNo] = 1");
            string[] strName = strDocumentName.Split('^');

            if (File.Exists(Server.MapPath("../Documents" + "\\" + longLoadID + "\\" + strName[0])))
            {
                return true;
            }

            else
            {
                return false;
            }

        }
        catch
        {
        }

        return false;
    }
    
    private string GetInvoiceSubject(string LoadID, string Container, string RefNo)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();

        strBody.Append("Re ");
        if (Container.Length > 0)
            strBody.Append("unit # " + Container);
        if (RefNo.Length > 0)
            strBody.Append(" ,  Ref #  " + RefNo);
        strBody.Append(". Invoice_" + LoadID + " copy attached");

        return strBody.ToString();
    }

    private string GetInvoiceBodyDetails(string[] DocNames)
    {
        System.Text.StringBuilder strBody = new System.Text.StringBuilder();
        strBody.Append("Dear Valued Customer, <br/>");

        string DocList = "";
        int i = 0;
        foreach (string docname in DocNames)
        {
            DocList = DocList + ", " + DocNames[i];
            i++;
        }
        string NewDocList1 = DocList.TrimStart(',');
        NewDocList1 = DocList.Trim(',');
        strBody.Append("<p>Please find the attached copy of" + NewDocList1 + ".</p>");
        strBody.Append("<p>We know you have a choice of carriers. Thanks for choosing " + GetFromXML.CompanyName + ". Please choose " + GetFromXML.CompanyName + " for your future transportation needs.</p>");
        strBody.Append("<p></p>Regards<br/>" + GetFromXML.CompanyName + "<br/>Accounting Dept.<br/>" + GetFromXML.CompanyPhone + "<br/>" + GetFromXML.CompanyFax);
        return strBody.ToString();
    }

    private string ShowAlert(string strSwitch)
    {
        string strMessage = "";
        switch (strSwitch)
        {
            case "success": 
                strMessage = "Load(s) closed and email sent successfully.";
                break;
            case "NoInvoice":
                strMessage = "Load cannot be Closed. Invoice not generated or saved as pdf. Load# :" + ViewState["strList_LoadsWithNOdocs"].ToString().TrimStart(',');
                break;
            case "NoBillingEmail":
                strMessage = "Load Closed. Email not sent, Billing EMail id not available";
                break;
            case "NoAllDocuments":
                strMessage = "No documents to attach. Not all Loads closed. Check docs for Loads# :" + ViewState["strList_LoadsWithNOdocs"].ToString().ToString().TrimStart(',');
                break;
            case "NoSendInvoice":
                strMessage = "Load(s) closed successfully.";
                break;
        }
        Response.Write("<script>alert('" + strMessage + "')</script>");
        return strMessage;
    }

    //this method will check if the Customer has opted for sending the Invoice by EMail or Not
    private bool CheckIsSendInvoice(Int64 longLoadID)
    {
        try
        {
           return Convert.ToBoolean(DBClass.executeScalar("SELECT eTn_Customer.bit_SendInvoice FROM eTn_Load INNER JOIN eTn_Customer ON eTn_Load.bint_CustomerId = eTn_Customer.bint_CustomerId where  eTn_Load.bint_LoadId=" + longLoadID));
        }

        catch(Exception e)
        {
            CommonFunctions.LogError(e);
            return false;
        }
    }

    #endregion

    


    //Checking is all the loads had documents
    //if (ViewState["strList_LoadsWithNOdocs"] == "success")
    //{
    //    Response.Write("<script>alert('Loads closed and email sent successfully.')</script>");
    //}
    //else
    //{
    //    Response.Write("<script>alert('No documents to attach. Loads closed but email was not sent to the below records." + ViewState["strList_LoadsWithNOdocs"].ToString() + "')</script>");
    //}

}

