using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.IO;
using System.Xml;
using FAXService;

//SLine 2016 Enhancements
using System.Collections.Generic;
using System.Linq;
using PdfSharp;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using PdfSharp.Drawing;
using System.Diagnostics;
using System.Drawing;
using Twilio.Rest.Api.V2010.Account;

public class CommonFunctions
{
    const string ORIGINCODES = "AL,CL,PA,PL,RT,SL,LD";
    const string SWP = "Stay with Pick";
    const string SWD = "Stay with Delivery";
    const string DEP = "Drop Empty at Pick";
    const string PLP = "Pull Loaded from Pick";
    const string DLD = "Drop Loaded at Delivery";
    public CommonFunctions()
    {

    }
    public static long FormatPhone(string strPhone)
    {
        if (strPhone.Trim().Length > 0)
            return Convert.ToInt64(Convert.ToString(strPhone).Trim().Replace("-", "").Replace(" ", "").Replace("(", "").Replace(")", "").Trim());
        else
            return 0;
    }
    public static string SetPhoneString(string strPhone)
    {
        if (strPhone.Trim().Length > 0 && strPhone.Trim().Length == 10)
        {
            return strPhone.Substring(0, 3) + "-" + strPhone.Substring(3, 3) + "-" + strPhone.Substring(6, 4);
        }
        else
            return "";
    }
    public static string AddressQueryString(string strColumnNames, string WrapColumnName)
    {
        string[] strColumns = strColumnNames.Trim().Replace(",", ";").Split(';');
        StringBuilder strReturn = new StringBuilder();
        for (int i = 0; i < strColumns.Length; i++)
        {
            if (i == 0)
                strReturn.Append(" " + strColumns[0]);
            else
                strReturn.Append(" + case when len(" + strColumns[i] + ")>0 then ('" + ((i == 1) ? "<br/>" : ", ") + "' + " + strColumns[i] + ") else '' end ");
        }
        if (strReturn.Length > 0)
            strReturn.Append(" as '" + WrapColumnName + "'");
        strColumns = null;
        return strReturn.ToString();
    }
    public static string AddressQueryStringWithLinks(string strColumnNames, string strPhoneColName, string strDate, string WrapColumnName, string LinkAddress, string Params, string LinkName, bool IsApptDateVisible)
    {
        string[] strColumns = strColumnNames.Trim().Replace(",", ";").Split(';');
        string[] strParams = Params.Split('^');
        StringBuilder strReturn = new StringBuilder();
        for (int i = 0; i < strColumns.Length; i++)
        {
            if (i == 0)
                strReturn.Append(" " + strColumns[0]);
            else
                strReturn.Append(" + case when len(" + strColumns[i] + ")>0 then ('" + ((i == 1) ? "<br/>" : ", ") + "' + " + strColumns[i] + ") else '' end ");
        }
        if (strReturn.Length > 0)
        {
            strReturn.Append(" + case when len(" + strPhoneColName + ")=10 then '<br/>Tel:' + substring(Convert(varchar(10)," + strPhoneColName + "),1,3) + '-' + substring(Convert(varchar(10)," + strPhoneColName + "),4,3) + '-' + substring(Convert(varchar(10)," + strPhoneColName + "),7,4) else '' end ");
        }
        if (IsApptDateVisible)
        {
            if (strDate.Trim().Length > 0)
                strReturn.Append(" + case when len(Convert(nvarchar(25)," + strDate + "))=0 or " + strDate + " is null then  '<br/><a href=" + LinkAddress.Trim() + "?'+" + Params + "+'>" + LinkName + "</a>'  else '<br/><a href=" + LinkAddress.Trim() + "?'+" + Params + "+'>" + LinkName + "</a>' end ");
        }
        if (strReturn.Length > 0)
            strReturn.Append(" as '" + WrapColumnName + "'");
        strColumns = null;
        return strReturn.ToString();
        //strReturn.Append("'<a href=" + RedirectPage + "?'+ " + strColumns[0] + " +'>'+ " + strColumns[0] + "+'</a>'");

    }
    public static string AddressQueryStringWithLinks(string strColumnNames, string strPhoneColName, string strDate, string WrapColumnName, string LinkAddress, string Params, string LinkName)
    {
        string[] strColumns = strColumnNames.Trim().Replace(",", ";").Split(';');
        string[] strParams = Params.Split('^');
        StringBuilder strReturn = new StringBuilder();
        for (int i = 0; i < strColumns.Length; i++)
        {
            if (i == 0)
                strReturn.Append(" " + strColumns[0]);
            else
                strReturn.Append(" + case when len(" + strColumns[i] + ")>0 then ('" + ((i == 1) ? "<br/>" : ", ") + "' + " + strColumns[i] + ") else '' end ");
        }
        if (strReturn.Length > 0)
        {
            strReturn.Append(" + case when len(" + strPhoneColName + ")=10 then '<br/>Tel:' + substring(Convert(varchar(10)," + strPhoneColName + "),1,3) + '-' + substring(Convert(varchar(10)," + strPhoneColName + "),4,3) + '-' + substring(Convert(varchar(10)," + strPhoneColName + "),7,4) else '' end ");
        }
        if (strDate.Trim().Length > 0)
            strReturn.Append(" + case when len(Convert(nvarchar(25)," + strDate + "))=0 or " + strDate + " is null then  '<br/><a href=" + LinkAddress.Trim() + "?'+" + Params + "+'>" + LinkName + "</a>'  else '' end ");
        if (strReturn.Length > 0)
            strReturn.Append(" as '" + WrapColumnName + "'");
        strColumns = null;
        return strReturn.ToString();
        //strReturn.Append("'<a href=" + RedirectPage + "?'+ " + strColumns[0] + " +'>'+ " + strColumns[0] + "+'</a>'");

    }
    public static string AddressQueryStringWithPhone(string strColumnNames, string strPhoneColName, string WrapColumnName)
    {
        string[] strColumns = strColumnNames.Trim().Replace(",", ";").Split(';');
        StringBuilder strReturn = new StringBuilder();
        for (int i = 0; i < strColumns.Length; i++)
        {
            if (i == 0)
                strReturn.Append(" " + strColumns[0]);
            else
                strReturn.Append(" + case when len(" + strColumns[i] + ")>0 then ('" + ((i == 1) ? "<br/>" : ", ") + "'+ " + strColumns[i] + ") else '' end ");
        }
        if (strReturn.Length > 0)
        {
            strReturn.Append(" + case when len(" + strPhoneColName + ")=10 then '<br/>Tel:' + substring(Convert(varchar(10)," + strPhoneColName + "),1,3) + '-' + substring(Convert(varchar(10)," + strPhoneColName + "),4,3) + '-' + substring(Convert(varchar(10)," + strPhoneColName + "),7,4) else '' end ");
            strReturn.Append(" as '" + WrapColumnName + "'");
        }
        strColumns = null;
        return strReturn.ToString();
    }
    public static string PhoneQueryString(string strColumnName, string DispColumnName)
    {
        return " case when len(" + strColumnName + ")=10 then substring(Convert(varchar(10)," + strColumnName + "),1,3) + '-' + substring(Convert(varchar(10)," + strColumnName + "),4,3) + '-' + substring(Convert(varchar(10)," + strColumnName + "),7,4) else '' end as '" + DispColumnName + "'";
    }
    public static string PhoneQueryString(string strColumnName)
    {
        return " case when len(" + strColumnName + ")=10 then '<br/>Cell:' + substring(Convert(varchar(10)," + strColumnName + "),1,3) + '-' + substring(Convert(varchar(10)," + strColumnName + "),4,3) + '-' + substring(Convert(varchar(10)," + strColumnName + "),7,4) else '' end";
    }
    public static string DateQueryString(string strDateColumn, string DispColumnName)
    {
        return " case when len(" + strDateColumn + ")>0 then convert(varchar(10)," + strDateColumn + ",101) else '' end as '" + DispColumnName + "'";
    }
    public static string StatusString(string strStatusColumn, string DispColumnName)
    {
        return " case when " + strStatusColumn + "=1 then 'Active' else 'InActive' end as '" + DispColumnName + "'";
    }
    public static string LoadColumnQueryString(string strColumnNames, string WrapColumnName, string RedirectPage)
    {
        string[] strColumns = strColumnNames.Trim().Split(';');
        StringBuilder strReturn = new StringBuilder();
        if (strColumns.Length == 3)
        {
            strReturn.Append("'<a href=" + RedirectPage + "?'+ " + strColumns[0] + " +'>'+" + strColumns[0] + "+'</a>'");
            strReturn.Append(" + case when len(" + strColumns[1] + ")>0 and len(" + strColumns[2] + ")>0 then ('<b><font color=blue><br/>(D)' + " + strColumns[1] + "+'</font></b>') + ('<b><font color=blue><br/>(P)' + " + strColumns[2] + "+'</font></b>')");
            strReturn.Append(" when len(" + strColumns[1] + ")>0 and len(" + strColumns[2] + ")=0 then ('<b><font color=blue><br/>' + " + strColumns[1] + "+'</font></b>')");
            strReturn.Append(" when len(" + strColumns[1] + ")=0 and len(" + strColumns[2] + ")>0 then ('<b><font color=blue><br/>' + " + strColumns[2] + "+'</font></b>') else '' end");
        }
        if (strReturn.Length > 0)
            strReturn.Append(" as '" + WrapColumnName + "'");
        strColumns = null;
        return strReturn.ToString();
    }
    public static object CheckDateTimeNull(string strDate)
    {
        if (strDate != null)
        {
            if (strDate.Trim().Length > 0)
                return Convert.ToDateTime(strDate);
            else return DBNull.Value;
        }
        else return DBNull.Value;
    }
    public static string FormatMultipleOrigins(string strColValue)
    {
        string strFormat = "";
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0)
        {
            strFormat = "<table cellpadding=0 cellspacing=0 border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;>";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (i == 0)
                    strFormat += "<tr ><td  width=100% align=left valign=middle style=color:black;font-size:12;height:90px;>" + strMuls[i].Trim() + "</td></tr>";
                else
                {
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:black;font-size:12;height:90px;>" + strMuls[i].Trim() + "</td></tr>";
                }
            }
            strFormat += "</table>";
        }
        strMuls = null;
        return strFormat;
    }
    public static string FormatMultipleOriginsNew(string strColValue)
    {
        string strFormat = "";
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0)
        {
            strFormat = "<table cellpadding=0 cellspacing=0 border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;>";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (i == 0)
                    strFormat += "<tr ><td border=0 width=100% align=left valign=middle style=color:black;font-size:12;height:90px;>" + strMuls[i].Trim() + "</td></tr>";
                else
                {
                    strFormat += "<tr><td border=0 width=100% align=left valign=middle style=color:black;font-size:12;height:90px;>" + strMuls[i].Trim() + "</td></tr>";
                }
            }
            strFormat += "</table>";
        }
        strMuls = null;
        return strFormat;
    }
    public static string OriginsWithPickUp(string strColumnNames, string WrapColumnName)
    {
        string[] strColumns = strColumnNames.Trim().Replace(",", ";").Split(';');
        StringBuilder strReturn = new StringBuilder();
        for (int i = 0; i < strColumns.Length; i++)
        {
            if (i == 0)
                strReturn.Append(" " + strColumns[0]);
            else
                strReturn.Append(" + case when len(" + strColumns[i] + ")>0 then ('" + ((i == 1) ? "<br/>" : ", ") + "' + " + strColumns[i] + ") else '' end ");
        }
        if (strReturn.Length > 0)
            strReturn.Append(" as '" + WrapColumnName + "'");
        strColumns = null;
        return strReturn.ToString();
    }
    //public static void SendEmail(string From,
    //   string To,
    //   string Cc,
    //   string Bcc,
    //   string Subject,
    //   string BodyMessage,
    //   string[] filepaths)
    //{
    //    int iRes = 0;
    //    try
    //    {
    //        MailMessage msg = new MailMessage();
    //        msg.From = new MailAddress(From);
    //        msg.To.Add(new MailAddress(To));
    //        msg.CC.Add(new MailAddress(Cc));
    //        msg.Bcc.Add(new MailAddress(Bcc));
    //        msg.Subject = Subject;
    //        msg.Body = BodyMessage;
    //        msg.IsBodyHtml = true;
    //        SmtpClient client = new SmtpClient(ConfigurationSettings.AppSettings["SmtpServer"], Convert.ToInt32(ConfigurationSettings.AppSettings["SmtpPort"]));
    //        client.EnableSsl = true;
    //        client.DeliveryMethod = SmtpDeliveryMethod.Network;
    //        System.Net.NetworkCredential oCredential = new System.Net.NetworkCredential(ConfigurationSettings.AppSettings["SmtpUserName"], ConfigurationSettings.AppSettings["SmtpPassword"]);
    //        client.UseDefaultCredentials = false;
    //        client.Credentials = oCredential;
    //        client.Send(msg);
    //        msg = null;
    //        client = null;
    //        iRes = 1;
    //    }
    //    catch (Exception ex)
    //    {
    //        iRes = -1;
    //    }
    //    return iRes;
    //}

    public static void SendEmail(string From,
       string To,
       string Cc,
       string Bcc,
       string Subject,
       string BodyMessage,
       string[] filepaths,
        string[] attachFilepaths)
    {
        int iRes = 1;
        //SLine 2016 ---- Updates
        //CommonFunctions Cmnf = new CommonFunctions();    
        try
        {
            //SLine 2016 Update ---- Added Port values, and User Credentials to the CLient for, Mail failure in One Domain
            SmtpClient client = null;
            if (string.IsNullOrEmpty(ConfigurationSettings.AppSettings["SmtpPort"]))
            {
                client = new SmtpClient(ConfigurationSettings.AppSettings["SmtpServer"].ToString());
            }
            else
            {
                client = new SmtpClient(ConfigurationSettings.AppSettings["SmtpServer"].ToString(), Convert.ToInt32(ConfigurationSettings.AppSettings["SmtpPort"]));
            }
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            System.Net.NetworkCredential oCredential = new System.Net.NetworkCredential(ConfigurationSettings.AppSettings["SmtpUserName"], ConfigurationSettings.AppSettings["SmtpPassword"]);
            //SLine 2016 Updates
            MailMessage msg = new MailMessage();
            bool IsToAddress = false;
            if (From.Trim().Length == 0)
            {
                From = ConfigurationSettings.AppSettings["SmtpUserName"];
            }
            msg.From = new MailAddress(From);
            try
            {
                msg.To.Add(new MailAddress(To));
            }
            catch
            {
                IsToAddress = true;
                if (Cc.Length == 0)
                    return;
            }
            if (Cc.Trim().Length > 0)
            {
                if (Cc.Contains(";"))
                {
                    string[] strEmails = Cc.Split(';');
                    if (To.Length == 0)
                    {
                        foreach (string Email in strEmails)
                        {
                            msg.To.Add(new MailAddress(Email));
                        }
                    }
                    else
                    {
                        foreach (string Email in strEmails)
                        {
                            msg.CC.Add(new MailAddress(Email));
                        }
                    }
                    strEmails = null;
                }
                else
                {
                    if (To.Length == 0)
                        msg.To.Add(new MailAddress(Cc));
                    else
                        msg.CC.Add(new MailAddress(Cc));
                }
            }
            if (Bcc.Trim().Length > 0)
                msg.Bcc.Add(new MailAddress(Bcc));
            msg.Subject = Subject;
            msg.Body = (IsToAddress == true ? filepaths[0] + BodyMessage : BodyMessage);
            if (attachFilepaths != null)
            {
                foreach (string path in attachFilepaths)
                {
                    try
                    {
                        Attachment ath = new Attachment(path);
                        msg.Attachments.Add(ath);
                    }
                    catch (Exception ex)
                    {
                        LogError(ex);
                    }
                }
            }

            msg.IsBodyHtml = true;
            //SLine 2016 Updates
            client.Credentials = oCredential;
            //SLine 2016 Updates
            client.Send(msg);
            if (msg != null) { msg.Dispose(); msg = null; }
            if (client != null) { client = null; }
        }
        catch (Exception ex)
        {
            //SLine 2016 Updates -- Added Error Catching
            LogError(ex);
            iRes = -1;
        }
        //return iRes;
    }

    public static void FormatDataTable(ref DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    if (Convert.ToString(dt.Rows[i][j]).Contains("^^^^"))
                    {
                        dt.Rows[i][j] = FormatMultipleRowColumns(Convert.ToString(dt.Rows[i][j]).Trim());
                    }
                }
            }
        }
    }
    public static string FormatMultipleRowColumns(string strColValue)
    {
        string strFormat = "";
        string[] strMuls = strColValue.Trim().Replace("^^^^", "^").Split('^');
        if (strMuls.Length > 0)
        {
            strFormat = "<table cellpadding=0 cellspacing=0 border=0 width=100% style=height:" + ((strMuls.Length * 90) + (strMuls.Length - 1)) + "px;>";
            for (int i = 0; i < strMuls.Length; i++)
            {
                if (i == 0)
                    strFormat += "<tr ><td  width=100% align=left valign=middle style=color:black;font-size:12;height:90px;>" + strMuls[i].Trim() + "</td></tr>";
                else
                {
                    strFormat += "<tr><td  width=100% align=left valign=middle style=color:black;height:1px;background-color:black;></td></tr>";
                    strFormat += "<tr><td width=100% align=left valign=middle style=color:black;font-size:12;height:90px;>" + strMuls[i].Trim() + "</td></tr>";
                }
            }
            strFormat += "</table>";
        }
        strMuls = null;
        return strFormat;
    }
    public static string ReportDisplay(DataTable dt, string[] strColumnsList, string[] colWidths)
    {
        string strDisplay = "";
        if (dt.Rows.Count > 0)
        {
            string strColProperties = " align=left valign=middle style=font-size:12px;border-top-width:0px;border-right-width:0px;border-color:black;padding-left:3px;padding-right:3px;background-color:#F3F8FC;font-family:Arial,Helvetica,sans-serif;";
            string strHeadProperties = " align=left valign=middle style=font-size:14px;border-top-width:0px;border-right-width:0px;border-color:black;background-color:lightblue;padding-left:4px;padding-right:4px;font-family:Arial,Helvetica,sans-serif;";
            string strTableProperties = " border=1 cellpading=0 cellspacing=0 width=100% align=center valign=top style=border-color:black;border-left-width:0px;border-bottom-width:0px;";//padding-left:2px;padding-right:2px;";
            strDisplay = "<table width=100% border=0 cellpadding=0 cellspacing=0 id=ContentTbl><tr valign=top><td>" +
                "<table " + strTableProperties + ">";
            for (int i = 0; i < strColumnsList.Length; i++)
            {
                if (i == 0)
                    strDisplay += "<tr>";
                strDisplay += "<th " + strHeadProperties + "width:" + colWidths[i] + "; align=left >" + strColumnsList[i] + "</th>";
                if (i == strColumnsList.Length - 1)
                    strDisplay += "</tr>";
            }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                strDisplay += "<tr >";
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    strDisplay += "<td" + strColProperties + ">";
                    strDisplay += Convert.ToString(dt.Rows[i][j]);
                    strDisplay += "</td>";
                }
                strDisplay += "</tr>";
            }
            strDisplay += "</table>" +
                "</td></tr></table>";
        }
        else
        {
            strDisplay = "<table width=100% border=0 cellpadding=0 cellspacing=0 id=ContentTbl><tr valign=top><td>" +
                "<table border=0 width=100% height=200px border=0 cellpadding=0 cellspacing=0 align=center valign=middle><tr><td width=100% align=center valign=middle>" +
                "<b><font color=blue>No Records To Display.<font></b></td></tr></table><br/><br/><br/></td></tr></table>";
        }
        return strDisplay;
        //Page.Controls.Add(new LiteralControl(strDisplay));       
    }

    public static void InsertLogEntry(string UserId)
    {
        DBClass.executeNonQuery("Insert into eTn_UserLogs([bint_UserLoginId],[date_LoginDate]) Select [bint_UserLoginId],getdate() from [eTn_UserLogin] where lower([nvar_UserId])=lower('" + UserId + "')");
    }

    public static string ReplaceSpecialCharacters(string msgText)
    {
        msgText = msgText.Replace("%", "%25").Replace("<", "%3C").Replace(">", "%3E").Replace("&", "%26").Replace("+", "%2B").Replace("#", "%23").Replace("*", "%2A").Replace("!", "%21").Replace(",", "%2C").Replace("�", "%27").Replace("\\", "%5C").Replace("=", "%3D").Replace("�", "%E2%82%AC").Replace("\"", "%22").Replace("?", "%3F").Replace("@", "%40").Replace("\r\n", "%0A");
        return msgText;
    }

    public static string ReverseReplaceSpecialCharacters(string msgText)
    {
        if (string.IsNullOrEmpty(msgText))
        {
            return string.Empty;
        }
        else
        {
            msgText = msgText.Replace("%25", "%").Replace("%3C", "<").Replace("%3E", ">").Replace("%26", "&").Replace("%2B", "+").Replace("%23", "#").Replace("%2A", "*").Replace("%21", "!").Replace("%2C", ",").Replace("%27", "�").Replace("%5C", "\\").Replace("%3D", "=").Replace("%E2%82%AC", "�").Replace("%22", "\"").Replace("%3F", "?").Replace("%40", "@").Replace("%3A", ":");
            return msgText;
        }
    }

    //SLine 2017 Enhancement for Office Location
    public static string SendSms(Int64 loadId, string to, string toNumber, string type, string messageText, Int64 userId, long officeLocationId, bool checkWhatsAppStatus)
    {
        string whatsAppStatus = string.Empty;
        if (bool.Parse(ConfigurationSettings.AppSettings["AlowSendSMS"].ToString().ToLower()))
        {
            StringBuilder errorMsg = new StringBuilder();
            errorMsg.Append(string.Empty);
            string userName = ConfigurationSettings.AppSettings["SMSApiKey"].ToString();
            string password = ConfigurationSettings.AppSettings["SMSApiPassword"].ToString();
            //string from = ConfigurationSettings.AppSettings["SMSApiFrom"].ToString();
            //Reading from site settings...
            string from = GetFromXML.NexmoSMSNumber.ToString();
            string ClientRef = ConfigurationSettings.AppSettings["SMSClientRef"].ToString();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            string webReq = "https://rest.nexmo.com/sms/xml?username=" + userName + "&password=" + password + "&from=" + from + "&to=" + toNumber.Trim() + "&status-report-req=1&client-ref=" + ClientRef + "&text=" + CommonFunctions.ReplaceSpecialCharacters(messageText.Trim());
            WebRequest myReq = WebRequest.Create(webReq);
            WebResponse res = myReq.GetResponse();
            Stream receiveStream = res.GetResponseStream();
            StreamReader reader = new StreamReader(receiveStream, Encoding.UTF8);
            string xmlResponse = reader.ReadToEnd();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlResponse);
            int msgCount = doc.SelectNodes("//messageId").Count;            
            if (msgCount > 0)
            {
                if(checkWhatsAppStatus == true)
                {
                    whatsAppStatus = SendWhatsAppSms(loadId, to, toNumber, type, messageText, userId, officeLocationId, false);                    
                }
                //SLine 2017 Enhancement for Office Location
                string SMSId = Convert.ToString(SqlHelper.ExecuteScalar(ConfigurationSettings.AppSettings["conString"], "sp_insert_eTn_SMSLog", new object[] { loadId, to, toNumber, type, messageText, userId, msgCount.ToString(), officeLocationId, 1, checkWhatsAppStatus, whatsAppStatus }));

                if (ConfigurationSettings.AppSettings["AlowSendWhatsApp"].ToString() == "true" && !String.IsNullOrEmpty(whatsAppStatus) && whatsAppStatus.Contains("Error"))
                {                    
                    errorMsg.AppendLine("WhatsApp Send Error");
                }

                if (!string.IsNullOrEmpty(SMSId))
                {
                    for (int i = 0; i < msgCount; i++)
                    {
                        string messageId = doc.SelectNodes("//messageId")[i].InnerText;
                        string toNum = doc.SelectNodes("//to")[i].InnerText;
                        string messagePrice = doc.SelectNodes("//messagePrice")[i].InnerText;
                        string remainingBalance = doc.SelectNodes("//remainingBalance")[i].InnerText;
                        string status = doc.SelectNodes("//status")[i].InnerText;
                        //SLine 2017 Enhancement for Office Location
                        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_insert_eTn_SMSLogDetails", new object[] { Convert.ToInt64(SMSId), messageId, toNum, messagePrice, remainingBalance, status, officeLocationId });
                    }
                }
                if (doc.SelectNodes("//errorText").Count > 0)
                {
                    XmlNodeList xmlErrorNodes = doc.SelectNodes("//errorText");
                    foreach (XmlNode xn in xmlErrorNodes)
                    {
                        errorMsg.AppendLine(xn.InnerText);
                    }
                }
            }
            else
            {
                if (checkWhatsAppStatus == true)
                {
                    whatsAppStatus = SendWhatsAppSms(loadId, to, toNumber, type, messageText, userId, officeLocationId, false);
                    if (!string.IsNullOrEmpty(whatsAppStatus))
                    {
                        string SMSId = Convert.ToString(SqlHelper.ExecuteScalar(ConfigurationSettings.AppSettings["conString"], "sp_insert_eTn_SMSLog", new object[] { loadId, to, toNumber, type, messageText, userId, msgCount.ToString(), officeLocationId, 1, 1, whatsAppStatus }));
                    }
                }
                //SLine 2017 Enhancement for Office Location
                if (ConfigurationSettings.AppSettings["AlowSendWhatsApp"].ToString() == "true" && !String.IsNullOrEmpty(whatsAppStatus) && whatsAppStatus.Contains("Error"))
                {
                    errorMsg.AppendLine("SMS Send Error");
                    errorMsg.AppendLine("WhatsApp Send Error");
                }
                else
                {
                    errorMsg.AppendLine("SMS Send Error");
                }
               
            }
                
            return errorMsg.ToString();
        }
        else
        {
            if (checkWhatsAppStatus == true)
            {
                whatsAppStatus = SendWhatsAppSms(loadId, to, toNumber, type, messageText, userId, officeLocationId, false);
                if (!string.IsNullOrEmpty(whatsAppStatus))
                {
                    string SMSId = Convert.ToString(SqlHelper.ExecuteScalar(ConfigurationSettings.AppSettings["conString"], "sp_insert_eTn_SMSLog", new object[] { loadId, to, toNumber, type, messageText, userId, 0, officeLocationId, 1, 1, whatsAppStatus }));
                }
            }
            return string.Empty;
        }
    }
    public static string SendWhatsAppSms(Int64 loadId, string driverName, string toNumber, string type, string messageText, Int64 userId, long officeLocationId, bool onlyWhatsApp)
    {
        string whatsAppStatus = string.Empty;
        if (ConfigurationSettings.AppSettings["AlowSendWhatsApp"].ToString() == "true")
        {
            try
            {
                string body = messageText.Trim();
                string AccountSID = ConfigurationSettings.AppSettings["WhatsAppSID"].ToString();
                string AuthToken = ConfigurationSettings.AppSettings["WhatsAppAuthToken"].ToString();
                Twilio.TwilioClient.Init(AccountSID, AuthToken);
                var message = MessageResource.Create(
                    body: body,
                    from: new Twilio.Types.PhoneNumber("WhatsApp:" + ConfigurationSettings.AppSettings["WhatsAppFromPhoneNumber"]),
                    to: new Twilio.Types.PhoneNumber("WhatsApp:+" + toNumber)
                    );
                whatsAppStatus = "Sent - " + message.Sid;
            }
            catch (Exception ex)
            {
                whatsAppStatus = "Whatsapp Error - " + ex.Message;
            }
            if (onlyWhatsApp)
            {
                string SMSId = Convert.ToString(SqlHelper.ExecuteScalar(ConfigurationSettings.AppSettings["conString"], "sp_insert_eTn_SMSLog", new object[] { loadId, driverName, toNumber, type, messageText, userId, 0, officeLocationId, 0, 1, whatsAppStatus }));
            }
        }
        return whatsAppStatus;

    }
    //SLine 2017 Enhancement for Office Location
    public static long SendFax(string faxNumber, string discription, Int64 userId, byte[] attachedFile, string fileType, Int64 loadID, string fileName, long officeLocationId)
    {
        InterFax inf = new InterFax();
        long returnValue = 0;
        try
        {
            System.Net.ServicePointManager.Expect100Continue = false;
            returnValue = inf.SendfaxEx(ConfigurationSettings.AppSettings["InterFaxUserName"].ToString(), ConfigurationSettings.AppSettings["InterFaxPassword"].ToString(), faxNumber, attachedFile, fileType, attachedFile.Length.ToString(), new DateTime(), true, "", ConfigurationSettings.AppSettings["SMSClientRef"].ToString(), "");
            //returnValue = inf.Sendfax(ConfigurationSettings.AppSettings["InterFaxUserName"].ToString(), ConfigurationSettings.AppSettings["InterFaxPassword"].ToString(), faxNumber, attachedFile, fileType);
            //SLine 2017 Enhancement for Office Location
            SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "sp_insert_eTn_FAXLog", new object[] { loadID, fileName, faxNumber, discription, userId, returnValue, officeLocationId });

        }
        catch (Exception ex)
        {
            throw;
        }
        return returnValue;
    }

    //SLine 2016 Enhancements
    //This method, Concatenate's all the .pdf files into One single and returns the Name of the file
    public static string MergeAllPdf(DataSet parameterValues, string strPath, string strPath2, Int64 big_LoadId)
    {
        string[] strDocumentName = new string[parameterValues.Tables[0].Rows.Count];
        string strInvoiceName = "";
        PdfDocument outputDocument = new PdfDocument();
        List<PdfDocument> pdflist = new List<PdfDocument>();
        CommonFunctions Cmnf = new CommonFunctions();

        try
        {
            if (parameterValues.Tables[0] != null && parameterValues.Tables[0].Rows.Count > 0)
            {

                for (int intCounter = 0; intCounter < strDocumentName.Length; intCounter++)
                {
                    string[] arrDocName = parameterValues.Tables[0].Rows[intCounter]["DocumentName"].ToString().Split('^');
                    strDocumentName[intCounter] = HttpContext.Current.Server.MapPath("../Documents/" + big_LoadId.ToString() + "/" + arrDocName[0]);
                }

                // Iterate files
                foreach (string file in strDocumentName)
                {
                    // Open the document to import pages from it.
                    PdfDocument inputDocument = PdfReader.Open(file, PdfDocumentOpenMode.Import);

                    // Iterate pages
                    int count = inputDocument.PageCount;
                    for (int idx = 0; idx < count; idx++)
                    {
                        // Get the page from the external document...
                        PdfPage page = inputDocument.Pages[idx];
                        // ...and add it to the output document.
                        outputDocument.AddPage(page);
                    }
                }
                // Save the document...
                strInvoiceName = strPath + "\\" + "Invoice_" + big_LoadId + ".pdf";
                outputDocument.Save(strInvoiceName);
                return strInvoiceName;
            }
        }

        catch (Exception ex)
        {
            LogError(ex);
            return strInvoiceName = "";
        }
        return strInvoiceName;
    }

    public static void LogError(Exception ex)
    {
        string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
        message += Environment.NewLine;
        message += "-----------------------------------------------------------";
        message += Environment.NewLine;
        message += string.Format("Message: {0}", ex.Message);
        message += Environment.NewLine;
        message += string.Format("StackTrace: {0}", ex.StackTrace);
        message += Environment.NewLine;
        message += string.Format("Source: {0}", ex.Source);
        message += Environment.NewLine;
        message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
        message += Environment.NewLine;
        message += "-----------------------------------------------------------";
        message += Environment.NewLine;
        string path = HttpContext.Current.Server.MapPath("ErrorLog.txt");
        using (StreamWriter writer = new StreamWriter(path, true))
        {
            writer.WriteLine(message);
            writer.Close();
        }
    }

    public static Customer ConvertEDILoadDetails(LoadDetail param, string SiteName)
    {
        Customer objcust = new Customer();
        objcust.SiteName = SiteName;
        objcust.TransactionId = param.TransactionId;
        objcust.orderNumber = param.OrderNumber;
        objcust.name = param.AggregatorCode;
        objcust.PartnerCode = param.PartnerCode;
        objcust.RailPickupNum = string.IsNullOrWhiteSpace(param.RailPickup) ? string.Empty : param.RailPickup;
        int offset = 0;
        if (param.Offset.HasValue)
            offset = Convert.ToInt32(param.Offset);
        if (param.ExpireDate.HasValue)
        {
            objcust.ExpireDate = param.ExpireDate.Value.AddMinutes(offset);
            objcust.ExpireDateString = objcust.ExpireDate.Value.ToString("MMM d yyyy h:mtt");
        }
        if (param.EquipmentDetails != null && param.EquipmentDetails.Count > 0)
        {
            objcust.ContainerNumber = (param.EquipmentDetails.FirstOrDefault().EquipmentNumber != null ? (param.EquipmentDetails.FirstOrDefault().EquipmentNumber) : string.Empty);
            objcust.Equipment = (param.EquipmentDetails.FirstOrDefault().EquipmentDescriptionCode != null ? (param.EquipmentDetails.FirstOrDefault().EquipmentDescriptionCode + " ") : string.Empty) +
                (param.EquipmentDetails.FirstOrDefault().EquipmentNumber != null ? (param.EquipmentDetails.FirstOrDefault().EquipmentNumber + " ") : string.Empty) +
                (param.EquipmentDetails.FirstOrDefault().EquipmentLength != null ? (param.EquipmentDetails.FirstOrDefault().EquipmentLength + " ") : string.Empty);

            objcust.TrailerNumber = (param.EquipmentDetails.FirstOrDefault().EquipmentInitial != null ? (param.EquipmentDetails.FirstOrDefault().EquipmentInitial) : string.Empty) + (param.EquipmentDetails.FirstOrDefault().EquipmentNumber != null ? (param.EquipmentDetails.FirstOrDefault().EquipmentNumber) : string.Empty);
            objcust.SealNumber = param.EquipmentDetails.FirstOrDefault().SealNumber;
            objcust.EquipmentLength = (param.EquipmentDetails.FirstOrDefault().EquipmentLength != null ? (param.EquipmentDetails.FirstOrDefault().EquipmentLength) : string.Empty);
        }
        objcust.EquipmentProvider = param.InterlineInformationAlphaCode;
        if (param.OrganizationDetails != null)
        {
            foreach (OrganizationDetails org in param.OrganizationDetails)
            {
                if (org.EntityIdentifierCode == "SH")
                {
                    objcust.customerName = org.Name;
                }
            }
        }
        objcust.AT5 = param.AT5;
        if (objcust.name.ToUpper().Contains("HUB"))
        {
            if (param.AT5 != null && param.AT5.Count > 0)
                if (param.AT5.Where(x => x != null && x.ToLower().Contains(DEP.ToLower())).Count() > 0)
                    objcust.TripType = "ROUND";
                else if (param.AT5.Where(x => x != null && x.ToLower().Contains(SWP.ToLower())).Count() > 0)
                    objcust.TripType = "PU";
                else if (param.AT5.Where(x => x != null && x.ToLower().Contains(SWD.ToLower())).Count() > 0)
                    objcust.TripType = "DL";
                else if (param.AT5.Where(x => x != null && x.ToLower().Contains(PLP.ToLower())).Count() > 0)
                    objcust.TripType = "PU";
                else if (param.AT5.Where(x => x != null && x.ToLower().Contains(DLD.ToLower())).Count() > 0)
                    objcust.TripType = "DL";
                else
                    objcust.TripType = "PU";
            else
                objcust.TripType = "PU";
        }
        else
        {
            string containerNumber = string.Empty;
            if (!string.IsNullOrWhiteSpace(objcust.ContainerNumber))
                objcust.TripType = "PU";
            else
                objcust.TripType = "DL";
        }
        objcust.remarks = string.Empty;
        if (param.Notes != null && param.Notes.Count > 0)
        {
            var notes = param.Notes;
            string remarks = string.Empty;
            foreach (string str in notes)
            {
                objcust.remarks += (str + Environment.NewLine);
            }
        }
        objcust.OriginDetails = new List<OriginDestination>();
        objcust.DestinationDetails = new List<OriginDestination>();
        if (param.Stops != null && param.Stops.Count > 0)
        {
            foreach (Stop od in param.Stops)
            {
                OriginDestination objod = new OriginDestination();
                objod.sequenceNumber = od.SequenceNumber;
                objod.reasonCode = od.ReasonCode;
                objod.CompanyName = od.OrganizationDetails.Name;
                objod.Weight = string.IsNullOrWhiteSpace(od.Weight) ? null : od.Weight;
                objod.Pieces = string.IsNullOrWhiteSpace(od.Pices) ? null : od.Pices;
                objod.AppNum = string.IsNullOrWhiteSpace(od.AppNum) ? null : (od.AppNum.Length > 50 ? od.AppNum.Substring(0, 50) : od.AppNum);
                objod.Address = (string.IsNullOrWhiteSpace(od.OrganizationDetails.Address1) ? string.Empty : od.OrganizationDetails.Address1);
                objod.Address1 = (string.IsNullOrWhiteSpace(od.OrganizationDetails.Address2) ? string.Empty : od.OrganizationDetails.Address2);
                objod.City = od.OrganizationDetails.CityName;
                objod.StateorProvinceCode = od.OrganizationDetails.StateorProvinceCode;
                objod.PostalCode = od.OrganizationDetails.PostalCode;
                objod.ContactPerson = od.OrganizationDetails.ContactName;
                objod.Tel = TelphoneNumber(od.OrganizationDetails.TeleNumber);
                objod.Fax = TelphoneNumber(od.OrganizationDetails.Fax);
                objod.Email = od.OrganizationDetails.Email;

                if (od.DatetimeDetails != null && od.DatetimeDetails.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(od.DatetimeDetails.First().Date) && !string.IsNullOrWhiteSpace(od.DatetimeDetails.First().Time))
                        objod.FromDate = DateTime.ParseExact(od.DatetimeDetails.First().Date + od.DatetimeDetails.First().Time, "yyyyMMddHHmmss", null);
                    if (od.DatetimeDetails.Count > 1 && !string.IsNullOrWhiteSpace(od.DatetimeDetails.Last().Date) && !string.IsNullOrWhiteSpace(od.DatetimeDetails.Last().Time))
                        objod.ToDate = DateTime.ParseExact(od.DatetimeDetails.Last().Date + od.DatetimeDetails.Last().Time, "yyyyMMddHHmmss", null);
                }
                if (ORIGINCODES.Contains(objod.reasonCode) || (objod.reasonCode == "DR" && param.AT5.Where(x => x.ToLower().Contains(SWD.ToLower()) || x.ToLower().Contains(DLD.ToLower())).Count() > 0))
                    objcust.OriginDetails.Add(objod);
                else
                    objcust.DestinationDetails.Add(objod);
            }
        }
        if (objcust.OriginDetails != null && objcust.OriginDetails.Count > 0)
        {
            objcust.PickupDates = objcust.Origins = "<table border='0' width='100% ' class='tendered-loads-subtable'>";
            foreach (OriginDestination org in objcust.OriginDetails)
            {
                objcust.Origins = objcust.Origins + "<tr><td width='100% ' align='left' valign='middle'>" + org.CompanyName + "</br>" + org.Address + " " + org.Address1 + ", " + org.City + ", " + org.StateorProvinceCode + ", " + org.PostalCode + (string.IsNullOrWhiteSpace(org.Tel) ? string.Empty : "</br>Tel: " + org.Tel) + "</td></tr>";
                objcust.PickupDates = objcust.PickupDates + "<tr><td width='100% ' align='left' valign='middle'>" + (org.FromDate != null ? org.FromDate.Value.ToString("MMM d yyyy h:mmtt") : string.Empty) + (org.ToDate != null ? (" to " + org.ToDate.Value.ToString("h:mmtt")) : string.Empty) + "</td></tr>";
            }
            objcust.PickupDates = objcust.PickupDates + "</table>";
            objcust.Origins = objcust.Origins + "</table>";
        }
        if (objcust.DestinationDetails != null && objcust.DestinationDetails.Count > 0)
        {
            objcust.DeliveryDates = objcust.Destinations = "<table border='0' width='100% ' class='tendered-loads-subtable'>";
            foreach (OriginDestination org in objcust.DestinationDetails)
            {
                objcust.Destinations = objcust.Destinations + "<tr><td width='100% ' align='left' valign='middle'>" + org.CompanyName + "</br>" + org.Address + " " + org.Address1 + ", " + org.City + ", " + org.StateorProvinceCode + ", " + org.PostalCode + (string.IsNullOrWhiteSpace(org.Tel) ? string.Empty : "</br>Tel: " + org.Tel) + "</td></tr>";
                objcust.DeliveryDates = objcust.DeliveryDates + "<tr><td width='100% ' align='left' valign='middle'>" + (org.FromDate != null ? org.FromDate.Value.ToString("MMM d yyyy h:mmtt") : string.Empty) + (org.ToDate != null ? (" to " + org.ToDate.Value.ToString("h:mmtt")) : string.Empty) + "</td></tr>";
            }
            objcust.DeliveryDates = objcust.DeliveryDates + "</table>";
            objcust.Destinations = objcust.Destinations + "</table>";
        }
        Charges objcharge = new Charges();
        if (param.WeightAndCharges != null)
        {
            objcharge.Charge = ConverttoMoney(param.WeightAndCharges.Charge);
            objcharge.Advances = ConverttoMoney(param.WeightAndCharges.Advances);
            objcharge.FreightRate = ConverttoMoney(param.WeightAndCharges.FreightRate);
            objcharge.Weight = ConverttoMoney(param.WeightAndCharges.Weight, true);
        }
        objcust.Charges = objcharge;
        return objcust;
    }
    private static string ConverttoMoney(string value, bool isNotMoney = false)
    {
        if (string.IsNullOrWhiteSpace(value))
            return string.Empty;
        else if (value.Trim().Length > 2)
            return (!isNotMoney ? "$" : string.Empty) + value.Trim().Substring(0, value.Trim().Length - 2) + "." + value.Trim().Substring(value.Trim().Length - 2);
        else if (value.Trim().Length == 2)
            return (!isNotMoney ? "$" : string.Empty) + "0." + value.Trim();
        else if (value.Trim().Length == 1)
            return (!isNotMoney ? "$" : string.Empty) + "0.0" + value.Trim();
        return value;

    }

    public static string RemoveCurencySymbol(string value)
    {
        if (string.IsNullOrWhiteSpace(value))
            return null;
        else
            return value.Replace("$", string.Empty).Trim();
    }

    public static string TelNumber(string value)
    {
        try
        {
            string tel = (string.IsNullOrWhiteSpace(value) ? "0" : value);
            tel = tel.Trim().Replace(") ", string.Empty).Replace(")", string.Empty).Replace("(", string.Empty).Replace("-", string.Empty).Split(' ')[0];
            if (tel.Length > 10)
                tel = tel.Substring(1, 10);
            long.Parse(tel);
            return tel;
        }
        catch
        {
            return "0";
        }
    }

    public static string FaxNumber(string value)
    {
        try
        {
            string tel = (string.IsNullOrWhiteSpace(value) ? null : value);
            tel = tel.Trim().Replace(") ", string.Empty).Replace(")", string.Empty).Replace("(", string.Empty).Replace("-", string.Empty).Split(' ')[0];
            if (tel.Length > 10)
                tel = tel.Substring(1, 10);
            long.Parse(tel);
            return tel;
        }
        catch
        {
            return null;
        }
    }

    public static string TelphoneNumber(string value)
    {
        if (string.IsNullOrWhiteSpace(value))
        {
            return string.Empty;
        }
        else
        {
            if (value.Contains("|"))
            {
                value = value.Replace("|", string.Empty).Trim();
                if (value.Length > 10)
                    value = value.Substring(1, 10);
                return value;
            }
            else
                return value;
        }
    }

    public static void InsertIntoInfoLog(string info)
    {
        if (ConfigurationSettings.AppSettings["IsInfoLog"].ToString().ToLower() == "true")
        {
            string strpath = System.Web.HttpRuntime.AppDomainAppPath + "\\InfoLog.txt";
            System.IO.FileInfo fileinfo = new System.IO.FileInfo(strpath);

            if (fileinfo.Exists)
            {
                System.IO.StreamWriter writer = fileinfo.AppendText();
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(info + " :: " + DateTime.Now.ToString("MM-dd-yyyy HH:mm:ss:fff"));
                writer.Write(sb.ToString());
                writer.Close();
            }
            else
            {
                System.IO.StreamWriter writer = System.IO.File.CreateText(fileinfo.FullName);
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(info + " :: " + DateTime.Now.ToString("MM-dd-yyyy HH:mm:ss:fff"));
                writer.Write(sb.ToString());
                writer.Close();
            }
        }
    }
}




