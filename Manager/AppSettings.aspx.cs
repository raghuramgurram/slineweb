using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Manager_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Site Settings";
        if (!IsPostBack)
            GetAppSettings();
    }

    private void GetAppSettings()
    {
        txtNotificationToEmail.Text = GetFromXML.NotificationToEmail;
        txtFromAccessorial.Text = GetFromXML.FromAccessrial;
        txtAdminEmail.Text = GetFromXML.AdminEmail;
        txtFromDriveronWaiting.Text = GetFromXML.FromDriveronWaiting;
        txtCcEmail.Text = GetFromXML.CcEmail;
        txtCompanyName.Text = GetFromXML.CompanyName;
        txtComapanyAddress.Text = GetFromXML.ComapanyAddress;
        string[] CityDetails=GetFromXML.ComapanyState.Split(',',' ');
        txtComapanyState.Text = CityDetails[0];
        txtCity.Text = CityDetails[2];
        txtZip.Text = CityDetails[3];
        txtPhone.Text = GetFromXML.CompanyPhone.Replace("Phone : ","").Replace("-","").Trim();
        txtFax.Text = GetFromXML.CompanyFax.Replace("Fax : ", "").Replace("-", "").Trim();
        txtAllowedIPAddresses.Text = GetFromXML.AllowedIPAddresses;
        txtNexmoSMSNumber.Text = GetFromXML.NexmoSMSNumber;
        txtPickupCCEmail.Text = GetFromXML.CCPickup;
        txtDriveronWaitingCCEmail.Text = GetFromXML.CCDriveronWaiting;
        txtAccessorialChargesCCEmail.Text = GetFromXML.CCAccessorial;
        txtFromLoadPlanner.Text = GetFromXML.FromLoadPlanner;
        txtPreviousYears.Text = GetFromXML.PreviousYearsSelection;
        //SLine 2016 Updates ---- Added new Properties for Street Turn and Others
        txtStreetTurnEmail.Text = GetFromXML.StreetFromAndToEMail;
        txtFromInvoiceEmailAddress.Text = GetFromXML.FromInvoiceEmailAddress;
        txtCCInvoiceEmailAddress.Text = GetFromXML.CCInvoiceEmailAddress;


        CityDetails = null;
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        GetFromXML.NotificationToEmail = txtNotificationToEmail.Text.Trim();
        GetFromXML.FromDriveronWaiting = txtFromDriveronWaiting.Text.Trim();
        GetFromXML.FromAccessrial = txtFromAccessorial.Text.Trim();
        GetFromXML.AdminEmail = txtAdminEmail.Text.Trim();
        GetFromXML.CcEmail = txtCcEmail.Text.Trim();
        GetFromXML.CompanyName = txtCompanyName.Text.Trim();
        GetFromXML.ComapanyAddress = txtComapanyAddress.Text.Trim();
        GetFromXML.ComapanyState = txtComapanyState.Text.Trim()+", "+txtCity.Text+" "+txtZip.Text;
        GetFromXML.CompanyPhone = CommonFunctions.SetPhoneString(txtPhone.Text.Trim());
        GetFromXML.CompanyFax = CommonFunctions.SetPhoneString(txtFax.Text.Trim());
        GetFromXML.AllowedIPAddresses = txtAllowedIPAddresses.Text.Trim();
        GetFromXML.NexmoSMSNumber = txtNexmoSMSNumber.Text.Trim();
        GetFromXML.CCPickup = txtPickupCCEmail.Text.Trim();
        GetFromXML.CCDriveronWaiting = txtDriveronWaitingCCEmail.Text.Trim();
        GetFromXML.CCAccessorial = txtAccessorialChargesCCEmail.Text.Trim();
        GetFromXML.FromLoadPlanner = txtFromLoadPlanner.Text.Trim();
        GetFromXML.PreviousYearsSelection = txtPreviousYears.Text;
        //SLine 2016 Updates ---- Added new Properties for Street Turn and Others
        GetFromXML.StreetFromAndToEMail = txtStreetTurnEmail.Text;
        GetFromXML.FromInvoiceEmailAddress = txtFromInvoiceEmailAddress.Text;
        GetFromXML.CCInvoiceEmailAddress = txtCCInvoiceEmailAddress.Text;
        Response.Redirect("~/Manager/DashBoard.aspx");
    }
}
