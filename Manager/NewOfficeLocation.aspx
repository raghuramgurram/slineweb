﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Manager/MasterPage.master"
    CodeFile="NewOfficeLocation.aspx.cs" Inherits="Manager_NewOfficeLocation" Title="S Line Transport Inc" %>

<%@ Register Src="../UserControls/Phone.ascx" TagName="Phone" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
        <tr valign="top">
            <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblform1">
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblform">
                    <tr>
                        <th colspan="2">
                            Office Location
                        </th>
                    </tr>
                    <tr id="row">
                        <td align="right">
                            Office Location Name :
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtOfficeLocationName" runat="server" Width="300px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtOfficeLocationName"
                                Display="Dynamic" ErrorMessage="Please enter office location name" ToolTip="Please enter office location name">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="altrow">
                        <td align="right">
                            Office Location Short Name :
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtOfficeLocationShortName" runat="server" Width="300px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtOfficeLocationShortName"
                                Display="Dynamic" ErrorMessage="Please enter office location short name" ToolTip="Please enter office location short name">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="row">
                        <td align="right">
                            Description :
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtDescription" runat="server" Width="300px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDescription"
                                Display="Dynamic" ErrorMessage="Please enter description" ToolTip="Please enter description">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="altrow">
                        <td align="right">
                            Active :
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlActive" runat="server">
                                <asp:ListItem Text="True" Value="True"></asp:ListItem>
                                <asp:ListItem Text="False" Value="False"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="row">
                        <td align="right">
                            Address :
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtAddress" runat="server" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="altrow">
                        <td align="right">
                            City :
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtCity" runat="server" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="row">
                        <td align="right">
                            State :
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtState" runat="server" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="altrow">
                        <td align="right">
                            Zip :
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtZip" runat="server" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="row">
                        <td align="right">
                            Phone # :
                        </td>
                        <td align="left">
                            <uc1:Phone ID="txtPhone" runat="server" IsRequiredField="false" Visible="true" />
                        </td>
                    </tr>
                    <tr id="altrow">
                        <td align="right">
                            Fax # :
                        </td>
                        <td align="left">
                            <uc1:Phone ID="txtFax" runat="server" IsRequiredField="false" Visible="true" />
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img alt="" height="5" src="../Images/pix.gif" width="1" />
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
                    <tr>
                        <td align="right">
                            &nbsp;<asp:Button ID="btnSave" runat="server" Height="22px" Text="Save" CssClass="btnstyle"
                                OnClick="btnSave_Click" />
                            <asp:Button ID="btnCancle" runat="server" CausesValidation="False" CssClass="btnstyle"
                                Height="22px" Text="Cancel" OnClick="btnCancel_Click" UseSubmitBehavior="False" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
