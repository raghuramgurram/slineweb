﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class OfficeLocation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Manage Office Location";
        DataSet locationDs = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["conString"], "Get_OfficeLocation_ForTopMenu", new object[] { Convert.ToInt64(Session["UserLoginId"]) });
        Session["LocationDS"] = locationDs;
        if (!Convert.ToBoolean(Session["IsAdmin"]))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert",
                "alert('You do not have permission to view this page.');window.location ='Dashboard.aspx';", true);
        }
        else
        {
            if (!IsPostBack)
            {
                gridCurrent.Visible = true;
                gridCurrent.DeleteVisible = true;
                gridCurrent.EditVisible = true;
                gridCurrent.TableName = "eTn_OfficeLocation";
                gridCurrent.Primarykey = "bint_OfficeLocationId";
                gridCurrent.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
                gridCurrent.ColumnsList = "bint_OfficeLocationId;nvar_OfficeLocationName;nvar_OfficeLocationShortName;nvar_Description;bit_Active;nvar_Address;nvar_City;nvar_State;nvar_Zip;" +
                    CommonFunctions.PhoneQueryString("num_Phone", "Phone") + ";" + CommonFunctions.PhoneQueryString("num_Fax", "Fax");
                gridCurrent.VisibleColumnsList = "nvar_OfficeLocationName;nvar_OfficeLocationShortName;nvar_Description;bit_Active;nvar_Address;nvar_City;nvar_State;nvar_Zip;Phone;Fax";
                gridCurrent.VisibleHeadersList = "Office Location Name;Office Location Short Name;Description;Active;Address;City;State;Zip;Phone;Fax";
                gridCurrent.DependentTablesList = "eTn_Shipping;eTn_Company;eTn_Load";
                gridCurrent.DependentTableKeysList = "bint_OfficeLocationId;bint_OfficeLocationId;bint_OfficeLocationId";
                gridCurrent.DeleteTablesList = "eTn_OfficeLocation_User;eTn_OfficeLocation";
                gridCurrent.DeleteTablePKList = "bint_OfficeLocationId;bint_OfficeLocationId";
                gridCurrent.EditPage = "~/Manager/NewOfficeLocation.aspx";
                gridCurrent.OrderByClause = "bint_OfficeLocationId";
                gridCurrent.IsPagerVisible = true;
                gridCurrent.BindGrid(0);
            }
        }
    }
}