using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Mobile_MyLoads : System.Web.UI.Page
{
    #region PageLoad and repiter Bind
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Keys.LOGIN_ID_KEY] == null || Session[Keys.EMPLOY_ID] == null)
        {
            Response.Redirect(Keys.PAGE_LOAGIN);
        }

        if (!IsPostBack)
        {
            Session[Keys.TAB_SELECTED] = PageType.Loads;
            Session[Keys.LOAD_ID] = null;
            MuPageMenu.SubTitle = ConfigurationManager.AppSettings["CompanyName"].ToString() + " - Your Loads";
            //MuPageMenu.Menuselected = PageType.Loads;
            MuPageMenu.LoadMenu();
            LoadsBase load = new LoadsBase();
            rptLoads.DataSource = load.LoadLoads(Convert.ToInt32(Session[Keys.EMPLOY_ID].ToString()));
            rptLoads.DataBind();
        }


    }
    protected void rptLoads_databind(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
        {
            HiddenField hfeald = e.Item.FindControl("hfldstatusid") as HiddenField;
            HiddenField hloadid = e.Item.FindControl("hfealdloadid") as HiddenField;
            Literal ltl = e.Item.FindControl("ltlvalueforstatus") as Literal;
            Literal ltLFD = e.Item.FindControl("ltlLFDdatetime") as Literal;
            Repeater rpt = e.Item.FindControl("rpeordate") as Repeater;



            if (hfeald != null && ltl != null)
            {
                int id = Convert.ToInt32(hfeald.Value.ToString());
                loadStatus value = ((loadStatus)id);

                if (value == loadStatus.Delivered)
                {
                    ltl.Text = " <label class='green'> " + value.ToString() + "</label>";
                }
                else
                {

                    if (value == loadStatus.En_route)
                        ltl.Text = " <label class='red'> " + value.ToString().Replace("_", "-") + "</label>";
                    else
                        ltl.Text = " <label class='red'> " + value.ToString().Replace("_", " ") + "</label>";
                }


            }

            int loadid = 0;
            if (hloadid != null)
            {
                if (!string.IsNullOrEmpty(hloadid.Value.ToString()))
                {
                    loadid = Convert.ToInt32(hloadid.Value.ToString());
                }
            }

            if (rpt != null)
            {
                LoadDetailsBase load = new LoadDetailsBase();
                DataTable tolist = load.loaddatesforapp(loadid);
                rpt.DataSource = tolist;
                rpt.DataBind();
            }

            if (ltLFD != null)
            {
                if (!string.IsNullOrEmpty(ltLFD.Text.Trim()))
                {
                    DateTime date = Convert.ToDateTime(ltLFD.Text);

                    ltLFD.Text = date.ToString("MMM dd yyyy");
                }
            }
        }
    }
    protected void rpeordate_databind(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
        {
            HiddenField hffrom = e.Item.FindControl("hfealdforaptdatetime") as HiddenField;
            HiddenField hfto = e.Item.FindControl("hfealdfortoaptdatetime") as HiddenField;
            Literal ltl = e.Item.FindControl("ltlAPTdatetime") as Literal;

            string datetimefordate = string.Empty;

            if (hffrom != null)
            {
                if (!string.IsNullOrEmpty(hffrom.Value.ToString()))
                {
                    DateTime formdate = Convert.ToDateTime(hffrom.Value.ToString());
                    datetimefordate = formdate.ToString("MMM dd yyyy hh:mm tt");
                }
            }
            if (hfto != null)
            {
                if (!string.IsNullOrEmpty(hfto.Value.ToString()))
                {
                    DateTime todate = Convert.ToDateTime(hfto.Value.ToString());
                    datetimefordate += " To " + todate.ToString("hh:mm tt");
                }
            }
            if (ltl != null)
            {
                ltl.Text = datetimefordate;
            }

        }
    }
    #endregion

    #region Button clicks
    protected void lnkloadetails_click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        if (lnk != null)
        {
            Session[Keys.LOAD_ID] = Convert.ToInt32(lnk.CommandArgument.ToString());
        }
        Response.Redirect(Keys.PAGE_LOAD_DETAILS);
    }
    protected void lnkupdateloadestatus_click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        if (lnk != null)
        {
            Session[Keys.LOAD_ID] = Convert.ToInt32(lnk.CommandArgument.ToString());
        }
        Response.Redirect(Keys.PAGE_CHANGE_STATUS);
    }
    protected void lnkuploaddocuments_click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        if (lnk != null)
        {
            Session[Keys.LOAD_ID] = Convert.ToInt32(lnk.CommandArgument.ToString());
        }
        Response.Redirect(Keys.PAGE_UPLOAD_DOCUMENTS);
    }
    #endregion
}
