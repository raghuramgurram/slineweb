<%@ Page Language="C#" MasterPageFile="~/Manager/MasterPage.master" AutoEventWireup="true"
    CodeFile="AppSettings.aspx.cs" Inherits="Manager_Default" Title="Untitled Page" %>

<%@ Register Src="../UserControls/Phone.ascx" TagName="Phone" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
        <tr valign="top">
            <td colspan="2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblform1">
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="49%" align="left" valign="top">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblform">
                                <tr>
                                    <th colspan="2">
                                        Email Settings
                                    </th>
                                </tr>
                                 <tr id="row">
                                    <td  colspan="2" align="left">
                                        The following email addresses are used in the FROM address of the emails sent by the system.
                                     </td>
                                </tr>
                                
                                <tr id="row">
                                    <td align="right">
                                        Notification Email Address :
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtNotificationToEmail" runat="server" Width="165px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtNotificationToEmail"
                                            Display="Dynamic" ErrorMessage="Please enter notification emailaddress" ToolTip="Please enter notification emailaddress">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtNotificationToEmail"
                                            Display="Dynamic" ErrorMessage="Please enter valid email id" ToolTip="Please enter valid email id"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        Equipment Email Address :
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtAdminEmail" runat="server" Width="165px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtAdminEmail"
                                            Display="Dynamic" ErrorMessage="Please enter equipment mail address" ToolTip="Please enter equipment mail address">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtAdminEmail"
                                            Display="Dynamic" ErrorMessage="Please enter valid email id" ToolTip="Please enter valid email id"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        Group Email Address :
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCcEmail" runat="server" Width="165px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtCcEmail"
                                            Display="Dynamic" ErrorMessage="Please enter group email address" ToolTip="Please enter group email address">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtCcEmail"
                                            Display="Dynamic" ErrorMessage="Please enter valid email id" ToolTip="Please enter valid email id"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
                                </tr>
                                <tr id="row">
                                    <td align="right">
                                        Driver On Waiting Email Address :
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFromDriveronWaiting" runat="server" Width="165px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtFromDriveronWaiting"
                                            Display="Dynamic" ErrorMessage="Please enter driveronwaiting email" ToolTip="Please enter driveronwaiting email">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtFromDriveronWaiting"
                                            Display="Dynamic" ErrorMessage="Please enter valid email id" ToolTip="Please enter valid email id"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        Accessorial Charges Email Address :
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFromAccessorial" runat="server" Width="165px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtFromAccessorial"
                                            Display="Dynamic" ErrorMessage="Please enter accessorialcharges email" ToolTip="Please enter accessorialcharges email">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtFromAccessorial"
                                            Display="Dynamic" ErrorMessage="Please enter valid email id" ToolTip="Please enter valid email id"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
                                </tr>
                                <tr id="row">
                                    <td align="right">
                                        Load Planner Email Address :
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFromLoadPlanner" runat="server" Width="165px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtFromLoadPlanner"
                                            Display="Dynamic" ErrorMessage="Please enter loadplanner email" ToolTip="Please enter loadplanner email">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtFromLoadPlanner"
                                            Display="Dynamic" ErrorMessage="Please enter valid email id" ToolTip="Please enter valid email id"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        Invoice Email Address :
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFromInvoiceEmailAddress" runat="server" Width="165px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtFromInvoiceEmailAddress"
                                            Display="Dynamic" ErrorMessage="Please enter Invoice email id" ToolTip="Please enter Invoice email id">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtFromInvoiceEmailAddress"
                                            Display="Dynamic" ErrorMessage="Please enter Invoice email id" ToolTip="Please enter Invoice email id"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
                                </tr>
                                <tr>
                                    <td colspan="2" height="12px;">
                                    </td>
                                </tr>        
                                <tr id="row">
                                    <td  colspan="2" align="left">
                                         The following email addresses are used in the CC address of the emails sent by the system.
                                    </td>
                                </tr>  
                                <tr id="row">
                                    <td align="right">
                                        Pickup Email Address :
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPickupCCEmail" runat="server" Width="165px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ReqtxtPickupCCEmail" runat="server" ControlToValidate="txtPickupCCEmail"
                                            Display="Dynamic" ErrorMessage="Please enter pickup emailaddress" ToolTip="Please enter pickup emailaddress">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegtxtPickupCCEmail" runat="server" ControlToValidate="txtPickupCCEmail"
                                            Display="Dynamic" ErrorMessage="Please enter valid email id" ToolTip="Please enter valid email id"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
                                </tr>  
                                <tr id="altrow">
                                    <td align="right">
                                        Driver On Waiting Email Address :
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtDriveronWaitingCCEmail" runat="server" Width="165px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ReqtxtDriveronWaitingCCEmail" runat="server" ControlToValidate="txtDriveronWaitingCCEmail"
                                            Display="Dynamic" ErrorMessage="Please enter driveronwaiting emailaddress" ToolTip="Please enter driveronwaiting emailaddress">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegtxtDriveronWaitingCCEmail" runat="server" ControlToValidate="txtDriveronWaitingCCEmail"
                                            Display="Dynamic" ErrorMessage="Please enter valid email id" ToolTip="Please enter valid email id"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
                                </tr> 
                                  <tr id="row">
                                    <td align="right">
                                        Accessorial Charges Email Address :
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtAccessorialChargesCCEmail" runat="server" Width="165px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ReqtxtAccessorialChargesCCEmail" runat="server" ControlToValidate="txtAccessorialChargesCCEmail"
                                            Display="Dynamic" ErrorMessage="Please enter accessorialcharges emailaddress" ToolTip="Please enter accessorialcharges emailaddress">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegtxtAccessorialChargesCCEmail" runat="server" ControlToValidate="txtAccessorialChargesCCEmail"
                                            Display="Dynamic" ErrorMessage="Please enter valid email id" ToolTip="Please enter valid email id"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
                                </tr>  
                                 <tr id="altrow">
                                    <td align="right">
                                        Invoice Email Address :
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCCInvoiceEmailAddress" runat="server" Width="165px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtCCInvoiceEmailAddress"
                                            Display="Dynamic" ErrorMessage="Please enter invoice email id" ToolTip="Please enter invoice email id">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtCCInvoiceEmailAddress"
                                            Display="Dynamic" ErrorMessage="Please enter invoice email id" ToolTip="Please enter invoice email id"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
                                </tr>  
                                <tr>
                                    <td colspan="2" height="12px;">
                                    </td>
                                </tr>    

                                 <%--SLine 2016 Updates Added a new Field--%> 
                                
                                 <tr id="row">
                                    <td  colspan="2" align="left">
                                         The following email address is used in FROM & TO email addresses to send the Street Turn email.
                                    </td>
                                </tr>    
                                
                                <tr id="Tr4">
                                    <td align="right">
                                        Street Turn Email Address:
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtStreetTurnEmail" runat="server" Width="165px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ReqtxtStreetTurnEmail" runat="server" ControlToValidate="txtStreetTurnEmail"
                                            Display="Dynamic" ErrorMessage="Please enter street turn email address" ToolTip="Please enter street turn email address">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegtxtStreetTurnEmail" runat="server" ControlToValidate="txtAccessorialChargesCCEmail"
                                            Display="Dynamic" ErrorMessage="Please enter valid email id" ToolTip="Please enter valid email id"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
                                </tr>  
                                
                                 <tr>
                                    <td colspan="2" height="12px;">
                                    </td>
                                </tr>
                                
                                
                                <%--SLine 2016 Updates--%>                
                            </table>
                        </td>                        
                        <td width="2%">
                        </td>
                        <td align="left" valign="top">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblform">
                                <tr>
                                    <th colspan="2">
                                        Company Settings
                                    </th>
                                </tr>
                                <tr id="row">
                                    <td align="right">
                                        Company Name :
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCompanyName" runat="server" Width="235px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtCompanyName"
                                            Display="Dynamic" ErrorMessage="Please enter company name" ToolTip="Please enter company name">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        Comapany Address :
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtComapanyAddress" runat="server" Width="235px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtComapanyAddress"
                                            Display="Dynamic" ErrorMessage="Please enter company address" ToolTip="Please enter company address">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr id="row">
                                    <td align="right">
                                        City :
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCity" runat="server" Width="235px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtCity"
                                            Display="Dynamic" ErrorMessage="Please enter city " ToolTip="Please enter city ">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        State :
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtComapanyState" runat="server" Width="235px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtComapanyState"
                                            Display="Dynamic" ErrorMessage="Please enter state" ToolTip="Please enter state">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr id="row">
                                    <td align="right">
                                        Zip :
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtZip" runat="server" Width="235px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtZip"
                                            Display="Dynamic" ErrorMessage="Please enter zip" ToolTip="Please enter zip">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        Company Phone # :
                                    </td>
                                    <td align="left">
                                        &nbsp;<uc1:Phone ID="txtPhone" runat="server" IsRequiredField="true" Visible="true" />
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td align="right">
                                        Company Fax # :
                                    </td>
                                    <td align="left">
                                        &nbsp;<uc1:Phone ID="txtFax" runat="server" IsRequiredField="true" Visible="true" />
                                    </td>
                                </tr>
                                
                                <tr id="altrow"><td align="right">&nbsp; </td><td align="left">&nbsp;</td></tr>
                                <%--<tr id="row"><td align="right">&nbsp; </td><td align="left">&nbsp;</td></tr>--%>
                                
                                <tr id="row">
                                    <td align="right" style="font-weight:bold;">
                                        Nexmo SMS # :
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtNexmoSMSNumber" runat="server" Width="235px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtNexmoSMSNumber"
                                            Display="Dynamic" ErrorMessage="Please enter Nexmo SMS number." ToolTip="Please enter Nexmo SMS number">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="Reg3" runat="server" ControlToValidate="txtNexmoSMSNumber" Display="Dynamic" 
                                            ErrorMessage="Please enter valid 11 digits in the Nexmo SMS number." ValidationExpression="\d\d\d\d\d\d\d\d\d\d\d">*</asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        No.of Previous Years for Selection :
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPreviousYears" runat="server" Width="235px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtPreviousYears"
                                            Display="Dynamic" ErrorMessage="Please enter zip" ToolTip="Please enter No.of Previous Years for Selection">*</asp:RequiredFieldValidator></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr><td height="10px;" colspan="3"></td></tr>
                    
                    <tr>
                        <td width="49%" align="left" valign="top" id="tblform">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                                <tr>
                                    <th colspan="2">
                                        Allowed IP Addresses
                                    </th>
                                </tr> 
                                <tr id="Tr1">
                                    <td align="center">
                                       <asp:TextBox ID="txtAllowedIPAddresses" TextMode="MultiLine" runat="server" Height="120" Width="400px"></asp:TextBox>
                                    </td>                                    
                                </tr>
                                <tr id="Tr2">
                                    <td align="center" style="font-size:xx-small">(ex:123.255.45.147,255.25.4.145)</td>
                                    </tr>                      
                            </table>
                        </td>
                        <td width="2%"></td>
                        <td width="49%"></td>
                        </tr>
                    
                    <tr><td height="10px;" colspan="3"></td></tr>
                    <tr>
                        <td colspan="3" align="center" valign="middle">
                            <asp:Button ID="Button1" runat="server" CssClass="btnStyle" Text="Save Settings"
                                OnClick="btnSave_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
