<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Url.ascx.cs" Inherits="Url" %>
<asp:TextBox ID="txtUrl" runat="server" MaxLength="100" Width="145px"></asp:TextBox><asp:RequiredFieldValidator ID="ReqUrl" runat="server" ControlToValidate="txtUrl"
    Display="Dynamic" ErrorMessage="Please enter web site Url." Enabled="False">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegUrl" runat="server" ControlToValidate="txtUrl"
    Display="Dynamic" ErrorMessage="Please enter valid Url." ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?">*</asp:RegularExpressionValidator>
