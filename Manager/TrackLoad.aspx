<%@ Page AutoEventWireup="true" CodeFile="TrackLoad.aspx.cs" Inherits="TrackLoad"
    Language="C#" MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/UserControls/Grid.ascx" TagName="Grid" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/MulFileUpload1.ascx" TagName="MulFileUpload1" TagPrefix="ucMultiFile" %>
<%@ Register Src="~/UserControls/Phone.ascx" TagName="Phone" TagPrefix="uc3" %>

<%@ Register Src="~/UserControls/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc12" %>
<%@ Register Src="~/UserControls/TimePicker.ascx" TagName="TimePicker" TagPrefix="uc13" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="ContentTbl">
        <tr valign="top">
            <td style="width: 100%">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblbox1">
                    <tr>
                        <td align="left">
                        </td>
                        <td align="right">
                            Track Load:
                            <asp:TextBox ID="txtContainer" runat="server" />
                            <asp:DropDownList ID="ddlContainer" runat="server">
                                <asp:ListItem Selected="True" Value="1">Container#</asp:ListItem>
                                <asp:ListItem Value="2">Ref#</asp:ListItem>
                                <asp:ListItem Value="3">Booking#</asp:ListItem>
                                <asp:ListItem Value="4">Chasis#</asp:ListItem>
                                <asp:ListItem Value="5">Load#</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Button ID="btnSearch" runat="server" CssClass="btnstyle" Text="Search" OnClick="btnSearch_Click" />
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img src="../Images/pix.gif" alt="" width="1" height="3" />
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <uc1:GridBar ID="LinksBar" runat="server" />
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img src="../Images/pix.gif" alt="" width="1" height="3" />
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="left" valign="top" style="width: 35%">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tbldisplay" onclick="return tbldisplay_onclick()">
                                <tr>
                                    <th colspan="2" style="width: 100%; text-align: inherit">
                                        <table id="tbldisplay" style="width: 100%; height: auto; border: none; color: #000;">
                                            <tr>
                                                <td style="width: 50%; font-weight: bold;">
                                                    Identification -&nbsp;<asp:Label ID="lblLoadId" runat="server" Text="LoadId" Width="50%" />&nbsp;
                                                </td>
                                                <td style="width: 20%">
                                                    <%--new coloumns add by madhav for Hazmat --%>
                                                    <span id="spanishazmat" runat="server" style="font-weight: bold; color: Red; text-align: left;
                                                        width: 20%">HAZMAT</span>
                                                    <%--end--%>
                                                </td>
                                                <td style="width: 10%">
                                                </td>
                                                <td style="width: 10%; text-align: right; padding-right: 8px">
                                                    <%--new coloumns add by Akhtar for SLine 2016 Enhancements--%>
                                                    <span id="spanRailContainer" runat="server" style="font-weight: bold; color: Red;
                                                        width: 10%">Rail</span>
                                                    <%--end--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                <tr id="row">
                                    <td width="40%" align="right" style="height: 20px">
                                        Ref.# : &nbsp;
                                    </td>
                                    <td width="60%" style="height: 20px">
                                        <asp:Label ID="lblRef" runat="server"></asp:Label>&nbsp;
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" width="40%">
                                        Assigned To :
                                    </td>
                                    <td width="60%">
                                        &nbsp;<asp:Label ID="lblAssigned" runat="server" />
                                        <%--<uc1:GridBar ID="GridBar2" runat="server" HeaderText="Load Status Information"/>--%>
                                        <%--<uc1:GridBar ID="GridBar2" runat="server" HeaderText="Load Status Information"/>--%>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tbldisplay">
                                <tr>
                                    <th colspan="2" style="width: 100%; text-align: inherit">
                                        <table id="tbldisplay" style="width: 100%; height: auto; border: none; color: #000;">
                                            <tr>
                                                <td style="width: 75%; text-align: left; font-weight: bold">
                                                    Order Status:
                                                </td>
                                                <td style="width: 24%; text-align: right">
                                                    <asp:Label ID="lblTripType" runat="server" Text="ROUND" Visible="false"></asp:Label>&nbsp;<asp:Image ID="imgTripType" runat="server" Visible="false"/>
                                                </td>
                                                <td style="width: 1%; text-align: right"></td>
                                            </tr>
                                        </table>
                                    </th>
                                   
                                </tr>
                                <tr id="row">
                                    <td width="40%" align="right">
                                        Load Status :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblStatus" runat="server" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        Container Size/Location :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblRailBill" runat="server" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td align="right">
                                        Order Bill Date :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblBillDate" runat="server" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        Person Bill :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPersonBill" runat="server" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td align="right">
                                        Booking Problem :
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkBookingProblem" runat="server" OnCheckedChanged="chkBookingProblem_CheckedChanged"
                                            AutoPostBack="true"></asp:CheckBox>&nbsp;
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        Put on Hold :
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkPutonHold" runat="server" OnCheckedChanged="chkPutonHold_CheckedChanged"
                                            AutoPostBack="true"></asp:CheckBox>&nbsp;
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td align="right">
                                        Chassis Rent :
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkChassisRent" runat="server" OnCheckedChanged="chkChassisRent_CheckedChanged"
                                            AutoPostBack="true"></asp:CheckBox>&nbsp;
                                    </td>
                                </tr>                                
                                <tr id="altrow">
                                    <td align="right">
                                        Hot Shipment :
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkHotShipment" runat="server" OnCheckedChanged="chkHotShipment_CheckedChanged"
                                            AutoPostBack="true"></asp:CheckBox>&nbsp;
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td align="right">
                                        Is Accessorial Charges :
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkAccessorialCharges" runat="server" OnCheckedChanged="chkAccessorialCharges_CheckedChanged"
                                            AutoPostBack="true"></asp:CheckBox>&nbsp;
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        Is Export Load :
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkExportLoad" runat="server" OnCheckedChanged="chkExportLoad_CheckedChanged"
                                            AutoPostBack="true"></asp:CheckBox>&nbsp;
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tbldisplay">
                                <tr>
                                    <th colspan="2">
                                        Routing
                                    </th>
                                </tr>
                                <tr id="row">
                                    <td colspan="2" style="height: 75px">
                                        <%--  <asp:PlaceHolder ID="plhDocs" runat="server"></asp:PlaceHolder>--%>
                                        <asp:Label ID="lblRoute" runat="server" Text="" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td width="40%" align="right">
                                        Pier Termination / Empty Return:
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPier" runat="server" Text=""></asp:Label>&nbsp;
                                    </td>
                                </tr>
                                <tr valign="top" id="row">
                                    <td align="right">
                                        Contact Person(s) :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblShippingContact" runat="server" Text=""></asp:Label>&nbsp;
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="maroonBar">
                                <tr>
                                    <td align="left" Width="60%">
                                        Documents
                                        <%--<uc1:GridBar ID="GridBar2" runat="server" HeaderText="Load Status Information"/>--%>
                                    </td>

                                    <div id="divOverdue" runat="server">
                                    <td style="Width: 30%; text-align: right; vertical-align: middle" >
                                    <asp:CheckBox ID="chkOverdue" runat="server" AutoPostBack="true" 
                                            oncheckedchanged="chkOverdue_CheckedChanged"></asp:CheckBox>
                                    </td>
                                    <td style="Width: 10%; text-align: left; vertical-align: middle" >
                                    Overdue
                                    </td>
                                    </div>
                                    <div id="divVerifiedChkBox" runat="server">
                                    <td style="Width: 30%; text-align: right; vertical-align: middle" >
                                    <asp:CheckBox ID="chkIsVerified" runat="server" AutoPostBack="true" 
                                            oncheckedchanged="chkIsVerified_CheckedChanged"></asp:CheckBox>
                                    </td>
                                    <td style="Width: 10%; text-align: left; vertical-align: middle" >
                                    Verified
                                    </td>
                                    </div>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tbldisplay">
                                <tr>
                                    <th style="width: 45%">
                                        Document Type
                                    </th>
                                    <th style="width: 23%; border-right: 1px solid #787878;">
                                        &nbsp;
                                    </th>
                                    <th style="width: 32%">
                                        Date Received
                                    </th>
                                </tr>
                                <tr id="row">
                                    <td align="right">
                                        <asp:LinkButton ID="lnkLoadOrder" runat="server" OnClick="lnkLoadOrder_Click">Load Order</asp:LinkButton>
                                    </td>
                                    <td style="height: 16px; width: 40px;">
                                        <asp:ImageButton ID="ImgLoadOrder" CommandArgument="1$lblLoadOrder" CommandName="Load Order"
                                            runat="server" ToolTip="Upload" ImageUrl="~/Images/upload-icon.png" OnClick="FileUploadwithFiles"
                                            Style="width: 16px;" Height="16px"></asp:ImageButton>
                                        <asp:ImageButton ID="ImgLoadOrderFax" runat="server" CommandArgument="1" CommandName="Load Order"
                                            ToolTip="Send Fax" ImageUrl="~/Images/FAX.png" OnClick="SendFaxWithFiles" Style="width: 16px;"
                                            Height="16px"></asp:ImageButton>
                                    </td>
                                    <td style="height: 16px; width: 115px;">
                                        <asp:Label ID="lblLoadOrder" runat="server" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        <asp:LinkButton ID="lnlPoOrder" runat="server" OnClick="lnlPoOrder_Click">Bare Chassis In-Gate/Out-Gate</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImgPoOrder" runat="server" CommandArgument="2$lblPOrder" CommandName="Purchase Order"
                                            ToolTip="Upload" ImageUrl="~/Images/upload-icon.png" OnClick="FileUploadwithFiles"
                                            Style="height: 16px;"></asp:ImageButton>
                                        <asp:ImageButton ID="ImgPoOrderFax" runat="server" CommandArgument="2" CommandName="Purchase Order"
                                            ToolTip="Send Fax" ImageUrl="~/Images/FAX.png" OnClick="SendFaxWithFiles" Style="width: 16px;"
                                            Height="16px"></asp:ImageButton>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPOrder" runat="server" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td align="right">
                                        <asp:LinkButton ID="lnkLading" runat="server" OnClick="lnkLading_Click">Bill Of Lading</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImgLading" runat="server" CommandArgument="3$lblBillofLading"
                                            CommandName="Bill Of Lading" ToolTip="Upload" ImageUrl="~/Images/upload-icon.png"
                                            OnClick="FileUploadwithFiles" Style="width: 16px; height: 16px"></asp:ImageButton>
                                        <asp:ImageButton ID="ImgLadingFax" runat="server" CommandArgument="3" CommandName="Bill Of Lading"
                                            ToolTip="Send Fax" ImageUrl="~/Images/FAX.png" OnClick="SendFaxWithFiles" Style="width: 16px;"
                                            Height="16px"></asp:ImageButton>
                                        <asp:LinkButton ID="lnkLadingV" runat="server" CommandArgument="3" OnClick="Verified_Click"
                                            Style="vertical-align: top" ForeColor="blue">Verified</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblBillofLading" runat="server" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        <asp:LinkButton ID="lnkAccessorial" runat="server" OnClick="lnkAccessorial_Click">Accessorial Charges</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImgAccessorial" runat="server" CommandArgument="4$lblAccessorial"
                                            CommandName="Accessorial Charges" ToolTip="Upload" ImageUrl="~/Images/upload-icon.png"
                                            OnClick="FileUploadwithFiles"></asp:ImageButton>
                                        <asp:ImageButton ID="ImgAccessorialMail" runat="server" CommandArgument="4" CommandName="Accessorial Charges"
                                            ToolTip="Send Mail" ImageUrl="~/Images/mail_icon.jpg" OnClick="SendMailwithFiles"
                                            Style="width: 16px;" Height="16px"></asp:ImageButton>
                                        <asp:ImageButton ID="ImgAccessorialFax" runat="server" CommandArgument="4" CommandName="Accessorial Charges"
                                            ToolTip="Send Fax" ImageUrl="~/Images/FAX.png" OnClick="SendFaxWithFiles" Style="width: 16px;"
                                            Height="16px"></asp:ImageButton>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblAccessorial" runat="server" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td align="right">
                                        <asp:LinkButton ID="lnkOutgate" runat="server" OnClick="lnkOutgate_Click">Out-Gate Interchange</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImgOutgate" runat="server" CommandArgument="5$lblOutGate" CommandName="Out-Gate Interchange"
                                            ToolTip="Upload" ImageUrl="~/Images/upload-icon.png" OnClick="FileUploadwithFiles" />
                                        <asp:ImageButton ID="ImgOutgateFax" runat="server" CommandArgument="5" CommandName="Out-Gate Interchange"
                                            ToolTip="Send Fax" ImageUrl="~/Images/FAX.png" OnClick="SendFaxWithFiles" Style="width: 16px;"
                                            Height="16px"></asp:ImageButton>
                                        <asp:LinkButton ID="lnkOutGateV" runat="server" CommandArgument="5" OnClick="Verified_Click"
                                            Style="vertical-align: top;" ForeColor="blue">Verified</asp:LinkButton>
                                    </td>
                                    <td style="height: 16px">
                                        <asp:Label ID="lblOutGate" runat="server" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        <asp:LinkButton ID="lnkInGate" runat="server" OnClick="lnkInGate_Click">In-Gate Interchange</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImgInGate" runat="server" CommandArgument="6$lblInGate" CommandName="In-Gate Interchange"
                                            ToolTip="Upload" ImageUrl="~/Images/upload-icon.png" OnClick="FileUploadwithFiles" />
                                        <asp:ImageButton ID="ImgInGateFAX" runat="server" CommandArgument="6" CommandName="In-Gate Interchange"
                                            ToolTip="Send Fax" ImageUrl="~/Images/FAX.png" OnClick="SendFaxWithFiles" Style="width: 16px;"
                                            Height="16px"></asp:ImageButton>
                                        <asp:LinkButton ID="lnkInGateV" runat="server" CommandArgument="6" OnClick="Verified_Click"
                                            Style="vertical-align: top;" ForeColor="blue">Verified</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblInGate" runat="server" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td align="right">
                                        <asp:LinkButton ID="lnkProof" runat="server" OnClick="lnkProof_Click">Proof of Delivery</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImgProof" runat="server" CommandArgument="7$lblDelivery" CommandName="Proof of Delivery"
                                            ToolTip="Upload" ImageUrl="~/Images/upload-icon.png" OnClick="FileUploadwithFiles"
                                            Style="height: 16px" />
                                        <asp:ImageButton ID="ImgProofMail" runat="server" CommandArgument="7" CommandName="Proof of Delivery"
                                            ToolTip="Send Mail" ImageUrl="~/Images/mail_icon.jpg" OnClick="SendMailwithFiles"
                                            Style="width: 16px;" Height="16px"></asp:ImageButton>
                                        <asp:ImageButton ID="ImgProofFAX" runat="server" CommandArgument="7" CommandName="Proof of Delivery"
                                            ToolTip="Send Fax" ImageUrl="~/Images/FAX.png" OnClick="SendFaxWithFiles" Style="width: 16px;"
                                            Height="16px"></asp:ImageButton>
                                        <asp:LinkButton ID="lnkProofV" runat="server" CommandArgument="7" OnClick="Verified_Click"
                                            Style="vertical-align: top;" ForeColor="blue">Verified</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblDelivery" runat="server" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        <asp:LinkButton ID="lnkScale" runat="server" OnClick="lnkScale_Click">Scale Ticket</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImgScale" runat="server" CommandArgument="8$lblTicket" CommandName="Scale Ticket"
                                            ToolTip="Upload" ImageUrl="~/Images/upload-icon.png" OnClick="FileUploadwithFiles"
                                            Style="height: 16px" />
                                        <asp:ImageButton ID="ImgScaleFax" runat="server" CommandArgument="8" CommandName="Scale Ticket"
                                            ToolTip="Send Fax" ImageUrl="~/Images/FAX.png" OnClick="SendFaxWithFiles" Style="width: 16px;"
                                            Height="16px"></asp:ImageButton>
                                        <asp:LinkButton ID="lnkScaleV" runat="server" CommandArgument="8" OnClick="Verified_Click"
                                            Style="vertical-align: top;" ForeColor="blue">Verified</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblTicket" runat="server" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td align="right">
                                        <asp:LinkButton ID="lnkLumper" runat="server" OnClick="lnkLumper_Click">Lumper Receipt</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImgLumper" runat="server" CommandArgument="9$lblLumperReceipt"
                                            CommandName="Lumper Receipt" ToolTip="Upload" ImageUrl="~/Images/upload-icon.png"
                                            OnClick="FileUploadwithFiles" Style="height: 16px" />
                                        <asp:ImageButton ID="ImgLumperFax" runat="server" CommandArgument="9" CommandName="Lumper Receipt"
                                            ToolTip="Send Fax" ImageUrl="~/Images/FAX.png" OnClick="SendFaxWithFiles" Style="width: 16px;"
                                            Height="16px"></asp:ImageButton>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblLumperReceipt" runat="server" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right">
                                        <asp:LinkButton ID="lnkMisc" runat="server" OnClick="lnkMisc_Click">Misc.</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImgMisc" runat="server" CommandArgument="10$lblMisc" CommandName="Misc."
                                            ToolTip="Upload" ImageUrl="~/Images/upload-icon.png" OnClick="FileUploadwithFiles"
                                            Style="height: 16px" />
                                        <asp:ImageButton ID="ImgMiscFax" runat="server" CommandArgument="10" CommandName="Misc."
                                            ToolTip="Send Fax" ImageUrl="~/Images/FAX.png" OnClick="SendFaxWithFiles" Style="width: 16px;"
                                            Height="16px"></asp:ImageButton>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblMisc" runat="server" />&nbsp;
                                    </td>
                                </tr>

                                <%--SLIne 2016----Adding Link for Invoice --%>
                                 <tr id="row">
                                    <td align="right">
                                        <%--<asp:HyperLink ID="hplInvoice" runat="server" Target="_blank" Font-Underline="true" ToolTip="Show pdf">Invoice</asp:HyperLink>--%>
                                        <asp:LinkButton ID="lnkShowInvoicePdf" runat="server" OnClick="ShowInvoicePdf">Invoice</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="imgSendFaxInvoice" runat="server" CommandArgument="11" CommandName="Invoice"
                                            ToolTip="Send Fax" ImageUrl="~/Images/FAX.png" OnClick="SendFaxWithFiles" Style="width: 16px;"
                                            Height="16px"></asp:ImageButton>
                                            <asp:ImageButton ID="imgSendMail" runat="server" CommandArgument="11" CommandName="Invoice"
                                            ToolTip="Send Mail" ImageUrl="~/Images/mail_icon.jpg" OnClick="SendMailwithFiles"
                                            Style="width: 16px;" Height="16px"></asp:ImageButton>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblInvoiceDate" runat="server" />&nbsp;
                                    </td>
                                </tr>
                                <%--SLIne 2016----Adding Link for Invoice --%>

                            </table>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <img src="../Images/pix.gif" alt="" width="1" height="3" />
                                    </td>
                                </tr>
                            </table>
                            <asp:Panel ID="pnlPhDocs" runat="server" Visible="false">
                                <table cellpadding="0" runat="server" cellspacing="0" border="0" id="tbldisplay">
                                    <tr id="row">
                                        <td>
                                            <asp:PlaceHolder ID="plhDocs" runat="server"></asp:PlaceHolder>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <%-- <asp:Panel ID="pnlUploads" runat="server" Visible="false" >
                             
                                        <table border="1"   cellpadding="0" id="tbldisplay"  cellspacing="0">
                                            <tr >
                                                <th style="height: 24px">
                                                    <asp:Label ID="lblUploadText" runat="server">Upload - </asp:Label>
                                                    &nbsp;
                                                    <asp:Label ID="lblSelectedDocumentTypeText" runat="server"></asp:Label>
                                                </th>
                                            </tr>
                                            <tr id="row">
                                                <td>--%>
                            <%--       <asp:Panel ID="pnlFileUpload" Width="100%" BorderWidth="0" CssClass="fileupload"
                                                        runat="server">
                                                        <table width="100%" runat="server" border="0" cellpadding="0" cellspacing="0">
                                                            
                                                            <tr>
                                                                <td>
                                                                    <asp:Panel ID="pnlUpload0" CssClass="Fileupload" Width="100%" runat="server">
                                                                        <ucMultiFile:MulFileUpload1 ID="fupLoad" runat="server" />
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblBottomBar">
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="btnupload2" runat="server" CssClass="btnstyle" Text="Save" OnClick="btnupload_Click"
                                                                                    Width="58px" />&nbsp;<asp:Button ID="btncancel0" runat="server" CssClass="btnstyle"
                                                                                        Text="Cancel" CausesValidation="False" OnClick="btncancel_Click" Width="53px" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>--%>
                            <asp:Panel ID="pnlSendFax" runat="server" Visible="false">
                                <table style="background: #fff; font-family: Arial; color: #555; font-size: 12px;
                                    padding-left: 5px;" width="100%">
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblFaxError" runat="server" Visible="false"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="FaxValidationt" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top" style="padding-left: 5px;">
                                            <span style="margin-right: 15px;">Country :</span><asp:DropDownList ID="ddlCountryFax"
                                                runat="Server">
                                                <asp:ListItem Text="USA" Value="1" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="India" Value="91"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td width="20%" valign="top" colspan="2">
                                            <span style="margin-right: 5px;">To Number : </span>
                                            <uc3:Phone ID="PhFax" runat="server" IsRequiredField="true" ValidationGroupforUser="FaxValidationt" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="110" align="left" valign="middle" style="font-family: Arial, Helvetica, sans-serif;
                                            font-size: 12px; color: #333;">
                                            Name/Description :
                                        </td>
                                        <td align="left" valign="middle">
                                            <asp:TextBox ID="txtuUernameFax" runat="Server" TextMode="MultiLine" Height="50"
                                                Width="250" MaxLength="255"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="FaxValidationt"
                                                ControlToValidate="txtuUernameFax" Display="dynamic" ErrorMessage="Please Enter the Name/Description.">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top" style="margin: 5px">
                                            <asp:Button ID="lnkSendFax" ValidationGroup="FaxValidationt" runat="server" Text="Send FAX"
                                                CssClass="btnstyle" OnClick="lnkSendFax_click" Style="display: block; margin-bottom: 5px;" />
                                        </td>
                                        <td align="left" valign="top" style="margin: 5px">
                                            <asp:Button runat="Server" Text="   Cancel   " ID="lnkCancelFax" CssClass="lnkstyle"
                                                CausesValidation="False" UseSubmitBehavior="False" OnClick="lnkCancelFax_click" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlUploads" runat="server" Visible="false">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tbldisplay">
                                    <tr>
                                        <th style="height: 24px">
                                            <asp:Label ID="lblUploadText" runat="server">Upload - </asp:Label>
                                            &nbsp;
                                            <asp:Label ID="lblSelectedDocumentTypeText" runat="server"></asp:Label>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" style="padding: 5px; border: 1px solid #787878;">
                                            <asp:Panel ID="pnlFileUpload" Width="100%" BorderWidth="0" CssClass="fileupload"
                                                runat="server">
                                                <table id="Table1" width="100%" runat="server" border="0" cellpadding="0" cellspacing="0"
                                                    style="border: none;">
                                                    <tr>
                                                        <td align="left" valign="middle">
                                                            <asp:Panel ID="pnlUpload0" CssClass="Fileupload" Width="100%" runat="server">
                                                                <ucMultiFile:MulFileUpload1 ID="fupLoad" runat="server" />
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" valign="middle">
                                                            <asp:Button ID="btnupload2" runat="server" CssClass="btnstyle" Text="Save" OnClick="btnupload_Click"
                                                                Width="58px" />&nbsp;<asp:Button ID="btncancel0" runat="server" CssClass="btnstyle"
                                                                    Text="Cancel" CausesValidation="False" OnClick="btncancel_Click" Width="53px" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <%--</td>
                                            </tr>
                                        </table>
                                        </asp:Panel>--%>
                            <br />
                            <%--<uc1:GridBar ID="GridBar2" runat="server" HeaderText="Load Status Information"/>--%>
                        </td>
                        <td>
                        </td>
                        <td width="1%" align="left" valign="top">
                            <img src="../images/pix.gif" alt="" width="5" height="1" />
                        </td>
                        <td align="left" valign="top">
                            <table id="tbldisplay" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td colspan="4">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="goldBar">
                                            <tr>
                                                <td align="left">
                                                    <strong>Order Identification</strong>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td align="right" valign="middle" width="20%">
                                        Customer :
                                    </td>
                                    <td colspan="3" valign="middle">
                                        <asp:Label ID="lblCustomer" runat="server" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" valign="middle">
                                        Pickup# :
                                    </td>
                                    <td valign="middle" width="25%">
                                        <asp:Label ID="lblPickUp" runat="server" />&nbsp;
                                    </td>
                                    <td align="right" valign="middle">
                                        Last Free Date :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblLastfreedate" runat="server" Text="" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td align="right" valign="middle">
                                        Container# :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblContainer" runat="server" />&nbsp;
                                    </td>
                                    <td align="right" valign="middle">
                                        Commodity :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblCommodity" runat="server" Text="" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" valign="middle">
                                        Chasis# :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblChasis" runat="server" Text=""></asp:Label>&nbsp;
                                    </td>
                                    <td align="right" valign="middle">
                                        Seal# :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblSeal" runat="server" Text="" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td align="right" valign="middle">
                                        Pieces :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblPieces" runat="server"></asp:Label>&nbsp;
                                    </td>
                                    <td align="right" valign="middle">
                                        Booking# :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblBooking" runat="server" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" valign="middle">
                                        Weight :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblWeigth" runat="server"></asp:Label>&nbsp;
                                    </td>
                                    <td align="right" valign="middle">
                                        &nbsp;
                                    </td>
                                    <td valign="middle">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td align="right" valign="middle">
                                        Created By :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblPersong" runat="server" Text="" />&nbsp;
                                    </td>
                                    <td align="right" valign="middle">
                                        Created On :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblCreated" runat="server" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" valign="middle">
                                        Updated By :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblUpdatedBy" runat="server" Text="" />&nbsp;
                                    </td>
                                    <td align="right" valign="middle">
                                        Updated On :
                                    </td>
                                    <td valign="middle">
                                        <asp:Label ID="lblUpdatedOn" runat="server" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="row">
                                    <td align="right" valign="middle" width="20%">
                                        Description :
                                    </td>
                                    <td colspan="3" valign="middle">
                                        <asp:Label ID="lblDescription" runat="server" />&nbsp;
                                    </td>
                                </tr>
                                <tr id="altrow">
                                    <td align="right" valign="middle" width="20%">
                                        Remarks :
                                    </td>
                                    <td colspan="3" valign="middle">
                                        <asp:Label ID="lblRemarks" runat="server" />&nbsp;
                                    </td>
                                </tr>
                            </table>
                           
                           

                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="10" src="../Images/pix.gif" width="1" />
                                    </td>
                                </tr>
                            </table>


                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <uc1:GridBar ID="gridbarPayables" runat="server" HeaderText="Receivable Details" />
                                        <uc2:Grid ID="gridPayables" runat="server" />
                                    </td>
                                </tr>
                            </table>

                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="10" src="../Images/pix.gif" width="1" />&nbsp;
                                    </td>
                                </tr>
                            </table>


                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="orangeBar">
                                            <tr>
                                                <td align="left">
                                                    <strong>Origin(s)</strong>
                                                </td>
                                            </tr>
                                        </table>
                                        <uc2:Grid ID="Grid1" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="10" src="../Images/pix.gif" width="1" />
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tealBar">
                                            <tr>
                                                <td align="left">
                                                    <strong>Destination(s)</strong>
                                                </td>
                                            </tr>
                                        </table>
                                        <uc2:Grid ID="Grid2" runat="server" />
                                    </td>
                                </tr>
                            </table>




                             
                            
                            <asp:Panel ID="pnlEdiGrid" runat="server" Visible="false">   
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="10" src="../Images/pix.gif" width="1" />
                                    </td>
                                </tr>
                            </table>                             
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="yellowBar" style=" background:#6c2271">
                                <tr><td align="left">EDI Load Status Information - <asp:Label ID="lblloadType" runat="server" Text=""></asp:Label></td></tr>
                            </table>
                                 <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr><td align="right">
                                                    <asp:DataGrid ID="dgrEdiTrackLoad" runat="server" AlternatingItemStyle-CssClass="GridAltItem"
                                                        AutoGenerateColumns="False" CssClass="Grid" GridLines="both"
                                                        PageSize="3" Width="100%">
                                                        <ItemStyle CssClass="GridItem"/><HeaderStyle CssClass="GridHeader"/><AlternatingItemStyle CssClass="GridAltItem"/>
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-CssClass="GridHeader" HeaderText="Location" ItemStyle-CssClass="GridItem">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLoc" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Location")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-CssClass="GridHeader" HeaderText="">
                                                                <ItemTemplate>
                                                                     <asp:DataGrid ID="dgrEdiLoad" runat="server" AlternatingItemStyle-CssClass="GridAltItem"
                                                        AutoGenerateColumns="False" CssClass="Grid" GridLines="both" OnItemCommand="dgrEdiLoad_ItemCommand"
                                                        PageSize="3" Width="100%" DataSource='<%#DataBinder.Eval(Container.DataItem,"EDIStatusDetailsList")%>'>
                                                        <ItemStyle CssClass="GridItem" />
                                                        <HeaderStyle CssClass="GridHeader" />
                                                        <AlternatingItemStyle CssClass="GridAltItem" />
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-CssClass="GridHeader" HeaderText="Name" ItemStyle-CssClass="GridItem">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Name")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-CssClass="GridHeader" HeaderText="Status" ItemStyle-CssClass="GridItem">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="dropEdiStatus" runat="server" AutoPostBack="true"  
                                                                        DataSource='<%#DataBinder.Eval(Container.DataItem,"DDlSource")%>' DataValueField="Value" DataTextField="Text" Visible='<%#DataBinder.Eval(Container.DataItem,"IsNew")%>' Width="150px">                                                                        
                                                                    </asp:DropDownList>
                                                                    <asp:Label ID="lblStatus" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Status")%>'
                                                                         Visible='<%#DataBinder.Eval(Container.DataItem,"IsOld")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-CssClass="GridHeader" HeaderText="Date/Time" ItemStyle-CssClass="GridItem" ItemStyle-Width="300px">
                                                                <ItemTemplate>
                                                                    <uc12:DatePicker ID="txtDate" runat="server"  Visible='<%#DataBinder.Eval(Container.DataItem,"IsNew")%>' IsRequired="true" IsDefault="true"
                                                                        CountNextYears="2" CountPreviousYears="0" SetInitialDate="true" />
                                                                    <br />
                                                                    <uc13:TimePicker ID="txtTime" runat="server"  Visible='<%#DataBinder.Eval(Container.DataItem,"IsNew")%>' EnableDropDowns="true"
                                                                        ReqFieldValidation="false" SetCurrentTime="true" />
                                                                    <asp:Label ID="lblDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"StatusTime")%>'  Visible='<%#DataBinder.Eval(Container.DataItem,"IsOld")%>' > </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-CssClass="GridHeader" HeaderText="Comments/Notes" ItemStyle-CssClass="GridItem">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtComments" runat="server"  Visible='<%#DataBinder.Eval(Container.DataItem,"IsNew")%>'></asp:TextBox>
                                                                    <asp:Label ID="lblComments" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Comment")%>'  Visible='<%#DataBinder.Eval(Container.DataItem,"IsOld")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-CssClass="GridHeader" ItemStyle-CssClass="GridItem">
                                                                <ItemTemplate>
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td align="center" style="width: 100%;" valign="middle">
                                                                                <asp:Button ID="btnAdd" runat="server" CommandName="ADD" Text="Add"  Visible='<%#DataBinder.Eval(Container.DataItem,"IsNew")%>'
                                                                                    CssClass="btnstyle" CausesValidation="true" />
                                                                                <asp:ImageButton ID="btnDel" runat="server" CommandName="Delete" ImageUrl="~/images/delete_icon.gif"
                                                                                    ToolTip="Delete" Height="13px" Width="12" CausesValidation="false"  Visible='<%#DataBinder.Eval(Container.DataItem,"IsOld")%>'
                                                                                    OnClientClick="return confirm('Are you sure you want to delete?');" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-CssClass="GridHeader" HeaderText="ID" ItemStyle-CssClass="GridItem" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EDITrackLoadId")%>'
                                                                        Visible="False"></asp:Label>
                                                                    <asp:Label ID="lblLocationId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"LocationID")%>'
                                                                        Visible="False"></asp:Label>
                                                                    <asp:Label ID="lblCity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"City")%>'
                                                                        Visible="False"></asp:Label>
                                                                    <asp:Label ID="lblState" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"StateCode")%>'
                                                                        Visible="False"></asp:Label>
                                                                    <asp:Label ID="lblIsOrgin" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"IsOrigin")%>'
                                                                        Visible="False"></asp:Label>
                                                                     <asp:Label ID="lblStopNumber" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"StopNumber")%>'
                                                                        Visible="False"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </td>
                                                </tr>
                                     </table>
                             
                            
                            </asp:Panel>

                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody><tr>
                                    <td>
                                        <img alt="" height="10" src="../Images/pix.gif" width="1">
                                    </td>
                                </tr>
                            </tbody></table>
                            
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="yellowBar">
                                <tr>
                                    <td align="left">
                                        Load Status Information <asp:Label ID="lblEdiLoadType" runat="server" Text=""></asp:Label>
                                        <%--<uc1:GridBar ID="GridBar2" runat="server" HeaderText="Load Status Information"/>--%>
                                    </td>
                                </tr>
                            </table>

                            <%--Load Status Information Showing the OLD Grid only for New status--%>
                            <asp:Panel ID="panelOldGrid" runat="server">
                              <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <uc2:Grid ID="Grid4" runat="server" PageSize="20" />
                                    </td>
                                </tr>
                            </table>
                            </asp:Panel>
                            
                            <%--Load Status Information Showing the NEW Grid only All the Other Status'--%>
                            <asp:Panel ID="panelNewGrid" runat="server">
                             <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <%--SLine 2016 Enhancements
        COMMENTED THE BELOW GRID ---- FOR Showing Upload Load Status in TRACK-LOAD-PAGE--%>
                                        <%--<uc2:Grid ID="Grid4" runat="server" PageSize="20" />--%>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="right">
                                                    <asp:CheckBox ID="ChkSendMail" runat="server" Text="Send Mail " TextAlign="Left"
                                                        Visible="false" Style="text-align: right" ForeColor="Red" />
                                                    <%--SLine 2016 Enhancements
                                        FOR Showing Upload Load Status in TRACK-LOAD-PAGE--%>
                                                    <asp:DataGrid ID="dgrLoad" runat="server" AlternatingItemStyle-CssClass="GridAltItem"
                                                        AutoGenerateColumns="False" CssClass="Grid" GridLines="both" OnItemCommand="dgrLoad_ItemCommand"
                                                        PageSize="3" Width="100%" OnItemDataBound="dgrLoad_ItemDataBound">
                                                        <ItemStyle CssClass="GridItem" />
                                                        <HeaderStyle CssClass="GridHeader" />
                                                        <AlternatingItemStyle CssClass="GridAltItem" />
                                                        <%-- <ItemStyle BorderColor="BLACK" BorderStyle="Solid" BorderWidth="2px" />
            <HeaderStyle BorderColor="BLACK" BorderStyle="Solid" BorderWidth="2px" />
            <AlternatingItemStyle BorderColor="BLACK" BorderStyle="Solid" BorderWidth="2px" />--%>
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-CssClass="GridHeader" HeaderText="Name" ItemStyle-CssClass="GridItem">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Name")%>'
                                                                        Visible="False"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-CssClass="GridHeader" HeaderText="Status" ItemStyle-CssClass="GridItem">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="dropStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dropStatus_SelectedIndexChanged">
                                                                        <asp:ListItem Value="3">Pickedup</asp:ListItem>
                                                                        <asp:ListItem Value="4">Loaded in Yard</asp:ListItem>                                                                        
                                                                        <asp:ListItem Value="5">En-route</asp:ListItem>
                                                                        <asp:ListItem Value="6">Drop in Warehouse</asp:ListItem>
                                                                        <asp:ListItem Value="14">Reached Destination</asp:ListItem>
                                                                        <asp:ListItem Value="7">Driver On Waiting</asp:ListItem>
                                                                        <asp:ListItem Value="8">Delivered</asp:ListItem>
                                                                        <asp:ListItem Value="9">Empty in Yard</asp:ListItem>
                                                                        <asp:ListItem Value="17">Street Turn</asp:ListItem>
                                                                        <asp:ListItem Value="10">Terminated</asp:ListItem>
                                                                        <asp:ListItem Value="11">Paperwork Pending</asp:ListItem>
                                                                        <asp:ListItem Value="16">Invoiceable</asp:ListItem>
                                                                        <asp:ListItem Value="12">Closed</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:Label ID="lblStatus" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"LoadStatus")%>'
                                                                        Visible="False"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-CssClass="GridHeader" HeaderText="Location" ItemStyle-CssClass="GridItem">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtLoc" runat="server" Visible="False"></asp:TextBox>
                                                                    <%--                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter location." ControlToValidate="txtLoc" Text="*"></asp:RequiredFieldValidator>
                                                                    --%>
                                                                    <asp:Label ID="lblLoc" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"nvar_Location")%>'
                                                                        Visible="False"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-CssClass="GridHeader" HeaderText="Date/Time" ItemStyle-CssClass="GridItem"
                                                                ItemStyle-Width="300px">
                                                                <ItemTemplate>
                                                                    <%--<asp:TextBox ID="txtDate" runat="server" Visible="False"></asp:TextBox>--%>
                                                                    <uc12:DatePicker ID="txtDate" runat="server" Visible="False" IsRequired="true" IsDefault="true"
                                                                        CountNextYears="2" CountPreviousYears="0" SetInitialDate="true" />
                                                                    <br />
                                                                    <uc13:TimePicker ID="txtTime" runat="server" Visible="False" EnableDropDowns="true"
                                                                        ReqFieldValidation="false" SetCurrentTime="true" />
                                                                    <asp:Label ID="lblDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"date_CreateDate")%>'
                                                                        Visible="False">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-CssClass="GridHeader" HeaderText="Comments/Notes"
                                                                ItemStyle-CssClass="GridItem">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtComments" runat="server" Visible="False"></asp:TextBox>
                                                                    <asp:Label ID="lblComments" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"nvar_Notes")%>'
                                                                        Visible="False"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-CssClass="GridHeader" ItemStyle-CssClass="GridItem">
                                                                <ItemTemplate>
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td align="center" style="width: 100%;" valign="middle">
                                                                                <asp:Button ID="btnAdd" runat="server" CommandName="ADD" Text="Add" Visible="false"
                                                                                    CssClass="btnstyle" CausesValidation="true" />
                                                                                <asp:ImageButton ID="btnDel" runat="server" CommandName="Delete" ImageUrl="~/images/delete_icon.gif"
                                                                                    ToolTip="Delete" Visible="false" Height="13px" Width="12" CausesValidation="false"
                                                                                    OnClientClick="return confirm('Are you sure you want to delete?');" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-CssClass="GridHeader" HeaderText="ID" ItemStyle-CssClass="GridItem"
                                                                Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"bint_TrackLoadId")%>'
                                                                        Visible="False"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                    <%--SLine 2016 Enhancements
                                        FOR Showing Upload Load Status in TRACK-LOAD-PAGE--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            </asp:Panel>





                            <table border="
          0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="10" src="../Images/pix.gif" width="1" />
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <uc1:GridBar ID="gridbarDrivers" runat="server" HeaderText="Assigned Drivers" />
                                        <uc2:Grid ID="Grid3" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="10" src="../Images/pix.gif" width="1" />
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="orangeBar">
                                <tr>
                                    <td align="left">
                                        Chassis Charges Days
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: 1px solid #787878;">
                                <tr id="row">
                                    <td style="font-size: 12px; width: 125px">
                                        &nbsp; Number of Days :
                                    </td>
                                    <td style="font-size: 12px">
                                        <asp:Label ID="lblChassisDays" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="10" src="../Images/pix.gif" width="1" />
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tealBar">
                                <tr>
                                    <td align="left">
                                        Notes
                                    </td>
                                    <td align="right">
                                        <asp:LinkButton ID="lnkNotes" runat="server">Add Note</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <uc2:Grid ID="Grid5" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="BlueBar">
                                <tr>
                                    <td align="left">
                                        Office Notes
                                    </td>
                                    <td align="right">
                                        <asp:LinkButton ID="lnkOfficeNotes" runat="server">Add Office Note</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <uc2:Grid ID="OfficeNoteGrid" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="10" src="../Images/pix.gif" width="1" />
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="violetBar">
                                <tr>
                                    <td align="left">
                                        Rail/Equipment Tracing
                                    </td>
                                    <td align="right">
                                        <asp:LinkButton ID="lnkRailNote" runat="server">Add Rail/Equipment Note</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <uc2:Grid ID="GridRailNotes" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <img alt="" height="10" src="../Images/pix.gif" width="1" />
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <uc1:GridBar ID="GridBar4" runat="server" HeaderText="Contact Persons" />
                                                </td>
                                            </tr>
                                        </table>
                                        <uc2:Grid ID="Grid6" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
