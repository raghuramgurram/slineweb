<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StaffMistakes.aspx.cs" Inherits="Manager_StaffMistakes"
    MasterPageFile="~/Manager/MasterPage.master" Title="S Line Transport Inc" %>

<%@ Register Src="~/UserControls/Grid.ascx" TagName="Grid" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/GridBar.ascx" TagName="GridBar" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="ContentTbl">
        <tr valign="top">
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="3" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="tblform1">
                    <tr>
                        <td align="right" valign="middle">
                            <asp:Button ID="btnBack" runat="Server" CssClass="btnstyle" Text="Back" Visible="false"
                                OnClick="btnBack_Click" />
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <img alt="" height="3" src="../Images/pix.gif" width="1" /></td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td align="left" width="100%">
                            <uc2:GridBar ID="barCurrent" runat="server" HeaderText="Mistakes" LinksList="Add New Mistake"
                                PagesList="NewMistake.aspx" Visible="true" />
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img src="../Images/pix.gif" alt="" width="1" height="5" /></td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <uc3:Grid ID="gridCurrent" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
