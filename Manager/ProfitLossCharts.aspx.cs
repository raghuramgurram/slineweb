using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;

public partial class Manager_ProfitLossGraphs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            FillLists();
        //SLine 2017 Enhancement for Office Location Starts
        Session["AllOfficeLocationChecked"] = chkAllOfficeLocation.Checked;
        if (Convert.ToBoolean(Session["IsAdmin"]))
        {
            spnAllOfficeLocation.Style.Add(HtmlTextWriterStyle.Display, "");
        }
        else
        {
            spnAllOfficeLocation.Style.Add(HtmlTextWriterStyle.Display, "none");
        }
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        //SLine 2017 Enhancement for Office Location Ends
        DataTable dt = new DataTable();
        Session["TopHeaderText"] = "Profit and Loss Charts";
        if (Convert.ToString(searchType.Value) == "1")
        {
            Session["TopHeaderText"] = "Profit and Loss Chart Monthly";
            //SLine 2017 Enhancement for Office Location
            Session["ReportParameters"] = ddlMonth.SelectedValue + "~~^^^^~~" + ddlYear.SelectedValue + "~~^^^^~~" + getLongName(Convert.ToInt16(ddlMonth.SelectedValue)) + "~~^^^^~~" + (chkAllOfficeLocation.Checked == true ? "1" : "0") + "~~^^^^~~" + officeLocationId.ToString();
            dt = GetMonthlyData();
        }
        else if (Convert.ToString(searchType.Value) == "2")
        {
            Session["TopHeaderText"] = "Profit and Loss Chart Daily";
            //SLine 2017 Enhancement for Office Location
            Session["ReportParameters"] = ddlMonth.SelectedValue + "~~^^^^~~" + ddlYear.SelectedValue + "~~^^^^~~" + getLongName(Convert.ToInt16(ddlMonth.SelectedValue)) + "~~^^^^~~" + (chkAllOfficeLocation.Checked == true ? "1" : "0") + "~~^^^^~~" + officeLocationId.ToString();
            dt = GetDailyData();
        }
        else if (Convert.ToString(searchType.Value) == "3")
        {
            Session["TopHeaderText"] = "Profit and Loss Chart Year Wise";
            //SLine 2017 Enhancement for Office Location
            Session["ReportParameters"] = ddlMonth.SelectedValue + "~~^^^^~~" + ddlYear.SelectedValue + "~~^^^^~~" + getLongName(Convert.ToInt16(ddlMonth.SelectedValue)) + "~~^^^^~~" + (chkAllOfficeLocation.Checked == true ? "1" : "0") + "~~^^^^~~" + officeLocationId.ToString();
            dt = GetAllYearsData();
        }
  
            if (dt.Rows.Count > 0)
            {
                lblDisplay.Visible = false;
                DataView dv = dt.DefaultView;
                Chart1.Series[0].Points.DataBindXY(dv, "DeliveredDate", dv, "LoadId"); 
                Chart1.Series[0]["PieLabelStyle"] = "Disabled";
                Chart1.Series[0].MarkerStyle = MarkerStyle.Circle;
                Chart1.Series[0].MarkerColor = Color.DeepPink;
                if (Convert.ToString(searchType.Value) == "1")
                  Chart1.Series[0].ToolTip = "Month: #VALX Loads: #VALY";
                else if (Convert.ToString(searchType.Value) == "2")
                  Chart1.Series[0].ToolTip = "Date: #VALX Loads: #VALY";
                else if (Convert.ToString(searchType.Value) == "3")
                    Chart1.Series[0].ToolTip = "Year: #VALX Loads: #VALY";

                Chart2.Series[0].Points.DataBindXY(dv, "DeliveredDate", dv, "Receivable");
                Chart2.Series[0]["PieLabelStyle"] = "Disabled";
                Chart2.Series[0].MarkerStyle = MarkerStyle.Circle;
                Chart2.Series[0].MarkerColor = Color.DarkGreen;
                if (Convert.ToString(searchType.Value) == "1")
                  Chart2.Series[0].ToolTip = "Month: #VALX Receivable: #VALY";
                else if (Convert.ToString(searchType.Value) == "2")
                  Chart2.Series[0].ToolTip = "Date: #VALX Receivable: #VALY";
                else if (Convert.ToString(searchType.Value) == "3")
                    Chart2.Series[0].ToolTip = "Year: #VALX Receivable: #VALY";

                Chart3.Series[0].Points.DataBindXY(dv, "DeliveredDate", dv, "Payable");
                Chart3.Series[0]["PieLabelStyle"] = "Disabled";
                Chart3.Series[0].MarkerStyle = MarkerStyle.Circle;
                Chart3.Series[0].MarkerColor = Color.DarkRed;
                if (Convert.ToString(searchType.Value) == "1")
                  Chart3.Series[0].ToolTip = "Month: #VALX Payable: #VALY";
                else if (Convert.ToString(searchType.Value) == "2")
                  Chart3.Series[0].ToolTip = "Date: #VALX Payable: #VALY";
                else if (Convert.ToString(searchType.Value) == "3")
                  Chart3.Series[0].ToolTip = "Year: #VALX Payable: #VALY";

                Chart4.Series[0].Points.DataBindXY(dv, "DeliveredDate", dv, "Margin");
                Chart4.Series[0]["PieLabelStyle"] = "Disabled";
                Chart4.Series[0].MarkerStyle = MarkerStyle.Circle;
                Chart4.Series[0].MarkerColor = Color.DarkBlue;
                if (Convert.ToString(searchType.Value) == "1")
                  Chart4.Series[0].ToolTip = "Month: #VALX Margin: #VALY";
                else if (Convert.ToString(searchType.Value) == "2")
                  Chart4.Series[0].ToolTip = "Date: #VALX Margin: #VALY";
                else if (Convert.ToString(searchType.Value) == "3")
                  Chart4.Series[0].ToolTip = "Year: #VALX Margin: #VALY";
            }
            else if (dt.Rows.Count == 0)
            {
                if (Convert.ToString(searchType.Value) == "1" || Convert.ToString(searchType.Value) == "2" || Convert.ToString(searchType.Value) == "3")
                {
                    Chart1.Visible = false;
                    Chart2.Visible = false;
                    Chart3.Visible = false;
                    Chart4.Visible = false;
                    lblDisplay.Visible = true;
                    lblDisplay.Text = "<table width=100% border=0 cellpadding=0 cellspacing=0 id=ContentTbl><tr valign=top><td>" +
                        "<table border=0 width=100% height=200px border=0 cellpadding=0 cellspacing=0 align=center valign=middle><tr><td width=100% align=center valign=middle>" +
                        "<b><font color=blue>No Records To Display.<font></b></td></tr></table><br/><br/><br/></td></tr></table>";
                }
                else
                {
                    lblDisplay.Visible = false;
                    Chart1.Visible = false;
                    Chart2.Visible = false;
                    Chart3.Visible = false;
                    Chart4.Visible = false;
                }
            }
    }

    private void FillLists()
    {
        ddlMonth.SelectedValue = DateTime.Today.Month.ToString();
        int currentYear = DateTime.Today.Year;
        for (int i = 8; i >= 0; i--)
        {
            ddlYear.Items.Add((currentYear - i).ToString());
        }
        ddlYear.Items.Add((currentYear + 1).ToString());
        ddlYear.SelectedValue = currentYear.ToString();
    }

    /*protected void btnChartMonthly_Click(object sender, EventArgs e)
    {
        Session["ReportParameters"] = ddlMonth.SelectedValue + "~~^^^^~~" + ddlYear.SelectedValue + "~~^^^^~~" + getLongName(Convert.ToInt16(ddlMonth.SelectedValue));
        Response.Redirect("~/Manager/ProfitLossCharts.aspx?type=1");
    }

    protected void btnChartDaily_Click(object sender, EventArgs e)
    {
        Session["ReportParameters"] = ddlMonth.SelectedValue + "~~^^^^~~" + ddlYear.SelectedValue + "~~^^^^~~" + getLongName(Convert.ToInt16(ddlMonth.SelectedValue));
        Response.Redirect("~/Manager/ProfitLossCharts.aspx?type=2");
    }*/

    private string getLongName(int key)
    {
        string[] Months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
        return Months[key - 1];
    }

    public DataTable GetMonthlyData()
    {
        object[] objParams = null;
        DataSet ds = new DataSet();
        objParams = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
        objParams[2] = "Monthly";
        ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetProfitLossGraphData", objParams);
        return ds.Tables[0];            
    }

    public DataTable GetDailyData()
    {
        object[] objParams = null;
        DataSet ds = new DataSet();
        objParams = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
        objParams[2] = "Daily";
        ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetProfitLossGraphData", objParams);
        return ds.Tables[0];        
    }

    public DataTable GetAllYearsData()
    {
        object[] objParams = null;
        DataSet ds = new DataSet();
        objParams = Convert.ToString(Session["ReportParameters"]).Trim().Replace("~~^^^^~~", "^").Split('^');
        objParams[2] = "AllYears";
        ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetProfitLossGraphData", objParams);
        return ds.Tables[0];    
    }
}
