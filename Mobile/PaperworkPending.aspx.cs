using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Mobile_PaperworkPending : System.Web.UI.Page
{
    #region Page Load and repiter Bind

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Keys.LOGIN_ID_KEY] == null || Session[Keys.EMPLOY_ID] == null)
        {
            Response.Redirect(Keys.PAGE_LOAGIN);
        }

        if (!IsPostBack)
        {
            Session[Keys.TAB_SELECTED] = PageType.Paperworkpending;
            Session[Keys.LOAD_ID] = null;
            MuPageMenu.SubTitle = ConfigurationManager.AppSettings["CompanyName"].ToString() + " - Paper Work Pending Loads";
            //MuPageMenu.Menuselected = PageType.Paperworkpending;
            MuPageMenu.LoadMenu();
            LoadsBase load = new LoadsBase();
            rptLoads.DataSource = load.Loadpaperworkpending(Convert.ToInt32(Session[Keys.EMPLOY_ID].ToString()));
            rptLoads.DataBind();
        }
    }

    protected void rptLoads_databind(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
        {
            Literal ltl = e.Item.FindControl("ltlDelivereddatetime") as Literal;

            if (ltl != null)
            {
                if (!string.IsNullOrEmpty(ltl.Text.Trim()))
                {
                    DateTime date = Convert.ToDateTime(ltl.Text);

                    ltl.Text = date.ToString("MMM dd yyyy");
                }
            }
        }
    }

    #endregion

    #region Button clicks
    protected void lnkloadetails_click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        if (lnk != null)
        {
            Session[Keys.LOAD_ID] = Convert.ToInt32(lnk.CommandArgument.ToString());
        }
        Response.Redirect(Keys.PAGE_LOAD_DETAILS);
    }
    protected void lnkupdateloadestatus_click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        if (lnk != null)
        {
            Session[Keys.LOAD_ID] = Convert.ToInt32(lnk.CommandArgument.ToString());
        }
        Response.Redirect(Keys.PAGE_CHANGE_STATUS);
    }
    protected void lnkuploaddocuments_click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        if (lnk != null)
        {
            Session[Keys.LOAD_ID] = Convert.ToInt32(lnk.CommandArgument.ToString());
        }
        Response.Redirect(Keys.PAGE_UPLOAD_DOCUMENTS);
    }
    #endregion
}
