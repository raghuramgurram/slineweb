﻿using System;
using System.Collections.Generic;
using System.Web;


public class Enums
{
}
    public enum loadStatus
    {
        New = 1,
        Assigned = 2,
        Pickedup = 3,
        Loaded_in_Yard = 4,
        En_route = 5,
        Drop_in_Warehouse = 6,
        Driver_on_Waiting = 7,
        Delivered = 8,
        Empty_in_Yard = 9,
        Terminated = 10,
        Paperwork_pending = 11,
        Closed = 12,
        Cancel = 13,
        Reached_Destination = 14,
        Load_Planner = 15
    }
    public enum PageType
    {
        Loads = 1,
        Paperworkpending = 2
    }
    public enum UserType
    {
        Manager = 1,
        Driver = 2,
        Staff = 3,
        Dispatcher = 4,
        Carrier = 5,
        Customer = 6
    }
    public enum DocumentType
    {
        Load_Order = 1,
        Bare_Chassis_In_Gate_Out_Gate = 2,
        Bill_of_Lading = 3,
        Accessorial_Charges = 4,
        Out_Gate_Interchange = 5,
        In_Gate_Interchange = 6,
        Proof_of_Delivery = 7,
        Scale_Ticket = 8,
        Lumper_Receipt = 9,
        Misc = 10
    }
