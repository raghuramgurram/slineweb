using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class AddRailNotes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Add Rail Notes";
        if (!IsPostBack)
        {
            btnsave.Attributes.Add("onclick", "this.style.display='none';");
            if (Request.QueryString.Count > 0 && Session["UserId"] != null)
            {
                ViewState["ID"] = Convert.ToString(Request.QueryString[0]).Trim();
                lblLoadNo.Text = Convert.ToString(ViewState["ID"]);
                lblEnterBy.Text = Convert.ToString(Session["UserId"]);
            }
        }
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Manager/TrackLoad.aspx?" + Convert.ToString(ViewState["ID"]));
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (ViewState["ID"] != null)
        {
            object[] objParams = new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(Session["UserLoginId"]), txtNotes.Text.Trim() };
            if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Add_RailNotes", objParams) > 0)
            {
                Response.Redirect("~/Manager/TrackLoad.aspx?" + Convert.ToString(ViewState["ID"]));
            }
        }
    }
}
