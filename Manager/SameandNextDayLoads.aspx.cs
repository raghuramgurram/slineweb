using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Manager_SameandNextDayLoads : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["LoadGridPageIndex"] = null;
        }

        int type = Convert.ToInt32(Request.QueryString["type"]);
        int day = 0;
        //New Header Test changes (madhav)
        if (type == 1)
        {
            Session["TopHeaderText"] = "Same Day Loads";
            barManager.HeaderText = "Same Day Loads";
        }
        else if (type == 2)
        {
            Session["TopHeaderText"] = "Next Day Loads";
            barManager.HeaderText = "Next Day Loads";
        }
        else if (type == 4)
        {
            Session["TopHeaderText"] = "Today�s LFD Loads";
            barManager.HeaderText = "Today�s LFD Loads";
        }
        else if (type == 3)
        {
            Session["TopHeaderText"] = Request.QueryString["name"] + " Loads";
            barManager.HeaderText = Request.QueryString["name"] + " Loads";
            day = Convert.ToInt32(Request.QueryString["day"]);

        }
        else if (type == 5)
        {
            Session["TopHeaderText"] = "LFD Expired Loads";
            barManager.HeaderText = "LFD Expired Loads";
        }
        else if (type == 6)
        {
            Session["TopHeaderText"] = "Export Loads";
            barManager.HeaderText = "Export Loads";
        }
        Session["ShowStatusValueforUser"] = null;
        FillGrid(type, day);

    }
    public void FillGrid(int GridDataToShow, int day)
    {
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        //SLine 2017 Enhancement for Office Location Ends
        if (GridDataToShow == 1) // code for today list(Madhav)
        {
            grdManager.ShowStatus = "LFD";
            grdManager.MainTableName = "eTn_Load";
            grdManager.MainTablePK = "eTn_Load.bint_LoadId";
            grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
            "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2', eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2',(Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end) + eTn_LoadType.nvar_LoadTypeDesc as 'Type'," +
            "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
            "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Pickup as 'Pickup#' ,eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold,etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bint_LoadStatusId as LoadStatusID, eTn_Load.nvar_TripType as TripType ";
            grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId inner join eTn_Destination on eTn_Destination.bint_LoadId = eTn_Load.bint_LoadId inner join eTn_DeliveryApptDate_View on eTn_DeliveryApptDate_View.bint_LoadId = eTn_Load.bint_LoadId ";
            if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId in (1,4)" +
                  " AND eTn_Destination.date_DeliveryAppointmentDate >= CONVERT(datetime, CONVERT(varchar, DATEADD(day, 0, GETDATE()), 102))" +
                  " AND eTn_Destination.date_DeliveryAppointmentDate < CONVERT(datetime, CONVERT(varchar, DATEADD(day, 1, GETDATE()), 102))" +
                  //SLine 2017 Enhancement for Office Location
                  " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.DeleteVisible = true;
                grdManager.LastColumnLinksList = "Load Planner;Cancel";
                grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/DashBoard.aspx";
            }
            else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId in (1,4)" +
                   " AND eTn_Destination.date_DeliveryAppointmentDate >= CONVERT(datetime, CONVERT(varchar, DATEADD(day, 0, GETDATE()), 102))" +
                   " AND eTn_Destination.date_DeliveryAppointmentDate < CONVERT(datetime, CONVERT(varchar, DATEADD(day, 1, GETDATE()), 102))" +
                    //SLine 2017 Enhancement for Office Location
                  " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.DeleteVisible = false;
                grdManager.LastColumnLinksList = "Dispatch";
                grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx";
            }
            else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId in (1,4)" +
                 " AND eTn_Destination.date_DeliveryAppointmentDate >= CONVERT(datetime, CONVERT(varchar, DATEADD(day, 0, GETDATE()), 102))" +
                 " AND eTn_Destination.date_DeliveryAppointmentDate < CONVERT(datetime, CONVERT(varchar, DATEADD(day, 1, GETDATE()), 102))" +
                  //SLine 2017 Enhancement for Office Location
                  " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.DeleteVisible = false;
                grdManager.LastColumnLinksList = "";
            }
           
        }
        else if (GridDataToShow == 2)// code for tomarrow list(Madhav)
        {
            grdManager.ShowStatus = "LFD";
            grdManager.MainTableName = "eTn_Load";
            grdManager.MainTablePK = "eTn_Load.bint_LoadId";
            grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
            "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', (Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end) + eTn_LoadType.nvar_LoadTypeDesc as 'Type'," +
            "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
            "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Pickup as 'Pickup#' ,eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold,etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bint_LoadStatusId as LoadStatusID,eTn_Load.nvar_TripType as TripType ";
            grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId inner join eTn_Destination on eTn_Destination.bint_LoadId = eTn_Load.bint_LoadId  inner join eTn_DeliveryApptDate_View on eTn_DeliveryApptDate_View.bint_LoadId = eTn_Load.bint_LoadId ";
            if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId in (1,4)" +
                    //SLine 2017 Enhancement for Office Location
                  " and eTn_Load.bint_OfficeLocationId = " + officeLocationId +
                  " AND eTn_Destination.date_DeliveryAppointmentDate >= CONVERT(datetime, CONVERT(varchar, DATEADD(day, 1, GETDATE()), 102))" +
                  " AND eTn_Destination.date_DeliveryAppointmentDate < CONVERT(datetime, CONVERT(varchar, DATEADD(day, 2, GETDATE()), 102))";
                    
                grdManager.DeleteVisible = true;
                grdManager.LastColumnLinksList = "Load Planner;Cancel";
                grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/DashBoard.aspx";
            }
            else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId in (1,4)" +
                   " AND eTn_Destination.date_DeliveryAppointmentDate >= CONVERT(datetime, CONVERT(varchar, DATEADD(day, 1, GETDATE()), 102))" +
                   " AND eTn_Destination.date_DeliveryAppointmentDate < CONVERT(datetime, CONVERT(varchar, DATEADD(day, 2, GETDATE()), 102))" +
                    //SLine 2017 Enhancement for Office Location
                  " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.DeleteVisible = false;
                grdManager.LastColumnLinksList = "Dispatch";
                grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx";
            }
            else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId in (1,4)" +
                 " AND eTn_Destination.date_DeliveryAppointmentDate >= CONVERT(datetime, CONVERT(varchar, DATEADD(day, 1, GETDATE()), 102))" +
                 " AND eTn_Destination.date_DeliveryAppointmentDate < CONVERT(datetime, CONVERT(varchar, DATEADD(day, 2, GETDATE()), 102))" +
                    //SLine 2017 Enhancement for Office Location
                  " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.DeleteVisible = false;
                grdManager.LastColumnLinksList = "";
            }
        }

        else if (GridDataToShow == 3)// code for day list(Raghu)
        {
            grdManager.ShowStatus = "LFD";
            grdManager.MainTableName = "eTn_Load";
            grdManager.MainTablePK = "eTn_Load.bint_LoadId";
            grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
            "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', (Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end) + eTn_LoadType.nvar_LoadTypeDesc as 'Type'," +
            "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
            "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Pickup as 'Pickup#' ,eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold,etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bint_LoadStatusId as LoadStatusID,eTn_Load.nvar_TripType as TripType ";
            grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId inner join eTn_Destination on eTn_Destination.bint_LoadId = eTn_Load.bint_LoadId inner join eTn_DeliveryApptDate_View on eTn_DeliveryApptDate_View.bint_LoadId = eTn_Load.bint_LoadId ";
            if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId in (1,4)" +
                  " AND eTn_Destination.date_DeliveryAppointmentDate >= CONVERT(datetime, CONVERT(varchar, DATEADD(day, "+ day.ToString() +", GETDATE()), 102))" +
                  " AND eTn_Destination.date_DeliveryAppointmentDate < CONVERT(datetime, CONVERT(varchar, DATEADD(day, " + (day+1).ToString() + ", GETDATE()), 102))" +
                    //SLine 2017 Enhancement for Office Location
                  " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.DeleteVisible = true;
                grdManager.LastColumnLinksList = "Load Planner;Cancel";
                grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/DashBoard.aspx";
            }
            else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                grdManager. SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId in (1,4)" +
                   " AND eTn_Destination.date_DeliveryAppointmentDate >= CONVERT(datetime, CONVERT(varchar,DATEADD(day, " + day.ToString() + ", GETDATE()), 102))" +
                   " AND eTn_Destination.date_DeliveryAppointmentDate < CONVERT(datetime, CONVERT(varchar, DATEADD(day, " + (day + 1).ToString() + ", GETDATE()), 102))" +
                    //SLine 2017 Enhancement for Office Location
                  " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.DeleteVisible = false;
                grdManager.LastColumnLinksList = "Dispatch";
                grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx";
            }
            else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId in (1,4)" +
                 " AND eTn_Destination.date_DeliveryAppointmentDate >= CONVERT(datetime, CONVERT(varchar, DATEADD(day, " + day.ToString() + ", GETDATE()), 102))" +
                 " AND eTn_Destination.date_DeliveryAppointmentDate < CONVERT(datetime, CONVERT(varchar, DATEADD(day, " + (day + 1).ToString() + ", GETDATE()), 102))" +
                    //SLine 2017 Enhancement for Office Location
                  " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.DeleteVisible = false;
                grdManager.LastColumnLinksList = "";
            }
        }

        else if (GridDataToShow == 4)// code for tomarrow list(Madhav)
        {
            grdManager.ShowStatus = "LFD";
            grdManager.MainTableName = "eTn_Load";
            grdManager.MainTablePK = "eTn_Load.bint_LoadId";
            grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
            "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2' ,(Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end) + eTn_LoadType.nvar_LoadTypeDesc as 'Type'," +
            "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
            "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Pickup as 'Pickup#' ,eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold,etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bint_LoadStatusId as LoadStatusID,eTn_Load.nvar_TripType as TripType ";
            grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId inner join eTn_Destination on eTn_Destination.bint_LoadId = eTn_Load.bint_LoadId left outer join eTn_DeliveryApptDate_View on eTn_DeliveryApptDate_View.bint_LoadId = eTn_Load.bint_LoadId ";
            if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId in (1,2,15)" +
                  //SLine 2017 Enhancement for Office Location
                  " and eTn_Load.bint_OfficeLocationId = " + officeLocationId +
                  " AND eTn_Load.date_LastFreeDate >= CONVERT(datetime, CONVERT(varchar, DATEADD(day, 0, GETDATE()), 102))" +
                  " AND eTn_Load.date_LastFreeDate < CONVERT(datetime, CONVERT(varchar, DATEADD(day, 1, GETDATE()), 102)) AND eTn_Load.bint_LoadId not in (select eTn_TrackLoad.bint_LoadId  " +
    "from eTn_TrackLoad inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId"+
     " where eTn_Load.bint_LoadStatusId = 2 "+
    "group by eTn_TrackLoad.bint_LoadId ,eTn_TrackLoad.bint_LoadStatusId having COUNT(1) > 1)";
                grdManager.DeleteVisible = true;
                grdManager.LastColumnLinksList = "";
            }
            else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId in (1,2,15)" +
                  //SLine 2017 Enhancement for Office Location
                  " and eTn_Load.bint_OfficeLocationId = " + officeLocationId +
                  " AND eTn_Load.date_LastFreeDate >= CONVERT(datetime, CONVERT(varchar, DATEADD(day, 0, GETDATE()), 102))" +
                  " AND eTn_Load.date_LastFreeDate < CONVERT(datetime, CONVERT(varchar, DATEADD(day, 1, GETDATE()), 102)) AND eTn_Load.bint_LoadId not in (select eTn_TrackLoad.bint_LoadId  " +
    "from eTn_TrackLoad inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId" +
     " where eTn_Load.bint_LoadStatusId = 2 " +
    "group by eTn_TrackLoad.bint_LoadId ,eTn_TrackLoad.bint_LoadStatusId having COUNT(1) > 1)";
                grdManager.DeleteVisible = false;
                grdManager.LastColumnLinksList = "";
            }
            else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId in (1,2,15)" +
                  //SLine 2017 Enhancement for Office Location
                  " and eTn_Load.bint_OfficeLocationId = " + officeLocationId +
                   " AND eTn_Load.date_LastFreeDate >= CONVERT(datetime, CONVERT(varchar, DATEADD(day, 0, GETDATE()), 102))" +
                   " AND eTn_Load.date_LastFreeDate < CONVERT(datetime, CONVERT(varchar, DATEADD(day, 1, GETDATE()), 102)) AND eTn_Load.bint_LoadId not in (select eTn_TrackLoad.bint_LoadId  " +
     "from eTn_TrackLoad inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId" +
      " where eTn_Load.bint_LoadStatusId = 2 " +
     "group by eTn_TrackLoad.bint_LoadId ,eTn_TrackLoad.bint_LoadStatusId having COUNT(1) > 1)";
                grdManager.DeleteVisible = false;
                grdManager.LastColumnLinksList = "";

            }
        }

        else if (GridDataToShow == 5)// code for LFD expired list(Raghu)
        {
            grdManager.ShowStatus = "LFD";
            grdManager.MainTableName = "eTn_Load";
            grdManager.MainTablePK = "eTn_Load.bint_LoadId";
            grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
            "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', (Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end) + eTn_LoadType.nvar_LoadTypeDesc as 'Type'," +
            "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
            "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Pickup as 'Pickup#' ,eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold,etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bint_LoadStatusId as LoadStatusID,eTn_Load.nvar_TripType as TripType ";
            grdManager.InnerJoinClauseWithOnlySingleRowTables = " inner join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                " inner join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId inner join eTn_Destination on eTn_Destination.bint_LoadId = eTn_Load.bint_LoadId left outer join eTn_DeliveryApptDate_View on eTn_DeliveryApptDate_View.bint_LoadId = eTn_Load.bint_LoadId ";
            if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId in (1,2,15)" +
                  //SLine 2017 Enhancement for Office Location
                  " and eTn_Load.bint_OfficeLocationId = " + officeLocationId +
                  " AND eTn_Load.date_LastFreeDate < CONVERT(datetime, CONVERT(varchar, DATEADD(day, 0, GETDATE()), 102)) AND eTn_Load.bint_LoadId not in (select eTn_TrackLoad.bint_LoadId  " +
    "from eTn_TrackLoad inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId" +
     " where eTn_Load.bint_LoadStatusId = 2 " +
    "group by eTn_TrackLoad.bint_LoadId ,eTn_TrackLoad.bint_LoadStatusId having COUNT(1) > 1)";
                grdManager.DeleteVisible = true;
                grdManager.LastColumnLinksList = "";
            }
            else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId in (1,2,15)" +
                  //SLine 2017 Enhancement for Office Location
                  " and eTn_Load.bint_OfficeLocationId = " + officeLocationId +
                  " AND eTn_Load.date_LastFreeDate < CONVERT(datetime, CONVERT(varchar, DATEADD(day, 0, GETDATE()), 102)) AND eTn_Load.bint_LoadId not in (select eTn_TrackLoad.bint_LoadId  " +
    "from eTn_TrackLoad inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId" +
     " where eTn_Load.bint_LoadStatusId = 2 " +
    "group by eTn_TrackLoad.bint_LoadId ,eTn_TrackLoad.bint_LoadStatusId having COUNT(1) > 1)";
                grdManager.DeleteVisible = false;
                grdManager.LastColumnLinksList = "";
            }
            else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId in (1,2,15)" +
                  //SLine 2017 Enhancement for Office Location
                  " and eTn_Load.bint_OfficeLocationId = " + officeLocationId +
                   " AND eTn_Load.date_LastFreeDate < CONVERT(datetime, CONVERT(varchar, DATEADD(day, 0, GETDATE()), 102)) AND eTn_Load.bint_LoadId not in (select eTn_TrackLoad.bint_LoadId  " +
     "from eTn_TrackLoad inner join eTn_Load on eTn_Load.bint_LoadId = eTn_TrackLoad.bint_LoadId" +
      " where eTn_Load.bint_LoadStatusId = 2 " +
     "group by eTn_TrackLoad.bint_LoadId ,eTn_TrackLoad.bint_LoadStatusId having COUNT(1) > 1)";
                grdManager.DeleteVisible = false;
                grdManager.LastColumnLinksList = "";

            }
        }
        //SLine 2021 Enhancement for IsExortLoads
        if (GridDataToShow == 6) // code for IsExortLoads list(Padmavathi)
        {
            grdManager.ShowStatus = "LFD";
            grdManager.MainTableName = "eTn_Load";
            grdManager.MainTablePK = "eTn_Load.bint_LoadId";
            grdManager.SingleRowColumnsList = "Convert(varchar(20),eTn_Load.bint_LoadId) + '^' + Convert(varchar(20),eTn_Customer.bint_CustomerId) as 'ID',eTn_Load.bint_LoadId as 'Load'," +
            "eTn_Load.nvar_Container as 'Container1',eTn_Load.nvar_Container1 as 'Container2',eTn_Load.nvar_Chasis as 'Chasis1',eTn_Load.nvar_Chasis1 as 'Chasis2', (Case when eTn_Load.bit_RailContainer=1 then 'Rail-' else '' end) + eTn_LoadType.nvar_LoadTypeDesc as 'Type'," +
            "eTn_Customer.nvar_CustomerName as 'CustomerName'," + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_Street,eTn_Customer.nvar_City,eTn_Customer.nvar_State,eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "CustomerAddress") + "," +
            "'' as 'Customer'," + CommonFunctions.DateQueryString("eTn_Load.date_LastFreeDate", "Last Free Date") + ";nvar_Pickup as 'Pickup#' ,eTn_Load.bit_IsHazmatLoad as HazmatLoad , eTn_Load.bit_IsBookingProblem as BookingProblem, eTn_Load.bit_IsPutOnHold as PutOnHold,etn_Load.bit_HotShipment as 'HotShipment', eTn_Load.bint_LoadStatusId as LoadStatusID, eTn_Load.nvar_TripType as TripType ";
            grdManager.InnerJoinClauseWithOnlySingleRowTables = " left join eTn_Customer on eTn_Customer.bint_CustomerId = eTn_Load.bint_CustomerId " +
                " left join eTn_LoadType on eTn_LoadType.bint_LoadTypeId = eTn_Load.bint_LoadTypeId left join eTn_Destination on eTn_Destination.bint_LoadId = eTn_Load.bint_LoadId left join eTn_DeliveryApptDate_View on eTn_DeliveryApptDate_View.bint_LoadId = eTn_Load.bint_LoadId ";
            if (string.Compare(Convert.ToString(Session["Role"]), "Manager", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId in (1,4)" +
                  //" AND eTn_Destination.date_DeliveryAppointmentDate >= CONVERT(datetime, CONVERT(varchar, DATEADD(day, 0, GETDATE()), 102))" +
                  //" AND eTn_Destination.date_DeliveryAppointmentDate < CONVERT(datetime, CONVERT(varchar, DATEADD(day, 1, GETDATE()), 102))" +
                  "AND eTn_Load.IsExportLoad = 1" +
                  //SLine 2017 Enhancement for Office Location
                  " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.DeleteVisible = true;
                grdManager.LastColumnLinksList = "Load Planner;Cancel";
                grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx;~/Manager/DashBoard.aspx";
            }
            else if (string.Compare(Convert.ToString(Session["Role"]), "Dispatcher", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId in (1,4)" +
                   //" AND eTn_Destination.date_DeliveryAppointmentDate >= CONVERT(datetime, CONVERT(varchar, DATEADD(day, 0, GETDATE()), 102))" +
                   //" AND eTn_Destination.date_DeliveryAppointmentDate < CONVERT(datetime, CONVERT(varchar, DATEADD(day, 1, GETDATE()), 102))" +
                   "AND eTn_Load.IsExportLoad = 1" +
                  //SLine 2017 Enhancement for Office Location
                  " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.DeleteVisible = false;
                grdManager.LastColumnLinksList = "Dispatch";
                grdManager.LastColumnPagesList = "~/Manager/Dispatch.aspx";
            }
            else if (string.Compare(Convert.ToString(Session["Role"]), "Staff", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
            {
                grdManager.SingleRowColumnsWhereClause = "eTn_Load.bint_LoadStatusId in (1,4)" +
                 //" AND eTn_Destination.date_DeliveryAppointmentDate >= CONVERT(datetime, CONVERT(varchar, DATEADD(day, 0, GETDATE()), 102))" +
                 //" AND eTn_Destination.date_DeliveryAppointmentDate < CONVERT(datetime, CONVERT(varchar, DATEADD(day, 1, GETDATE()), 102))" +
                 "AND eTn_Load.IsExportLoad = 1" +
                  //SLine 2017 Enhancement for Office Location
                  " and eTn_Load.bint_OfficeLocationId = " + officeLocationId;
                grdManager.DeleteVisible = false;
                grdManager.LastColumnLinksList = "";
            }

        }


        // code modified by Raghu - Sline 2016
        if (GridDataToShow == 5)// code for LFD expired list(Raghu)
        {
            grdManager.SingleRowColumnsOrderByClause = " order by eTn_Load.date_LastFreeDate,eTn_Load.bint_LoadId";
        }
        else
        {
            grdManager.SingleRowColumnsOrderByClause = "  order by eTn_DeliveryApptDate_View.date_DeliveryAppointmentDate,eTn_Load.bint_LoadId";
        }
        // code modified by Raghu - Sline 2016
        grdManager.VisibleColumnsList = "Load;Type;Customer;Origin;Pickup#;Pickup;Destination;Delivery Appt Date;Last Free Date";
        grdManager.MultipleRowTablesList = "eTn_Origin;eTn_Destination";
        grdManager.MultipleRowTableKeyList = "eTn_Origin.bint_LoadId;eTn_Destination.bint_LoadId";
        grdManager.MultipleRowTableColumnsList = CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin") + "^" +
            "case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+'<br/> To <br/>'+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Pickup';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Company.nvar_CompanyName,eTn_Location.nvar_Street,eTn_Location.nvar_City,eTn_Location.nvar_State,eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination") + "^" +
            "eTn_Destination.date_DeliveryAppointmentDate as 'Delivery Appt.',case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' <br/>To<br/> '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Delivery Appt Date'";
        grdManager.MultipleRowTablesInnnerJoinClause = " inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Origin.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId;" +
            "inner join eTn_Location on eTn_Location.bint_LocationId = eTn_Destination.bint_LocationId inner join eTn_Company on eTn_Company.bint_CompanyId = eTn_Location.bint_CompanyId";
        grdManager.DeleteTablesList = "eTn_Notes;eTn_UploadDocuments;eTn_TrackLoad;eTn_Receivables;eTn_Payables;eTn_AssignCarrier;eTn_AssignDriver;eTn_Origin;eTn_Destination;eTn_Load";
        grdManager.DeleteTablePKList = "bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId;bint_LoadId";

        //New Header Test changes(Madhav)
            
                      
        grdManager.LastLocationVisible = false;
        grdManager.LoadStatus = "New";
        if (Session["LoadGridPageIndex"] == null)
            Session["LoadGridPageIndex"] = 0;
        grdManager.BindGrid(Convert.ToInt32(Session["LoadGridPageIndex"]));
        //Session["ShowStatusValueforUser"] = null;

    }
}
