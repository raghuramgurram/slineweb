using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class SearchCarrier : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Search Carrier";
        if (!IsPostBack)
        {
            gridCurrent.Visible = true;
            gridCurrent.DeleteVisible = true;
            gridCurrent.EditVisible = true;
            gridCurrent.TableName = "eTn_Carrier";
            gridCurrent.Primarykey = "bint_CarrierId";
            gridCurrent.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
            gridCurrent.ColumnsList = "bint_CarrierId;" + CommonFunctions.AddressQueryString("nvar_CarrierName;nvar_Street;nvar_Suite;nvar_City;nvar_State", "Name") + ";" +
                CommonFunctions.PhoneQueryString("num_Phone", "Phone") + ";" + CommonFunctions.PhoneQueryString("num_Fax", "Fax") + ";" + CommonFunctions.DateQueryString("date_CreatedDate", "Created");
            gridCurrent.VisibleColumnsList = "Name;Phone;Fax;Created";
            gridCurrent.VisibleHeadersList = "Name;Phone;Fax;Created";
            gridCurrent.DependentTablesList = "eTn_UserLogin;eTn_AssignCarrier;eTn_Payables";
            gridCurrent.DependentTableKeysList = "bint_RolePlayerId;bint_CarrierId;eTn_Payables.bint_PayablePlayerId";
            gridCurrent.DependentTablesInnerJoinsList = ";;inner join eTn_Load on eTn_Load.bint_LoadId = eTn_Payables.bint_LoadId";
            gridCurrent.DeleteTablesList = "eTn_CarrierInsurance;eTn_CarrierContacts;eTn_Carrier";
            gridCurrent.DeleteTablePKList = "bint_CarrierId;bint_CarrierId;bint_CarrierId";
            gridCurrent.DependentTableAddtionialWhereClauseList = "bint_RoleId=" + DBClass.executeScalar("select bint_RoleId from eTn_Role where Lower(nvar_RoleName)='carrier'") + ";; Lower(eTn_Load.nvar_AssignTo)='carrier'";
            gridCurrent.EditPage = "~/Manager/NewCarrier.aspx";
            gridCurrent.OrderByClause = "nvar_CarrierName";
            //SLine 2017 Enhancement for Office Location Starts
            long officeLocationId = 0;
            if (Session["OfficeLocationID"] != null)
            {
                officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
            }
            gridCurrent.WhereClause = "bit_Status=" + ddlList.SelectedValue + " and bint_OfficeLocationId=" + officeLocationId;
            //SLine 2017 Enhancement for Office Location Ends
            gridCurrent.IsPagerVisible = true;
            gridCurrent.BindGrid(0);

            lblShowError.Visible = false;            
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Session["GridPageIndex"] = 0;
        lblShowError.Visible = false;
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        string strWhereClause = "bit_Status=" + ddlList.SelectedValue + " and bint_OfficeLocationId=" + officeLocationId;
        //SLine 2017 Enhancement for Office Location Ends
        if (txtName.Text.Trim().Length > 0)
        {
            strWhereClause += " and nvar_CarrierName like '" + txtName.Text.Trim() + "%'";
            int iCount = Convert.ToInt32(DBClass.executeScalar("select count(bint_CarrierId) from eTn_Carrier where " + strWhereClause));
            if (iCount > 1)
            {
                gridCurrent.Visible = true;
                gridCurrent.WhereClause = strWhereClause;
                gridCurrent.BindGrid(0);
            }
            else if (iCount == 1)
            {
                Response.Redirect("~/Manager/NewCarrier.aspx?" + DBClass.executeScalar("select [bint_CarrierId] from eTn_Carrier where " + strWhereClause));
            }
            else if (iCount == 0)
            {
                lblShowError.Visible = true;
                gridCurrent.Visible = false;
            }
        }
        else
        {
            gridCurrent.Visible = true;
            gridCurrent.WhereClause = strWhereClause;
            gridCurrent.BindGrid(0);
        }
    }   
}
