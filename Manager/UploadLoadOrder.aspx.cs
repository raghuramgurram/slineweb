using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

public partial class UploadLoadOrder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Upload Load Order";
        if (!IsPostBack)
        {
            if (Request.QueryString.Count > 0)
            {
                string[] strParams = Convert.ToString(Request.QueryString[0]).Split('^');
                ViewState["ID"] = strParams[0].Trim();
                MulFileUpload1_1.LoadID = Convert.ToInt64(ViewState["ID"]);
                MulFileUpload1_1.DocumentID = 1;
                GridBar1.HeaderText = "Load Number : " + Request.QueryString[0].Trim();
                ShowDocuments();
            }
        }
        //if (Request.QueryString.Count > 0)
        //{
        //    string[] strParams=Convert.ToString(Request.QueryString[0]).Split('^');
        //    upLoadOrder.LoadID = strParams[0].Trim();
        //     ViewState["ID"] = strParams[0].Trim();
        //     GridBar1.HeaderText = "Load Number : " + strParams[0].Trim();
        //     if (!Page.IsPostBack)
        //     {
        //         upLoadOrder.Fill();
        //     }
        //}
    }

    protected void btnupload_Click(object sender, EventArgs e)
    {
        if ((MulFileUpload1_1.bAdd || MulFileUpload1_1.bDelete))
        {
            try
            {
                if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_SaveUploadDocuments", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(1), MulFileUpload1_1.Files }) > 0)
                {
                    DataSet ds = DBClass.returnDataSet("select [nvar_DocumentName],[bint_FileNo] from [eTn_UploadDocuments] where [bint_LoadId] =" + Convert.ToInt64(ViewState["ID"]) + " and [bint_DocumentId] =" + Convert.ToInt64(1) + " and [bint_FileNo]>" + MulFileUpload1_1.FileNo.ToString(), "tblDocuments");
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        System.IO.DirectoryInfo DirInfo = new System.IO.DirectoryInfo(Server.MapPath("~/Documents") + "\\" + Convert.ToString(ViewState["ID"]) + "\\");
                        if (!DirInfo.Exists)
                        {
                            DirInfo.Create();
                        }
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            List<HttpPostedFile> postedfiles = MulFileUpload1_1.PostedFiles;
                            if (ds.Tables[0].Rows.Count == postedfiles.Count)
                            {
                                for (int i = 0; i < postedfiles.Count; i++)
                                {
                                    if (!System.IO.File.Exists(DirInfo.FullName + Convert.ToString(ds.Tables[0].Rows[i][0])))
                                        postedfiles[i].SaveAs(DirInfo.FullName + Convert.ToString(ds.Tables[0].Rows[i][0]));
                                }
                            }
                            Session["PostedFiles"] = null;
                        }
                        if (MulFileUpload1_1.bDelete)
                        {
                            string[] strfiles = MulFileUpload1_1.Files.Trim().Split(';');
                            if (strfiles.Length > 0)
                            {
                                System.IO.FileInfo fileinfo = null;
                                for (int i = 0; i < strfiles.Length; i++)
                                {
                                    string[] strFileName = strfiles[i].Trim().Split('^');
                                    if (strFileName.Length > 1)
                                    {
                                        if (strFileName[1] == "D")
                                        {
                                            fileinfo = new System.IO.FileInfo(DirInfo.FullName + strFileName[0]);
                                            if (fileinfo.Exists)
                                                fileinfo.Delete();
                                        }
                                    }
                                    strFileName = null;
                                    fileinfo = null;
                                }
                                strfiles = null;
                            }
                        }
                    }
                }
            }
            catch
            { }
            finally
            {
                Response.Redirect("~/Manager/NewLoad.aspx?" + Convert.ToString(ViewState["ID"]));
            }
        }
    }
    protected void ShowDocuments()
    {
        if (ViewState["ID"] != null)
        {
            //MulFileUpload1_1.GetFileNo(Convert.ToInt64(ViewState["LoadID"]),Convert.ToInt64(ddlDocuments.SelectedItem.Value));
            DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDocuments", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(1) });
            if (ds != null && ds.Tables.Count > 0)
            {
                MulFileUpload1_1.BindData(ds.Tables[0]);
                if (ds.Tables[1].Rows.Count > 0)
                    MulFileUpload1_1.FileNo = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                MulFileUpload1_1.Files = string.Empty;
                Session["PostedFiles"] = null;
            }
        }
    }   

    //protected void btnupload_Click(object sender, EventArgs e)
    //{
    //    upLoadOrder.Save();
    //    Response.Redirect("~/Manager/NewLoad.aspx?" + Convert.ToString(ViewState["ID"]));
    //}
    protected void btncancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Manager/NewLoad.aspx?" + Convert.ToString(ViewState["ID"]));
    }
}
