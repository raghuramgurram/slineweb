using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;

/// <summary>
/// Summary description for PrintCheck
/// </summary>
public class PrintCheck
{
    // Public properties (all read-only).
    public string Text
    {
        get { return this.text; }
    }
    public string Date
    {
        get { return this.date; }
    }
    public string Name
    {
        get { return this.name; }
    }
    public string Address
    {
        get { return this.address; }
    }
    public string Address1
    {
        get { return this.address1; }
    }
    public double Amount
    {
        get { return this.amount; }
    }
    public string AmountinWords
    {
        get { return this.AmountInWordsFormat(Amount); }
    }
    public Bitmap Image
    {
        get { return this.image; }
    }
    public int Width
    {
        get { return this.width; }
    }
    public int Height
    {
        get { return this.height; }
    }

    // Internal properties.
    private string text;
    private string date;
    private string name;
    private string address;
    private string address1;
    private double amount;
    private int width;
    private int height;
    private string familyName;
    private Bitmap image;

    private string AmountInWordsFormat(double Double)
    {
        string output = ConvertNumbertoWords(Double);
        string points = "00";
        if (Double.ToString().Split('.').Length == 2)
            points = Double.ToString().Split('.')[1];
        if (int.Parse(points) < 10 && points.Length<2)
            points = points + "0";
        output = output + " and " + points + "/100*******************************************************************************************************************************************";
        return output.Substring(0, 1).ToUpper() + output.Substring(1).ToLower();
    }

    public string ConvertNumbertoWords(double Double)
    {
        int number = int.Parse(Double.ToString().Split('.')[0]);        
        if (number == 0)
            return "ZERO";
        if (number < 0)
            return "minus " + ConvertNumbertoWords(Math.Abs(number));
        string words = "";
        if ((number / 1000000) > 0)
        {
            words += ConvertNumbertoWords(number / 1000000) + " MILLION ";
            number %= 1000000;
        }
        if ((number / 1000) > 0)
        {
            words += ConvertNumbertoWords(number / 1000) + " THOUSAND ";
            number %= 1000;
        }
        if ((number / 100) > 0)
        {
            words += ConvertNumbertoWords(number / 100) + " HUNDRED ";
            number %= 100;
        }
        if (number > 0)
        {
            //if (words != "")
            //    words += "AND ";
            var unitsMap = new[] { "ZERO", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN" };
            var tensMap = new[] { "ZERO", "TEN", "TWENTY", "THIRTY", "FORTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY" };

            if (number < 20)
                words += unitsMap[number];
            else
            {
                words += tensMap[number / 10];
                if ((number % 10) > 0)
                    words += " " + unitsMap[number % 10];
            }
        }
        
        return words;
        
    }

    // For generating random numbers.
    private Random random = new Random();

    // ====================================================================
    // Initializes a new instance of the CaptchaImage class using the
    // specified text, width and height.
    // ====================================================================
    //public SmallCardBack(string s, int width, int height)
    //{
    //    this.text = s;
    //    this.SetDimensions(width, height);
    //    this.GenerateImage();
    //}

    // ====================================================================
    // Initializes a new instance of the CaptchaImage class using the
    // specified text, width, height and font family.
    // ====================================================================
    public PrintCheck(string s, string date, string name, string address, string address1,  double amount, int width, int height, string familyName)
    {
        this.text = s;
        this.date = date;
        this.name = name;
        this.address = address;
        this.address1 = address1;
        this.amount = amount;
        this.SetDimensions(width, height);
        this.SetFamilyName(familyName);
        this.GenerateImage();
    }

    // ====================================================================
    // This member overrides Object.Finalize.
    // ====================================================================
    ~PrintCheck()
    {
        Dispose(false);
    }

    // ====================================================================
    // Releases all resources used by this object.
    // ====================================================================
    public void Dispose()
    {
        GC.SuppressFinalize(this);
        this.Dispose(true);
    }

    // ====================================================================
    // Custom Dispose method to clean up unmanaged resources.
    // ====================================================================
    protected virtual void Dispose(bool disposing)
    {
        if (disposing)
            // Dispose of the bitmap.
            this.image.Dispose();
    }

    // ====================================================================
    // Sets the image width and height.
    // ====================================================================
    private void SetDimensions(int width, int height)
    {
        // Check the width and height.
        if (width <= 0)
            throw new ArgumentOutOfRangeException("width", width, "Argument out of range, must be greater than zero.");
        if (height <= 0)
            throw new ArgumentOutOfRangeException("height", height, "Argument out of range, must be greater than zero.");
        this.width = width;
        this.height = height;
    }

    // ====================================================================
    // Sets the font used for the image text.
    // ====================================================================
    private void SetFamilyName(string familyName)
    {
        // If the named font is not installed, default to a system font.
        try
        {
            Font font = new Font(this.familyName, 12F);
            this.familyName = familyName;
            font.Dispose();
        }
        catch (Exception ex)
        {
            this.familyName = System.Drawing.FontFamily.GenericSerif.Name;
        }
    }

    // ====================================================================
    // Creates the bitmap image.
    // ====================================================================
    private void GenerateImage()
    {
        try
        {
            // Create a new 32-bit bitmap image.
            Bitmap bitmap = new Bitmap(this.width, this.height, PixelFormat.Format32bppArgb);

            // Create a graphics object for drawing.
            Graphics g = Graphics.FromImage(bitmap);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            Rectangle rect = new Rectangle(0, 0, this.width, this.height);

            // Fill in the background.
            HatchBrush hatchBrush = new HatchBrush(HatchStyle.SmallConfetti, Color.White, Color.White);
            g.FillRectangle(hatchBrush, rect);

            // Set up the text font.
            SizeF size;
            float fontSize = rect.Height + 1;
            Font font;
            // Adjust the font size until the text fits within the image.
            do
            {
                fontSize--;
                font = new Font(this.familyName, fontSize, FontStyle.Bold);
                size = g.MeasureString(this.text, font);
            } while (size.Width > rect.Width);
            int heightoffset = Convert.ToInt32(ConfigurationSettings.AppSettings["CheckPrintOffset"]);
            // Set up the text format.
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;

            SolidBrush sbDate = new SolidBrush(Color.Black);
            g.DrawString(Date, new Font(font.FontFamily, 11, FontStyle.Regular), sbDate, new PointF(685, 82 + heightoffset));

            //SolidBrush sbName = new SolidBrush(Color.Black);
            g.DrawString(Name.ToUpper(), new Font(font.FontFamily, 11, FontStyle.Regular), sbDate, new PointF(120, 133 + heightoffset));

            //SolidBrush sbAmount = new SolidBrush(Color.Black);
            g.DrawString("**" + Convert.ToDecimal(Amount).ToString("#,##0.00"), new Font(font.FontFamily, 11, FontStyle.Regular), sbDate, new PointF(668, 135 + heightoffset));

            //SolidBrush sbAmountinWords = new SolidBrush(Color.Black);
            g.DrawString((AmountinWords.Substring(0,85)), new Font(font.FontFamily, 11, FontStyle.Regular), sbDate, new PointF(28, 162 + heightoffset));

            //SolidBrush brush34 = new SolidBrush(Color.Black);
            g.DrawString(Name, new Font(font.FontFamily, 11, FontStyle.Regular), sbDate, new PointF(105, 195 + heightoffset));
            g.DrawString(Address.ToUpper(), new Font(font.FontFamily, 11, FontStyle.Regular), sbDate, new PointF(105, 215 + heightoffset));
            g.DrawString(Address1.ToUpper(), new Font(font.FontFamily, 11, FontStyle.Regular), sbDate, new PointF(105, 235 + heightoffset));

            ///////////////
            // Add some random noise.
            //int m = Math.Max(rect.Width, rect.Height);
            //for (int i = 0; i < (int)(rect.Width * rect.Height / 30F); i++)
            //{
            //    int x = this.random.Next(rect.Width);
            //    int y = this.random.Next(rect.Height);
            //    int w = this.random.Next(m / 50);
            //    int h = this.random.Next(m / 50);
            //    g.FillEllipse(hatchBrush, x, y, w, h);
            //}

            // Clean up.
            font.Dispose();
            hatchBrush.Dispose();
            g.Dispose();

            // Set the image.
            this.image = bitmap;
        }
        catch (Exception ex)
        {
            Bitmap bitmap = new Bitmap(this.width, this.height, PixelFormat.Format32bppArgb);
            // Create a graphics object for drawing.
            Graphics g = Graphics.FromImage(bitmap);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            Rectangle rect = new Rectangle(0, 0, this.width, this.height);

            // Fill in the background.
            HatchBrush hatchBrush = new HatchBrush(HatchStyle.SmallConfetti, Color.White, Color.White);
            g.FillRectangle(hatchBrush, rect);
            SizeF size;
            float fontSize = rect.Height + 1;
            Font font;
            // Adjust the font size until the text fits within the image.
            do
            {
                fontSize--;
                font = new Font(this.familyName, fontSize, FontStyle.Bold);
                size = g.MeasureString(this.text, font);
            }
            while (size.Width > rect.Width);
            int heightoffset = Convert.ToInt32(ConfigurationSettings.AppSettings["CheckPrintOffset"]);
            // Set up the text format.
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;
            SolidBrush Time = new SolidBrush(Color.Black);
            g.DrawString("There is Error in data. Please check and try again.", new Font(font.FontFamily, 10, FontStyle.Regular), Time, new PointF(0, 0));

            font.Dispose();
            hatchBrush.Dispose();
            g.Dispose();
            // Set the image.
            this.image = bitmap;
        }
    }

}
