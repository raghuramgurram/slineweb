using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Globalization;

public partial class Dispatch : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Dispatch Load";
        btnUnassignCarrier.Visible = false;
        lblErrDriver.Visible = false;
        lblErrCarrier.Visible = false;
        txtOrderBillDate.DisplayError = false;
        //Using a flag value to check driver details

        if (!IsPostBack)
        {
            //btnSaveCarrier.Attributes.Add("onclick", "javascript:ValidateDriverAssign('" + ddlDriverAssign.ClientID + "','Select')");
            //btnSaveCarrier.Attributes.Add("onclick", "javascript:ValidateDriverAssign('" + ddlDrivers.ClientID + "','--Select Driver--');");
            // btnSaveCarrier.Attributes.Add("onClick", "javascript:return ValidateDriverAssign()");

            if (Request.QueryString.Count > 0)
            {
                ViewState["ID"] = Convert.ToInt64(Request.QueryString[0]);
                lnkUploadStatus.PostBackUrl = "UpdateLoadStatus.aspx?" + Convert.ToString(ViewState["ID"]);
                //lnkUploadStatus.Attributes.Add("onClick", "javascript:parent.location('UpdateLoadStatus.aspx?" + Convert.ToString(ViewState["ID"]) + "');"); +'~'+[bit_IsHazmatCertified]

                // Checking the is the load hazmatload
                int notesCount = int.Parse(DBClass.executeScalar("SELECT COUNT([bint_NoteId]) from [eTn_Notes] where [bint_LoadId]=" + Convert.ToInt64(ViewState["ID"])));
                int officeNotesCount = int.Parse(DBClass.executeScalar("SELECT COUNT([bint_OfficeNoteId]) from [eTn_OfficeNotes] where [bint_LoadId]=" + Convert.ToInt64(ViewState["ID"])));
                lblWarning.Visible = (notesCount == 0 && officeNotesCount == 0) ? false : true;
                string strisHazmat = DBClass.executeScalar("Select [bit_IsHazmatLoad] from [eTn_Load] where [bint_LoadId]=" + Convert.ToInt64(ViewState["ID"]));
                bool strisPutOnHold = Convert.ToBoolean(DBClass.executeScalar("Select [bit_IsPutOnHold] from [eTn_Load] where [bint_LoadId]=" + Convert.ToInt64(ViewState["ID"])));

                string strAssigned = DBClass.executeScalar("Select [nvar_AssignTo]+'~'+ CONVERT (nvarchar(50),[bint_LoadStatusId]) from [eTn_Load] where [bint_LoadId]=" + Convert.ToInt64(ViewState["ID"]));


                string[] strList = new string[3];
                if (!string.IsNullOrEmpty(strAssigned))
                    strList = strAssigned.Split('~');
                ViewState["State"] = strList[0];
                ViewState["LoadStatusId"] = strList[1];
                // viewstate with the hazmatdetails
                ViewState["LoadIsHazmatCertified"] = Convert.ToBoolean(strisHazmat);
                ViewState["LoadIsPutOnHold"] = strisHazmat;
                EnableButtons(strList[1]);
                if (strisPutOnHold)
                {
                    lblWarningPOH.Visible = strisPutOnHold;
                    btnAssignToLoadPlaner.Visible = !strisPutOnHold;
                    btnConformAssign.Visible = !strisPutOnHold;
                    btnSaveCarrier.Visible = !strisPutOnHold;
                    btnSave.Visible = !strisPutOnHold;
                }
                if (strList[0].Trim().Length == 0)
                {
                    //ddlDriverAssign.Items.Remove(ddlDriverAssign.Items.FindByText("Edit"));
                    lnkUploadStatus.Visible = false;
                    ViewState["Mode"] = "New";
                    NewMode();
                    //SLine 2016 ---- Added new condition for default view of the Dispatcher, which is same as Carrier
                    //if (Session["Role"].ToString().ToUpper() == "DISPATCHER")
                    //{
                    //    ShowCarriers(false);
                    //    FillCarriers();
                    //    FillDrivers();
                    //}
                }
                else
                {
                    // ddlDriverAssign.Items.Insert(0, new ListItem("Select", "Select"));
                    ViewState["Mode"] = "Edit";
                    lnkUploadStatus.Visible = true;
                    if (string.Compare(strList[0], "Driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                    {
                        ShowCarriers(false);
                        FillDrivers();
                        FillTeams();
                        //FillDriverDetails();
                    }
                    else if (string.Compare(strList[0], "Carrier", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                    {
                        ShowCarriers(true);
                        FillCarriers();
                    }
                }
                lnkUpload.PostBackUrl = "UpLoadDocuments.aspx?" + Convert.ToString(ViewState["ID"]).Trim();
                FillDetails(Convert.ToInt64(ViewState["ID"]));
                lblLoadId.Text = "Load Number : " + Convert.ToString(ViewState["ID"]) + " | " + "Last Free Date : " + DBClass.executeScalar("select convert(varchar(20),[date_LastFreeDate],101) as [Date] from [eTn_Load] where [bint_LoadId]=" + Convert.ToString(ViewState["ID"]));
                lnkLoadDetails.Text = "<a href='LoadDetails.aspx?" + Convert.ToInt64(ViewState["ID"]) + "' target=_blank>Load Details</a>";
                if (string.Compare(strList[0], "Driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    lnkLoadRequest.Enabled = false;
                }
                else
                {
                    lnkLoadRequest.Enabled = true;
                    lnkLoadRequest.PostBackUrl = "LoadRequest.aspx?" + Convert.ToString(ViewState["ID"]).Trim();
                }
                Session["BackPage4"] = "~/Manager/Dispatch.aspx?" + Convert.ToString(ViewState["ID"]);
                // Session["BackPage3"] = "~/Manager/Dispatch.aspx?" + Convert.ToString(ViewState["ID"]); 
                Session["BackPage5"] = null;
                //////lnkUploadStatus.PostBackUrl = "UpdateLoadStatus.aspx?" + Convert.ToInt64(ViewState["ID"]);

                //lnkLoadDetails.Attributes.Add("onClick", "javascript:WinOpen(" + Convert.ToInt64(ViewState["ID"]) + ")");
                CheckStatus(strList[0]);
            }
            ViewState["IsDriverValid"] = "0";
        }
    }

    private void EnableButtons(string loadStatusId)
    {
        if ((loadStatusId.Trim() == "1" || loadStatusId.Trim() == "4"))
        {
            if (Session["Role"].ToString() == "Manager")
            {
                btnAssignToLoadPlaner.Visible = true;
                btnConformAssign.Visible = false;
                btnSaveCarrier.Visible = false;
            }
            else if (Convert.ToBoolean(Session["LoadPlannerViewPermission"].ToString()))
            {
                btnAssignToLoadPlaner.Visible = true;
                btnConformAssign.Visible = false;
                btnSaveCarrier.Visible = false;
            }
            else
            {
                btnAssignToLoadPlaner.Visible = false;
                btnConformAssign.Visible = false;
                btnSaveCarrier.Visible = true;
            }
        }

        if (loadStatusId.Trim() == "15")
        {
            btnAssignToLoadPlaner.Visible = false;
            btnConformAssign.Visible = true;
            btnSaveCarrier.Visible = false;
        }
        if (loadStatusId.Trim() != "1" && loadStatusId.Trim() != "15" && loadStatusId.Trim() != "4")
        {
            btnAssignToLoadPlaner.Visible = false;
            btnConformAssign.Visible = false;
            btnSaveCarrier.Visible = true;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Session["ReturnValue"] != null && ViewState["ID"] != null)
        {
            try
            {
                if (Convert.ToInt32(Session["ReturnValue"]) == 1)
                {
                    SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_MakeNewAgain", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(Session["UserLoginId"]) });
                    if (Session["Role"].ToString() == "Manager")
                    {
                        btnAssignToLoadPlaner.Visible = true;
                        btnSaveCarrier.Visible = false;
                    }
                    else
                    {
                        btnAssignToLoadPlaner.Visible = false;
                        btnSaveCarrier.Visible = true;
                    }
                    btnConformAssign.Visible = false;

                }
                else
                {
                    SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_DeleteDriversStatusChange", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(Session["UserLoginId"]) });
                }
                Session["ReturnValue"] = null;
            }
            catch
            { }
        }
        dtpAssignDriver.SetDate(DateTime.Now);
        tpAssignDriver.Time = DateTime.Now.ToShortTimeString();
    }
    private void CheckStatus(string DriverOrCarrier)
    {
        string strStatus = DBClass.executeScalar("Select eTn_LoadStatus.[nvar_LoadStatusDesc] from [eTn_Load] inner join [eTn_LoadStatus] on eTn_Load.[bint_LoadStatusId]=[eTn_LoadStatus].[bint_LoadStatusId] where [bint_LoadId]=" + Convert.ToInt64(ViewState["ID"]));
        if (string.Compare(strStatus, "New", true, System.Globalization.CultureInfo.CurrentCulture) != 0)
        {
            if (string.Compare(DriverOrCarrier, "Carrier", true, System.Globalization.CultureInfo.CurrentCulture) == 0 && radCarrier.Checked)
            {
                radCarrier.Enabled = true;
                ddlCarrier.Enabled = true;
                txtInvoice.ReadOnly = false;
                btnUnassignCarrier.Visible = true;

                rabDriver.Enabled = false;
                ddlDrivers.Enabled = false;
                txtFrom.ReadOnly = true;
                txtFromCity.ReadOnly = true;
                txtFromstate.ReadOnly = true;
                txtTO.ReadOnly = true;
                txtToCity.ReadOnly = true;
                txtState.ReadOnly = true;
                txtTag.ReadOnly = true;
            }
            else if (string.Compare(DriverOrCarrier, "Driver", true, System.Globalization.CultureInfo.CurrentCulture) == 0 && rabDriver.Checked)
            {
                radCarrier.Enabled = false;
                ddlCarrier.Enabled = false;
                txtInvoice.ReadOnly = false;
                btnUnassignCarrier.Visible = false;

                rabDriver.Enabled = true;
                ddlDrivers.Enabled = true;
                txtFrom.ReadOnly = false;
                txtFromCity.ReadOnly = false;
                txtFromstate.ReadOnly = false;
                txtTO.ReadOnly = false;
                txtToCity.ReadOnly = false;
                txtState.ReadOnly = false;
                txtTag.ReadOnly = false;
            }
        }
    }

    private DateTime GetFirstDayInWeek()
    {
        DayOfWeek firstDay = CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;
        DateTime firstDayInWeek = DateTime.Now.Date;
        while (firstDayInWeek.DayOfWeek != firstDay)
            firstDayInWeek = firstDayInWeek.AddDays(-1);
        return firstDayInWeek.AddDays(1);
    }

    private void NewMode()
    {
        ShowCarriers(true);
        FillCarriers();
    }
    private void ShowCarriers(bool show)
    {
        lblCarrier.Visible = show;
        ddlCarrier.Visible = show;
        lblInvoice.Visible = show;
        txtInvoice.Visible = show;
        radCarrier.Checked = show;
        lblCustAssigned.Visible = show;
        dtpCustAssigned.Visible = show;
        tpCustAssigned.Visible = show;
        rabDriver.Checked = (!show);
        pnlDrives.Visible = (!show);
        Grid2.Visible = (!show);
    }
    private void FillCarriers()
    {
        pnlSpace.Visible = false;
        ddlCarrier.Items.Clear();
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        //SLine 2017 Enhancement for Office Location Ends
        //DataTable dt = DBClass.returnDataTable("select bint_CarrierId,nvar_CarrierName from eTn_Carrier order by nvar_CarrierName");
        //SLine 2017 Enhancement for Office Location added parameter to ExecuteDataset
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetCarrierNames", new object[] { officeLocationId });
        if (ds.Tables.Count > 0)
        {
            ddlCarrier.DataSource = ds.Tables[0];
            ddlCarrier.DataTextField = "nvar_CarrierName";
            ddlCarrier.DataValueField = "bint_CarrierId";
            ddlCarrier.DataBind();
        }
        ddlCarrier.Items.Insert(0, "--Select Carrier--");
        ds.Tables.Clear();
        if (ViewState["ID"] != null)
        {
            //dt = DBClass.returnDataTable("select [bint_CarrierId],[nvar_Invoice] from [eTn_AssignCarrier] where [bint_LoadId]="+Convert.ToInt64(ViewState["ID"]));
            ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetAssignCarrierDetails", new object[] { Convert.ToInt64(ViewState["ID"]) });
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtInvoice.Text = Convert.ToString(ds.Tables[0].Rows[0][1]);
                ddlCarrier.SelectedIndex = ddlCarrier.Items.IndexOf(ddlCarrier.Items.FindByValue(Convert.ToString(ds.Tables[0].Rows[0][0])));
                try
                {
                    DateTime dt = Convert.ToDateTime(ds.Tables[1].Rows[0][0]);
                    dtpCustAssigned.SetDate(dt);
                    tpCustAssigned.Time = dt.ToShortTimeString();
                }
                catch
                {
                }
            }
            else
                tpCustAssigned.Time = DateTime.Now.ToShortTimeString();
        }
        if (ds != null) { ds.Dispose(); ds = null; }
    }
    private void FillDrivers()
    {
        pnlSpace.Visible = true;
        ddlDrivers.Items.Clear();
        //dt = DBClass.returnDataTable("select bint_EmployeeId as 'Id',nvar_NickName as 'Name' from eTn_Employee where bint_RoleId=(select R.bint_RoleId from eTn_Role R where lower(nvar_RoleName)='driver') order by [nvar_NickName]");
        DataSet ds;
        //if(Convert.ToString(ViewState["State"]).Trim().Length==0)

        // changes to load the hazmit drivers if the load is hazmit
        bool ishazmit = Convert.ToBoolean(ViewState["LoadIsHazmatCertified"]);
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        //SLine 2017 Enhancement for Office Location Ends
        //SLine 2017 Enhancement for Office Location added a parameter officeLocationId at the end to ExecuteDataset
        ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetActiveDriversWithPaymentsandHazmat", new object[] { GetFirstDayInWeek(), DateTime.Now, (ishazmit ? 1 : 0), officeLocationId, 0 });
        //else
        //    ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriverNames");
        if (ds.Tables.Count > 0)
        {
            ddlDrivers.DataSource = ds.Tables[0];
            ddlDrivers.DataTextField = "Name";
            ddlDrivers.DataValueField = "Id";
            ddlDrivers.DataBind();

            dtpAssignDriver.SetDate(DateTime.Now);
            tpAssignDriver.Time = DateTime.Now.ToShortTimeString();
        }
        ddlDrivers.Items.Insert(0, "--Select Driver--");
        if (ds != null) { ds.Dispose(); ds = null; }
    }

    protected void rabDriver_CheckedChanged(object sender, EventArgs e)
    {
        if (rabDriver.Checked)
        {
            ShowCarriers(false);
            FillDrivers();
            FillTeams();
        }
    }
    protected void radCarrier_CheckedChanged(object sender, EventArgs e)
    {
        if (radCarrier.Checked)
        {
            ShowCarriers(true);
            FillCarriers();
        }
    }

    private void FillDetails(long ID)
    {
        long ShippingId = 0;
        Grid3.Visible = true;
        Grid3.DeleteVisible = false;
        Grid3.EditVisible = false;

        //Origin Grid
        Grid3.TableName = "eTn_Origin";
        Grid3.Primarykey = "eTn_Origin.bint_OriginId";
        Grid3.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        Grid3.ColumnsList = "eTn_Origin.bint_OriginId as 'Id';eTn_Origin.nvar_PO as 'PO';eTn_Origin.bint_Pieces as 'Pieces';eTn_Origin.num_Weight as 'Weight';case when eTn_Origin.date_PickupToDateTime is not null then convert(varchar(25),eTn_Origin.date_PickupDateTime)+' To '+substring(convert(varchar(30),[date_PickupToDateTime]),len(convert(varchar(30),[date_PickupToDateTime]))-6,len(convert(varchar(30),[date_PickupToDateTime]))) else convert(varchar(25),eTn_Origin.date_PickupDateTime) end as 'Date';eTn_Origin.nvar_ApptGivenby as 'ApptGivenBy';eTn_Origin.nvar_App as 'Appt';" +
                "eTn_Company.nvar_CompanyName + '<br/>' + " + CommonFunctions.AddressQueryStringWithPhone("eTn_Location.nvar_Street;eTn_Location.nvar_Suite;eTn_Location.nvar_City;eTn_Location.nvar_State;eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Origin");
        //CommonFunctions.AddressQueryString("eTn_Company.nvar_CompanyName;eTn_Location.nvar_Street;eTn_Location.nvar_Suite;eTn_Location.nvar_City;eTn_Location.nvar_State;eTn_Location.nvar_Zip","Origin");
        Grid3.VisibleColumnsList = "PO;Pieces;Weight;Origin;Date;Appt;ApptGivenBy";
        Grid3.VisibleHeadersList = "P.O.#;Pieces;Weight;Origin(s);Pickup Date /Time;Appt.#;Appt. Given by";
        Grid3.InnerJoinClause = " inner join eTn_Location on eTn_Origin.bint_LocationId = eTn_Location.bint_LocationId " +
               " inner join eTn_Company on eTn_Location.bint_CompanyId = eTn_Company.bint_CompanyId ";
        Grid3.WhereClause = "eTn_Origin.[bint_LoadId]=" + ID;
        Grid3.IsPagerVisible = false;
        Grid3.BindGrid(0);

        ViewState["OrgPhoneNo"] = ViewState["Origin"] = ViewState["PickUpDate"] = string.Empty;
        if (Grid3.GridControl.Items.Count > 0)
        {
            //string tempText = Grid3.GridControl.Items[0].Cells[7].Text.Replace("&nbsp;", " ").Trim();
            string tempText = Grid3.GridControl.Items[0].Cells[7].Text.Replace("<br/>", ",").Replace("&nbsp;", " ").Trim();
            if (tempText.Contains("Tel:"))
            {
                string[] str;
                str = tempText.Split(new string[] { "Tel:" }, StringSplitOptions.None);
                ViewState["OrgPhoneNo"] = str[1].Trim();
                tempText = str[0].Trim();
            }
            /*if (tempText.Contains("<br/>"))
            {
                string[] str;
                str = tempText.Split(new string[] { "<br/>" }, StringSplitOptions.None);
                tempText = str[0].Trim();
            }*/
            ViewState["PickUpDate"] = Grid3.GridControl.Items[0].Cells[4].Text;
            ViewState["Origin"] = tempText.Substring(tempText.Length - 1) == "," ? tempText.Substring(0, tempText.Length - 1) : tempText;
        }

        //Destination Grid
        Grid4.Visible = true;
        Grid4.DeleteVisible = false;
        Grid4.EditVisible = false;
        Grid4.TableName = "eTn_Destination";
        Grid4.Primarykey = "eTn_Destination.bint_DestinationId";
        Grid4.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        Grid4.ColumnsList = "eTn_Destination.bint_DestinationId as 'Id';eTn_Destination.nvar_PO as 'PO';eTn_Destination.bint_Pieces as 'Pieces';eTn_Destination.num_Weight as 'Weight';case when eTn_Destination.date_DeliveryAppointmentToDate is not null then convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate)+' To '+substring(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]),len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))-6,len(convert(varchar(30),eTn_Destination.[date_DeliveryAppointmentToDate]))) else convert(varchar(25),eTn_Destination.date_DeliveryAppointmentDate) end as 'Date';eTn_Destination.nvar_ApptGivenby as 'ApptGivenBy';eTn_Destination.nvar_App as 'Appt';" +
            "eTn_Company.nvar_CompanyName + '<br/>' + " + CommonFunctions.AddressQueryStringWithPhone("eTn_Location.nvar_Street;eTn_Location.nvar_Suite;eTn_Location.nvar_City;eTn_Location.nvar_State;eTn_Location.nvar_Zip", "eTn_Location.num_Phone", "Destination");
        //CommonFunctions.AddressQueryString("eTn_Company.nvar_CompanyName;eTn_Location.nvar_Street;eTn_Location.nvar_Suite;eTn_Location.nvar_City;eTn_Location.nvar_State;eTn_Location.nvar_Zip", "Destination");
        Grid4.VisibleColumnsList = "PO;Pieces;Weight;Destination;Date;Appt;ApptGivenBy";
        Grid4.VisibleHeadersList = "P.O.#;Pieces;Weight;Destination(s);Appt. Date /Time;Appt.#;Appt. Given by";
        Grid4.InnerJoinClause = " inner join eTn_Location on eTn_Destination.bint_LocationId = eTn_Location.bint_LocationId " +
               " inner join eTn_Company on eTn_Location.bint_CompanyId = eTn_Company.bint_CompanyId ";
        Grid4.WhereClause = "eTn_Destination.[bint_LoadId]=" + ID;
        Grid4.IsPagerVisible = false;
        Grid4.BindGrid(0);
        ViewState["PhoneNo"] = ViewState["DelivaryDate"] = ViewState["Destination"] = string.Empty;
        if (Grid4.GridControl.Items.Count > 0)
        {
            ViewState["DelivaryDate"] = Grid4.GridControl.Items[0].Cells[4].Text;
            string tempText = Grid4.GridControl.Items[0].Cells[7].Text.Replace("<br/>", ",").Replace("&nbsp;", " ").Trim();

            if (tempText.Contains("Tel:"))
            {
                string[] str;
                str = tempText.Split(new string[] { "Tel:" }, StringSplitOptions.None);
                ViewState["PhoneNo"] = str[1].Trim();
                tempText = str[0].Trim();
            }
            ViewState["Destination"] = tempText.Substring(tempText.Length - 1) == "," ? tempText.Substring(0, tempText.Length - 1) : tempText;
        }

        //Drivers Grid
        Grid2.Visible = true;
        Grid2.DeleteVisible = true;
        Grid2.EditVisible = true;
        Grid2.TableName = "eTn_AssignDriver";
        Grid2.Primarykey = "eTn_AssignDriver.bint_EmployeeId";
        Grid2.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        Grid2.ColumnsList = "eTn_AssignDriver.bint_AssignedId;eTn_AssignDriver.bint_AssignedId as 'Id';eTn_Employee.nvar_NickName+case when len(cast([eTn_Employee].[num_Mobile]  as varchar(10)))=10 then ' <br/> Mobile: '+ substring(cast([eTn_Employee].[num_Mobile]  as varchar(10)),1,3)+'-'+substring(cast([eTn_Employee].[num_Mobile]  as varchar(10)),4,3)+'-'+substring(cast([eTn_Employee].[num_Mobile]  as varchar(10)),7,4) else '' end  as 'Name';eTn_AssignDriver.nvar_Tag as 'Tag';" + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_From;eTn_AssignDriver.nvar_FromCity;eTn_AssignDriver.nvar_FromState", "From") + ";" + CommonFunctions.AddressQueryString("eTn_AssignDriver.nvar_To;eTn_AssignDriver.nvar_Tocity;eTn_AssignDriver.nvar_ToState", "To") + ";" +
                        "isnull(eTn_Payables.[num_Charges],0)-isnull(eTn_Payables.[num_AdvancedPaid],0)+isnull(eTn_Payables.[num_Dryrun],0)+isnull(eTn_Payables.[num_FuelSurcharge],0)+isnull(eTn_Payables.[num_FuelSurchargeAmounts],0)+" +
                        "isnull(eTn_Payables.[num_PierTermination],0)+isnull(eTn_Payables.[bint_ExtraStops],0)+" +
                        "isnull(eTn_Payables.[num_LumperCharges],0) +isnull(eTn_Payables.[num_DetentionCharges],0)+isnull(eTn_Payables.[num_ChassisSplit],0)+" +
                        "isnull(eTn_Payables.[num_ChassisRent],0)+isnull(eTn_Payables.[num_Pallets],0)+isnull(eTn_Payables.[num_ContainerWashout],0)+" +
                        "isnull(eTn_Payables.[num_YardStorage],0)+isnull(eTn_Payables.[num_RampPullCharges],0)+isnull(eTn_Payables.[num_TriaxleCharge],0)+" +
                        "isnull(eTn_Payables.[num_TransLoadCharges],0)+isnull(eTn_Payables.[num_HLScaleCharges],0)+" +
                        "isnull(eTn_Payables.[num_ScaleTicketCharges],0)+isnull(eTn_Payables.[num_OtherCharges1],0)+" +
                        "isnull(eTn_Payables.[num_OtherCharges2],0)+isnull(eTn_Payables.[num_OtherCharges3],0)+isnull(eTn_Payables.[num_OtherCharges4],0)-" +
                        "isnull(eTn_Payables.[num_PaidToAnotherDriver],0)+isnull(eTn_Payables.[num_TollCharges],0)+isnull(eTn_Payables.[num_SlipCharges],0)+isnull(eTn_Payables.[num_RepairCharges],0) AS 'Charges';" +
                        "'<a target=_blank href=LoadRequest.aspx?'+convert(varchar(20),eTn_AssignDriver.bint_LoadId)+'^'+convert(varchar(20),eTn_AssignDriver.bint_EmployeeId)+'^'+convert(varchar(20),eTn_AssignDriver.bint_AssignedId)+'>Load Request</a>' as 'LoadRequest'";
        Grid2.VisibleColumnsList = "Name;Tag;From;To;Charges;LoadRequest";
        Grid2.VisibleHeadersList = "Name;Tag;From City;To City;Charges;";
        Grid2.InnerJoinClause = " inner join eTn_Employee on eTn_Employee.bint_EmployeeId = eTn_AssignDriver.bint_EmployeeId " +
                           " left outer join eTn_Payables on eTn_Payables.bint_AssignedId = eTn_AssignDriver.bint_AssignedId";
        //" left outer join eTn_Payables on eTn_Payables.bint_LoadId = eTn_AssignDriver.bint_LoadId and eTn_Payables.bint_PayablePlayerId=eTn_AssignDriver.bint_EmployeeId";
        Grid2.WhereClause = "eTn_AssignDriver.[bint_LoadId]=" + ID;
        Grid2.DeleteTablesList = "eTn_Payables;eTn_AssignDriver";
        Grid2.DeleteTablePKList = "eTn_Payables.bint_AssignedId;eTn_AssignDriver.bint_AssignedId";
        Grid2.DeleteTableAddtionialWhereClauseList = "eTn_Payables.bint_LoadId=" + ID + ";eTn_AssignDriver.bint_LoadId=" + ID;
        Grid2.EditPage = "~/Manager/EditDriver.aspx?" + ID;
        Grid2.IsDistinctRequired = true;
        Grid2.IsPagerVisible = false;
        Grid2.PrintVisible = true;
        Grid2.OrderByClause = "eTn_AssignDriver.[date_DateAssigned]";
        Grid2.PrintPage = "~/Manager/PrintDriver.aspx?" + ID;
        Grid2.BindGrid(0);

        ViewState["DriverName"] = ViewState["SMSNo"] = ViewState["ToLoc"] = ViewState["FromLoc"] = ViewState["Tag"] = string.Empty;
        if (Grid2.GridControl.Items.Count > 0)
        {
            int index = Grid2.GridControl.Items.Count - 1;
            string fromLoc = Grid2.GridControl.Items[index].Cells[4].Text.Replace("<br/>", ",").Replace("&nbsp;", " ").Trim();
            /*if (!string.IsNullOrEmpty(fromLoc))
                fromLoc = fromLoc.Split(',')[0];*/
            string ToLoc = Grid2.GridControl.Items[index].Cells[5].Text.Replace("<br/>", ",").Replace("&nbsp;", " ").Trim();
            if (fromLoc.Length > 0)
                ViewState["FromLoc"] = fromLoc.Substring(fromLoc.Length - 1) == "," ? fromLoc.Substring(0, fromLoc.Length - 1) : fromLoc;
            if (ToLoc.Length > 0)
                ViewState["ToLoc"] = ToLoc.Substring(ToLoc.Length - 1) == "," ? ToLoc.Substring(0, ToLoc.Length - 1) : ToLoc;
            ViewState["Tag"] = Grid2.GridControl.Items[index].Cells[3].Text;
            string tempText = Grid2.GridControl.Items[index].Cells[2].Text.Replace("<br/>", ",").Replace("&nbsp;", " ").Trim();
            if (tempText.Contains("Mobile:"))
            {
                string[] str;
                str = tempText.Split(new string[] { "Mobile:" }, StringSplitOptions.None);
                ViewState["SMSNo"] = str[1].Trim().Replace("-", "");
                tempText = str[0].Trim();
            }
            ViewState["DriverName"] = tempText.Substring(tempText.Length - 1) == "," ? tempText.Substring(0, tempText.Length - 1) : tempText;
        }

        //string strQuery = "SELECT C.[nvar_CustomerName],L.[nvar_Pickup],L.[nvar_Container],L.[nvar_Booking],CT.[nvar_CommodityTypeDesc],L.[nvar_Seal],L.[date_DateOfCreation],L.[bint_ShippingId],L.[nvar_RailBill],L.[date_OrderBillDate],L.[nvar_PersonalBill] FROM [eTn_Load] L " +
        //                "inner join [eTn_Customer] C on L.[bint_CustomerId]=C.[bint_CustomerId] inner join [eTn_CommodityType] CT on L.[bint_CommodityTypeId]=CT.[bint_CommodityTypeId] " +
        //                " WHERE L.[bint_LoadId]=" + ID;
        //DataTable dt = DBClass.returnDataTable(strQuery);
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDispacthLoadDetails", new object[] { ID });
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                Label1.Text = Convert.ToString(ds.Tables[0].Rows[0][0]);
                lblPickUp.Text = Convert.ToString(ds.Tables[0].Rows[0][1]);
                spanishazmat.Visible = Convert.ToBoolean(ds.Tables[0].Rows[0][19]);
                if (Convert.ToString(ds.Tables[0].Rows[0][2]).Trim().Length > 0)
                {
                    if (Convert.ToString(ds.Tables[0].Rows[0][3]).Trim().Length > 0)
                    {
                        lblContainer.Text = Convert.ToString(ds.Tables[0].Rows[0][2]) + "<br/>" + Convert.ToString(ds.Tables[0].Rows[0][3]);
                        ViewState["ContainerNo"] = Convert.ToString(ds.Tables[0].Rows[0][2]) + "-" + Convert.ToString(ds.Tables[0].Rows[0][3]);
                    }
                    else
                    {
                        lblContainer.Text = Convert.ToString(ds.Tables[0].Rows[0][2]);
                        ViewState["ContainerNo"] = Convert.ToString(ds.Tables[0].Rows[0][2]);
                    }
                }
                else
                {
                    lblContainer.Text = Convert.ToString(ds.Tables[0].Rows[0][3]);
                    ViewState["ContainerNo"] = Convert.ToString(ds.Tables[0].Rows[0][2]);
                }
                lblBooking.Text = Convert.ToString(ds.Tables[0].Rows[0][4]);
                lblCommodity.Text = Convert.ToString(ds.Tables[0].Rows[0][5]);
                lblSeal.Text = Convert.ToString(ds.Tables[0].Rows[0][6]);
                lblCreated.Text = Convert.ToString(ds.Tables[0].Rows[0][7]);
                //lblPersong.Text = Convert.ToString(ds.Tables[0].Rows[0][8]);
                // lblRef.Text = Convert.ToString(ds.Tables[0].Rows[0][8]);
                //strAssigned = Convert.ToString(ds.Tables[0].Rows[0][9]);
                // lblStatus.Text = Convert.ToString(ds.Tables[0].Rows[0][10]);
                ViewState["LoadState"] = Convert.ToString(ds.Tables[0].Rows[0][10]);
                txtRailBill.Text = Convert.ToString(ds.Tables[0].Rows[0][11]);
                txtOrderBillDate.Date = Convert.ToString(ds.Tables[0].Rows[0][12]);
                txtPersonBill.Text = Convert.ToString(ds.Tables[0].Rows[0][13]);
                //strEnterBy = Convert.ToString(ds.Tables[0].Rows[0][13]);
                ShippingId = Convert.ToInt64(ds.Tables[0].Rows[0][14]);
                if (Convert.ToString(ds.Tables[0].Rows[0][15]).Trim().Length > 0)
                {
                    if (Convert.ToString(ds.Tables[0].Rows[0][16]).Trim().Length > 0)
                        lblChasis.Text = Convert.ToString(ds.Tables[0].Rows[0][15]) + "<br/>" + Convert.ToString(ds.Tables[0].Rows[0][16]);
                    else
                        lblChasis.Text = Convert.ToString(ds.Tables[0].Rows[0][15]);
                }
                else
                    lblChasis.Text = Convert.ToString(ds.Tables[0].Rows[0][16]);
                lblLastfreedate.Text = Convert.ToString(ds.Tables[0].Rows[0][17]);
                lblUpdatedOn.Text = Convert.ToString(ds.Tables[0].Rows[0][18]);
            }

            //strQuery = "select '<b>'+[nvar_ShippingLineName]+'</b> <br/>'+[nvar_Street]+'&nbsp;'+ [nvar_Suite]+'<br/>'+[nvar_City]+','+[nvar_State] +'&nbsp;'+ [nvar_Zip]+'<br/><a href='+ [nvar_WebsiteURL]+'>'+[nvar_WebsiteURL]+'</a>',[nvar_PierTermination] from [eTn_Shipping] where [bint_ShippingId]=" + ShippingId;
            //dt = DBClass.returnDataTable(strQuery);
            if (ds.Tables[1].Rows.Count > 0)
            {
                lblRoute.Text = Convert.ToString(ds.Tables[1].Rows[0][0]);
                lblPier.Text = Convert.ToString(ds.Tables[1].Rows[0][1]);
                String SL = (Convert.ToString(ds.Tables[1].Rows[0][0])).Substring(3, (Convert.ToString(ds.Tables[1].Rows[0][0])).IndexOf("</b>"));
                ViewState["ShippingLine"] = SL.Remove(SL.Length - 3);
                ViewState["Return"] = Convert.ToString(ds.Tables[1].Rows[0][1]);
            }
            if (ds.Tables[2].Rows.Count > 0)
            {
                StringBuilder tempStr = new StringBuilder();
                tempStr.Append(string.Empty);
                foreach (DataRow dr in ds.Tables[2].Rows)
                {
                    if (tempStr.ToString().Trim().Length != 0)
                        tempStr.Append("<br/>");
                    tempStr.Append(Convert.ToString(dr[0]).Trim() + "<br/>");
                }
                lblShippingContact.Text = tempStr.ToString().Trim();
            }

            lblPersong.Text = Convert.ToString(ds.Tables[3].Rows[0][0]);
            if (ds.Tables[4].Rows.Count > 0)
                lblUpdatedBy.Text = Convert.ToString(ds.Tables[4].Rows[0][0]);

            //strQuery = "select [nvar_Name]+'<br/>'+case when len(cast([num_WorkPhone]  as varchar(10)))=10 then substring(cast([num_WorkPhone]  as varchar(10)),1,3)+'-'+" +
            //          "substring(cast([num_WorkPhone]  as varchar(10)),4,3)+'-'+substring(cast([num_WorkPhone]  as varchar(10)),7,4) else '' end +'<br/>'+case when len(cast([num_Mobile]  as varchar(10)))=10 then substring(cast([num_Mobile]  as varchar(10)),1,3)+'-'+" +
            //          "substring(cast([num_Mobile]  as varchar(10)),4,3)+'-'+substring(cast([num_Mobile]  as varchar(10)),7,4) else '' end" +
            //          " from [eTn_ShippingContacts] where bint_ShippingId=" + ShippingId;
            //lblShippingContact.Text = Convert.ToString(DBClass.executeScalar(strQuery));          
        }
        if (ds != null) { ds.Dispose(); ds = null; }
        //SLine 2016 ---- The Below line was Commented
        //Under update it was Uncommented, to show Customer Name
        lblCustomer.Text = DBClass.executeScalar("SELECT C.nvar_CustomerName from [eTn_Load] L INNER JOIN [eTn_Customer] C on L.[bint_CustomerId]=C.[bint_CustomerId] WHERE L.[bint_LoadId]=" + ID).Trim();
    }

    private void SendSMSAndEmail()
    {
        string[] sendDetails = (Convert.ToString(ViewState["SMSNo"])).Split('~');
        StringBuilder smsMsg = new StringBuilder();

        smsMsg.AppendLine(ConfigurationSettings.AppSettings["SMSFrom"] + "^^^^");
        smsMsg.AppendLine("L: " + GetStringValue(Convert.ToString(ViewState["ID"])) + "^^^^");
        smsMsg.AppendLine("U: " + GetStringValue(Convert.ToString(ViewState["ContainerNo"])) + "^^^^");
        smsMsg.AppendLine("Tag#: " + GetStringValue(Convert.ToString(ViewState["Tag"])) + "^^^^");
        if (string.IsNullOrEmpty((Convert.ToString(ViewState["FromLoc"]).Trim())))
        {
            smsMsg.AppendLine("B: " + GetStringValue(lblBooking.Text.Trim()) + "^^^^");
            smsMsg.AppendLine("Loc: " + GetStringValue(txtRailBill.Text.Trim()) + "^^^^");
            smsMsg.AppendLine("Pick: " + GetStringValue(lblPickUp.Text.Trim()) + "^^^^");
        }
        string from = !string.IsNullOrEmpty((Convert.ToString(ViewState["FromLoc"]).Trim())) ? Convert.ToString(ViewState["FromLoc"]).Trim() : Convert.ToString(ViewState["Origin"]).Trim();
        string destination = !string.IsNullOrEmpty((Convert.ToString(ViewState["ToLoc"]).Trim())) ? Convert.ToString(ViewState["ToLoc"]).Trim() : Convert.ToString(ViewState["Destination"]).Trim();
        smsMsg.AppendLine("F: " + GetStringValue(from) + "^^^^");
        if (string.IsNullOrEmpty((Convert.ToString(ViewState["FromLoc"]).Trim())))
        {
            smsMsg.AppendLine("T: " + GetStringValue(Convert.ToString(ViewState["OrgPhoneNo"])) + "^^^^");
            smsMsg.AppendLine("Pick Dt: " + GetStringValue(Convert.ToString(ViewState["PickUpDate"])) + "^^^^");
        }
        smsMsg.AppendLine("D: " + GetStringValue(destination) + "^^^^");
        if (string.IsNullOrEmpty((Convert.ToString(ViewState["ToLoc"]).Trim())))
        {
            smsMsg.AppendLine("T: " + GetStringValue(Convert.ToString(ViewState["PhoneNo"])) + "^^^^");
            smsMsg.AppendLine("Dt: " + GetStringValue(Convert.ToString(ViewState["DelivaryDate"])) + "^^^^");
        }
        smsMsg.AppendLine("SL: " + GetStringValue(Convert.ToString(ViewState["ShippingLine"])) + "^^^^");
        smsMsg.AppendLine("RET: " + GetStringValue(Convert.ToString(ViewState["Return"])) + "^^^^");
        //dsDiscounts.Tables[1].Rows[0]["CONTACT_ID"]
        //SLine 2016 Enhancements ---- Adding PO# and APPno# to the SMS and EMail
        DataSet dsSMSDetails = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GETPOandApp", new object[] { Convert.ToInt64(ViewState["ID"]) });
        string strPO = dsSMSDetails.Tables[0].Rows[0][0].ToString();
        string strAPPno = dsSMSDetails.Tables[0].Rows[0][1].ToString();
        smsMsg.AppendLine("PO#: " + strPO + "^^^^");
        smsMsg.AppendLine("APPT#: " + strAPPno + "^^^^");
        //SLine 2016 Enhancements

        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        //SLine 2017 Enhancement for Office Location Ends

        bool isMobileNo = true;
        bool isMail = true;
        string emailMsg = smsMsg.ToString();

        if (chkSendSms.Checked) //sms checked and whatsapp not checked || sms checked and whatsapp checked 
        {
            smsMsg.Replace("^^^^", "");
            if (sendDetails.Length > 0 && !string.IsNullOrEmpty(sendDetails[0]) && sendDetails[0].Length >= 10)
            {
                string mobileNunber = ConfigurationSettings.AppSettings["CountryCode"] + sendDetails[0];
                //SLine 2017 Enhancement for Office Location
                string Status = CommonFunctions.SendSms(Int64.Parse(Convert.ToString(ViewState["ID"])), Convert.ToString(ViewState["DriverName"]), mobileNunber, "Driver", smsMsg.ToString(), Int64.Parse(Convert.ToString(Session["UserLoginId"])), officeLocationId, Chkwhatsapp.Checked);
                if (!string.IsNullOrEmpty(Status))
                {
                    Response.Write("<script>alert('" + Status + "')</script>");
                }
            }
            else
            {
                isMobileNo = false;
            }
        }
        if (!chkSendSms.Checked && Chkwhatsapp.Checked)
        {
            smsMsg.Replace("^^^^", "");
            if (sendDetails.Length > 0 && !string.IsNullOrEmpty(sendDetails[0]) && sendDetails[0].Length >= 10)
            {
                string mobileNunber = ConfigurationSettings.AppSettings["CountryCode"] + sendDetails[0];
                //SLine 2017 Enhancement for Office Location
                string Status = CommonFunctions.SendWhatsAppSms(Int64.Parse(Convert.ToString(ViewState["ID"])), Convert.ToString(ViewState["DriverName"]), mobileNunber, "Driver", smsMsg.ToString(), Int64.Parse(Convert.ToString(Session["UserLoginId"])), officeLocationId, true);
                if (!string.IsNullOrEmpty(Status) && Status.Contains("Error"))
                {
                    Response.Write("<script>alert('" + Status + "')</script>");
                }
            }
            else
            {
                isMobileNo = false;
            }
        }
        if (chkSendMail.Checked)
        {
            emailMsg = emailMsg.Replace("^^^^", "<br />");
            if (sendDetails.Length > 1 && !string.IsNullOrEmpty(sendDetails[1]) && sendDetails[1].Length >= 10)
            {
                CommonFunctions.SendEmail(GetFromXML.FromLoadPlanner, sendDetails[1], "", "", ("Assign Load : " + ViewState["ID"]), emailMsg.ToString(), null, null);
            }
            else
            {
                isMail = false;
            }
        }
        if (!isMobileNo && !isMail)
        {
            Response.Write("<script>alert('Driver does not have Mobile Number and Email.')</script>");
        }
        else if (!isMobileNo)
        {
            Response.Write("<script>alert('Driver does not have Mobile Number.')</script>");
        }
        else if (!isMail)
        {
            Response.Write("<script>alert('Driver does not have Email.')</script>");
        }
    }

    private string GetStringValue(string str)
    {
        if (str == null || string.IsNullOrEmpty(str.Trim()))
            str = "-NA-";
        return str;
    }

    private void MakeNewAgain()
    {
        if (Grid2.RowCount == 0 && rabDriver.Checked)
        {
            SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_MakeNewAgain", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(Session["UserLoginId"]) });
            if (Session["Role"].ToString() == "Manager")
            {
                btnAssignToLoadPlaner.Visible = true;
                btnSaveCarrier.Visible = false;
            }
            else
            {
                btnAssignToLoadPlaner.Visible = false;
                btnSaveCarrier.Visible = true;
            }
            btnConformAssign.Visible = false;
        }

    }

    protected void btnConformAssign_Click(object sender, EventArgs e)
    {
        try
        {
            SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_MakeToAssignState", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(Session["UserLoginId"]) });
            //SendSMS();
            btnConformAssign.Visible = false;
            btnSaveCarrier.Visible = true;
        }
        catch
        {
            CheckBackPage();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        //The below flag prevent the user from changing the btn visibility behaviour till the driver details are updated
        if (ViewState["IsDriverValid"].ToString() == "1" || radCarrier.Checked == true || Session["Role"].ToString() == "Manager")
        {
            try
            {
                if (Page.IsPostBack)
                {
                    lblErrCarrier.Visible = false;
                    lblErrDriver.Visible = false;
                    txtOrderBillDate.DisplayError = false;
                    if (rabDriver.Checked)
                    {
                        if (ddlDrivers.SelectedIndex == 0)
                        {
                            lblErrDriver.Visible = true;
                            return;
                        }
                    }
                    if (radCarrier.Checked)
                    {
                        if (ddlCarrier.SelectedIndex == 0)
                        {
                            lblErrCarrier.Visible = true;
                            return;
                        }
                    }
                    if (!txtOrderBillDate.IsValidDate)
                        return;
                    if (((Button)sender).ID == "btnSaveCarrier")
                    {
                        SaveDetails(true);
                    }
                    else
                    {
                        SaveDetails(false);
                        btnConformAssign.Visible = true;
                        btnAssignToLoadPlaner.Visible = false;
                    }
                    tpAssignDriver.Clear();
                    tpCustAssigned.Clear();
                }
            }
            catch (Exception ex )
            {
                CheckBackPage();
            }
        }
        else
        {
            Response.Write("<script>alert('Please update the Driver details and Continue')</script>");
            trForUpdateDriverDetails.Style.Clear();
            trForUpdateDriverDetails.Style.Add(HtmlTextWriterStyle.Display, "");
        }


    }
    private void SaveDetails(bool isAssined)
    {


        #region SaveDetails
        lblErrDriver.Visible = false;
        lblErrCarrier.Visible = false;
        object[] objParams = null;
        bool result = false;
        if (ViewState["Mode"] != null)
        {
            if (ViewState["ID"] != null)
            {
                if (Convert.ToString(ViewState["Mode"]).StartsWith("New", true, System.Globalization.CultureInfo.CurrentCulture))
                {
                    if (radCarrier.Checked)
                    {
                        if (ViewState["CResult"] != null)
                        {
                            if (Convert.ToInt32(ViewState["CResult"]) == -1)
                            {
                                result = true;
                            }
                        }
                        if (!result)
                        {
                            objParams = new object[] { Convert.ToInt64(ddlCarrier.SelectedValue),
                                Convert.ToInt64(ViewState["ID"]), txtInvoice.Text.Trim(), txtRailBill.Text.Trim(),
                                CommonFunctions.CheckDateTimeNull(txtOrderBillDate.Date), txtPersonBill.Text.Trim(),Convert.ToDateTime(dtpCustAssigned.Date+" "+tpCustAssigned.Time),Convert.ToInt64(Session["UserLoginId"]),isAssined?"Assigned":"Load Planner" };
                            if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Add_AssignCarrier", objParams) > 0)
                            {
                                objParams = null;
                                lnkUploadStatus.Visible = true;
                                CheckBackPage();
                                Response.Redirect("~/Manager/DashBoard.aspx");
                            }
                        }
                        else
                        {
                            Response.Write("<script>alert('Carrier insurence date has been expired.')</script>");
                            ddlCarrier.SelectedIndex = 0;
                            return;
                        }
                    }
                    else if (rabDriver.Checked)
                    {
                        if (ValidateDriverDetails())
                        {
                            tpAssignDriver.Time = DateTime.Now.ToShortTimeString();
                            return;
                        }
                        if (ViewState["DResult"] != null)
                        {
                            if (Convert.ToInt32(ViewState["DResult"]) == -1)
                            {
                                result = true;
                            }
                        }
                        if (!result)
                        {
                            objParams = new object[] { Convert.ToInt64(ddlDrivers.SelectedValue),
                            Convert.ToInt64(ViewState["ID"]), txtFrom.Text.Trim(), txtFromCity.Text.Trim(),
                            txtFromstate.Text.Trim(), txtTO.Text.Trim(), txtToCity.Text.Trim(), txtState.Text.Trim(),
                            txtTag.Text.Trim(), txtRailBill.Text.Trim(), CommonFunctions.CheckDateTimeNull(txtOrderBillDate.Date),
                            txtPersonBill.Text.Trim(),Convert.ToDateTime(dtpAssignDriver.Date+" "+tpAssignDriver.Time),Convert.ToInt64(Session["UserLoginId"]),isAssined?"Assigned":"Load Planner" };
                            if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Add_AssignDrivers", objParams) > 0)
                            {
                                ViewState["DriverName"] = ViewState["SMSNo"] = ViewState["ToLoc"] = ViewState["FromLoc"] = ViewState["Tag"] = string.Empty;
                                ViewState["DriverName"] = ddlDrivers.SelectedItem.Text;
                                if (!string.IsNullOrEmpty(txtFrom.Text.Trim()))
                                //|| !string.IsNullOrEmpty(txtFromCity.Text.Trim())|| !string.IsNullOrEmpty(txtFromstate.Text.Trim()))
                                {
                                    string fromLoc = (!string.IsNullOrEmpty(txtFrom.Text.Trim()) ? txtFrom.Text.Trim() + "," : string.Empty);
                                    //+ (!string.IsNullOrEmpty(txtFromCity.Text.Trim()) ? txtFromCity.Text.Trim() + "," : string.Empty) + (!string.IsNullOrEmpty(txtFromstate.Text.Trim()) ? txtFromstate.Text.Trim() + "," : string.Empty);
                                    ViewState["FromLoc"] = fromLoc.Substring(0, fromLoc.Length - 1);
                                }
                                if (!string.IsNullOrEmpty(txtTO.Text.Trim()) || !string.IsNullOrEmpty(txtToCity.Text.Trim()) || !string.IsNullOrEmpty(txtState.Text.Trim()))
                                {
                                    string toLoc = (!string.IsNullOrEmpty(txtTO.Text.Trim()) ? txtTO.Text.Trim() + "," : string.Empty) + (!string.IsNullOrEmpty(txtToCity.Text.Trim()) ? txtToCity.Text.Trim() + "," : string.Empty) + (!string.IsNullOrEmpty(txtState.Text.Trim()) ? txtState.Text.Trim() + "," : string.Empty);
                                    ViewState["ToLoc"] = toLoc.Substring(0, toLoc.Length - 1);
                                }
                                ViewState["Tag"] = txtTag.Text.Trim();
                                ViewState["SMSNo"] = Convert.ToString(SqlHelper.ExecuteScalar(ConfigurationSettings.AppSettings["conString"], "SP_GetEmployeeMobileNumber", new object[] { Convert.ToInt64(ddlDrivers.SelectedValue) }));
                                SendSMSAndEmail();
                                objParams = null;
                                ViewState["Mode"] = "Edit";
                                Grid2.BindGrid(0);
                                ViewState["DriverName"] = ViewState["SMSNo"] = ViewState["ToLoc"] = ViewState["FromLoc"] = ViewState["Tag"] = string.Empty;
                                if (Grid2.GridControl.Items.Count > 0)
                                {
                                    int index = Grid2.GridControl.Items.Count - 1;
                                    string fromLoc = Grid2.GridControl.Items[index].Cells[4].Text.Replace("<br/>", ",").Replace("&nbsp;", " ").Trim();
                                    if (!string.IsNullOrEmpty(fromLoc))
                                        fromLoc = fromLoc.Split(',')[0];
                                    string ToLoc = Grid2.GridControl.Items[index].Cells[5].Text.Replace("<br/>", ",").Replace("&nbsp;", " ").Trim();
                                    if (fromLoc.Length > 0)
                                        ViewState["FromLoc"] = fromLoc;
                                    if (ToLoc.Length > 0)
                                        ViewState["ToLoc"] = ToLoc.Substring(ToLoc.Length - 1) == "," ? ToLoc.Substring(0, ToLoc.Length - 1) : ToLoc;

                                    string tempText = Grid2.GridControl.Items[index].Cells[2].Text.Replace("<br/>", ",").Replace("&nbsp;", " ").Trim();
                                    if (tempText.Contains("Mobile:"))
                                    {
                                        string[] str;
                                        str = tempText.Split(new string[] { "Mobile:" }, StringSplitOptions.None);
                                        ViewState["SMSNo"] = str[1].Trim().Replace("-", "");
                                        tempText = str[0].Trim();
                                    }
                                    ViewState["DriverName"] = tempText.Substring(tempText.Length - 1) == "," ? tempText.Substring(0, tempText.Length - 1) : tempText;
                                }
                                lnkUploadStatus.Visible = true;
                                ddlDrivers.SelectedIndex = 0;
                                ClearDriverDeatils();
                                //ddlDriverAssign.Items.Insert(0, new ListItem("Select", "Select"));
                                // ddlDriverAssign.Items.Insert(2, new ListItem("Edit", "Edit"));
                            }
                        }
                        else
                        {
                            Response.Write("<script>alert('Equipment insurence date has been expired.')</script>");
                            ddlDrivers.SelectedIndex = 0;
                            return;
                        }
                    }
                }
                else if (Convert.ToString(ViewState["Mode"]).StartsWith("Edit", true, System.Globalization.CultureInfo.CurrentCulture))
                {
                    if (radCarrier.Checked)
                    {
                        if (ViewState["CResult"] != null)
                        {
                            if (Convert.ToInt32(ViewState["CResult"]) == -1)
                            {
                                result = true;
                            }
                        }
                        if (!result)
                        {
                            objParams = new object[] { Convert.ToInt64(ViewState["ID"]),Convert.ToInt64(ddlCarrier.SelectedValue), txtInvoice.Text.Trim(),
                            0,"", "", "",  "",  "",  "", "", txtRailBill.Text.Trim(), CommonFunctions.CheckDateTimeNull(txtOrderBillDate.Date),
                            txtPersonBill.Text.Trim(),"carrier",Convert.ToDateTime(dtpCustAssigned.Date+" "+tpCustAssigned.Time),Convert.ToInt64(Session["UserLoginId"]),isAssined?"Assigned":"Load Planner" };
                            if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Update_AssignLoad", objParams) > 0)
                            {
                                ViewState["Result"] = null;
                                objParams = null;
                                lnkUploadStatus.Visible = true;
                                CheckBackPage();
                                Response.Redirect("~/Manager/DashBoard.aspx");
                            }
                        }
                        else
                        {
                            Response.Write("<script>alert('Carrier insurence date has been expired.')</script>");
                            ddlCarrier.SelectedIndex = 0;
                            return;
                        }

                    }
                    else if (rabDriver.Checked)
                    {
                        if (ValidateDriverDetails())
                        {
                            tpAssignDriver.Time = DateTime.Now.ToShortTimeString();
                            return;
                        }
                        if (ViewState["DResult"] != null)
                        {
                            if (Convert.ToInt32(ViewState["DResult"]) == -1)
                            {
                                result = true;
                            }
                        }
                        if (!result)
                        {
                            //if (ddlDriverAssign.SelectedItem.Text == "Edit")
                            //{
                            //    objParams = new object[] { Convert.ToInt64(ViewState["ID"]),0, "",
                            //    Convert.ToInt64(ddlDrivers.SelectedValue),txtFrom.Text.Trim(), txtFromCity.Text.Trim(), 
                            //    txtFromstate.Text.Trim(), txtTO.Text.Trim(), txtToCity.Text.Trim(), txtState.Text.Trim(), 
                            //    txtTag.Text.Trim(), txtRailBill.Text.Trim(), CommonFunctions.CheckDateTimeNull(txtOrderBillDate.Date),
                            //    txtPersonBill.Text.Trim(),"driver",Convert.ToDateTime(dtpAssignDriver.Date+" "+tpAssignDriver.Time),Convert.ToInt64(Session["UserLoginId"]) };
                            //    if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Update_AssignLoad", objParams) > 0)
                            //    {
                            //        objParams = null;
                            //        Grid2.BindGrid(0);
                            //        lnkUploadStatus.Enabled = true;
                            //        ddlDrivers.SelectedIndex = 0;
                            //        ClearDriverDeatils();
                            //    }
                            //}
                            //else if (ddlDriverAssign.SelectedItem.Text == "Assign")
                            //{
                            objParams = new object[] { Convert.ToInt64(ddlDrivers.SelectedValue),
                                Convert.ToInt64(ViewState["ID"]), txtFrom.Text.Trim(), txtFromCity.Text.Trim(),
                                txtFromstate.Text.Trim(), txtTO.Text.Trim(), txtToCity.Text.Trim(), txtState.Text.Trim(),
                                txtTag.Text.Trim(), txtRailBill.Text.Trim(), CommonFunctions.CheckDateTimeNull(txtOrderBillDate.Date),
                                txtPersonBill.Text.Trim(),Convert.ToDateTime(dtpAssignDriver.Date+" "+tpAssignDriver.Time),Convert.ToInt64(Session["UserLoginId"]),isAssined?"Assigned":"Load Planner" };
                            if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_Add_AssignDrivers", objParams) > 0)
                            {
                                ViewState["DriverName"] = ViewState["SMSNo"] = ViewState["ToLoc"] = ViewState["FromLoc"] = ViewState["Tag"] = string.Empty;
                                ViewState["DriverName"] = ddlDrivers.SelectedItem.Text;
                                if (!string.IsNullOrEmpty(txtFrom.Text.Trim()))
                                //|| !string.IsNullOrEmpty(txtFromCity.Text.Trim())|| !string.IsNullOrEmpty(txtFromstate.Text.Trim()))
                                {
                                    string fromLoc = (!string.IsNullOrEmpty(txtFrom.Text.Trim()) ? txtFrom.Text.Trim() + "," : string.Empty);
                                    //+ (!string.IsNullOrEmpty(txtFromCity.Text.Trim()) ? txtFromCity.Text.Trim() + "," : string.Empty) + (!string.IsNullOrEmpty(txtFromstate.Text.Trim()) ? txtFromstate.Text.Trim() + "," : string.Empty);
                                    ViewState["FromLoc"] = fromLoc.Substring(0, fromLoc.Length - 1);
                                }
                                if (!string.IsNullOrEmpty(txtTO.Text.Trim()) || !string.IsNullOrEmpty(txtToCity.Text.Trim()) || !string.IsNullOrEmpty(txtState.Text.Trim()))
                                {
                                    string toLoc = (!string.IsNullOrEmpty(txtTO.Text.Trim()) ? txtTO.Text.Trim() + "," : string.Empty) + (!string.IsNullOrEmpty(txtToCity.Text.Trim()) ? txtToCity.Text.Trim() + "," : string.Empty) + (!string.IsNullOrEmpty(txtState.Text.Trim()) ? txtState.Text.Trim() + "," : string.Empty);
                                    ViewState["ToLoc"] = toLoc.Substring(0, toLoc.Length - 1);
                                }
                                ViewState["Tag"] = txtTag.Text.Trim();
                                ViewState["SMSNo"] = Convert.ToString(SqlHelper.ExecuteScalar(ConfigurationSettings.AppSettings["conString"], "SP_GetEmployeeMobileNumber", new object[] { Convert.ToInt64(ddlDrivers.SelectedValue) }));
                                SendSMSAndEmail();
                                objParams = null;
                                ViewState["Mode"] = "Edit";
                                Grid2.BindGrid(0);
                                ViewState["DriverName"] = ViewState["SMSNo"] = ViewState["ToLoc"] = ViewState["FromLoc"] = string.Empty;
                                if (Grid2.GridControl.Items.Count > 0)
                                {
                                    int index = Grid2.GridControl.Items.Count - 1;
                                    string fromLoc = Grid2.GridControl.Items[index].Cells[4].Text.Replace("<br/>", ",").Replace("&nbsp;", " ").Trim();
                                    if (!string.IsNullOrEmpty(fromLoc))
                                        fromLoc = fromLoc.Split(',')[0];
                                    string ToLoc = Grid2.GridControl.Items[index].Cells[5].Text.Replace("<br/>", ",").Replace("&nbsp;", " ").Trim();
                                    if (fromLoc.Length > 0)
                                        ViewState["FromLoc"] = fromLoc;
                                    if (ToLoc.Length > 0)
                                        ViewState["ToLoc"] = ToLoc.Substring(ToLoc.Length - 1) == "," ? ToLoc.Substring(0, ToLoc.Length - 1) : ToLoc;

                                    string tempText = Grid2.GridControl.Items[index].Cells[2].Text.Replace("<br/>", ",").Replace("&nbsp;", " ").Trim();
                                    if (tempText.Contains("Mobile:"))
                                    {
                                        string[] str;
                                        str = tempText.Split(new string[] { "Mobile:" }, StringSplitOptions.None);
                                        ViewState["SMSNo"] = str[1].Trim().Replace("-", "");
                                        tempText = str[0].Trim();
                                    }
                                    ViewState["DriverName"] = tempText.Substring(tempText.Length - 1) == "," ? tempText.Substring(0, tempText.Length - 1) : tempText;
                                }
                                lnkUploadStatus.Visible = true;
                                ddlDrivers.SelectedIndex = 0;
                                ClearDriverDeatils();
                            }
                            //}
                        }
                        else
                        {
                            Response.Write("<script>alert('Equipment insurence date has been expired.')</script>");
                            ddlDrivers.SelectedIndex = 0;
                            return;
                        }
                    }
                }
            }
        }
        #endregion



    }

    protected void lnkLoadCharge_Click(object sender, EventArgs e)
    {
        if (ViewState["ID"] != null)
        {
            Session["BackPage4"] = "~/Manager/Dispatch.aspx?" + Convert.ToString(ViewState["ID"]);
            Response.Redirect("~/Manager/ShowReceivables.aspx?" + Convert.ToString(ViewState["ID"]));
        }
    }
    protected void btnCancelCarrier_Click(object sender, EventArgs e)
    {
        CheckBackPage();
        Response.Redirect("~/Manager/DashBoard.aspx");
    }

    private bool ValidateDriverDetails()
    {
        bool bReturn = false; int Result = 0;
        bool ishaxmet = Convert.ToBoolean(ViewState["LoadIsHazmatCertified"]);
        object[] objParams = new object[] { Convert.ToInt64(ddlDrivers.SelectedValue), (ishaxmet ? 1 : 0), Result };
        // Checking is the hazmat expited or not it the load is hazmat
        System.Data.SqlClient.SqlParameter[] objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_CheckDriverExpireDate", objParams);
        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_CheckDriverExpireDate", objsqlparams);
        ViewState["DResult"] = Convert.ToInt32(objsqlparams[objsqlparams.Length - 1].Value);
        int iRes = Convert.ToInt32(ViewState["DResult"]);
        if (iRes < 0)
        {
            string strMess = "";
            ViewState["ExpiredItem1"] = "";
            ViewState["ExpiredItem2"] = "";
            bReturn = true;
            switch (iRes)
            {
                case -1:
                    strMess = "This Driver Equipment insurance date has been expired.";
                    break;
                //case -2:
                //    strMess = "This Driver Tag date has been expired.";
                //    break;
                case -3:
                    strMess = "Equipment has not been assigned to this Driver.";
                    break;
                case -4:
                    strMess = "This Driver work permit has been expired.";
                    break;
                case -5:
                    strMess = "This Driver License has been expired.";
                    ViewState["ExpiredItem1"] = "License";
                    break;
                case -6:
                    strMess = "This Medical Card Expire has been expired.";
                    ViewState["ExpiredItem2"] = "MedicalCard";
                    break;
                case -7:
                    strMess = "This Driver Hazmat certificate has been expired.";
                    break;
            }
            Response.Write("<script>alert('" + strMess + "')</script>");
            //ddlDrivers.SelectedIndex = 0;
            //ddlDriverAssign.SelectedIndex = 0;
        }
        return bReturn;
    }
    protected void FillDriverDetails()
    {
        if (ddlDrivers.SelectedIndex > 0)
        {
            DataSet dsDriver = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetAssignedDriverDetails", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(ddlDrivers.SelectedValue) });
            if (dsDriver.Tables.Count > 0)
            {
                if (dsDriver.Tables[0].Rows.Count > 0)
                {
                    txtFrom.Text = Convert.ToString(dsDriver.Tables[0].Rows[0][0]);
                    txtFromCity.Text = Convert.ToString(dsDriver.Tables[0].Rows[0][1]);
                    txtFromstate.Text = Convert.ToString(dsDriver.Tables[0].Rows[0][2]);
                    txtTO.Text = Convert.ToString(dsDriver.Tables[0].Rows[0][3]);
                    txtToCity.Text = Convert.ToString(dsDriver.Tables[0].Rows[0][4]);
                    txtState.Text = Convert.ToString(dsDriver.Tables[0].Rows[0][5]);
                    txtTag.Text = Convert.ToString(dsDriver.Tables[0].Rows[0][6]);
                    try
                    {
                        DateTime dt = Convert.ToDateTime(dsDriver.Tables[0].Rows[0][7]);
                        dtpAssignDriver.SetDate(dt);
                        tpAssignDriver.Time = dt.ToShortTimeString();
                    }
                    catch
                    { }
                }
                else
                    ClearDriverDeatils();
            }
            else
                ClearDriverDeatils();
        }
        else
            ClearDriverDeatils();
    }
    protected void ClearDriverDeatils()
    {
        txtFrom.Text = "";
        txtFromCity.Text = "";
        txtFromstate.Text = "";
        txtTO.Text = "";
        txtToCity.Text = "";
        txtState.Text = "";
        txtTag.Text = "";
    }
    //protected void lnkLoadDetails_Click(object sender, EventArgs e)
    //{
    //    Response.Write("<script>window.open('LoadDetails.aspx?" + Convert.ToInt64(ViewState["ID"]) + "','Load Details')</script>");
    //}
    protected void ddlCarrier_SelectedIndexChanged(object sender, EventArgs e)
    {
        int Result = 0;
        if (ddlCarrier.SelectedIndex > 0)
        {
            object[] objParams = new object[] { Convert.ToInt64(ddlCarrier.SelectedValue), Result };
            System.Data.SqlClient.SqlParameter[] objsqlparams = SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_CheckCarrierExpireDate", objParams);
            SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_CheckCarrierExpireDate", objsqlparams);
            ViewState["CResult"] = Convert.ToInt32(objsqlparams[objsqlparams.Length - 1].Value);
            if (Convert.ToInt32(ViewState["CResult"]) == -1)
            {
                Response.Write("<script>alert('This Carrier insurence date has been expired.')</script>");
                ddlCarrier.SelectedIndex = 0;
                return;
            }
        }
    }
    protected void lnkNew_Click(object sender, EventArgs e)
    {
        if (ViewState["ID"] != null)
        {
            Response.Redirect("~/Manager/NewEmployee.aspx");
        }
    }

    protected void lnkDriverCharges_Click(object sender, EventArgs e)
    {
        if (ViewState["ID"] != null)
        {
            Session["BackPage4"] = "~/Manager/Dispatch.aspx?" + Convert.ToString(ViewState["ID"]);
            Response.Redirect("~/Manager/ShowPayables.aspx?" + Convert.ToString(ViewState["ID"]));
        }
    }
    protected void lnkUpload_Click(object sender, EventArgs e)
    {
        if (ViewState["ID"] != null)
        {
            lnkUpload.PostBackUrl = "UpLoadDocuments.aspx?" + Convert.ToString(ViewState["ID"]).Trim();
        }
    }
    protected void lnkUploadStatus_Click(object sender, EventArgs e)
    {
        if (ViewState["ID"] != null)
        {
            Response.Redirect("UpdateLoadStatus.aspx?" + Convert.ToInt64(ViewState["ID"]));
        }
    }
    protected void btnUnassignCarrier_Click(object sender, EventArgs e)
    {
        SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], "SP_MakeNewAgain", new object[] { Convert.ToInt64(ViewState["ID"]), Convert.ToInt64(Session["UserLoginId"]) });
        txtInvoice.Text = "";
        ddlCarrier.SelectedIndex = 0;
        if (Session["Role"].ToString() == "Manager")
        {
            btnAssignToLoadPlaner.Visible = true;
            btnSaveCarrier.Visible = false;
        }
        else
        {
            btnAssignToLoadPlaner.Visible = false;
            btnSaveCarrier.Visible = true;
        }
        btnConformAssign.Visible = false;

    }
    private void CheckBackPage()
    {
        if (Session["BackPage3"] != null)
        {
            if (Convert.ToString(Session["BackPage3"]).Trim().Length > 0)
            {
                string strTemp = Convert.ToString(Session["BackPage3"]).Trim();
                Session["BackPage3"] = null;
                Response.Redirect(strTemp);
            }
        }
    }

    //SLIne enhancement in 2021
    protected void ddlTeam_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ddlDriverAssign.SelectedIndex = 0;
        ddlDrivers.Items.Clear();
        // ddlDrivers.Items.Add("--Select State--");
        bool ishazmit = Convert.ToBoolean(ViewState["LoadIsHazmatCertified"]);
        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetActiveDriversWithPaymentsandHazmat", new object[] { GetFirstDayInWeek(), DateTime.Now, (ishazmit ? 1 : 0), officeLocationId, ddlteam.SelectedItem.Value == "All" ? "0" : ddlteam.SelectedItem.Value });
        if (ds.Tables.Count > 0)
        {
            ddlDrivers.DataSource = ds.Tables[0];
            ddlDrivers.DataTextField = "Name";
            ddlDrivers.DataValueField = "Id";
            ddlDrivers.DataBind();

            dtpAssignDriver.SetDate(DateTime.Now);
            tpAssignDriver.Time = DateTime.Now.ToShortTimeString();
        }
        ddlDrivers.Items.Insert(0, "--Select Driver--");
        if (ds != null) { ds.Dispose(); ds = null; }

    }

    //Old Code
    //protected void ddlDrivers_SelectedIndexChanged1(object sender, EventArgs e)
    //{
    //    //ddlDriverAssign.SelectedIndex = 0;
    //}

    //SLine 2016 Enhancements for Providing Dispatcher provision to edit Driver details


    protected void ddlDrivers_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlDrivers.SelectedIndex > 0)
        {
            if (Session["Role"].ToString().Equals("Dispatcher"))
            {
                object[] param = new object[] { Convert.ToInt64(ddlDrivers.SelectedValue) };
                DataSet ds_DriverDetails = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetDriverLicenceAndMedExpiry_ForDispatcher", param);

                if (ds_DriverDetails.Tables.Count > 0 && Convert.ToInt32(ds_DriverDetails.Tables[0].Rows[0]["Flag"]) != 4)
                {
                    trForUpdateDriverDetails.Style.Clear();
                    trForUpdateDriverDetails.Style.Add(HtmlTextWriterStyle.Display, "");
                    FillDriverDetailsToUpdate(ds_DriverDetails);
                    ViewState["IsDriverValid"] = "0";
                }

                else
                {
                    ClearControlsDispatcher();
                    ViewState["IsDriverValid"] = "1";
                }
            }
        }
        else if (ddlDrivers.SelectedIndex == 0)
        {
            ClearControlsDispatcher();
        }


    }

    private void FillDriverDetailsToUpdate(DataSet ds)
    {
        if (ds != null)
        {
            if (Convert.ToInt32(ds.Tables[0].Rows[0]["Flag"]) != 4)
            {
                int strCondition = Convert.ToInt32(ds.Tables[0].Rows[0]["Flag"]);
                string strAlertMessage = "";
                bool boolMsgFlag = true;
                switch (strCondition)
                {
                    //Both
                    case 1:
                        //Showing both the elements as both are expired
                        trLicenceExp.Style.Clear();
                        trLicenceExp.Style.Add(HtmlTextWriterStyle.Display, "");
                        trMedExp.Style.Clear();
                        trMedExp.Style.Add(HtmlTextWriterStyle.Display, "");

                        txtLicense.Text = Convert.ToString(ds.Tables[0].Rows[0]["Licence"]);
                        //[MedicalExpireDate]
                        //dtpLicenseExpiry.SelectedDate = DateTime.Parse("12/30/2020"); //DateTime.Parse(ds.Tables[0].Rows[0]["LicenseExpireDate"].ToString());
                        //dtpMedicalCard.SelectedDate = DateTime.Parse("11/28/2030"); //DateTime.Parse(ds.Tables[0].Rows[0]["date_MedicalExpireDate"].ToString());

                        dtpLicenseExpiry.SelectedDate = DateTime.Parse(FormatDate(ds.Tables[0].Rows[0]["LicenseExpireDate"].ToString()));
                        dtpMedicalCard.SelectedDate = DateTime.Parse(FormatDate(ds.Tables[0].Rows[0]["MedicalExpireDate"].ToString()));

                        //Set the Alert message
                        strAlertMessage = "Drivers Licence and Medical Card Expired, Please update below.";
                        break;

                    //"OnlyLicence"
                    case 2:
                        //Showing the Licence elements only
                        trLicenceExp.Style.Clear();
                        trLicenceExp.Style.Add(HtmlTextWriterStyle.Display, "");
                        //filling the data
                        txtLicense.Text = Convert.ToString(ds.Tables[0].Rows[0]["Licence"]);
                        dtpLicenseExpiry.SelectedDate = DateTime.Parse(FormatDate(ds.Tables[0].Rows[0]["LicenseExpireDate"].ToString()));

                        //hiding the med licence as it is not expired
                        trMedExp.Style.Clear();
                        trMedExp.Style.Add(HtmlTextWriterStyle.Display, "none");
                        //Also assiging the Med Original value, to keep it AS IS
                        dtpMedicalCard.SelectedDate = DateTime.Parse(FormatDate(ds.Tables[0].Rows[0]["MedicalExpireDate"].ToString()));

                        //Set the Alert message
                        strAlertMessage = "Drivers Licence Expired, Please update below.";
                        break;

                    //"OnlyMed"
                    case 3:
                        //Showing the Med licence elements only
                        trMedExp.Style.Clear();
                        trMedExp.Style.Add(HtmlTextWriterStyle.Display, "");
                        dtpMedicalCard.SelectedDate = DateTime.Parse(FormatDate(ds.Tables[0].Rows[0]["MedicalExpireDate"].ToString()));

                        //hiding the med licence as it is not expired
                        trLicenceExp.Style.Clear();
                        trLicenceExp.Style.Add(HtmlTextWriterStyle.Display, "none");
                        txtLicense.Text = Convert.ToString(ds.Tables[0].Rows[0]["Licence"]);
                        dtpLicenseExpiry.SelectedDate = DateTime.Parse(FormatDate(ds.Tables[0].Rows[0]["LicenseExpireDate"].ToString()));

                        //Set the Alert message
                        strAlertMessage = "Medical Card Expired, Please update below."; break;

                    //"NoExp"
                    case 4:
                        boolMsgFlag = false;
                        ClearControlsDispatcher();
                        break;
                }

                if (boolMsgFlag)
                {
                    Response.Write("<script>alert('" + strAlertMessage + "')</script>");
                }
            }
        }
    }

    private string FormatDate(string GetDate)
    {

        string[] s = GetDate.Split(' ');
        string[] ss = s[0].Split('/');

        string strSetDate = ss[1] + "/" + ss[0] + "/" + ss[2];

        return strSetDate;
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        //this will validate the user from selecting any value less than/back dates
        if (dtpLicenseExpiry.SelectedDate > DateTime.Now.Date && dtpMedicalCard.SelectedDate > DateTime.Now.Date)
        {
            object[] objParams = new object[] { CommonFunctions.CheckDateTimeNull(Convert.ToString(dtpLicenseExpiry.SelectedDate)), CommonFunctions.CheckDateTimeNull(Convert.ToString(dtpMedicalCard.SelectedDate)), Convert.ToInt64(ddlDrivers.SelectedValue) };

            if (SqlHelper.ExecuteNonQuery(ConfigurationSettings.AppSettings["conString"], CommandType.StoredProcedure, "SP_Update_DriverDetails_FromDispatcher", SqlHelper.PrepareSqlParamsFromSP(ConfigurationSettings.AppSettings["conString"], "SP_Update_DriverDetails_FromDispatcher", objParams)) > 0)
            {
                ViewState["IsDriverValid"] = "1";
                ClearControlsDispatcher();
            }

            if (ValidateDriverDetails() && ViewState["ExpiredItem1"].ToString() != "" && ViewState["ExpiredItem2"].ToString() != "")
            {
                trForUpdateDriverDetails.Style.Clear();
                trForUpdateDriverDetails.Style.Add(HtmlTextWriterStyle.Display, "");
                ViewState["IsDriverValid"] = "0";
            }
        }
        else
        {
            if (dtpLicenseExpiry.SelectedDate <= DateTime.Now.Date)
                Response.Write("<script>alert('Please select correct values for License Expiry')</script>");
            else if (dtpMedicalCard.SelectedDate <= DateTime.Now.Date)
                Response.Write("<script>alert('Please select correct values for Medical Card')</script>");
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ClearControlsDispatcher();
    }
    private void ClearControlsDispatcher()
    {
        trForUpdateDriverDetails.Style.Clear();
        trForUpdateDriverDetails.Style.Add(HtmlTextWriterStyle.Display, "none");
        txtLicense.Text = "";
    }

    private void FillTeams()
    {
        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetTeamNames");
        //SLine 2017 Enhancement for Office Location Ends
        if (ds.Tables.Count > 0)
        {
            ddlteam.DataSource = ds.Tables[0];
            ddlteam.DataTextField = "nvar_TeamName";
            ddlteam.DataValueField = "bint_TeamId";
            ddlteam.DataBind();
        }
        ddlteam.Items.Insert(0, "All");
        ddlteam.SelectedIndex = 0;
        if (ds != null) { ds.Dispose(); ds = null; }
    }

    //SLine 2016 Enhancements for Providing Dispatcher provision to edit Driver details
}
