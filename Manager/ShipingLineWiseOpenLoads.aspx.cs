using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class ShipingLineWiseOpenLoads : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillLists();
            if (Request.QueryString.Count > 0)
            {
                if(Convert.ToString(Request.QueryString["Type"]) == "1")
                {
                    Session["TopHeaderText"] = "Shipping Line Wise Open Loads";
                }
                else if (Convert.ToString(Request.QueryString["Type"]) == "2")
                {
                    Session["TopHeaderText"] = "Shipping Line Wise Loads";
                }
            }
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
            Session["SerchCriteria"] = null;
            Session["ReportParameters"] = null;
            Session["ReportName"] = null;
            Session["ShipingLineName"] = ddlShipingLines.SelectedItem.Text;
            Session["ReportParameters"] = ddlShipingLines.SelectedValue + "~~^^^^~~" + dtpFromDate.Date + "~~^^^^~~" + dtpToDate.Date;
            Session["SerchCriteria"] = null;
            if(Convert.ToString(Request.QueryString["Type"]) == "1")
            Session["ReportName"] = "GetShipingLineWiseOpenLoads";
            else if (Convert.ToString(Request.QueryString["Type"]) == "2")
            Session["ReportName"] = "GetShipingLineWiseLoads";
            Response.Redirect("~/Manager/DisplayReport.aspx");
    }

    private void FillLists()
    {
        ddlShipingLines.Items.Clear();

        //SLine 2017 Enhancement for Office Location Starts
        long officeLocationId = 0;
        if (Session["OfficeLocationID"] != null)
        {
            officeLocationId = Convert.ToInt64(Session["OfficeLocationID"]);
        }
        //SLine 2017 Enhancement for Office Location Ends

        DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings["conString"], "SP_GetActiveShipingLines", new object[] { officeLocationId });
        if(ds.Tables.Count > 0)
        {
            ddlShipingLines.DataSource = ds.Tables[0];
            ddlShipingLines.DataTextField = "ShippingLineName";
            ddlShipingLines.DataValueField = "Id";
            ddlShipingLines.DataBind();
        }        
        if(ds != null) { ds.Dispose(); ds = null; }
    }
    
}
