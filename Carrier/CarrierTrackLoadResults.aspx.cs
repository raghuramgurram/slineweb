using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CarrierTrackLoadResults : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TopHeaderText"] = "Track Load";
        if (!IsPostBack)
        {
            DisplayDetails();
        }
    }

    private void DisplayDetails()
    {
        GridSearch.Visible = true;
        GridSearch.DeleteVisible = false;
        GridSearch.EditVisible = false;
        GridSearch.TableName = "eTn_Load";
        GridSearch.Primarykey = "eTn_Load.bint_LoadId";
        GridSearch.PageSize = Convert.ToInt32(ConfigurationSettings.AppSettings["GeneralPageSize"]);
        GridSearch.ColumnsList = "eTn_Load.bint_LoadId;" + CommonFunctions.LoadColumnQueryString("convert(varchar(20),eTn_Load.bint_LoadId);eTn_Load.nvar_Container;eTn_Load.nvar_Container1", "Load", "CarrierTrackLoad.aspx") + ";eTn_LoadType.nvar_LoadTypeDesc as 'Type';eTn_LoadStatus.nvar_LoadStatusDesc as 'Status';" + CommonFunctions.AddressQueryStringWithPhone("eTn_Customer.nvar_CustomerName;eTn_Customer.nvar_Street;eTn_Customer.nvar_Suite;eTn_Customer.nvar_City;eTn_Customer.nvar_State;eTn_Customer.nvar_Zip", "eTn_Customer.num_Phone", "Customer");
        GridSearch.VisibleColumnsList = "Load;Type;Status;Customer";
        GridSearch.VisibleHeadersList = "Load;Type;Status;Customer";
        GridSearch.InnerJoinClause = " inner join eTn_Customer on eTn_Load.bint_CustomerId = eTn_Customer.bint_CustomerId " +
               " inner join eTn_LoadStatus on eTn_Load.bint_LoadStatusId = eTn_LoadStatus.bint_LoadStatusId " +
               " inner join [eTn_AssignCarrier] on eTn_Load.[bint_LoadId]=[eTn_AssignCarrier].[bint_LoadId]" +
               " inner join eTn_LoadType on eTn_Load.bint_LoadTypeId = eTn_LoadType.bint_LoadTypeId ";
        if (Session["TrackWhereClause"] != null)
            GridSearch.WhereClause = Convert.ToString(Session["TrackWhereClause"]);
        GridSearch.IsPagerVisible = true;
        GridSearch.BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (Session["CarrierId"] != null)
        {
            if (txtContainer.Text.Trim().Length > 0)
            {
                switch (ddlContainer.SelectedIndex)
                {
                    case 0:
                        Session["TrackWhereClause"] = "(eTn_Load.[nvar_Container] like '" + txtContainer.Text.Trim() + "' or eTn_Load.[nvar_Container1] like '" + txtContainer.Text.Trim() + "')";
                        break;
                    case 1:
                        Session["TrackWhereClause"] = "[eTn_AssignCarrier].[nvar_Invoice] like '" + txtContainer.Text.Trim() + "%'";
                        break;
                    case 2:
                        Session["TrackWhereClause"] = "eTn_Load.[nvar_Booking] like '" + txtContainer.Text.Trim() + "%'";
                        break;
                    case 3:
                        Session["TrackWhereClause"] = "(eTn_Load.[nvar_Chasis] like '" + txtContainer.Text.Trim() + "%' or eTn_Load.[nvar_Chasis1] like '" + txtContainer.Text.Trim() + "%')";
                        break;
                    case 4:
                        try
                        {
                            Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=" + Convert.ToInt64(txtContainer.Text.Trim());
                        }
                        catch
                        {
                            Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=0";
                        }
                        break;
                }
                Session["TrackWhereClause"] += " and lower(eTn_Load.[nvar_AssignTo])='carrier' and [eTn_AssignCarrier].[bint_CarrierId]=" + Convert.ToString(Session["CarrierId"]);
            }
            else
            {
                Session["TrackWhereClause"] = "eTn_Load.[bint_LoadId]=0";
            }
            bool blVal = false;
            try
            {
                if (Convert.ToInt32(DBClass.executeScalar("select count([eTn_AssignCarrier].bint_LoadId) from eTn_Load inner join [eTn_AssignCarrier] on eTn_Load.[bint_LoadId]=[eTn_AssignCarrier].[bint_LoadId] where " + Convert.ToString(Session["TrackWhereClause"]))) == 1)
                {
                    blVal = true;
                }
            }
            catch
            {
            }
            if (blVal)
            {
                Response.Redirect("~/Carrier/CarrierTrackLoad.aspx?" + DBClass.executeScalar("select [eTn_AssignCarrier].bint_LoadId from eTn_Load inner join [eTn_AssignCarrier] on eTn_Load.[bint_LoadId]=[eTn_AssignCarrier].[bint_LoadId] where " + Convert.ToString(Session["TrackWhereClause"])));
            }
            
            DisplayDetails();
        }
    }
   
}
